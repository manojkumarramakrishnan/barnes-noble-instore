package bnInstoreBasicFeatures;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BNConstant 
{
	static String filename = new SimpleDateFormat("dd-MMM-yyyy HH-mm").format(new Date())+".html";
	//public static String BNInstoreASTUrl = "http:\\bn-uat2.skavaone.com\\skavastream\\studio\\reader\\preprod\\bninstore";
	public static String BNInstoreASTUrl = "http:\\bninstore.skavaone.com\\skavastream\\studio\\reader\\prod\\bninstore";
	public static String ObjectFile = "E:\\eclipseworkspace\\barnes-noble-instore\\src\\bnInstoreBasicFeatures\\objects.properties";
	//public static String BNInstoreKIOSKUrl = "http:\\bn-uat2.skavaone.com\\skavastream\\studio\\reader\\preprod/bninstore\\";
	public static String BNInstoreKIOSKUrl = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bnkiosk";
	public static String BNExtentReport = "E:\\eclipseworkspace\\barnes-noble-instore\\BNInstoreReport\\BNASTInstore "+filename;
	public static String commonExcelFile = "E:\\eclipseworkspace\\barnes-noble-instore\\ExcelFiles\\BNInstoreData.xls";
	public static String StoreID = "2932";
}
