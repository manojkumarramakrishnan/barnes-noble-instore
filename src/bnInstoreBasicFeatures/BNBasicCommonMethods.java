package bnInstoreBasicFeatures;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;

public class BNBasicCommonMethods
{
	public static Properties obj;
	public static WebDriverWait wait;
	public ExtentTest parent;
	public ExtentTest child;
	public static HSSFWorkbook workbook;
	public static HSSFSheet sheet;
	public static File BNExcelSrc;
	
	
	
	public static FileInputStream loadPropertiesFile() throws Exception
	{
		FileInputStream objfile = null;
		//Properties obj = null;
		try
		{
			if(objfile == null)
			{
				objfile = new FileInputStream(BNConstant.ObjectFile);
				//Thread.sleep(1000);
				return objfile;
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		return objfile;
	}
	
	public static WebElement findElement(WebDriver driver ,Properties obj, String ele) throws Exception
	{
		//Thread.sleep(500);
		WebElement varEle = driver.findElement(By.xpath(obj.getProperty(ele)));
		return varEle;
	}
	
	/*public static List<WebElement> findElements(WebDriver driver ,Properties obj, String ele)
	{
		List <WebElement> varEle = driver.findElements(By.xpath(obj.getProperty(ele)));
		return varEle;
	}*/
	
	
	public static void signInclearAll(WebElement id, WebElement pass)
	{
		id.clear();
		pass.clear();
	}
	
	public static boolean waitforElement(WebDriverWait wait, Properties obj, String element)
	{
		try
		{
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(obj.getProperty(element))));
			return true;
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}
	}
	
	public static void WaitForLoading(WebDriverWait wait)
	{
		try
		{
			wait.until(ExpectedConditions.attributeContains(By.xpath("//*[@id='skMob_fullPageSpinner']"), "style", "display: none;"));
			Thread.sleep(1000);
			  
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			
		}

	}
	/* Excel File Setup */
	public static HSSFWorkbook excelsetUp() throws IOException
	{
		File BNExcelSrc = new File(BNConstant.commonExcelFile);
		FileInputStream fis = new FileInputStream(BNExcelSrc);
		HSSFWorkbook workbook = new HSSFWorkbook(fis);
		return workbook;
	}
	
	/* Excel File Setup */
	public static HSSFSheet excelsetUp(String sheetName) throws IOException
	{
		File BNExcelSrc = new File(BNConstant.commonExcelFile);
		FileInputStream fis = new FileInputStream(BNExcelSrc);
		workbook = new HSSFWorkbook(fis);
		sheet = workbook.getSheet(sheetName);
		return sheet;
	}
	

	// Check Broken Image 
	public static int imageBroken(WebElement image,ArrayList<String> log) throws ClientProtocolException, IOException
	{
		HttpClient client = HttpClientBuilder.create().build();
		/*System.out.println(image);
		System.out.println(image.getAttribute("src"));*/
		HttpGet get = new HttpGet(image.getAttribute("src"));
		String user = "bnuser";
		String pass = "bn123";
		String authStr = user + ":" + pass;
		byte[] authEncBytes = Base64.encodeBase64(authStr.getBytes());
		String utf8 = new String(authEncBytes,"UTF-8");
		get.setHeader("authorization", "Basic " + utf8);
		HttpResponse response = client.execute(get);  
		log.add("The Verified Image URL : " + image.getAttribute("src") + " and its Response Code is : " + response.getStatusLine().getStatusCode());
		return response.getStatusLine().getStatusCode();
	}
	
	public static boolean isElementPresent(WebElement element)
	{
		try
		{
			element.isDisplayed();
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}
	
	// Check If List Element Present	
	public static boolean isListElementPresent(List<WebElement> element)
	{
		try
		{
			if(element.size()>0)
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
		return false;
	}
		
	
	//JS Script to Click Element
	public static void click(WebElement element, WebDriver driver)
	{
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	}
	
	// Color Code Converter FOR BORDER
	public static String colorfinder(String csvalue)
	{
		String ColorName = csvalue.replace("1px solid", "");
		Color colorhxcnvt = Color.fromString(ColorName);
		String hexCode = colorhxcnvt.asHex();
		return hexCode;
	}
	
	// Scroll Down to Element
	public static void scrolldown(WebElement element,WebDriver driver)
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);",element);
	}
	
	// Scroll Up to Element
	public static void scrollup(WebElement element,WebDriver driver)
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);",element);
	}
	
	/*// Navigate to Checkout page
	public static void checkoutNavigation(ArrayList<WebElement> elements, WebDriver driver)
	{
		int count = 0;
		try
		{
			wait.until(ExpectedConditions.visibilityOf(elements.get(0)));
			if(BNBasicfeature.isElementPresent(elements.get(0)))
			{
				boolean pgok = false;
				do
				{
					try
					{
						elements.get(0).click();
						wait.until(ExpectedConditions.visibilityOf(elements.get(1)));
						if(BNBasicfeature.isElementPresent(elements.get(1)))
						{
							WebElement ele = elements.get(1);
							BNBasicfeature.scrolldown(ele, driver);
							Thread.sleep(2000);
							elements.get(1).click();
							pgok = true;
							break;
						}
						else
						{
							Fail("The Shopping Bag is not found.");
							break;
						}
					}
					catch (Exception e)
					{
						if(BNBasicfeature.isElementPresent(elements.get(2)))
						{
							//elements.get(2).click();
							((JavascriptExecutor) driver).executeScript("arguments[0].click();", elements.get(2));
							Thread.sleep(3000);
							pgok = false;
							count++;
							System.out.println(count);
						}
						else
						{
							Fail("The Secure Checkout Button is not found on the shopping bag container section.");
							break;
						}
					}
				}
				while(count<3||pgok==true);
			}
			else
			{
				Fail("The Shopping Bag element is not found on the Home Page.");
			}
		}
		catch(Exception e)
		{
			Exception("There is something wrong with the Page. Please Check." + e.getMessage());
		}
	}
*/
	// Check Broken Image 
	
	//Write in Excel File
	
	// Get Value from the Excel Sheet
	public static String getExcelVal(String tcId, HSSFSheet sheet, int cellVal) throws Exception
	{
		String searchvalue =""; 
		try
		{
			int brmkey = sheet.getLastRowNum();
			for(int i = 0; i <= brmkey; i++)
			{
				String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
				if(cellCont.equals(tcId))
				{
					searchvalue = sheet.getRow(i).getCell(cellVal).getStringCellValue();
					break;
				}
				else
				{
					continue;
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		return searchvalue;
	}
	
	// Get Numeric Value from the Excel Sheet
	public static String getExcelNumericVal(String tcId, HSSFSheet sheet, int cellVal) throws Exception
		{
			String searchvalue =""; 
			try
			{
				int brmkey = sheet.getLastRowNum();
				for(int i = 0; i <= brmkey; i++)
				{
					String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
					if(cellCont.equals(tcId))
					{
						DataFormatter formatter = new DataFormatter(); //creating formatter using the default locale
						HSSFCell cell = sheet.getRow(i).getCell(cellVal);
						cell.setCellType(Cell.CELL_TYPE_STRING);
						searchvalue = formatter.formatCellValue(cell).toString();
					}
					else
					{
						continue;
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(e.getMessage());
			}
			return searchvalue;
		}
	// Color Code Converter
	public static String colorfinder(WebElement element)
	{
		String ColorName = element.getCssValue("border").replace("1px solid", "");
		Color colorhxcnvt = Color.fromString(ColorName);
		String hexCode = colorhxcnvt.asHex();
		//System.out.println(hexCode);
		return hexCode;
	}
	
	public static String colorfindernew(WebElement element)
	{
		String ColorName = element.getCssValue("background-color");
		Color colorhxcnvt = Color.fromString(ColorName);
		String hexCode = colorhxcnvt.asHex();
		return hexCode;
	}
	
	public static String colorfinderForeColor(WebElement element)
	{
		String ColorName = element.getCssValue("color");
		Color colorhxcnvt = Color.fromString(ColorName);
		String hexCode = colorhxcnvt.asHex();
		return hexCode;
	}
	
	// Random Number Generation
		public static int RandomNumGen(List<WebElement> num,ArrayList<String> log)
		{
			int sel=0;
			try
			{
				if(num.size()>0)
				{
					Random r = new Random();
					sel = r.nextInt(num.size());
					if(sel<1||sel==num.size())
					{
						sel=1;
					}
					log.add("The Randomly Selected Number is : " + sel);
				}
			}
			catch (Exception e) 
			{
				e.getMessage();
			}
			return sel;
		}
			
		// Seperating font from font family
	public static String isFontName(String Fontfamily)
	{
			Fontfamily.replaceAll(" ", "");
			String Splitname[]=Fontfamily.split(",");
			//	System.out.println("Font Name" +Splitname[1].trim());
			return Splitname[0];
	}

}
