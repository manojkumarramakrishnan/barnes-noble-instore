package extentReport;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import bnInstoreBasicFeatures.BNConstant;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.NetworkMode;

public abstract class extentRptManager 
{
	public static ExtentReports reporter;
	public ExtentTest parent;
	public  ExtentTest child;
	public ArrayList<String> log = new ArrayList<>();
	
	@BeforeSuite
	public static ExtentReports getReporter() 
	{
		
		if (reporter == null) 
		{
			reporter = new ExtentReports(BNConstant.BNExtentReport, false, NetworkMode.ONLINE);
		}
		return reporter;
	}
	
	@BeforeMethod
	public void beforeTest(Method name)
	{
		parent = reporter.startTest(name.getName());
		
	}
	
	@AfterMethod
	public void AfterMethod(Method name)
	{
		reporter.endTest(parent);
		reporter.flush();
	}
	
	@AfterSuite
	public void closeReporter() 
	{
		reporter.flush();
	}
	
	
	public void ChildCreation(String name)
	{
		child = reporter.startTest(name);
		parent.appendChild(child);
	}
	
	public void endTest()
	{
		reporter.endTest(child);
	}
	
	public void createlog(String val)
	{
		child.log(LogStatus.INFO, val);
	}
	
	public void pass(String val)
	{
		child.log(LogStatus.PASS, val);
		endTest();
	}
	
	public void pass(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.PASS, val);
		for(String lval:logval)
		{
			createlog(lval);
		}
		logval.removeAll(logval);
		endTest();
	}
	
	public void fail(String val)
	{
		child.log(LogStatus.FAIL, val);
		endTest();
	}
	
	public void fail(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.FAIL, val);
		for(String lval:logval)
		{
			createlog(lval);
		}
		logval.removeAll(logval);
		endTest();
	}
	
	public void exception(String val)
	{
		child.log(LogStatus.ERROR,val);
		endTest();
	}
	
	public void Skip(String val)
	{
		child.log(LogStatus.SKIP, val);
		endTest();
	}
	
}
