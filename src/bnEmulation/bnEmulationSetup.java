package bnEmulation;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import bnInstoreBasicFeatures.BNConstant;
import extentReport.extentRptManager;


public class bnEmulationSetup  extends extentRptManager
{
	public static WebDriver driver = null;
	
	
	public static WebDriver bnASTEmulationSetup() throws InterruptedException 
	{
		//Thread.sleep(250000);
		// Dimension for ipad mini
		WebDriver driver = null;
		System.setProperty("webdriver.chrome.driver","E:\\ChromeDriver\\chromedriver.exe");
		Map<String, String> mobileEmulation = new HashMap<String, String>();
		mobileEmulation.put("deviceName", "Apple iPad");
		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		chromeOptions.put("mobileEmulation", mobileEmulation);
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		if(driver==null)
		{
			driver = new ChromeDriver(capabilities);
			driver.manage().window().setSize(new Dimension(780, 670));
			driver.get(BNConstant.BNInstoreASTUrl);
		}
		return driver;
	}
	
	
	
	
	public static WebDriver bnASTEmulationSetupCheckout()
	{
		
		WebDriver driver = null;
		String chromeexePath = "E:\\ChromeDriver\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver",chromeexePath);
		Map<String, Object> deviceMetrics = new HashMap<String, Object>();
	/*	deviceMetrics.put("width", 1024);
		deviceMetrics.put("height",768);*/
		deviceMetrics.put("pixelRatio", 3.0);
		Map<String, Object> mobileEmulation = new HashMap<String, Object>();
		mobileEmulation.put("deviceMetrics", deviceMetrics);
		mobileEmulation.put("userAgent", "Mozilla/5.0 (iPad; CPU OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1");
		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		chromeOptions.put("mobileEmulation", mobileEmulation);
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		if(driver==null)
		{
			driver = new ChromeDriver(capabilities);
			driver.manage().deleteAllCookies();
			driver.manage().window().setSize(new Dimension( 1024  , 768 ));
			driver.get(BNConstant.BNInstoreASTUrl);
			
		}
		return driver;
	}

}


