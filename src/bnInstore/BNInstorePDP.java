package bnInstore;

import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import extentReport.extentRptManager;
import bnEmulation.bnEmulationSetup;
import bnInstoreBasicFeatures.BNBasicCommonMethods;
import bnInstoreBasicFeatures.BNConstant;


public class BNInstorePDP 
{
  WebDriver driver;
  public Properties obj;
  public ArrayList<String> log = new ArrayList<>();
  public WebDriverWait wait;
  
  public static ExtentReports reporter = extentRptManager.getReporter();
  public static ExtentTest parent, child;
  
  HSSFSheet sheet;
  boolean pdpTT = false;
  boolean pdpTF = false;
  boolean pdpFT = false;
  boolean pdpFF = false;
  public boolean desc = false;
  String TTprdtId = "";
  String TFprdtId = "";
  String FTprdtId = "";
  String FFprdtId = "";
  /*String TTprdtId = "9780545685405";
  String TFprdtId = "9780545685368";
  String FTprdtId = "9780594728962";
  String FFprdtId = "9781478602590";*/
  
  
  //Facets Boolean Variables
  static boolean FacetFlag331 = false;
  static boolean FacetFlag332 = false;
  static boolean FacetFlag333 = false;
  static boolean FacetFlag337 = false;
  static boolean FacetFlag334 = false;
  static boolean FacetFlag335 = false;
  static boolean FacetFlag336 = false;
  static boolean FacetFlag338 = false;
  static boolean FacetFlag339 = false;
  static boolean FacetFlag440 = false;
  static boolean FacetFlag342 = false;
  
  
  public BNInstorePDP() throws Exception
  {
	  obj = new Properties(System.getProperties());
	  obj.load(BNBasicCommonMethods.loadPropertiesFile());
	 // sheet = BNBasicCommonMethods.excelsetUp("display Splash screen");
  }
  
  /*Temp Login*/ 
	public void TempSignIn()
	  {    
		  // Temp Signin  
		  //WebDriverWait wait =  new WebDriverWait(driver, 30);
		    try
		    {
		    	BNBasicCommonMethods.waitforElement(wait, obj, "splashScreen");
		    	WebElement Splash=BNBasicCommonMethods.findElement(driver, obj, "splashScreen");
		    	BNBasicCommonMethods.WaitForLoading(wait);
		    	Splash.click();
		    	BNBasicCommonMethods.waitforElement(wait, obj, "signInEmpId");
		    	WebElement id = BNBasicCommonMethods.findElement(driver, obj, "signInEmpId");
		  	   	WebElement pass = BNBasicCommonMethods.findElement(driver, obj, "signInEmpPass");
		    	BNBasicCommonMethods.signInclearAll(id, pass);
		    	Thread.sleep(1000);
		    	id.sendKeys("234567854");
		    	pass.sendKeys("skava");
		    	BNBasicCommonMethods.findElement(driver, obj, "signInClick").click();
		    	BNBasicCommonMethods.WaitForLoading(wait);
		    	BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
		    	Thread.sleep(2000);
		    }
		    catch(Exception e)
		    {
		    	 exception(e.getMessage()); 
		    }
	  }
		    
		    public void SignIn()
		    {
		    	try
		  		{
		  		    BNBasicCommonMethods.waitforElement(wait, obj, "splashScreen");
		  		   	WebElement Splash=BNBasicCommonMethods.findElement(driver, obj, "splashScreen");
		  		   	BNBasicCommonMethods.WaitForLoading(wait);
		  		   	Splash.click();
		  		   	BNBasicCommonMethods.waitforElement(wait, obj, "signInEmpId");
		  	    	WebElement id = BNBasicCommonMethods.findElement(driver, obj, "signInEmpId");
		  	  	   	WebElement pass = BNBasicCommonMethods.findElement(driver, obj, "signInEmpPass");
		 		   	//id.clear();
		 		   // pass.clear();
	  		    	Thread.sleep(1500);
	  		    	/*Actions act = new Actions(driver);
	  		    	act.click().moveToElement(id).sendKeys("234567854");*/
	  		    	pass.sendKeys("skava");
	  		    	Thread.sleep(1000);
	  		    	id.click();
	  		    	Thread.sleep(1000);
		  		   	id.sendKeys("234567854");
		  		   	BNBasicCommonMethods.findElement(driver, obj, "signInClick").click();
		  	    	BNBasicCommonMethods.WaitForLoading(wait);
		  	    	BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
				    Thread.sleep(2000);
		  		}
		  		catch(Exception e)
		  		{
		  		   	exception(e.getMessage()); 
		  		}
		    }
	  
	  

	 
  /*Temp Signout*/
	public void TemSignOut() throws Exception 
	 {
		  //BNBasicCommonMethods.WaitForLoading(wait);	
		  Thread.sleep(2500);
	   	  BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
	  WebElement Hamburger = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
	  BNBasicCommonMethods.WaitForLoading(wait);
	 	  Hamburger.click();
	 	  Thread.sleep(2000);
	 	  BNBasicCommonMethods.waitforElement(wait, obj, "SignOut");
	 	  WebElement SignOut= BNBasicCommonMethods.findElement(driver, obj, "SignOut");
	 	 SignOut.click();
	 }
	
	public void TempSessionTimeout()
	{
		try
		{
			Thread.sleep(1500);
			if (driver instanceof JavascriptExecutor) {
			    ((JavascriptExecutor)driver).executeScript("ski_idleTimerObj.idleTimeCount=1");
			} 
			else 
			{
			    
				throw new IllegalStateException("This driver does not support JavaScript!");
			}
			Thread.sleep(3000);
			WebElement TimerSession = BNBasicCommonMethods.findElement(driver, obj, "TimerResetButton");
			TimerSession.click();
			Thread.sleep(5000);
		}
		catch(Exception e)
		{
			exception("There is something wrong or mmismatch occured"+e.getMessage());
		}
	}
  	
   /************************************************************** Product Description Page (PDP) **********************************************************************************/
	
  	@Test(priority= 2)
	public void BNIA08_PDP_Page() throws Exception
	{
		TempSignIn();
		BNIA346();
		BNIA348();
		PDPLogic();
		BNIA418();
		BNIA419();
		BNIA420();
		BNIA411();
		BNIA347();
		BNIA349();
		BNIA350();
		BNIA351();
		BNIA354();
		BNIA353();
		BNIA352();
		BNIA355();
		BNIA356();
		BNIA357();
		BNIA359();
		BNIA360();
		BNIA361();
		BNIA363();
		BNIA364();
		BNIA365();
		BNIA366();
		BNIA367();
		BNIA573();
		BNIA368();
		BNIA369();
		BNIA370();
		BNIA371();
		BNIA372();
		BNIA373();
		BNIA374();
		BNIA375();
		BNIA376();
		BNIA377();
		BNIA378();
		BNIA380();
		BNIA381();
		BNIA382();
	    BNIA383();
		BNIA384();
		BNIA385();
		BNIA386();
		BNIA387();
		BNIA388();
		BNIA389();
		BNIA390();
		BNIA391();
		BNIA392();
		BNIA393();
		BNIA394();
		BNIA395();
		BNIA396();
		BNIA397();
		BNIA398();
		BNIA399();
		BNIA400();
		BNIA401();
		BNIA402();
		//BNIA403(); // not able to automate
		BNIA405();
		BNIA406();
		BNIA719();
		BNIA407();
		BNIA408();
		BNIA409();
		BNIA410();
		BNIA423();
		BNIA412();
		BNIA413();
		BNIA414();
		BNIA415();
		BNIA417();
		BNIA421();
		BNIA422();
		BNIA424();
		BNIA430();
		BNIA431();
		BNIA432();
		BNIA433();
		BNIA434();
		BNIA435();
		BNIA436();
		BNIA437();
		BNIA438();
		BNIA440();
		BNIA447();
		BNIA449();
		BNIA714();
		BNIA715();
		BNIA716();
		BNIA717();
		BNIA528();
		//BNIA561(); - Email Mask - need to check
		BNIA571();
		BNIA576();
		BNIA577();
		BNIA579();
		BNIA580();
		BNIA425();
		BNIA426();
		BNIA427();
		BNIA428();
		BNIA429();
		//TemSignOut();	
		TempSessionTimeout();
	}
	
	/*BNIA346 - Verify that  Product detail page should be displayed as per the creative*/
	public void BNIA346()
	{
		ChildCreation("BNIA346 - Verify that  Product detail page should be displayed as per the creative");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "SearchTile");
			BNBasicCommonMethods.findElement(driver, obj, "SearchTile").click();
			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			WebElement searchbox1 = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String EAN = BNBasicCommonMethods.getExcelNumericVal("BNIA346", sheet, 3);
			searchbox1.sendKeys(EAN);
			Thread.sleep(500);
			Actions actt = new Actions(driver);
		    actt.sendKeys(Keys.ENTER).build().perform();
			///searchbox1.sendKeys(Keys.ENTER);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductName");
			WebElement ProductTitle = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductName");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AuthorName");
			WebElement AuthorName = BNBasicCommonMethods.findElement(driver, obj, "PDP_AuthorName");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_Reviews");
			WebElement Reviews = BNBasicCommonMethods.findElement(driver, obj, "PDP_Reviews");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SelectedFormatText");
			WebElement SelectedFormatText = BNBasicCommonMethods.findElement(driver, obj, "PDP_SelectedFormatText");
			WebElement SalePrice = BNBasicCommonMethods.findElement(driver, obj, "PDP_SalePrice");
			WebElement ATBButton = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
			WebElement EmailButton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			WebElement OverviewLinkText = BNBasicCommonMethods.findElement(driver, obj, "PDPOverviewLink");
			/*WebElement CustomerAlsoBoughtText = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomeralsoBoughtLink");
			WebElement CustomerReviewsTitle1 = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsTitle");
			WebElement customeralsobought = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomeralsoBoughtLink");*/
			//WebElement customerreview = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerReviewsLink");
			Actions act = new Actions(driver);
			SelectedFormatText.click();
			for(int ctr=0 ; ctr<50 ;ctr++)
			{
				act.sendKeys(Keys.CONTROL).sendKeys(Keys.ARROW_RIGHT).build().perform();
			}
			//BNBasicCommonMethods.scrollup(customerreview, driver);
			
			/*String url = driver.getCurrentUrl();
			driver.get(url);
			Thread.sleep(2000);
			customerreview = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerReviewsLink");
			BNBasicCommonMethods.scrollup(customerreview, driver);*/
			Thread.sleep(3000);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerAlsoBoughtThisSection_Products1");
			WebElement CustomerBoughtThisAlsoBoughtProductName = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerAlsoBoughtThisSection_Products1");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerAlsoBoughtThisSection_AuthorName1");
			WebElement CustomerBoughtThisAlsoBoughtAuthorName = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerAlsoBoughtThisSection_AuthorName1");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerReviewsTitle");
			WebElement CustomerReviewsTitle = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsTitle");
			
			//WebElement CustomerReviewsText = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsText1");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_EditorialReviews_PublisherText");
			WebElement EditorialReviewsPublisherText = BNBasicCommonMethods.findElement(driver, obj, "PDP_EditorialReviews_PublisherText");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductDetails_Text1");
			WebElement ProductDetails_Text = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductDetails_Text1");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_RelatedSubjects_Text1");
			WebElement RelatedSubjects_Text = BNBasicCommonMethods.findElement(driver, obj, "PDP_RelatedSubjects_Text1");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String title = BNBasicCommonMethods.getExcelVal("BNIA346", sheet, 4);
			String Title_Values[] = title.split("\n");
			String color = BNBasicCommonMethods.getExcelVal("BNIA346", sheet, 6);
			String Color_Values[] = color.split("\n");
			String size = BNBasicCommonMethods.getExcelVal("BNIA346", sheet, 5);
			String Size_Values[] = size.split("\n");
			
			//Product Title
			
			String Prodtitlename_font = Title_Values[0];
			String Prodtitle_size = Size_Values[0];
			String Prodtitlename_color = Color_Values[0];
			
			String PDPprdtitle_font = ProductTitle.getCssValue("font-family");
			String PDPprodtitle_size = ProductTitle.getCssValue("font-size");
			//System.out.println(""+PDPprdtitle_font);
			//System.out.println(""+PDPprodtitle_size);
			String PDPprodtitle_color = ProductTitle.getCssValue("color");
			Color PDPprodtitlecolor = Color.fromString(PDPprodtitle_color);
			String hexPDPprodtitlecolors = PDPprodtitlecolor.asHex();
			//System.out.println(""+hexPDPprodtitlecolors);
			
			if((PDPprdtitle_font.contains(Prodtitlename_font)) && (PDPprodtitle_size.equals(Prodtitle_size)) && (hexPDPprodtitlecolors.equals(Prodtitlename_color)))
			{
				log.add("The PDP Product Title Text font, size and color is "+"Current font name, size and color "+PDPprdtitle_font +PDPprodtitle_size +hexPDPprodtitlecolors +"Expected font name, size and color is "+Prodtitlename_font +Prodtitle_size +Prodtitlename_color);
				pass("PDP Product Title font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The PDP Product Title Text font, size and color is "+"Current font name, size and color "+PDPprdtitle_font +PDPprodtitle_size +hexPDPprodtitlecolors +"Expected font name, size and color is "+Prodtitlename_font +Prodtitle_size +Prodtitlename_color);
				fail("PDP Product Title font, font size, font color is not as per the creative",log);
			}	
			
			//Author Name
			String PDPAuthorname_font = Title_Values[1];
			String PDPAuthorname_size = Size_Values[1];
			String PDPAuthorname_color = Color_Values[0];
			String PDPAuthornamefont = AuthorName.getCssValue("font-family");
			String PDPAuthornamesize = AuthorName.getCssValue("font-size");
			//System.out.println(""+PDPAuthornamefont);
			//System.out.println(""+PDPAuthorname_size);
			String PDPAuthorname_size_color = AuthorName.getCssValue("color");
			Color PDPAuthorname_sizecolor = Color.fromString(PDPAuthorname_size_color);
			String hexPDPAuthorname_sizecolors = PDPAuthorname_sizecolor.asHex();
			//System.out.println(""+hexPDPAuthorname_sizecolors);
			
			if((PDPAuthornamefont.contains(PDPAuthorname_font)) && (PDPAuthornamesize.equals(PDPAuthorname_size)) && (hexPDPAuthorname_sizecolors.equals(PDPAuthorname_color)))
			{
				log.add("The PDP Author Name Text font, size and color is "+"Current font name, size and color "+PDPAuthornamefont +PDPAuthornamesize +hexPDPAuthorname_sizecolors +"Expected font name, size and color is "+PDPAuthorname_font +PDPAuthorname_size +PDPAuthorname_color);
				pass("PDP Author Name font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The Author Name Text font, size and color is "+"Current font name, size and color "+PDPAuthornamefont +PDPAuthornamesize +hexPDPAuthorname_sizecolors +"Expected font name, size and color is "+PDPAuthorname_font +PDPAuthorname_size +PDPAuthorname_color);
				fail("PDP Author Name font, font size, font color is not as per the creative",log);
			}
			
			//Reviews
			String PDPReviews_font = Title_Values[1];
			String PDPReviews_size = Size_Values[1];
			String PDPReviews_color = Color_Values[0];
			//String PDPReviews_lineheight = "16px";
			
			String PDPReviewsfont = Reviews.getCssValue("font-family");
			String PDPReviewssize = Reviews.getCssValue("font-size");
			//String PDPReviewslineheight = Reviews.getCssValue("line-height");
			//System.out.println(""+PDPReviewsfont);
			//System.out.println(""+PDPReviewssize);
			//System.out.println(""+PDPReviewslineheight);
			String PDPReviews_size_color = Reviews.getCssValue("color");
			Color PDPReviews_sizecolor = Color.fromString(PDPReviews_size_color);
			String hexPDPReviews_sizecolors = PDPReviews_sizecolor.asHex();
			//System.out.println(""+hexPDPReviews_sizecolors);
			
			if((PDPReviewsfont.contains(PDPReviews_font)) && (PDPReviewssize.equals(PDPReviews_size)) && (hexPDPReviews_sizecolors.equals(PDPReviews_color)))
			{
				log.add("The PDP Reviews Text font, size and color is "+"Current font name, size and color "+PDPReviewsfont +PDPReviewssize +hexPDPReviews_sizecolors +"Expected font name, size and color is "+PDPReviews_font +PDPReviews_size +PDPReviews_color);
				pass("PDP Reviews font, font size, font color is as per the creative",log);
				/*if(PDPReviewslineheight.equals(PDPReviews_lineheight))
				{
					log.add("The PDP Reviews Text Line Height is "+"Current "+PDPReviewslineheight +"Expected "+PDPReviews_lineheight);
					pass("PDP Reviews Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP Reviews Text Line Height is "+"Current "+PDPReviewslineheight +"Expected "+PDPReviews_lineheight);
					fail("PDP Reviews Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Reviews Text font, size and color is "+"Current font name, size and color "+PDPReviewsfont +PDPReviewssize +hexPDPReviews_sizecolors +"Expected font name, size and color is "+PDPReviews_font +PDPReviews_size +PDPReviews_color);
				fail("PDP Reviews font, font size, font color is not as per the creative",log);
			}
			
			//SelectedFormatText
			String PDPSelectedFormatText_font = Title_Values[2];
			String PDPSelectedFormatText_size = Size_Values[1];
			String PDPSelectedFormatText_color = Color_Values[0];
			//String PDPSelectedFormatText_lineheight = "28px";
			//String lineheight = "normal";
			
			String PDPSelectedFormatTextfont = SelectedFormatText.getCssValue("font-family");
			String PDPSelectedFormatTextsize = SelectedFormatText.getCssValue("font-size");
			//String PDPSelectedFormatTextlineheight = SelectedFormatText.getCssValue("line-height");
			//System.out.println(""+PDPSelectedFormatTextfont);
			//System.out.println(""+PDPSelectedFormatTextsize);
			//System.out.println(""+PDPSelectedFormatTextlineheight);
			String PDPSelectedFormatText_size_color = SelectedFormatText.getCssValue("color");
			Color PDPSelectedFormatText_sizecolor = Color.fromString(PDPSelectedFormatText_size_color);
			String hexPDPSelectedFormatText_sizecolors = PDPSelectedFormatText_sizecolor.asHex();
			//System.out.println(""+hexPDPSelectedFormatText_sizecolors);
			
			if((PDPSelectedFormatTextfont.contains(PDPSelectedFormatText_font)) && (PDPSelectedFormatTextsize.equals(PDPSelectedFormatText_size)) && (hexPDPSelectedFormatText_sizecolors.equals(PDPSelectedFormatText_color)))
			{
				log.add("The PDP Selected Format Text font, size and color is "+"Current font name, size and color "+PDPSelectedFormatTextfont +PDPSelectedFormatTextsize +hexPDPSelectedFormatText_sizecolors +"Expected font name, size and color is "+PDPSelectedFormatText_font +PDPSelectedFormatText_size +PDPSelectedFormatText_color);
				pass("PDP Selected Format font, font size, font color is as per the creative",log);
				/*if(PDPSelectedFormatTextlineheight.equals(PDPSelectedFormatText_lineheight))
				{
					log.add("The PDP Selected Format Text Line Height is "+"Current "+PDPSelectedFormatTextlineheight +"Expected "+PDPSelectedFormatText_lineheight);
					pass("PDP Selected Format Line height is as per the creative",log);
				}
				else if(PDPSelectedFormatTextlineheight.equals(lineheight))
				{
					log.add("The PDP Selected Format Text Line Height is "+"Current "+PDPSelectedFormatTextlineheight +"Expected "+PDPSelectedFormatText_lineheight);
					fail("PDP Selected Format Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Selected Format Text font, size and color is "+"Current font name, size and color "+PDPSelectedFormatTextfont +PDPSelectedFormatTextsize +hexPDPSelectedFormatText_sizecolors +"Expected font name, size and color is "+PDPSelectedFormatText_font +PDPSelectedFormatText_size +PDPSelectedFormatText_color);
				fail("PDP Reviews font, font size, font color is not as per the creative",log);
			}
			
			
			//SalePrice
			String PDPSalePrice_font = Title_Values[2];
			String PDPSalePrice_size = Size_Values[2];
			String PDPSalePrice_color = Color_Values[0];
			//String PDPSalePrice_lineheight = "24px";
			
			String PDPSalePricefont = SalePrice.getCssValue("font-family");
			String PDPSalePricesize = SalePrice.getCssValue("font-size");
			//String PDPSalePricelineheight = SalePrice.getCssValue("line-height");
			//System.out.println(""+PDPSalePricefont);
			//System.out.println(""+PDPSalePricesize);
			//System.out.println(""+PDPSalePricelineheight);
			String PDPSalePrice_size_color = SalePrice.getCssValue("color");
			Color PDPSalePrice_sizecolor = Color.fromString(PDPSalePrice_size_color);
			String hexPDPSalePrice_sizecolors = PDPSalePrice_sizecolor.asHex();
			//System.out.println(""+hexPDPSalePrice_sizecolors);
			
			if((PDPSalePricefont.contains(PDPSalePrice_font)) && (PDPSalePricesize.equals(PDPSalePrice_size)) && (hexPDPSalePrice_sizecolors.equals(PDPSalePrice_color)))
			{
				log.add("The PDP Sale Price Text font, size and color is \n"+"Current font name, size and color \n"+PDPSalePricefont +"\n"+PDPSalePricesize+"\n" +hexPDPSalePrice_sizecolors+"\n"+"Expected font name, size and color is "+PDPSalePrice_font +PDPSalePrice_size +PDPSalePrice_color);
				pass("PDP Sale Price font, font size, font color is as per the creative",log);
				/*if(PDPSalePricelineheight.equals(PDPSalePrice_lineheight))
				{
					log.add("The PDP Sale Price Text Line Height is "+"Current "+PDPSalePricelineheight +"Expected "+PDPSalePrice_lineheight);
					pass("PDP Sale Price Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP Sale Price Text Line Height is "+"Current "+PDPSalePricelineheight +"Expected "+PDPSalePrice_lineheight);
					fail("PDP Sale Price Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Sale Price Text font, size and color is "+"Current font name, size and color "+PDPSalePricefont +PDPSalePricesize +hexPDPSalePrice_sizecolors +"Expected font name, size and color is "+PDPSalePrice_font +PDPSalePrice_size +PDPSalePrice_color);
				fail("PDP Sale Price font, font size, font color is not as per the creative",log);
			}
			
			//ATBButton
			String PDPATBButton_font = Title_Values[1];
			String PDPATBButton_size = Size_Values[3];
			String PDPATBButtonText_color = Color_Values[1];
			//String PDPATBButton_color = Color_Values[2];
			//String PDPATBButton_lineheight = "16px";
			
			String PDPATBButtonfont = ATBButton.getCssValue("font-family");
			String PDPATBButtonsize = ATBButton.getCssValue("font-size");
			//String PDPATBButtonlineheight = ATBButton.getCssValue("line-height");
			//System.out.println(""+PDPATBButtonfont);
			//System.out.println(""+PDPATBButtonsize);
			//System.out.println(""+PDPSalePricelineheight);
			String PDPATBButtonText1_color = ATBButton.getCssValue("color");
			Color PDPATBButtonTexcolor = Color.fromString(PDPATBButtonText1_color);
			String hexPDPATBButton_sizecolors = PDPATBButtonTexcolor.asHex();
			//System.out.println(""+hexPDPATBButton_sizecolors);
			String PDPATBButton1_color = ATBButton.getCssValue("background-color");
			Color PDPATBButton_sizecolor = Color.fromString(PDPATBButton1_color);
			String hexPDPATBButton1_sizecolors = PDPATBButton_sizecolor.asHex();
			//System.out.println(""+hexPDPATBButton1_sizecolors);
			
			if((PDPATBButtonfont.contains(PDPATBButton_font)) && (PDPATBButtonsize.equals(PDPATBButton_size)) && (hexPDPATBButton_sizecolors.equals(PDPATBButtonText_color)))
			{
				log.add("The PDP ATB Button Text font, size and color is "+"Current font name, size and color "+PDPATBButtonfont +PDPATBButtonsize +hexPDPATBButton_sizecolors +"Expected font name, size and color is "+PDPATBButton_font +PDPATBButton_size +PDPATBButtonText_color);
				pass("PDP ATB Button font, font size, font color is as per the creative",log);
				/*if((PDPATBButtonlineheight.equals(PDPATBButton_lineheight)) && (hexPDPATBButton1_sizecolors.equals(PDPATBButton_color)))
				{
					log.add("The PDP ATB Button Text Line Height and background color is "+"Current "+PDPATBButtonlineheight +hexPDPATBButton1_sizecolors +"Expected "+PDPATBButton_lineheight +PDPATBButton_color);
					pass("PDP ATB Button Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP ATB Button Text Line Height and background color is "+"Current "+PDPATBButtonlineheight +hexPDPATBButton1_sizecolors +"Expected "+PDPATBButton_lineheight +PDPATBButton_color);
					fail("PDP ATB Button Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP ATB Button Text font, size and color is "+"Current font name, size and color "+PDPATBButtonfont +PDPATBButtonsize +hexPDPATBButton_sizecolors +"Expected font name, size and color is "+PDPATBButton_font +PDPATBButton_size +PDPATBButtonText_color);
				fail("PDP ATB Button font, font size, font color is not as per the creative",log);
			}
			
			//EmailButton
			String PDPEmailButton_font = Title_Values[1];
			String PDPEmailButton_size = Size_Values[1];
			String PDPEmailButton_color = Color_Values[3];
			//String PDPEmailButton_lineheight = "16px";
			String PDPEmailButtonBorder = Color_Values[3];
			
			String PDPEmailButtonfont = EmailButton.getCssValue("font-family");
			String PDPEmailButtonsize = EmailButton.getCssValue("font-size");
			//String PDPEmailButtonlineheight = SalePrice.getCssValue("line-height");
			String PDPEmailButtonBordercolor = EmailButton.getCssValue("border");
			String PDPEmailcolorRGBColor  = PDPEmailButtonBordercolor.replace("1px solid ", "");
			Color PDPbordercolor = Color.fromString(PDPEmailcolorRGBColor);
			String hexborder_colors = PDPbordercolor.asHex();
			//System.out.println(""+hexborder_colors);
			//System.out.println(""+PDPEmailButtonfont);
			//System.out.println(""+PDPEmailButtonsize);
			//System.out.println(""+PDPSalePricelineheight);
			String PDPEmailButton1_color = EmailButton.getCssValue("color");
			Color PDPEmailButtoncolor = Color.fromString(PDPEmailButton1_color);
			String hexPDPEmailButton_colors = PDPEmailButtoncolor.asHex();
			//System.out.println(""+hexPDPEmailButton_colors);
			
			if((PDPEmailButtonfont.contains(PDPEmailButton_font)) && (PDPEmailButtonsize.equals(PDPEmailButton_size)) && (hexPDPEmailButton_colors.equals(PDPEmailButton_color)))
			{
				log.add("The PDP EmailButton Text font, size and color is "+"Current font name, size and color "+PDPEmailButtonfont +PDPEmailButtonsize +hexPDPEmailButton_colors +"Expected font name, size and color is "+PDPEmailButton_font +PDPEmailButton_size +PDPEmailButton_color);
				pass("PDP Sale Price font, font size, font color is as per the creative",log);
				if((PDPEmailButtonBorder.contains(hexborder_colors)))
				{
					log.add("The PDP EmailButton Text Line Height and border color is "+"Current "+PDPEmailButtonBordercolor +"Expected "+PDPEmailButtonBorder);
					pass("PDP EmailButton Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP EmailButton Text Line Height and border color is "+"Current "+PDPEmailButtonBordercolor +"Expected " +PDPEmailButtonBorder);
					fail("PDP EmailButton Line height is not as per the creative",log);
				}
			}
			else
			{
				log.add("The PDP EmailButton Text font, size and color is "+"Current font name, size and color "+PDPEmailButtonfont +PDPEmailButtonsize +hexPDPEmailButton_colors +"Expected font name, size and color is "+PDPEmailButton_font +PDPEmailButton_size +PDPEmailButton_color);
				fail("PDP EmailButton font, font size, font color is not as per the creative",log);
			}
			
			
			//OverviewLinkText
			String PDPOverviewLinkText_font = Title_Values[2];
			String PDPOverviewLinkText_size = Size_Values[1];
			String PDPOverviewLinkText_color = Color_Values[3];
			
			String PDPOverviewLinkTextfont = OverviewLinkText.getCssValue("font-family");
			String PDPOverviewLinkTextsize = OverviewLinkText.getCssValue("font-size");
			//System.out.println(""+PDPOverviewLinkTextfont);
			//System.out.println(""+PDPOverviewLinkTextsize);
			String PDPOverviewLinkText_size_color = OverviewLinkText.getCssValue("color");
			Color PDPOverviewLinkText_sizecolor = Color.fromString(PDPOverviewLinkText_size_color);
			String hexPDPOverviewLinkText_sizecolors = PDPOverviewLinkText_sizecolor.asHex();
			//System.out.println(""+hexPDPOverviewLinkText_sizecolors);
			
			if((PDPOverviewLinkTextfont.contains(PDPOverviewLinkText_font)) && (PDPOverviewLinkTextsize.equals(PDPOverviewLinkText_size)) && (hexPDPOverviewLinkText_sizecolors.equals(PDPOverviewLinkText_color)))
			{
				log.add("The PDP OverviewLink Text font, size and color is "+"Current font name, size and color "+PDPOverviewLinkTextfont +PDPOverviewLinkTextsize +hexPDPOverviewLinkText_sizecolors +"Expected font name, size and color is "+PDPOverviewLinkText_font +PDPOverviewLinkText_size +PDPOverviewLinkText_color);
				pass("PDP OverviewLink font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The PDP OverviewLink Text font, size and color is "+"Current font name, size and color "+PDPOverviewLinkTextfont +PDPOverviewLinkTextsize +hexPDPOverviewLinkText_sizecolors +"Expected font name, size and color is "+PDPOverviewLinkText_font +PDPOverviewLinkText_size +PDPOverviewLinkText_color);
				fail("PDP OverviewLink font, font size, font color is not as per the creative",log);
			}
			
			
			//CustomerBoughtThisAlsoBoughtProductName
			
			String PDPCusBoughtPrdName_font = Title_Values[2];
			String PDPCusBoughtPrdName_size = Size_Values[3];
			String PDPCusBoughtPrdName_color = Color_Values[4];
			//String PDPCusBoughtPrdName_lineheight = "20px";
			
			String PDPCusBoughtPrdNamefont = CustomerBoughtThisAlsoBoughtProductName.getCssValue("font-family");
			String PDPCusBoughtPrdNamesize = CustomerBoughtThisAlsoBoughtProductName.getCssValue("font-size");
			//String PDPCusBoughtPrdNamelineheight = CustomerBoughtThisAlsoBoughtProductName.getCssValue("line-height");
			//System.out.println(""+PDPCusBoughtPrdNamefont);
			//System.out.println(""+PDPCusBoughtPrdNamesize);
			//System.out.println(""+PDPCusBoughtPrdNamelineheight);
			String PDPCusBoughtPrdName_size_color = CustomerBoughtThisAlsoBoughtProductName.getCssValue("color");
			Color PDPCusBoughtPrdName_sizecolor = Color.fromString(PDPCusBoughtPrdName_size_color);
			String hexPDPCusBoughtPrdName_sizecolors = PDPCusBoughtPrdName_sizecolor.asHex();
			//System.out.println(""+hexPDPCusBoughtPrdName_sizecolors);
			
			if((PDPCusBoughtPrdNamefont.contains(PDPCusBoughtPrdName_font)) && (PDPCusBoughtPrdNamesize.equals(PDPCusBoughtPrdName_size)) && (hexPDPCusBoughtPrdName_sizecolors.equals(PDPCusBoughtPrdName_color)))
			{
				log.add("The PDP Customer bought Product Name Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtPrdNamefont +PDPCusBoughtPrdNamesize +hexPDPCusBoughtPrdName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtPrdName_font +PDPCusBoughtPrdName_size +PDPCusBoughtPrdName_color);
				pass("PDP Customer bought Product Name font, font size, font color is as per the creative",log);
				/*if(PDPCusBoughtPrdNamelineheight.equals(PDPCusBoughtPrdName_lineheight))
				{
					log.add("The PDP Customer bought Product Name Text Line Height is "+"Current "+PDPCusBoughtPrdNamelineheight +"Expected "+PDPCusBoughtPrdName_lineheight);
					pass("PDP Customer bought Product Name Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP Customer bought Product Name Text Line Height is "+"Current "+PDPCusBoughtPrdNamelineheight +"Expected "+PDPCusBoughtPrdName_lineheight);
					fail("PDP Customer bought Product Name Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Customer bought Product Name Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtPrdNamefont +PDPCusBoughtPrdNamesize +hexPDPCusBoughtPrdName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtPrdName_font +PDPCusBoughtPrdName_size +PDPCusBoughtPrdName_color);
				fail("PDP Customer bought Product Name font, font size, font color is not as per the creative",log);
			}
			
			
			//CustomerBoughtThisAlsoBoughtAuthorName
			
			String PDPCusBoughtAuthorName_font = Title_Values[1];
			String PDPCusBoughtAuthorName_size = Size_Values[1];
			String PDPCusBoughtAuthorName_color = Color_Values[5];
			//String PDPCusBoughtAuthorName_lineheight = "20px";
			
			String PDPCusBoughtAuthorNamefont = CustomerBoughtThisAlsoBoughtAuthorName.getCssValue("font-family");
			String PDPCusBoughtAuthorNamesize = CustomerBoughtThisAlsoBoughtAuthorName.getCssValue("font-size");
			//String PDPCusBoughtAuthorNamelineheight = CustomerBoughtThisAlsoBoughtAuthorName.getCssValue("line-height");
			//System.out.println(""+PDPCusBoughtAuthorNamefont);
			//System.out.println(""+PDPCusBoughtAuthorNamesize);
			//System.out.println(""+PDPCusBoughtAuthorNamelineheight);
			String PDPCusBoughtAuthorName_size_color = CustomerBoughtThisAlsoBoughtAuthorName.getCssValue("color");
			Color PDPCusBoughtAuthorName_sizecolor = Color.fromString(PDPCusBoughtAuthorName_size_color);
			String hexPDPCusBoughtAuthorName_sizecolors = PDPCusBoughtAuthorName_sizecolor.asHex();
			//System.out.println(""+hexPDPCusBoughtAuthorName_sizecolors);
			
			if((PDPCusBoughtAuthorNamefont.contains(PDPCusBoughtAuthorName_font)) && (PDPCusBoughtAuthorNamesize.equals(PDPCusBoughtAuthorName_size)) && (hexPDPCusBoughtAuthorName_sizecolors.equals(PDPCusBoughtAuthorName_color)))
			{
				log.add("The PDP Customer bought Author Name Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtAuthorNamefont +PDPCusBoughtAuthorNamesize +hexPDPCusBoughtAuthorName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtAuthorName_font +PDPCusBoughtAuthorName_size +PDPCusBoughtAuthorName_color);
				pass("PDP Customer bought Author Name font, font size, font color is as per the creative",log);
				/*if(PDPCusBoughtAuthorNamelineheight.equals(PDPCusBoughtAuthorName_lineheight))
				{
					log.add("The PDP Customer bought Author Name Text Line Height is "+"Current "+PDPCusBoughtAuthorNamelineheight +"Expected "+PDPCusBoughtAuthorName_lineheight);
					pass("PDP Customer bought Author Name Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP Customer bought Author Name Text Line Height is "+"Current "+PDPCusBoughtAuthorNamelineheight +"Expected "+PDPCusBoughtAuthorName_lineheight);
					fail("PDP Customer bought Author Name Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Customer bought Author Name Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtAuthorNamefont +PDPCusBoughtAuthorNamesize +hexPDPCusBoughtAuthorName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtAuthorName_font +PDPCusBoughtAuthorName_size +PDPCusBoughtAuthorName_color);
				fail("PDP Customer bought Author Name font, font size, font color is not as per the creative",log);
			}
			
			
			//CustomerReviewsTitle
			
			String PDPCustomerReviewTitle_font = Title_Values[0];
			String PDPCustomerReviewTitle_size = Size_Values[2];
			String PDPCustomerReviewTitle_color = Color_Values[0];
			//String PDPCustomerReviewTitle_lineheight = "27px";
			
			String PDPCustomerReviewTitlefont = CustomerReviewsTitle.getCssValue("font-family");
			String PDPCustomerReviewTitlesize = CustomerReviewsTitle.getCssValue("font-size");
			//String PDPCustomerReviewTitlelineheight = CustomerReviewsTitle.getCssValue("line-height");
			//System.out.println(""+PDPCustomerReviewTitlefont);
			//System.out.println(""+PDPCustomerReviewTitlesize);
			//System.out.println(""+PDPCustomerReviewTitlelineheight);
			String PDPCustomerReviewTitle_size_color = CustomerReviewsTitle.getCssValue("color");
			Color PDPCustomerReviewTitle_sizecolor = Color.fromString(PDPCustomerReviewTitle_size_color);
			String hexPDPCustomerReviewTitlee_sizecolors = PDPCustomerReviewTitle_sizecolor.asHex();
			//System.out.println(""+hexPDPCustomerReviewTitlee_sizecolors);
			
			if((PDPCustomerReviewTitlefont.contains(PDPCustomerReviewTitle_font)) && (PDPCustomerReviewTitlesize.equals(PDPCustomerReviewTitle_size)) && (hexPDPCustomerReviewTitlee_sizecolors.equals(PDPCustomerReviewTitle_color)))
			{
				log.add("The PDP CustomerReviewsTitle Text font, size and color is "+"Current font name, size and color "+PDPCustomerReviewTitlefont +PDPCustomerReviewTitlesize +hexPDPCustomerReviewTitlee_sizecolors +"Expected font name, size and color is "+PDPCustomerReviewTitle_font +PDPCustomerReviewTitle_size +PDPCustomerReviewTitle_color);
				pass("PDP CustomerReviewsTitlee font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP CustomerReviewsTitle Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtAuthorNamefont +PDPCusBoughtAuthorNamesize +hexPDPCusBoughtAuthorName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtAuthorName_font +PDPCusBoughtAuthorName_size +PDPCusBoughtAuthorName_color);
				fail("PDP CustomerReviewsTitle font, font size, font color is not as per the creative",log);
			}
			
			
			//CustomerReviewsText
			
			String PDPCustomerReviewText_font = Title_Values[0];
			String PDPCustomerReviewText_size = Size_Values[1];
			String PDPCustomerReviewText_color = Color_Values[0];
			//String PDPCustomerReviewText_lineheight = "22px";
			
			String PDPCustomerReviewTextfont = CustomerReviewsTitle.getCssValue("font-family");
			String PDPCustomerReviewTextsize = CustomerReviewsTitle.getCssValue("font-size");
			//String PDPCustomerReviewTextlineheight = CustomerReviewsTitle.getCssValue("line-height");
			//System.out.println(""+PDPCustomerReviewTextfont);
			//System.out.println(""+PDPCustomerReviewTextsize);
			//System.out.println(""+PDPCustomerReviewTextlineheight);
			String PDPCustomerReviewText_size_color = CustomerReviewsTitle.getCssValue("color");
			Color PDPCustomerReviewText_sizecolor = Color.fromString(PDPCustomerReviewText_size_color);
			String hexPDPCustomerReviewText_sizecolors = PDPCustomerReviewText_sizecolor.asHex();
			//System.out.println(""+hexPDPCustomerReviewText_sizecolors);
			
			if((PDPCustomerReviewTextfont.contains(PDPCustomerReviewText_font)) && (PDPCustomerReviewTextsize.equals(PDPCustomerReviewText_size)) && (hexPDPCustomerReviewText_sizecolors.equals(PDPCustomerReviewText_color)))
			{
				log.add("The PDP CustomerReviewsTitle Text font, size and color is "+"Current font name, size and color "+PDPCustomerReviewTextfont +PDPCustomerReviewTextsize +hexPDPCustomerReviewText_sizecolors +"Expected font name, size and color is "+PDPCustomerReviewText_font +PDPCustomerReviewText_size +PDPCustomerReviewText_color);
				pass("PDP CustomerReviewsTitle font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP CustomerReviewsTitle Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtAuthorNamefont +PDPCusBoughtAuthorNamesize +hexPDPCusBoughtAuthorName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtAuthorName_font +PDPCusBoughtAuthorName_size +PDPCusBoughtAuthorName_color);
				fail("PDP CustomerReviewsTitle font, font size, font color is not as per the creative",log);
			}
			
			
			//EditorialReviewsPublisherText
			String PDPEditorialReviewsPublisherText_font = Title_Values[2];
			String PDPEditorialReviewsPublisherText_size = Size_Values[3];
			String PDPEditorialReviewsPublisherText_color = Color_Values[6];
			//String PDPEditorialReviewsPublisherText_lineheight = "22px";
			
			String PDPEditorialReviewsPublisherTextfont = EditorialReviewsPublisherText.getCssValue("font-family");
			String PDPEditorialReviewsPublisherTextsize = EditorialReviewsPublisherText.getCssValue("font-size");
			//String PDPEditorialReviewsPublisherTextlineheight = EditorialReviewsPublisherText.getCssValue("line-height");
			//System.out.println(""+PDPEditorialReviewsPublisherTextfont);
			//System.out.println(""+PDPEditorialReviewsPublisherTextsize);
			//System.out.println(""+PDPEditorialReviewsPublisherTextlineheight);
			String PDPEditorialReviewsPublisherText_size_color = EditorialReviewsPublisherText.getCssValue("color");
			Color PDPEditorialReviewsPublisherText_sizecolor = Color.fromString(PDPEditorialReviewsPublisherText_size_color);
			String hexPDPEditorialReviewsPublisherText_sizecolors = PDPEditorialReviewsPublisherText_sizecolor.asHex();
			//System.out.println(""+hexPDPEditorialReviewsPublisherText_sizecolors);
			
			if((PDPEditorialReviewsPublisherTextfont.contains(PDPEditorialReviewsPublisherText_font)) && (PDPEditorialReviewsPublisherTextsize.equals(PDPEditorialReviewsPublisherText_size)) && (hexPDPEditorialReviewsPublisherText_sizecolors.equals(PDPEditorialReviewsPublisherText_color)))
			{
				log.add("The PDP Editorial Review Publisher Text font, size and color is "+"Current font name, size and color "+PDPEditorialReviewsPublisherTextfont +PDPEditorialReviewsPublisherTextsize +hexPDPEditorialReviewsPublisherText_sizecolors +"Expected font name, size and color is "+PDPEditorialReviewsPublisherText_font +PDPEditorialReviewsPublisherText_size +PDPEditorialReviewsPublisherText_color);
				pass("PDP Editorial Review Publisher font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP Editorial Review Publisher Text font, size and color is "+"Current font name, size and color "+PDPEditorialReviewsPublisherTextfont +PDPEditorialReviewsPublisherTextsize +hexPDPEditorialReviewsPublisherText_sizecolors +"Expected font name, size and color is "+PDPEditorialReviewsPublisherText_font +PDPEditorialReviewsPublisherText_size +PDPEditorialReviewsPublisherText_color);
				fail("PDP Editorial Review Publishere font, font size, font color is not as per the creative",log);
			}
			
			
			
			//ProductDetails_Text
			
			String PDPProductDetailsText_font = Title_Values[1];
			String PDPProductDetailsText_size = Size_Values[3];
			String PDPProductDetailsText_color = Color_Values[6];
			//String PDPProductDetailsText_lineheight = "22px";
			
			String PDPProductDetailsTextfont = ProductDetails_Text.getCssValue("font-family");
			String PDPProductDetailsTextsize = ProductDetails_Text.getCssValue("font-size");
			//String PDPProductDetailsTextlineheight = ProductDetails_Text.getCssValue("line-height");
			//System.out.println(""+PDPProductDetailsTextfont);
			//System.out.println(""+PDPProductDetailsTextsize);
			//System.out.println(""+PDPProductDetailsTextlineheight);
			String PDPProductDetailsText_size_color = ProductDetails_Text.getCssValue("color");
			Color PDPProductDetailsText_sizecolor = Color.fromString(PDPProductDetailsText_size_color);
			String hexPDPProductDetailsText_sizecolors = PDPProductDetailsText_sizecolor.asHex();
			//System.out.println(""+hexPDPProductDetailsText_sizecolors);
			
			if((PDPProductDetailsTextfont.contains(PDPProductDetailsText_font)) && (PDPProductDetailsTextsize.equals(PDPProductDetailsText_size)) && (hexPDPProductDetailsText_sizecolors.equals(PDPProductDetailsText_color)))
			{
				log.add("The PDP Product details Text font, size and color is "+"Current font name, size and color "+PDPProductDetailsTextfont +PDPProductDetailsTextsize +hexPDPProductDetailsText_sizecolors +"Expected font name, size and color is "+PDPProductDetailsText_font +PDPProductDetailsText_size +PDPProductDetailsText_color);
				pass("PDP Product details font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP Product details Text font, size and color is "+"Current font name, size and color "+PDPProductDetailsTextfont +PDPProductDetailsTextsize +hexPDPProductDetailsText_sizecolors +"Expected font name, size and color is "+PDPProductDetailsText_font +PDPProductDetailsText_size +PDPProductDetailsText_color);
				fail("PDP Product details font, font size, font color is not as per the creative",log);
			}
			
			
			//RelatedSubjects_Text
			
			String PDPRelatedSubjectsText_font = Title_Values[1];
			String PDPRelatedSubjectsText_size = Size_Values[4];
			String PDPRelatedSubjectsText_color = Color_Values[1];
			//String PDPRelatedSubjectsText_lineheight = "22px";
			
			String PDPRelatedSubjectsTextfont = RelatedSubjects_Text.getCssValue("font-family");
			String PDPRelatedSubjectsTextsize = RelatedSubjects_Text.getCssValue("font-size");
			//String PDPRelatedSubjectsTextlineheight = RelatedSubjects_Text.getCssValue("line-height");
			//System.out.println(""+PDPRelatedSubjectsTextfont);
			//System.out.println(""+PDPRelatedSubjectsTextsize);
			//System.out.println(""+PDPRelatedSubjectsTextlineheight);
			String PDPRelatedSubjectsText_size_color = RelatedSubjects_Text.getCssValue("color");
			Color PDPRelatedSubjectsText_sizecolor = Color.fromString(PDPRelatedSubjectsText_size_color);
			String hexPDPRelatedSubjectsText_sizecolors = PDPRelatedSubjectsText_sizecolor.asHex();
			//System.out.println(""+hexPDPRelatedSubjectsText_sizecolors);
			
			if((PDPRelatedSubjectsTextfont.contains(PDPRelatedSubjectsText_font)) && (PDPRelatedSubjectsTextsize.equals(PDPRelatedSubjectsText_size)) && (hexPDPRelatedSubjectsText_sizecolors.equals(PDPRelatedSubjectsText_color)))
			{
				log.add("The PDP Related Subject Text font, size and color is "+"Current font name, size and color "+PDPRelatedSubjectsTextfont +PDPRelatedSubjectsTextsize +hexPDPRelatedSubjectsText_sizecolors +"Expected font name, size and color is "+PDPRelatedSubjectsText_font +PDPRelatedSubjectsText_size +PDPRelatedSubjectsText_color);
				pass("PDP Related Subject font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP Related Subject Text font, size and color is "+"Current font name, size and color "+PDPRelatedSubjectsTextfont +PDPRelatedSubjectsTextsize +hexPDPRelatedSubjectsText_sizecolors +"Expected font name, size and color is "+PDPRelatedSubjectsText_font +PDPRelatedSubjectsText_size +PDPRelatedSubjectsText_color);
				fail("PDP Related Subjects font, font size, font color is not as per the creative",log);
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA346"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA348 - Verify that  font size, font color, padding in the product detail page should be as per the creative*/
	public void BNIA348()
	{
		ChildCreation("BNIA348 - Verify that  font size, font color, padding in the product detail page should be as per the creative");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductName");
			WebElement ProductTitle = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductName");
			WebElement AuthorName = BNBasicCommonMethods.findElement(driver, obj, "PDP_AuthorName");
			WebElement Reviews = BNBasicCommonMethods.findElement(driver, obj, "PDP_Reviews");
			WebElement SelectedFormatText = BNBasicCommonMethods.findElement(driver, obj, "PDP_SelectedFormatText");
			WebElement SalePrice = BNBasicCommonMethods.findElement(driver, obj, "PDP_SalePrice");
			WebElement ATBButton = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
			WebElement EmailButton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			WebElement OverviewLinkText = BNBasicCommonMethods.findElement(driver, obj, "PDPOverviewLink");
			/*WebElement CustomerAlsoBoughtText = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomeralsoBoughtLink");
			WebElement CustomerReviewsTitle1 = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsTitle");
			WebElement customeralsobought = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomeralsoBoughtLink");*/
			//WebElement customerreview = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerReviewsLink");
			Actions act = new Actions(driver);
			SelectedFormatText.click();
			for(int ctr=0 ; ctr<50 ;ctr++)
			{
				act.sendKeys(Keys.CONTROL).sendKeys(Keys.ARROW_RIGHT).build().perform();
			}
			//BNBasicCommonMethods.scrollup(customerreview, driver);
			
			/*String url = driver.getCurrentUrl();
			driver.get(url);
			Thread.sleep(2000);
			customerreview = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerReviewsLink");
			BNBasicCommonMethods.scrollup(customerreview, driver);*/
			Thread.sleep(2000);
			WebElement CustomerBoughtThisAlsoBoughtProductName = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerAlsoBoughtThisSection_Products1");
			WebElement CustomerBoughtThisAlsoBoughtAuthorName = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerAlsoBoughtThisSection_AuthorName1");
			WebElement CustomerReviewsTitle = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsTitle");
			//WebElement CustomerReviewsText = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsText1");
			WebElement EditorialReviewsPublisherText = BNBasicCommonMethods.findElement(driver, obj, "PDP_EditorialReviews_PublisherText");
			WebElement ProductDetails_Text = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductDetails_Text1");
			WebElement RelatedSubjects_Text = BNBasicCommonMethods.findElement(driver, obj, "PDP_RelatedSubjects_Text1");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String title = BNBasicCommonMethods.getExcelVal("BNIA346", sheet, 4);
			String Title_Values[] = title.split("\n");
			String color = BNBasicCommonMethods.getExcelVal("BNIA346", sheet, 6);
			String Color_Values[] = color.split("\n");
			String size = BNBasicCommonMethods.getExcelVal("BNIA346", sheet, 5);
			String Size_Values[] = size.split("\n");
			
			//Product Title
			
			String Prodtitlename_font = Title_Values[0];
			String Prodtitle_size = Size_Values[0];
			String Prodtitlename_color = Color_Values[0];
			
			String PDPprdtitle_font = ProductTitle.getCssValue("font-family");
			String PDPprodtitle_size = ProductTitle.getCssValue("font-size");
			//System.out.println(""+PDPprdtitle_font);
			//System.out.println(""+PDPprodtitle_size);
			String PDPprodtitle_color = ProductTitle.getCssValue("color");
			Color PDPprodtitlecolor = Color.fromString(PDPprodtitle_color);
			String hexPDPprodtitlecolors = PDPprodtitlecolor.asHex();
			//System.out.println(""+hexPDPprodtitlecolors);
			
			if((PDPprdtitle_font.contains(Prodtitlename_font)) && (PDPprodtitle_size.equals(Prodtitle_size)) && (hexPDPprodtitlecolors.equals(Prodtitlename_color)))
			{
				log.add("The PDP Product Title Text font, size and color is "+"Current font name, size and color "+PDPprdtitle_font +PDPprodtitle_size +hexPDPprodtitlecolors +"Expected font name, size and color is "+Prodtitlename_font +Prodtitle_size +Prodtitlename_color);
				pass("PDP Product Title font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The PDP Product Title Text font, size and color is "+"Current font name, size and color "+PDPprdtitle_font +PDPprodtitle_size +hexPDPprodtitlecolors +"Expected font name, size and color is "+Prodtitlename_font +Prodtitle_size +Prodtitlename_color);
				fail("PDP Product Title font, font size, font color is not as per the creative",log);
			}	
			
			//Author Name
			String PDPAuthorname_font = Title_Values[1];
			String PDPAuthorname_size = Size_Values[1];
			String PDPAuthorname_color = Color_Values[0];
			String PDPAuthornamefont = AuthorName.getCssValue("font-family");
			String PDPAuthornamesize = AuthorName.getCssValue("font-size");
			//System.out.println(""+PDPAuthornamefont);
			//System.out.println(""+PDPAuthorname_size);
			String PDPAuthorname_size_color = AuthorName.getCssValue("color");
			Color PDPAuthorname_sizecolor = Color.fromString(PDPAuthorname_size_color);
			String hexPDPAuthorname_sizecolors = PDPAuthorname_sizecolor.asHex();
			//System.out.println(""+hexPDPAuthorname_sizecolors);
			
			if((PDPAuthornamefont.contains(PDPAuthorname_font)) && (PDPAuthornamesize.equals(PDPAuthorname_size)) && (hexPDPAuthorname_sizecolors.equals(PDPAuthorname_color)))
			{
				log.add("The PDP Author Name Text font, size and color is "+"Current font name, size and color "+PDPAuthornamefont +PDPAuthornamesize +hexPDPAuthorname_sizecolors +"Expected font name, size and color is "+PDPAuthorname_font +PDPAuthorname_size +PDPAuthorname_color);
				pass("PDP Author Name font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The Author Name Text font, size and color is "+"Current font name, size and color "+PDPAuthornamefont +PDPAuthornamesize +hexPDPAuthorname_sizecolors +"Expected font name, size and color is "+PDPAuthorname_font +PDPAuthorname_size +PDPAuthorname_color);
				fail("PDP Author Name font, font size, font color is not as per the creative",log);
			}
			
			//Reviews
			String PDPReviews_font = Title_Values[1];
			String PDPReviews_size = Size_Values[1];
			String PDPReviews_color = Color_Values[0];
			//String PDPReviews_lineheight = "16px";
			
			String PDPReviewsfont = Reviews.getCssValue("font-family");
			String PDPReviewssize = Reviews.getCssValue("font-size");
			//String PDPReviewslineheight = Reviews.getCssValue("line-height");
			//System.out.println(""+PDPReviewsfont);
			//System.out.println(""+PDPReviewssize);
			//System.out.println(""+PDPReviewslineheight);
			String PDPReviews_size_color = Reviews.getCssValue("color");
			Color PDPReviews_sizecolor = Color.fromString(PDPReviews_size_color);
			String hexPDPReviews_sizecolors = PDPReviews_sizecolor.asHex();
			//System.out.println(""+hexPDPReviews_sizecolors);
			
			if((PDPReviewsfont.contains(PDPReviews_font)) && (PDPReviewssize.equals(PDPReviews_size)) && (hexPDPReviews_sizecolors.equals(PDPReviews_color)))
			{
				log.add("The PDP Reviews Text font, size and color is "+"Current font name, size and color "+PDPReviewsfont +PDPReviewssize +hexPDPReviews_sizecolors +"Expected font name, size and color is "+PDPReviews_font +PDPReviews_size +PDPReviews_color);
				pass("PDP Reviews font, font size, font color is as per the creative",log);
				/*if(PDPReviewslineheight.equals(PDPReviews_lineheight))
				{
					log.add("The PDP Reviews Text Line Height is "+"Current "+PDPReviewslineheight +"Expected "+PDPReviews_lineheight);
					pass("PDP Reviews Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP Reviews Text Line Height is "+"Current "+PDPReviewslineheight +"Expected "+PDPReviews_lineheight);
					fail("PDP Reviews Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Reviews Text font, size and color is "+"Current font name, size and color "+PDPReviewsfont +PDPReviewssize +hexPDPReviews_sizecolors +"Expected font name, size and color is "+PDPReviews_font +PDPReviews_size +PDPReviews_color);
				fail("PDP Reviews font, font size, font color is not as per the creative",log);
			}
			
			//SelectedFormatText
			String PDPSelectedFormatText_font = Title_Values[2];
			String PDPSelectedFormatText_size = Size_Values[1];
			String PDPSelectedFormatText_color = Color_Values[0];
			//String PDPSelectedFormatText_lineheight = "28px";
			//String lineheight = "normal";
			
			String PDPSelectedFormatTextfont = SelectedFormatText.getCssValue("font-family");
			String PDPSelectedFormatTextsize = SelectedFormatText.getCssValue("font-size");
			//String PDPSelectedFormatTextlineheight = SelectedFormatText.getCssValue("line-height");
			//System.out.println(""+PDPSelectedFormatTextfont);
			//System.out.println(""+PDPSelectedFormatTextsize);
			//System.out.println(""+PDPSelectedFormatTextlineheight);
			String PDPSelectedFormatText_size_color = SelectedFormatText.getCssValue("color");
			Color PDPSelectedFormatText_sizecolor = Color.fromString(PDPSelectedFormatText_size_color);
			String hexPDPSelectedFormatText_sizecolors = PDPSelectedFormatText_sizecolor.asHex();
			//System.out.println(""+hexPDPSelectedFormatText_sizecolors);
			
			if((PDPSelectedFormatTextfont.contains(PDPSelectedFormatText_font)) && (PDPSelectedFormatTextsize.equals(PDPSelectedFormatText_size)) && (hexPDPSelectedFormatText_sizecolors.equals(PDPSelectedFormatText_color)))
			{
				log.add("The PDP Selected Format Text font, size and color is "+"Current font name, size and color "+PDPSelectedFormatTextfont +PDPSelectedFormatTextsize +hexPDPSelectedFormatText_sizecolors +"Expected font name, size and color is "+PDPSelectedFormatText_font +PDPSelectedFormatText_size +PDPSelectedFormatText_color);
				pass("PDP Selected Format font, font size, font color is as per the creative",log);
				/*if(PDPSelectedFormatTextlineheight.equals(PDPSelectedFormatText_lineheight))
				{
					log.add("The PDP Selected Format Text Line Height is "+"Current "+PDPSelectedFormatTextlineheight +"Expected "+PDPSelectedFormatText_lineheight);
					pass("PDP Selected Format Line height is as per the creative",log);
				}
				else if(PDPSelectedFormatTextlineheight.equals(lineheight))
				{
					log.add("The PDP Selected Format Text Line Height is "+"Current "+PDPSelectedFormatTextlineheight +"Expected "+PDPSelectedFormatText_lineheight);
					fail("PDP Selected Format Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Selected Format Text font, size and color is "+"Current font name, size and color "+PDPSelectedFormatTextfont +PDPSelectedFormatTextsize +hexPDPSelectedFormatText_sizecolors +"Expected font name, size and color is "+PDPSelectedFormatText_font +PDPSelectedFormatText_size +PDPSelectedFormatText_color);
				fail("PDP Reviews font, font size, font color is not as per the creative",log);
			}
			
			
			//SalePrice
			String PDPSalePrice_font = Title_Values[2];
			String PDPSalePrice_size = Size_Values[2];
			String PDPSalePrice_color = Color_Values[0];
			//String PDPSalePrice_lineheight = "24px";
			
			String PDPSalePricefont = SalePrice.getCssValue("font-family");
			String PDPSalePricesize = SalePrice.getCssValue("font-size");
			//String PDPSalePricelineheight = SalePrice.getCssValue("line-height");
			//System.out.println(""+PDPSalePricefont);
			//System.out.println(""+PDPSalePricesize);
			//System.out.println(""+PDPSalePricelineheight);
			String PDPSalePrice_size_color = SalePrice.getCssValue("color");
			Color PDPSalePrice_sizecolor = Color.fromString(PDPSalePrice_size_color);
			String hexPDPSalePrice_sizecolors = PDPSalePrice_sizecolor.asHex();
			//System.out.println(""+hexPDPSalePrice_sizecolors);
			
			if((PDPSalePricefont.contains(PDPSalePrice_font)) && (PDPSalePricesize.equals(PDPSalePrice_size)) && (hexPDPSalePrice_sizecolors.equals(PDPSalePrice_color)))
			{
				log.add("The PDP Sale Price Text font, size and color is \n"+"Current font name, size and color \n"+PDPSalePricefont +"\n"+PDPSalePricesize+"\n" +hexPDPSalePrice_sizecolors+"\n"+"Expected font name, size and color is "+PDPSalePrice_font +PDPSalePrice_size +PDPSalePrice_color);
				pass("PDP Sale Price font, font size, font color is as per the creative",log);
				/*if(PDPSalePricelineheight.equals(PDPSalePrice_lineheight))
				{
					log.add("The PDP Sale Price Text Line Height is "+"Current "+PDPSalePricelineheight +"Expected "+PDPSalePrice_lineheight);
					pass("PDP Sale Price Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP Sale Price Text Line Height is "+"Current "+PDPSalePricelineheight +"Expected "+PDPSalePrice_lineheight);
					fail("PDP Sale Price Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Sale Price Text font, size and color is "+"Current font name, size and color "+PDPSalePricefont +PDPSalePricesize +hexPDPSalePrice_sizecolors +"Expected font name, size and color is "+PDPSalePrice_font +PDPSalePrice_size +PDPSalePrice_color);
				fail("PDP Sale Price font, font size, font color is not as per the creative",log);
			}
			
			//ATBButton
			String PDPATBButton_font = Title_Values[1];
			String PDPATBButton_size = Size_Values[3];
			String PDPATBButtonText_color = Color_Values[1];
			//String PDPATBButton_color = Color_Values[2];
			//String PDPATBButton_lineheight = "16px";
			
			String PDPATBButtonfont = ATBButton.getCssValue("font-family");
			String PDPATBButtonsize = ATBButton.getCssValue("font-size");
			//String PDPATBButtonlineheight = ATBButton.getCssValue("line-height");
			//System.out.println(""+PDPATBButtonfont);
			//System.out.println(""+PDPATBButtonsize);
			//System.out.println(""+PDPSalePricelineheight);
			String PDPATBButtonText1_color = ATBButton.getCssValue("color");
			Color PDPATBButtonTexcolor = Color.fromString(PDPATBButtonText1_color);
			String hexPDPATBButton_sizecolors = PDPATBButtonTexcolor.asHex();
			//System.out.println(""+hexPDPATBButton_sizecolors);
			String PDPATBButton1_color = ATBButton.getCssValue("background-color");
			Color PDPATBButton_sizecolor = Color.fromString(PDPATBButton1_color);
			String hexPDPATBButton1_sizecolors = PDPATBButton_sizecolor.asHex();
			//System.out.println(""+hexPDPATBButton1_sizecolors);
			
			if((PDPATBButtonfont.contains(PDPATBButton_font)) && (PDPATBButtonsize.equals(PDPATBButton_size)) && (hexPDPATBButton_sizecolors.equals(PDPATBButtonText_color)))
			{
				log.add("The PDP ATB Button Text font, size and color is "+"Current font name, size and color "+PDPATBButtonfont +PDPATBButtonsize +hexPDPATBButton_sizecolors +"Expected font name, size and color is "+PDPATBButton_font +PDPATBButton_size +PDPATBButtonText_color);
				pass("PDP ATB Button font, font size, font color is as per the creative",log);
				/*if((PDPATBButtonlineheight.equals(PDPATBButton_lineheight)) && (hexPDPATBButton1_sizecolors.equals(PDPATBButton_color)))
				{
					log.add("The PDP ATB Button Text Line Height and background color is "+"Current "+PDPATBButtonlineheight +hexPDPATBButton1_sizecolors +"Expected "+PDPATBButton_lineheight +PDPATBButton_color);
					pass("PDP ATB Button Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP ATB Button Text Line Height and background color is "+"Current "+PDPATBButtonlineheight +hexPDPATBButton1_sizecolors +"Expected "+PDPATBButton_lineheight +PDPATBButton_color);
					fail("PDP ATB Button Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP ATB Button Text font, size and color is "+"Current font name, size and color "+PDPATBButtonfont +PDPATBButtonsize +hexPDPATBButton_sizecolors +"Expected font name, size and color is "+PDPATBButton_font +PDPATBButton_size +PDPATBButtonText_color);
				fail("PDP ATB Button font, font size, font color is not as per the creative",log);
			}
			
			//EmailButton
			String PDPEmailButton_font = Title_Values[1];
			String PDPEmailButton_size = Size_Values[1];
			String PDPEmailButton_color = Color_Values[3];
			//String PDPEmailButton_lineheight = "16px";
			String PDPEmailButtonBorder = Color_Values[3];
			
			String PDPEmailButtonfont = EmailButton.getCssValue("font-family");
			String PDPEmailButtonsize = EmailButton.getCssValue("font-size");
			//String PDPEmailButtonlineheight = SalePrice.getCssValue("line-height");
			String PDPEmailButtonBordercolor = EmailButton.getCssValue("border");
			String PDPEmailcolorRGBColor  = PDPEmailButtonBordercolor.replace("1px solid ", "");
			Color PDPbordercolor = Color.fromString(PDPEmailcolorRGBColor);
			String hexborder_colors = PDPbordercolor.asHex();
			//System.out.println(""+hexborder_colors);
			//System.out.println(""+PDPEmailButtonfont);
			//System.out.println(""+PDPEmailButtonsize);
			//System.out.println(""+PDPSalePricelineheight);
			String PDPEmailButton1_color = EmailButton.getCssValue("color");
			Color PDPEmailButtoncolor = Color.fromString(PDPEmailButton1_color);
			String hexPDPEmailButton_colors = PDPEmailButtoncolor.asHex();
			//System.out.println(""+hexPDPEmailButton_colors);
			
			if((PDPEmailButtonfont.contains(PDPEmailButton_font)) && (PDPEmailButtonsize.equals(PDPEmailButton_size)) && (hexPDPEmailButton_colors.equals(PDPEmailButton_color)))
			{
				log.add("The PDP EmailButton Text font, size and color is "+"Current font name, size and color "+PDPEmailButtonfont +PDPEmailButtonsize +hexPDPEmailButton_colors +"Expected font name, size and color is "+PDPEmailButton_font +PDPEmailButton_size +PDPEmailButton_color);
				pass("PDP Sale Price font, font size, font color is as per the creative",log);
				if((PDPEmailButtonBorder.contains(hexborder_colors)))
				{
					log.add("The PDP EmailButton Text Line Height and border color is "+"Current "+PDPEmailButtonBordercolor +"Expected "+PDPEmailButtonBorder);
					pass("PDP EmailButton Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP EmailButton Text Line Height and border color is "+"Current "+PDPEmailButtonBordercolor +"Expected " +PDPEmailButtonBorder);
					fail("PDP EmailButton Line height is not as per the creative",log);
				}
			}
			else
			{
				log.add("The PDP EmailButton Text font, size and color is "+"Current font name, size and color "+PDPEmailButtonfont +PDPEmailButtonsize +hexPDPEmailButton_colors +"Expected font name, size and color is "+PDPEmailButton_font +PDPEmailButton_size +PDPEmailButton_color);
				fail("PDP EmailButton font, font size, font color is not as per the creative",log);
			}
			
			
			//OverviewLinkText
			String PDPOverviewLinkText_font = Title_Values[2];
			String PDPOverviewLinkText_size = Size_Values[1];
			String PDPOverviewLinkText_color = Color_Values[3];
			
			String PDPOverviewLinkTextfont = OverviewLinkText.getCssValue("font-family");
			String PDPOverviewLinkTextsize = OverviewLinkText.getCssValue("font-size");
			//System.out.println(""+PDPOverviewLinkTextfont);
			//System.out.println(""+PDPOverviewLinkTextsize);
			String PDPOverviewLinkText_size_color = OverviewLinkText.getCssValue("color");
			Color PDPOverviewLinkText_sizecolor = Color.fromString(PDPOverviewLinkText_size_color);
			String hexPDPOverviewLinkText_sizecolors = PDPOverviewLinkText_sizecolor.asHex();
			//System.out.println(""+hexPDPOverviewLinkText_sizecolors);
			
			if((PDPOverviewLinkTextfont.contains(PDPOverviewLinkText_font)) && (PDPOverviewLinkTextsize.equals(PDPOverviewLinkText_size)) && (hexPDPOverviewLinkText_sizecolors.equals(PDPOverviewLinkText_color)))
			{
				log.add("The PDP OverviewLink Text font, size and color is "+"Current font name, size and color "+PDPOverviewLinkTextfont +PDPOverviewLinkTextsize +hexPDPOverviewLinkText_sizecolors +"Expected font name, size and color is "+PDPOverviewLinkText_font +PDPOverviewLinkText_size +PDPOverviewLinkText_color);
				pass("PDP OverviewLink font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The PDP OverviewLink Text font, size and color is "+"Current font name, size and color "+PDPOverviewLinkTextfont +PDPOverviewLinkTextsize +hexPDPOverviewLinkText_sizecolors +"Expected font name, size and color is "+PDPOverviewLinkText_font +PDPOverviewLinkText_size +PDPOverviewLinkText_color);
				fail("PDP OverviewLink font, font size, font color is not as per the creative",log);
			}
			
			
			//CustomerBoughtThisAlsoBoughtProductName
			
			String PDPCusBoughtPrdName_font = Title_Values[2];
			String PDPCusBoughtPrdName_size = Size_Values[3];
			String PDPCusBoughtPrdName_color = Color_Values[4];
			//String PDPCusBoughtPrdName_lineheight = "20px";
			
			String PDPCusBoughtPrdNamefont = CustomerBoughtThisAlsoBoughtProductName.getCssValue("font-family");
			String PDPCusBoughtPrdNamesize = CustomerBoughtThisAlsoBoughtProductName.getCssValue("font-size");
			//String PDPCusBoughtPrdNamelineheight = CustomerBoughtThisAlsoBoughtProductName.getCssValue("line-height");
			//System.out.println(""+PDPCusBoughtPrdNamefont);
			//System.out.println(""+PDPCusBoughtPrdNamesize);
			//System.out.println(""+PDPCusBoughtPrdNamelineheight);
			String PDPCusBoughtPrdName_size_color = CustomerBoughtThisAlsoBoughtProductName.getCssValue("color");
			Color PDPCusBoughtPrdName_sizecolor = Color.fromString(PDPCusBoughtPrdName_size_color);
			String hexPDPCusBoughtPrdName_sizecolors = PDPCusBoughtPrdName_sizecolor.asHex();
			//System.out.println(""+hexPDPCusBoughtPrdName_sizecolors);
			
			if((PDPCusBoughtPrdNamefont.contains(PDPCusBoughtPrdName_font)) && (PDPCusBoughtPrdNamesize.equals(PDPCusBoughtPrdName_size)) && (hexPDPCusBoughtPrdName_sizecolors.equals(PDPCusBoughtPrdName_color)))
			{
				log.add("The PDP Customer bought Product Name Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtPrdNamefont +PDPCusBoughtPrdNamesize +hexPDPCusBoughtPrdName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtPrdName_font +PDPCusBoughtPrdName_size +PDPCusBoughtPrdName_color);
				pass("PDP Customer bought Product Name font, font size, font color is as per the creative",log);
				/*if(PDPCusBoughtPrdNamelineheight.equals(PDPCusBoughtPrdName_lineheight))
				{
					log.add("The PDP Customer bought Product Name Text Line Height is "+"Current "+PDPCusBoughtPrdNamelineheight +"Expected "+PDPCusBoughtPrdName_lineheight);
					pass("PDP Customer bought Product Name Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP Customer bought Product Name Text Line Height is "+"Current "+PDPCusBoughtPrdNamelineheight +"Expected "+PDPCusBoughtPrdName_lineheight);
					fail("PDP Customer bought Product Name Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Customer bought Product Name Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtPrdNamefont +PDPCusBoughtPrdNamesize +hexPDPCusBoughtPrdName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtPrdName_font +PDPCusBoughtPrdName_size +PDPCusBoughtPrdName_color);
				fail("PDP Customer bought Product Name font, font size, font color is not as per the creative",log);
			}
			
			
			//CustomerBoughtThisAlsoBoughtAuthorName
			
			String PDPCusBoughtAuthorName_font = Title_Values[1];
			String PDPCusBoughtAuthorName_size = Size_Values[1];
			String PDPCusBoughtAuthorName_color = Color_Values[5];
			//String PDPCusBoughtAuthorName_lineheight = "20px";
			
			String PDPCusBoughtAuthorNamefont = CustomerBoughtThisAlsoBoughtAuthorName.getCssValue("font-family");
			String PDPCusBoughtAuthorNamesize = CustomerBoughtThisAlsoBoughtAuthorName.getCssValue("font-size");
			//String PDPCusBoughtAuthorNamelineheight = CustomerBoughtThisAlsoBoughtAuthorName.getCssValue("line-height");
			//System.out.println(""+PDPCusBoughtAuthorNamefont);
			//System.out.println(""+PDPCusBoughtAuthorNamesize);
			//System.out.println(""+PDPCusBoughtAuthorNamelineheight);
			String PDPCusBoughtAuthorName_size_color = CustomerBoughtThisAlsoBoughtAuthorName.getCssValue("color");
			Color PDPCusBoughtAuthorName_sizecolor = Color.fromString(PDPCusBoughtAuthorName_size_color);
			String hexPDPCusBoughtAuthorName_sizecolors = PDPCusBoughtAuthorName_sizecolor.asHex();
			//System.out.println(""+hexPDPCusBoughtAuthorName_sizecolors);
			
			if((PDPCusBoughtAuthorNamefont.contains(PDPCusBoughtAuthorName_font)) && (PDPCusBoughtAuthorNamesize.equals(PDPCusBoughtAuthorName_size)) && (hexPDPCusBoughtAuthorName_sizecolors.equals(PDPCusBoughtAuthorName_color)))
			{
				log.add("The PDP Customer bought Author Name Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtAuthorNamefont +PDPCusBoughtAuthorNamesize +hexPDPCusBoughtAuthorName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtAuthorName_font +PDPCusBoughtAuthorName_size +PDPCusBoughtAuthorName_color);
				pass("PDP Customer bought Author Name font, font size, font color is as per the creative",log);
				/*if(PDPCusBoughtAuthorNamelineheight.equals(PDPCusBoughtAuthorName_lineheight))
				{
					log.add("The PDP Customer bought Author Name Text Line Height is "+"Current "+PDPCusBoughtAuthorNamelineheight +"Expected "+PDPCusBoughtAuthorName_lineheight);
					pass("PDP Customer bought Author Name Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP Customer bought Author Name Text Line Height is "+"Current "+PDPCusBoughtAuthorNamelineheight +"Expected "+PDPCusBoughtAuthorName_lineheight);
					fail("PDP Customer bought Author Name Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Customer bought Author Name Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtAuthorNamefont +PDPCusBoughtAuthorNamesize +hexPDPCusBoughtAuthorName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtAuthorName_font +PDPCusBoughtAuthorName_size +PDPCusBoughtAuthorName_color);
				fail("PDP Customer bought Author Name font, font size, font color is not as per the creative",log);
			}
			
			
			//CustomerReviewsTitle
			
			String PDPCustomerReviewTitle_font = Title_Values[0];
			String PDPCustomerReviewTitle_size = Size_Values[2];
			String PDPCustomerReviewTitle_color = Color_Values[0];
			//String PDPCustomerReviewTitle_lineheight = "27px";
			
			String PDPCustomerReviewTitlefont = CustomerReviewsTitle.getCssValue("font-family");
			String PDPCustomerReviewTitlesize = CustomerReviewsTitle.getCssValue("font-size");
			//String PDPCustomerReviewTitlelineheight = CustomerReviewsTitle.getCssValue("line-height");
			//System.out.println(""+PDPCustomerReviewTitlefont);
			//System.out.println(""+PDPCustomerReviewTitlesize);
			//System.out.println(""+PDPCustomerReviewTitlelineheight);
			String PDPCustomerReviewTitle_size_color = CustomerReviewsTitle.getCssValue("color");
			Color PDPCustomerReviewTitle_sizecolor = Color.fromString(PDPCustomerReviewTitle_size_color);
			String hexPDPCustomerReviewTitlee_sizecolors = PDPCustomerReviewTitle_sizecolor.asHex();
			//System.out.println(""+hexPDPCustomerReviewTitlee_sizecolors);
			
			if((PDPCustomerReviewTitlefont.contains(PDPCustomerReviewTitle_font)) && (PDPCustomerReviewTitlesize.equals(PDPCustomerReviewTitle_size)) && (hexPDPCustomerReviewTitlee_sizecolors.equals(PDPCustomerReviewTitle_color)))
			{
				log.add("The PDP CustomerReviewsTitle Text font, size and color is "+"Current font name, size and color "+PDPCustomerReviewTitlefont +PDPCustomerReviewTitlesize +hexPDPCustomerReviewTitlee_sizecolors +"Expected font name, size and color is "+PDPCustomerReviewTitle_font +PDPCustomerReviewTitle_size +PDPCustomerReviewTitle_color);
				pass("PDP CustomerReviewsTitlee font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP CustomerReviewsTitle Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtAuthorNamefont +PDPCusBoughtAuthorNamesize +hexPDPCusBoughtAuthorName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtAuthorName_font +PDPCusBoughtAuthorName_size +PDPCusBoughtAuthorName_color);
				fail("PDP CustomerReviewsTitle font, font size, font color is not as per the creative",log);
			}
			
			
			//CustomerReviewsText
			
			String PDPCustomerReviewText_font = Title_Values[0];
			String PDPCustomerReviewText_size = Size_Values[1];
			String PDPCustomerReviewText_color = Color_Values[0];
			//String PDPCustomerReviewText_lineheight = "22px";
			
			String PDPCustomerReviewTextfont = CustomerReviewsTitle.getCssValue("font-family");
			String PDPCustomerReviewTextsize = CustomerReviewsTitle.getCssValue("font-size");
			//String PDPCustomerReviewTextlineheight = CustomerReviewsTitle.getCssValue("line-height");
			//System.out.println(""+PDPCustomerReviewTextfont);
			//System.out.println(""+PDPCustomerReviewTextsize);
			//System.out.println(""+PDPCustomerReviewTextlineheight);
			String PDPCustomerReviewText_size_color = CustomerReviewsTitle.getCssValue("color");
			Color PDPCustomerReviewText_sizecolor = Color.fromString(PDPCustomerReviewText_size_color);
			String hexPDPCustomerReviewText_sizecolors = PDPCustomerReviewText_sizecolor.asHex();
			//System.out.println(""+hexPDPCustomerReviewText_sizecolors);
			
			if((PDPCustomerReviewTextfont.contains(PDPCustomerReviewText_font)) && (PDPCustomerReviewTextsize.equals(PDPCustomerReviewText_size)) && (hexPDPCustomerReviewText_sizecolors.equals(PDPCustomerReviewText_color)))
			{
				log.add("The PDP CustomerReviewsTitle Text font, size and color is "+"Current font name, size and color "+PDPCustomerReviewTextfont +PDPCustomerReviewTextsize +hexPDPCustomerReviewText_sizecolors +"Expected font name, size and color is "+PDPCustomerReviewText_font +PDPCustomerReviewText_size +PDPCustomerReviewText_color);
				pass("PDP CustomerReviewsTitle font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP CustomerReviewsTitle Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtAuthorNamefont +PDPCusBoughtAuthorNamesize +hexPDPCusBoughtAuthorName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtAuthorName_font +PDPCusBoughtAuthorName_size +PDPCusBoughtAuthorName_color);
				fail("PDP CustomerReviewsTitle font, font size, font color is not as per the creative",log);
			}
			
			
			//EditorialReviewsPublisherText
			String PDPEditorialReviewsPublisherText_font = Title_Values[2];
			String PDPEditorialReviewsPublisherText_size = Size_Values[3];
			String PDPEditorialReviewsPublisherText_color = Color_Values[6];
			//String PDPEditorialReviewsPublisherText_lineheight = "22px";
			
			String PDPEditorialReviewsPublisherTextfont = EditorialReviewsPublisherText.getCssValue("font-family");
			String PDPEditorialReviewsPublisherTextsize = EditorialReviewsPublisherText.getCssValue("font-size");
			//String PDPEditorialReviewsPublisherTextlineheight = EditorialReviewsPublisherText.getCssValue("line-height");
			//System.out.println(""+PDPEditorialReviewsPublisherTextfont);
			//System.out.println(""+PDPEditorialReviewsPublisherTextsize);
			//System.out.println(""+PDPEditorialReviewsPublisherTextlineheight);
			String PDPEditorialReviewsPublisherText_size_color = EditorialReviewsPublisherText.getCssValue("color");
			Color PDPEditorialReviewsPublisherText_sizecolor = Color.fromString(PDPEditorialReviewsPublisherText_size_color);
			String hexPDPEditorialReviewsPublisherText_sizecolors = PDPEditorialReviewsPublisherText_sizecolor.asHex();
			//System.out.println(""+hexPDPEditorialReviewsPublisherText_sizecolors);
			
			if((PDPEditorialReviewsPublisherTextfont.contains(PDPEditorialReviewsPublisherText_font)) && (PDPEditorialReviewsPublisherTextsize.equals(PDPEditorialReviewsPublisherText_size)) && (hexPDPEditorialReviewsPublisherText_sizecolors.equals(PDPEditorialReviewsPublisherText_color)))
			{
				log.add("The PDP Editorial Review Publisher Text font, size and color is "+"Current font name, size and color "+PDPEditorialReviewsPublisherTextfont +PDPEditorialReviewsPublisherTextsize +hexPDPEditorialReviewsPublisherText_sizecolors +"Expected font name, size and color is "+PDPEditorialReviewsPublisherText_font +PDPEditorialReviewsPublisherText_size +PDPEditorialReviewsPublisherText_color);
				pass("PDP Editorial Review Publisher font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP Editorial Review Publisher Text font, size and color is "+"Current font name, size and color "+PDPEditorialReviewsPublisherTextfont +PDPEditorialReviewsPublisherTextsize +hexPDPEditorialReviewsPublisherText_sizecolors +"Expected font name, size and color is "+PDPEditorialReviewsPublisherText_font +PDPEditorialReviewsPublisherText_size +PDPEditorialReviewsPublisherText_color);
				fail("PDP Editorial Review Publishere font, font size, font color is not as per the creative",log);
			}
			
			
			
			//ProductDetails_Text
			
			String PDPProductDetailsText_font = Title_Values[1];
			String PDPProductDetailsText_size = Size_Values[3];
			String PDPProductDetailsText_color = Color_Values[6];
			//String PDPProductDetailsText_lineheight = "22px";
			
			String PDPProductDetailsTextfont = ProductDetails_Text.getCssValue("font-family");
			String PDPProductDetailsTextsize = ProductDetails_Text.getCssValue("font-size");
			//String PDPProductDetailsTextlineheight = ProductDetails_Text.getCssValue("line-height");
			//System.out.println(""+PDPProductDetailsTextfont);
			//System.out.println(""+PDPProductDetailsTextsize);
			//System.out.println(""+PDPProductDetailsTextlineheight);
			String PDPProductDetailsText_size_color = ProductDetails_Text.getCssValue("color");
			Color PDPProductDetailsText_sizecolor = Color.fromString(PDPProductDetailsText_size_color);
			String hexPDPProductDetailsText_sizecolors = PDPProductDetailsText_sizecolor.asHex();
			//System.out.println(""+hexPDPProductDetailsText_sizecolors);
			
			if((PDPProductDetailsTextfont.contains(PDPProductDetailsText_font)) && (PDPProductDetailsTextsize.equals(PDPProductDetailsText_size)) && (hexPDPProductDetailsText_sizecolors.equals(PDPProductDetailsText_color)))
			{
				log.add("The PDP Product details Text font, size and color is "+"Current font name, size and color "+PDPProductDetailsTextfont +PDPProductDetailsTextsize +hexPDPProductDetailsText_sizecolors +"Expected font name, size and color is "+PDPProductDetailsText_font +PDPProductDetailsText_size +PDPProductDetailsText_color);
				pass("PDP Product details font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP Product details Text font, size and color is "+"Current font name, size and color "+PDPProductDetailsTextfont +PDPProductDetailsTextsize +hexPDPProductDetailsText_sizecolors +"Expected font name, size and color is "+PDPProductDetailsText_font +PDPProductDetailsText_size +PDPProductDetailsText_color);
				fail("PDP Product details font, font size, font color is not as per the creative",log);
			}
			
			
			//RelatedSubjects_Text
			
			String PDPRelatedSubjectsText_font = Title_Values[1];
			String PDPRelatedSubjectsText_size = Size_Values[4];
			String PDPRelatedSubjectsText_color = Color_Values[1];
			//String PDPRelatedSubjectsText_lineheight = "22px";
			
			String PDPRelatedSubjectsTextfont = RelatedSubjects_Text.getCssValue("font-family");
			String PDPRelatedSubjectsTextsize = RelatedSubjects_Text.getCssValue("font-size");
			//String PDPRelatedSubjectsTextlineheight = RelatedSubjects_Text.getCssValue("line-height");
			//System.out.println(""+PDPRelatedSubjectsTextfont);
			//System.out.println(""+PDPRelatedSubjectsTextsize);
			//System.out.println(""+PDPRelatedSubjectsTextlineheight);
			String PDPRelatedSubjectsText_size_color = RelatedSubjects_Text.getCssValue("color");
			Color PDPRelatedSubjectsText_sizecolor = Color.fromString(PDPRelatedSubjectsText_size_color);
			String hexPDPRelatedSubjectsText_sizecolors = PDPRelatedSubjectsText_sizecolor.asHex();
			//System.out.println(""+hexPDPRelatedSubjectsText_sizecolors);
			
			if((PDPRelatedSubjectsTextfont.contains(PDPRelatedSubjectsText_font)) && (PDPRelatedSubjectsTextsize.equals(PDPRelatedSubjectsText_size)) && (hexPDPRelatedSubjectsText_sizecolors.equals(PDPRelatedSubjectsText_color)))
			{
				log.add("The PDP Related Subject Text font, size and color is "+"Current font name, size and color "+PDPRelatedSubjectsTextfont +PDPRelatedSubjectsTextsize +hexPDPRelatedSubjectsText_sizecolors +"Expected font name, size and color is "+PDPRelatedSubjectsText_font +PDPRelatedSubjectsText_size +PDPRelatedSubjectsText_color);
				pass("PDP Related Subject font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP Related Subject Text font, size and color is "+"Current font name, size and color "+PDPRelatedSubjectsTextfont +PDPRelatedSubjectsTextsize +hexPDPRelatedSubjectsText_sizecolors +"Expected font name, size and color is "+PDPRelatedSubjectsText_font +PDPRelatedSubjectsText_size +PDPRelatedSubjectsText_color);
				fail("PDP Related Subjects font, font size, font color is not as per the creative",log);
			}
			BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
			WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			logo.click();
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA346"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA-347 Verify that Header should be displayed in the product detail page*/
	public void BNIA347()
	{
		ChildCreation("BNIA-347 Verify that Header should be displayed in the product detail page");
		try
	 	{
			Thread.sleep(2500);
			int ctr=1;
			//boolean facets1 = true;
			/*if(facets1!=true)*/
				
			do
			{
				BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
				WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
				logo.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "BrowseTile");
		 	 	WebElement browse_tile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
		 	 	browse_tile.click();
		 	 	BNBasicCommonMethods.waitforElement(wait, obj, "browsepage");
		 	 	WebElement browsepg = BNBasicCommonMethods.findElement(driver, obj, "browsepage");
		 	 	wait.until(ExpectedConditions.visibilityOf(browsepg));
		 	 	List<WebElement> browse_pg = driver.findElements(By.xpath("//*[contains(@class, 'hotSpotOverlay')]"));
		 	 	Random ran = new Random();
		   	 	int sel = ran.nextInt(browse_pg.size());
		   	 	WebElement menu = driver.findElement(By.xpath("(//*[contains(@class, 'hotSpotOverlay')])["+sel+"]"));
		   	    JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView(true);",menu);
				Thread.sleep(1000);
		   	 	menu.click();
		   	 	Thread.sleep(2000);
		   	 	
		   	 	try
		   	 	{
			   	 	Thread.sleep(2000);
			   	 	boolean facets = false;
			   	 	facets = BNBasicCommonMethods.findElement(driver, obj, "PLP_emptyfactes").isDisplayed();
				   	 	if(facets)
				   	 	{
				   	 		log.add("PLP page is shown");
				   	 		ctr=2;
				   	 	Thread.sleep(2500);
				        List<WebElement> PLP = driver.findElements(By.xpath("//*[@class = 'skMob_productImg showimg scTrack scLink']"));
						Random r = new Random();
						int new1 = r.nextInt(PLP.size());
						Thread.sleep(2500);
						WebElement plp_prd = driver.findElement(By.xpath("(//*[@class = 'skMob_productImg showimg scTrack scLink' ])["+new1+"]"));
						plp_prd.click();
						Thread.sleep(1000);
						log.add("PDP page is shown");
						WebElement head = BNBasicCommonMethods.findElement(driver, obj, "Header");
						boolean header = BNBasicCommonMethods.isElementPresent(head);
						if(header)
						{
							pass("Header is displayed in the product detail page",log);
						}
						else
						{
							fail("Header is not displayed in the product detail page",log);
						}
				   	 		
				   	 	}
				   	 	else
				   	 	{
				   	 		log.add("PLP facets is not found");
				   	 		
				   	 	}
		   	 	}
		   	 	catch(Exception e)
		   	 	{
		   	 	String url = driver.getCurrentUrl();
			   	 	if(url.contains("pdp?"))
			   	 	{
			   	 		WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			   	 	    BNLogo.click();
			   	 	    Thread.sleep(1000);
			   	 	}
		   	 	}
		   	 	
			}while(ctr <  2);
	 	}	
         catch(Exception e)
		{
        	e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA349 - Verify that barnes and noble logo should be shown in the header and aligned center in product detail page*/
	public void BNIA349()
	{
		ChildCreation("BNIA349 - Verify that barnes and noble logo should be shown in the header and aligned center in product detail page");
		try
		{
			WebElement BNlogo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			BNlogo.getSize();
			//System.out.println(" BNLogo Width and Height is "+BNlogo.getSize());
			//System.out.println("Location points is "+BNlogo.getLocation());
			String Loc = BNlogo.getLocation().toString();
			String LogolocationWidthHeight = "(401, 23)";
			boolean logo = BNBasicCommonMethods.isElementPresent(BNlogo);
			if(LogolocationWidthHeight.equals(Loc) && (logo))
			 {
				pass("barnes and noble logo shown in the header and aligned center in product detail page");
	    	 }
			else
			{
				fail("barnes and noble logo is not shown in the header and aligned center in product detail page");
			}
		
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA350 - Verify that on tapping barnes and noble logo, it should navigate to home page */
	public void BNIA350() throws Exception
	{
		ChildCreation("BNIA350 - Verify that on tapping barnes and noble logo, it should navigate to home page ");
		try
		{
			WebElement BNlogo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			BNlogo.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Homepage");
			WebElement home = BNBasicCommonMethods.findElement(driver, obj, "Homepage");
			boolean home_page = BNBasicCommonMethods.isElementPresent(home);
			if(home_page)
			{
				pass("On tapping barnes and noble logo, it navigates to home page");
			}
			else
			{
				fail("On tapping barnes and noble logo, it does not navigates to home page");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	
	public void SearchNavigatepdp() throws Exception
	{
		/*WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
		logo.click();*/
		BNBasicCommonMethods.waitforElement(wait, obj, "Searchtile");
		BNBasicCommonMethods.findElement(driver, obj, "Searchtile").click();
		BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
		WebElement searchbox1 = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
		searchbox1.sendKeys("9780316407021");
		searchbox1.sendKeys(Keys.ENTER);
	}
	
	/*BNIA-351 Verify that Back Arrow icon should be diplayed at the header in the product detail page*/
	public void BNIA351()
	{
		ChildCreation("BNIA-351 Verify that Back Arrow icon should be diplayed at the header in the product detail page");
		
		try
	 	{
			int ctr=1;
			//boolean facets1 = true;
			/*if(facets1!=true)*/
				
			do
			{
				BNBasicCommonMethods.waitforElement(wait, obj, "BrowseTile");
		 	 	WebElement browse_tile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
		 	 	browse_tile.click();
		 	 	BNBasicCommonMethods.waitforElement(wait, obj, "browsepage");
		 	 	WebElement browsepg = BNBasicCommonMethods.findElement(driver, obj, "browsepage");
		 	 	wait.until(ExpectedConditions.visibilityOf(browsepg));
		 	 	List<WebElement> browse_pg = driver.findElements(By.xpath("//*[contains(@class, 'hotSpotOverlay')]"));
		 	 	Random ran = new Random();
		   	 	int sel = ran.nextInt(browse_pg.size());
		   	 	WebElement menu = driver.findElement(By.xpath("(//*[contains(@class, 'hotSpotOverlay')])["+sel+"]"));
		   	    JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView(true);",menu);
				Thread.sleep(1000);
		   	 	menu.click();
		   	 	Thread.sleep(2000);
		   	 	
		   	 	try
		   	 	{
			   	 	Thread.sleep(2000);
			   	 	boolean facets = false;
			   	 	facets = BNBasicCommonMethods.findElement(driver, obj, "PLP_emptyfactes").isDisplayed();
				   	 	if(facets)
				   	 	{
				   	 		log.add("PLP page is shown");
				   	 		ctr=2;
					   	 	Thread.sleep(2500);
					        List<WebElement> PLP = driver.findElements(By.xpath("//*[@class = 'skMob_productImg showimg scTrack scLink']"));
							Random r = new Random();
							int new1 = r.nextInt(PLP.size());
							Thread.sleep(2500);
							WebElement plp_prd = driver.findElement(By.xpath("(//*[@class = 'skMob_productImg showimg scTrack scLink' ])["+new1+"]"));
							plp_prd.click();
							Thread.sleep(1000);
							log.add("PDP page is shown");
							WebElement head = BNBasicCommonMethods.findElement(driver, obj, "Header");
							boolean header = BNBasicCommonMethods.isElementPresent(head);
							if(header)
							{
								pass("Header is displayed in the product detail page",log);
							}
							else
							{
								fail("Header is not displayed in the product detail page",log);
							}
					   	 		
					   	 	}
					   	 	else
				   	 	{
				   	 		log.add("PLP facets is not found");
				   	 		
				   	 	}
		   	 	}
		   	 	catch(Exception e)
		   	 	{
		   	 	String url = driver.getCurrentUrl();
			   	 	if(url.contains("pdp?"))
			   	 	{
			   	 		WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			   	 	    BNLogo.click();
			   	 	    Thread.sleep(1000);
			   	 	}
		   	 	}
		   	 	
			}while(ctr <  2);
			WebElement bckarrow = BNBasicCommonMethods.findElement(driver, obj, "backarrow");
			boolean barrow = BNBasicCommonMethods.isElementPresent(bckarrow);
			if(barrow)
			{
				pass("Back Arrow icon is diplayed at the header in the product detail page",log);
			}
			else
			{
				fail("Back Arrow icon is not diplayed at the header in the product detail page",log);
			}
	 	}	
         catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA351\n"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA352 - Verify that on tapping barnes and noble logo at the header, it should not navigate to any other page except home page.*/
	public void BNIA352()
	{
		ChildCreation("BNIA352 - Verify that on tapping barnes and noble logo at the header, it should not navigate to any other page except home page.");
		try
		{
			Thread.sleep(2000);
			WebElement BNlogo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			BNlogo.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Homepage");
			WebElement home = BNBasicCommonMethods.findElement(driver, obj, "Homepage");
			boolean home_page = BNBasicCommonMethods.isElementPresent(home);
			if(home_page)
			{
				pass("On tapping barnes and noble logo at the header, it does not navigates to any other page except home page.");
			}
			else
			{
				fail("On tapping barnes and noble logo at the header, it navigates to any other page except home page.");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA353 - Verify that  on selecting the back arrow button at the header, it should navigates to previous page*/
	public void BNIA353() throws Exception
	{
		ChildCreation("BNIA353 - Verify that  on selecting the back arrow button at the header, it should navigates to previous page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "backarrow");
			WebElement bckarrow = BNBasicCommonMethods.findElement(driver, obj, "backarrow");
			bckarrow.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "PLP");
			WebElement plp = BNBasicCommonMethods.findElement(driver, obj, "PLP");
			boolean bool = false;
			bool = plp.isDisplayed();
			if(bool)
			{
				pass("On selecting the back arrow button at the header, it navigates to previous page");
			}
			else
			{
				fail("On selecting the back arrow button at the header, it does not navigates to previous page");
			}
		}
		catch(Exception e)
		{
			e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
		
	}
	
	/*BNIA354 - Verify that hamburger icon should be displayed next to back arrow button in the product detail page*/
	public void BNIA354() throws Exception
	{
		ChildCreation("BNIA354 - Verify that hamburger icon should be displayed next to back arrow button in the product detail page");
		try
		{  
			/*BNBasicCommonMethods.findElement(driver, obj, "logoicon").click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Searchtile");
			BNBasicCommonMethods.findElement(driver, obj, "Searchtile").click();
			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			WebElement searchbox1 = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			//Thread.sleep(2000);
			
			searchbox1.sendKeys("9780316407021");*/
			WebElement pancake = BNBasicCommonMethods.findElement(driver, obj, "PancakeMenu");
			boolean pan = BNBasicCommonMethods.isElementPresent(pancake);
			if(pan)
			{
				pass("Hamburger icon is displayed next to back arrow button in the product detail page");
			}
			else
			{
				fail("Hamburger icon is not displayed next to back arrow button in the product detail page");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
		
	}
	
	/*BNIA355 - Verify that  while tapping hamburger menu at the header,the Pancake Flyover should be displayed*/
	public void BNIA355() throws Exception
	{
		ChildCreation("BNIA355 - Verify that  while tapping hamburger menu at the header,the Pancake Flyover should be displayed");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PancakeMenu");
			WebElement pancake = BNBasicCommonMethods.findElement(driver, obj, "PancakeMenu");
			pancake.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Pancakeflyover");
			WebElement panflyover = BNBasicCommonMethods.findElement(driver, obj, "Pancakeflyover");
			boolean flyover = BNBasicCommonMethods.isElementPresent(panflyover);
			if(flyover)
			{
				pass("while tapping hamburger menu at the header,the Pancake Flyover gets displayed");
			}
			else
			{
				fail("while tapping hamburger menu at the header,the Pancake Flyover does not gets displayed");
			}
			Thread.sleep(1000);
			BNBasicCommonMethods.findElement(driver, obj, "Mask").click();
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA356 - Verify that Search icon should be displayed at the header in the product detail page*/
	public void BNIA356() throws Exception
	{
		ChildCreation("BNIA356 - Verify that Search icon should be displayed at the header in the product detail page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "BrowseTile");
			WebElement menu = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
			menu.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "SearchIcon");
			WebElement searchicon = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
			boolean ser_icon = BNBasicCommonMethods.isElementPresent(searchicon);
			if(ser_icon)
			{
				pass("Search icon is displayed at the header in the product detail page");
			}
			else
			{
				fail("Search icon is not displayed at the header in the product detail page");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
		
	}
	
	/*BNIA357 - Verify that while Selecting search icon at the header, it should navigate to search page */
	public void BNIA357() throws Exception
	{
		ChildCreation("BNIA357 - Verify that while Selecting search icon at the header, it should navigate to search page ");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "SearchIcon");
			WebElement searchicon = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
			searchicon.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			WebElement searchbox1 = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
			boolean searchbx = BNBasicCommonMethods.isElementPresent(searchbox1);
			WebElement scanbtn = BNBasicCommonMethods.findElement(driver, obj, "scanbutton");
			boolean scnbutton = BNBasicCommonMethods.isElementPresent(scanbtn);
	 	 	if(searchbx && scnbutton)
	 	 	{
	 	 		pass(" while Selecting search icon at the header, it navigates to search page ");
	 	 	}
	 	 	else
	 	 	{
	 	 		fail(" while Selecting search icon at the header, it does not navigates to search page ");
	 	 	}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA359 - Verify that bag icon should be displayed at the header in the product detail page*/
	public void BNIA359() throws Exception
	{
		ChildCreation("BNIA359 - Verify that bag icon should be displayed at the header in the product detail page");
		try
		{
			int ctr=1;
			do
			{
				if(!TFprdtId.equals(""))
				{
					String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+TFprdtId;
					driver.get(PDPURL);
					try
					{
						ctr=6;
						BNBasicCommonMethods.waitforElement(wait, obj, "ShopIcon");
						BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
						WebElement shop_icon = BNBasicCommonMethods.findElement(driver, obj, "ShopIcon");
						boolean shop = BNBasicCommonMethods.isElementPresent(shop_icon);
						if(shop)
						{
							pass("bag icon is displayed at the header in the product detail page ");
						}
						else
						{
							fail("bag icon is not displayed at the header in the product detail page ");
						}
					}
					catch(Exception e)
					{
						System.out.println("Shop icon & ATB button is not shown");
						ctr++;
					}
				}
				else
				{
					System.out.println("TF Product ID is not shown");
				}
			}while(ctr<=5);
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA359\n"+e.getMessage());
 			exception(e.getMessage());
		}
		
	}
	
	public void logoclick() throws Exception
	{
		BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
		WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
		logo.click();
	}
	
	/*public boolean PDPTTLogic(ArrayList<String> eanNo) throws Exception
	{
		boolean inventory = false;
		boolean browseOK= browse();
		if(browseOK==true)
		{
			 ArrayList<String> productId = new ArrayList<>();
			 for(int i=1;i<=productId.size();i++)
			  {
				  	String Stream_OnlineInventoryURL = "http://bninstore.skavaone.com/skavastream/core/v5/bandnkiosk/product/"+productId.get(i-1)+"?campaignId=1";
					String Stream_StoreInventoryURL = "http://bninstore.skavaone.com/skavastream/xact/v5/bandnkiosk/getinventory?campaignId=1&storeid=2932&skuid="+productId.get(i-1);
					//String skavaStreamURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam=9780545791328";
					String urlResp = IOUtils.toString(new URI(Stream_OnlineInventoryURL));
					String urlResp1 = IOUtils.toString(new URI(Stream_StoreInventoryURL));
					JSONObject urlObjProp = new JSONObject(urlResp);
					JSONObject prop = urlObjProp.getJSONObject("properties").getJSONObject("buyinfo");
					JSONArray Onlineavailability = prop.getJSONArray("availability");
					System.out.println(Onlineavailability);
					String str = Onlineavailability.toString();
					JSONObject urlObjProp1 = new JSONObject(urlResp1);
					JSONObject prop1 = urlObjProp1.getJSONObject("properties");
					System.out.println(prop1.length());
					JSONArray storeInfo  = prop1.getJSONArray("storeinfo");
					String invent = storeInfo.getJSONObject(0).getString("inventory");
					System.out.println(Onlineavailability);
					System.out.println(invent);
					boolean StroreInventory  = false;
					if(invent.equals("y"))
					{
						StroreInventory = true;
					}
					String [] stringary = str.split(":");
					String str1 = stringary[stringary.length-1];
					boolean Onlineinventory  = false;
					if(str1.contains("true"))
					{
						Onlineinventory = true;
					}
					
					if(StroreInventory && Onlineinventory)
					{
						
					}
					else
					{
						
					}
			}
		}
	return inventory;
		
	}*/
	
	public boolean browse()
	{
		int i=1;
		boolean facet = false;
		do
		{
			try
			{
				try
				{
					boolean facets = BNBasicCommonMethods.findElement(driver, obj, "PLP_facets").isDisplayed();
					if(facets==true)
					{
						String HomeUrl = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/home";
						driver.get(HomeUrl);
						/*BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
						Thread.sleep(1000);
						WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
						JavascriptExecutor executor = (JavascriptExecutor)driver;
						executor.executeScript("arguments[0].click();", logo);
						//logo.click();
*/						Thread.sleep(1000);
					}
					
					BNBasicCommonMethods.waitforElement(wait, obj, "BrowseTile");
			 	 	WebElement browse_tile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
			 	 	browse_tile.click();
			 	 	BNBasicCommonMethods.waitforElement(wait, obj, "browsepage");
			 	 	WebElement browsepg = BNBasicCommonMethods.findElement(driver, obj, "browsepage");
			 	 	wait.until(ExpectedConditions.visibilityOf(browsepg));
			 	 	Thread.sleep(1000);
			 	 	if(BNBasicCommonMethods.isElementPresent(browsepg))
			 	 	{
			 	 		facet = true;
			 	 		break;
			 	 	}
			 	 	else
			 	 	{
			 	 		facet = false;
			 	 		i++;
			 	 		continue;
			 	 	}
				}
				catch(Exception e)
				{
					String HomeUrl = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/home";
					driver.get(HomeUrl);
					BNBasicCommonMethods.waitforElement(wait, obj, "BrowseTile");
			 	 	WebElement browse_tile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
			 	 	browse_tile.click();
			 	 	BNBasicCommonMethods.waitforElement(wait, obj, "browsepage");
			 	 	WebElement browsepg = BNBasicCommonMethods.findElement(driver, obj, "browsepage");
			 	 	wait.until(ExpectedConditions.visibilityOf(browsepg));
			 	 	Thread.sleep(1000);
			 	 	if(BNBasicCommonMethods.isElementPresent(browsepg))
			 	 	{
			 	 		facet = true;
			 	 		break;
			 	 	}
			 	 	else
			 	 	{
			 	 		facet = false;
			 	 		i++;
			 	 		continue;
			 	 	}
				}
			}
		 	catch(Exception e)
			{
		 		e.getMessage();
		 		System.out.println("Something went wrong"+e.getMessage());
		 		exception(e.getMessage());
			}
		}while((facet!=true)||(i<5));
		return facet;
	}
	public void PDPATB() throws Exception
	{
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
			WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
			boolean atb = BNBasicCommonMethods.isElementPresent(ATB);
			if(atb == true)
			{
				
				pass("ATB button is shown");
			}
			else
			{
				fail("ATb button is not shown");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
		
	}
	
	public void PDPAllformats()
	{
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "Allformats");
			WebElement allformats = BNBasicCommonMethods.findElement(driver, obj, "Allformats");
			boolean all_formats = BNBasicCommonMethods.isElementPresent(allformats);
			if(all_formats == true)
			{
				
				pass("All Formats dropdown is shown");
			}
			/*else if(check_nearby == true)
			{
				pass("Check Near By Stores for Stock button is shown");
				logoclick();
				browse();
			}*/
			else
			{
				fail("All Formats dropdown is not shown");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	public void PDPmarketplace()
	{
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "Marketplace");
			WebElement marketplace = BNBasicCommonMethods.findElement(driver, obj, "Marketplace");
			boolean market_place = BNBasicCommonMethods.isElementPresent(marketplace);
			if(market_place == true)
			{
				
				pass("All Formats dropdown is shown");
			}
			/*else if(check_nearby == true)
			{
				pass("Check Near By Stores for Stock button is shown");
				logoclick();
				browse();
			}*/
			else
			{
				fail("All Formats dropdown is not shown");
			}
	    }
		catch(Exception e)
	     {
			e.getMessage();
			System.out.println("Something went wrong"+e.getMessage());
			exception(e.getMessage());
	     }
	}
	
	public void PDPchecknerbystores()
	{
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "Nearbystore");
			//WebElement checknearby = BNBasicCommonMethods.findElement(driver, obj, "Nearbystore");
			//boolean check_nearby = BNBasicCommonMethods.isElementPresent(checknearby);
		}
		catch(Exception e)
	     {
			e.getMessage();
			System.out.println("Something went wrong"+e.getMessage());
			exception(e.getMessage());
	     }
	}
		/*public void PDPATBClick()
		{
			try
			{
				Thread.sleep(1000);
				BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
				WebElement atbclick = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
				atbclick.click();
			}
			catch(Exception e)
		     {
				e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		     }
		}*/
		
		
	
	/*BNIA360 - Verify that product count should be displayed on Bag icon at the header in the product detail page */
	public void BNIA360()
	{
		ChildCreation("BNIA360 - Verify that product count should be displayed on Bag icon at the header in the product detail page ");
		try
		{
			//WebElement shopcount = BNBasicCommonMethods.findElement(driver, obj, "shopicon_count");
			String script = "return $('#bag_count').text()";
		    	String n = (String) ((JavascriptExecutor) driver).executeScript(script, "shopcount");
		    	String []prdtcnt = n.split(" ");
		    	//System.out.println(prdtcnt[0]);
		    	if(Integer.parseInt(prdtcnt[0]) == 0)
		    	{
		    		log.add("The Bag count is 0, On clicking the ATB Button, the bag count will be shown "+prdtcnt[0]);
		    		BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
		    		WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
		    		int ctr=1;
		    		do
		    		{
		    			Thread.sleep(1000);
		    			ATB.click();
		    			Thread.sleep(1000);
			    		try
			    		{
			    			System.out.println("360");
			    			ctr=4;
			    			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay_ContinueButton");
				    		WebElement continuebtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ContinueButton");
				    		Thread.sleep(1000);
				    		continuebtn.click();
				    		Thread.sleep(500);
				    		//WebElement shopcount1 = BNBasicCommonMethods.findElement(driver, obj, "shopicon_count");
							String script1 = "return $('#bag_count').text()";
						    	String n1 = (String) ((JavascriptExecutor) driver).executeScript(script1, "shopcount1");
						    	String []prdtcnt1 = n1.split(" ");
						    	//System.out.println(prdtcnt1[0]);
						    	log.add("The Bag count is "+prdtcnt[0]);
						    	pass("Product count is displayed on Bag icon at the header in the product detail page",log);
			    		}
			    		catch(Exception e)
			    		{
			    			WebElement ATBErrorAlt = driver.findElement(By.xpath("//*[@id='skMob_ErrorsDiv_id']"));
		  					if(ATBErrorAlt.isDisplayed())
		  					{
		  						WebElement ErroBtn = driver.findElement(By.xpath("//*[@id='skMob_ErrorsOK_id']"));
		  						ErroBtn.click();
		  						ctr++;
		  					}
			    		}
		    		
		    		}while(ctr<=3);
		    	}
		    	else if(!(Integer.parseInt(prdtcnt[0]) == 0))
		    	{
		    		System.out.println("The Product count in the bag is "+prdtcnt[0]);
		    		log.add("The Bag count is "+prdtcnt[0]);
		    		pass("Product count is displayed on Bag icon at the header in the product detail page",log);
		    	}
		    	else
		    	{
		    		fail("Product count is not displayed on Bag icon at the header in the product detail page");
		    	}
			
			/*boolean prdtadded = false;
			int count = 1;
			do
			{
					WebElement ShoppingBagCount = BNBasicCommonMethods.findElement(driver, obj, "shopicon_count");
					System.out.println(ShoppingBagCount.getText());
					String str  = ShoppingBagCount.getText();
					int prdtval = Integer.parseInt(str);
				    WebElement ShoppingBagIcon = BNBasicCommonMethods.findElement(driver, obj, "EmptyShoppingBagCount");
				
				if(prdtval>0)
				{
					prdtadded=true;
					ShoppingBagIcon = BNBasicCommonMethods.findElement(driver, obj, "EmptyShoppingBagCount");
					log.add("Product Count is shown");
					pass("product count is displayed on Bag icon at the header in the product detail page",log);
					ShoppingBagIcon.click();
					Thread.sleep(4000);
					BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
					WebElement LoginCheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "LoginCheckoutBtn");
					wait.until(ExpectedConditions.visibilityOf(LoginCheckoutBtn));
					Thread.sleep(1000);
					LoginCheckoutBtn.click();
					Thread.sleep(3000);
					WebElement signInIframe = BNBasicCommonMethods.findElement(driver, obj, "signInIframe");
					driver.switchTo().frame(signInIframe);
					wait.until(ExpectedConditions.attributeToBe(miniCartLoadingGauge, "style", "display: none;"));
				    wait.until(ExpectedConditions.visibilityOf(secureCheckOutBtn));
					break;
				}
				else
				{
					fail("product count is not displayed on Bag icon at the header in the product detail page");
					count++;
					prdtadded=false;
					PDPATBClick();
					continue;
				}
			}
			while(prdtadded==true||count<3);*/
			
		}
		catch(Exception e)
		{
			//pass("0 product count in the shopping bag icon is shown");
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
		
	/*BNIA361 - Verify that while tapping bag icon,it should navigate to shopping bag page */
	public void BNIA361()
	{
		ChildCreation("BNIA361 - Verify that while tapping bag icon,it should navigate to shopping bag page ");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
			WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			logo.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Searchtile");
			WebElement searchtile = BNBasicCommonMethods.findElement(driver, obj, "Searchtile");
			searchtile.click();
			Thread.sleep(1500);
			BNBasicCommonMethods.waitforElement(wait, obj, "ShopIcon");
			WebElement shopicon = BNBasicCommonMethods.findElement(driver, obj, "ShopIcon");
			shopicon.click();
			Thread.sleep(2500);
			try
			{
				BNBasicCommonMethods.waitforElement(wait, obj, "ShoppingBag_OrderSummary");
				WebElement ShoppingBagPage_Ordersummery =  BNBasicCommonMethods.findElement(driver, obj, "ShoppingBag_OrderSummary");
				if(ShoppingBagPage_Ordersummery.isDisplayed())
				{
					log.add("The Product Containing Shopping Bag page is shown");
					pass("while tapping bag icon,it navigates to shopping bag page",log);
				}
				else
				{
					fail("The Product Containing Shopping Bag page is not shown");
				}
			}
			catch(Exception e)
			{
				BNBasicCommonMethods.waitforElement(wait, obj, "ContinueShoppingbutton");
				WebElement ShoppingBag_ContinueButton = BNBasicCommonMethods.findElement(driver, obj, "ContinueShoppingbutton");
				if(ShoppingBag_ContinueButton.isDisplayed())
				{
					log.add("Empty Shopping Bag Page is Shown");
					
					pass("while tapping bag icon,it navigates to shopping bag page",log);
				}
				else
				{
					fail("while tapping bag icon,it does not navigates to shopping bag page");
				}
			}
			
				
			/*BNBasicCommonMethods.waitforElement(wait, obj, "ContinueShoppingbutton");
			WebElement continueshopping = BNBasicCommonMethods.findElement(driver, obj, "ContinueShoppingbutton");*/
			//WebElement coupon_code = BNBasicCommonMethods.findElement(driver, obj, "couponcode");
			/*boolean continueshopping = 
			boolean coupon_code = 
					BNBasicCommonMethods.isElementPresent(ContinueShoppingbutton);
					BNBasicCommonMethods.isElementPresent(couponcode);*/
			/*if((BNBasicCommonMethods.isElementPresent(continueshopping)))
			{
				pass("while tapping bag icon,it navigates to shopping bag page");
			}
			else
				fail("while tapping bag icon,it does not navigates to shopping bag page");*/
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA361\n"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA363 - Verify that  product image should be shown and fit to screen*/
	public void BNIA363()
	{
		ChildCreation("BNIA363 - Verify that  product image should be shown and fit to screen");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
			WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			logo.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Searchtile");
			BNBasicCommonMethods.findElement(driver, obj, "Searchtile").click();
			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			WebElement searchbox1 = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
			Thread.sleep(2000);
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String SearchEan  = BNBasicCommonMethods.getExcelNumericVal("BNIA363", sheet, 3);
			searchbox1.sendKeys(SearchEan);
			searchbox1.sendKeys(Keys.ENTER);
			/*BNBasicCommonMethods.waitforElement(wait, obj, "backarrow");
			BNBasicCommonMethods.findElement(driver, obj, "backarrow").click();
			BNBasicCommonMethods.waitforElement(wait, obj, element)*/
			BNBasicCommonMethods.waitforElement(wait, obj, "pdp_image");
			WebElement image = BNBasicCommonMethods.findElement(driver, obj, "pdp_image");
			image.click();
			Thread.sleep(1000);
			//BNBasicCommonMethods.waitforElement(wait, obj, "pdp_imageMask");
			WebElement Imask = BNBasicCommonMethods.findElement(driver, obj, "ImageClickEvent1");
			Actions actions = new Actions(driver);
			actions.moveToElement(Imask).click().build().perform();
			Imask.click();
			if(BNBasicCommonMethods.isElementPresent(image))
			{
				pass("product image is shown and fit to screen");
			}
			else
			{
				fail("product image is not shown and fit to screen");
			}
			/*Thread.sleep(1000);
			WebElement imageZoomed = BNBasicCommonMethods.findElement(driver, obj, "pdp_imageZoomed");
			if(BNBasicCommonMethods.isElementPresent(imageZoomed))
			{
				
			}*/
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA364 - Verify that  while tapping the image in the product detail page,it should be zoomed */
	public void BNIA364()
	{
		ChildCreation("BNIA364 - Verify that  while tapping the image in the product detail page,it should be zoomed ");
		try
		{
			WebElement image = BNBasicCommonMethods.findElement(driver, obj, "pdp_image");
			image.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "pdp_imageZoomed");
			WebElement imageZoomed = BNBasicCommonMethods.findElement(driver, obj, "pdp_imageZoomed");
			
			if(BNBasicCommonMethods.isElementPresent(imageZoomed))
			{
				pass("while tapping the image in the product detail page,it gets zoomed");
			}
			else
			{
				fail("while tapping the image in the product detail page,it does not gets zoomed");
			}
			
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA365 - Verify that while tapping anywhere in the product detail page,the image overlay should be closed*/
	public void BNIA365()
	{
		ChildCreation("BNIA365 - Verify that while tapping anywhere in the product detail page,the image overlay should be closed");
		try
		{
			/*WebElement image = BNBasicCommonMethods.findElement(driver, obj, "pdp_image");
			image.click();*/
			WebElement Imask = BNBasicCommonMethods.findElement(driver, obj, "ImageClickEvent1");
			Actions actions = new Actions(driver);
			actions.moveToElement(Imask).click().build().perform();
			Imask.click();
			/*BNBasicCommonMethods.waitforElement(wait, obj, "pdp_imageZoomed");
			WebElement imageZoomed = BNBasicCommonMethods.findElement(driver, obj, "pdp_imageZoomed");
			BNBasicCommonMethods.findElement(driver, obj, "pdp_imageMask").click();*/
			WebElement imagezoom_notactive = BNBasicCommonMethods.findElement(driver, obj, "pdp_imagenotactive");
			if(BNBasicCommonMethods.isElementPresent(imagezoom_notactive))
			{
				pass("while tapping anywhere in the product detail page,the image overlay gets closed");
			}
			else
			{
				fail("while tapping anywhere in the product detail page,the image overlay does not gets closed");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA366 - Verify that on tapping outside the image ,it should close the zoomed Image */
	public void BNIA366()
	{
		ChildCreation("BNIA366 - Verify that on tapping outside the image ,it should close the zoomed Image ");
		try
		{
			WebElement image = BNBasicCommonMethods.findElement(driver, obj, "pdp_image");
			image.click();
			WebElement Imask = BNBasicCommonMethods.findElement(driver, obj, "ImageClickEvent1");
			Actions actions = new Actions(driver);
			actions.moveToElement(Imask).click().build().perform();
			Imask.click();
			WebElement imagezoom_notactive = BNBasicCommonMethods.findElement(driver, obj, "pdp_imagenotactive");
			if(BNBasicCommonMethods.isElementPresent(imagezoom_notactive))
			{
				pass("On tapping outside the image ,it closed the zoomed Image");
			}
			else
			{
				fail("On tapping outside the image ,it does not closed the zoomed Image");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA367 - Verify that other images of the product should be displayed below the primary image in the product detail page*/
	public void BNIA367()
	{
		ChildCreation("BNIA367 - Verify that other images of the product should be displayed below the primary image in the product detail page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
			WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			logo.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Searchtile");
			WebElement searchtile =  BNBasicCommonMethods.findElement(driver, obj, "Searchtile");
			searchtile.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			WebElement sbox = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String SearchEAN = BNBasicCommonMethods.getExcelNumericVal("BNIA367", sheet, 3);
			sbox.sendKeys(SearchEAN);
			Thread.sleep(500);
			Actions act = new Actions(driver);
		    act.sendKeys(Keys.ENTER).build().perform();
			//sbox.sendKeys(Keys.ENTER);
			BNBasicCommonMethods.waitforElement(wait, obj, "secondaryimages");
			WebElement secImages = BNBasicCommonMethods.findElement(driver, obj, "secondaryimages");
			if(BNBasicCommonMethods.isElementPresent(secImages))
			{
				pass("other images of the product is displayed below the primary image in the product detail page");
			}
			else
			{
				fail("other images of the product is not displayed below the primary image in the product detail page");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA367 "+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA368 - Verify that tapped image should be shown enlarged when the image is selected from the list*/
	public void BNIA368()
	{
		ChildCreation("BNIA368 - Verify that tapped image should be shown enlarged when the image is selected from the list");
		try
		{
			Thread.sleep(1000);
			List<WebElement> PDPsec_images = driver.findElements(By.xpath("//*[@class='pdpImgAltImageCont']"));
			Random r = new Random();
			int new1 = r.nextInt(PDPsec_images.size());
			if(new1<1)
			{
				new1 = 1;
			}
			Thread.sleep(2000);
			WebElement Secondaryimage = driver.findElement(By.xpath("(//*[@class='pdpImgAltImage'])["+new1+"]"));
			Secondaryimage.click();
			WebElement PrimaryImage = BNBasicCommonMethods.findElement(driver, obj, "pdp_image");
			String PrimaryImageUrl = PrimaryImage.getAttribute("src");
			//System.out.println(""+PrimaryImageUrl);
			String SecImageUrl = Secondaryimage.getAttribute("src");
			//System.out.println(""+SecImageUrl);
			
			if(SecImageUrl.equals(PrimaryImageUrl))
			{
				pass("Selected image is displayed in the primary Image in PDP Page.");
			}
			else
			{
				fail("Selected image is not displayed in the Primay Image in PDP page ");
			}
			
			
			/*String str = "//prodimage.images-bn.com/pimages/0700304046703_p1_v3_s670x700.jpg";
			WebElement pdpimage = BNBasicCommonMethods.findElement(driver, obj, "pdp_image");*/
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA368 "+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA369 - Verify that Email this Item button should be displayed in the product detail page*/
	public void BNIA369()
	{
		ChildCreation("BNIA369 - Verify that Email this Item button should be displayed in the product detail page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "EmailButton");
			WebElement emailbutton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			if(BNBasicCommonMethods.isElementPresent(emailbutton))
			{
				pass("Email this Item button is displayed in the product detail page");
			}
			else
			{
				fail("Email this Item button is not displayed in the product detail page");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA369 "+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA370 - Verify that while tapping Email this Item button, the Email this item overlay should be displayed*/
	public void BNIA370()
	{
		ChildCreation("BNIA370 - Verify that while tapping Email this Item button, the Email this item overlay should be displayed");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "EmailButton");
			WebElement emailbutton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			emailbutton.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "EmailOverlay");
			WebElement emailoverlay = BNBasicCommonMethods.findElement(driver, obj, "EmailOverlay");
			if(BNBasicCommonMethods.isElementPresent(emailoverlay))
			{
				pass("while tapping Email this Item button, the Email this item overlay is displayed");
			}
			else
			{
				fail("while tapping Email this Item button, the Email this item overlay is not displayed");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA370 "+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA371 - Verify that  Email this Item overlay should be displayed in center at the product detail page*/
	public void BNIA371()
	{
		ChildCreation("BNIA371 - Verify that  Email this Item overlay should be displayed in center at the product detail page");
		try
		{
			//BNBasicCommonMethods.waitforElement(wait, obj, "EmailButton");
			//WebElement emailbutton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			//emailbutton.click();
			WebElement email_overlay = BNBasicCommonMethods.findElement(driver, obj, "EmailOverlay");
			//System.out.println("The Email this Item Overlay Location is "+email_overlay.getLocation());
			
			WebElement ToEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailToAddress");
			WebElement FromEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailFromAddress");
			WebElement ToNamefield = BNBasicCommonMethods.findElement(driver, obj, "EmailToName");
			WebElement FromNameField = BNBasicCommonMethods.findElement(driver, obj, "EmailFromName");
			WebElement messagefield = BNBasicCommonMethods.findElement(driver, obj, "EmailMessageField");
			if((BNBasicCommonMethods.isElementPresent(ToEmailfield)) && (BNBasicCommonMethods.isElementPresent(FromEmailfield)) && (BNBasicCommonMethods.isElementPresent(ToNamefield)))
			{
				if((BNBasicCommonMethods.isElementPresent(FromNameField)) && (BNBasicCommonMethods.isElementPresent(messagefield)))
				{
					pass("Email this Item overlay is displayed in center at the product detail page");
				}
				else
				{
					fail("Email this Item overlay is not displayed in center at the product detail page");
				}
			}
			else
			{
				fail("Email this Item overlay is not displayed in center at the product detail page");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA371 "+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA372 - Verify that Email this item overlay should contains email text field & name text field for both the From & To section.*/
	public void BNIA372()
	{
		ChildCreation("BNIA372 - Verify that Email this item overlay should contains email text field & name text field for both the From & To section.");
		try
		{
			WebElement ToEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailToAddress");
			WebElement FromEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailFromAddress");
			WebElement ToNamefield = BNBasicCommonMethods.findElement(driver, obj, "EmailToName");
			WebElement FromNameField = BNBasicCommonMethods.findElement(driver, obj, "EmailToName");
			WebElement messagefield = BNBasicCommonMethods.findElement(driver, obj, "EmailMessageField");
			if((BNBasicCommonMethods.isElementPresent(ToEmailfield)) && (BNBasicCommonMethods.isElementPresent(FromEmailfield)) && (BNBasicCommonMethods.isElementPresent(ToNamefield)))
			{
				if((BNBasicCommonMethods.isElementPresent(FromNameField)) && (BNBasicCommonMethods.isElementPresent(messagefield)))
				{
					pass("Email this item overlay contains email text field & name text field for both the From & To section.");
				}
				else
				{
					fail("Email this item overlay does not contains email text field & name text field for both the From & To section.");
				}
			}
			else
			{
				fail("Email this item overlay does not contains email text field & name text field for both the From & To section.");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA372 "+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA373 - Verify that user able to enter in the text fields and it should accepts only valid formats (for email address fields: alphanumeric characters,symbols and special characters)(for name fields :characters and symbols)*/
	public void BNIA373()
	{		ChildCreation("BNIA373 - Verify that user able to enter in the text fields and it should accepts only valid formats (for email address fields: alphanumeric characters,symbols and special characters)(for name fields :characters and symbols)");
		try
		{
			WebElement ToEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailToAddress");
			WebElement FromEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailFromAddress");
			WebElement ToNamefield = BNBasicCommonMethods.findElement(driver, obj, "EmailToName");
			WebElement FromNameField = BNBasicCommonMethods.findElement(driver, obj, "EmailFromName");
			WebElement Alert1 = BNBasicCommonMethods.findElement(driver, obj, "EmailErrorAlert1");
			WebElement Alert2 = BNBasicCommonMethods.findElement(driver, obj, "EmailErrorAlert2");
			WebElement Alert3 = BNBasicCommonMethods.findElement(driver, obj, "EmailErrorAlert3");
			WebElement Alert4 = BNBasicCommonMethods.findElement(driver, obj, "EmailErrorAlert4");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String new1 = BNBasicCommonMethods.getExcelVal("BNIA373", sheet, 2);
			String newary[] = new1.split("\n");
			Actions act = new Actions(driver);
			act.click(ToEmailfield).sendKeys(newary[4]).build().perform();
			act.click(FromEmailfield).sendKeys(newary[5]).build().perform();
			WebElement emailbutton = BNBasicCommonMethods.findElement(driver, obj, "EmailShareButton");
			emailbutton.click();
			if((Alert1.getText().equals(newary[0])) && (!Alert3.getText().equals(newary[2])))
			{
				log.add("To Email Address field is invalid & From Email Address field is valid");
			}
			else
			{
				log.add("Something went wrong in To Email Address field & From Email Address field\n"+newary[0] +"\n"+newary[2]);
			}
		    ToEmailfield.clear();
			FromEmailfield.clear();
			act.click(ToEmailfield).sendKeys(newary[5]).build().perform();
			act.click(FromEmailfield).sendKeys(newary[6]).build().perform();
			emailbutton.click();
			if((Alert3.getText().equals(newary[2])) && (!Alert1.getText().equals(newary[0])))
			{
				log.add("To Email Address field is valid & From Email Address field is invalid");
			}
			else
			{
				log.add("Something went wrong in To Email Address field & From Email Address field\n"+newary[2] +"\n"+newary[1]);
			}
			ToEmailfield.clear();
			FromEmailfield.clear();
			act.click(ToEmailfield).sendKeys(newary[7]).build().perform();
			act.click(FromEmailfield).sendKeys(newary[8]).build().perform();
			emailbutton.click();
			if((Alert3.getText().equals(newary[2])) && (Alert1.getText().equals(newary[0])))
			{
				log.add("Both To Email Address field & From Email Address field is invalid");
			}
			else
			{
				log.add("Something went wrong in To Email Address field & From Email Address field\n"+newary[2] +"\n"+newary[0]);
			}
			ToEmailfield.clear();
			FromEmailfield.clear();
			
			act.click(ToNamefield).sendKeys(newary[9]).build().perform();
			act.click(FromNameField).sendKeys(newary[10]).build().perform();
			emailbutton.click();
			if((ToNamefield.getAttribute("value").isEmpty()) && (!Alert4.getText().equals(newary[3])))
			{
				log.add("To Name field is does not accepts special characters(empty) & From Name field is valid");
				
			}
			else
			{
				log.add("Something went wrong in To Name field & From Name field field\n"+newary[3]);
			}
			ToNamefield.clear();
			FromNameField.clear();
			
			act.click(ToNamefield).sendKeys(newary[11]).build().perform();
			act.click(FromNameField).sendKeys(newary[12]).build().perform();
			emailbutton.click();
			if((!Alert2.getText().equals(newary[1])) && (FromNameField.getAttribute("value").isEmpty()))
			{
				log.add("To Name field is valid & From Name field does not accepts numbers(empty)");
				
			}
			else
			{
				log.add("Something went wrong in To Name field & From Name field field\n"+newary[1]);
			}
			ToNamefield.clear();
			FromEmailfield.clear();
			
			act.click(ToNamefield).sendKeys(newary[13]).build().perform();
			act.click(FromEmailfield).sendKeys(newary[14]).build().perform();
			emailbutton.click();
			if((ToNamefield.getAttribute("value").isEmpty()) && (FromNameField.getAttribute("value").isEmpty()))
			{
				log.add("Both To Name field & From Name field does not accepts special characters and numbers(i.e.user is not able to enter special characters and numbers)");
				
				
			}
			else
			{
				log.add("Something went wrong in To Name field & From Name field field\n");
			}
			ToNamefield.clear();
			FromEmailfield.clear();
			ToEmailfield.clear();
			FromEmailfield.clear();
			emailbutton.click();
			if((Alert1.getText().equals(newary[0])) && (Alert3.getText().equals(newary[2])))
			{
				log.add("Both To Email Address and From Email Address is Empty");
				if((Alert2.getText().equals(newary[1])) && (Alert4.getText().equals(newary[3])))
				{
					log.add("Both To Name Field and From Name Field is Empty");
					pass("user able to enter in the text fields and it accepts only valid formats (for email address fields: alphanumeric characters,symbols and special characters)(for name fields :characters and symbols)",log);
				}
				else
				{
					fail("user not able to enter in the text fields and it does not accepts only valid formats (for email address fields: alphanumeric characters,symbols and special characters)(for name fields :characters and symbols)",log);
				}
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA373\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA374 - Verify that Message text box should be displayed with inline text optional in the PDP page*/
	public void BNIA374()
	{
		ChildCreation("BNIA374 - Verify that Message text box should be displayed with inline text optional in the PDP page");
		try
		{
			WebElement messagefield = BNBasicCommonMethods.findElement(driver, obj, "EmailMessageField");
			String EmailInlinetext = messagefield.getAttribute("placeholder");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String EmailTextboxText = BNBasicCommonMethods.getExcelVal("BNIA374", sheet, 2);
			//System.out.println(""+EmailTextboxText);
			if((BNBasicCommonMethods.isElementPresent(messagefield)) && (EmailInlinetext.equals(EmailTextboxText)))
			{
				log.add("Message text box is present and inline text is shown\n"+EmailTextboxText);
				pass("Message text box is displayed with inline text optional in the PDP page",log);
			}
			else
			{
				fail("Message text box is not displayed with inline text optional in the PDP page");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA374\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA375 - Verify that inline optional field should accept maximum of 250 characters*/
	public void BNIA375()
	{
		ChildCreation("BNIA375 - Verify that inline optional field should accept maximum of 250 characters");
		try
		{
			WebElement messagefield = BNBasicCommonMethods.findElement(driver, obj, "EmailMessageField");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String MessageFieldText = BNBasicCommonMethods.getExcelVal("BNIA375", sheet, 2); 
			messagefield.sendKeys(MessageFieldText);
			int fieldlength = messagefield.getAttribute("value").length();
			if(fieldlength<=250)
			{
				log.add("The inline field accepts less than or maximum of 250 characters");
			}
			else
			{
				log.add("The inline field accepts more than 250 characters");
			}
			messagefield.clear();
			Thread.sleep(1000);
			//String val = "Optional (Maximum 250 characters)";
			if(messagefield.getAttribute("value").isEmpty())
			{
				log.add("The inline field is empty");
			}
			else
			{
				return;
			}
			messagefield.sendKeys("Software testing is an investigation conducted to provide stakeholders with information "
					+ "about the quality of the product or service under test.Software testing can also provide an objective,"
					+ "For example, in a phased process,most testing occurs systemsSoftware testing involves the execution of "
					+ "a software component or system component to evaluate one or more properties of interest. In general, these"
					+ " properties indicate the extent to which the component or system under test:");
			int fieldlength1 = messagefield.getAttribute("value").length();
			if(fieldlength1==250)
			{
				log.add("The inline field does not accepts more than 250 characters");
				pass("The inline optional field accepts maximum of 250 characters",log);
			}
			else
			{
				fail("The inline optional field does not accepts maximum of 250 characters");
			}
			messagefield.clear();
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA376 - Verify that on selecting send button after entering valid email address and name, success alert should be displayed.*/
	public void BNIA376()
	{
		ChildCreation("BNIA376 - Verify that on selecting send button after entering valid email address and name, success alert should be displayed.");
		try
		{
			WebElement ToEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailToAddress");
			WebElement FromEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailFromAddress");
			WebElement ToNamefield = BNBasicCommonMethods.findElement(driver, obj, "EmailToName");
			WebElement FromNameField = BNBasicCommonMethods.findElement(driver, obj, "EmailFromName");
			WebElement messagefield = BNBasicCommonMethods.findElement(driver, obj, "EmailMessageField");
			WebElement EmailButton = BNBasicCommonMethods.findElement(driver, obj, "EmailShareButton");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String values = BNBasicCommonMethods.getExcelVal("BNIA376", sheet, 2);
			String val[]  = values.split("\n");
			ToEmailfield.sendKeys(val[0]);
			ToNamefield.sendKeys(val[1]);
			Thread.sleep(1000);
			final String ToName_field = ToNamefield.getAttribute("value");
			FromEmailfield.sendKeys(val[2]);
			FromNameField.sendKeys(val[3]);
			messagefield.sendKeys(val[4]);
			EmailButton.click();
			Thread.sleep(2000);
			WebElement emailonthewayOverlay = BNBasicCommonMethods.findElement(driver, obj, "EmailOntheWayOverlay");
			WebElement emailonthewayalert = BNBasicCommonMethods.findElement(driver, obj, "EmailOntheWayAlert");
			WebElement SuccessAlertText = BNBasicCommonMethods.findElement(driver, obj, "EmailSentTextAlert");
			String ErrorAlert = val[5]; 
			String techAlert = emailonthewayalert.getText();
			String success_Alert = SuccessAlertText.getText();
			//ToNamefield = BNBasicCommonMethods.findElement(driver, obj, "EmailToName");
			//System.out.println(""+success_Alert);
			String Successalert = "Your email to "+ToName_field+" has been sent successfully.";
			//String onthewayAlert = emailonthewayalert.getText();
			//String onthewayAlert1 = val[6];
			//Thread.sleep(500);
			if(BNBasicCommonMethods.isElementPresent(emailonthewayOverlay))
			{
				if(Successalert.equals(success_Alert))
				{
					log.add("Email Sent Successfully");
					pass("On selecting send button after entering valid email address and name, success alert gets displayed.",log);
				}
				else if(techAlert.equals(ErrorAlert))
				{
						fail("Email not sent" +ErrorAlert+"Alert is shown");
				}
				else
				{
					fail("Email not sent - fail");
				}
			}
			else 
			{
				fail("Email is not sent");
			}
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA376\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA377 - Verify that  while tapping send by entering invalid email address and  names then Invalid message should be displayed as per the creative*/
	public void BNIA377()
	{
		ChildCreation("BNIA377 - Verify that  while tapping send by entering invalid email address and  names then Invalid message should be displayed as per the creative");
		try
		{
			WebElement Emailcloseicon = BNBasicCommonMethods.findElement(driver, obj, "EmailSuccessOverlay_Closeicon");
			Emailcloseicon.click();
			WebElement emailbutton1 = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			emailbutton1.click();
			WebElement ToEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailToAddress");
			WebElement FromEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailFromAddress");
			WebElement ToNamefield = BNBasicCommonMethods.findElement(driver, obj, "EmailToName");
			WebElement FromNameField = BNBasicCommonMethods.findElement(driver, obj, "EmailFromName");
			WebElement Alert1 = BNBasicCommonMethods.findElement(driver, obj, "EmailErrorAlert1");
			//WebElement Alert2 = BNBasicCommonMethods.findElement(driver, obj, "EmailErrorAlert2");
			WebElement Alert3 = BNBasicCommonMethods.findElement(driver, obj, "EmailErrorAlert3");
			//WebElement Alert4 = BNBasicCommonMethods.findElement(driver, obj, "EmailErrorAlert4");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String new1 = BNBasicCommonMethods.getExcelVal("BNIA373", sheet, 2);
			String newary[] = new1.split("\n");
			ToEmailfield.sendKeys(newary[15]);
			FromEmailfield.sendKeys(newary[16]);
			String alert1 = newary[0];
			String alert2 = newary[2];
			//String alert3 = newary[3];
			//String alert4 = newary[4];
			WebElement emailbutton = BNBasicCommonMethods.findElement(driver, obj, "EmailShareButton");
			emailbutton.click();
			//String alt1 = Alert1.getText();
			//String alt3 = Alert3.getText();
			if((Alert1.getText().equals(alert1)) && (Alert3.getText().equals(alert2)))
			{
				log.add("Both To Email Address and From Email Address is Empty");
				if((ToNamefield.getAttribute("value").isEmpty()) && ((FromNameField.getAttribute("value").isEmpty())))
				{
					log.add("Both To Name Field and From Name Field is Empty");
					pass("user able to enter in the text fields and it accepts only valid formats (for email address fields: alphanumeric characters,symbols and special characters)(for name fields :characters and symbols)",log);
				}
				else
				{
					fail("user not able to enter in the text fields and it does not accepts only valid formats (for email address fields: alphanumeric characters,symbols and special characters)(for name fields :characters and symbols)",log);
				}
			}
			ToEmailfield.clear();
			FromEmailfield.clear();
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA377\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA378 - Verify that send button should be highlighted by default in the PDP page*/
	public void BNIA378()
	{
		ChildCreation("BNIA378 - Verify that send button should be highlighted by default in the PDP page");
		try
		{
			WebElement EmailButton = BNBasicCommonMethods.findElement(driver, obj, "EmailShareButton");
			String email_sharebutton = EmailButton.getCssValue("background-color");
			//System.out.println(""+email_sharebutton);
			Color emailsharebuttoncolor = Color.fromString(email_sharebutton);
			String hexemail_sharebutton = emailsharebuttoncolor.asHex();
			//System.out.println(""+hexemail_sharebutton);
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String btncolor = BNBasicCommonMethods.getExcelVal("BNIA378", sheet, 6);
			if(hexemail_sharebutton.equals(btncolor))
			{
				log.add("Email Share Button is highlighted in green color" +"("+btncolor+")");
				pass("Send button is highlighted by default in the PDP page",log);
			}
			else
			{
				fail("Send button is not highlighted by default in the PDP page",log);
			}
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA378\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA380 - Verify that while tapping cancel button, the overlay should be closed and remains in the PDP page*/
	public void BNIA380()
	{
		ChildCreation("BNIA380 - Verify that while tapping cancel button, the overlay should be closed and remains in the PDP page");
		try
		{
			WebElement cancelbtn = BNBasicCommonMethods.findElement(driver, obj, "EmailCancelButton");
			cancelbtn.click();
			WebElement pdpimage = BNBasicCommonMethods.findElement(driver, obj, "pdp_image");
			if(BNBasicCommonMethods.isElementPresent(pdpimage))
			{
				pass("while tapping cancel button, the overlay gets closed and remains in the PDP page");
			}
			else
			{
				fail("while tapping cancel button, the overlay does not gets closed");
			}
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA380\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA381 - Verify that overview link should be displayed with right arrow symbol in the product detail page*/
	public void BNIA381()
	{
		ChildCreation("BNIA381 - Verify that overview link should be displayed with right arrow symbol in the product detail page");
		try
		{
			 BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
			 WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			 logo.click();
			 BNBasicCommonMethods.waitforElement(wait, obj, "Searchtile");
			 BNBasicCommonMethods.findElement(driver, obj, "Searchtile").click();
			 BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			 WebElement searchbox1 = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
			 sheet = BNBasicCommonMethods.excelsetUp("PDP");
			 String EAN = BNBasicCommonMethods.getExcelNumericVal("BNIA363", sheet, 3);
			 searchbox1.sendKeys(EAN);
			 Thread.sleep(500);
			 Actions act = new Actions(driver);
			 act.sendKeys(Keys.ENTER).build().perform();
			// searchbox1.sendKeys(Keys.ENTER);
			 BNBasicCommonMethods.waitforElement(wait, obj, "PDPOverviewLink");
			 WebElement overviewlink = BNBasicCommonMethods.findElement(driver, obj, "PDPOverviewLink");
			 BNBasicCommonMethods.waitforElement(wait, obj, "PDPOverviewLink_Arrow");
			 WebElement overviewlink_arrow = BNBasicCommonMethods.findElement(driver, obj, "PDPOverviewLink_Arrow");
			 if((BNBasicCommonMethods.isElementPresent(overviewlink)) && (BNBasicCommonMethods.isElementPresent(overviewlink_arrow)))
			 {
				 pass("overview link is displayed with right arrow symbol in the product detail page");
			 }
			 else
			 {
				 fail("overview link is not displayed with right arrow symbol in the product detail page");
			 }
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA381\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA382 - Verify that while tapping Customer also Bought link the Customer Who Bought This also Bought section should be displayed*/
	public void BNIA382()
	{
		ChildCreation("BNIA382 - Verify that while tapping Customer also Bought link the Customer Who Bought This also Bought section should be displayed");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomeralsoBoughtLink");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomeralsoBoughtLink_Arrow");
			WebElement customeralsoboughtlink = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomeralsoBoughtLink");
			Thread.sleep(1500);
			customeralsoboughtlink.click();
			Thread.sleep(4500);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerAlsoBoughtThisSection");
			WebElement customeralsoboughtthissection = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerAlsoBoughtThisSection");
				if((BNBasicCommonMethods.isElementPresent(customeralsoboughtthissection)))
				{
					pass("while tapping Customer also Bought link the Customer Who Bought This also Bought section is displayed");
				}
				else
				{
					fail("while tapping  Customer also Bought link the Customer Who Bought This also Bought section is not displayed");
				}
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA382\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA383 - Verify that Customer Who Bought This also Bought section should contains list of Products with product image,product name,Author name & rating stars */
	public void BNIA383()
	{
		ChildCreation("BNIA383 - Verify that Customer Who Bought This also Bought section should contains list of Products with product image,product name,Author name & rating stars ");
		try
		{
			Thread.sleep(3000);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerAlsoBoughtThisSection_ShowMoreLink");
			WebElement PDP_cusalsobought_showmore = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerAlsoBoughtThisSection_ShowMoreLink");
			PDP_cusalsobought_showmore.click();
			Thread.sleep(2000);
			//List<WebElement> CustomeralsoboughtProductsImg = driver.findElements(By.xpath("//*[contains(@class, 'sk_mobRecommendedItemImgLink')]"));
			List<WebElement> CustomeralsoboughtProductsTitle = driver.findElements(By.xpath("//*[contains(@class,  'sktab_pdtGridItemTitle')]"));
			List<WebElement> CustomeralsoboughtProductsAuthorName = driver.findElements(By.xpath("//*[contains(@class,  'pdpSuggestedBkAuthor')]"));
			//List<WebElement> CustomeralsoboughtProductsRatings = driver.findElements(By.xpath("//*[contains(@class,  'sktab_pdtGrid_zeroRatingStar')]"));
			for(int i = 1;i<=CustomeralsoboughtProductsTitle.size();i++)
			{
				String str = driver.findElement(By.xpath("(//*[@class='sktab_pdtGridItemTitle'])["+i+"]")).getText();
				//(//*[@class='sktab_pdtGridItemTitle'])[+"i"+]
				//System.out.println(""+str);
				if(driver.findElement(By.xpath("(//*[@class='sktab_pdtGridItemTitle'])["+i+"]")).isDisplayed())
				{
					log.add(""+str +"Title is shown");
					pass("Product Title is shown for the corresponding products in Customer Who Bought This also Bought Section",log);
				}
				else
				{
					fail("Product Title is not shown");
				}
			}	
					
				for(int j = 1;j<=CustomeralsoboughtProductsAuthorName.size();j++)
				{
					String str1 = driver.findElement(By.xpath("(//*[@class='pdpSuggestedBkAuthor'])["+j+"]")).getText();
					//System.out.println(""+str1);
					if(driver.findElement(By.xpath("(//*[@class='pdpSuggestedBkAuthor'])["+j+"]")).isDisplayed())
					{
						log.add("+str1+" +"Author Name is shown");
						pass("Author Name is shown for the corresponding products in Customer Who Bought This also Bought Section",log);
					}
					else
					{
						fail("Author Name is not shown");
					}
				}
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA383\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA384 - Verify that Customer Review link should be displayed with right arrow symbol in Customer Who Bought This also Bought section*/
	public void BNIA384()
	{
		ChildCreation("BNIA384 - Verify that Customer Review link should be displayed with right arrow symbol in Customer Who Bought This also Bought section");
		try
		{
			 BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomerReviewsLink");
			 WebElement CustomerReviewsLink = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerReviewsLink");
			 BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomerReviewsLink_Arrow");
			 WebElement customerReviewsLink_arrow = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerReviewsLink_Arrow");
			 if((BNBasicCommonMethods.isElementPresent(CustomerReviewsLink)) && (BNBasicCommonMethods.isElementPresent(customerReviewsLink_arrow)))
			 {
				 pass("Customer Review link is displayed with right arrow symbol in Customer Who Bought This also Bought section");
			 }
			 else
			 {
				 fail("Customer Review link is not displayed with right arrow symbol in Customer Who Bought This also Bought section");
			 }
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA384\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	//BNIA385 - Verify that while tapping Customer Review link,the Customer Review section should be displayed
	public void BNIA385()
	{
		ChildCreation("BNIA385 - Verify that while tapping Customer Review link,the Customer Review section should be displayed");
		try
		{
			WebElement CustomerReviewsLink = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerReviewsLink");
			CustomerReviewsLink.click();
			WebElement customerReviewsSection = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsSection");
			if(BNBasicCommonMethods.isElementPresent(customerReviewsSection))
			{
				pass("while tapping  Customer Review link,the Customer Review section is displayed");
			}
			else
			{
				fail("while tapping  Customer Review link,the Customer Review section is not displayed");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA385\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	//BNIA386 - Verify that Customer Review Section should contains Rating stars(with Highlighted ratings),Review Description,username and Date of review in list view
	public void BNIA386()
	{
		ChildCreation("BNIA386 - Verify that Customer Review Section should contains Rating stars(with Highlighted ratings),Review Description,username and Date of review in list view");
		try
		{
			List<WebElement> CustomerReviewsRatings = driver.findElements(By.xpath("//*[contains(@class, 'pdpReviewListRatingsImg')]"));
			List<WebElement> CustomerReviewsDescription = driver.findElements(By.xpath("//*[contains(@class, 'pdpReviewListValueTxt')]"));
			List<WebElement> CustomerReviews_Name = driver.findElements(By.xpath("//*[contains(@class, 'pdpReviewListLabelTxt')]"));
			List<WebElement> CustomerReviews_Date = driver.findElements(By.xpath("//*[contains(@class, 'pdpReviewListDateTxt')]"));
			
			
			for(int i=1;i<=CustomerReviewsRatings.size();i++)
			{
				String str = driver.findElement(By.xpath("(//*[@class='pdpReviewListRatingsImg'])["+i+"]")).getAttribute("aria-label");
				
				//System.out.println(""+str);
				if(driver.findElement(By.xpath("(//*[@class='pdpReviewListRatingsImg'])["+i+"]")).isDisplayed())
				{
					log.add("+str+"+ "is shown");
					pass("Customer Ratings is shown in the Customer Reviews Section",log);
				}
				else
				{
					fail("Customer Ratings is not shown Customer Reviews Section");
				}
			}	
				for(int j=1;j<=CustomerReviewsDescription.size();j++)
				{
					String str1 = driver.findElement(By.xpath("(//*[@class='pdpReviewListValueTxt'])["+j+"]")).getText();
					//System.out.println(""+str1);
					if(driver.findElement(By.xpath("(//*[@class='pdpReviewListValueTxt'])["+j+"]")).isDisplayed())
					{
						log.add("+str1+\n" +"is shown in the Customer reviews Section");
						pass("Customer Reviews is shown in the Customer Reviews Section",log);
					}
					else
					{
						fail("Customer Reviews is not shown Customer Reviews Section");
					}
				}
			
			for(int m=1;m<=CustomerReviews_Name.size();m++)
			{
				String str = driver.findElement(By.xpath("(//*[@class='pdpReviewListLabelTxt'])["+m+"]")).getText();
				//System.out.println(""+str);
				if(driver.findElement(By.xpath("(//*[@class='pdpReviewListLabelTxt'])["+m+"]")).isDisplayed())
				{
					log.add("+str+\n"+ "is shown");
					pass("Customer Name is shown in the Customer Reviews Section",log);
				}
				else
				{
					fail("Customer Name is not shown Customer Reviews Section");
				}
			}	
				for(int n=1;n<=CustomerReviews_Date.size();n++)
				{
					String str1 = driver.findElement(By.xpath("(//*[@class='pdpReviewListDateTxt'])["+n+"]")).getText();
					//System.out.println(""+str1);
					if(driver.findElement(By.xpath("(//*[@class='pdpReviewListDateTxt'])["+n+"]")).isDisplayed())
					{
						log.add("+str1+\n"+"is shown");
						pass("Customer Name is shown in the Customer Reviews Section",log);
					}
					else
					{
						fail("Customer Name is not shown Customer Reviews Section");
					}
				}
			}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA386\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	//BNIA387 - Verify that  "Editorial Reviews" link should be displayed with right arrow symbol in "Customer Review" section
	public void BNIA387()
	{
		ChildCreation("BNIA387 - Verify that Editorial Reviews link should be displayed with right arrow symbol in Customer Review section");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomerEditorialReviewsLink");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomerEditorialReviewsLink_Arrow");
			WebElement editorialreviewslink = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerEditorialReviewsLink");
			WebElement editorialreviewsarrow = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerEditorialReviewsLink_Arrow");
			if((BNBasicCommonMethods.isElementPresent(editorialreviewslink)) && (BNBasicCommonMethods.isElementPresent(editorialreviewsarrow)))
			{
				pass("Editorial Reviews link is displayed with right arrow symbol in Customer Review section");
			}
			else
			{
				fail("Editorial Reviews link is not displayed with right arrow symbol in Customer Review section");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA387\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	//BNIA388 - Verify that while tapping Editorial Reviews link ,the Editorial Reviews section should be displayed.
	public void BNIA388()
	{
		ChildCreation("BNIA388 - Verify that while tapping Editorial Reviews link ,the Editorial Reviews section should be displayed");
		try
		{
			WebElement editorialreviewslink = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerEditorialReviewsLink");
			editorialreviewslink.click();
			Thread.sleep(2000);
			WebElement editorialreviewssection = BNBasicCommonMethods.findElement(driver, obj, "PDP_EditorialReviewsSection");
			if((BNBasicCommonMethods.isElementPresent(editorialreviewssection)))
			{
				pass("While tapping Editorial Reviews link ,the Editorial Reviews section is displayed");
			}
			else
			{
				fail("While tapping Editorial Reviews link ,the Editorial Reviews section is not displayed");
			}
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA388\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	//BNIA389 - Verify that Editorial Reviews  section should contains Editorial Reviews Description.
	public void BNIA389()
	{
		ChildCreation("BNIA389 - Verify that Editorial Reviews  section should contains Editorial Reviews Description");
		try
		{
			WebElement editorialreviews_title = BNBasicCommonMethods.findElement(driver, obj, "PDP_EditorialReviewsSection_Title");
			WebElement editorialreviews_description = BNBasicCommonMethods.findElement(driver, obj, "PDP_EditorialReviewsSection_Description");
			if((BNBasicCommonMethods.isElementPresent(editorialreviews_title)) && (BNBasicCommonMethods.isElementPresent(editorialreviews_description)))
			{
				log.add("Editorial reviews description and Title is shown");
				pass("Editorial Reviews section contains Editorial Reviews Description",log);
			}
			else
			{
				fail("Editorial Reviews section does not contains Editorial Reviews Description");
			}
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA389"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA390 - Verify that Meet the Author link should be displayed with right arrow symbol in Editorial Reviews section.*/	
	public void BNIA390()
	{
		ChildCreation("BNIA390 - Verify that Meet the Author link should be displayed with right arrow symbol in Editorial Reviews section");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPMeetTheAuthorLink");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPMeetTheAuthorLink_Arrow");
			WebElement meettheauthor = BNBasicCommonMethods.findElement(driver, obj, "PDPMeetTheAuthorLink");
			WebElement meettheauthorarrow = BNBasicCommonMethods.findElement(driver, obj, "PDPMeetTheAuthorLink_Arrow");
			if((BNBasicCommonMethods.isElementPresent(meettheauthor)) && (BNBasicCommonMethods.isElementPresent(meettheauthorarrow)))
			{
				pass("Meet the Author link is displayed with right arrow symbol in Editorial Reviews section");
			}
			else
			{
				fail("Meet the Author link is not displayed with right arrow symbol in Editorial Reviews section");
			}
		}
		catch(Exception e)
		{
            System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA390\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA391 - Verify that while tapping Meet the Author link,the Meet the Author section should be displayed*/
	public void BNIA391()
	{
		ChildCreation("BNIA391 - Verify that while tapping Meet the Author link,the Meet the Author section should be displayed");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPMeetTheAuthorLink");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPMeetTheAuthorLink_Arrow");
			WebElement meettheauthorlink = BNBasicCommonMethods.findElement(driver, obj, "PDPMeetTheAuthorLink");
			meettheauthorlink.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_MeetTheAuthorSection");
			WebElement meettheauthorsection = BNBasicCommonMethods.findElement(driver, obj, "PDP_MeetTheAuthorSection");
			WebElement meettheauthorsection_desc = BNBasicCommonMethods.findElement(driver, obj, "PDP_MeetTheAuthorSection_Description");
			if(BNBasicCommonMethods.isElementPresent(meettheauthorsection) && (BNBasicCommonMethods.isElementPresent(meettheauthorsection_desc)))
			{
				log.add("Author Description is shown");
				pass("While tapping Meet the Author link,the Meet the Author section is displayed",log);
			}
			else
			{
				fail("While tapping Meet the Author link,the Meet the Author section is not displayed");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA391\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA392 - Verify that Meet the Author section should contains Author image & Meet The Author description*/
	public void BNIA392()
	{
		ChildCreation("BNIA392 - Verify that Meet the Author section should contains Author image & Meet The Author description");
		try
		{
			WebElement meettheauthorsection_desc = BNBasicCommonMethods.findElement(driver, obj, "PDP_MeetTheAuthorSection_Description");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_MeetTheAuthorSection_Image");
			WebElement meettheauthorsection_img = BNBasicCommonMethods.findElement(driver, obj, "PDP_MeetTheAuthorSection_Image");
			if((BNBasicCommonMethods.isElementPresent(meettheauthorsection_desc)) && (BNBasicCommonMethods.isElementPresent(meettheauthorsection_img)))
			{
				pass("Meet the Author section contains Author image & Meet The Author description");
			}
			else
			{
				fail("Meet the Author section does not contains Author image & Meet The Author description");
			}
			
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA392\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	public void BNIA393()
	{
		ChildCreation("BNIA393 - Verify that Product Details link should be displayed with right arrow symbol in Meet the Author section");
		try
		{
			WebElement prdDetailsLink = BNBasicCommonMethods.findElement(driver, obj, "PDPProductDetailsLink");
			WebElement prdDetailsArrow = BNBasicCommonMethods.findElement(driver, obj, "PDPProductDetailsLink_Arrow");
			if((BNBasicCommonMethods.isElementPresent(prdDetailsLink)) && (BNBasicCommonMethods.isElementPresent(prdDetailsArrow)))
			{
				pass("Product Details link is displayed with right arrow symbol in Meet the Author section");
			}
			else
			{
				fail("Product Details link is not displayed with right arrow symbol in Meet the Author section");
			}
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA393\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA394 - Verify that while tapping Product Details link,the Product Details section should be displayed*/
	public void BNIA394()
	{
		ChildCreation("BNIA394 - Verify that while tapping Product Details link,the Product Details section should be displayed");
		try
		{
			WebElement prdDetailsLink = BNBasicCommonMethods.findElement(driver, obj, "PDPProductDetailsLink");
			prdDetailsLink.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductDetailsSection");
			WebElement prddetailssection = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductDetailsSection");
			if((BNBasicCommonMethods.isElementPresent(prddetailssection)))
			{
				pass("While tapping Product Details link,the Product Details section is displayed");
			}
			else
			{
				fail("While tapping Product Details link,the Product Details section is not displayed");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA394\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA395 - Verify that Product Details link should contains product description*/
	public void BNIA395()
	{
		ChildCreation("BNIA395 - Verify that Product Details link should contains product description");
		try
		{
			List<WebElement> prddetailssection_prddetailsLabel = driver.findElements(By.xpath("//*[contains(@class,  'sk_productDetailsLabel')]"));
			List<WebElement> prddetailssection_prddetailsValue = driver.findElements(By.xpath("//*[contains(@class,  'sk_productDetailsValue')]"));
			for(int i=1 , j=1 ; (i<=prddetailssection_prddetailsLabel.size()) &&(j<= prddetailssection_prddetailsValue.size()) ;i++ ,j++)
			{
				String str1 = driver.findElement(By.xpath("(//*[@class='sk_productDetailsLabel'])["+i+"]")).getText();
				String str2 = driver.findElement(By.xpath("(//*[@class='sk_productDetailsValue'])["+i+"]")).getText();
				//System.out.println(""+str1);
				//System.out.println(""+str2);
				if(driver.findElement(By.xpath("(//*[@class='sk_productDetailsLabel'])["+i+"]")).isDisplayed() && driver.findElement(By.xpath("(//*[@class='sk_productDetailsValue'])["+j+"]")).isDisplayed())
				{
					log.add(str1+" : "+str2);
					pass("Product Details is shown in the Product Details Section",log);
				}
				else
				{
					fail("Product Details Label is not shown in the Product Details Section");
				}
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA395\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA396 - Verify that Related Subject link should be displayed with right arrow symbol in Product Details section*/
	public void BNIA396()
	{
		ChildCreation("BNIA396 - Verify that Related Subject link should be displayed with right arrow symbol in Product Details section");
		try
		{
			WebElement relatedsubjectslink = BNBasicCommonMethods.findElement(driver, obj, "PDPRelatedSubjectsLink");
			WebElement relatedsubjectsArrow = BNBasicCommonMethods.findElement(driver, obj, "PDPRelatedSubjectsLink_Arrow");
			if((BNBasicCommonMethods.isElementPresent(relatedsubjectslink)) && (BNBasicCommonMethods.isElementPresent(relatedsubjectsArrow)))
			{
				pass("Related Subject link is displayed with right arrow symbol in Product Details section");
			}
			else
			{
				fail("Related Subject link is not displayed with right arrow symbol in Product Details section");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA396"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA397 - Verify that while tapping Related Subject link,the Related Subject section should be displayed*/
	public void BNIA397()
	{
		ChildCreation("BNIA397 - Verify that while tapping Related Subject link,the Related Subject section should be displayed");
		try
		{
			Thread.sleep(1500);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPRelatedSubjectsLink");
			WebElement relatedsubjectslink = BNBasicCommonMethods.findElement(driver, obj, "PDPRelatedSubjectsLink");
			relatedsubjectslink.click();
			Thread.sleep(3000);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_RelatedSubjectsSection_Link");
			WebElement relatedsubjectslink_link = BNBasicCommonMethods.findElement(driver, obj, "PDP_RelatedSubjectsSection_Link");
			if((BNBasicCommonMethods.isElementPresent(relatedsubjectslink)) && (BNBasicCommonMethods.isElementPresent(relatedsubjectslink_link)))
			{
				pass("While tapping Related Subject link,the Related Subject section is displayed");
			}
			else
			{
				fail("While tapping Related Subject link,the Related Subject section is not displayed");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA397\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA398 - Verify that Related Subject section should contains Related Subject description(links)*/
	public void BNIA398()
	{
		ChildCreation("BNIA398 - Verify that Related Subject section should contains Related Subject description(links)");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_RelatedSubjectsSection_Link");
			WebElement relatedsubjectslink_link = BNBasicCommonMethods.findElement(driver, obj, "PDP_RelatedSubjectsSection_Link");
			if((BNBasicCommonMethods.isElementPresent(relatedsubjectslink_link)))
			{
				pass("Related Subject section contains Related Subject description(links)");
			}
			else
				fail("Related Subject section does not contains Related Subject description(links)");
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA398n\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA399 - Verify that product name should be displayed at the top center in the product detail page*/
	public void BNIA399()
	{
		ChildCreation("BNIA399 - Verify that product name should be displayed at the top center in the product detail page");
		try
		{
			WebElement EmailButton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView(true);",EmailButton);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductName");
			WebElement PDP_ProductName = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductName");
			if(PDP_ProductName.isDisplayed())
			{
			   log.add("Displayed product title is "+PDP_ProductName.getText());
			   pass("Product title is displayed successfully",log);
				
			}
			else
			{
			   fail("product title is not displayed successfully");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA399\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA400 - Verify that Author name should be displayed below the product title in the product detail page*/
	public void BNIA400()
	{
		ChildCreation("BNIA400 - Verify that Author name should be displayed below the product title in the product detail page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AuthorName");
			WebElement pdp_prdname = BNBasicCommonMethods.findElement(driver, obj, "PDP_AuthorName");
			if((BNBasicCommonMethods.isElementPresent(pdp_prdname)))
			{
				log.add("Displayed Author Name is "+pdp_prdname.getText());
				pass("Author name is displayed below the product title in the product detail page",log);
			}
			else
			{
				fail("Author name is not displayed below the product title in the product detail page");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA400\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA401 - Verify that  product name should be highlighted in Bold*/
	public void BNIA401()
	{
		ChildCreation("BNIA401 - Verify that  product name should be highlighted in Bold");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductName");
			WebElement prdName = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductName");
			String val = prdName.getCssValue("font-family");
			//System.out.println(""+val);
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String str = BNBasicCommonMethods.getExcelVal("BNIA401", sheet, 4);
			if(str.contains(val))
			{
				log.add("The Product Title Font is\n"+val);
				pass("Product name is highlighted in Bold",log);
			}
			else
			{
				fail("Product name is not highlighted in Bold");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA401\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA402 - Verify that Rating and Review Link should be displayed in the product detail page*/
	public void BNIA402()
	{
		ChildCreation("BNIA402 - Verify that Rating and Review Link should be displayed in the product detail page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_Ratings");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_Reviews");
			WebElement pdpRatings = BNBasicCommonMethods.findElement(driver, obj, "PDP_Ratings");
			WebElement pdpReviews = BNBasicCommonMethods.findElement(driver, obj, "PDP_Reviews");
			if((BNBasicCommonMethods.isElementPresent(pdpRatings)) && (BNBasicCommonMethods.isElementPresent(pdpReviews)))
			{
				pass("Rating and Review Link is displayed in the product detail page");
			}
			else
			{
				fail("Rating and Review Link is not displayed in the product detail page");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA402\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	public void BNIA403()
	{
		ChildCreation("BNIA403 - Verify that rating should have maximum of 5 stars and it should be highlighted depends on rating");
		try
		{
			WebElement pdpRatings = BNBasicCommonMethods.findElement(driver, obj, "PDP_Ratings");
			String size = pdpRatings.getSize().toString();
			//System.out.println(""+size);
			String count = pdpRatings.getAttribute("aria-label");
			if(size=="5")
			{
				log.add("The Current raing is\n"+count);
				pass("Rating have maximum of 5 stars and it is highlighted depends on rating",log);
			}
			else
				fail("Rating does not have maximum of 5 stars and it is not highlighted depends on rating");
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA403\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA405 - Verify that All Formats & Editions page should display Paperback, Hardcover, NOOK Book, Audiobook tab switching options.*/
	public void BNIA405()
	{
		ChildCreation("BNIA405 - Verify that All Formats & Editions page should display Paperback, Hardcover, NOOK Book, Audiobook tab switching options.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOption");
			WebElement pdpAllformats = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOption");
			pdpAllformats.click();
			Thread.sleep(1000);
			List<WebElement> DropdownList = driver.findElements(By.xpath("//*[@class = 'sk_bn_formatOption']"));
			
			for(int i=1;i<=DropdownList.size();i++)
			//for(WebElement dropdown : DropdownList)
			{
				WebElement dropdownoptions = driver.findElement(By.xpath("(//*[@class = 'sk_bn_formatOption'])["+i+"]"));
				String drdplist = dropdownoptions.getAttribute("value");
				//System.out.println(""+drdplist);
				if(drdplist.equals("allFormat"))
				{
					dropdownoptions.click();
				}
				else
				{
					System.out.println("There is no All Fromats Option in the dropdown menu for the Selected product");
					//return;
				}
			}
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOverlay");
			//WebElement allformatsOverlay = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOverlay");
			List<WebElement> AllFormatsOverlayOptions = driver.findElements(By.xpath("//*[@class = 'sk_formatTitleTxt']"));
			for(int i=1;i<=AllFormatsOverlayOptions.size();i++)
			{
				String str = driver.findElement(By.xpath("(//*[@class='sk_formatTitleTxt'])["+i+"]")).getText();
				//System.out.println(""+str);
				if(driver.findElement(By.xpath("(//*[@class='sk_formatTitleTxt'])["+i+"]")).isDisplayed())
				{
					log.add("All Formats & Editions page displayes "+str+" options");
					pass("All Formats & Editions page displays Paperback, Hardcover, NOOK Book, Audiobook tab switching options.",log);
				}
				else
				{
					fail("All Formats & Editions page is not displays Paperback, Hardcover, NOOK Book, Audiobook tab switching options.");
				}
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA405\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA406 - Verify that all the tab switching options should display Product Name, Pub.Date, Publisher, See complete product details link, Add to Bag button*/
	public void BNIA406() throws Exception
	{
		ChildCreation("BNIA406 - Verify that all the tab switching options should display Product Name, Pub.Date, Publisher, See complete product details link, Add to Bag button");
		try
		{
			boolean PgOk = BNBasicCommonMethods.waitforElement(wait, obj, "PdpPageAllFormatList");
			if(PgOk==true)
			{
				Thread.sleep(1000);
				List<WebElement> formatSize = driver.findElements(By.xpath("//*[@class='sk_allFormatTitleCont']//*[contains(@class,'allFormatProductTitle')]"));
				for(int i = 0; i<formatSize.size();i++)
				{
					formatSize.get(i).click();
					Thread.sleep(2500);
					WebElement formatprdt = driver.findElement(By.xpath("(//*[@class='sk_allFormatContent']//*[@class='sk_allFormatDetailsCont'])["+(i+1)+"]"));
					wait.until(ExpectedConditions.attributeContains(formatprdt, "style", "block"));
					List<WebElement> prdtDesc = driver.findElements(By.xpath("(//*[@class='sk_allFormatContent']//*[@class='sk_allFormatDetailsCont'])["+(i+1)+"]//*[contains(@class,'allFormatProductDetails')]"));
					boolean formatOk = formatprdt.getAttribute("style").contains("block");
					if(formatOk==true)
					{
						for(int j = 1;j<=prdtDesc.size();j++)
						{
							WebElement prdtTtle = driver.findElement(By.xpath("((//*[@class='sk_allFormatContent']//*[@class='sk_allFormatDetailsCont'])["+(i+1)+"]//*[contains(@class,'allFormatProductDetails')]//*[contains(@class,'ProductTitleTxt')])["+j+"]"));
							BNBasicCommonMethods.scrolldown(prdtTtle, driver);
							String prdtTitle = prdtTtle.getText();
							if(prdtTitle.isEmpty())
							{
								fail("The product title is not displayed");
							}
							else
							{
								System.out.println(prdtTitle);
								pass("The product title is displayed.The displayed product title is :\n" + prdtTitle);
							}
							
							List<WebElement> prdtPublisher = driver.findElements(By.xpath("((//*[@class='sk_allFormatContent']//*[@class='sk_allFormatDetailsCont'])["+(i+1)+"]//*[contains(@class,'allFormatProductDetails')])["+j+"]//*[@class='sk_formatProductPublish']"));
							for(int k = 1; k<=prdtPublisher.size();k++)
							{
								String prdtPubDate = driver.findElement(By.xpath("(((//*[@class='sk_allFormatContent']//*[@class='sk_allFormatDetailsCont'])["+(i+1)+"]//*[contains(@class,'allFormatProductDetails')])["+j+"]//*[contains(@class,'ProductPublish')])["+k+"]")).getText();
								if(prdtPubDate.isEmpty())
								{
									fail("The publish date / details is not displayed");
								}
								else
								{
									System.out.println(prdtPubDate);
									pass("The product publish date / details is displayed.The displayed product date is :\n" + prdtPubDate);
								}
							}
							
							String completePrdtDet = driver.findElement(By.xpath("((//*[@class='sk_allFormatContent']//*[@class='sk_allFormatDetailsCont'])["+(i+1)+"]//*[contains(@class,'allFormatProductDetails')])["+j+"]//*[contains(@class,'sk_allFormatSeeFullDetail')]")).getText();
							if(completePrdtDet.isEmpty())
							{
								fail("The Complete All Format Link is not displayed");
							}
							else
							{
								System.out.println(completePrdtDet);
								pass("The Complete All Format Link is displayed.The displayed product date is :\n" + completePrdtDet);
							}
						}
					}
					else
					{
						fail("Failed to load the all format page.");
					}
				}
			}
			else
			{
				fail("The Page failed to load.");
			}
			WebElement paperback = BNBasicCommonMethods.findElement(driver, obj, "AllFormatsOverlay_PaperBackTab");
			paperback.click();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA406\n"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA407 - Verift that when See complete product details link is clicked, then it should display the product details section in the corresponding PDP page.*/
	public void BNIA407()
	{
		ChildCreation("BNIA407 - Verift that when See complete product details link is clicked, then it should display the product details section in the corresponding PDP page.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOverlay_SeeCompleteProdDetails1");
			WebElement seecompleteproddetails = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOverlay_SeeCompleteProdDetails1");
			//WebElement allformatsoverlayImg = driver.findElement(By.xpath("(//*[@class = 'sk_allFormatProductDetails sk_allFormatProductDetails_0'])[1]"));
			WebElement allformatsoverlay_atb = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOverlay_ATB1");
			String atbEANno = allformatsoverlay_atb.getAttribute("identifier");
			//System.out.println(""+atbEANno);
			//String prdImg = allformatsoverlayImg.getAttribute("src");
			//WebElement pdppage = BNBasicCommonMethods.findElement(driver, obj, "PDPContainer");
			seecompleteproddetails.click();
			Thread.sleep(2000);
			BNBasicCommonMethods.waitforElement(wait, obj, "pdp_image");
			WebElement pdpean = driver.findElement(By.xpath("//*[@id = 'id_pdpWrapper']"));
			String pdp = pdpean.getAttribute("identifier");
			//System.out.println(""+pdp);
			//WebElement PDP_Image = BNBasicCommonMethods.findElement(driver, obj, "pdp_image");
			//String src = "//prodimage.images-bn.com/pimages/9781455584987_p0_v2_s391x700-mobile.jpg";
			if(atbEANno.equals(pdp))
			{
				pass("When See complete product details link is clicked, then it displays the product details section in the corresponding PDP page.");
			}
			else
				fail("When See complete product details link is clicked, then it does not displays the product details section in the corresponding PDP page.");
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA407\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA408 - Verify that when X icon is clicked in overlay then it should navigate to prevoius page*/
	public void BNIA408() throws InterruptedException
	{
		Thread.sleep(1500);
		ChildCreation("BNIA408 - Verify that when X icon is clicked in overlay then it should navigate to prevoius page");
		
		try
		{
			Thread.sleep(2000);

			int ctr=1;
			do
			{
				try
				{
					
					BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOption");
					WebElement pdpAllformats = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOption");
					pdpAllformats.click();
					Thread.sleep(2000);
					List<WebElement> DropdownList = driver.findElements(By.xpath("//*[@class = 'sk_bn_formatOption']"));
					for(int i=1;i<=DropdownList.size();i++)
					{
							
						WebElement dropdownoptions = driver.findElement(By.xpath("(//*[@class = 'sk_bn_formatOption'])["+i+"]"));
						String drdplist = dropdownoptions.getAttribute("value");
						//System.out.println(""+drdplist);
						if(drdplist.equals("allFormat"))
						{
							dropdownoptions.click();
							i=DropdownList.size();
						
						}
						else
						{
							System.out.println("There is no All Fromats Option in the dropdown menu for the Selected product");
						}
					}
					BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOverlay");
					WebElement closeicon = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOverlay_Closeicon");
					closeicon.click();
					WebElement pdppage = BNBasicCommonMethods.findElement(driver, obj, "PDPContainer");
					if((BNBasicCommonMethods.isElementPresent(pdppage)))
					{
						pass("when X icon is clicked in overlay then it navigates to prevoius page");
						ctr=4;
						break;
					}
					else
					{
						fail("when X icon is clicked in overlay then it does not navigates to prevoius page");
					}
				}
				catch(Exception e)
				{
		        	String URL = driver.getCurrentUrl();
		        	Thread.sleep(2000);
		        	driver.get(URL);
		        	Thread.sleep(1500);
		        	ctr++;
		 		}	
			
			}while(ctr<=3);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA410\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}	
	
	/*BNIA409 - Verify that while selecting  the Format type in  PDP page,the selected format should be displayed  as Format:XXXXX*/
	private void BNIA409()
	{
		ChildCreation("BNIA409 - Verify that while selecting  the Format type in  PDP page,the selected format should be displayed  as Format:XXXXX");
		try
		{
			WebElement selectedformat = BNBasicCommonMethods.findElement(driver, obj, "PDP_SelectedFormatOption");
			String str = selectedformat.getAttribute("formattype");
			String formatoption = selectedformat.getText();
			//System.out.println(""+formatoption);
			//WebElement selectedFormatText = BNBasicCommonMethods.findElement(driver, obj, "PDP_SelectedFormatText");
			//String seltext = selectedFormatText.getText();
			String str1 = "Format: "+str;
			//System.out.println("Format: "+str);
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			//String selformattext = BNBasicCommonMethods.getExcelVal("BNIA409", sheet, 1);
			if(formatoption.equals(str1))
			{
				log.add("The Selected format is displayed as\n"+formatoption);
				pass("While selecting the Format type in PDP page,the selected format is displayed  as Format:XXXXX\n",log);
			}
			else
			{
				fail("While selecting the Format type in PDP page,the selected format is not displayed  as Format:XXXXX");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA409\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA410 - Verify that product availability either In Stock or Available Only in Online or Not Available at this time should be displayed in the product detail page*/
	public void BNIA410()
	{
		ChildCreation("BNIA410 - Verify that product availability either In Stock or Available Only in Online or Not Available at this time should be displayed in the product detail page");
		try
		{
			int ctr=1;
			do
			{
				if(!FFprdtId.isEmpty())
				{
					ctr=6;
					String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+FFprdtId;
					driver.get(PDPURL);
					/*sheet = BNBasicCommonMethods.excelsetUp("PDP");
					String EAN = BNBasicCommonMethods.getExcelNumericVal("BNIA410", sheet, 3);
					search.sendKeys(EAN);
					search.sendKeys(Keys.ENTER);*/
					try
					{
						BNBasicCommonMethods.waitforElement(wait, obj, "PDPContainer");
						BNBasicCommonMethods.waitforElement(wait, obj, "PDP_OutOfStockText");
						WebElement outofstocktext = BNBasicCommonMethods.findElement(driver, obj, "PDP_OutOfStockText");
						String outofstock_text = outofstocktext.getText();
						if(BNBasicCommonMethods.isElementPresent(outofstocktext))
						{
							log.add("The Product Availability Status is \n"+outofstock_text);
							pass("Product availability either In Stock or Available Only in Online or Not Available at this time is displayed in the product detail page",log);
						}
						else
						{
							fail("Product availability either In Stock or Available Only in Online or Not Available at this time is not displayed in the product detail page");
						}
					}
					catch(Exception e)
					{
						System.out.println("Out of Stock Text Message is not shown");
						ctr++;
					}
					
				}
				else
				{
					System.out.println("FF Product ID is not shown");
				}
			}while(ctr<=5);
			
			/*WebElement searchicon = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
			searchicon.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			WebElement search = BNBasicCommonMethods.findElement(driver, obj, "searchbox");*/
			
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA410\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	
	/*BNIA412 - Verify that selected format option for the product should be shown in the product detail page*/
	public void BNIA412()
	{
		ChildCreation("BNIA412 - Verify that selected format option for the product should be shown in the product detail page");
		try
		{
			String SearchURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/search";
			driver.get(SearchURL);
			/*WebElement search = BNBasicCommonMethods.findElement(driver, obj, "Searchtile");
			search.click();*/
			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			WebElement serbox = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
			serbox.clear();
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String EAN = BNBasicCommonMethods.getExcelNumericVal("BNIA363", sheet, 3);
			serbox.sendKeys(EAN);
			Thread.sleep(500);
			serbox.sendKeys(Keys.ENTER);
			//bckarrow.click();
			//bckarrow.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOption");
			WebElement selectedformat = BNBasicCommonMethods.findElement(driver, obj, "PDP_SelectedFormatOption");
			String str = selectedformat.getAttribute("formattype");
			//String formatoption = selectedformat.getText();
			//System.out.println(""+str);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SelectedFormatText");
			WebElement selectedFormatText = BNBasicCommonMethods.findElement(driver, obj, "PDP_SelectedFormatText");
			String seltext = selectedFormatText.getText();
			//System.out.println(""+seltext);
			if(str.equals(seltext))
			{
				pass("Selected format option for the product is shown in the product detail page");
			}
			else
			{
				fail("Selected format option for the product is not shown in the product detail page");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA412\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA413 - Verify that product price should be displayed in the product detail page*/
	public void BNIA413()
	{
		ChildCreation("BNIA413 - Verify that product price should be displayed in the product detail page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductPrice");
			WebElement actualprice = BNBasicCommonMethods.findElement(driver, obj, "PDP_SalePrice");
			WebElement saleprice = BNBasicCommonMethods.findElement(driver, obj, "PDP_ActualPrice");
			String saleprice1 = saleprice.getAttribute("pricetype");
			String sale_price = saleprice.getAttribute("price");
			//System.out.println("saleprice :"  +sale_price);
			String actualprice1 = actualprice.getAttribute("pricetype");
			String actual_price = actualprice.getAttribute("price");
			//System.out.println(""+actualprice1+ " : " +actual_price);
			if((BNBasicCommonMethods.isElementPresent(saleprice)) && (BNBasicCommonMethods.isElementPresent(actualprice)))
			{
				log.add("Sale Price of the product is \n"+saleprice1+ " : " +sale_price);
				log.add("Actual Price of the Product is \n"+actualprice1+ " : " +actual_price);
				pass("Product price is in the product detail page\n",log);
			}
			else
			{
				fail("Product price is not in the product detail page");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA413\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA414 - Verify that product price should include prefix $ symbol*/
	public void BNIA414()
	{
		ChildCreation("BNIA414 - Verify that product price should include prefix $ symbol");
		try
		{
			WebElement salepricedollar = BNBasicCommonMethods.findElement(driver, obj, "PDP_SalePriceDollar");
			if(BNBasicCommonMethods.isElementPresent(salepricedollar))
			{
				pass("product price includes prefix $ symbol");
			}
			else
			{
				fail("product price does not includes prefix $ symbol");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA414\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA415 - Verify that sale price ,original pice & save XX%  should be displayed only when the product is available in online*/
	public void BNIA415()
	{
		ChildCreation("BNIA415 - Verify that sale price ,original pice & save XX%  should be displayed only when the product is available in online");
		try
		{
			if(!TFprdtId.isEmpty())
			{
				String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+TFprdtId;
				driver.get(PDPURL);
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
				WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
				if((BNBasicCommonMethods.isElementPresent(ATB)))
				{
					log.add("The product is available in Online");
				}
				else
				{
					return;
				}
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductPrice");
				try
				{
					WebElement actualprice = BNBasicCommonMethods.findElement(driver, obj, "PDP_SalePrice");
					WebElement saleprice = BNBasicCommonMethods.findElement(driver, obj, "PDP_ActualPrice");
					WebElement savepercentage = BNBasicCommonMethods.findElement(driver, obj, "PDP_SavePercentage");
					String saleprice1 = saleprice.getAttribute("pricetype");
					String sale_price = saleprice.getAttribute("price");
					String savepercentage1 = savepercentage.getText();
					//System.out.println(""+saleprice+ " : " +sale_price);
					String actualprice1 = actualprice.getAttribute("pricetype");
					String actual_price = actualprice.getAttribute("price");
					//System.out.println(""+actualprice1+ " : " +actual_price);
					if((BNBasicCommonMethods.isElementPresent(saleprice)) && (BNBasicCommonMethods.isElementPresent(actualprice)) && (BNBasicCommonMethods.isElementPresent(savepercentage)))
					{
						log.add("Sale Price of the product is "+saleprice1+ " : " +sale_price);
						log.add("Actual Price of the Product is "+actualprice1+ " : " +actual_price);
						log.add("Save Percentage Value is "+savepercentage1);
						pass("sale price ,original pice & save XX%  should be displayed only when the product is available in online\n",log);
					}
					else
					{
						fail("Product price is not in the product detail page");
					}
				}
				catch(Exception e1)
				{
					Skip("There is no Actual price and Save Percentage for this Product");
				}
				
			}
			else
			{
				System.out.println("FT Product ID is not shown");
			}
			
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA415\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA417 - Verify that sale price of the product should be highlighted as BOLD */
	public void BNIA417()
	{
		ChildCreation("BNIA417 - Verify that sale price of the product should be highlighted as BOLD ");
		try
		{
			WebElement Saleprice = BNBasicCommonMethods.findElement(driver, obj, "PDP_SalePrice");
			String sale_price = Saleprice.getCssValue("font-size");
			String sale_price1 = Saleprice.getCssValue("font-weight");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String sale_price2 = BNBasicCommonMethods.getExcelVal("BNIA417", sheet, 9);
			if(sale_price1.equals(sale_price2))
			{
				log.add("The font size of the Sale Price is \n"+sale_price);
				log.add("The font weight is \n"+sale_price1);
				pass("Sale price of the product is highlighted as BOLD\n",log);
			}
			else
			{
				fail("Sale price of the product is not highlighted as BOLD");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA417\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA421 - Verify that Add to Bag buttton should be shown only when the product is available in Online*/
	public void BNIA421() throws Exception
	{
		ChildCreation("BNIA421 - Verify that Add to Bag buttton should be shown only when the product is available in Online");
		try
		{
			WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
			if(BNBasicCommonMethods.isElementPresent(ATB))
			{
				log.add("The Product is available in Online");
				pass("Add to Bag buttton is shown, when the product is available in Online",log);
			}
			else
			{
				fail("ATB Button is not shown");
			}
		}
		catch(Exception e)
		{
			WebElement outofstocktext = BNBasicCommonMethods.findElement(driver, obj, "PDP_OutOfStockText");
			String str1 = outofstocktext.getText();
			if(BNBasicCommonMethods.isElementPresent(outofstocktext))
			{
				pass("The Product is not available in online,\n" +str1+"\n text is shown");
			}
			else
			{
				fail("The product is not available in online");
			}
        }
	}
	
	/*BNIA422 - Verify that Add To Bag button should be shown only when the product is available in online*/
	public void BNIA422() throws Exception
	{
		ChildCreation("BNIA422 - Verify that Add To Bag button should be shown only when the product is available in online");
		try
		{
			WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
			if(BNBasicCommonMethods.isElementPresent(ATB))
			{
				log.add("The Product is available in Online");
				pass("Add to Bag buttton is shown, when the product is available in Online",log);
			}
			else
			{
				fail("ATB Button is not shown");
			}
		}
		catch(Exception e)
		{
			WebElement outofstocktext = BNBasicCommonMethods.findElement(driver, obj, "PDP_OutOfStockText");
			String str1 = outofstocktext.getText();
			if(BNBasicCommonMethods.isElementPresent(outofstocktext))
			{
				pass("The Product is not available in online,\n" +str1+ "\n text is shown");
			}
			else
			{
				fail("The product is not available in online");
			}
        }
	}
	
	/*BNIA423 - Verify that SHOW IN STORE & ADD TO BAG buttons should hidden when the product out of stock (Not Available at this time)*/
	public void BNIA423()
	{
		ChildCreation("BNIA423 - Verify that SHOW IN STORE & ADD TO BAG buttons should hidden when the product out of stock (Not Available at this time)");
		try
		{
			
			if(!FFprdtId.isEmpty())
			{
				String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+FFprdtId;
				driver.get(PDPURL);
				try
				{
					BNBasicCommonMethods.waitforElement(wait, obj, "PDP_OutOfStockText");
					WebElement outofstocktext = BNBasicCommonMethods.findElement(driver, obj, "PDP_OutOfStockText");
					String outofstock_text = outofstocktext.getText();
					if(BNBasicCommonMethods.isElementPresent(outofstocktext))
					{
						log.add("Show in Store & Add to Bag buttons is not shown instead  \n"+outofstock_text+ "\n text is shown");
						pass("SHOW IN STORE & ADD TO BAG buttons is hidden when the product out of stock (Not Available at this time)");
					}
					else
					{
						fail("SHOW IN STORE & ADD TO BAG buttons is not hidden when the product out of stock (Not Available at this time)");
					}
				}
				catch(Exception e)
				{
					System.out.println("Not Available at this Time Text is not Shown");
				}
			}
				
			else
			{
				System.out.println("FF Product ID is not shown");
			}
			
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA423\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA424 - Verify that market place from XXXX should be shown below to ADD TO BAG button*/
	public void BNIA424()
	{
		ChildCreation("BNIA424 - Verify that market place from XXXX should be shown below to ADD TO BAG button");
		try
		{
			if(!TFprdtId.isEmpty())
			{
				String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+TFprdtId;
				driver.get(PDPURL);
				try
				{
					BNBasicCommonMethods.waitforElement(wait, obj, "Marketplace");
					WebElement marketplace = BNBasicCommonMethods.findElement(driver, obj, "Marketplace");
					//String marketplacetext = marketplace.getText();
					WebElement marketplaceprice = BNBasicCommonMethods.findElement(driver, obj, "MarketplacePrice");
					String marketplace_price = marketplaceprice.getText();
					if((BNBasicCommonMethods.isElementPresent(marketplace)) && (BNBasicCommonMethods.isElementPresent(marketplaceprice)))
					{
						log.add("+marketplacetext+\n"+marketplace_price);
						pass("Market place from XXXX is shown below to ADD TO BAG button",log);
					}
					else
					{
						fail("Market place from XXXX is not shown below to ADD TO BAG button");
					}
				}
				catch(Exception e)
				{
					System.out.println("Market Place Link is not available for the\n"+TTprdtId+ "\nEAN Number" );
				}
			}
			else
			{
				System.out.println("TF Product ID is not Shown");
			}
			
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA424\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA425 - Verify that on Selecting ADD TO BAG button,it shows success alert in the overlay*/
	public void BNIA425() throws Exception
	{
		ChildCreation("BNIA425 - Verify that on Selecting ADD TO BAG button,it shows success alert in the overlay");
		try
		{
			if(!TTprdtId.isEmpty())
			{
				int k=1;
				do
				{
					String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+TTprdtId;
					driver.get(PDPURL);
					((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
					((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
					((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
					Thread.sleep(2500);
					
					try
					{
						Thread.sleep(3000);
						//BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AlsoAvailableOnlineTextLink");
						WebElement AlsoAvailOnlineText = BNBasicCommonMethods.findElement(driver, obj, "PDP_AlsoAvailableOnlineTextLink");
						Actions act = new Actions(driver);
						for(int ctr=0 ; ctr<5 ;ctr++)
						  {
						  	act.sendKeys(Keys.CONTROL).sendKeys(Keys.ARROW_DOWN).build().perform();
						  }
						AlsoAvailOnlineText.click();
						k=11;
						BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
						WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
						int ctr=1;
						do
						{
							if(ATB.isDisplayed())
							{
								ctr=4;
								ATB.click();
								try
								{
									Thread.sleep(1500);
									//ctr=4;
									try
									{
										BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay");
										WebElement successoverlay = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay");
										WebElement successoverlay_continuebtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ContinueButton");
										WebElement successoverlay_viewbagbtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ViewBagButton");
										if(BNBasicCommonMethods.isElementPresent(successoverlay))
										{
											log.add("On Selecting ADD TO BAG button,it shows success alert in the overlay");
											if((BNBasicCommonMethods.isElementPresent(successoverlay_continuebtn)) && (BNBasicCommonMethods.isElementPresent(successoverlay_viewbagbtn)))
											{
												log.add("In Success Overlay, Continue Button & View Bag Button is Shown");
												pass("On Selecting ADD TO BAG button,it shows success alert in the overlay",log);
											}
											else
											{
												fail("In Success Overlay, Continue Button & View Bag Button is not Shown");
											}
										}
										else
										{
											fail("On Selecting ADD TO BAG button,it does not shows success alert in the overlay");
										}
									}
									catch(Exception e)
									{
										WebElement nookfailureAlert = BNBasicCommonMethods.findElement(driver, obj, "PDP_NookFailureAlertOverlay");
										if(BNBasicCommonMethods.isElementPresent(nookfailureAlert))
										{
											log.add("Only one Nook Product can able to add in the shopping bag");
											fail("On Selecting ADD TO BAG button,it does not shows success alert in the overlay",log);
										}
									}
									
								}
								catch(Exception e)
								{
									WebElement ErrorAlertOverlay = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATBclick_ErrorAlert");
									if(ErrorAlertOverlay.isDisplayed())
									{
										WebElement ErrorAlertOkButton = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATBclick_ErrorAlertOkButton");
										ErrorAlertOkButton.click();
										driver.getCurrentUrl();
									}
								}
							}
							else
							{
								System.out.println("ATB button is not shown");
							}
						}while(ctr<=3);
					}
					catch(Exception e)
					{
						System.out.println("ATB link is not shown");
						k++;
						/*WebElement nookfailureAlert = BNBasicCommonMethods.findElement(driver, obj, "PDP_NookFailureAlertOverlay");
						if(BNBasicCommonMethods.isElementPresent(nookfailureAlert))
						{
							log.add("Only one Nook Product can able to add in the shopping bag");
							fail("On Selecting ADD TO BAG button,it does not shows success alert in the overlay",log);
						}*/
			        }
				}while(k<=10);
				
			}
			else
			{
				System.out.println("TT Product ID is not Shown");
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA425\n"+e.getMessage());
 			exception(e.getMessage());
		}
		
		
	}
	
	/*BNIA426 - Verify that Add to bag overlay should display product image, product title, author name,format option & selling price along with Continue Shopping & View Bag button*/
	public void BNIA426()
	{
		ChildCreation("BNIA426 - Verify that Add to bag overlay should display product image, product title, author name,format option & selling price along with Continue Shopping & View Bag button");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay");
			WebElement successoverlay = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay_ContinueButton");
			WebElement successoverlay_continuebtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ContinueButton");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay_ViewBagButton");
			WebElement successoverlay_viewbagbtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ViewBagButton");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay_PrdImage");
			WebElement PrdImage = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_PrdImage");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay_PrdTitle");
			WebElement PrdTitle = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_PrdTitle");
			//WebElement PrdAuthorName = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_PrdAuthorName");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay_PrdFormat");
			WebElement PrdFormat = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_PrdFormat");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay_PrdPrice");
			WebElement PrdPrice = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_PrdPrice");
			boolean continuebutton = false;
			continuebutton = successoverlay_continuebtn.isDisplayed();
			boolean viewbagbutton = false;
			viewbagbutton = successoverlay_viewbagbtn.isDisplayed();
			boolean image = false;
			image = PrdImage.isDisplayed();
			boolean title = false;
			title = PrdTitle.isDisplayed();
			/*boolean authorname = false;
			authorname = PrdAuthorName.isDisplayed();*/
			boolean Prdformat = false;
			Prdformat = PrdFormat.isDisplayed();
			boolean price = false;
			price = PrdPrice.isDisplayed();
			if(BNBasicCommonMethods.isElementPresent(successoverlay))
			{
				log.add("Add to Bag Success Overlay is shown");
				if((image) && (title))
				{
					log.add("Product Image, Product Title, is shown in the ATB Success Overlay");
					if((Prdformat) & (price))
					{
						log.add("Product Format, Product Price is Shown in the ATB Success Overlay");
						if((continuebutton) && (viewbagbutton))
						{
							log.add("Continue Button & VIew Bag Button is shown in the ATB Overlay");
							pass("Add to bag overlay displays product image, product title, author name,format option & selling price along with Continue Shopping & View Bag button",log);
						}
						else
						{
							 fail("Add to bag overlay does not displays product image, product title, author name,format option & selling price along with Continue Shopping & View Bag button");
						}
					}
					else
					{
						fail("Product Format and Price is not shown");
					}
				}
				else
				{
					fail("Image, Title, is not shown");
				}
			}
			else
			{
				fail("ATB Success Overlay is not shown");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA426\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA427 - Verify that View Bag button should be highlighted in RED color */
	public void BNIA427()
	{
		ChildCreation("BNIA427 - Verify that View Bag button should be highlighted in RED color");
		try
		{
			WebElement successoverlay_viewbagbtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ViewBagButton");
			String viewbag = successoverlay_viewbagbtn.getCssValue("background-color");
			Color viewbagcolors = Color.fromString(viewbag);
			String hexviewbagcolors = viewbagcolors.asHex();
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String viewbagColor = BNBasicCommonMethods.getExcelVal("BNIA427", sheet, 6);
			if(hexviewbagcolors.equals(viewbagColor))
			{
				log.add("The hexa value of RED color is \n"+viewbag);
				pass("View Bag button is highlighted in RED color",log);
			}
			else
			{
				fail("View Bag button is not highlighted in RED color");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA427\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA428 - Verify that while tapping continue shopping button the overlay should be closed and it remains in PDP page */
	public void BNIA428()
	{
		ChildCreation("BNIA428 - Verify that while tapping continue shopping button the overlay should be closed and it remains in PDP page ");
		try
		{
			WebElement successoverlay_continuebtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ContinueButton");
			successoverlay_continuebtn.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
			WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
			if(BNBasicCommonMethods.isElementPresent(ATB))
			{
				pass("While tapping continue shopping button, the overlay closed and it remains in PDP page");
			}
			else
			{
				fail("While tapping continue shopping button, the overlay does not get closed");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA428\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA429 - Verify that while tapping View bag button,it should shown shopping bag*/
	public void BNIA429() throws Exception
	{
		ChildCreation("BNIA429 - Verify that while tapping View bag button,it should shown shopping bag");
		try
		{
			WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
			ATB.click();
			try
			{
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay");
				WebElement successoverlay_viewbagbtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ViewBagButton");
				successoverlay_viewbagbtn.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "ShoppingBag_OrderSummary");
				WebElement  shoppingbag = BNBasicCommonMethods.findElement(driver, obj, "ShoppingBag_OrderSummary");
				if(BNBasicCommonMethods.isElementPresent(shoppingbag))
				{
					pass("While tapping View bag button,it shows shopping bag");
				}
				else
				{
					fail("While tapping View bag button,it does not shows shopping bag");
				}
				System.out.println("1/2 - BNIA08_PDP_Page is completed");
			}
			catch(Exception e1)
			{
				WebElement ErrorAlert = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATBclick_ErrorAlert");
				if(ErrorAlert.isDisplayed())
				{
					//System.out.println("Rental product cannot be added twice");
					sheet = BNBasicCommonMethods.excelsetUp("PDP");
					String EAN = BNBasicCommonMethods.getExcelNumericVal("BNIA363", sheet, 3);
					String URL = "http://"+"bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+EAN;
					driver.get(URL);
					Thread.sleep(2000);
					/*WebElement ErrorOk = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATBclick_ErrorAlertOkButton");
					ErrorOk.click();*/
					
					//Skip("This item cannot be added to the cart");
				}
			}
			
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA429\n"+e.getMessage());
 			exception(e.getMessage());
 		}
		
		BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
		WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
		BNLogo.click();
		Thread.sleep(1000);
	}
	
	/*BNIA430 - Verify that while tapping Market place button Market place new and used overlay should be shown*/
	public void BNIA430()
	{
		ChildCreation("BNIA430 - Verify that while tapping Market place button Market place new and used overlay should be shown");
		try
		{
			WebElement marketplacelink = BNBasicCommonMethods.findElement(driver, obj, "Marketplace");
			marketplacelink.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "MarketplaceOverlay");
			WebElement marketplace_overlay = BNBasicCommonMethods.findElement(driver, obj, "MarketplaceOverlay");
			if(BNBasicCommonMethods.isElementPresent(marketplace_overlay))
			{
				pass("While tapping Market place button Market place new and used overlay is shown");
			}
			else
			{
				fail("While tapping Market place button Market place new and used overlay is not shown");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA430\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA431 - Verify that  Market place new and used title should be highlighted by bold*/
	public void BNIA431()
	{
		ChildCreation("BNIA431 - Verify that  Market place new and used title should be highlighted by bold");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String ExpectedFontName = BNBasicCommonMethods.getExcelVal("BNIA431", sheet, 4);
			WebElement marketplacetext = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_Title");
			String title = marketplacetext.getCssValue("font-family");
			//System.out.println(title);
			String[] title1 = title.split(",");
			if(title1[0].equals(ExpectedFontName))
			{
				log.add("Expected font name : \n"+ExpectedFontName);
				log.add("Current font name : \n"+title1[0]);
				pass("Market place new and used title is highlighted by bold",log);
			}
			else
			{
				log.add("Expected font name : \n"+ExpectedFontName);
				log.add("Current font name : \n"+title1[0]);
				pass("Market place new and used title is not highlighted by bold",log);
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA431\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA432 - Verify that Note description should be displayed below the Market place new and used text*/
	public void BNIA432()
	{
		ChildCreation("BNIA432 - Verify that Note description should be displayed below the Market place new and used text");
		try
		{
			WebElement marketplace_notetext1 = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_NoteText");
			/*WebElement marketplace_notetext = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_NoteText1");
			String noteText = marketplace_notetext.getText();*/
			//String notetext1 = marketplace_notetext1.getText();
			if(BNBasicCommonMethods.isElementPresent(marketplace_notetext1))
			{
				log.add("+notetext1+");
				pass("Note description is displayed below the Market place new and used text",log);
			}
			else
			{
				fail("Note description is not displayed below the Market place new and used text");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA432\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA433 - Verify that product name and author name should be displayed below Note in  the overlay*/
	public void BNIA433()
	{
		ChildCreation("BNIA433 - Verify that product name and author name should be displayed below Note in  the overlay");
		try
		{
			WebElement title = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_PrdTitle");
			WebElement authorname = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_AuthorName");
			String Title = title.getText();
			String Authorname = authorname.getText();
			if((BNBasicCommonMethods.isElementPresent(title)) && (BNBasicCommonMethods.isElementPresent(authorname)))
			{
				log.add("The Product Title and the Author Name is \n"+Title +"\n" +Authorname);
				pass("Product name and author name is displayed below Note in the overlay",log);
			}
			else
			{
				fail("Product name and author name is not displayed below Note in the overlay");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA433\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA434 - Verify that while tapping review link,it should navigate to review section in the product detail page*/
	public void BNIA434()
	{
		ChildCreation("BNIA434 - Verify that while tapping review link,it should navigate to review section in the product detail page");
		try
		{
			WebElement reviewlink = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ReviewLink");
			reviewlink.click();
			try
			{
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerReviewsSection");
				WebElement customerreviews = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsSection");
				if(BNBasicCommonMethods.isElementPresent(customerreviews))
				{
					pass("While tapping review link,it navigates to review section in the product detail page");
				}
				else
				{
					fail("While tapping review link,it does not navigates to review section in the product detail page");
				}
				WebElement marketplacelink = BNBasicCommonMethods.findElement(driver, obj, "Marketplace");
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView(true);",marketplacelink);
				marketplacelink.click();
			}
			catch(Exception e)
			{
				System.out.println("O Reviews is available for the selected product");
				WebElement marketplacelink = BNBasicCommonMethods.findElement(driver, obj, "Marketplace");
				marketplacelink.click();
				Skip("O Reviews is available for the selected product");
			}
			
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA434\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA435 - Verify that all,new,used tab switching with product count should be displayed below the product title */
	public void BNIA435()
	{
		ChildCreation("BNIA435 - Verify that all,new,used tab switching with product count should be displayed below the product title ");
		try
		{
			Thread.sleep(2500);
			List<WebElement> MarketPlaceTabs = driver.findElements(By.xpath("//*[@class = 'sk_mkt_rollUpTitleTxt']"));
			for(int i=1;i<=MarketPlaceTabs.size();i++)
			{
				//BNBasicCommonMethods.waitforElement(wait, obj, "tabs1");
				WebElement tabs1 = driver.findElement(By.xpath("(//*[@class='sk_mkt_rollUpTitleTxt'])["+i+"]"));
				                                               // (//*[@class='sk_mkt_rollUpTitleTxt'])
				String tabs = tabs1.getText();
				//System.out.println(""+tabs);
				if(driver.findElement(By.xpath("(//*[@class='sk_mkt_rollUpTitleTxt'])["+i+"]")).isDisplayed())
				{
					log.add(" " +tabs +"tab is shown");
					pass("All,New,Used tab switching with product count is displayed below the product title",log);
				}
				else
				{
					fail("All,New,Used tab switching with product count is displayed below the product title");
				}
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA435\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA436 - Verify that Product Sorting dropdown should be displayed with various sorting option(Eg: low to high,high to low,popularity,newly added)*/
	public void BNIA436()
	{
		ChildCreation("BNIA436 - Verify that Product Sorting dropdown should be displayed with various sorting option(Eg: low to high,high to low,popularity,newly added)");
		try
		{
			WebElement dropdown = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_Dropdown");
			dropdown.click();
			Thread.sleep(500);
			WebElement dropdown_Option1 = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_Dropdown_Option1");
			WebElement dropdown_option2 = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_Dropdown_Option2");
			WebElement dropdown_option3 = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_Dropdown_Option3");
			WebElement dropdown_option4 = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_Dropdown_Option4");
			String Option1 = dropdown_Option1.getText();
			String Option2 =dropdown_option2.getText();
			String Option3 =dropdown_option3.getText();
			String Option4 =dropdown_option4.getText();
			
			
				if((dropdown_Option1.isDisplayed()) && (dropdown_option2.isDisplayed()))
				{
					log.add("Dropdown options shown are \n"+Option1+"\n" +Option2);
					if((dropdown_option3.isDisplayed()) && (dropdown_option4.isDisplayed()))
					{
						
					log.add("Dropdown options shown are \n"+Option3+ "\n" + Option4);
					pass("Product Sorting dropdown is displayed with various sorting option(Eg: low to high,high to low,popularity,newly added)",log);
					}
					else
					{
						return;
					}
				}
				else
				{
					fail("Product Sorting dropdown is not displayed with various sorting option(Eg: low to high,high to low,popularity,newly added)");
				}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA436\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA437 - Verify that product sorting dropdown should be selected in Price low to  High option  by default*/
	public void BNIA437()
	{
		ChildCreation("BNIA437 - Verify that product sorting dropdown should be selected in Price low to  High option  by default");
		try
		{
			WebElement dropdown = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_Dropdown");
			String dropdwonText = dropdown.getText();
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String dropdownDefText = BNBasicCommonMethods.getExcelVal("BNIA437", sheet, 1);
			if(dropdwonText.equals(dropdownDefText))
			{
				log.add("The Default Text in the Dropdown is \n"+dropdwonText);
				pass("Product sorting dropdown is selected in Price low to  High option by default",log);
			}
			else
			{
				fail("Product sorting dropdown is not selected in Price low to  High option by default");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA437\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	

	/*BNIA438 - Verify that listed product should contains product price,name of the seller,seller since,rating,review count,review link,condition, seller details,shipping option with + icon and add to bag button with price*/
	public void BNIA438()
	{
		ChildCreation("BNIA438 - Verify that listed product should contains product price,name of the seller,seller since,rating,review count,review link,condition, seller details,shipping option with + icon and add to bag button with price");
		
	 try
		{
		 Thread.sleep(1500);
		/* String pdp = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam=9780316407021";
		 driver.get(pdp);
		 Thread.sleep(3000);
		 driver.get(pdp);
		 Thread.sleep(3000);
		 BNBasicCommonMethods.findElement(driver, obj, "Marketplace").click();
			//boolean PgOk = BNBasicCommonMethods.waitforElement(wait, obj, "PdpPageAllFormatList");
*/			if(true)
			{
				Thread.sleep(1000);
				List<WebElement> MarketPlaceText = driver.findElements(By.xpath("//*[@class='sk_allRollupTitleCont']//*[contains(@class,'sk_allRollupCondTitle')]"));
				for(int i = 0; i<MarketPlaceText.size();i++)
				{
					MarketPlaceText.get(i).click();
					Thread.sleep(2500);
					
					WebElement formatprdt = driver.findElement(By.xpath("(//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(i+1)+"]"));
					wait.until(ExpectedConditions.attributeContains(formatprdt, "style", "block"));
					List<WebElement> prdtDesc = driver.findElements(By.xpath("(//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(i+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]"));
					//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent']//*[contains(@class,'sk_bn_mktSingleItemCont')]
					boolean ProductOk = formatprdt.getAttribute("style").contains("block");
					int ProductCounter = prdtDesc.size();
					if(ProductOk==true)
					{
						if(prdtDesc.size()>5)
						{
							ProductCounter = 5;
						}
						for(int j = 1;j<=ProductCounter;j++)
						{
							WebElement ProductLeftPricedetails = driver.findElement(By.xpath("((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(i+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktLeftCont')])["+j+"]"));
																																																				
							
							BNBasicCommonMethods.scrolldown(ProductLeftPricedetails, driver);
							String prdtTitle = ProductLeftPricedetails.getText();
							if(prdtTitle.isEmpty())
							{
								fail("The product title is not displayed");
							}
							else
							{
								System.out.println(prdtTitle.toString());
								log.add("Product Price With Description  : "+prdtTitle);
							}
							
							WebElement ProductCenterdetails = driver.findElement(By.xpath("((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(i+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+j+"]"));
							BNBasicCommonMethods.scrolldown(ProductLeftPricedetails, driver);
							String prdtTitle1 = ProductCenterdetails.getText();
							
							if(prdtTitle1.isEmpty())
							{
								fail("The product title is not displayed");
							}
							else
							{
								System.out.println(prdtTitle1);
								log.add("Product Condition descrption with Read More Link : "+prdtTitle1);
							}
							
							WebElement ProductLeftdetails = driver.findElement(By.xpath("((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(i+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktRightCont')])["+j+"]"));
							BNBasicCommonMethods.scrolldown(ProductLeftPricedetails, driver);
							String prdtTitle2 = ProductLeftdetails.getText();
							if(prdtTitle2.isEmpty())
							{
								fail("The product title is not displayed");
							}
							else
							{
								System.out.println(prdtTitle2);
								log.add("Add to Bag button with Price details "+prdtTitle2);
							}
							
						}
						
						pass("Product Details In Market Place OVerlay",log);
					}
					else
					{
						fail("Failed to load the all format page.");
					}
				}
			}
			
			
		}
		catch(Exception e)
		{
			System.out.println("Something went wrong in BNIA406\n"+e.getMessage());
			exception(e.getMessage());
		}
   }
	
	
	
	/*BNIA440 - Verify that price should be highlighted in Bold for the listed products.*/
	/*BNIA441 - Verify that product condition should be shown with description*/
	/*BNIA442 - Verify that product condition description is lengthy then it should be shown in  ellipses with  Read more link and down arrow*/
	/*BNIA443 - Verify that while tapping Read more link that should shown more about description */
	/*BNIA444 - Verify that + icon  should be shown for shipping option */
	/*BNIA445 - Verify that while tapping "+" icon that should display shipping options in tooltip-container*/
	/*BNIA446 - Verify that + icon should be highlighted(greyed out) while displaying shipping options in tooltip-container*/
	/*BNIA448 - Verify that listed products should be scrolled vertically*/
	public void BNIA440() throws Exception
	{
		
		/*String pdp = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam=9780316407021";
		driver.get(pdp);
		Thread.sleep(3000);
		driver.get(pdp);
		Thread.sleep(3000);
		BNBasicCommonMethods.findElement(driver, obj, "Marketplace").click();*/
		Thread.sleep(1000);
		Random r = new Random();
		List<WebElement> MarketPlaceText = driver.findElements(By.xpath("//*[@class='sk_allRollupTitleCont']//*[contains(@class,'sk_allRollupCondTitle')]"));
		int sel = r.nextInt(MarketPlaceText.size());
      	MarketPlaceText.get(sel).click();
		   Thread.sleep(2500);
			WebElement formatprdt = driver.findElement(By.xpath("(//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]"));
			wait.until(ExpectedConditions.attributeContains(formatprdt, "style", "block"));
			List<WebElement> prdtDesc = driver.findElements(By.xpath("(//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]"));
			//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent']//*[contains(@class,'sk_bn_mktSingleItemCont')]
			//boolean ProductOk = formatprdt.getAttribute("style").contains("block");
			//int ProductCounter = prdtDesc.size();
			int productsel = r.nextInt(prdtDesc.size());
			if(productsel<1)
			{
				productsel = 1;
			}
			//WebElement ProductCondition = driver.findElement(By.xpath("((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktLeftCont')])["+productsel+"]"));
			WebElement ProductConditionScroll = driver.findElement(By.xpath("((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktLeftCont')])["+(productsel)+"]"));
				/*BNIA440 - Verify that price should be highlighted in Bold for the listed products.*/	
				ChildCreation("BNIA440 - Verify that price should be highlighted in Bold for the listed products.");	
				try
				{
					WebElement price = driver.findElement(By.xpath("(((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktLeftCont')])["+productsel+"]//*[@class = 'sk_bn_mktPdtPrice'])"));
					String pricesize = price.getCssValue("font-size");
					sheet = BNBasicCommonMethods.excelsetUp("PDP");
					String Expfont = BNBasicCommonMethods.getExcelVal("BNIA440", sheet, 4);
					String priceFont = price.getCssValue("font-family");
					String[] pricefontsplit = priceFont.split(",");
					String pricefontsize = BNBasicCommonMethods.getExcelVal("BNIA440", sheet, 5);
					if((pricefontsplit[0].equals(Expfont)) && (pricesize.equals(pricefontsize)))
					{
						log.add("Current font name and font size is : /n"+pricefontsplit[0]+ "\n"+pricesize);
						pass("Price is highlighted in Bold for the listed products.",log);
					}
					else
					{
						fail("Price is not highlighted in Bold for the listed products.",log);
					}
				}
				catch(Exception e)
				{
		        	System.out.println(e.getMessage());
		 			System.out.println("Something went wrong in BNIA440\n"+e.getMessage());
		 			exception(e.getMessage());
		 		}
			
				/*BNIA442 - Verify that product condition description is lengthy then it should be shown in  ellipses with  Read more link and down arrow*/
				ChildCreation("BNIA442 - Verify that product condition description is lengthy then it should be shown in  ellipses with  Read more link and down arrow");
				try
				{
						try
						{
							
							WebElement ShortDesc = driver.findElement(By.xpath("(((//*[@class='sk_marketPlaceCont']//*[@class = 'sk_marketPlaceContent'])[1]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+productsel+"]//*[@class = 'sk_bn_pdtReviewComment'])//*[@class = 'sk_bn_mktReviewVal']"));
							BNBasicCommonMethods.scrolldown(ShortDesc, driver);
							String elipses = ShortDesc.getText();
							WebElement readmorelink = driver.findElement(By.xpath("(((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])[1]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])[1]//*[@class = 'sk_bn_mktReviewVal'])//*[@class = 'sk_bn_readMoreReviewArr']"));
							BNBasicCommonMethods.scrolldown(readmorelink, driver);
							String link = readmorelink.getText();
							//System.out.println(link);
							if((BNBasicCommonMethods.isElementPresent(ShortDesc)) && (BNBasicCommonMethods.isElementPresent(readmorelink)))
							{
								log.add("The Elipses and Read More link is shown, when the description is lengthy\n" +elipses+ "\n");
								pass("Product condition description is lengthy then it showns in  ellipses with Read more link and down arrow",log);
							}
							else
							{
								fail("Product condition description is lengthy then it does not showns in  ellipses with Read more link and down arrow");
							}
						}
						catch(Exception e)
						{
							System.out.println("Description is not shown for this product");
							try
							{
								WebElement readmorelink = driver.findElement(By.xpath("(((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+productsel+"]//*[@class = 'sk_bn_mktReviewVal'])"));
								String link = readmorelink.getText();
								pass("readmore link is shown and elipses are available"+link);
							}
							catch(Exception e1)
							{
								System.out.println("Read more link is not shown for this product");
								Skip("Read more Link and elipses is not shown for this product");
							}
							
						}
				}
				catch(Exception e)
				{
		        	System.out.println(e.getMessage());
		 			System.out.println("Something went wrong in BNIA442\n"+e.getMessage());
		 			exception(e.getMessage());
		 		}
		
		
				/*BNIA441 - Verify that product condition should be shown with description*/
				ChildCreation("BNIA441 - Verify that product condition should be shown with description");
				desc = false;
				try
				{	
						try
						{
						WebElement readmore = driver.findElement(By.xpath("((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+productsel+"]//*[@class='sk_bn_readMoreReview']"));
						readmore.click();
						String ConditionDesc = driver.findElement(By.xpath("((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+productsel+"]//*[@class = 'sk_bn_mktReviewVal']")).getText();
						//System.out.println(" "+ConditionDesc);
						if(!ConditionDesc.isEmpty())
						{
							log.add("The Seller Name is \n"+ConditionDesc);
							pass("Product condition is shown with description",log);
							desc=true;
						}
						else
						{
							fail("The seller Name is not shown");
							desc = false;
						}
						}
						catch(Exception e)
						{
							Skip("Read more link is not shown for this product");
							log.add("readmore link is not available");
							try
							{
								String ConditionDesc = driver.findElement(By.xpath("((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+productsel+"]//*[@class = 'sk_bn_mktReviewVal']")).getText();
								//System.out.println(""+ConditionDesc);
								if(desc)
								{
									log.add("The Seller Name is \n"+ConditionDesc);
									pass("Product condition is shown with description",log);
									desc=true;
								}
								else
								{
									fail("The seller Name is not shown");
									desc = false;
								}
							}
							catch(Exception ee)
							{
								System.out.println("Condition is not shown for this product");
								Skip("product condition is not shown with description for this product");
							}
							
						}
				  }     
				  catch(Exception e)
				  {
				        System.out.println(e.getMessage());
				 		System.out.println("Something went Wrong in BNIA441 "+e.getMessage());
				 		exception(e.getMessage());
				 		desc = false;
				 }
				
				
				/*BNIA443 - Verify that while tapping Read more link that should shown more about description */
				ChildCreation("BNIA443 - Verify that while tapping Read more link that should shown more about description ");
				try
				{
					if(desc == true)
					{
						pass("while tapping Read more link it shows more about the description");
					}
					else
					{
						Skip("Read more link is not shown");
						//fail("while tapping Read more link it does not shows more about the description");
					}
				}
				catch(Exception e)
				{
					System.out.println(e.getMessage());
		 			System.out.println("Something went wrong in BNIA443\n"+e.getMessage());
		 			exception(e.getMessage());
		        }
				
				/*BNIA444 - Verify that + icon  should be shown for shipping option */
				ChildCreation("BNIA444 - Verify that + icon  should be shown for shipping option ");
				try
				{
					WebElement plusicon1 = driver.findElement(By.xpath("(((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+productsel+"]//*[@class='sk_bn_mktShippingIcon'])"));
					if(BNBasicCommonMethods.isElementPresent(plusicon1))
					{
						log.add("Plus icon is shown");
						pass("+ icon is shown for shipping option",log);
					}
					else
					{
						fail("+ icon is not shown for shipping option");
					}
				}
				catch(Exception e)
				{
		        	System.out.println(e.getMessage());
		 			System.out.println("Something went wrong in BNIA444\n"+e.getMessage());
		 			exception(e.getMessage());
		 		}
				
				/*BNIA445 - Verify that while tapping "+" icon that should display shipping options in tooltip-container*/
				ChildCreation("BNIA445 - Verify that while tapping "+" icon that should display shipping options in tooltip-container");
				try
				{
					WebElement plusicon1 = driver.findElement(By.xpath("(((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+productsel+"]//*[@class='sk_bn_mktShippingIcon'])"));
					plusicon1.click();
					//*[@class='sk_bn_mktSingleItemCont Used shippingOptEnabled']
					WebElement plusActive = driver.findElement(By.xpath("//*[@class='sk_bn_mktShippingIcon activeShippingIcon']"));
					if(plusActive.isDisplayed())
					{
						log.add("Tooltip Container is shown");
						pass("While tapping + icon it displays shipping options in tooltip-container",log);
					}
					else
					{
						fail("While tapping + icon it does not displays shipping options in tooltip-container");
					}
				}
				catch(Exception e)
				{
		        	System.out.println(e.getMessage());
		 			System.out.println("Something went wrong in BNIA444 : "+e.getMessage());
		 			exception(e.getMessage());
		 		}
				
				/*BNIA446 - Verify that + icon should be highlighted(greyed out) while displaying shipping options in tooltip-container*/
				ChildCreation("BNIA446 - Verify that + icon should be highlighted(greyed out) while displaying shipping options in tooltip-container");
				try
				{
					/*WebElement plusicon1 = driver.findElement(By.xpath("(((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+productsel+"]//*[@class='sk_bn_mktShippingIcon'])"));
					plusicon1.click();*/
					//*[@class='sk_bn_mktSingleItemCont Used shippingOptEnabled']
					WebElement plusActive = driver.findElement(By.xpath("//*[@class='sk_bn_mktShippingIcon activeShippingIcon']"));
					String activepluscolor = plusActive.getCssValue("background-position");
					sheet = BNBasicCommonMethods.excelsetUp("PDP");
					String color = BNBasicCommonMethods.getExcelVal("BNIA446", sheet, 5);
					if(activepluscolor.equals(color))
					{
						log.add("The Active Plus Icon Color is\n"+activepluscolor);
						pass("+ icon is highlighted(greyed out) while displaying shipping options in tooltip-container",log);
					}
					else
					{
						fail("+ icon is not highlighted(greyed out) while displaying shipping options in tooltip-container");
					}
				}
				catch(Exception e)
				{
		        	System.out.println(e.getMessage());
		 			System.out.println("Something went wrong in BNIA446\n"+e.getMessage());
		 			exception(e.getMessage());
		 		}
				
				/*BNIA448 - Verify that listed products should be scrolled vertically*/
				ChildCreation("BNIA448 - Verify that listed products should be scrolled vertically");
				try
				{
				
				     if(ProductConditionScroll.isDisplayed())
				     {
				    	 pass("Market place listed products are scrolling vertically"); 
				     }
				     
				     else
				     {
				    	 fail("Market palce listed products are not scrolling vertically");
				     }
				}
				catch(Exception e)
				{
			    	System.out.println(e.getMessage());
					System.out.println("Something went wrong in BNIA448\n"+e.getMessage());
					exception(e.getMessage());
				}
	}
	
	/*BNIA447 - Verify that while selecting the All,New and Used sections in the Marketplace overlay,the respective tab should be displayed */
	/*public void BNIA447()
	{
		ChildCreation("BNIA447 - Verify that while selecting the All,New and Used sections in the Marketplace overlay,the respective tab should be displayed ");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "MarketplaceOverlay");
			Thread.sleep(1000);
			try
			{
				
				/*AllTab.click();
				Thread.sleep(1000);
				BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveAllTab");
			    WebElement AllTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveAllTab");
			    WebElement NewTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_NewTab");
			    WebElement UsedTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_UsedTab");
				String alltab = AllTabActive.getText();
				WebElement Details = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_TabDetails");
				if(BNBasicCommonMethods.isElementPresent(AllTabActive) && BNBasicCommonMethods.isElementPresent(Details))
				{
					log.add("The Active Tab is\n"+alltab);
				}
				else
				{
					return;
				}
				NewTab.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveNewTab");
				WebElement NewTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveNewTab");
				String newtab = NewTabActive.getText();
				if((BNBasicCommonMethods.isElementPresent(NewTabActive)) && (BNBasicCommonMethods.isElementPresent(Details)))
				{
					log.add("The Active Tab is \n"+newtab);
				}
				else
				{
					return;
				}
				UsedTab.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveUsedTab");
				WebElement UsedTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveUsedTab");
				String usedtab = UsedTabActive.getText();
				if(BNBasicCommonMethods.isElementPresent(UsedTabActive) && (BNBasicCommonMethods.isElementPresent(Details)))
				{
					log.add("The Active tab is\n"+usedtab);
					pass("While selecting the All,New and Used sections in the Marketplace overlay,the respective tab is displayed",log);
				}
				else
				{
					fail("While selecting the All,New and Used sections in the Marketplace overlay,the respective tab is not displayed");
				}
				WebElement AllTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_AllTab");
				AllTab.click();
			}
			catch(Exception e1)
			{
				try
				{
					BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveNewTab");
					WebElement NewTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveNewTab");
					WebElement Details = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_TabDetails");
					String newtab = NewTabActive.getText();
					if((BNBasicCommonMethods.isElementPresent(NewTabActive)) && (BNBasicCommonMethods.isElementPresent(Details)))
					{
						log.add("The Active Tab is \n"+newtab);
					}
					else
					{
						return;
					}
					WebElement AllTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_AllTab");
					AllTab.click();
					Thread.sleep(1000);
					BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveAllTab");
				    WebElement AllTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveAllTab");
				   String alltab = AllTabActive.getText();
				   if(BNBasicCommonMethods.isElementPresent(AllTabActive) && BNBasicCommonMethods.isElementPresent(Details))
					{
						log.add("The Active Tab is\n"+alltab);
					}
					else
					{
						return;
					}
					WebElement UsedTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_UsedTab");
					UsedTab.click();
					BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveUsedTab");
					WebElement UsedTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveUsedTab");
					String usedtab = UsedTabActive.getText();
					if(BNBasicCommonMethods.isElementPresent(UsedTabActive) && (BNBasicCommonMethods.isElementPresent(Details)))
					{
						log.add("The Active tab is\n"+usedtab);
						pass("While selecting the All,New and Used sections in the Marketplace overlay,the respective tab is displayed",log);
					}
					else
					{
						fail("While selecting the All,New and Used sections in the Marketplace overlay,the respective tab is not displayed");
					}
					
				}
				catch(Exception e2)
				{
					BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveUsedTab");
					WebElement UsedTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveUsedTab");
					String usedtab = UsedTabActive.getText();
					WebElement Details = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_TabDetails");
					if(BNBasicCommonMethods.isElementPresent(UsedTabActive) && (BNBasicCommonMethods.isElementPresent(Details)))
					{
						log.add("The Active tab is\n"+usedtab);
						pass("While selecting the All,New and Used sections in the Marketplace overlay,the respective tab is displayed",log);
					}
					else
					{
						fail("While selecting the All,New and Used sections in the Marketplace overlay,the respective tab is not displayed");
					}
					 WebElement NewTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_NewTab");
					NewTab.click();
					BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveNewTab");
					WebElement NewTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveNewTab");
					String newtab = NewTabActive.getText();
					if((BNBasicCommonMethods.isElementPresent(NewTabActive)) && (BNBasicCommonMethods.isElementPresent(Details)))
					{
						log.add("The Active Tab is \n"+newtab);
					}
					else
					{
						return;
					}
					WebElement AllTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_AllTab");
					AllTab.click();
					Thread.sleep(1000);
					BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveAllTab");
				    WebElement AllTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveAllTab");
				    String alltab = AllTabActive.getText();
					
					if(BNBasicCommonMethods.isElementPresent(AllTabActive) && BNBasicCommonMethods.isElementPresent(Details))
					{
						log.add("The Active Tab is\n"+alltab);
					}
					else
					{
						return;
					}
				}
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA447\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}*/
	
	/*BNIA447 - Verify that while selecting the All,New and Used sections in the Marketplace overlay,the respective tab should be displayed */
	public void BNIA447()
	{
		ChildCreation("BNIA447 - Verify that while selecting the All,New and Used sections in the Marketplace overlay,the respective tab should be displayed ");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "MarketplaceOverlay");
			Thread.sleep(2500);
			try
			{
				
				WebElement AllTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_AllTab");
				AllTab.click();
				Thread.sleep(2000);
				WebElement AllTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveAllTab");
				String alltab = AllTabActive.getText();
			    WebElement Details = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_TabDetails");
				if(BNBasicCommonMethods.isElementPresent(AllTabActive) && BNBasicCommonMethods.isElementPresent(Details))
				{
					log.add("The Active Tab is "+alltab);
					pass("While selecting the All section in the Marketplace overlay,the respective tab is displayed",log);
				}
				else
				{
					return;
				}
				try
				{
					WebElement NewTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_NewTab");
					NewTab.click();
					Thread.sleep(2000);
					WebElement NewTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveNewTab");
					String newtab = NewTabActive.getText();
					if((BNBasicCommonMethods.isElementPresent(NewTabActive)) && (BNBasicCommonMethods.isElementPresent(Details)))
					{
						log.add("The Active Tab is "+newtab);
						pass("While selecting the New section in the Marketplace overlay,the respective tab is displayed",log);
					}
					else
					{
						return;
					}
					
				}
				catch(Exception w)
				{
					Skip("New Tab is not available for this Product");
				}
				
				try
				{
					WebElement UsedTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_UsedTab");
					UsedTab.click();
					Thread.sleep(2000);
					WebElement UsedTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveUsedTab");
					String usedtab = UsedTabActive.getText();
					if(BNBasicCommonMethods.isElementPresent(UsedTabActive) && (BNBasicCommonMethods.isElementPresent(Details)))
					{
						log.add("The Active tab is\n"+usedtab);
						pass("While selecting the All,New and Used sections in the Marketplace overlay,the respective tab is displayed",log);
					}
					else
					{
						fail("While selecting the All,New and Used sections in the Marketplace overlay,the respective tab is not displayed");
					}
				}
				catch(Exception q)
				{
					Skip("Used Tab is not available for this Product");
				}
			}
			catch(Exception e1)
			{
				try
				{
					//BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveAllTab");
				    WebElement AllTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveAllTab");
				    String alltab = AllTabActive.getText();
				    WebElement Details = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_TabDetails");
					if(BNBasicCommonMethods.isElementPresent(AllTabActive) && BNBasicCommonMethods.isElementPresent(Details))
					{
						log.add("The Active Tab is\n"+alltab);
						pass("While selecting the All section in the Marketplace overlay,the respective tab is displayed",log);
					}
					else
					{
						return;
					}
					try
					{
						WebElement NewTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_NewTab");
						NewTab.click();
						Thread.sleep(2000);
						WebElement NewTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveNewTab");
						String newtab = NewTabActive.getText();
						if((BNBasicCommonMethods.isElementPresent(NewTabActive)) && (BNBasicCommonMethods.isElementPresent(Details)))
						{
							log.add("The Active Tab is \n"+newtab);
							pass("While selecting the New section in the Marketplace overlay,the respective tab is displayed",log);
						}
						else
						{
							return;
						}
						try
						{
							WebElement UsedTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_UsedTab");
							UsedTab.click();
							Thread.sleep(2000);
							WebElement UsedTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveUsedTab");
							String usedtab = UsedTabActive.getText();
							if(BNBasicCommonMethods.isElementPresent(UsedTabActive) && (BNBasicCommonMethods.isElementPresent(Details)))
							{
								log.add("The Active tab is\n"+usedtab);
								pass("While selecting the All,New and Used sections in the Marketplace overlay,the respective tab is displayed",log);
							}
							else
							{
								fail("While selecting the All,New and Used sections in the Marketplace overlay,the respective tab is not displayed");
							}
						}
						catch(Exception r)
						{
							Skip("Used Tab is not available for this Product");
						}
					}
					catch(Exception e12)
					{
						Skip("New Tab is not available for this Product");
					}
				}
				catch(Exception e11)
				{
					Skip("All Tab is not available for this Product");
				}
			}
			
		}	
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA447\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
		
	/*BNIA449 - Verify that Market place overlay should be close on tapping the close button.*/
	public void BNIA449()
	{
		ChildCreation("BNIA449 - Verify that Market place overlay should be close on tapping the close button.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_CloseIcon");
			WebElement closeIcon = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_CloseIcon");
			closeIcon.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "EmailButton");
			WebElement pdpemailbutton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			if(pdpemailbutton.isDisplayed())
			{
				pass("Market place overlay is closed on tapping the close button.");
			}
			else
			{
				fail("Market place overlay is not closed on tapping the close button.");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA449\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA714 - Verify that Market place overlay Price of the Product should be displayed with $(ie $XX.X)*/
	public void BNIA714()
	{
		ChildCreation("BNIA714 - Verify that Market place overlay Price of the Product should be displayed with $(ie $XX.X)");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "Marketplace");
			WebElement marketplacelink = BNBasicCommonMethods.findElement(driver, obj, "Marketplace");
			marketplacelink.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "MarketplaceOverlay");
			WebElement marketplaceprice1 = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_Price1");
			WebElement marketplaceprice2 = driver.findElement(By.xpath("//*[@class = 'sk_bn_currencyCode']"));
			
			String price = marketplaceprice1.getText();
			String str = price.replace("$", "");
			String price1 = marketplaceprice2.getText();
			String str1 = ""+price1+str;
			if(price.equals(str1))
			{
				log.add("The Price is shown as\n"+str1);
				pass("Market place overlay Price of the Product is displayed with $(ie $XX.X)",log);
				
			}
			else
			{
				fail("Market place overlay Price of the Product is not displayed with $(ie $XX.X)");
			}
			Thread.sleep(1000);
			BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_CloseIcon");
			WebElement closeicon = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_CloseIcon");
			closeicon.click();
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA714\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA715 -  Verify that when Add to Bag button is clicked in overlay, then it should navigate to the corresponding page.*/
	public void BNIA715() throws InterruptedException
	{
		Thread.sleep(1000);
		ChildCreation("BNIA715 -  Verify that when Add to Bag button is clicked in overlay, then it should navigate to the corresponding page.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String EAN = BNBasicCommonMethods.getExcelNumericVal("BNIA363", sheet, 3);
			String URL = "http://"+"bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+EAN;
			Thread.sleep(1000);
			driver.get(URL);
			Thread.sleep(3000);
			int ctr=1;
			do
			{
				try
				{
					BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOption");
					WebElement AllFormats = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOption");
					if(BNBasicCommonMethods.isElementPresent(AllFormats))
					{
						Thread.sleep(2000);
						AllFormats.click();
					}
					Thread.sleep(2000);
					List<WebElement> DropdownList = driver.findElements(By.xpath("//*[@class = 'sk_bn_formatOption']"));
					for(int i=1;i<=DropdownList.size();i++)
					{
						WebElement dropdownoptions = driver.findElement(By.xpath("(//*[@class = 'sk_bn_formatOption'])["+i+"]"));
						String drdplist = dropdownoptions.getAttribute("value");
						//System.out.println(""+drdplist);
						if(drdplist.equals("allFormat"))
						{
							Thread.sleep(3000);
							dropdownoptions.click();
							Thread.sleep(3000);
							
						}
						else
						{
							System.out.println("There is no All Fromats Option in the dropdown menu for the Selected product");
						}
					}
					
					try
					{
						Thread.sleep(2000);
						BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOverlay");
						JavascriptExecutor js = (JavascriptExecutor) driver;
					    js.executeScript("skMob.hideLoadingSpinner();");
						List<WebElement> formatSize = driver.findElements(By.xpath("(//*[@class='sk_allFormatContent']//*[@class='sk_allFormatDetailsCont'])[1]"));
						for(int i = 0; i<formatSize.size();i++)
						{
						Random r = new Random();
						List<WebElement> ATB = driver.findElements(By.xpath("(//*[@class='sk_allFormatContent']//*[@class='sk_allFormatDetailsCont'])["+(i+1)+"]//*[@class = 'sk_allFromatAddToBagBtn ']"));
						int sel = r.nextInt(ATB.size());
						Thread.sleep(500);
						ATB.get(sel).click();
						
						Thread.sleep(1000);
						BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay");
						if(BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay").isDisplayed())
						{
							ctr=5;
							break;
						}
						}
						/*WebElement ATB1 = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOverlay_ATB1");
						ATB1.click();*/
						try
						{
							BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay");
							WebElement ATBSuccessalert = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay");
							Thread.sleep(1000);
							WebElement ATBSuccessalert_ContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ContinueButton");
							if((ATBSuccessalert.isDisplayed()) && (ATBSuccessalert_ContinueBtn.isDisplayed()))
							{
								log.add("Continue Button is shown in the Success Overlay");
								pass("When Add to Bag button is clicked in overlay, then it navigate to the corresponding page or it shows success overlay",log);
							}
							else
							{
								fail("When Add to Bag button is clicked in overlay, then it does not navigate to the corresponding page or it shows success overlay");
							}
						}
						catch(Exception e)
						{
							System.out.println("ATB Success Overlay is not shown");
						}
					}
					catch(Exception e1)
					{
						BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATBclick_ErrorAlert");
						WebElement OkButton = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATBclick_ErrorAlertOkButton");
						OkButton.click();
					}
					
					
					
					/*BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ATBButton1");
					WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ATBButton1");
					ATB.click();
					BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay");
					WebElement ATBSuccessalert = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay");
					WebElement ATBSuccessalert_ContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ContinueButton");
					if((ATBSuccessalert.isDisplayed()) && (ATBSuccessalert_ContinueBtn.isDisplayed()))
					{
						log.add("Continue Button is shown in the Success Overlay");
						pass("When Add to Bag button is clicked in overlay, then it navigate to the corresponding page or it shows success overlay",log);
					}
					else
					{
						fail("When Add to Bag button is clicked in overlay, then it does not navigate to the corresponding page or it shows success overlay");
					}*/
						
				}
				catch(Exception e1)
				{
					System.out.println("Format Dropdown is not shown");
					String URL1 = "http://"+"bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+EAN;
					driver.get(URL1);
					Thread.sleep(3000);
					ctr++;
					//continue;
				}
			}while(ctr<=4);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA715\n"+e.getMessage());
 			exception("There is something went wrong"+e.getMessage());
 		}
	}		
	
	/*BNIA716 - Verify that Add to Bag button should be highlighted in bold.*/
	public void BNIA716()
	{
		ChildCreation("BNIA716 - Verify that Add to Bag button should be highlighted in bold.");
		try
		{
			WebElement continueshop = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ContinueButton");
			continueshop.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOverlay_ATB1");
			WebElement ATB1 = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOverlay_ATB1");
			String atb = ATB1.getCssValue("background-color");
			Color bagcolors = Color.fromString(atb);
			String hexbagcolors = bagcolors.asHex();
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String bagColor = BNBasicCommonMethods.getExcelVal("BNIA716", sheet, 6);
			if(hexbagcolors.equals(bagColor))
			{
				log.add("The hexa value of RED color is\n"+bagcolors);
				pass("Add to Bag button is highlighted in bold(RED COLOR)",log);
			}
			else
			{
				fail("Add to Bag button is not highlighted in bold(RED COLOR)");
			}
			
			/*WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ATBButton1");
			String atb = ATB.getCssValue("background-color");
			Color bagcolors = Color.fromString(atb);
			String hexbagcolors = bagcolors.asHex();
			String bagColor = "#000000";
			if(hexbagcolors.equals(bagColor))
			{
				log.add("The hexa value of RED color is "+bagcolors);
				pass("Add to Bag button is highlighted in bold(WHITE COLOR)",log);
			}
			else
			{
				fail("Add to Bag button is not highlighted in bold(WHITE COLOR)");
			}*/
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA716\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}	
	
	/*BNIA717 - Verify that in Add to Bag button the Add to Bag - $X.XX inline text should be shown.*/
	public void BNIA717()
	{
		ChildCreation("BNIA717 - Verify that in Add to Bag button the Add to Bag - $X.XX inline text should be shown.");
		try
		{
			WebElement ATB1 = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOverlay_ATB1");
			String atb = ATB1.getText();
			//String atbsplit = atb.replace("$", "");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String atbText = BNBasicCommonMethods.getExcelVal("BNIA717", sheet, 1);
			if(atb.contains(atbText))
			{
				log.add("The ATB Inline text is\n"+atb);
				pass("In Add to Bag button the Add to Bag - $X.XX inline text is shown",log);
			}
			else
			{
				fail("In Add to Bag button the Add to Bag - $X.XX inline text is not shown");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA717\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA719 - Verify that each tab switching options should display Product count as Paperback(XX), Hardcovers(XX), and so on...*/
	public void BNIA719()
	{
		ChildCreation("BNIA719 - Verify that each tab switching options should display Product count as Paperback(XX), Hardcovers(XX), and so on...");
		try
		{
			boolean PgOk = BNBasicCommonMethods.waitforElement(wait, obj, "PdpPageAllFormatList");
			if(PgOk==true)
			{
				Thread.sleep(1000);
				List<WebElement> formatSize = driver.findElements(By.xpath("//*[@class='sk_allFormatTitleCont']//*[contains(@class,'allFormatProductTitle')]"));
				for(int i = 0; i<formatSize.size();i++)
				{
					formatSize.get(i).click();
					Thread.sleep(2500);
					String str = driver.findElement(By.xpath("//*[@class='sk_allFormatTitleCont']//*[contains(@class,'allFormatProductTitle')]["+(i+1)+"]")).getText();
					//System.out.println(""+str);
					if(driver.findElement(By.xpath("//*[@class='sk_allFormatTitleCont']//*[contains(@class,'allFormatProductTitle')]["+(i+1)+"]")).isDisplayed())
					{
						log.add("+str+\n" +"Title is shown");
						pass("Each tab switching options displays Product count as Paperback(XX), Hardcovers(XX), and so on...",log);
					}
					else
					{
						fail("Each tab switching options displays Product count as Paperback(XX), Hardcovers(XX), and so on...");
					}	
				}
				WebElement paperback = BNBasicCommonMethods.findElement(driver, obj, "AllFormatsOverlay_PaperBackTab");
				paperback.click();
			}			 
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA719\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA528 - Verify that on tapping outside the PDP page, the hamburger menu should be closed. */
	public void BNIA528()
	{
		ChildCreation("BNIA528 - Verify that on tapping outside the PDP page, the hamburger menu should be closed. ");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOverlay_Closeicon");
			WebElement close = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOverlay_Closeicon");
			close.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "PancakeMenu");
			WebElement hamburger = BNBasicCommonMethods.findElement(driver, obj, "PancakeMenu");
			hamburger.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Mask");
			WebElement mask = BNBasicCommonMethods.findElement(driver, obj, "Mask");
			mask.click();
			if(!mask.isDisplayed())
			{
				pass("On tapping outside the PDP page, the hamburger menu is closed");
			}
			else
			{
				fail("On tapping outside the PDP page, the hamburger menu should is not closed");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA528\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA561 - Verify that  On tapping outside the Email this Item overlay ,that should be closed*/
	public void BNIA561()
	{
		ChildCreation("BNIA561 - Verify that  On tapping outside the Email this Item overlay ,that should be closed");
		try
		{
			WebElement emailbutton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			emailbutton.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPContainer");
			//WebElement emailbutton1 = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			WebElement emailbutton2 = BNBasicCommonMethods.findElement(driver, obj, "EmailMask");
			emailbutton2.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductName");
			if(BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductName").isDisplayed())
			{
				log.add("The PDP page is shown");
				pass("On tapping outside the Email this Item overlay ,it gets closed",log);
			}
			else
			{
				fail("On tapping outside the Email this Item overlay ,it does not gets closed");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA561"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA571 - Verify that empty section should not be shown in the PDP page, when the respective products does not have certain sections or links.*/
	public void BNIA571()
	{
		ChildCreation("BNIA571 - Verify that empty section should not be shown in the PDP page, when the respective products does not have certain sections or links.");
		try
		{
			Thread.sleep(2000);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPContainer");
			//WebElement overview = BNBasicCommonMethods.findElement(driver, obj, "PDPOverviewLink");
			try
			{
				WebElement overviewsection = BNBasicCommonMethods.findElement(driver, obj, "PDPOverviewSection");
				if(BNBasicCommonMethods.isElementPresent(overviewsection))
				{
					log.add("Overview section is shown");
				}
				else
				{
					return;
				}
			}
			catch(Exception e1)
			{
				Skip("There is no Overview Link for the Selected product");
			}
			
			try
			{
				
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomeralsoBoughtLink");
				WebElement cuslink = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomeralsoBoughtLink");
				cuslink.click();
				Thread.sleep(3000);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerAlsoBoughtThisSection");
				WebElement showmorelink = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerAlsoBoughtThisSection_ShowMoreLink");
				showmorelink.click();
				
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerAlsoBoughtThisSection_Products");
				WebElement customeralsoboughtsection = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerAlsoBoughtThisSection_Products");
				if(BNBasicCommonMethods.isElementPresent(customeralsoboughtsection))
				{
					log.add("The customer also bought section is shown");
				}
				else
				{
					return;
				}
			}
			catch(Exception e2)
			{
				Skip("There is no Customer Also Bought Section for the selected product");
			}
			
			try
			{
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomerReviewsLink");
				WebElement customerreviewslink = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerReviewsLink");
				customerreviewslink.click();
				
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerReviewsSection");
				WebElement reviews = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsSection_Reviews");
				if(BNBasicCommonMethods.isElementPresent(reviews))
				{
					log.add("THe Customer Reviews section is shown");
				}
				else
				{
					return;
				}
			}
			catch(Exception e3)
			{
				Skip("There is no Customer Reviews section for the selected product");
			}
			
			try
			{
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomerEditorialReviewsLink");
				WebElement editoriallink = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerEditorialReviewsLink");
				editoriallink.click();
				
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_EditorialReviewsSection");
				WebElement editorialReviews = BNBasicCommonMethods.findElement(driver, obj, "PDP_EditorialReviewsSection_Reviews");
				if(BNBasicCommonMethods.isElementPresent(editorialReviews))
				{
					log.add("Editorial Reviews is Shown");
				}
				else
				{
					return;
				}
			}
			catch(Exception e4)
			{
				Skip("There is no Editorial Reviews Section for the selected product");
			}
			
			try
			{
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDPMeetTheAuthorLink");
				WebElement authorlink = BNBasicCommonMethods.findElement(driver, obj, "PDPMeetTheAuthorLink");
				authorlink.click();
			
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_MeetTheAuthorSection_Text");
				WebElement authortext = BNBasicCommonMethods.findElement(driver, obj, "PDP_MeetTheAuthorSection_Text");
				if(BNBasicCommonMethods.isElementPresent(authortext))
				{
					log.add("Meet the Author Section is shown ");
				}
				else
				{
					return;
				}
			}
			catch(Exception e5)
			{
				Skip("There is no Meet the Author section for the selected product");
			}
			
			try
			{
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDPProductDetailsLink");
				WebElement prddetailslink = BNBasicCommonMethods.findElement(driver, obj, "PDPProductDetailsLink");
				prddetailslink.click();
				
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductDetailsSection");
				WebElement prddetails = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductDetailsSection_Content");
				if(BNBasicCommonMethods.isElementPresent(prddetails))
				{
					log.add("The Product Details Section is Shown");
				}
				else
				{
					return;
				}
			}
			catch(Exception e6)
			{
				Skip("There is no Product Details Section for the Selected product");
			}
			
			try
			{
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDPRelatedSubjectsLink");
				WebElement relatedsublink = BNBasicCommonMethods.findElement(driver, obj, "PDPRelatedSubjectsLink");
				relatedsublink.click();
				
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_RelatedSubjectsSection_Link");
				WebElement relatessublinks = BNBasicCommonMethods.findElement(driver, obj, "PDP_RelatedSubjectsSection_Link");
				if(BNBasicCommonMethods.isElementPresent(relatessublinks))
				{
					log.add("The Related Subjects Links is shown");
					pass("Empty section is not shown in the PDP page, when the respective products does not have certain sections or links.");
				}
				else
				{
					fail("Empty section is shown in the PDP page, when the respective products does not have certain sections or links.");
				}
			}
			catch(Exception e7)
			{
				Skip("There is no Related subjects link for the selected product");
			}
			WebElement scroll = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductName");
			BNBasicCommonMethods.scrollup(scroll, driver);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA571\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA573 - Verify that  other images of the product displayed below the primary image should be as per creative*/
	public void BNIA573()
	{
		ChildCreation("BNIA573 - Verify that  other images of the product displayed below the primary image should be as per creative");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "secondaryimages");
			WebElement secImages = BNBasicCommonMethods.findElement(driver, obj, "secondaryimages");
			if(BNBasicCommonMethods.isElementPresent(secImages))
			{
				pass("other images of the product is displayed below the primary image in the product detail page");
			}
			else
			{
				fail("other images of the product is not displayed below the primary image in the product detail page");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA573\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA576 - Verify that Format option in PDP page should be as per creative*/
	public void BNIA576()
	{
		ChildCreation("BNIA576 - Verify that Format option in PDP page should be as per creative");
		try
		{
			Thread.sleep(2000);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOption");
			WebElement allformats = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOption");
			String inactiveallformats = allformats.getCssValue("color");
			Color inactiveallformatscolors = Color.fromString(inactiveallformats);
			String hexinactiveformatscolors = inactiveallformatscolors.asHex();
			Thread.sleep(2000);
			allformats.click();
			Thread.sleep(2000);
			String activeformats = allformats.getCssValue("background-color");
			Color activeallformatscolors = Color.fromString(activeformats);
			String hexactiveformatscolors = activeallformatscolors.asHex();
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String inactive_formats = BNBasicCommonMethods.getExcelVal("BNIA378", sheet, 6);
			String active_formats = BNBasicCommonMethods.getExcelVal("BNIA378", sheet, 6);
			if((hexinactiveformatscolors.equals(inactive_formats)) && (hexactiveformatscolors.equals(active_formats)))
			{
				log.add("The Inactive Allformats Text Color is\n"+hexinactiveformatscolors);
				log.add("The Active Allformats Button Color is\n"+hexactiveformatscolors);
				pass("Format option in PDP page is as per creative",log);
			}
			else
			{
				fail("Format option in PDP page is not as per creative");
			}
			allformats.click();
			Thread.sleep(2000);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA576\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA577 -  Verify that  Email this Item overlay is displayed user should not able to access the PDP page */
	public void BNIA577() throws Exception
	{
		ChildCreation("BNIA577 -  Verify that Email this Item overlay is displayed user should not able to access the PDP page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "EmailButton");
			WebElement emailbutton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			Thread.sleep(2000);
			emailbutton.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "EmailOverlay");
			if(BNBasicCommonMethods.findElement(driver, obj, "EmailButton").isEnabled())
			{
				Thread.sleep(2000);
				emailbutton.click();
				fail("Email this Item overlay is displayed user is able to access the PDP page");
			}
			else
			{
				return;
			}
			
		}
		catch(Exception e)
		{
			pass("Email this Item overlay is displayed user not able to access the PDP page");
			WebElement cancelbtn = BNBasicCommonMethods.findElement(driver, obj, "EmailCancelButton");
			Thread.sleep(2000);
			cancelbtn.click();
			Thread.sleep(2000);
        }
	}
	
	/*BNIA579 - Verify that overview section should display overview title with overview description*/
	public void BNIA579()
	{
		ChildCreation("BNIA579 - Verify that overview section should display overview title with overview description");
		try
		{
			//WebElement overview = BNBasicCommonMethods.findElement(driver, obj, "PDPOverviewLink");
			WebElement overviewsection = BNBasicCommonMethods.findElement(driver, obj, "PDPOverviewSection");
			WebElement overviewsection_Title = BNBasicCommonMethods.findElement(driver, obj, "PDPOverview_TitleText");
			if((BNBasicCommonMethods.isElementPresent(overviewsection)) && (BNBasicCommonMethods.isElementPresent(overviewsection_Title)))
			{
				pass("Overview section displays overview title with overview description");
			}
			else
			{
				fail("Overview section not displays overview title with overview description");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA579\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	public void BNIA580()
	{
		ChildCreation("BNIA580 - Verify that  while tapping hamburger menu at the header,the PDP page should not be enabled.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PancakeMenu");
			WebElement hamburgermenu = BNBasicCommonMethods.findElement(driver, obj, "PancakeMenu");
			hamburgermenu.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Mask");
			WebElement mask = BNBasicCommonMethods.findElement(driver, obj, "Mask");
			if(BNBasicCommonMethods.isElementPresent(mask))
			{
				pass("while tapping hamburger menu at the header,the PDP page is not enabled.");
			}
			else
			{
				fail("while tapping hamburger menu at the header,the PDP page is enabled.");
			}
			
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA579\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA418 - Verify that Product ONLINE or SHOW IN STORE should be displayed  based on the inventort details*/
	public void BNIA418()
	{
		ChildCreation("BNIA418 - Verify that Product ONLINE or SHOW IN STORE should be displayed  based on the inventort details");
		try
		{
			int ctr=1,count = 0;
			boolean btnFound = false;
			do
			{
				if(!FTprdtId.equals(""))
				{
					do
					{
						String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+FTprdtId;
						driver.get(PDPURL); 
						((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
						((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
						((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
						Thread.sleep(1500);
						try
						{
							//BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ShowInStoreButton");
							//BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AlsoAvailableOnlineTextLink");
							Thread.sleep(2000);
							WebElement ShowInStore = BNBasicCommonMethods.findElement(driver, obj, "PDP_ShowInStoreButton");
							if(BNBasicCommonMethods.isElementPresent(ShowInStore))
							{
								btnFound = true;
								//System.out.println(btnFound);
								break;
							}
							else
							{
								btnFound = false;
								count++;
								continue;
							}
						}
						catch(Exception e)
						{
							btnFound = false;
							count++;
							continue;
						}
					}while(count<=10);
					
					try
					{
						BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ShowInStoreButton");
						//BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AlsoAvailableOnlineTextLink");
						WebElement ShowInStore = BNBasicCommonMethods.findElement(driver, obj, "PDP_ShowInStoreButton");
						if(BNBasicCommonMethods.isElementPresent(ShowInStore))
						{
							ctr=6;
							String StoreURL = "http://bnstoremaps.com/product.php?storeNbr="+BNConstant.StoreID+"&itemId="+FTprdtId+"&mode=ast";
							ShowInStore.click();
							driver.get(StoreURL);
							Thread.sleep(2000);
							//WebElement ean = BNBasicCommonMethods.findElement(driver, obj, "PDP_ShowInStore_EAN");
							//String eanno = ean.getText();
							driver.navigate().back();
							if (driver instanceof JavascriptExecutor) 
							{
							    ((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
							    ((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
							    ((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
							} 
							else 
							{
							    throw new IllegalStateException("This driver does not support JavaScript!");
							}
							
							pass("Product ONLINE or SHOW IN STORE button is displayed  based on the inventort details");
						}
						else
						{
							System.out.println("Show in Store button is not shown");
						}
						
					}
					catch(Exception e)
					{
						System.out.println("Show in Store Button is not shown");
						ctr++;
					}
				}
			
			else
			{
			System.out.println("FTProduct ID is not found");
			}
		  }while(ctr <= 5);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA418\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA419 - Verify that SHOW IN STORE & ADD TO BAG buttons should be shown when the product available in both store & online */
	public void BNIA419() throws InterruptedException
	{
		Thread.sleep(1500);
		ChildCreation("BNIA419 - Verify that SHOW IN STORE & ADD TO BAG buttons should be shown when the product available in both store & online ");
		try
		{
			Thread.sleep(2000);
			int ctr=1;
			do
			{
				if(!TTprdtId.equals(""))
				{
					String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+TTprdtId;
					driver.get(PDPURL); 
					((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
					((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
					((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
				try
					{
						Thread.sleep(2500);
						//BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AlsoAvailableOnlineTextLink");
						WebElement AlsoAvailableOnlineText = BNBasicCommonMethods.findElement(driver, obj, "PDP_AlsoAvailableOnlineTextLink");
						WebElement ShowInStore = BNBasicCommonMethods.findElement(driver, obj, "PDP_ShowInStoreButton");
						if(BNBasicCommonMethods.isElementPresent(AlsoAvailableOnlineText))
						{
							ctr=6;
							AlsoAvailableOnlineText.click();
							Thread.sleep(1000);
							BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
							WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
							if(BNBasicCommonMethods.isElementPresent(ATB) && BNBasicCommonMethods.isElementPresent(ShowInStore))
							{
								pass("SHOW IN STORE & ADD TO BAG buttons is shown when the product available in both store & online");
							}
							else
							{
								fail("SHOW IN STORE & ADD TO BAG buttons is not shown when the product available in both store & online");
							}
						}
					}
					catch(Exception e)
					{
						System.out.println("Show in Store Button is not shown");
						ctr++;
					}
				}
				else
				{
				System.out.println("FTProduct ID is not found");
				}
			}while(ctr <=5);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA419\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA420 - Verify that SHOW IN STORE button should be shown only when the product is available in Store*/
	public void BNIA420()
	{
		ChildCreation("BNIA420 - Verify that SHOW IN STORE button should be shown only when the product is available in Store");
		try
		{
			int ctr=1;
			do
			{
				if(!FTprdtId.equals(""))
				{
					String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+FTprdtId;
					driver.get(PDPURL); 
				    ((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
				    ((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
				    ((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
				try
					{
						Thread.sleep(2500);
						//BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ShowInStoreButton");
						WebElement ShowInStore = BNBasicCommonMethods.findElement(driver, obj, "PDP_ShowInStoreButton");
						if(BNBasicCommonMethods.isElementPresent(ShowInStore))
						{
							ctr=11;
							Thread.sleep(1000);
							
							if(BNBasicCommonMethods.isElementPresent(ShowInStore))
							{
								pass("SHOW IN STORE button is shown only when the product is available in Store");
							}
							else
							{
								fail("SHOW IN STORE button is not shown only when the product is available in Store");
							}
						}
					}
					catch(Exception e)
					{
						System.out.println("Show in Store Button is not shown");
						ctr++;
					}
				}
				else
				{
				System.out.println("FTProduct ID is not found");
				}
			}while(ctr <=10);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA419\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA411 - Verify that product availability should be shown on after selecting format option */
	/*BNIA416 - Verify that Sale Price alone should be shown, when the product is available only  in store*/ 
	public void BNIA411()
	{
		boolean childflag = false;
		ChildCreation("BNIA411 - Verify that product availability should be shown on after selecting format option ");
		try
		{
			int ctr=1,count = 1;
			boolean btnFound = false;
			do
			{
				if(!FTprdtId.equals(""))
				{
					do
					{ 
						childflag = true; 
						String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+FTprdtId;
						driver.get(PDPURL); 
						((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
						((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+BNConstant.StoreID+");}");
						Thread.sleep(1500);
						try
						{
							Thread.sleep(3000);
							WebElement ShowInStore = BNBasicCommonMethods.findElement(driver, obj, "PDP_ShowInStoreButton");
							if(BNBasicCommonMethods.isElementPresent(ShowInStore))
							{
								count=11;
								WebElement Storeprice = BNBasicCommonMethods.findElement(driver, obj, "PDP_StorePrice");
								if(Storeprice.isDisplayed())
								{
									try
									{
										WebElement Saleprice = BNBasicCommonMethods.findElement(driver, obj, "PDP_SalePrice");
										if(Saleprice.isDisplayed())
										{
											fail("Sale Price alone is not shown, when the product is available only in store");
										}
									}
									catch(Exception e1)
									{
										childflag = true; 
										//pass("Sale Price alone is shown, when the product is available only in store");
									}
								}
								btnFound = true;
								//System.out.println(btnFound);
								break;
							}
							else
							{
								btnFound = false;
								count++;
								continue;
							}
						}
						catch(Exception e)
						{
							btnFound = false;
							count++;
							continue;
						}
					}while(count<=10);
					
					try
					{
						BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ShowInStoreButton");
						WebElement AvailabilityText = BNBasicCommonMethods.findElement(driver, obj, "PDP_PrdAvailabilityText");
						if(BNBasicCommonMethods.isElementPresent(AvailabilityText))
						{
						ctr=6;
						pass("product availability is shown on after selecting format option");
						}
						else
						{
							System.out.println("Show in Store button is not shown");
						}
					}
					catch(Exception e)
					{
						System.out.println("Show in Store Button is not shown");
						ctr++;
					}
				}
			
			else
			{
			System.out.println("FTProduct ID is not found");
			}
		  }while(ctr <= 5);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA418\n"+e.getMessage());
 			exception(e.getMessage());
 		}
		
		
		ChildCreation("BNIA416 - Verify that Sale Price alone should be shown, when the product is available only  in store ");
		if(childflag)
			pass("Sale Price alone is shown, when the product is available only in store");
		else
			fail("Sale Price alone is not shown, when the product is available only in store");
	}
	
	
	
 
	public void PDPLogic( )
	{
		//boolean allEanNum = false;
		boolean facets,samerandom;
		int sel = 0;
		ArrayList<Integer> random = new ArrayList<>();
		ArrayList<String> productId = new ArrayList<String>();
		do
		{
			boolean browseOk = browse();
			facets = false;
			if(browseOk==true)
			{
				try
				{
					List<WebElement> browse_pg = driver.findElements(By.xpath("//*[contains(@class, 'hotSpotOverlay')]"));
			 	 	Random ran = new Random();
			 	 	do
			 	 	{
			 	 		sel = ran.nextInt(browse_pg.size());
			 	 		if(sel<1)
			 	 		{
			 	 			sel = 1;
			 	 		}
			 	 		samerandom = false;
			 	 		if(random.contains(sel))
			 	 		{
			 	 			samerandom = false;
			 	 			continue;
			 	 		}
			 	 		else
			 	 		{
			 	 			samerandom = true;
			 	 			random.add(sel);
			 	 		}
			 	 	}while(samerandom!=true);
			   	 	
			 	 	if(random.size()!=browse_pg.size())
			 	 	{
				 	 	wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("(//*[contains(@class, 'hotSpotOverlay')])["+sel+"]")));
				 	 	Thread.sleep(500);
				   	 	WebElement menu = driver.findElement(By.xpath("(//*[contains(@class, 'hotSpotOverlay')])["+sel+"]"));
				   	 	BNBasicCommonMethods.scrolldown(menu, driver);
				   	 	Thread.sleep(1000);
				   	 	menu.click();
				   	 	Thread.sleep(2000);
				   	 	String url = driver.getCurrentUrl();
				   	 	if(url.contains("pdp?"))
				   	 	{
				   	 		facets = false;
				   	 		//driver.navigate().back();
					   	 	BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
							Thread.sleep(1000);
							WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
							logo.click();
							Thread.sleep(1000);
				   	 	    continue;
				   	 	}
				   	 	else
				   	 	{
					   	 	Thread.sleep(1000);
					   	 	facets = false;
					   	 	facets = BNBasicCommonMethods.findElement(driver, obj, "PLP_emptyfactes").isDisplayed();
				   	 		if(facets==true)
				   	 		{
				   	 			log.add("PLP page is shown");
				   	 			Thread.sleep(1000);
				   	 			List<WebElement> productListContainer = driver.findElements(By.xpath("//*[@class='skMob_productListItemOuterCont']"));
				   	 			productId.removeAll(productId);
							 	for(int j = 0;j<productListContainer.size();j++)
							 	{
							 		//System.out.println(productListContainer.get(j).getAttribute("prodid"));
							 		productId.add(productListContainer.get(j).getAttribute("prodid"));
							 	} 
							 	
							 	for(int j = 0; j<productId.size();j++)
							 	{
							 		System.out.println(j);
							 		URL onlineInvurl = new URL("http://bninstore.skavaone.com/skavastream/core/v5/bandnkiosk/product/"+productId.get(j)+"?campaignId=1");
							 		String Invjson = IOUtils.toString(onlineInvurl);
							 		JSONObject urlJson = new JSONObject(Invjson);
							 		JSONObject available = urlJson.getJSONObject("properties").getJSONObject("buyinfo").getJSONArray("availability").getJSONObject(0);
							 		//String instore = available.getString("instore");
							 		String onlineinve = available.getString("onlineinventory");
							 		JSONObject marketPlace = urlJson.getJSONObject("properties").getJSONObject("iteminfo").getJSONArray("flags").getJSONObject(1);
							 		String mp = marketPlace.getString("value");
							 		//Need to set the storeId value instead of the storeId
							 		URL storeInvurl = new URL("http://bninstore.skavaone.com/skavastream/xact/v5/bandnkiosk/getinventory?campaignId=1&storeid=2932&skuid="+productId.get(j));
							 		String Strjson = IOUtils.toString(storeInvurl);
							 		JSONObject strUrlJson = new JSONObject(Strjson);
							 		JSONObject state = strUrlJson.getJSONObject("properties").getJSONObject("state");
							 		String stStatus = state.getString("status");
							 		String strIventory = null;
							 		if(stStatus.contains("success"))
							 		{
							 			JSONObject stInfo = strUrlJson.getJSONObject("properties").getJSONArray("storeinfo").getJSONObject(0);
								 		strIventory = stInfo.getString("inventory");
							 		}
							 		
							 		if(pdpTT==false)
								 	{
								 		if((onlineinve.equals("true"))&&(stStatus.equals("success"))&&(strIventory.equals("y")))
								 		{
								 			TTprdtId = productId.get(j);
								 			pdpTT = true;
								 			System.out.println(" TT Products : " + TTprdtId);
								 		}
								 	}
							 		if(pdpTF==false)
								 	{
							 			if((onlineinve.equals("true"))&&(stStatus.equals("success"))&&(strIventory.equals("n")&&(mp.equals("true"))))
								 		{
							 				TFprdtId = productId.get(j);
								 			pdpTF = true;
								 			System.out.println(" TF Products : " + TFprdtId);
								 		}
							 			else if((onlineinve.equals("true"))&&(stStatus.equals("failure"))&&(mp.equals("true")))
							 			{
								 			TFprdtId = productId.get(j);
								 			pdpTF = true;
								 			System.out.println(" TF Products : " + TFprdtId);
								 		}
							 	 	}
							 		if(pdpFT==false)
								 	{
							 			if((onlineinve.equals("false"))&&(stStatus.equals("success"))&&(strIventory.equals("y")))
								 		{
							 				FTprdtId = productId.get(j);
								 			pdpFT = true;
								 			System.out.println(" FT Products : " + FTprdtId);
								 		}
							 			
							 	 	}
							 		if(pdpFF==false)
								 	{
								 		if((onlineinve.equals("false")&&(stStatus.equals("failure"))))
								 		{
								 			FFprdtId = productId.get(j);
								 			pdpFF = true;
								 			System.out.println(" FF Products : " + FFprdtId);
								 		}
								 		else if((onlineinve.equals("false"))&&(stStatus.equals("success"))&&(strIventory.equals("n")))
								 		{
								 			FFprdtId = productId.get(j);
								 			pdpFF = true;
								 			System.out.println(" FF Products : " + FFprdtId);
								 		}
								 	}
							 	}
							}
				   	 		else
				   	 		{
				   	 			facets = false;
				   	 			continue;
				   	 		}
				   	 	}
			 	 	}
			 	 	else
			 	 	{
			 	 		break;
			 	 	}
				}
				catch(Exception e)
				{
					System.out.println(e.getMessage());
				}
			}
			else
			{
				System.out.println("Not in Browse page.");
				break;
			}
		}while((pdpTT!=true)||(pdpTF!=true)||(pdpFT!=true)||(pdpFF!=true));
			
		
		System.out.println(" TT Products : " + TTprdtId);
		System.out.println(" TF Products : " + TFprdtId);
		System.out.println(" FT Products : " + FTprdtId);
		System.out.println(" FF Products : " + FFprdtId);
	}

/******************************************* BNIA-39  Ability to perform Gift Card balance lookup **************************************************************/
	
	
    @Test(priority=1)
	public void BNIA39_Gift_Card() throws Exception
	{
		// GiftCard Look UP Story
      TempSignIn();
	  BNIAGIFTNAVIGATE();
	  BNIA71();
	  BNIA72();
	  BNIA75();
	  BNIA73();
	  BNIA74();
	  BNIA76();
	  BNIA173();
	  BNIA78();
	  BNIA82();
	  BNIA83();
	  BNIA80();
	  BNIA79();
	  //TemSignOut();
	  TempSessionTimeout();
	  
	}
	  
	/*Temproary Sign in code*/ 
  	public void BNIAGIFTNAVIGATE() throws Exception 
  	{
  	  Thread.sleep(2000);
  	  BNBasicCommonMethods.WaitForLoading(wait);
  	  BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
  	  WebElement Hamburger = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
  	  BNBasicCommonMethods.WaitForLoading(wait);
	  Hamburger.click();
	  Thread.sleep(2000);
	  BNBasicCommonMethods.waitforElement(wait, obj, "pancakegiftcard");
	  WebElement Giftcard= BNBasicCommonMethods.findElement(driver, obj, "pancakegiftcard");
	  Giftcard.click();
	  BNBasicCommonMethods.WaitForLoading(wait);
	}
  		
  	/*BNIA-71 Verify that Gift Card lookup page should be displayed as per the creative*/
  	public void BNIA71()
  	{
  		ChildCreation("BNIA-71 Verify that Gift Card lookup page should be displayed as per the creative");
  		try 
  		{
  			Thread.sleep(3000);
  			sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
		 	WebElement TextBox= BNBasicCommonMethods.findElement(driver, obj, "GiftCardTextBox");
			String TextBoxcolor=TextBox.getCssValue("border-top-color");// top bottom or side
			Color Giftcardtextbox = Color.fromString(TextBoxcolor);
			String Giftcardtextboxstrokecolor = Giftcardtextbox.asHex();
		    String GiftcardTextBoxWidth=TextBox.getCssValue("width");
		    String GiftCardTextBoxHeight=TextBox.getCssValue("height");
			
			
		    /*to verify gift card Label text  size and color*/ 
		    WebElement GiftLabel = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt");
			String GiftCardLabelFontName=GiftLabel.getCssValue("font-family");
			String GiftCardLabelName = GiftLabel.getAttribute("placeholder");
			//String GiftCardLabelName =GiftLabel.getText().toString();
			String GiftCardLabelFontSize=GiftLabel.getCssValue("font-size");
			//System.out.println();
			
			/*get font name from font family*/ 
			String GiftCardTextBoxLFontName=BNBasicCommonMethods.isFontName(GiftCardLabelFontName);
			String ExpectedGiftcardtextboxstrokecolor = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 3);
			String ExpectedGiftCardTextBoxHeight = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 5);
			String ExpectedGiftcardTextBoxWidth = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 6	);
		
			String ExpectedGiftCardTextBoxLFontName = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 7);
			String ExpectedGiftCardLabelFontSize = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 9);
			String ExpectedGiftCardLabeName = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 8);

				 if(Giftcardtextboxstrokecolor.equals(ExpectedGiftcardtextboxstrokecolor))
					pass("Gift Card TextField stroke color Current : "+Giftcardtextboxstrokecolor+" Expected : "+ExpectedGiftcardtextboxstrokecolor);
				else
					fail("Gift Card TextField stroke color Current : "+Giftcardtextboxstrokecolor+" Expected : "+ExpectedGiftcardtextboxstrokecolor);
					
					
			     if(GiftcardTextBoxWidth.equals(ExpectedGiftcardTextBoxWidth) && GiftCardTextBoxHeight.equals(ExpectedGiftCardTextBoxHeight))
					pass("Gift Card TextField Current Width & Height : "+GiftcardTextBoxWidth+"&"+GiftCardTextBoxHeight+" Expected : "+ExpectedGiftcardTextBoxWidth+"&"+ExpectedGiftCardTextBoxHeight);
				else
					fail("Gift Card TextField Current Width & Height : "+GiftcardTextBoxWidth+"&"+GiftCardTextBoxHeight+" Expected : "+ExpectedGiftcardTextBoxWidth+"&"+ExpectedGiftCardTextBoxHeight);
		
				if(GiftCardLabelName.equals(ExpectedGiftCardLabeName))
					pass("Gift Card TextField Label Name Current : "+GiftCardLabelName+" Expected : "+ExpectedGiftCardLabeName);
				else
					fail("Gift Card TextField Label Name Current : "+GiftCardLabelName+" Expected : "+ExpectedGiftCardLabeName);
						
				if(GiftCardTextBoxLFontName.equals(ExpectedGiftCardTextBoxLFontName))
					pass("Gift Card TextField Label Font Current : "+GiftCardTextBoxLFontName+" Expected : "+ExpectedGiftCardTextBoxLFontName);
				else
					fail("Gift Card TextField Label Font Current : "+GiftCardTextBoxLFontName+" Expected : "+ExpectedGiftCardTextBoxLFontName);
							
				if(GiftCardLabelFontSize.equals(ExpectedGiftCardLabelFontSize))
					pass("Gift Card TextField Label Font Size Current : "+GiftCardLabelFontSize+" Expected : "+ExpectedGiftCardLabelFontSize);
				else
					fail("Gift Card TextField Label Font Size Current : "+GiftCardLabelFontSize+" Expected : "+ExpectedGiftCardLabelFontSize);
						
				//System.out.println();
						
				
	     //to verify check balance button 
		WebElement ChField = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
		WebElement GiftCardCheckBalance = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
		String Chbtn = GiftCardCheckBalance.getCssValue("color");
		Color Chcolor = Color.fromString(Chbtn);
		String ChBtnColor = Chcolor.asHex();
		String CheckbuttonWidth=ChField.getCssValue("width");
		String CheckButtonHeight=ChField.getCssValue("height");
	
		String expebtnwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 6);
		String expebtnheight = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 5);
		String expecbtncolor = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 3);
		String expecbtnname = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 7);
		
		   if(ChBtnColor.equals(expecbtncolor))
		   {
			   log.add("Expected"+expecbtncolor);
			   log.add("Current"+ChBtnColor);
			   pass("CheckButton Color is Matched ",log);
		   }
		   
		   else
		   {
			   log.add("Expected"+expecbtncolor);
			   log.add("Current"+ChBtnColor);
			   fail("CheckButton Color is Not Matched ",log);
			   
		   }
		   
		   if(CheckbuttonWidth.equals(expebtnwidth) && CheckButtonHeight.equals(expebtnheight) )
		   {
			   log.add("Expected width & Height"+expebtnwidth+expebtnheight);
			   log.add("Current width & Height"+CheckbuttonWidth+expebtnheight);
			   pass("CheckButton Width and height is Matched ",log);
		   }
		   else
		   {
			   log.add("Expected width & Height "+expebtnwidth+expebtnheight);
			   log.add("Current width & Height "+CheckbuttonWidth+expebtnheight);
			   fail("CheckButton Width and Height is not Matched ",log);
		   }
		   
		   if(ChField.getText().equals(expecbtnname))
		   {
			   log.add("Expected Text "+expecbtnname);
			   log.add("Current text "+ChField.getText());
			   pass("CheckButton text is Matched ",log);
		   }
		   
		   else
		   {
			   log.add("Expected"+expecbtnname);
			   log.add("Current"+ChField.getText());
			   fail("CheckButton text is not Matched ",log);
		   }
		   
  		}
  		
  		catch(Exception e)
  		{
  			exception("There is something wrong, so please check");
  		}
  	}
  	
  	/*BNIA-72 Verify that Font size, Font style, alignments & padding of Gift Card lookup page should be displayed as per the comps*/
  	public void BNIA72()
  	{
  		ChildCreation("BNIA-72 Verify that Font size, Font style, alignments & padding of Gift Card lookup page should be displayed as per the comps");
  		try 
  		{
  			Thread.sleep(3000);
  			sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
		 	WebElement TextBox= BNBasicCommonMethods.findElement(driver, obj, "GiftCardTextBox");
			String TextBoxcolor=TextBox.getCssValue("border-top-color");// top bottom or side
			Color Giftcardtextbox = Color.fromString(TextBoxcolor);
			String Giftcardtextboxstrokecolor = Giftcardtextbox.asHex();
		    String GiftcardTextBoxWidth=TextBox.getCssValue("width");
		    String GiftCardTextBoxHeight=TextBox.getCssValue("height");
			
			
		    /*to verify gift card Label text  size and color*/ 
		    WebElement GiftLabel = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt");
			String GiftCardLabelFontName=GiftLabel.getCssValue("font-family");
			String GiftCardLabelName = GiftLabel.getAttribute("placeholder");
			//String GiftCardLabelName =GiftLabel.getText().toString();
			String GiftCardLabelFontSize=GiftLabel.getCssValue("font-size");
			//System.out.println();
			
			/*get font name from font family*/ 
			String GiftCardTextBoxLFontName=BNBasicCommonMethods.isFontName(GiftCardLabelFontName);
			String ExpectedGiftcardtextboxstrokecolor = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 3);
			String ExpectedGiftCardTextBoxHeight = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 5);
			String ExpectedGiftcardTextBoxWidth = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 6	);
		
			String ExpectedGiftCardTextBoxLFontName = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 7);
			String ExpectedGiftCardLabelFontSize = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 9);
			String ExpectedGiftCardLabeName = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 8);
			
			
		
		   // System.out.println();	
			
				 if(Giftcardtextboxstrokecolor.equals(ExpectedGiftcardtextboxstrokecolor))
					pass("Gift Card TextField stroke color Current : "+Giftcardtextboxstrokecolor+" Expected : "+ExpectedGiftcardtextboxstrokecolor);
				else
					fail("Gift Card TextField stroke color Current : "+Giftcardtextboxstrokecolor+" Expected : "+ExpectedGiftcardtextboxstrokecolor);
					
					
			     if(GiftcardTextBoxWidth.equals(ExpectedGiftcardTextBoxWidth) && GiftCardTextBoxHeight.equals(ExpectedGiftCardTextBoxHeight))
					pass("Gift Card TextField Current Width & Height : "+GiftcardTextBoxWidth+"&"+GiftCardTextBoxHeight+" Expected : "+ExpectedGiftcardTextBoxWidth+"&"+ExpectedGiftCardTextBoxHeight);
				else
					fail("Gift Card TextField Current Width & Height : "+GiftcardTextBoxWidth+"&"+GiftCardTextBoxHeight+" Expected : "+ExpectedGiftcardTextBoxWidth+"&"+ExpectedGiftCardTextBoxHeight);
		
				if(GiftCardLabelName.equals(ExpectedGiftCardLabeName))
					pass("Gift Card TextField Label Name Current : "+GiftCardLabelName+" Expected : "+ExpectedGiftCardLabeName);
				else
					fail("Gift Card TextField Label Name Current : "+GiftCardLabelName+" Expected : "+ExpectedGiftCardLabeName);
						
				if(GiftCardTextBoxLFontName.equals(ExpectedGiftCardTextBoxLFontName))
					pass("Gift Card TextField Label Font Current : "+GiftCardTextBoxLFontName+" Expected : "+ExpectedGiftCardTextBoxLFontName);
				else
					fail("Gift Card TextField Label Font Current : "+GiftCardTextBoxLFontName+" Expected : "+ExpectedGiftCardTextBoxLFontName);
							
				if(GiftCardLabelFontSize.equals(ExpectedGiftCardLabelFontSize))
					pass("Gift Card TextField Label Font Size Current : "+GiftCardLabelFontSize+" Expected : "+ExpectedGiftCardLabelFontSize);
				else
					fail("Gift Card TextField Label Font Size Current : "+GiftCardLabelFontSize+" Expected : "+ExpectedGiftCardLabelFontSize);
						
				//System.out.println();
						
				
	     //to verify check balance button 
		WebElement ChField = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
		WebElement GiftCardCheckBalance = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
		String Chbtn = GiftCardCheckBalance.getCssValue("color");
		Color Chcolor = Color.fromString(Chbtn);
		String ChBtnColor = Chcolor.asHex();
		String CheckbuttonWidth=ChField.getCssValue("width");
		String CheckButtonHeight=ChField.getCssValue("height");

		String expebtnwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 6);
		String expebtnheight = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 5);
		String expecbtncolor = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 3);
		String expecbtnname = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 7);
		
		   if(ChBtnColor.equals(expecbtncolor))
		   {
			   log.add("Expected"+expecbtncolor);
			   log.add("Current"+ChBtnColor);
			   pass("CheckButton Color is Matched ",log);
		   }
		   
		   else
		   {
			   log.add("Expected"+expecbtncolor);
			   log.add("Current"+ChBtnColor);
			   fail("CheckButton Color is Not Matched ",log);
			   
		   }
		   
		   if(CheckbuttonWidth.equals(expebtnwidth) && CheckButtonHeight.equals(expebtnheight) )
		   {
			   log.add("Expected width & Height"+expebtnwidth+expebtnheight);
			   log.add("Current width & Height"+CheckbuttonWidth+expebtnheight);
			   pass("CheckButton Width and height is Matched ",log);
		   }
		   else
		   {
			   log.add("Expected width & Height "+expebtnwidth+expebtnheight);
			   log.add("Current width & Height "+CheckbuttonWidth+expebtnheight);
			   fail("CheckButton Width and Height is not Matched ",log);
		   }
		   
		   if(ChField.getText().equals(expecbtnname))
		   {
			   log.add("Expected Text "+expecbtnname);
			   log.add("Current text "+ChField.getText());
			   pass("CheckButton text is Matched ",log);
		   }
		   
		   else
		   {
			   log.add("Expected"+expecbtnname);
			   log.add("Current"+ChField.getText());
			   fail("CheckButton text is not Matched ",log);
		   }
		   
  		}
  		
  		catch(Exception e)
  		{
  			exception("There is something wrong, so please check");
  		}
  	}
  	
  	/*BNIA-75 Verify that Gift Card text box & Check Balance button should be displayed as per the creative*/
  	public void BNIA75()
  	{
  		ChildCreation("BNIA-75 Verify that Gift Card text box & Check Balance button should be displayed as per the creative");
  		try 
  		{
  			Thread.sleep(3000);
  			sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
		 	WebElement TextBox= BNBasicCommonMethods.findElement(driver, obj, "GiftCardTextBox");
			String TextBoxcolor=TextBox.getCssValue("border-top-color");// top bottom or side
			Color Giftcardtextbox = Color.fromString(TextBoxcolor);
			String Giftcardtextboxstrokecolor = Giftcardtextbox.asHex();
		    String GiftcardTextBoxWidth=TextBox.getCssValue("width");
		    String GiftCardTextBoxHeight=TextBox.getCssValue("height");
			
			
		    /*to verify gift card Label text  size and color*/ 
		    WebElement GiftLabel = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt");
			String GiftCardLabelFontName=GiftLabel.getCssValue("font-family");
			String GiftCardLabelName = GiftLabel.getAttribute("placeholder");
			//String GiftCardLabelName =GiftLabel.getText().toString();
			String GiftCardLabelFontSize=GiftLabel.getCssValue("font-size");
			//System.out.println();
			
			/*get font name from font family*/ 
			String GiftCardTextBoxLFontName=BNBasicCommonMethods.isFontName(GiftCardLabelFontName);
			String ExpectedGiftcardtextboxstrokecolor = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 3);
			String ExpectedGiftCardTextBoxHeight = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 5);
			String ExpectedGiftcardTextBoxWidth = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 6	);
		
			String ExpectedGiftCardTextBoxLFontName = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 7);
			String ExpectedGiftCardLabelFontSize = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 9);
			String ExpectedGiftCardLabeName = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 8);
			
			
		
		   // System.out.println();	
			
				 if(Giftcardtextboxstrokecolor.equals(ExpectedGiftcardtextboxstrokecolor))
					pass("Gift Card TextField stroke color Current : "+Giftcardtextboxstrokecolor+" Expected : "+ExpectedGiftcardtextboxstrokecolor);
				else
					fail("Gift Card TextField stroke color Current : "+Giftcardtextboxstrokecolor+" Expected : "+ExpectedGiftcardtextboxstrokecolor);
					
					
			     if(GiftcardTextBoxWidth.equals(ExpectedGiftcardTextBoxWidth) && GiftCardTextBoxHeight.equals(ExpectedGiftCardTextBoxHeight))
					pass("Gift Card TextField Current Width & Height : "+GiftcardTextBoxWidth+"&"+GiftCardTextBoxHeight+" Expected : "+ExpectedGiftcardTextBoxWidth+"&"+ExpectedGiftCardTextBoxHeight);
				else
					fail("Gift Card TextField Current Width & Height : "+GiftcardTextBoxWidth+"&"+GiftCardTextBoxHeight+" Expected : "+ExpectedGiftcardTextBoxWidth+"&"+ExpectedGiftCardTextBoxHeight);
		
				if(GiftCardLabelName.equals(ExpectedGiftCardLabeName))
					pass("Gift Card TextField Label Name Current : "+GiftCardLabelName+" Expected : "+ExpectedGiftCardLabeName);
				else
					fail("Gift Card TextField Label Name Current : "+GiftCardLabelName+" Expected : "+ExpectedGiftCardLabeName);
						
				if(GiftCardTextBoxLFontName.equals(ExpectedGiftCardTextBoxLFontName))
					pass("Gift Card TextField Label Font Current : "+GiftCardTextBoxLFontName+" Expected : "+ExpectedGiftCardTextBoxLFontName);
				else
					fail("Gift Card TextField Label Font Current : "+GiftCardTextBoxLFontName+" Expected : "+ExpectedGiftCardTextBoxLFontName);
							
				if(GiftCardLabelFontSize.equals(ExpectedGiftCardLabelFontSize))
					pass("Gift Card TextField Label Font Size Current : "+GiftCardLabelFontSize+" Expected : "+ExpectedGiftCardLabelFontSize);
				else
					fail("Gift Card TextField Label Font Size Current : "+GiftCardLabelFontSize+" Expected : "+ExpectedGiftCardLabelFontSize);
						
				//System.out.println();
						
				
	     //to verify check balance button 
		WebElement ChField = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
		WebElement GiftCardCheckBalance = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
		String Chbtn = GiftCardCheckBalance.getCssValue("color");
		Color Chcolor = Color.fromString(Chbtn);
		String ChBtnColor = Chcolor.asHex();
		String CheckbuttonWidth=ChField.getCssValue("width");
		String CheckButtonHeight=ChField.getCssValue("height");
		String expebtnwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 6);
		String expebtnheight = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 5);
		String expecbtncolor = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 3);
		String expecbtnname = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 7);
		
		   if(ChBtnColor.equals(expecbtncolor))
		   {
			   log.add("Expected"+expecbtncolor);
			   log.add("Current"+ChBtnColor);
			   pass("CheckButton Color is Matched ",log);
		   }
		   
		   else
		   {
			   log.add("Expected"+expecbtncolor);
			   log.add("Current"+ChBtnColor);
			   fail("CheckButton Color is Not Matched ",log);
			   
		   }
		   
		   if(CheckbuttonWidth.equals(expebtnwidth) && CheckButtonHeight.equals(expebtnheight) )
		   {
			   log.add("Expected width & Height"+expebtnwidth+expebtnheight);
			   log.add("Current width & Height"+CheckbuttonWidth+expebtnheight);
			   pass("CheckButton Width and height is Matched ",log);
		   }
		   else
		   {
			   log.add("Expected width & Height "+expebtnwidth+expebtnheight);
			   log.add("Current width & Height "+CheckbuttonWidth+expebtnheight);
			   fail("CheckButton Width and Height is not Matched ",log);
		   }
		   
		   if(ChField.getText().equals(expecbtnname))
		   {
			   log.add("Expected Text "+expecbtnname);
			   log.add("Current text "+ChField.getText());
			   pass("CheckButton text is Matched ",log);
		   }
		   
		   else
		   {
			   log.add("Expected"+expecbtnname);
			   log.add("Current"+ChField.getText());
			   fail("CheckButton text is not Matched ",log);
		   }
		   
  		}
  		
  		catch(Exception e)
  		{
  			exception("There is something wrong, so please check");
  		}
  	}
  
  	/*BNIA-73 Verify that B&N Header should be displayed in the Gift Card lookup page as per the creative*/
  	public void BNIA73()
  	{
  		ChildCreation("BNIA-73 Verify that B&N Header should be displayed in the Gift Card lookup page as per the creative.");
  		// to verifying B&N header is Displayed
  		boolean eleVisible = false;
  		try
  		{
  			Thread.sleep(3000);
  			eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "BNHeader");
  			if(eleVisible)
  			{
  				pass("Header is Displayed in the GiftCard Page");
  			}
  			
  			else
  			{
  				fail("Header is Not Displayed in the GiftCard Page");
  			}
  		}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  	}
  		
  	/*BNIA-74 Verify that while selecting the icons in the B&N Header from the Gift Card lookup page,it should navigate to the respective page*/
  	public void BNIA74() throws Exception
  	{
  		Thread.sleep(3000);
  		//Verify that while selecting the icons in the B&N Header from the Gift Card lookup page,it should navigate to the respective page
  		ChildCreation("BNIA-74 Verify that while selecting the icons in the B&N Header from the Gift Card lookup page,it should navigate to the respective page.");
  		BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
  		WebElement Logo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
  		Actions act = new Actions(driver);
  		act.moveToElement(Logo).click().build().perform();
  		boolean eleVisible = false;
  		try
  		{
  			BNBasicCommonMethods.WaitForLoading(wait);
  			eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
  			if(eleVisible)
  			{
  				pass("On Tapping Header BNlogo page Navigates to home page");
  			}
  			else
  			{
  				fail("On Tapping Header BNlogo page is not Navigates to home page");
  			}
  		}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  		
  		finally
  		{   
  			
  			BNIAGIFTNAVIGATE();
  		}
  	}
  		
  	/*BNIA-76 Verify that Gift Card text box should accepts numbers only*/
  	public void BNIA76() throws Exception
  	{
  		ChildCreation("BNIA-76 Verify that Gift Card text box should accepts numbers only");
  		sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
  		String str = BNBasicCommonMethods.getExcelVal("BNIA76", sheet, 2);
  		//String str = "asrtghtvcerbgthngds 1234567890098765432 !@#$%^&*((*&^%$#@!@ FGHLJKdffjgv!@$#3453623 ";
  		String[]  val = str.split("\n");
  		try
  		{
  			BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardInputTxt");
  			
	  		WebElement GiftBox = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt");
	  		for(int ctr=0 ; ctr < val.length ; ctr++)
	  		{
	  			GiftBox.clear();
	  			GiftBox.click();
	  			
	  			GiftBox.sendKeys(val[ctr]);
	  			String currentvalue = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt").getAttribute("value");
                
	  			if(currentvalue.isEmpty() || /*currentvalue.equals(val[ctr])*/ currentvalue.matches("[0-9]+"))
	  			{
	  				log.add("Given Value "+val[ctr]+" Current Value "+currentvalue);
	  				pass("GiftCard TextBox is Accepts Numbers Only ",log);
	  			}
	  			
	  			else
	  			{
	  				log.add("Given Value "+val[ctr]+"Current Value "+currentvalue);
	  				pass("GiftCard TextBox Accepts All Data Types",log);
	  			}
	              			
	  		}
	  		
  		}
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}

  	}
  	
  	/*BNIA-173 Verify that while selecting the "Check Balance" button after entering the invalid gift card value,the alert message should be displayed*/
  	public void BNIA173() throws Exception
  	{
  		//Verify that Gift Card text box should accepts numbers only
  		ChildCreation("BNIA-173 Verify that while selecting the Check Balance button after entering the invalid gift card value,the alert message should be displayed.");
  		try
  		{
  		    sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
  			String GiftAlert = BNBasicCommonMethods.getExcelVal("BNIA76", sheet, 4);
	  		WebElement GiftCardCheckBalance = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
	  		Thread.sleep(1000);
	  		GiftCardCheckBalance.click();
	  		Thread.sleep(1000);
	  		WebElement InvalidGiftTxt = BNBasicCommonMethods.findElement(driver, obj, "InvalidGiftTxt");
	  	    if(InvalidGiftTxt.getText().equals(GiftAlert))
	  	    {
	  	    	log.add("Displayed gift card alert is "+InvalidGiftTxt.getText());
	  	    	log.add("Expected Error Alert message is "+GiftAlert);
	  	    	pass("Gift Card Error Alert Message is Displayed successfully",log);
	  	    }
	  		
	  	    else
	  	    {
	  	    	log.add("Displayed gift card alert is "+InvalidGiftTxt.getText());
	  	    	log.add("Expected Error Alert message is "+GiftAlert);
	  	    	pass("Gift Card Error Alert Message is not Displayed successfully",log);
	  	    	
	  	    }
	  	}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  	}
  		
  	/*BNIA-78 Verify that while selecting the "Check Balance" button after entering the valid gift card value,the "Gift Card Balance:$xxx" message should be displayed*/
  	public void BNIA78()
  	{
  		ChildCreation("BNIA-78 Verify that while selecting the Check Balance button after entering the valid gift card value,the Gift Card Balance:$xxx message should be displayed");
  		try
  		{
  			sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
	    	String ValidGiftcard = BNBasicCommonMethods.getExcelNumericVal("BNIA78", sheet, 2);
	    	String[] VGcard = ValidGiftcard.split("\n");
	    	Thread.sleep(1500);
	    	BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardInputTxt");
	    	for(int ctr=0 ; ctr< VGcard.length ; ctr++)
	    	{
	    	WebElement GiftCardInputTxt = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt");
	    	WebElement GiftCardCheckBalance = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
	    	GiftCardInputTxt.click();
	    	GiftCardInputTxt.clear();
  	    	if(GiftCardInputTxt.isEnabled())
  	    	{
  	    		GiftCardInputTxt.sendKeys(VGcard[ctr]);
  	    		Thread.sleep(500);
  	    		GiftCardCheckBalance.click();
  	    		Thread.sleep(1500);
  	    		log.add("Gift Card Balance is Displayed");
  	    		Thread.sleep(1500);
  	    		BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardValidBal");
  	    		WebElement GiftCardValidBal = BNBasicCommonMethods.findElement(driver, obj, "GiftCardValidBal");
  	    		//System.out.println(GiftCardValidBal.getText());
  	    		if(GiftCardValidBal.isDisplayed())
  	    		{
  	    			log.add("Gift card displayed text message is "+GiftCardValidBal.getText());
  	    			pass("Balance for Valid Giftcard Successfully Displayed ",log);
  	    		}
  	    		else
  	    		{
  	    			fail("Balance for Valid Giftcard is not Displayed ",log);
  	    		}
  	    	}
  	    	else
  	    	{
  	    		fail("Gift Card input text is not enable ");
  	    	}
  			
  		   }
  		} 	
  		
  	   catch(Exception e)
  	  {
  		 exception(e.getMessage());
  	  }
  	}
  	
  	/*BNIA-82 Verify that Gift Card balance value should be higlighted in Green color below the Gift card text box*/
  	public void BNIA82()
  	{
  		ChildCreation("BNIA-82 Verify that Gift Card balance value should be higlighted in Green color below the Gift card text box.");
  		try
  		{
  			sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
	    	String GetColorvalid = BNBasicCommonMethods.getExcelVal("BNIA78", sheet, 3);
  			WebElement GiftBalTxt = BNBasicCommonMethods.findElement(driver, obj, "GiftBalTxt");
  	    	String getColor = GiftBalTxt.getCssValue("color");
  	    	Color getBlaColor = Color.fromString(getColor);
  	    	String Currentcolor = getBlaColor.asHex();
  	    	if(Currentcolor.equals(GetColorvalid))
  	    	{
  	    		log.add("Current Color "+Currentcolor+"Expected Color "+GetColorvalid);
  	    		pass("GiftCard Balance font is Matched",log);
  	    	}
  	    	else
  	    	{
  	    		//System.out.println();
  	    		log.add("Current Color "+Currentcolor+"Expected Color "+GetColorvalid);
  	    		fail("GiftCard Balance font is not Matched",log);
  	    	}
  		}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  	}
  	
  	/*BNIA-83 Verify that Gift card balance should be higlighted in green color with Prefix $ included*/
  	public void BNIA83()
  	{
  		ChildCreation("BNIA-83 Verify that Gift card balance should be higlighted in green color with Prefix $ included.");
  		try
  		{
  			WebElement GiftBalPrefix = BNBasicCommonMethods.findElement(driver, obj, "GiftBalPrefix");
  	    	String Str = GiftBalPrefix.getText();
  	    	if(Str.startsWith("$"))
  	    	{
  	    		log.add("Giftcard Balance is Starts with "+Str);
  	    		pass("Gift card balance should is displayed with Prefix $",log);
  	    	}
  	    	
  	    	else
  	    	{
  	    		log.add("Giftcard Balance is Starts with "+Str);
  	    		fail("Gift card balance should is not displayed with Prefix $",log);
  	    	}
  		}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  	}
  	
  	/*BNIA-80 Verify that while selecting the "Check Balance" button after entering the valid Gift card details,the gift card balance values should be retrieved from the backend call*/
  	public void BNIA80()
  	{
  		ChildCreation("BNIA-80 Verify that while selecting the Check Balance button after entering the valid Gift card details,the gift card balance values should be retrieved from the backend call");
  	    try
  	    {
  	    	sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
  	    	String ValidGiftcard = BNBasicCommonMethods.getExcelNumericVal("BNIA78", sheet, 2);
  	    	String[] VGcard = ValidGiftcard.split("\n");
  	    	String GetColorvalid = BNBasicCommonMethods.getExcelVal("BNIA78", sheet, 3);
  	    	Thread.sleep(2000);
  	    	BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardInputTxt");
  	    	for(int ctr=0 ; ctr< VGcard.length ; ctr++)
  	    	{
  	    	WebElement GiftCardInputTxt = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt");
  	    	WebElement GiftCardCheckBalance = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
  	    	GiftCardInputTxt.click();
  	    	GiftCardInputTxt.clear();
	  	    	if(GiftCardInputTxt.isEnabled())
	  	    	{
	  	    		GiftCardInputTxt.sendKeys(VGcard[ctr]);
	  	    		Thread.sleep(500);
	  	    		GiftCardCheckBalance.click();
	  	    		Thread.sleep(1500);
	  	    		log.add("Gift Card Balance is Displayed");
	  	    		Thread.sleep(2000);
	  	    		BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardValidBal");
	  	    		WebElement GiftCardValidBal = BNBasicCommonMethods.findElement(driver, obj, "GiftCardValidBal");
	  	    	//	System.out.println(GiftCardValidBal.getText());
	  	    		if(GiftCardValidBal.isDisplayed())
	  	    		{
	  	    			log.add("Gift card displayed text message is "+GiftCardValidBal.getText());
	  	    			pass("Balance for Valid Giftcard Successfully Displayed ",log);
	  	    		}
	  	    		else
	  	    		{
	  	    			fail("Balance for Valid Giftcard is not Displayed ",log);
	  	    		}
	  	    	}
	  	    	else
	  	    	{
	  	    		fail("Gift Card input text is not enable ");
	  	    	}
  	    	}
  	    	
  	    }
  	    
  	    catch(Exception e)
  	    {
  	    	exception("There is something wrong,so please verify"+e.getMessage());
  	    }
  	}

  	/*BNIA-79 Verify that Gift Card text box should accept maximum of 19 digits value*/
  	public void BNIA79() throws Exception
  	{
  		sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
  		ChildCreation("BNIA-79 Verify that Gift Card text box should accept maximum of 19 digits value.");
  		String str = BNBasicCommonMethods.getExcelNumericVal("BNIA79", sheet, 2);
  		//String str = "123456789098765432 1234567890987654321 12345678900987654321";
  		String[]  val = str.split("\n");
  		try
  		{
  			BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardInputTxt");
  			BNBasicCommonMethods.WaitForLoading(wait);
	  		WebElement GiftBox = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt");
	  		for(int ctr=0 ; ctr < val.length ; ctr++)
	  		{
	  			GiftBox.clear();
	  			GiftBox.click();
	  			GiftBox.sendKeys(val[ctr]);
	  			String currentvalue = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt").getAttribute("value");
                
	  			if(currentvalue.length()<=val[ctr].length())
	  			{
	  				log.add("Given Value Length "+val[ctr].length()+" Current Value Length "+currentvalue.length());
	  				pass("GiftCard TextBox Accepts Maximum of 19 Digits",log);
	  			}
	  			
	  			else
	  			{
	  				log.add("Given Value Length "+val[ctr].length()+" Current Value Length "+currentvalue.length());
	  				pass("GiftCard TextBox is Exceeds Maximum of digits 19",log);
	  			}
	        }
	  		System.out.println("2/2 - BNIA39_Gift_Card is completed");
	  	}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  	}

	
 @BeforeClass
 public void beforeTest() throws Exception
 {
	 Thread.sleep(3000);
	 driver = bnEmulationSetup.bnASTEmulationSetup();
	 wait = new WebDriverWait(driver, 30);
 }
 
 @AfterClass
 public void afterTest()
 {
     driver.close();
 }
 
 @BeforeMethod
	public static void beforeTest(Method name)
	{
		parent = reporter.startTest(name.getName());
		
	}
	
	@org.testng.annotations.AfterMethod
	public static void AfterMethod(Method name)
	{
		reporter.endTest(parent);
		reporter.flush();
	}
	
	/*@AfterSuite
	public static void closeReporter() 
	{
		reporter.flush();
	}
	*/
	
	public static void ChildCreation(String name)
	{
		child = reporter.startTest(name);
		parent.appendChild(child);
	}
	
	public static void endTest()
	{
		reporter.endTest(child);
	}
	
	public static void createlog(String val)
	{
		child.log(LogStatus.INFO, val);
	}
	
	public static void pass(String val)
	{
		child.log(LogStatus.PASS, val);
		endTest();
	}
	
	public static void pass(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.PASS, val);
		for(String lval:logval)
		{
			createlog(lval);
		}
		logval.removeAll(logval);
		endTest();
	}
	
	public static void fail(String val)
	{
		child.log(LogStatus.FAIL, val);
		endTest();
	}
	
	public static void fail(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.FAIL, val);
		for(String lval:logval)
		{
			createlog(lval);
		}
		logval.removeAll(logval);
		endTest();
	}
	
	public static  void exception(String val)
	{
		child.log(LogStatus.ERROR,val);
		endTest();
	}
	
	public static void Skip(String val)
	{
		child.log(LogStatus.SKIP, val);
		endTest();
	}
	
	
}