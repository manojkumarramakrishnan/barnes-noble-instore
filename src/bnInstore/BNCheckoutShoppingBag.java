package bnInstore;

import java.io.IOException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.exec.util.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.tools.ant.util.XMLFragment.Child;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

import bnEmulation.bnEmulationSetup;
/*import bnPageObjects.CheckOutEditShipandBillObj;*/
import bnInstoreBasicFeatures.BNBasicCommonMethods;
import bnInstoreBasicFeatures.BNConstant;
import extentReport.extentRptManager;
import jdk.nashorn.internal.ir.CatchNode;
public class BNCheckoutShoppingBag 
{
	WebDriver driver;
	 public Properties obj;
	  public static ArrayList<String> log = new ArrayList<>();
	  WebDriverWait wait;
	  public static ExtentReports reporter = extentRptManager.getReporter();
	  public static ExtentTest parent, child;
	HSSFSheet sheet;
	int sel;
	ArrayList<String> ProductPrice = new ArrayList<>(); 
	ArrayList<String> ProductQty = new ArrayList<>();
	String EstimatedPrice ;
	 
	
	public BNCheckoutShoppingBag() throws IOException, Exception
	 {
	     obj = new Properties(System.getProperties());
		 obj.load(BNBasicCommonMethods.loadPropertiesFile());
		 sheet = BNBasicCommonMethods.excelsetUp("Edit ShipandBill Address");
		 
	 }
	
	public void BeforeSetter()
	{
		Actions act = new Actions(driver);
		for(int ctr= 0 ; ctr<5 ; ctr++)
		act.sendKeys(Keys.DOWN.CONTROL).sendKeys(Keys.SUBTRACT).build().perform();
		
	}
   
 /************************************ BNInstore Checkout Main Method to Call Test Cases ***********************************/
 
	@Test(priority=1)
   public void BNIA267_Shopping_Bag() throws Exception
   {
		TempSignIn();
		continuebtn();
		NookBook();
		AddedProduct();
		ordersummar();
		ordersummarcolor();
		Couponcode();
		Couponcodeerror();
		CouponcodeInvaliderror();
		headerfreeshippingdetails();
		headerdetailalink();
		detailoverlayclose();
		/*ShoppingProductsList();
		UpdateRemoveSaveforLater();*/
		LinkColorCheck();
		DeliveryTextColor();
		QuantityUpdate();
		QuantityUpdateLink();
		QuantityLimit();
		PriceUpdateCheck();
		PriceUpdateOrderSumary();
		CheckTotalPrice();
		SaveForLater();
		SaveItemDetails();
		RemoveLinkCheck();
		PDPNavigation();
		SigninLinkInheader();
	//	Checkoutoverlay();
		signInExistingCustomer();
		CheckoutProductDetails();
		CheckoutEditbtn();
		ShoppingProductsList();
		UpdateRemoveSaveforLater();
		//CheckoutUpdate();
		Checkoutpricecheck();
		CheckoutQtycheck();
		CheckoutEstimataePricecheck();
		NooktoShopping();
		Electronicmail();
		
		
		
	 }
		   	
 /*********************************Temporary Sign In and Common Methods used in the functions**************************************/  
     
	public void TempSessionTimeout()
	{
		try
		{
			Thread.sleep(1500);
			if (driver instanceof JavascriptExecutor) {
			    ((JavascriptExecutor)driver).executeScript("ski_idleTimerObj.idleTimeCount=1");
			} 
			else 
			{
			    
				throw new IllegalStateException("This driver does not support JavaScript!");
			}
			Thread.sleep(3000);
			WebElement TimerSession = BNBasicCommonMethods.findElement(driver, obj, "TimerResetButton");
			TimerSession.click();
			Thread.sleep(5000);
		}
		catch(Exception e)
		{
			exception("There is something wrong or mmismatch occured"+e.getMessage());
		}
	}
	
 	/*Temp signIn for Employee to Checkout page*/
  	public void TempSignIn()
	 {    
		  // Temp Signin  
		  WebDriverWait wait =  new WebDriverWait(driver, 90);
		    try
		    {
		    	WebElement Splash=BNBasicCommonMethods.findElement(driver, obj, "splashScreen");
		    	BNBasicCommonMethods.WaitForLoading(wait);
		    	Splash.click();
		    	BNBasicCommonMethods.waitforElement(wait, obj, "signInEmpId");
		    	WebElement id = BNBasicCommonMethods.findElement(driver, obj, "signInEmpId");
		  	   	WebElement pass = BNBasicCommonMethods.findElement(driver, obj, "signInEmpPass");
		    	BNBasicCommonMethods.signInclearAll(id, pass);
		    	id.sendKeys("999999999");
		    	pass.sendKeys("skava");
		    	BNBasicCommonMethods.findElement(driver, obj, "signInClick").click();
		    	Thread.sleep(2000);
		    }
		    
	    catch(Exception e)
		    {
	    	System.out.println(e.getMessage());
	    	
		    	 exception(e.getMessage()); 
		    }
		    
		  }

  	/*clear all fields*/
  	public void clearAll() throws Exception
	{
  		WebElement fName = BNBasicCommonMethods.findElement(driver, obj, "fName");
		WebElement lName = BNBasicCommonMethods.findElement(driver, obj, "lName");
		WebElement stAddress = BNBasicCommonMethods.findElement(driver, obj, "stAddress");
		WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
		WebElement zipCode = BNBasicCommonMethods.findElement(driver, obj, "zipCode");
		WebElement contactNo = BNBasicCommonMethods.findElement(driver, obj, "contactNo");
		fName.clear();
		lName.clear();
		//BNBasicCommonMethods.scrolldown(stAddress,driver);
		stAddress.clear();
	   //aptSuiteAddress.clear();
		city.clear();
		zipCode.clear();
		contactNo.clear();
		//companyName.clear();
		//BNBasicCommonMethods.scrollup(fName,driver);
	}
     
  

	/* Sign In to the New User Account in Checkout */
	public void signInNewCustomer() throws IOException, Exception
	{
		ChildCreation("Add a new Shipping Address overlay should be Displayed");
		BNBasicCommonMethods.WaitForLoading(wait);
		BNBasicCommonMethods.waitforElement(wait, obj, "checkoutTile");
		WebElement CheckoutTile = BNBasicCommonMethods.findElement(driver, obj, "checkoutTile");
		wait.until(ExpectedConditions.elementToBeClickable(CheckoutTile));
		CheckoutTile.click();
		//Thread.sleep(1000);
		//driver.navigate().refresh();
		//BNBasicCommonMethods.WaitForLoading(wait);
		BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
		WebElement CheckoutSignIn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutSignIn");
		wait.until(ExpectedConditions.visibilityOf(CheckoutSignIn));
		//Thread.sleep(1000);
		CheckoutSignIn.click();
		
		log.add("The user is navigated to the Sign In Page.");
		BNBasicCommonMethods.waitforElement(wait, obj, "signInIframe");
		//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInIframe));
		WebElement signInIframe = BNBasicCommonMethods.findElement(driver, obj, "signInIframe");
		driver.switchTo().frame(signInIframe);
		WebElement SignInEmail = BNBasicCommonMethods.findElement(driver, obj, "SignInEmail");
		WebElement SignInPass = BNBasicCommonMethods.findElement(driver, obj, "SignInPass");
		WebElement secureSignIn = BNBasicCommonMethods.findElement(driver, obj, "secureSignIn");
		//sheet = BNBasicCommonMethods.excelsetUp("Edit ShipandBill Address");
		int bniakey = sheet.getLastRowNum();
		for(int i = 0; i <= bniakey; i++)
		{	
			String tcid = "BRM Sign In No Shipping Address";
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				String Username = sheet.getRow(i).getCell(2).getStringCellValue().toString();
				String passw = sheet.getRow(i).getCell(3).getStringCellValue().toString();
				try 
				{
					if(BNBasicCommonMethods.isElementPresent(SignInEmail))
					{
						SignInEmail.sendKeys(Username);
						log.add("The User Name is successfully entered.");
						if(BNBasicCommonMethods.isElementPresent(SignInPass))
						{
							SignInPass.sendKeys(passw);
						//	Thread.sleep(3000);
							log.add("The password is successfully entered.");
								 if(BNBasicCommonMethods.isElementPresent(secureSignIn))
								 {
									Thread.sleep(1000);
									secureSignIn.click();
									log.add("Successful enter in Checkout page.");
									BNBasicCommonMethods.waitforElement(wait, obj, "Signoutbtn");
									//Thread.sleep(1000);
									
										WebElement CheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutBtn");
										if(BNBasicCommonMethods.isElementPresent(CheckoutBtn))
										{
	
											System.out.println("In Checkout Flow");
											Thread.sleep(1000);
										   //CheckoutBtn.click();
											log.add("Successful Enter in Add a Shipping and Billing Address page.");
											/*try 
											{*/
											pass("Page navigated to checkout edit billing and shipping address",log);
												//driver.navigate().refresh();
												CheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutBtn");
												//Thread.sleep(1000);
												((JavascriptExecutor) driver).executeScript("arguments[0].click();", CheckoutBtn);
												//String CurrentUrl =  driver.getCurrentUrl();
												//CheckoutBtn.click();
												//driver.findElement(By.name("s")).sendKeys("\uE035");
												//driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
											//	driver.navigate().refresh();
												//
												//System.out.println("In try for Checkout page");
										/*		
											}
											
											catch(Exception e )
											{
												System.out.println("in catch block");
												String CurrentUrl =  driver.getCurrentUrl();
												driver.navigate().refresh();
												driver.get(driver.getCurrentUrl());
												driver.navigate().to(driver.getCurrentUrl());
												driver.findElement(By.id("Contact-us")).sendKeys(Keys.F5); 
												String CurrentUrl =  driver.getCurrentUrl();
												driver.findElement(By.name("s")).sendKeys("\uE035");
												Thread.sleep(1000);
												//driver.get(CurrentUrl);
												Thread.sleep(2000);
											}*/
										//	BNBasicCommonMethods.WaitForLoading(wait);
								  }

									
																	 }
								 else
								 {
									 fail("NO Successfull login to Checkout page");
								 }
						}
						else
						{
							fail("password field not found.");
							
						}
					}
					else
					{
						fail("Email Id field not found.");
						
					}
				}
				catch(Exception e)
				{
					System.out.println(e.getMessage());
					exception("Sign In to the User Account in Checkout Exception");
					fail(e.getMessage());
				}
			}
		}
	}
	
	// Color Finder	
	public String colorfinder(WebElement element)
	  {
		String ColorName = element.getCssValue("background-color");
		Color colorhxcnvt = Color.fromString(ColorName);
		String hexCode = colorhxcnvt.asHex();
		return hexCode;
	  }
			
	
 /******************************************************* Shopping Bag *********************************************************/
  	
	/*BNIA-782 Verify that, "Continue Shopping" button should be shown, when no products is available in "Shopping Bag" page*/
	public void continuebtn() throws Exception
	{
		try
		{
		ChildCreation("Checkout Page should be Displayed with Continue Shopping Button when no items added in bag");
		BNBasicCommonMethods.WaitForLoading(wait);
		BNBasicCommonMethods.waitforElement(wait, obj, "checkoutTile");
		WebElement CheckoutTile = BNBasicCommonMethods.findElement(driver, obj, "checkoutTile");
		wait.until(ExpectedConditions.elementToBeClickable(CheckoutTile));
		CheckoutTile.click();
		Thread.sleep(500);
		boolean Continueshoppingbtn = false;
		Continueshoppingbtn  = BNBasicCommonMethods.waitforElement(wait, obj, "Continueshoppingbtn");
		if(Continueshoppingbtn)
		{
			WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
  			BNLogo.click();
			pass("Checkout page is displayed with continue shoppig button");
		}
		
		else
		{
			WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
  			BNLogo.click();
			fail("Checkout page is not displayed with continue shopping button");
		}
		}
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
	}
	
	/*BNIA-783 Verify that added products should be shown in the "Shopping Bag" page.*/
  	public void AddedProduct()
  	{
  		ChildCreation("BNIA-783 Verify that added products should be shown in the Shopping Bag page.");
  		try
  		{
  			sheet = BNBasicCommonMethods.excelsetUp("shoppingbag");
	  		String EANs = BNBasicCommonMethods.getExcelVal("BNIA126", sheet, 3);
	  		String[] EAN = EANs.split(":");
  		   	Thread.sleep(500);
  		   	BNBasicCommonMethods.waitforElement(wait, obj, "SearchTile");
  		   	WebElement SearchTile = BNBasicCommonMethods.findElement(driver, obj, "SearchTile");
  		   	SearchTile.click();
  		   	for(int ctr = 0 ; ctr<EAN.length ;ctr++)
  		   	{
	  		   	Thread.sleep(500);
	  		   	BNBasicCommonMethods.waitforElement(wait, obj, "SearchTxtBx");
	  		    WebElement SearchTxtBx = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBx");
	  		    WebElement SearchTxtBxnew = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");	
	  		    Actions action = new Actions(driver);	
	  		    	SearchTxtBx.click();
		  			action.moveToElement(SearchTxtBx).click();
		  			SearchTxtBxnew.clear();
		  			SearchTxtBxnew.sendKeys(EAN[ctr]);
		  			//action.moveToElement(SearchTxtBx).sendKeys(EAN[ctr]).build().perform();
		  			System.out.println(EAN[ctr]);
		  			Thread.sleep(500);	
		  			SearchTxtBxnew.sendKeys(Keys.ENTER);
		  		    Thread.sleep(1000);
		  			String str = driver.getCurrentUrl();
  					driver.get(str);
		  			Thread.sleep(1000);
		  				int counter=1;
		  				do
		  				{
			  				try
			  				{
			  					BNBasicCommonMethods.waitforElement(wait, obj, "ATBBtn");
					  			WebElement ATBBtn = BNBasicCommonMethods.findElement(driver, obj, "ATBBtn");
					  			Thread.sleep(1000);
			  					ATBBtn.click();
			  					Thread.sleep(2000);
			  					BNBasicCommonMethods.waitforElement(wait, obj, "ATBSuccessAlertOverlay");
				  				WebElement ATBSuccessAlertOverlay = BNBasicCommonMethods.findElement(driver, obj, "ATBSuccessAlertOverlay");
				  				counter=5;
			  				}
			  				
			  				catch(Exception e)
			  				{
			  					WebElement ATBErrorAlt = driver.findElement(By.xpath("//*[@id='skMob_ErrorsDiv_id']"));
			  					if(ATBErrorAlt.isDisplayed())
			  					{
			  						WebElement ErroBtn = driver.findElement(By.xpath("//*[@id='skMob_ErrorsOK_id']"));
			  						ErroBtn.click();
			  					}
			  					String str1 = driver.getCurrentUrl();
			  					driver.get(str1);
			  			    }
			  				counter++;
		  				} while(counter == 5 );
		  				
		  				WebElement ATBSuccessAlertOverlay = BNBasicCommonMethods.findElement(driver, obj, "ATBSuccessAlertOverlay");
		  				if(BNBasicCommonMethods.isElementPresent(ATBSuccessAlertOverlay))
		  				{
		  					BNBasicCommonMethods.waitforElement(wait, obj, "ContineShoppingBtn");
		  					WebElement ContineShoppingBtn = BNBasicCommonMethods.findElement(driver, obj, "ContineShoppingBtn");
		  					ContineShoppingBtn.click();
		  					BNBasicCommonMethods.waitforElement(wait, obj, "BackIcon");
		  					WebElement BackIcon = BNBasicCommonMethods.findElement(driver, obj, "BackIcon");
		  					if(BackIcon.isDisplayed())
		  					{
		  						log.add("Product added to bag");
		  						BackIcon.click();
		  						Thread.sleep(500);
		  						BNBasicCommonMethods.waitforElement(wait, obj, "SearchTxtBx");
		  					}
		  					else
		  					{
		  						fail("Not added to Bag",log);
		  					}
		  				}
		  				
		  				else
		  				{
		  					fail("Add to Bag Success alert overlay is not displayed",log);
		  				}		  			
  		   	}
  		   	pass("Items added to Bag",log);
  /*			sheet = BNBasicCommonMethods.excelsetUp("TopMenu navigation");
	  		String EANs = BNBasicCommonMethods.getExcelNumericVal("BNIA126", sheet, 3);
            String [] EAN = EANs.split(":");*/
  		   	BNBasicCommonMethods.waitforElement(wait, obj, "BagIconHeader");
            WebElement BagIconHeader = BNBasicCommonMethods.findElement(driver, obj, "BagIconHeader");
            BagIconHeader.click();
            Thread.sleep(500);
            BNBasicCommonMethods.waitforElement(wait, obj, "EmptyBagCountHead");
            WebElement EmptyBagCountHead = BNBasicCommonMethods.findElement(driver, obj, "EmptyBagCountHead");
            String script = "return $('#bag_count').text()";
		    String n = (String) ((JavascriptExecutor) driver).executeScript(script, EmptyBagCountHead);
		    String []prdtcnt = n.split(" ");
            //Thread.sleep(2000);
  			int Count = Integer.parseInt(prdtcnt[0]);
  			if(Count > 0 )
  			{
  				log.add("Bag count is greater than zero :"+Count);
  				/*BagIconHeader = BNBasicCommonMethods.findElement(driver, obj, "BagIconHeader");
  				BagIconHeader.click();*/
  				Thread.sleep(1000);
  				BNBasicCommonMethods.waitforElement(wait, obj, "ItemsHeading");
  				List <WebElement> Productcount = driver.findElements(By.xpath("//*[@class='product-title']")); 
  				System.out.println(Productcount.size());
  				for(int ctr = 1 ; ctr < Productcount.size()+1  ; ctr++)
  				{
  					for(int ctr1 = 0 ; ctr1 < EAN.length; ctr1++)
  					{
	  					WebElement ProductsCount = driver.findElement(By.xpath("(//*[@class='product-title'])["+ctr+"]"));
	  					String prodhref = ProductsCount.getAttribute("href");
	  					String[] compareean = prodhref.split("ean=");
	  					System.out.println(compareean[1] +"Provided EAN"+EAN[ctr1]);
	  					//System.out.println();
	  					if(compareean[1].contains(EAN[ctr1]))
	  					{
	  						log.add("Added product is available in the bag and the respective product EAN is"+EAN[ctr1]);
	  					}
	  					else
	  					{
	  						//log.add("Added product is not available in the bag and the respective product EAN is"+EAN[ctr1]);
	  					}
  					}
  				}
  				
  				pass("Added product is displayed in the bag",log);
  				Thread.sleep(1000);
  			}
  			
  			else
  			{
  				fail(" Bag count is not dislpayed :"+Count);
  			}
  			
  			/*WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
  			BNLogo.click();*/
  			//Thread.sleep(2000);
  		}
  		
  		catch(Exception e)
		{
  			System.out.println(e.getMessage());
			exception("There is something went wrong , please verify"+e.getMessage());
		}
  	}
  	
	/*BNIA-797 Verify that, "Checkout" button should be shown in the Order Summary section, in the Shopping Bag page.*/
	public void ordersummar()
	{
		ChildCreation("BNIA-797 Verify that, Checkout button should be shown in the Order Summary section, in the Shopping Bag page.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "LoginCheckoutBtn");
			WebElement LoginCheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "LoginCheckoutBtn");
			if(LoginCheckoutBtn.isDisplayed())
			{
				pass("Checkout Order Summary section is displayed in shopping bag page ");
			}
			else
			{
				fail("Checkout Order Summary section is not displayed in shopping bag page ");
			}
				
		}
		catch(Exception e )
		{
			exception("Exception something went wrong"+e.getMessage());
		}
	}
	
	/*BNIA-798 Verify that, "Checkout" button should be highlighted in RED color.*/
	public void ordersummarcolor()
	{
		ChildCreation("BNIA-798 Verify that, Checkout button should be highlighted in RED color.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("shoppingbag");
			String Expectedcolor = BNBasicCommonMethods.getExcelVal("BNIA798", sheet, 4);
			BNBasicCommonMethods.waitforElement(wait, obj, "LoginCheckoutBtn");
			WebElement LoginCheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "LoginCheckoutBtn");
			String Currentcolor  = colorfinder(LoginCheckoutBtn);
			if(Currentcolor.equals(Expectedcolor))
			{
				log.add("Current color is "+Currentcolor);
				log.add("Expected color is "+Expectedcolor);
				pass("Checkout Order Summary section is displayed in shopping bag page ",log);
			}
			else
			{
				System.out.println(Currentcolor+" :  "+Expectedcolor);
				log.add("Current color is "+Currentcolor);
				log.add("Expected color is "+Expectedcolor);
				fail("Checkout Order Summary section is not displayed in shopping bag page ",log);
			}
				
		}
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
	}
	
	/*BNIA-799 Verify that, below Order Summary section, "Apply Coupon Code" textbox should be shown along with the "Apply" button*/
	public void Couponcode()
	{
		try 
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "CouponcodeBox");
			WebElement CouponcodeBox =  BNBasicCommonMethods.findElement(driver, obj, "CouponcodeBox");
			BNBasicCommonMethods.waitforElement(wait, obj, "CouponApplybtn");
			WebElement CouponApplybtn = BNBasicCommonMethods.findElement(driver, obj, "CouponApplybtn");
			if(CouponApplybtn.isDisplayed() && CouponcodeBox.isDisplayed())
			{
				pass("Apply Coupon Code text box and apply button is displayed");
			}
			else
			{
				fail("Apply Coupon Code text box and apply button are not displayed");
			}
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
	}

	/*BNIA-800 Verify that when the "Apply" button is cliked without entering the coupon code , then the alert message should be displayed.*/
	public void Couponcodeerror()
	{
		ChildCreation("BNIA-800 Verify that when the Apply button is cliked without entering the coupon code , then the alert message should be displayed.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("shoppingbag");
			String ExpectedAlert = BNBasicCommonMethods.getExcelVal("BNIA800", sheet, 5);
			BNBasicCommonMethods.waitforElement(wait, obj, "CouponApplybtn");
			WebElement CouponApplybtn = BNBasicCommonMethods.findElement(driver, obj, "CouponApplybtn");
			CouponApplybtn.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "CouponCodeErrorAlt");
			WebElement CouponCodeErrorAlt = BNBasicCommonMethods.findElement(driver, obj, "CouponCodeErrorAlt");
			String Currentalert =  CouponCodeErrorAlt.getText();
			if(ExpectedAlert.equals(Currentalert))
			{
				log.add("Current Alert is "+Currentalert);
				log.add("Expected Alert is "+ExpectedAlert);
		       pass("Empty coupon code alert is displayed",log);		
			}
			else
			{
				System.out.println(Currentalert+" :  "+ExpectedAlert);
				log.add("Current Alert is "+Currentalert);
				log.add("Expected Alert is "+ExpectedAlert);
				fail("Empty Coupon code is not displayed",log);
			}
		}
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
	}

	/*BNIA-801 Verify that, when the "Apply" button is clicked after entering the invalid coupon code, alert message should be shown.*/
	public void CouponcodeInvaliderror()
	{
		ChildCreation("BNIA-801 Verify that, when the Apply button is clicked after entering the invalid coupon code, alert message should be shown.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("shoppingbag");
			String Cuponcode = BNBasicCommonMethods.getExcelNumericVal("BNIA800", sheet, 6);
			BNBasicCommonMethods.waitforElement(wait, obj, "CouponcodeBox");
			WebElement CouponcodeBox =  BNBasicCommonMethods.findElement(driver, obj, "CouponcodeBox");
			CouponcodeBox.sendKeys(Cuponcode);
			BNBasicCommonMethods.waitforElement(wait, obj, "CouponApplybtn");
			WebElement CouponApplybtn = BNBasicCommonMethods.findElement(driver, obj, "CouponApplybtn");
			CouponApplybtn.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "CouponCodeErrorAlt");
			WebElement CouponCodeErrorAlt = BNBasicCommonMethods.findElement(driver, obj, "CouponCodeErrorAlt");
			String Currentalert =  CouponCodeErrorAlt.getText();
			if(!Currentalert.isEmpty())
			{
				log.add("Current Alert is "+Currentalert);
		       pass("coupon code alert is displayed"+Currentalert,log);		
			}
			else
			{
				System.out.println(Currentalert);
				log.add("Current Alert is "+Currentalert);
				fail("Coupon code is not displayed",log);
			}
		}
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
	}
	
	/*BNIA-802 Verify that, "Add $XXX of eligible items to qualify for FREE Shipping! - Details" text link should be shown in the top of the Shopping Bag page.*/
	public void headerfreeshippingdetails()
	{
		ChildCreation("BNIA-802 Verify that, Add $XXX of eligible items to qualify for FREE Shipping! - Details text link should be shown in the top of the Shopping Bag page.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "Freeshippingheaderdetails");
			WebElement Freeshippingheaderdetails = BNBasicCommonMethods.findElement(driver, obj, "Freeshippingheaderdetails");
			if(Freeshippingheaderdetails.isDisplayed())
			{
				log.add("Link in header is "+Freeshippingheaderdetails);
				pass("Details for free shipping link is available in header",log);
			}
			else
			{
				System.out.println(Freeshippingheaderdetails);
				log.add("Link in header is "+Freeshippingheaderdetails);
				fail("Details for free shipping link is not available in header",log);
			}
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
	}
	
	/*BNIA-803 Verify that, when "Details" link is clicked then the "Free Shipping" overlay should be shown. along with "X" icon.*/
	public void headerdetailalink()
	{
	  ChildCreation("BNIA-803 Verify that, when Details link is clicked then the Free Shipping overlay should be shown. along with X icon.");
	   try
	   {
		   
		   BNBasicCommonMethods.waitforElement(wait, obj, "haaderdetaillink");
		   WebElement haaderdetaillink = BNBasicCommonMethods.findElement(driver, obj, "haaderdetaillink");
		   haaderdetaillink.click(); 
		   BNBasicCommonMethods.waitforElement(wait, obj, "HeaderdetailOverlay");
		   BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutCloseIcon");
		   WebElement CheckoutCloseIcon = BNBasicCommonMethods.findElement(driver, obj, "CheckoutCloseIcon");
		   WebElement HeaderdetailOverlay = BNBasicCommonMethods.findElement(driver, obj, "HeaderdetailOverlay");
		   if(HeaderdetailOverlay.isDisplayed()&& CheckoutCloseIcon.isDisplayed()) 
		   {
			   pass("Free shippping detail overlay with close button is displayed");
		   }
		   else
		   {
			   fail("Free shippping detail overlay with close button is not displayed");
		   }
		   
	   }
		
	   catch(Exception e )
		{
		   System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
		
	}
	
	/*BNIA-804 Verify that, when the "X" icon in the "Free Shipping" overlay is clicked, then the overlay should be closed and should retain in the Shopping Bag page.*/
	public void detailoverlayclose()
	{
		ChildCreation("BNIA-804 Verify that, when the X icon in the Free Shipping overlay is clicked, then the overlay should be closed and should retain in the Shopping Bag page.");
		try
		{
			 WebElement CheckoutCloseIcon = BNBasicCommonMethods.findElement(driver, obj, "CheckoutCloseIcon");
			 CheckoutCloseIcon.click();
			 Thread.sleep(1000);
			 BNBasicCommonMethods.waitforElement(wait, obj, "CouponApplybtn");
			WebElement CouponApplybtn = BNBasicCommonMethods.findElement(driver, obj, "CouponApplybtn");
			if(CouponApplybtn.isDisplayed())
			{
				pass("Details overlay is closed on tapping close Icon in header");
			}
			else
			{
				fail("Details overlay is not closed on tapping close Icon in header");
			}
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
	}

	/*BNIA-784 Verify that, "Shopping Bag" page should display "Product Image","Title", "Author Name" and so on...*/
	public void ShoppingProductsList()
	{
		ChildCreation("BNIA-784 Verify that, Shopping Bag page should display Product Image,Title, Author Name and so on");
		try
		{
			Thread.sleep(1000);
			List <WebElement> productImage = driver.findElements(By.xpath("//*[@class='product-img']"));
			List <WebElement> productTitle = driver.findElements(By.xpath("//*[@class='product-desc']"));
			List <WebElement> productAuthor = driver.findElements(By.xpath("//*[@class='contributors']"));
			List <WebElement> productPrice = driver.findElements(By.xpath("//*[@class='item-price']"));
			List <WebElement> ProductTotalPrice = driver.findElements(By.xpath("//*[@class='total']//li"));
			
			for(int ctr=0 ; ctr < productImage.size() ; ctr++)
			{
				BNBasicCommonMethods.scrollup(productImage.get(ctr), driver);
				if(productImage.get(ctr).isDisplayed())
				{
					BNBasicCommonMethods.scrolldown(productImage.get(ctr), driver);
					log.add("Product Image is Displayed");
				}
				else
				{
					fail("Product Image is not Displayed");
				}
				
				if(productTitle.get(ctr).isDisplayed())
				{
					log.add("Product title is displayed"+productTitle.get(ctr).getText());
				}
				else
				{
					fail("Product title is not displayed");
				}
				
				if(productAuthor.get(ctr).isDisplayed())
				{
					log.add("Product Author Name is Displayed"+productAuthor.get(ctr).getText());
				}
				else
				{
					fail("Product Author Name is not Displayed");
				}
				
				if(productPrice.get(ctr).isDisplayed())
				{
					log.add("Product Price is Displayed "+productPrice.get(ctr).getText());
				}
				else
				{
					fail("Product Price is not Displayed ");
				}
				
				if(ProductTotalPrice.get(ctr).isDisplayed())
				{
					ProductPrice.add(ProductTotalPrice.get(ctr).getText());
					log.add("Product Total Price is Displayed"+ProductTotalPrice.get(ctr).getText());
				}
				else
				{
					fail("Product Total Price is not Displayed");
				}
				
			  pass("Added products shown ",log);
			}
			
			
			WebElement deliveryOptionCartTotalPrice = BNBasicCommonMethods.findElement(driver, obj, "deliveryOptionCartTotalPrice");
			EstimatedPrice = deliveryOptionCartTotalPrice.getText().toString();
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
		
		
	}
	
	/*BNIA-785 Verify that "Shopping Bag" page should shown "Update" ,"Remove", "Save for Later" text links in the "Quantity" column*/
	public void UpdateRemoveSaveforLater()
	{
		ChildCreation("BNIA-785 Verify that Shopping Bag page should shown Update ,Remove, Save for Later text links in the Quantity column");
		try
		{
			List <WebElement> ProdcutUpdate = driver.findElements(By.xpath("//*[@class='quantity']//*[contains(@type,'submit') and @class='text-link update-link']"));
			List <WebElement> ProdcutRemove = driver.findElements(By.xpath("//*[@class='remove-item']"));
			List <WebElement> ProductSaveLater = driver.findElements(By.xpath("//*[@class='quantity']//*[contains(@type,'submit') and @class='text-link']"));
			List <WebElement> productQty = driver.findElements(By.xpath("//*[contains(@class,'product-quantity') and @type='tel']"));
			
			for(int ctr=0; ctr<ProdcutUpdate.size();ctr++)
			{
				
				if(ProdcutUpdate.get(ctr).isDisplayed())
				{
					log.add("Prodcut Update Link is Displayed"+ProdcutUpdate.get(ctr).getText());
				}
				else
				{
					fail("Prodcut Update Link is not Displayed");
				}
				
				if(ProdcutRemove.get(ctr).isDisplayed())
				{
					log.add("Prodcut Remove Link is Displayed"+ProdcutRemove.get(ctr).getText());
				}
				else
				{
					fail("Prodcut Remove Link is not Displayed");
				}
				
				if(ProductSaveLater.get(ctr).isDisplayed())
				{
					log.add("Prodcut Remove Link is Displayed"+ProductSaveLater.get(ctr).getText());
				}
				else
				{
					fail("Prodcut Remove Link is not Displayed");
				}
				
				if(productQty.get(ctr).isDisplayed())
				{
					ProductQty.add(productQty.get(ctr).getAttribute("value").toString());
					log.add("Product Quantity is Displayed"+productQty.get(ctr).getAttribute("value").toString());
				}
				else
				{
				     fail("Product Quantity is not Displayed");	
				}
				
				pass("Product Links are displayed",log);
			}
			
			
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
		
	}
	
	/*BNIA-786 Verify that, "Update" ,"Remove", "Save for Later" text links in the "Quantity" column should be in green color and it should be underlined*/
	/*BNIA-1333 Verify that checkout page is displayed as per the creative*/
	public void LinkColorCheck()
	{
		ChildCreation("BNIA-786 Verify that, Update ,Remove, Save for Later text links in the Quantity column should be in green color and it should be underlined");
		boolean childflag = false;
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("shoppingbag");
			String Expectedcolor = BNBasicCommonMethods.getExcelVal("BNIA786", sheet, 4);
			List <WebElement> ProdcutUpdate = driver.findElements(By.xpath("//*[@class='quantity']//*[contains(@type,'submit') and @class='text-link update-link']"));
			List <WebElement> ProdcutRemove = driver.findElements(By.xpath("//*[@class='remove-item']//a"));
			List <WebElement> ProductSaveLater = driver.findElements(By.xpath("//*[@class='quantity']//*[contains(@type,'submit') and @class='text-link']"));
			List <WebElement> ProductTotalPrice = driver.findElements(By.xpath("//*[@class='total']//li//*[@class='total-price']"));
			int sel;
			Random r = new Random();
			sel = r.nextInt(ProdcutUpdate.size());
			String ProductUpcolor  = BNBasicCommonMethods.colorfinderForeColor(ProdcutUpdate.get(sel));
			String ProductRmcolor = BNBasicCommonMethods.colorfinderForeColor(ProdcutRemove.get(sel));
			String ProductSlColor = BNBasicCommonMethods.colorfinderForeColor(ProductSaveLater.get(sel));
			String TTlPriceColor = BNBasicCommonMethods.colorfinderForeColor(ProductTotalPrice.get(sel));
			if(ProductUpcolor.equals(Expectedcolor) && ProductRmcolor.equals(Expectedcolor) && ProductSlColor.equals(Expectedcolor) && TTlPriceColor.equals(Expectedcolor))
			{
				childflag = true;
			    pass("Link Colors are matched and as per creative");
			}
			else
			{
				fail("Link Colors are not matched");
			}
					
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
		
		ChildCreation("BNIA-1333 Verify that checkout page is displayed as per the creative");
		if(childflag)
			pass("Checkout page is displayed as per creative");
		else
			fail("Checkout page is not displayed as per creative");
	}

	/*BNIA-794 Verify that, the excepted delivery text should be highlighed in RED color.*/
	public void DeliveryTextColor()
	{
		ChildCreation("BNIA-794 Verify that, the excepted delivery text should be highlighed in RED color.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("shoppingbag");
			String Expectedcolor = BNBasicCommonMethods.getExcelVal("BNIA794", sheet, 4);
			WebElement ProductColor = driver.findElement(By.xpath("//*[@class='highlight']"));
			String CurrentColor = BNBasicCommonMethods.colorfinderForeColor(ProductColor);
			if(CurrentColor.equals(Expectedcolor))
			{
				pass("the excepted delivery text is highlighed in RED color as"+CurrentColor);
			}
			else
			{
				fail("the excepted delivery text is not highlighed in RED color as"+CurrentColor);
			}	
			
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
		
	}
	
	/*BNIA-787 Verify that, "Quantity" number should be editable in the "Shopping Bag" page.*/
	/*BNIA-1309 Checkout page: Verify that user can able to update the QTY in listed products in checkout page*/
	/*BNIA-1313 Checkout page: Make sure that scroll is working as expected to view all the products if user add more number of products*/
	public void QuantityUpdate()
	{
		boolean childflag = false;
		ChildCreation("BNIA-787 Verify that, Quantity number should be editable in the Shopping Bag page.");
		try
		{
			Thread.sleep(2000);
			sheet = BNBasicCommonMethods.excelsetUp("shoppingbag");
			String Updateval = BNBasicCommonMethods.getExcelNumericVal("BNIA787", sheet, 7);
			List <WebElement> productQty = driver.findElements(By.xpath("//*[contains(@class,'product-quantity')]"));
			List <WebElement> ProdcutUpdate = driver.findElements(By.xpath("//*[@class='quantity']//*[contains(@type,'submit') and @class='text-link update-link']"));
			int sel;
			Random r = new Random();
			sel = r.nextInt(ProdcutUpdate.size());
			
			if(productQty.get(sel).isDisplayed())
			{
				productQty.get(sel).click();
				productQty.get(sel).clear();
				productQty.get(sel).sendKeys(Updateval);
				productQty.get(sel).sendKeys(Keys.ENTER);
				Thread.sleep(1000);
				BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
				Thread.sleep(6000);
				productQty = driver.findElements(By.xpath("//*[contains(@class,'product-quantity')]"));
				if(productQty.get(sel).getAttribute("value").equals(Updateval))
				{
					childflag=true;
					pass("product Quantity is Updated");
				}
				else
				{
					System.out.println(productQty.get(sel).getText());
					fail("Product Qunatity is not updated");
				}
			}
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
		
		ChildCreation("BNIA-1309 Checkout page: Verify that user can able to update the QTY in listed products in checkout page");
		if(childflag)
			pass("product Quantity is Updated");
		else
			fail("Product Qunatity is not updated");
		
		ChildCreation("BNIA-1313 Checkout page: Make sure that scroll is working as expected to view all the products if user add more number of products");
		if(childflag)
			pass("product Quantity is Updated");
		else
			fail("Product Qunatity is not updated");
			
	}
	
	/*BNIA-788 Verify that on selecting the "Update" link, the product quantity should be updated.*/
	public void QuantityUpdateLink()
	{
		ChildCreation("BNIA-788 Verify that on selecting the Update link, the product quantity should be updated.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("shoppingbag");
			String Updateval = BNBasicCommonMethods.getExcelNumericVal("BNIA788", sheet, 7);
			List <WebElement> productQty = driver.findElements(By.xpath("//*[contains(@class,'product-quantity')]"));
			List <WebElement> ProdcutUpdate = driver.findElements(By.xpath("//*[@class='quantity']//*[contains(@type,'submit') and @class='text-link update-link']"));
			int sel,usel;
			Random r = new Random();
			sel = r.nextInt(ProdcutUpdate.size());
			//sel = 2;
			usel=sel;
			String UpdateType = ProdcutUpdate.get(sel).getAttribute("type");
			
			if(productQty.get(sel).isDisplayed())
			{
				productQty.get(sel).click();
				productQty.get(sel).clear();
				productQty.get(sel).sendKeys(Updateval);
				
				UpdateType = ProdcutUpdate.get(sel).getAttribute("type");
				if(UpdateType.equals("submit"))
				{
					ProdcutUpdate.get(sel).click();
					Thread.sleep(1000);
					BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
					Thread.sleep(5000);
					BNBasicCommonMethods.waitforElement(wait, obj, "productQty");
					productQty = driver.findElements(By.xpath("//*[contains(@class,'product-quantity')]"));
					if(productQty.get(sel).getAttribute("value").toString().equals(Updateval))
						pass("product Quantity is Updated");
					else
						fail("Product Qunatity is not updated");
				}
				else
				{
					fail("No update button is available");
				}
			}
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
		
	}
	
	/*BNIA-815 Verify that on adding more products to bag(more than 50) ,the checkout page functionality flow does not get affected*/
	public void QuantityLimit()
	{
		ChildCreation("BNIA-815 Verify that on adding more products to bag(more than 50) ,the checkout page functionality flow does not get affected");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("shoppingbag");
			String Updateval = BNBasicCommonMethods.getExcelNumericVal("BNIA815", sheet, 7);
			BNBasicCommonMethods.waitforElement(wait, obj, "EmptyBagCountHead");
			WebElement EmptyBagCountHead = BNBasicCommonMethods.findElement(driver, obj, "EmptyBagCountHead");
			
			List <WebElement> productQty = driver.findElements(By.xpath("//*[contains(@class,'product-quantity')]"));
			List <WebElement> ProdcutUpdate = driver.findElements(By.xpath("//*[@class='quantity']//*[contains(@type,'submit') and @class='text-link update-link']"));
			int sel;
			Random r = new Random();
			sel = r.nextInt(ProdcutUpdate.size());
			String BeforeCount  = EmptyBagCountHead.getText();
			String UpdateType = ProdcutUpdate.get(sel).getAttribute("type");
			if(productQty.get(sel).isDisplayed())
			{
				productQty.get(sel).click();
				productQty.get(sel).clear();
				productQty.get(sel).sendKeys(Updateval);
				ProdcutUpdate.get(sel).click();
				Thread.sleep(1000);
				BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "productQty");
				productQty = driver.findElements(By.xpath("//*[contains(@class,'product-quantity')]"));
				/*if(productQty.get(sel).getAttribute("value").toString().equals(Updateval))
				{
					pass("product Quantity is Updated");
				}
				else
				{
					fail("Product Qunatity is not updated");
				}*/
				EmptyBagCountHead = BNBasicCommonMethods.findElement(driver, obj, "EmptyBagCountHead");
				if(!EmptyBagCountHead.getText().equals(BeforeCount))
				{
					pass("Total Price get Updated while updating the product in the shopping bag page");
				}
				
				else
				{
					WebElement QuantityUpdateError = BNBasicCommonMethods.findElement(driver, obj, "QuantityUpdateError");
					if(QuantityUpdateError.isDisplayed())
					{
						Skip("Quantity is not Updated onlly limited stock available error message is displayed "+QuantityUpdateError.getText());
					}
					else
					{
						fail("Product is not updated to ");
					}
				}
			}
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
		
		
	}
	
	/*BNIA-789 Verify that, when the quantity is updated, the total price should also get updated.*/
	public void PriceUpdateCheck()
	{
		ChildCreation("BNIA-789 Verify that, when the quantity is updated, the total price should also get updated.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("shoppingbag");
			String Updateval = BNBasicCommonMethods.getExcelNumericVal("BNIA789", sheet, 7);
			BNBasicCommonMethods.waitforElement(wait, obj, "topOrderTotal");
			List <WebElement> ProductTotalPrice = driver.findElements(By.xpath("//*[@class='total']//li"));
			
			List <WebElement> productQty = driver.findElements(By.xpath("//*[contains(@class,'product-quantity')]"));
			List <WebElement> ProdcutUpdate = driver.findElements(By.xpath("//*[@class='quantity']//*[contains(@type,'submit') and @class='text-link update-link']"));
			int sel,usel;
			Random r = new Random();
			sel = r.nextInt(ProdcutUpdate.size());
			usel=sel;
			String BeforePrice  = ProductTotalPrice.get(sel).getText();
			String UpdateType = ProdcutUpdate.get(usel).getAttribute("type");
			if(productQty.get(sel).isDisplayed())
			{
				productQty.get(sel).click();
				productQty.get(sel).clear();
				productQty.get(sel).sendKeys(Updateval);
				UpdateType = ProdcutUpdate.get(sel).getAttribute("type");
				ProdcutUpdate.get(sel).click();
				Thread.sleep(500);
				BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "productQty");
				productQty = driver.findElements(By.xpath("//*[contains(@class,'product-quantity')]"));
				if(productQty.get(sel).getAttribute("value").toString().equals(Updateval))
				{
					pass("product Quantity is Updated");
				}
				else
				{
					fail("Product Qunatity is not updated");
				}
				ProductTotalPrice = driver.findElements(By.xpath("//*[@class='total']//li"));
				if(!ProductTotalPrice.get(sel).getText().equals(BeforePrice))
				{
					pass("Total Price get Updated while updating the product in the shopping bag page");
				}
				
				else
				{
					Skip("Total price is not get updated while updating the product in the shopping bag page");
				}
			}
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
		
	}

	/*BNIA-796 Verify that, the Order Summary section should get updated, when there is a change in the Shopping Bag page.*/
	/*BNIA-1320 Checkout page : Subtotal of the added items should be displayed in checkout page and it should matched with the shopping bag page*/
	public void PriceUpdateOrderSumary()
	{
		boolean childflag = false;
		ChildCreation("BNIA-796 Verify that, the Order Summary section should get updated, when there is a change in the Shopping Bag page.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("shoppingbag");
			String Updateval = BNBasicCommonMethods.getExcelNumericVal("BNIA787", sheet, 7);
			BNBasicCommonMethods.waitforElement(wait, obj, "topOrderTotal");
			WebElement topOrderTotal = BNBasicCommonMethods.findElement(driver, obj, "topOrderTotal");
			
			List <WebElement> productQty = driver.findElements(By.xpath("//*[contains(@class,'product-quantity')]"));
			List <WebElement> ProdcutUpdate = driver.findElements(By.xpath("//*[@class='quantity']//*[contains(@type,'submit') and @class='text-link update-link']"));
			int sel,usel;
			Random r = new Random();
			sel = r.nextInt(ProdcutUpdate.size());
			//usel=sel;
			String BeforePrice  =topOrderTotal.getText();
		//	String UpdateType = ProdcutUpdate.get(usel).getAttribute("type");
			if(productQty.get(sel).isDisplayed())
			{
				productQty.get(sel).click();
				productQty.get(sel).clear();
				productQty.get(sel).sendKeys(Updateval);
				ProdcutUpdate.get(sel).click();
				Thread.sleep(1000);
				BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "productQty");
				productQty = driver.findElements(By.xpath("//*[contains(@class,'product-quantity')]"));
				if(productQty.get(sel).getAttribute("value").toString().equals(Updateval))
				{
					pass("product Quantity is Updated");
				}
				else
				{
					fail("Product Qunatity is not updated");
				}
				topOrderTotal = BNBasicCommonMethods.findElement(driver, obj, "topOrderTotal");
				if(!topOrderTotal.getText().equals(BeforePrice))
				{
					childflag=true;
					pass("Total Price get Updated while updating the product in the shopping bag page");
				}
				
				else
				{
					Skip("Total price is not get updated while updating the product in the shopping bag page");
				}
			}
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
		
		ChildCreation("BNIA-1320 Checkout page : Subtotal of the added items should be displayed in checkout page and it should matched with the shopping bag page");
		if(childflag)
			pass("Total Price get Updated while updating the product in the shopping bag page");
		else
			Skip("Total price is not get updated while updating the product in the shopping bag page");
		
	}
	
	/*BNIA-1315 Checkout page: Make sure that total price is correctly displayed as per the added products*/
	public void CheckTotalPrice()
	{
		ChildCreation("BNIA-1315 Checkout page: Make sure that total price is correctly displayed as per the added products");
		try
		{
			float counttotal = 0;
			String str = null;
			List <WebElement> ProductTotalPrice = driver.findElements(By.xpath("//*[@class='total']//li"));
			
			for(int ctr=0 ; ctr < ProductTotalPrice.size() ; ctr++)
			{
				String Intcon = ProductTotalPrice.get(ctr).getText().toString();
				String PrpductPricecount = Intcon.replaceAll("[$,]", "");
				/*if(PrpductPricecount.contains(".00"))
					counttotal = counttotal + Integer.parseInt(PrpductPricecount);*/
				counttotal = counttotal + Float.parseFloat(PrpductPricecount);
			}
			str = new DecimalFormat("##.##").format(counttotal);
		 WebElement Subtotalval = driver.findElement(By.xpath("(//*[@id='orderSummary']//li)[1]"));
		 if(Subtotalval.getText().replaceAll("[$,]", "").contains(str))
		 {
			 System.out.println(Subtotalval.getText().replaceAll("[$,]", ""));
			 pass("Order subtotal price is matched with total price of all products");
		 }
		 else
		 {
			 System.out.println(Subtotalval.getText().replaceAll("[$,]", ""));
			 fail("Order subtotal price is not matched with total price of all products");
		 }
			
			
			
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
	}
	
	/*BNIA-791 Verify that on selecting the "Save for Later" link, the product should be removed from the Shopping Bag and should be shown below the "My Saved Items" section in the Shopiing Bag page.*/
	/*BNIA-1319 Checkout page: Make sure that product is removed from the checkout page if user selects "SAVE FOR LATER" in shoppingbag page*/
	public void SaveForLater()
	{
	
		ChildCreation("BNIA-791 Verify that on selecting the Save for Later link, the product should be removed from the Shopping Bag and should be shown below the My Saved Items section in the Shopiing Bag page.");
		try
		{
			String BeforeURL = null ;
			BNBasicCommonMethods.waitforElement(wait, obj, "deliveryOptionCartTotalPrice");
			WebElement deliveryOptionCartTotalPrice = BNBasicCommonMethods.findElement(driver, obj, "deliveryOptionCartTotalPrice");
			EstimatedPrice = deliveryOptionCartTotalPrice.getText().toString();
			List <WebElement> ProductSaveLater = driver.findElements(By.xpath("//*[@class='quantity']//*[contains(@type,'submit') and @class='text-link']"));
			List <WebElement> ProdcutImageUrl  = driver.findElements(By.xpath("//*[@class='product-img']"));
			Random r = new Random();
			
			int sel = r.nextInt(ProductSaveLater.size());
			int size = 0;
			if(ProductSaveLater.size() >=2)
				size = ProductSaveLater.size()-2;
			String ProductSaveType = ProductSaveLater.get(sel).getAttribute("type");
			if(ProductSaveType.equals("submit"))
			{
					BeforeURL = ProdcutImageUrl.get(sel).getAttribute("href").toString();
					ProductSaveLater.get(sel).click();
					BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
				    Thread.sleep(2000);
				    BNBasicCommonMethods.waitforElement(wait, obj, "productQty");
				    ProductSaveLater = driver.findElements(By.xpath("//*[@class='quantity']//*[contains(@type,'submit') and @class='text-link']"));
				    WebElement scrollele = ProductSaveLater.get(size);
				    BNBasicCommonMethods.scrolldown(scrollele, driver);
					WebElement SavedItems = driver.findElement(By.xpath("//*[@id='savedItems']"));
					if(SavedItems.isDisplayed())
					{
						pass("On Tapping Save for later in shopping bag then product is saved to bag ");
						List <WebElement> ProductEan = driver.findElements(By.xpath("//*[@class='carousel-image-link']"));
						for(int ctr = 0; ctr<ProductEan.size() ; ctr++)
						{
							String AfterSaveURL = ProductEan.get(ctr).getAttribute("href").toString();
							if(AfterSaveURL.equals(BeforeURL))
							{
								pass("On Tapping Save for later link product product removed form the bag");
							}
							/*else
							{
								System.out.println(AfterSaveURL+""+BeforeURL);
								fail("On tapping Save for later link product product is not removed form the bag");
							}*/
						}
					}
			}
			else
			{
				fail("Save for later button is not available");
			}
			ChildCreation("BNIA-1319 Checkout page: Make sure that product is removed from the checkout page if user selects SAVE FOR LATER in shoppingbag page");
			if(ProductSaveLater.contains(BeforeURL))
			{
				fail("Product Still Available in shopping bag");
			}
			else
			{
				pass("On selecting Save for Later porduct is not removed from shopping Bag");
			}
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
		
	}

	/*BNIA-792 Verify that, "My  Saved Items" section should shown "Add to Bag" button and "Remove" buton*/
	public void SaveItemDetails()
	{
		ChildCreation("BNIA-792 Verify that, My Saved Items section should shown Add to Bag button and Remove buton");
		try
		{
			List <WebElement> ProductEan = driver.findElements(By.xpath("//*[@class='caroufredsel_wrapper']//dt"));
			Random r = new Random();
			//int sel = r.nextInt(ProductEan.size());
			List <WebElement> ProductATB = driver.findElements(By.xpath("//*[@class='addSavedToBag']"));
			List <WebElement> ProductRem = driver.findElements(By.xpath("//*[@value='Remove']"));
			int size = r.nextInt(ProductEan.size());
			if(size >= ProductEan.size())
				size = ProductEan.size()-1;
			WebElement scrollele = ProductEan.get(size);
			BNBasicCommonMethods.scrolldown(scrollele, driver);
			if(ProductATB.get(size).isDisplayed() && ProductRem.get(size).isDisplayed())
			{
				pass("ADD to Bag Button and remove Link is displayed");
			}
			else
			{
				fail("ADD to Bag Button and remove link is not displayed");
			}
			
 			
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
		
	}
	
	/*BNIA-790 Verify that. the corresponding product should be removed from the "Shopping Bag" page, when the "Remove" link is clicked*/
	public void RemoveLinkCheck()
	{
		ChildCreation("BNIA-790 Verify that the corresponding product should be removed from the Shopping Bag page, when the Remove link is clicked");
		try
		{
			String BeforeURL = null;
			List <WebElement> ProdcutRemove = driver.findElements(By.xpath("//*[@class='remove-item']"));
			Random r = new Random();
			System.out.println(ProdcutRemove.size());
			int sel = r.nextInt(ProdcutRemove.size());
			if(sel >= ProdcutRemove.size())
				sel= sel-1;
			//BNBasicCommonMethods.scrollup(ProdcutRemove.get(sel), driver);
			if(ProdcutRemove.get(sel).isDisplayed())
			{
				BeforeURL = driver.findElement(By.xpath("(//*[@class='product-img'])["+(sel+1)+"]")).getAttribute("href").toString();
				/*WebElement scrollele = ProdcutRemove.get(sel);
				BNBasicCommonMethods.scrollup(scrollele, driver);*/
				//Thread.sleep(1500);
				ProdcutRemove.get(sel).click();
				Thread.sleep(3500);
				BNBasicCommonMethods.waitforElement(wait, obj, "productQty");
				BNBasicCommonMethods.waitforElement(wait, obj, "topOrderTotal");
				if(sel==0)
					sel=sel+1;
				try
				{
				    if(driver.findElement(By.xpath("(//*[@class='product-img'])["+sel+"]")).getAttribute("href").toString().contains(BeforeURL))
					{
						System.out.println(BeforeURL);
						System.out.println(driver.findElement(By.xpath("(//*[@class='product-img'])["+(sel+1)+"]")).getAttribute("href").toString());
						fail("Product is not Removed from the Bag");
					}
					else
					{
						System.out.println(BeforeURL);
						System.out.println(driver.findElement(By.xpath("(//*[@class='product-img'])["+sel+"]")).getAttribute("href").toString());
						pass("Product is Removed from the Bag");
					}
				
				}
			    catch(Exception e)
				{
			    	System.out.println(BeforeURL);
					System.out.println(driver.findElement(By.xpath("(//*[@class='product-img'])["+sel+"]")).getAttribute("href").toString());
					pass("Product is Removed from the Bag");
				}
			}
			else
			{
				fail("Product Remove Link is not Displayed");
			}
		}
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}	
	}
	 
	/*BNIA-1332 Reg Checkout page : Verify that onclicking product image/title,It is redirected to the custom pdp page*/
	public void PDPNavigation()
	{
		ChildCreation("BNIA-1332 Reg Checkout page : Verify that onclicking product image/title,It is redirected to the custom pdp page");
		try
		{
			List <WebElement> ProductImage = driver.findElements(By.xpath("//*[@class='product-img']"));
			Random r = new Random();
			int sel = r.nextInt(ProductImage.size());
			if(ProductImage.get(sel).isDisplayed())
			{
				Thread.sleep(2000);
				ProductImage.get(sel).click();
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductName");
				WebElement PDP_ProductName = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductName");
				if(PDP_ProductName.isDisplayed())
				{
					pass("Page Navigated to PDP page");
				}
				else
				{
					fail("Page not Navigated to PDP page");
				}
			}
			else
			{
				fail("No Product Image is found");
			}
			Thread.sleep(4000);
			BNBasicCommonMethods.waitforElement(wait, obj, "BagIconHeader");
			WebElement BagIconHeader = BNBasicCommonMethods.findElement(driver, obj, "BagIconHeader");
			BagIconHeader.click();
			Thread.sleep(3000);
			BNBasicCommonMethods.waitforElement(wait, obj, "productQty");
			/*ProductImage = driver.findElements(By.xpath("//*[@class='product-img']"));
			WebElement Scrollele = ProductImage.get(ProductImage.size()-1);
			BNBasicCommonMethods.scrolldown(Scrollele, driver);
			List <WebElement> ProductTitle = driver.findElements(By.xpath("//*[@class='product-desc']//a"));
			if(ProductTitle.get(sel).isDisplayed())
			{
				ProductTitle.get(sel).click();
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductName");
				WebElement PDP_ProductName = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductName");
				if(PDP_ProductName.isDisplayed())
				{
					pass("Page Navigated to PDP page");
				}
				else
				{
					fail("Page not Navigated to PDP page");
				}
			}
			
			else
			{
				fail("No Product Title is found ");
			}*/
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
		
		
		
	}
	
	/*BNIA-816 Guest user: Verify that "Are you a returning Customer? Sign In" text with link should be shown.*/
	public void SigninLinkInheader()
	{
		ChildCreation("BNIA-816 Guest user: Verify that  Sign In text with link should be shown.");
		try
		{
			Thread.sleep(3000);
			BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
			Thread.sleep(2000);
			BNBasicCommonMethods.waitforElement(wait, obj, "productQty");
			WebElement CheckoutSignIn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutSignIn");
			//wait.until(ExpectedConditions.visibilityOf(CheckoutSignIn));
			Thread.sleep(1000);
			if(CheckoutSignIn.isDisplayed())
			{
				pass("Sign In text is present in checkout page "+CheckoutSignIn.getText());
			}
			else
			{
				fail("Sign In text is not present in checkout page "+CheckoutSignIn.getText());
			}
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
	}
	
	/*BNIA-793 Verify that, for "Nook Books" the "Update" link should not be shown and the quantity number should be uneditable.*/
	public void NookBook() throws Exception
	{
		ChildCreation("BNIA-793 Verify that, for Nook Books the Update link should not be shown and the quantity number should be uneditable.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("shoppingbag");
	  		String EAN = BNBasicCommonMethods.getExcelVal("BNIA793", sheet, 3);
			BNBasicCommonMethods.waitforElement(wait, obj, "SearchTile");
			WebElement SearchTile = BNBasicCommonMethods.findElement(driver, obj, "SearchTile");
			SearchTile.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "SearchIcon");
			//String EAN  = "9780316408769";
			WebElement SearchIcon = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
			SearchIcon.click();
			WebElement SearchTxtBx = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBx");
			    WebElement SearchTxtBxnew = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");	
			    Actions action = new Actions(driver);	
			    	SearchTxtBx.click();
	  			action.moveToElement(SearchTxtBx).click();
	  			SearchTxtBxnew.clear();
	  			SearchTxtBxnew.sendKeys(EAN);
	  			//action.moveToElement(SearchTxtBx).sendKeys(EAN[ctr]).build().perform();
	  			System.out.println(EAN);
	  			Thread.sleep(500);	
	  			SearchTxtBxnew.sendKeys(Keys.ENTER);
	  		    Thread.sleep(1000);
	  			String str = driver.getCurrentUrl();
					driver.get(str);
	  			Thread.sleep(2000);
	  				int counter=1;
	  				do
	  				{
		  				try
		  				{
		  					BNBasicCommonMethods.waitforElement(wait, obj, "ATBBtn");
				  			WebElement ATBBtn = BNBasicCommonMethods.findElement(driver, obj, "ATBBtn");
				  			Thread.sleep(1000);
		  					ATBBtn.click();
		  					Thread.sleep(2500);
		  					BNBasicCommonMethods.waitforElement(wait, obj, "ATBSuccessAlertOverlay");
			  				WebElement ATBSuccessAlertOverlay = BNBasicCommonMethods.findElement(driver, obj, "ATBSuccessAlertOverlay");
			  				counter=5;
		  				}
		  				
		  				catch(Exception e)
		  				{
		  					WebElement ATBErrorAlt = driver.findElement(By.xpath("//*[@id='skMob_ErrorsDiv_id']"));
		  					if(ATBErrorAlt.isDisplayed())
		  					{
		  						WebElement ErroBtn = driver.findElement(By.xpath("//*[@id='skMob_ErrorsOK_id']"));
		  						ErroBtn.click();
		  					}
		  					String str1 = driver.getCurrentUrl();
		  					driver.get(str1);
		  			    }
	  				}while(counter < 5);
	  				BNBasicCommonMethods.waitforElement(wait, obj, "ContineShoppingBtn");
  					WebElement ContineShoppingBtn = BNBasicCommonMethods.findElement(driver, obj, "ContineShoppingBtn");
  					ContineShoppingBtn.click();
	  				BNBasicCommonMethods.waitforElement(wait, obj, "ShoppingBagIcon");
	  				WebElement ShoppingBagIcon = BNBasicCommonMethods.findElement(driver, obj, "ShoppingBagIcon");
	  				ShoppingBagIcon.click();
	  			   try
	  			   {
		  				BNBasicCommonMethods.waitforElement(wait, obj, "productQty");
		  				List <WebElement> productQty = driver.findElements(By.xpath("//*[contains(@class,'product-quantity') and @type='tel']"));
		  				//List <WebElement> ProdcutUpdate = driver.findElements(By.xpath("//*[@class='quantity']//*[contains(@type,'submit') and @class='text-link update-link']"));
		  				if(productQty.get(0).isDisplayed())
		  				{
		  					productQty.get(0).click();
		  					productQty.get(0).sendKeys("2");
		  					fail("Nook Product Able to Update the Quantity");
		  				}
	  			   }
	  			   
	  			   catch(Exception e)
	  			   {
	  				   pass("Nook Produdt not able to update the qunatity");
	  				 // exception("There is Something Wrong  : "+e.getMessage());   
	  			   }
	  				
                //  Thread.sleep(10000);
	  			 List <WebElement> ProdcutRemove = driver.findElements(By.xpath("//*[@class='remove-item']"));
	  			   
	  			 if(ProdcutRemove.get(0).isDisplayed())
	  			 {
	  				ProdcutRemove.get(0).click();
	  				 Thread.sleep(4000);
	  			    pass("Prodcut Remove Button is Clicked and Product is Removed");
	  			    BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
		  			 WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
		  			 BNLogo.click();
	  			 }
	  			 else
	  			 {
	  				 fail("Product is not Removed from the Bag");
	  			 }
	  			 
	  			 
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is Something went Wrong Please Verify  :"+e.getMessage());
		}
	}
	
	/*BNIA-805 Verify that on selecting the "Checkout" button in the shopping bag page,the Login overlay should be displayed when the user is not signed In*/
	public void Checkoutoverlay()
	{
		ChildCreation("BNIA-805 Verify that on selecting the Checkout button in the shopping bag page,the Login overlay should be displayed when the user is not signed In");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
			WebElement Eleshow = driver.findElement(By.xpath("//*[@class='product-img']"));
			wait.until(ExpectedConditions.visibilityOf(Eleshow));
			WebElement LoginCheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "LoginCheckoutBtn");
			wait.until(ExpectedConditions.visibilityOf(LoginCheckoutBtn));
			Thread.sleep(1000);
			LoginCheckoutBtn.click();
			log.add("The user is navigated to the Sign In Page.");
			BNBasicCommonMethods.waitforElement(wait, obj, "signInIframe");
			//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInIframe));
			WebElement signInIframe = BNBasicCommonMethods.findElement(driver, obj, "signInIframe");
			driver.switchTo().frame(signInIframe);
			WebElement CheckoutCloseIcon = BNBasicCommonMethods.findElement(driver, obj, "CheckoutCloseIcon");
			if(CheckoutCloseIcon.isDisplayed())
			{
				CheckoutCloseIcon = BNBasicCommonMethods.findElement(driver, obj, "CheckoutCloseIcon");
				CheckoutCloseIcon.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
				WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
	  			BNLogo.click();
				pass("Sign In Ovrelay is displayed successfully");
			}
			else
			{
				CheckoutCloseIcon = BNBasicCommonMethods.findElement(driver, obj, "CheckoutCloseIcon");
				CheckoutCloseIcon.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
				WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
	  			BNLogo.click();
				fail("Sign In Overlay is not displayed successfully");
			}
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
	}
	
	/*BNIA-806 Verify that on selecting the "Checkout" button in the shopping bag page,the Checkout page should be shown for registered user*/
	public void signInExistingCustomer() throws IOException, Exception
	{
		
		ChildCreation("BNIA-806 Verify that on selecting the Checkout button in the shopping bag page,the Checkout page should be shown for registered user");
		BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
		WebElement Eleshow = driver.findElement(By.xpath("//*[@class='product-img']"));
		wait.until(ExpectedConditions.visibilityOf(Eleshow));
		WebElement LoginCheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "LoginCheckoutBtn");
		wait.until(ExpectedConditions.visibilityOf(LoginCheckoutBtn));
		Thread.sleep(1000);
		LoginCheckoutBtn.click();
		pass("Sign In Ovrelay is displayed successfully");
		BNBasicCommonMethods.waitforElement(wait, obj, "signInIframe");
		//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInIframe));
		WebElement signInIframe = BNBasicCommonMethods.findElement(driver, obj, "signInIframe");
		driver.switchTo().frame(signInIframe);
		WebElement SignInEmail = BNBasicCommonMethods.findElement(driver, obj, "SignInEmail");
		WebElement SignInPass = BNBasicCommonMethods.findElement(driver, obj, "SignInPass");
		WebElement secureSignIn = BNBasicCommonMethods.findElement(driver, obj, "secureSignIn");
		sheet = BNBasicCommonMethods.excelsetUp("shoppingbag");
		int bniakey = sheet.getLastRowNum();
		for(int i = 0; i <= bniakey; i++)
		{	
			String tcid = "BNIA Sign In";
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				String Username = sheet.getRow(i).getCell(2).getStringCellValue().toString();
				String passw = sheet.getRow(i).getCell(3).getStringCellValue().toString();
				try 
				{
					if(BNBasicCommonMethods.isElementPresent(SignInEmail))
					{
						SignInEmail.sendKeys(Username);
						log.add("The User Name is successfully entered.");
						if(BNBasicCommonMethods.isElementPresent(SignInPass))
						{
							SignInPass.sendKeys(passw);
							log.add("The password is successfully entered.");
								 if(BNBasicCommonMethods.isElementPresent(secureSignIn))
								 {
									Thread.sleep(500);
									secureSignIn.click();
									log.add("Successful enter in Checkout page.");
									BNBasicCommonMethods.waitforElement(wait, obj, "Signoutbtn");
									Thread.sleep(1000);
									
										/*WebElement CheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutBtn");
										if(BNBasicCommonMethods.isElementPresent(CheckoutBtn))
										{
	
											System.out.println("In Checkout Flow");
											Thread.sleep(1000);
										   //CheckoutBtn.click();
											log.add("Successful Enter in Add a Shipping and Billing Address page.");
											try 
											{
										    	pass("Page navigated to checkout page ",log);
												//driver.navigate().refresh();
												CheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutBtn");
												Thread.sleep(1000);
												((JavascriptExecutor) driver).executeScript("arguments[0].click();", CheckoutBtn);
										   //}
											
										}*/
								 	}
								 else
								 {
									 fail("NO Successfull login to Checkout page");
								 }
						}
						else
						{
							fail("password field not found.");
							
						}
					}
					else
					{
						fail("Email Id field not found.");
						
					}
				}
				catch(Exception e)
				{
					System.out.println(e.getMessage());
					exception("Sign In to the User Account in Checkout Exception"+e.getMessage());
				//	fail(e.getMessage());
				}
			}
		}
		
		
	}
	
	/*BNIA-970 Verify that Saved Email must be shown near the Electronic delivery in shipping information for the digital content products*/
	/*BNIA-1072 Verify that Saved Email must be shown near the Electronic delivery in shipping information for the digital content products*/
	public void Electronicmail()
	{
		boolean childflag = false;
		ChildCreation("BNIA-970 Verify that Saved Email must be shown near the Electronic delivery in shipping information for the digital content products");
		try
		{
			WebElement CheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutBtn");
			if(BNBasicCommonMethods.isElementPresent(CheckoutBtn))
			{

				System.out.println("In Checkout Flow");
				Thread.sleep(1000);
			   CheckoutBtn.click();
			}
			WebElement waitforele = driver.findElement(By.xpath("//*[@class='egift-emails-display']"));
			BNBasicCommonMethods.scrollup(waitforele, driver);
			wait.until(ExpectedConditions.visibilityOf(waitforele));
			WebElement ElectronicMailId  =  BNBasicCommonMethods.findElement(driver, obj, "ElectronicMailId");
			if(ElectronicMailId.isDisplayed())
			{
				childflag = true;
				System.out.println(ElectronicMailId.getText());
				pass("Mail Id is Present for Electronic Delivery Option "+ElectronicMailId.getText());
			}
			else
			{
				fail("Mail Id is not present in Electronic delivery Option "+ElectronicMailId.getText() );
			}
		}
		
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something went worng"+e.getMessage());
		}
		
		if(childflag)
			pass("Mail Id is Present for Electronic Delivery Option ");
		else
			fail("Mail Id is not present in Electronic delivery Option ");
	}

	/*BNIA-1309 Checkout page: Verify that user can able to update the QTY in listed products in checkout page.*/
	/*BNIA-1313 Checkout page: Make sure that scroll is working as expected to view all the products if user add more number of products*/
	public void CheckoutUpdate()
	{
		boolean childflag = false;
		ChildCreation("BNIA-1309 Checkout page: Verify that user can able to update the QTY in listed products in checkout page.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("shoppingbag");
			String Updateval = BNBasicCommonMethods.getExcelNumericVal("BNIA788", sheet, 7);
			List <WebElement> productQty = driver.findElements(By.xpath("//*[contains(@class,'product-quantity')]"));
			List <WebElement> ProdcutUpdate = driver.findElements(By.xpath("//*[@class='quantity']//*[contains(@type,'submit') and @class='text-link update-link']"));
			int sel,usel;
			Random r = new Random();
			sel = r.nextInt(ProdcutUpdate.size());
			//sel = 2;
			usel=sel;
			String UpdateType = ProdcutUpdate.get(sel).getAttribute("type");
			BNBasicCommonMethods.scrolldown(ProdcutUpdate.get(sel), driver);
			if(productQty.get(sel).isDisplayed())
			{
				productQty.get(sel).click();
				productQty.get(sel).clear();
				productQty.get(sel).sendKeys(Updateval);
				
				UpdateType = ProdcutUpdate.get(sel).getAttribute("type");
				if(UpdateType.equals("submit"))
				{
					ProdcutUpdate.get(sel).click();
					childflag  =true;
					BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
					Thread.sleep(4500);
					BNBasicCommonMethods.waitforElement(wait, obj, "productQty");
					productQty = driver.findElements(By.xpath("//*[contains(@class,'product-quantity')]"));
					if(productQty.get(sel).getAttribute("value").toString().equals(Updateval))
						pass("product Quantity is Updated");
					else
						fail("Product Qunatity is not updated");
				}
				else
				{
					fail("No update button is available");
				}
			}
		}
		
		catch(Exception e )
		{
			System.out.println(e.getMessage());
			exception("Exception something went wrong"+e.getMessage());
		}
		
		ChildCreation("BNIA-1313 Checkout page: Make sure that scroll is working as expected to view all the products if user add more number of products");
		if(childflag)
			pass("User Abe to Scroll Any Product and able to Update");
		else
			fail("User not able to scroll the product");
			
		
	}
	
	/*BNIA-1314 Checkout page: Products displayed in checkout page should have the following details "Product name, Author, Availability status, Stock availability, Make it a gift link, price and QTY"*/
	public void CheckoutProductDetails()
	{
		ChildCreation("BNIA-1314 Checkout page: Products displayed in checkout page should have the following details Product name, Author, Availability status, Stock availability, Make it a gift link, price and QTY");
		try
		{
			WebElement deliveryOptionCartItemContainer = BNBasicCommonMethods.findElement(driver, obj, "deliveryOptionCartItemContainer");
			if(BNBasicCommonMethods.isElementPresent(deliveryOptionCartItemContainer))
			{
				pass("The Cart Item Container is displayed.");
				BNBasicCommonMethods.scrolldown(deliveryOptionCartItemContainer, driver);
				List <WebElement> deliveryOptionCartItemsImage = driver.findElements(By.xpath("//*[@id='checkoutContainer']//*[@id='cartItems']//*[@class='product-img']//img"));
				for(int i = 0;i<deliveryOptionCartItemsImage.size();i++)
				{
					BNBasicCommonMethods.scrolldown(deliveryOptionCartItemsImage.get(i), driver);
					WebElement img = deliveryOptionCartItemsImage.get(i);
					int imgResp = BNBasicCommonMethods.imageBroken(img, log);
					if(imgResp==200)
					{
						pass("The Image is not broken.",log);
					}
					else
					{
						fail("The Image is broken.",log);
					}
					
					List <WebElement> deliveryOptionCartItemsProductTitle = driver.findElements(By.xpath("//*[@id='checkoutContainer']//*[@id='cartItems']//*[@class='product-desc']//*[@class='product-title']"));
					WebElement title = deliveryOptionCartItemsProductTitle.get(i);
					String prdtTitle = title.getText();
					if(prdtTitle.isEmpty())
					{
						fail("The Product title is displayed. But the displayed title is empty.");
					}
					else
					{
						pass("The Product title is displayed. The displayed title is : " + prdtTitle);
					}
					
					List <WebElement> deliveryOptionCartItemsProductAuthor = driver.findElements(By.xpath("//*[@id='checkoutContainer']//*[@id='cartItems']//*[@class='product-desc']//*[@class='contributors']"));
					WebElement author = deliveryOptionCartItemsProductAuthor.get(i);
					String prdtAuthor = author.getText();
					if(prdtAuthor.isEmpty())
					{
						fail("The Product author is displayed. But the displayed author is empty.");
					}
					else
					{
						pass("The Product Author is displayed. The displayed author name is : " + prdtAuthor);
					}
					List <WebElement> deliveryOptionCartItemsProductPrice = driver.findElements(By.xpath("//*[@id='checkoutContainer']//*[@id='cartItems']//*[@class='price']//*[@class='item-price']"));
					WebElement price = deliveryOptionCartItemsProductPrice.get(i);
					String prdtPrice = price.getText();
					if(prdtPrice.isEmpty())
					{
						fail("The Product price is displayed. But the displayed price is empty.");
					}
					else
					{
						pass("The Product price is displayed. The displayed price is : " + prdtPrice);
					}
					List <WebElement> deliveryOptionCartItemsProductQty = driver.findElements(By.xpath("//*[@id='checkoutContainer']//*[@id='cartItems']//*[@class='quantity']//*[@class='edit-cart-items']//*[@class='mini-cart-quantity-update']"));
					WebElement qty = deliveryOptionCartItemsProductQty.get(i);
					String prdtQuantity = qty.getText();
					if(prdtQuantity.isEmpty())
					{
						fail("The Product quantity is displayed. But the displayed quantity is empty.");
					}
					else
					{
						pass("The Product quantity is displayed. The displayed quantity is : " + prdtQuantity);
					}
					List<WebElement> deliveryOptionCartItemsGiftSection = driver.findElements(By.xpath("//*[@id='checkoutContainer']//*[@class='product-desc']//*[@class='make-it-a-gift-select']"));
					WebElement giftElement = deliveryOptionCartItemsGiftSection.get(i);
					if(BNBasicCommonMethods.isElementPresent(giftElement))
					{
						pass("The gift section is displayed.");
					}
					else
					{
						fail("The gift section is not displayed.");
					}
				}
			}
			else
			{
				fail("The Cart Item Container is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong.Please Check." + e.getMessage());
		}
	}

	/*BNIA-1316 Checkout page:Verify that edit option is shown near the products in checkout page to update qty,price etc..*/
	public void CheckoutEditbtn()
	{
		ChildCreation("BNIA-1316 Checkout page:Verify that edit option is shown near the products in checkout page to update qty,price etc..");
		try
		{
			List <WebElement> QtyEdit = driver.findElements(By.xpath("//*[@id='checkoutContainer']//*[@id='cartItems']//*[@class='quantity']//*[@class='edit-cart-items']//a"));
			if(QtyEdit.get(0).isDisplayed())
			{
				pass("Edit Test is Displayed below the Quantity for Updating the product in chcekout Page"+QtyEdit.get(0).getText());
				BNBasicCommonMethods.waitforElement(wait, obj, "BackIcon");
				WebElement BackIcon = BNBasicCommonMethods.findElement(driver, obj, "BackIcon");
				BackIcon.click();
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong.Please Check." + e.getMessage());
		}
		
	}
	
	/*BNIA-1317 Checkout page: Make sure that price displayed for each product in shopping page is matched with the checkout page price*/
	public void Checkoutpricecheck()
	{
		ChildCreation("BNIA-1317Checkout page: Make sure that price displayed for each product in shopping page is matched with the checkout page price");
		try
		{
			
			WebElement CheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutBtn");
			if(BNBasicCommonMethods.isElementPresent(CheckoutBtn))
			{

				System.out.println("In Checkout Flow");
				Thread.sleep(1000);
			   CheckoutBtn.click();
			}
			Thread.sleep(2000);
			BNBasicCommonMethods.waitforElement(wait, obj, "deliveryOptionCartItemsProductPrice");
			for(int ctr = 0; ctr<ProductPrice.size() ;ctr++)
			{
				List <WebElement> deliveryOptionCartItemsProductPrice = driver.findElements(By.xpath("//*[@id='checkoutContainer']//*[@id='cartItems']//*[@class='total']//*[@class='total-price']"));
				WebElement price = deliveryOptionCartItemsProductPrice.get(ctr);
				BNBasicCommonMethods.scrolldown(price, driver);
				String prdtPrice = price.getText();
				if(ProductPrice.contains(prdtPrice))
				{
					pass("Prodcut Price is Matched"+prdtPrice);
				}
				else
				{
					System.out.println(prdtPrice);
					fail("Product Price is not Mathced"+prdtPrice);
				}
			}
		}
		
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong.Please Check." + e.getMessage());
		}
	}
	
	/*BNIA-1318 Checkout page: Make sure that qty displayed for each product in shopping page is matched with the checkout page*/
	public void CheckoutQtycheck()
	{
		ChildCreation("BNIA-1318 Checkout page: Make sure that qty displayed for each product in shopping page is matched with the checkout page");
		try
		{
			for(int ctr = 0; ctr<ProductPrice.size();ctr++)
			{
				List <WebElement> deliveryOptionCartItemsProductQty = driver.findElements(By.xpath("//*[@id='checkoutContainer']//*[@id='cartItems']//*[@class='quantity']//*[@class='edit-cart-items']//*[@class='mini-cart-quantity-update']"));
				WebElement qty = deliveryOptionCartItemsProductQty.get(ctr);
				BNBasicCommonMethods.scrolldown(qty, driver);
				String prdtQuantity = qty.getText();
				if(ProductQty.contains(prdtQuantity))
				{
					pass("Prodcut Quantity is Matched"+prdtQuantity);
				}
				/*else
				{
					fail("Product Quantity is not Mathced"+prdtQuantity);
				}*/
			}
		}
		
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong.Please Check." + e.getMessage());
		}
	}
	
	/*BNIA-1321 Checkout page: Verify that estimated shipping details should be displayed correctly and should matched with the shoppingbag page*/
	public void CheckoutEstimataePricecheck()
	{
		ChildCreation("BNIA-1321 Checkout page: Verify that estimated shipping details should be displayed correctly and should matched with the shoppingbag page");
		try
		{
			WebElement deliveryOptionCartTotalPrice = BNBasicCommonMethods.findElement(driver, obj, "deliveryOptionCartTotalPrice");
			if(EstimatedPrice.equals(deliveryOptionCartTotalPrice.getText()))
			{
				pass("Estimated Shipping details matched with Shopping bag page");
			}
			else
			{
				fail("Estimated Shipping details is not matched with Shopping bag Page");
			}
			
		}
		
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong.Please Check." + e.getMessage());
		}
	}
	
	public void NooktoShopping() throws Exception
	{
		Thread.sleep(3000);
		BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
		WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
		BNLogo.click();
		sheet = BNBasicCommonMethods.excelsetUp("shoppingbag");
  		String EAN = BNBasicCommonMethods.getExcelVal("BNIA793", sheet, 3);
		BNBasicCommonMethods.waitforElement(wait, obj, "SearchTile");
		WebElement SearchTile = BNBasicCommonMethods.findElement(driver, obj, "SearchTile");
		SearchTile.click();
		BNBasicCommonMethods.waitforElement(wait, obj, "SearchIcon");
		//String EAN  = "9780316408769";
		WebElement SearchIcon = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
		SearchIcon.click();
		WebElement SearchTxtBx = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBx");
		    WebElement SearchTxtBxnew = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");	
		    Actions action = new Actions(driver);	
		    	SearchTxtBx.click();
  			action.moveToElement(SearchTxtBx).click();
  			SearchTxtBxnew.clear();
  			SearchTxtBxnew.sendKeys(EAN);
  			//action.moveToElement(SearchTxtBx).sendKeys(EAN[ctr]).build().perform();
  			System.out.println(EAN);
  			Thread.sleep(1500);	
  			SearchTxtBxnew.sendKeys(Keys.ENTER);
  		    Thread.sleep(1000);
  			String str = driver.getCurrentUrl();
				driver.get(str);
  			Thread.sleep(2000);
  				int counter=1;
  				do
  				{
	  				try
	  				{
	  					BNBasicCommonMethods.waitforElement(wait, obj, "ATBBtn");
			  			WebElement ATBBtn = BNBasicCommonMethods.findElement(driver, obj, "ATBBtn");
			  			Thread.sleep(1000);
	  					ATBBtn.click();
	  					Thread.sleep(2000);
	  					BNBasicCommonMethods.waitforElement(wait, obj, "ATBSuccessAlertOverlay");
		  				WebElement ATBSuccessAlertOverlay = BNBasicCommonMethods.findElement(driver, obj, "ATBSuccessAlertOverlay");
		  				counter=5;
	  				}
	  				
	  				catch(Exception e)
	  				{
	  					WebElement ATBErrorAlt = driver.findElement(By.xpath("//*[@id='skMob_ErrorsDiv_id']"));
	  					if(ATBErrorAlt.isDisplayed())
	  					{
	  						WebElement ErroBtn = driver.findElement(By.xpath("//*[@id='skMob_ErrorsOK_id']"));
	  						ErroBtn.click();
	  					}
	  					String str1 = driver.getCurrentUrl();
	  					driver.get(str1);
	  			    }
  				}while(counter < 5);
  				BNBasicCommonMethods.waitforElement(wait, obj, "ContineShoppingBtn");
					WebElement ContineShoppingBtn = BNBasicCommonMethods.findElement(driver, obj, "ContineShoppingBtn");
					ContineShoppingBtn.click();
  				BNBasicCommonMethods.waitforElement(wait, obj, "ShoppingBagIcon");
  				WebElement ShoppingBagIcon = BNBasicCommonMethods.findElement(driver, obj, "ShoppingBagIcon");
  				ShoppingBagIcon.click();
  				Thread.sleep(750);
  				BNBasicCommonMethods.waitforElement(wait, obj, "Signoutbtn");
  				
	}

/************************************************************ Forgot password in checkout ************************************************************/
	
//	@Test(priority = 2)
    public void BNIA_291_Forgot_Password() throws Exception 
	  {
		  	sheet = BNBasicCommonMethods.excelsetUp("Forgot Password");
		  	TempSignIn();
			signInForForgotPass();
			forgotPasswordnavigation();
			forgorpasswordEmailandContinuefield();
			forgorpasswordelementpresence();
			forgotpasswordEmptyEmailAlrt();
			forgotpasswordEmailLength();
			forgotpasswordNonRegisterEmailAlrt();
			forgotpasswordHtmlEmail();
			forgotpasswordContinuebtn();
			forgotpasswordpageelements();
			forgotpasswordEmailVerify();
			forgotpasswordRecoveryOptionVerify();
			forgotpasswordRecoveryDefaultSelect();
			forgotpasswordRecoverySecurityQuestionCheck();
			forgotpasswordSecurityAnswerLimit();
			forgotpasswordInvalidSecurityAnswer();
			forgotpasswordInvalidSecurityLimitExceed();
			forgotpasswordResetByEmailLink();
			ForgotpasswordContinueShoppingNavigation();
			//forgotpasswordContinueShoppingNav();
			forgotpasswordSecurityQuestionVerification();
			forgotpasswordBlankPassAlrt();
			forgotpasswordResetRulesEnable();
			forgotpasswordResetRulesDisable();
			forgotpasswordResetSuccess();
			//forgotpasswordRecoveryCustomerService();
			
	  }	
	
    /* Sign In to the New User Account in Checkout */
	public void signInForForgotPass() throws IOException, Exception
	{
		//ChildCreation("Add a new Shipping Address overlay should be Displayed");
		BNBasicCommonMethods.WaitForLoading(wait);
		BNBasicCommonMethods.waitforElement(wait, obj, "checkoutTile");
		WebElement CheckoutTile = BNBasicCommonMethods.findElement(driver, obj, "checkoutTile");
		wait.until(ExpectedConditions.elementToBeClickable(CheckoutTile));
		CheckoutTile.click();
		Thread.sleep(1500);
		//driver.navigate().refresh();
		//BNBasicCommonMethods.WaitForLoading(wait);
		BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
		WebElement CheckoutSignIn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutSignIn");
		wait.until(ExpectedConditions.visibilityOf(CheckoutSignIn));
		Thread.sleep(1000);
		CheckoutSignIn.click();
		
		//log.add("The user is navigated to the Sign In Page.");
		BNBasicCommonMethods.waitforElement(wait, obj, "signInIframe");
		//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInIframe));
		WebElement signInIframe = BNBasicCommonMethods.findElement(driver, obj, "signInIframe");
		driver.switchTo().frame(signInIframe);
	}
     
	/* BRM - 350 Verify that while selecting the forgot your password option in the sign-in page, the "Forgot password" page should be displayed*/
	public void forgotPasswordnavigation() throws IOException
	{
		ChildCreation("Verify that while selecting the forgot your password option in the sign-in page, the Forgot password page should be displayed.");
		//signInExistingCustomer();
		try
		{
			String forgotpasscaption = BNBasicCommonMethods.getExcelVal("BRM350", sheet, 2);
			BNBasicCommonMethods.waitforElement(wait, obj, "forgotPassword");
			WebElement forgotPassword = BNBasicCommonMethods.findElement(driver, obj, "forgotPassword");
			wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
			forgotPassword.click();
			driver.switchTo().defaultContent();
			Thread.sleep(6000);
			
			/*//BNBasicCommonMethods.waitforElement(wait, obj, "frgtpageframe");
			WebElement frgtpageframe = BNBasicCommonMethods.findElement(driver, obj, "frgtpageframe");
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frgtpageframe));*/
			WebElement frgtpageframe = BNBasicCommonMethods.findElement(driver, obj, "frgtpageframe");
			driver.switchTo().frame(frgtpageframe);
			Thread.sleep(1000);
			BNBasicCommonMethods.waitforElement(wait, obj, "forgotPasswordTitle");
			WebElement forgotPasswordTitle = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordTitle");
			wait.until(ExpectedConditions.visibilityOf(forgotPasswordTitle));
			if(BNBasicCommonMethods.isElementPresent(forgotPasswordTitle))
			{
				log.add("The Forgot Password link is found and it is clicked.");
				if(forgotPasswordTitle.getText().equals(forgotpasscaption))
				{
					pass("The user is navigated to the Forgot Password Assistant page.",log);
				}
				else
				{
					System.out.println(forgotPasswordTitle.getText());
					fail("The user is not navigated to the Forgot Password Assistant page.",log);
				}
			}
			else
			{
				fail("The Forgot Password link is not visible in the Sign In page. Please Check.");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is Something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}

	/* BNIA-828 Verify that "email address" text field should be displayed in the Password Assistant page with the "Continue' button*/
	public void forgorpasswordEmailandContinuefield()
	{
		ChildCreation("BNIA-828 Verify that email address text field should be displayed in the Password Assistant page with the Continue button.");
		try
		{
			WebElement forgotPasswordEmail = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordEmail");
			if(BNBasicCommonMethods.isElementPresent(forgotPasswordEmail))
			{
				pass("The Email Address field is present in the Forgot Password Assistant page.");
			}
			else
			{
				fail("The Email Address field is not present in the Forgot Password Assistant page.");
			}
			
			WebElement forgotContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinueBtn");
			if(BNBasicCommonMethods.isElementPresent(forgotContinueBtn))
			{
				pass("The Submit Button is present in the Forgot Password Assistant page.");
			}
			else
			{
				pass("The Submit Button is not present in the Forgot Password Assistant page.");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}

	/* BNIA-829 Verify that forgot password page should match with the creative.*/
	public void forgorpasswordelementpresence()
	{
		ChildCreation("BNIA-829 Verify that forgot password page should match with the creative.");
		try
		{
			WebElement forgotPasswordTitle = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordTitle");
			if(BNBasicCommonMethods.isElementPresent(forgotPasswordTitle))
			{
				pass("The Forgot Password Assistant page title is displayed.");
			}
			else
			{
				fail("The Forgot Password Assistant page title is not displayed.");
			}
					
			WebElement forgotPasswordInfo = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordInfo");
			if(BNBasicCommonMethods.isElementPresent(forgotPasswordInfo))
			{
				pass("The Forgot Password Assistant page Info Message is displayed.");
			}
			else
			{
				fail("The Forgot Password Assistant page Info Message is not displayed.");
			}
					
			WebElement forgotPasswordEmail = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordEmail");
			if(BNBasicCommonMethods.isElementPresent(forgotPasswordEmail))
			{
				pass("The Forgot Password Assistant page Email Field is displayed.");
			}
			else
			{
				fail("The Forgot Password Assistant page Email field is not displayed.");
			}
			
			WebElement forgotContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinueBtn");
			if(BNBasicCommonMethods.isElementPresent(forgotContinueBtn))
			{
				pass("The Forgot Password Assistant Continue Button is displayed.");
			}
			else
			{
				fail("The Forgot Password Assistant page Continue button is not displayed.");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}

	/* BNIA-831 Verify that while selecting the "Continue" button after entering the valid Email Id,the password assistant page should be displayed with the "Continue" and "Cancel" button*/
	public void forgotpasswordContinuebtn()
	{
		ChildCreation("BNIA-831 Verify that while selecting the Continue button after entering the valid Email Id,the password assistant page should be displayed with the Continue and Cancel button.");
		try
		{
			WebElement forgotPasswordEmail = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordEmail");
			forgotPasswordEmail.clear();
			String forgotPasstitle = BNBasicCommonMethods.getExcelVal("BRM355", sheet, 2); 
			String validemailId = BNBasicCommonMethods.getExcelVal("BRM355", sheet, 6); 
			if(BNBasicCommonMethods.isElementPresent(forgotPasswordEmail))
			{
				log.add("The Email Id field is present in the Password Assistant Page.");
				forgotPasswordEmail.sendKeys(validemailId);
				log.add("The Email Id is entered in the Password Assistant Page Email Address Field.");
				WebElement forgotContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinueBtn");
				if(BNBasicCommonMethods.isElementPresent(forgotContinueBtn))
				{
					forgotContinueBtn.click();
					log.add("The Continue button in the Password Assistant is clicked.");
					//BNBasicCommonMethods.waitforElement(wait, obj, "frgtpageframe");
					//WebElement frgtpageframe = BNBasicCommonMethods.findElement(driver, obj, "frgtpageframe");
					//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frgtpageframe));
					BNBasicCommonMethods.waitforElement(wait, obj, "forgotPasswordTitle");
					WebElement forgotPasswordTitle = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordTitle");
					wait.until(ExpectedConditions.visibilityOf(forgotPasswordTitle));
					if(BNBasicCommonMethods.isElementPresent(forgotPasswordTitle))
					{
						log.add("The User is navigated to the Password Assistant Page.");
						if(forgotPasswordTitle.getText().equals(forgotPasstitle))
						{
							pass("The User is navigated to the Password Assistant Page to reset the password after entering the Valid Email Address and the header matches.");
						}
						else
						{
							fail("The User is navigated to the Password Assistant Page to reset the password after entering the Valid Email Address but the title of the page does not match.");
						}
					}
					else
					{
						fail("The User is not navigated to the Password Assistant Page to reset the password after entering the Valid Email Address.");
					}
				}
				else
				{
					fail("The Continue button is not present in the Password Assistant Page.");
				}
			}
			else
			{
				fail("The Email Id field is not present in the Password Assistant Page.");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}
	
	/* BNIA-832 Verify that while selecting the "Continue" button without entering the email address field,the alert message should be displayed*/
	public void forgotpasswordEmptyEmailAlrt()
	{
		ChildCreation("BNIA-832 Verify that while selecting the Continue button without entering the email address field,the alert message should be displayed.");
		try
		{
			String forgotPassalrt = BNBasicCommonMethods.getExcelVal("BRM356", sheet, 7);
			WebElement forgotContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinueBtn");
			if(BNBasicCommonMethods.isElementPresent(forgotContinueBtn))
			{
				forgotContinueBtn.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "forgotPasswordError");
				WebElement forgotPasswordError = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordError");
				wait.until(ExpectedConditions.visibilityOf(forgotPasswordError));
				Thread.sleep(1000);
				if(BNBasicCommonMethods.isElementPresent(forgotPasswordError))
				{
					pass("The alert is raised when user tries to proceed to Password Assistant page without entering the Email Id in the Email Address field.");
					if(forgotPasswordError.getText().equals(forgotPassalrt))
					{
						pass("The alert that was raised matches with the expected alert. The alert raised was " + forgotPasswordError.getText().toString());
					}
					else
					{
						fail("The alert that was raised does not matches with the expected alert. The raised alert was " + forgotPasswordError.getText().toString());
					}
				}
				else
				{
					fail("The Alert is not raised in the Password Assistant Page. Please Check.");
				}
			}
			else
			{
				fail("The Continue button is not present in the Forgot Assistant Page.");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}

	/* BNIA-833 Verify that Password assistant pages should be as per the creative */
	public void forgotpasswordpageelements()
	{
		ChildCreation("BNIA-833 Verify that Password assistant pages should be as per the creative .");
		try
		{
			
			Thread.sleep(6000);
			WebElement forgotPasswordTitle = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordTitle");
			if(BNBasicCommonMethods.isElementPresent(forgotPasswordTitle))
			{
				pass("The Forgot Password Assistant page title is displayed.");
			}
			else
			{
				fail("The Forgot Password Assistant page title is not displayed.");
			}
			
			driver.switchTo().defaultContent();
			Thread.sleep(2000);
			WebElement ForgotEmailtwo = BNBasicCommonMethods.findElement(driver, obj, "ForgotEmailtwo");
			driver.switchTo().frame(ForgotEmailtwo);
			WebElement forgotEmailDisplay = BNBasicCommonMethods.findElement(driver, obj, "forgotEmailDisplay");
			if(BNBasicCommonMethods.isElementPresent(forgotEmailDisplay))
			{
				pass("The entered Email field is displayed in the Forgot Password Assistant page.");
			}
			else
			{
				fail("The entered Email field is not displayed in the Forgot Password Assistant page.");
			}
			
			WebElement forgotEmailEdit = BNBasicCommonMethods.findElement(driver, obj, "forgotEmailEdit");
			if(BNBasicCommonMethods.isElementPresent(forgotEmailEdit))
			{
				pass("The Email Edit link is displayed in the Forgot Password Assistant page.");
			}
			else
			{
				fail("The Email Edit link is not displayed in the Forgot Password Assistant page.");
			}
			
			WebElement forgotEmailRecoveryOption = BNBasicCommonMethods.findElement(driver, obj, "forgotEmailRecoveryOption");
			if(BNBasicCommonMethods.isElementPresent(forgotEmailRecoveryOption))
			{
				pass("The Password recovery option by Email Option radio button is displayed in the Forgot Password Assistant page.");
			}
			else
			{
				fail("The Password recovery option by Email Option radio button is not displayed in the Forgot Password Assistant page.");
			}
			
			WebElement forgotSecurityRecoveryOption = BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityRecoveryOption");
			if(BNBasicCommonMethods.isElementPresent(forgotSecurityRecoveryOption))
			{
				pass("The Password recovery option by Security Question Option radio button is displayed in the Forgot Password Assistant page.");
			}
			else
			{
				fail("The Password recovery option by Security Question Option is not displayed in the Forgot Password Assistant page.");
			}
			
			WebElement forgotContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinueBtn");
			if(BNBasicCommonMethods.isElementPresent(forgotContinueBtn))
			{
				pass("The Continue button is displayed in the Forgot Password Assistant page.");
			}
			else
			{
				fail("The Continue button is not displayed in the Forgot Password Assistant page.");
			}
			
			WebElement forgotCancel = BNBasicCommonMethods.findElement(driver, obj, "forgotCancel");
			if(BNBasicCommonMethods.isElementPresent(forgotCancel))
			{
				pass("The Cancel button is displayed in the Forgot Password Assistant page.");
			}
			else
			{
				fail("The Cancel button is not displayed in the Forgot Password Assistant page.");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is Something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}

	/* BNIA-834 Verify that user entered email address should be displayed correctly in the password assistant page*/
	public void forgotpasswordEmailVerify()
	{
		ChildCreation(" BNIA-834 Verify that user entered email address should be displayed correctly in the password assistant page.");
		try
		{
			String forgotemail = BNBasicCommonMethods.getExcelVal("BRM380", sheet, 6);
			String forgotemailcaption = BNBasicCommonMethods.getExcelVal("BRM380", sheet, 3); 
			WebElement forgotEmailDisplay = BNBasicCommonMethods.findElement(driver, obj, "forgotEmailDisplay");
			if(forgotEmailDisplay.getText().equals(forgotemailcaption.concat(forgotemail)))
			{
				/*System.out.println(forgotEmailDisplay.getText());
				System.out.println(forgotemailcaption.concat(forgotemail));
				System.out.println(forgotEmailDisplay.getText().equals(forgotemailcaption.concat(forgotemail)));*/
				pass("The user entered email id is displayed with the Caption.");
			}
			else
			{
				/*System.out.println(forgotEmailDisplay.getText());
				System.out.println(forgotemailcaption.concat(forgotemail));
				System.out.println(forgotEmailDisplay.getText().equals(forgotemailcaption.concat(forgotemail)));*/
				fail("The user entered email id is displayed with the Caption but there is mismatch.");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}

	/* BNIA-835 Verify that "Send a link to my email" and "Answer a security question" options should be displayed as per the classic site*/
	public void forgotpasswordRecoveryOptionVerify()
	{
		ChildCreation("BNIA-835 Verify that Send a link to my email and Answer a security question options should be displayed as per the classic site.");
		try
		{
			String forgotemailrecoverycaption = BNBasicCommonMethods.getExcelVal("BRM402", sheet, 8);
			String forgotsecurityrecoverycaption = BNBasicCommonMethods.getExcelVal("BRM402", sheet, 9); 
			WebElement forgotEmailRecoveryOption = BNBasicCommonMethods.findElement(driver, obj, "forgotEmailRecoveryOption");
			if(BNBasicCommonMethods.isElementPresent(forgotEmailRecoveryOption))
			{
				pass("The Email Recovery Option is displayed.");
				WebElement forgotEmailRecoveryOptionTxt = BNBasicCommonMethods.findElement(driver, obj, "forgotEmailRecoveryOptionTxt");
				if(forgotEmailRecoveryOptionTxt.getText().equals(forgotemailrecoverycaption))
				{
					pass("The Email Recovery Option Caption matches with the expected one.");
				}
				else
				{
					fail("The Email Recovery Option Caption does not matches with the expected one.");
				}
			}
			else
			{
				fail("The Email Recovery Option is not displayed.");
			}
			
			WebElement forgotSecurityRecoveryOption = BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityRecoveryOption");
			if(BNBasicCommonMethods.isElementPresent(forgotSecurityRecoveryOption))
			{
				pass("The Security Recovery Option is displayed.");
				WebElement forgotSecurityRecoveryOptionTxt = BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityRecoveryOptionTxt");
				if(forgotSecurityRecoveryOptionTxt.getText().equals(forgotsecurityrecoverycaption))
				{
					pass("The Security Recovery Option Caption matches with the expected one.");
				}
				else
				{
					fail("The Security Recovery Option does not matches with the expected one.");
				}
			}
			else
			{
				fail("The Security Recovery Option is not displayed.");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}

	/* BNIA-836 Verify that by default the "send a link to my email" option should be selected in the forgot password page */
	public void forgotpasswordRecoveryDefaultSelect()
	{
		ChildCreation(" BNIA-836 Verify that by default the send a link to my email option should be selected in the forgot password page ");
		try
		{
			WebElement forgotEmailRecoveryOption = BNBasicCommonMethods.findElement(driver, obj, "forgotEmailRecoveryOption");
			if(BNBasicCommonMethods.isElementPresent(forgotEmailRecoveryOption))
			{
				if(forgotEmailRecoveryOption.getAttribute("value").equals("true"))
				{
					pass("The Send a link to my email is found and it is selected by Default.");
				}
				else
				{
					fail("The Send a link to my email is found and it is not selected by Default.");
				}
			}
			else
			{
				fail("The Email Recovery Option is not displayed.");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}
	
	/* BNIA-837 Verify that while selecting the "customer service" link in "check your mail" page,it should redirects to the respective page*/
	/*BNIA-842 Verify that clicking on edit displays the entered email address value in the email address field.*/
	public void forgotpasswordRecoveryCustomerService() throws IOException
	{
		boolean childflag = false;
		ChildCreation("BNIA-837 Verify that while selecting the customer service link in check your mail page,it should redirects to the respective page.");
		try
		{
			//driver.get(BNConstants.mobileSiteURL);
			//signInExistingCustomer();
			//String customerServiceURL = BNBasicCommonMethods.getExcelVal("BRM404", sheet, 14);
			String emailID = BNBasicCommonMethods.getExcelVal("BRM404", sheet, 6); 
			BNBasicCommonMethods.waitforElement(wait, obj, "forgotPassword");
			WebElement forgotPassword = BNBasicCommonMethods.findElement(driver, obj, "forgotPassword");
			wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
			forgotPassword.click();
			driver.switchTo().defaultContent();
			BNBasicCommonMethods.waitforElement(wait, obj, "frgtpageframe");
			WebElement frgtpageframe = BNBasicCommonMethods.findElement(driver, obj, "frgtpageframe");
			driver.switchTo().frame(frgtpageframe);
			//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frgtpageframe));
			BNBasicCommonMethods.waitforElement(wait, obj, "forgotPasswordTitle");
			WebElement forgotPasswordTitle = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordTitle");
			wait.until(ExpectedConditions.visibilityOf(forgotPasswordTitle));
			Thread.sleep(1000);
			WebElement forgotPasswordEmail = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordEmail");
			forgotPasswordEmail.sendKeys(emailID);
			log.add("The Email Id is entered in the Password Assistant Page Email Address Field.");
			WebElement forgotContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinueBtn");
			if(BNBasicCommonMethods.isElementPresent(forgotContinueBtn))
			{
				forgotContinueBtn.click();
				Thread.sleep(4000);
				log.add("The Continue button in the Password Assistant is clicked.");
				//driver.switchTo().defaultContent();
				BNBasicCommonMethods.waitforElement(wait, obj, "ForgotPassEdit");
				WebElement ForgotPassEdit = BNBasicCommonMethods.findElement(driver, obj, "ForgotPassEdit");
				ForgotPassEdit.click();
				pass("Edit button is clickable and  navigated to previous page ");
				forgotContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinueBtn");
				forgotContinueBtn.click();
				/*WebElement ForgotEmailtwo = BNBasicCommonMethods.findElement(driver, obj, "ForgotEmailtwo");
				driver.switchTo().frame(ForgotEmailtwo);*/
				//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frgtpageframe));
				//wait.until(ExpectedConditions.visibilityOf(forgotContinueBtn));
				Thread.sleep(1000);
				WebElement forgotContinuenewBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinuenewBtn");
				if(BNBasicCommonMethods.isElementPresent(forgotContinuenewBtn))
				{
					forgotContinuenewBtn.click();
					Thread.sleep(5000);
					driver.switchTo().defaultContent();
					WebElement forgotResetSuccessContainer = BNBasicCommonMethods.findElement(driver, obj, "forgotResetSuccessContainer");
					if(BNBasicCommonMethods.isElementPresent(forgotResetSuccessContainer))
					{
						/*WebElement customerServiceLink = BNBasicCommonMethods.findElement(driver, obj, "customerServiceLink");
						if(BNBasicCommonMethods.isElementPresent(customerServiceLink))
						{
							customerServiceLink.click();
							Thread.sleep(5000);
							driver.switchTo().defaultContent();
							String currentURL = driver.getCurrentUrl();
							if(currentURL.equals(customerServiceURL))
							{*/
						       childflag = true;
								pass("The user is navigated to the Password Reset Success message page and the Success instruction is displayed. The user is successfully navigated to the Customer Service Page. The Current URL of the Customer Service Page is " /*+ currentURL.toString()*/);
							/*}
							else
							{
								fail("The user is navigated to the Password Reset Success message page and the Success instruction is displayed. However user is not successfully navigated to the Customer Service Page. The Current user navigated URL is " + currentURL.toString());
							}
						}
						else
						{
							fail("The Customer Service Link is not displayed." );
						}*/
					}
					else
					{
						fail("The Success Message Container is not displayed." );
					}
				}
				else
				{
					fail("The Continue Button is not displayed." );
				}
			}
			else
			{
				fail("The Continue Button is not displayed." );
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
		
		if(childflag)
			pass("Edit button is clickable and  navigated to previous page ");
		else
			fail("Edit button is not able to clickable and  not able navigated to previous page ");
			
	}

	/* BNIA-838 Verify while selecting the "Answer a Security Question" in the forgot passsword page,the Security question should be displayed with the "Security Answer" text field*/
	public void forgotpasswordRecoverySecurityQuestionCheck()
	{
		ChildCreation("BNIA-838 Verify while selecting the Answer a Security Question in the forgot passsword page,the Security question should be displayed with the Security Answer text field.");
		try
		{
			WebElement forgotSecurityRecoveryOption = BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityRecoveryOption");
			if(BNBasicCommonMethods.isElementPresent(forgotSecurityRecoveryOption))
			{
				pass("The Security Question Recovery Option is displayed.");
				
				boolean secchk = forgotSecurityRecoveryOption.isSelected();
				if(secchk==false)
				{
					driver.findElement(By.xpath("(//*[@class='styled-radio'])[2]")).click();
				}
				
				BNBasicCommonMethods.waitforElement(wait, obj, "forgotSecurityAnswerFields");
				WebElement forgotSecurityAnswerFields = BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityAnswerFields");
				wait.until(ExpectedConditions.visibilityOf(forgotSecurityAnswerFields));
				Thread.sleep(1000);
				if(BNBasicCommonMethods.isElementPresent(forgotSecurityAnswerFields))
				{
					pass("The Security Question recovery option is clicked and the Security Fields section is displayed.");
				}
				else
				{
					fail("The Security Question recovery option is clicked and the Security Fields section are not displayed.");
				}
			}
			else
			{
				fail("The Security Question Recovery Option is not displayed.");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
			
		}
		
	}

	/* BNIA-839 Verify that while selecting the "Continue" button after entering the invalid security answer,the alert message should be displayed */
	public void forgotpasswordInvalidSecurityAnswer()
	{
		ChildCreation(" BNIA-839 Verify that while selecting the Continue button after entering the invalid security answer,the alert message should be displayed .");
		try
		{
			String forgotsecurityanswer = BNBasicCommonMethods.getExcelVal("BRM407", sheet, 10); 
			String forgotsecurityanswererroralrt = BNBasicCommonMethods.getExcelVal("BRM407", sheet, 7);
			WebElement forgotSecurityAnswerTextBox = BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityAnswerTextBox");
			if(BNBasicCommonMethods.isElementPresent(forgotSecurityAnswerTextBox))
			{
				forgotSecurityAnswerTextBox.clear();
				log.add("The Security Question Answer Field is present and it is visible.");
				forgotSecurityAnswerTextBox.sendKeys(forgotsecurityanswer);
				WebElement forgotContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinueBtn");
				forgotContinueBtn.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "forgotSecurityAnswerError");
				WebElement forgotSecurityAnswerError =  BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityAnswerError");
				wait.until(ExpectedConditions.visibilityOf(forgotSecurityAnswerError));
				if(BNBasicCommonMethods.isElementPresent(forgotSecurityAnswerError))
				{
					pass("The Security Question Answer error is raised and it is visible.");
					if(forgotSecurityAnswerError.getText().equals(forgotsecurityanswererroralrt))
					{
						pass("The Security Question Answer error is raised and the alert matches with the expected alert. The raised alert was " + forgotSecurityAnswerError.getText().toString());
					}
					else
					{
						fail("The Security Question Answer error is raised and the alert does not matches with the expected alert. The raised alert was " + forgotSecurityAnswerError.getText().toString() + " whereas the expected alert was " + forgotsecurityanswererroralrt);
					}
				}
				else
				{
					fail("The Security Question Answer field is not displayed.");
				}
			}
			else
			{
				fail("The Invalid Security Answer error is not raised or not displayed.");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}

	/* BNIA-840 Verify that while entering the invalid security answer for more than 3 items,the "You have exceeded 3 reset password attempts, please try again after 30 minutes." alert message should be displayed.*/
	public void forgotpasswordInvalidSecurityLimitExceed()
	{
		ChildCreation(" BNIA-840 Verify that while entering the invalid security answer for more than 3 items,the You have exceeded 3 reset password attempts, please try again after 30 minutes. alert message should be displayed.");
		try
		{
			String forgotsecurityanswer = BNBasicCommonMethods.getExcelVal("BRM408", sheet, 10); 
			String forgotsecurityanswererroralrt = BNBasicCommonMethods.getExcelVal("BRM408", sheet, 7);
			
			int j = 1;
			WebElement forgotSecurityAnswerTextBox = BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityAnswerTextBox");
			if(BNBasicCommonMethods.isElementPresent(forgotSecurityAnswerTextBox))
			{
			    do
				{
					forgotSecurityAnswerTextBox.clear();
					forgotSecurityAnswerTextBox.sendKeys(forgotsecurityanswer);
					WebElement forgotContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinueBtn");
					forgotContinueBtn.click();
					BNBasicCommonMethods.waitforElement(wait, obj, "forgotSecurityAnswerError");
					WebElement forgotSecurityAnswerError = BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityAnswerError");
					wait.until(ExpectedConditions.visibilityOf(forgotSecurityAnswerError));
					j++;
				} while(j<4);
				
			    BNBasicCommonMethods.waitforElement(wait, obj, "forgotSecurityAnswerError");
			    WebElement forgotSecurityAnswerError = BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityAnswerError");
				wait.until(ExpectedConditions.visibilityOf(forgotSecurityAnswerError));
				if(BNBasicCommonMethods.isElementPresent(forgotSecurityAnswerError))
				{
					log.add("The Security Question Answer error is raised and it is visible.");
					if(forgotSecurityAnswerError.getText().equals(forgotsecurityanswererroralrt))
					{
						pass("The Security Question Answer error is raised for each attempt and the alert indicating the maximum alert attempt is raised and it matches with the expected alert. The raised alert was " + forgotSecurityAnswerError.getText().toString());
					}
					else
					{
						fail("The Security Question Answer error is raised for each attempt and the alert indicating the maximum alert attempt is raised and it does not matches with the expected alert. The raised alert was " + forgotSecurityAnswerError.getText().toString());
					}
				}
				else
				{
					fail("The Security Question Answer error is not displayed.");
				}
			}
			else
			{
				fail("The Security Question Answer field is not displayed.");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}

	/* BNIA-843 Verify that email address text field should accept the maximum of 40 chars with the combination of numbers,alphabets and special characters..*/
	public void forgotpasswordEmailLength() throws Exception
	{
		ChildCreation("BNIA-843 Verify that email address text field should accept the maximum of 40 chars with the combination of numbers,alphabets and special characters..");
		log.add("The Forgot Password link is found and it is clicked.");
		int brmkey = sheet.getLastRowNum();
		for(int i = 0; i <= brmkey; i++)
		{	
			String tcid = "BRM426";
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				WebElement forgotPasswordEmail = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordEmail");
				forgotPasswordEmail.clear();
				String forgotEmailID = sheet.getRow(i).getCell(6).getStringCellValue().toString();
				try
				{
					if(BNBasicCommonMethods.isElementPresent(forgotPasswordEmail))
					{
						forgotPasswordEmail.sendKeys(forgotEmailID);
						String txtinemail = forgotPasswordEmail.getAttribute("value").toString();
						if(txtinemail.length()<=40)
						{
							pass("The user was successfull in entering the value in the Email Id Field. And the user entered data was " + txtinemail + " and its current length is " + txtinemail.length() + " and the value from the excel file was " + forgotEmailID + " and it is less than or equal to 40 Characters. The length of the EmailId from the excel file was " + forgotEmailID.length());
						}
						else
						{
							fail("The user was successfull in entering the value in the Email Id Field. And the user entered data was " + txtinemail + " and its current length is " + txtinemail.length() + " and the value from the excel file was " + forgotEmailID + " and it is more than 40 Characters. The length of the EmailId from the excel file was " + forgotEmailID.length());
						}
					}
					else
					{
						fail("The Forgot Password Email field is not displayed.");
					}
				}
				catch (Exception e)
				{
					exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
				}
			}
		}
	}

	/* BNIA-844 Verify that while selecting the "continue" button entering the non registered email address,the alert message should be displayed.*/
	public void forgotpasswordNonRegisterEmailAlrt()
	{
		ChildCreation("BNIA-844 Verify that while selecting the continue button entering the non registered email address,the alert message should be displayed.");
		try
		{
			WebElement forgotPasswordEmail = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordEmail");
			forgotPasswordEmail.clear();
			String forgotEmailID = BNBasicCommonMethods.getExcelVal("BRM427", sheet, 6);
			String forgotNonRgstrAlrt = BNBasicCommonMethods.getExcelVal("BRM427", sheet, 7);
			if(BNBasicCommonMethods.isElementPresent(forgotPasswordEmail))
			{
				forgotPasswordEmail.sendKeys(forgotEmailID);
				WebElement forgotContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinueBtn");
				forgotContinueBtn.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "forgotPasswordError");
				WebElement forgotPasswordError = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordError");
				wait.until(ExpectedConditions.visibilityOf(forgotPasswordError));
				
				if(BNBasicCommonMethods.isElementPresent(forgotPasswordError))
				{
					log.add("The password alert is raised to indicate the user and it can be viewed.");
					if(forgotPasswordError.getText().equals(forgotNonRgstrAlrt))
					{
						pass("The password alert raised to indicate the user matches with the expected alert. The raised alert was " + forgotPasswordError.getText());
					}
					else
					{
						fail("The password alert raised to indicate the user doen not matches with the expected alert. The raised alert was " + forgotPasswordError.getText());
					}
				}
				else
				{
					fail("The Forgot Password Error is not displayed.");
				}
			}
			else
			{
				fail("The Forgot Password Email field is not displayed.");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}
			
	/* BNIA-845 Verify that while selecting the "Continue" button after entering the html tags in the email and password field,the text should treat as per classic site behaviour*/
	public void forgotpasswordHtmlEmail()
	{
		ChildCreation("BNIA-845 Verify that while selecting the Continue button after entering the html tags in the email and password field,the text should treat as per classic site behaviour.");
		try
		{
			WebElement forgotPasswordEmail = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordEmail");
			forgotPasswordEmail.clear();
			String forgotEmailID = BNBasicCommonMethods.getExcelVal("BRM428", sheet, 6); 
			String forgotNonRgstrAlrt = BNBasicCommonMethods.getExcelVal("BRM428", sheet, 7);
			if(BNBasicCommonMethods.isElementPresent(forgotPasswordEmail))
			{
				forgotPasswordEmail.sendKeys(forgotEmailID);
				WebElement forgotContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinueBtn");
				forgotContinueBtn.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "forgotPasswordError");
				WebElement forgotPasswordError = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordError");
				wait.until(ExpectedConditions.visibilityOf(forgotPasswordError));
				if(BNBasicCommonMethods.isElementPresent(forgotPasswordError))
				{
					log.add("The password alert is raised to indicate the user and it can be viewed.");
					if(forgotPasswordError.getText().equals(forgotNonRgstrAlrt))
					{
						pass("The password alert raised to indicate the user matches with the expected alert. The raised alert was " + forgotPasswordError.getText());
					}
					else
					{
						fail("The password alert raised to indicate the user doen not matches with the expected alert. The raised alert was " + forgotPasswordError.getText());
					}
				}
				else
				{
					fail("The Forgot Password Error is not displayed.");
				}
			}
			else
			{
				fail("The Forgot Password Email field is not displayed.");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}

	/* BNIA-851 Verify that security answer text field should accept the maximum of 25 chars with the combination of numbers,alphabets and special characters.*/
	public void forgotpasswordSecurityAnswerLimit()
	{
		ChildCreation("BNIA-851 Verify that security answer text field should accept the maximum of 25 chars with the combination of numbers,alphabets and special characters.");
		log.add("The Forgot Password link is found and it is clicked.");
		int brmkey = sheet.getLastRowNum();
		for(int i = 0; i <= brmkey; i++)
		{	
			String tcid = "BRM999";
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				String SecQuesAnswer = sheet.getRow(i).getCell(10).getStringCellValue().toString();
				try
				{
					WebElement forgotSecurityAnswerTextBox = BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityAnswerTextBox");
					forgotSecurityAnswerTextBox.clear();
					if(BNBasicCommonMethods.isElementPresent(forgotSecurityAnswerTextBox))
					{
						forgotSecurityAnswerTextBox.sendKeys(SecQuesAnswer);
						String txtinSecAns = forgotSecurityAnswerTextBox.getAttribute("value").toString();
						if(txtinSecAns.length()<=25)
						{
							pass("The user was successfull in entering the value in the Security Question Field. And the user entered data was " + txtinSecAns + " and its current length is " + txtinSecAns.length() + " and the value from the excel file was " + SecQuesAnswer + " and it is less than or equal to 25 Characters. The length of the Security Answer from the excel file was " + SecQuesAnswer.length());
						}
						else
						{
							fail("The user was successfull in entering the value in the Security Question Field. And the user entered data was " + txtinSecAns + " and its current length is " + txtinSecAns.length() + " and the value from the excel file was " + SecQuesAnswer + " and it is more than 25 Characters. The length of the Security Answer from the excel file was " + SecQuesAnswer.length());
						}
					}
					else
					{
						fail("The Security Answer field is not displayed.");
					}
				}
				catch (Exception e)
				{
					exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
				}
			}
		}
	}

	/* BNIA-850 Verify that while clicking the continue button by selecting the "send a link to my email" option,it should move to the "Check your mail" acknowledgement page and the reset password link should be send to respective email Id. */
	public void forgotpasswordResetByEmailLink()
	{
		ChildCreation("BNIA-850 Verify that while clicking the continue button by selecting the send a link to my email option,it should move to the Check your mail acknowledgement page and the reset password link should be send to respective email Id. ");
		try
		{
			String SuccessMsg1 = BNBasicCommonMethods.getExcelVal("BRM989", sheet, 11);
			String SuccessMsg2 = BNBasicCommonMethods.getExcelVal("BRM989", sheet, 12);
			String SuccessMsg3 = BNBasicCommonMethods.getExcelVal("BRM989", sheet, 13);
			String emailId = BNBasicCommonMethods.getExcelVal("BRM989", sheet, 6); 
			WebElement forgotEmailRecoveryOption = BNBasicCommonMethods.findElement(driver, obj, "forgotEmailRecoveryOption");
			boolean emailchk = forgotEmailRecoveryOption.isSelected();
			if(emailchk==false)
			{
				driver.findElement(By.xpath("(//*[@class='styled-radio'])[1]")).click();
			}
			
			WebElement forgotContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinueBtn");
			forgotContinueBtn.click();
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			BNBasicCommonMethods.waitforElement(wait, obj, "forgotResetSuccessContainer");
			WebElement forgotResetSuccessContainer = BNBasicCommonMethods.findElement(driver, obj, "forgotResetSuccessContainer");
			if(BNBasicCommonMethods.isElementPresent(forgotResetSuccessContainer))
			{
				WebElement forgotSecurityResetSuccess1 = BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityResetSuccess1");
				if(BNBasicCommonMethods.isElementPresent(forgotSecurityResetSuccess1))
				{
					log.add("The user was able to reset the password using the Send a link to my email option.");
					if(forgotSecurityResetSuccess1.getText().contains(SuccessMsg1))
					{
						pass("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it matches the expected Content.The raised alert was " + forgotSecurityResetSuccess1.getText());
					}
					else
					{
						fail("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it does not matche the expected Content.The raised alert was " + forgotSecurityResetSuccess1.getText() );
					}
				}
				else
				{
					fail("The Success Message is not displayed." );
				}
				
				WebElement forgotSecurityResetSuccess2 = BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityResetSuccess2");
				if(BNBasicCommonMethods.isElementPresent(forgotSecurityResetSuccess2))
				{
					log.add("The user was able to reset the password using the Send a link to my email option.");
					if(forgotSecurityResetSuccess2.getText().contains(SuccessMsg2.concat(emailId)))
					{
						pass("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it matches the expected Content.The raised alert was " + forgotSecurityResetSuccess2.getText() );
					}
					else
					{
						fail("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it does not matche the expected Content. The raised alert was " + forgotSecurityResetSuccess2.getText());
					}
				}
				else
				{
					fail("The Success Message is not displayed." );
				}
				
				WebElement forgotSecurityResetSuccess3 = BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityResetSuccess3");
				if(BNBasicCommonMethods.isElementPresent(forgotSecurityResetSuccess3))
				{
					log.add("The user was able to reset the password using the Send a link to my email option.");
					if(forgotSecurityResetSuccess3.getText().equals(SuccessMsg3))
					{
						pass("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it matches the expected Content. The raised alert was " + forgotSecurityResetSuccess3.getText());
					}
					else
					{
						fail("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it does not matche the expected Content.The raised alert was " + forgotSecurityResetSuccess3.getText() );
					}
				}
				else
				{
					fail("The Success Message is not displayed." );
				}
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}

	/*BNIA-841 Verify that while clicking the "Customer Service" link in the "Check your mail" acknowledgement page,it should display the respective overlay.*/
	public void ForgotpasswordContinueShoppingNavigation()
	{
		ChildCreation("BNIA-841 Verify that while clicking the Customer Service link in the Check your mail acknowledgement page,it should display the respective overlay.");
		try
		{
			/*BNBasicCommonMethods.waitforElement(wait, obj, "checkoutTile");
			WebElement CheckoutTile = BNBasicCommonMethods.findElement(driver, obj, "checkoutTile");
			wait.until(ExpectedConditions.elementToBeClickable(CheckoutTile));
			CheckoutTile.click();*/
			Thread.sleep(1500);
			BNBasicCommonMethods.waitforElement(wait, obj, "Forgotpasswordcloseicon");
			WebElement Forgotpasswordcloseicon = BNBasicCommonMethods.findElement(driver, obj, "Forgotpasswordcloseicon");
			Forgotpasswordcloseicon.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
			WebElement CheckoutSignIn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutSignIn");
			if(BNBasicCommonMethods.isElementPresent(CheckoutSignIn))
			{
				pass("The user successfully reset the password and they are navigated to the respective home page as expected. The current URL is ");
			}
			else
			{
				fail("The user successfully reset the password and they are navigated to the some other page than the desired page . The current URL is " );
			}
			wait.until(ExpectedConditions.visibilityOf(CheckoutSignIn));
			Thread.sleep(1000);
			CheckoutSignIn.click();
			
			//log.add("The user is navigated to the Sign In Page.");
			BNBasicCommonMethods.waitforElement(wait, obj, "signInIframe");
			//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInIframe));
			WebElement signInIframe = BNBasicCommonMethods.findElement(driver, obj, "signInIframe");
			driver.switchTo().frame(signInIframe);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong please verify, "+e.getMessage());
		}
	}
	
	/* BNIA-841 Verify that while clicking the "Customer Service" link in the "Check your mail" acknowledgement page,it should display the respective overlay.*/
	public void forgotpasswordContinueShoppingNav()
	{
		ChildCreation("BNIA-841 Verify that while clicking the Customer Service link in the Check your mail acknowledgement page,it should display the respective overlay.");
		try
		{
			String NavigatedURL = BNBasicCommonMethods.getExcelVal("BRM991", sheet, 14); 
			WebElement continueShopBtn = BNBasicCommonMethods.findElement(driver, obj, "continueShopBtn");
			if(BNBasicCommonMethods.isElementPresent(continueShopBtn))
			{
				log.add("The user password has been reset and the mail is sent to their specified email id.");
				continueShopBtn.click();
				//wait.until(ExpectedConditions.visibilityOf(promoCarousel));
				BNBasicCommonMethods.waitforElement(wait, obj, "shoppingbagCount");
				WebElement shoppingbagCount = BNBasicCommonMethods.findElement(driver, obj, "shoppingbagCount");
				wait.until(ExpectedConditions.visibilityOf(shoppingbagCount));
				String currentUrl = driver.getCurrentUrl();
				
				if(currentUrl.contains(NavigatedURL))
				{
					pass("The user successfully reset the password and they are navigated to the respective home page as expected. The current URL is " + currentUrl.toString());
				}
				else
				{
					fail("The user successfully reset the password and they are navigated to the some other page than the desired page . The current URL is " + currentUrl.toString());
				}
			}
			else
			{
				fail("The Continue Shop button is not displayed." );
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}

	/* BNIA-830 Verify that Email Address,New Password,Re-enter New Password,Continue and Cancel buttons should be displayed as per the creative*/
	public void forgotpasswordSecurityQuestionVerification() throws IOException
	{
		ChildCreation("BNIA-830 Verify that Email Address,New Password,Re-enter New Password,Continue and Cancel buttons should be displayed as per the creative.");
		try
		{
			String emailID = BNBasicCommonMethods.getExcelVal("BRM353", sheet, 6);
			String securityAnswer = BNBasicCommonMethods.getExcelVal("BRM353", sheet, 10);
			//driver.get(BNConstants.mobileSiteURL);
			//signInExistingCustomer();
			BNBasicCommonMethods.waitforElement(wait, obj, "forgotPassword");
			WebElement forgotPassword = BNBasicCommonMethods.findElement(driver, obj, "forgotPassword");
			wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
			forgotPassword.click();
			Thread.sleep(4000);
			driver.switchTo().defaultContent();
			BNBasicCommonMethods.waitforElement(wait, obj, "frgtpageframe");
			WebElement frgtpageframe = BNBasicCommonMethods.findElement(driver, obj, "frgtpageframe");
			driver.switchTo().frame(frgtpageframe);
			//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frgtpageframe));
			Thread.sleep(1000);
			BNBasicCommonMethods.waitforElement(wait, obj, "forgotPasswordTitle");
			WebElement forgotPasswordTitle = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordTitle");
			wait.until(ExpectedConditions.visibilityOf(forgotPasswordTitle));
			WebElement forgotPasswordEmail = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordEmail");
			forgotPasswordEmail.sendKeys(emailID);
			log.add("The Email Id is entered in the Password Assistant Page Email Address Field.");
			WebElement forgotContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinueBtn");
			if(BNBasicCommonMethods.isElementPresent(forgotContinueBtn))
			{
				
				forgotContinueBtn.click();
				log.add("The Continue button in the Password Assistant is clicked.");
				Thread.sleep(4000);
				driver.switchTo().defaultContent();
				WebElement ForgotEmailtwo = BNBasicCommonMethods.findElement(driver, obj, "ForgotEmailtwo");
				driver.switchTo().frame(ForgotEmailtwo);
				//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frgtpageframe));
				//wait.until(ExpectedConditions.visibilityOf(forgotContinueBtn));
				
				//BNBasicCommonMethods.waitforElement(wait, obj, "forgotSecurityRecoveryOption");
				WebElement forgotSecurityRecoveryOption = BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityRecoveryOption");
				boolean secchk = forgotSecurityRecoveryOption.isSelected();
				if(secchk==false)
				{
					driver.findElement(By.xpath("(//*[@class='styled-radio'])[2]")).click();
				}
				   BNBasicCommonMethods.waitforElement(wait, obj, "forgotSecurityAnswerFields");
				   WebElement forgotSecurityAnswerFields = BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityAnswerFields");
					wait.until(ExpectedConditions.visibilityOf(forgotSecurityAnswerFields));
					if(BNBasicCommonMethods.isElementPresent(forgotSecurityAnswerFields))
					{
						WebElement forgotSecurityAnswerTextBox = BNBasicCommonMethods.findElement(driver, obj, "forgotSecurityAnswerTextBox");
						forgotSecurityAnswerTextBox.sendKeys(securityAnswer);
						WebElement forgotContinuenewBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinuenewBtn");
						forgotContinuenewBtn.click();
						//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frgtpageframe));
						Thread.sleep(1000);
						WebElement forgotPasswordResetPage = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordResetPage");
						if(BNBasicCommonMethods.isElementPresent(forgotPasswordResetPage))
						{
							pass("The user is navigated to the Password Reset page and the Caption is displayed.");
						}
						else
						{
							fail("The user is not navigated to the Password Reset page.");
						}
						
						driver.switchTo().defaultContent();
						WebElement ForgotChangepassNewName = BNBasicCommonMethods.findElement(driver, obj, "ForgotChangepassNewName");
						driver.switchTo().frame(ForgotChangepassNewName);
						WebElement forgotPasswordNew = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordNew");
						if(BNBasicCommonMethods.isElementPresent(forgotPasswordNew))
						{
							pass("The user is navigated to the Password Reset page and the New Pasword field is displayed.");
						}
						else
						{
							fail("The New Pasword field is not displayed");
						}
						
						WebElement forgotPasswordNewCnfm = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordNewCnfm");
						if(BNBasicCommonMethods.isElementPresent(forgotPasswordNewCnfm))
						{
							pass("The user is navigated to the Password Reset page and the Confirm New Pasword field is displayed.");
						}
						else
						{
							fail("The Confirm New Pasword field is not displayed");
						}
						
						/*WebElement forgotPasswordrules = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordrules");
						if(BNBasicCommonMethods.isElementPresent(forgotPasswordrules))
						{
							pass("The user is navigated to the Password Reset page and the Forgot Password rules is displayed.");
						}
						else
						{
							fail("The Forgot Password rules is not displayed");
						}*/
						WebElement ForgotChangepassNewContinue = BNBasicCommonMethods.findElement(driver, obj, "ForgotChangepassNewContinue");
						if(BNBasicCommonMethods.isElementPresent(ForgotChangepassNewContinue))
						{
							pass("The user is navigated to the Password Reset page and the Continue button is displayed.");
						}
						else
						{
							fail("The Continue button is not displayed");
						}
						
						WebElement forgotCancel = BNBasicCommonMethods.findElement(driver, obj, "forgotCancel");
						if(BNBasicCommonMethods.isElementPresent(forgotCancel))
						{
							pass("The user is navigated to the Password Reset page and the Cancel button is displayed.");
						}
						else
						{
							fail("The Cancel button is not displayed");
						}
					}
					else
					{
						fail("The Security answer field is not displayed." );
					}
				}
			else
			{
				fail("The Continue button is not displayed." );
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is Something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}

	/* BNIA-847 Verify that while selecting the "continue" button without entering the details in the new-password & re-enter password text fields,the alert message should be displayed*/
	public void forgotpasswordBlankPassAlrt() throws IOException
	{
		ChildCreation(" BNIA-847 Verify that while selecting the continue button without entering the details in the new-password & re-enter password text fields,the alert message should be displayed");
		try
		{
			String alert = BNBasicCommonMethods.getExcelVal("BRM430", sheet, 7);
			WebElement forgotContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinueBtn");
			if(BNBasicCommonMethods.isElementPresent(forgotContinueBtn))
			{
				forgotContinueBtn.click();
				BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordError");
				WebElement forgotPasswordError = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordError");
				wait.until(ExpectedConditions.visibilityOf(forgotPasswordError));
				WebElement forgotPasswordError1 = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordError1");
				WebElement forgotPasswordError2 = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordError2");
				String Actalrt1 = forgotPasswordError1.getText();
				String Actalrt2 = forgotPasswordError2.getText();
				
				if(Actalrt1.trim().concat(Actalrt2.trim()).equals(alert.trim()))
				{
					pass("The user is alerted with the appropriate error and the content matches with the expected one. The raised error is " + forgotPasswordError.getText());
				}
				else
				{
					fail("The user is alerted with the appropriate error and the content does not matches with the expected one. The raised error is " + forgotPasswordError.getText());
				}
			}
			else
			{
				fail("The Continue button is not displayed." );
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}
			
	/* BNIA-848 Verify that while selecting the "i" icon in the password rules option,the password rules overlay should be enabled*/
	public void forgotpasswordResetRulesEnable() throws IOException
	{
		ChildCreation("BNIA-848 Verify that while selecting the i icon in the password rules option,the password rules overlay should be enabled.");
		try
		{
			WebElement forgotPasswordrulesiiconShow = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordrulesiiconShow");
			if(BNBasicCommonMethods.isElementPresent(forgotPasswordrulesiiconShow))
			{
				log.add("The Forgot Password Rules i icon is found and displayed.");
				forgotPasswordrulesiiconShow.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "forgotPasswordruleslistnew");
				WebElement forgotPasswordruleslistnew = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordruleslistnew");
				wait.until(ExpectedConditions.visibilityOf(forgotPasswordruleslistnew));
				if(BNBasicCommonMethods.isElementPresent(forgotPasswordruleslistnew))
				{
					pass("The Forgot Password Rules are displayed.");
				}
				else
				{
					fail("The Forgot Password Rules are not displayed.");
				}
			}
			else
			{
				fail("The Forgot Password rules i icon is not displayed." );
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}
	
	/* BNIA-849 Verify that while selecting again the "i" icon in the password rules option,the password rules overlay should be closed */
	public void forgotpasswordResetRulesDisable() throws IOException
	{
		ChildCreation(" BNIA-849 Verify that while selecting again the i icon in the password rules option,the password rules overlay should be closed .");
		try
		{
			log.add("The Forgot Password Rules i icon is found and displayed.");
			WebElement forgotPasswordrulesiiconHide = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordrulesiiconHide");
			forgotPasswordrulesiiconHide.click();
			WebElement forgotPasswordruleslistnew = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordruleslistnew");
			if(forgotPasswordruleslistnew.getAttribute("style")=="display: block;")
			{
				fail("The Forgot Password Rules are displayed and not closed.",log);
			}
			else
			{
				pass("The Forgot Password Rules are not displayed.",log);
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}

	/* BNIA-846 Verify that while selecting the "continue" after entering the "password" and "confirm password" details,the page should be closed and password should reset for the entered email address*/
	public void forgotpasswordResetSuccess() throws IOException
	{
		ChildCreation("BNIA-846 Verify that while selecting the continue after entering the password and confirm password details,the page should be closed and password should reset for the entered email address.");
		try
		{
			String newpass = BNBasicCommonMethods.getExcelVal("BRM429", sheet, 15); 
			String cnfpass = BNBasicCommonMethods.getExcelVal("BRM429", sheet, 16);
			String cnfpassheader = BNBasicCommonMethods.getExcelVal("BRM429", sheet, 17);
			String cnfpassmessage = BNBasicCommonMethods.getExcelVal("BRM429", sheet, 18);
			String SignpgeTitle = BNBasicCommonMethods.getExcelVal("BRM429", sheet, 2);
			WebElement forgotContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotContinueBtn");
			if(BNBasicCommonMethods.isElementPresent(forgotContinueBtn))
			{
				WebElement forgotPasswordNew = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordNew");
				forgotPasswordNew.sendKeys(newpass);
				WebElement forgotPasswordNewCnfm = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordNewCnfm");
				forgotPasswordNewCnfm.sendKeys(cnfpass);
				forgotContinueBtn.click();
				
				Thread.sleep(3000);
				BNBasicCommonMethods.waitforElement(wait, obj, "forgotPasswordResetSuccessAlertHeader");
				WebElement forgotPasswordResetSuccessAlertHeader = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordResetSuccessAlertHeader");
				wait.until(ExpectedConditions.visibilityOf(forgotPasswordResetSuccessAlertHeader));
				if(BNBasicCommonMethods.isElementPresent(forgotPasswordResetSuccessAlertHeader))
				{
					log.add("The alert is displayed for the user.");
					//wait.until(ExpectedConditions.visibilityOf(forgotPasswordResetSuccessAlertHeader));
					if(forgotPasswordResetSuccessAlertHeader.getText().equals(cnfpassheader))
					{
						pass("The alert is displayed for the user and the content matches the expected value. The raised success message was " + forgotPasswordResetSuccessAlertHeader.getText());
					}
					else
					{
						fail("The alert is displayed for the user and the content does not matches the expected value. The raised success message was " + forgotPasswordResetSuccessAlertHeader.getText());
					}
				}
				else
				{
					fail("The Success Message Header is not displayed.");
				}
				
				WebElement forgotPasswordResetSuccessAlertConfirmation = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordResetSuccessAlertConfirmation");
				if(BNBasicCommonMethods.isElementPresent(forgotPasswordResetSuccessAlertConfirmation))
				{
					log.add("The confirmation message is displayed for the user.");
					if(forgotPasswordResetSuccessAlertConfirmation.getText().equals(cnfpassmessage))
					{
						pass("The confirmation displayed for the user and the content matches the expected value. The raised success message was " + forgotPasswordResetSuccessAlertConfirmation.getText(),log);
					}
					else
					{
						System.out.println(forgotPasswordResetSuccessAlertConfirmation.getText());
						fail("The confirmation is displayed for the user and the content does not matches the expected value. The raised success message was " + forgotPasswordResetSuccessAlertConfirmation.getText(),log);
					}
				}
				else
				{
					fail("The Success Message Header is not displayed.");
				}
				
				WebElement forgotPasswordResetSuccessAlertCloseBtn = BNBasicCommonMethods.findElement(driver, obj, "forgotPasswordResetSuccessAlertCloseBtn");
				if(BNBasicCommonMethods.isElementPresent(forgotPasswordResetSuccessAlertCloseBtn))
				{
					pass("The Close Button is visible for the User.");
					forgotPasswordResetSuccessAlertCloseBtn.click();
					pass("The Close Button is Clicked Successfully.");
					BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
					WebElement CheckoutSignIn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutSignIn");
					wait.until(ExpectedConditions.visibilityOf(CheckoutSignIn));
					Thread.sleep(1000);
					CheckoutSignIn.click();
					BNBasicCommonMethods.waitforElement(wait, obj, "signInIframe");
					WebElement signInIframe = BNBasicCommonMethods.findElement(driver, obj, "signInIframe");
					driver.switchTo().frame(signInIframe);
					BNBasicCommonMethods.waitforElement(wait, obj, "pgetitle");
					WebElement pgetitle = BNBasicCommonMethods.findElement(driver, obj, "pgetitle");
					wait.until(ExpectedConditions.visibilityOf(pgetitle));
					Thread.sleep(1000);
					if(pgetitle.getText().equals(SignpgeTitle))
					{
						pass("The page is closed and the user is navigated to the Sign In Page Successfully.");
					}
					else
					{
						fail("The page is closed and the user is not in the Sign In Page. Please Check.");
					}
				}
				else
				{
					fail("The Close Button is not visible for the User.");
				}
			}
			else
			{
				fail("The Continue button is not visible for the User. Please Check.");
			}
			
			Thread.sleep(2000);
			driver.get("http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/home");
			Thread.sleep(2000);
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
	}

	  public void beforeTest() throws Exception
	     {
	    	 //Thread.sleep(5000);
		  driver = bnEmulationSetup.bnASTEmulationSetupCheckout();
	    	 wait = new WebDriverWait(driver, 30);
	     }
	     
	     @AfterClass
	     public void afterTest()
	     {
	         driver.close();
	     }
	     
	     @BeforeMethod
	    	public static void beforeTest(Method name)
	    	{
	    		parent = reporter.startTest(name.getName());
	    		
	    	}
	    	
	    	@org.testng.annotations.AfterMethod
	    	public static void AfterMethod(Method name)
	    	{
	    		reporter.endTest(parent);
	    		reporter.flush();
	    	}
	    	
	    	/*@AfterSuite
	    	public static void closeReporter() 
	    	{
	    		reporter.flush();
	    	}*/
	    	
	    	
	    	public static void ChildCreation(String name)
	    	{
	    		child = reporter.startTest(name);
	    		parent.appendChild(child);
	    	}
	    	
	    	public static void endTest()
	    	{
	    		reporter.endTest(child);
	    	}
	    	
	    	public static void createlog(String val)
	    	{
	    		child.log(LogStatus.INFO, val);
	    	}
	    	
	    	public static void pass(String val)
	    	{
	    		child.log(LogStatus.PASS, val);
	    		endTest();
	    	}
	    	
	    	public static void pass(String val,ArrayList<String> logval)
	    	{
	    		child.log(LogStatus.PASS, val);
	    		for(String lval:logval)
	    		{
	    			createlog(lval);
	    		}
	    		logval.removeAll(logval);
	    		endTest();
	    	}
	    	
	    	public static void fail(String val)
	    	{
	    		child.log(LogStatus.FAIL, val);
	    		endTest();
	    	}
	    	
	    	public static void fail(String val,ArrayList<String> logval)
	    	{
	    		child.log(LogStatus.FAIL, val);
	    		for(String lval:logval)
	    		{
	    			createlog(lval);
	    		}
	    		logval.removeAll(logval);
	    		endTest();
	    	}
	    	
	    	public static  void exception(String val)
	    	{
	    		child.log(LogStatus.ERROR,val);
	    		endTest();
	    	}
	    	
	    	public static void Skip(String val)
	    	{
	    		child.log(LogStatus.SKIP, val);
	    		endTest();
	    	}
		
}