package bnInstore;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import extentReport.extentRptManager;
import bnEmulation.bnEmulationSetup;
import bnInstoreBasicFeatures.BNBasicCommonMethods;
import bnInstoreBasicFeatures.BNConstant;


public class BNInstorePLP 
{
  WebDriver driver;
  public Properties obj;
  public static ArrayList<String> log = new ArrayList<>();
  WebDriverWait wait;
  public static ExtentReports reporter = extentRptManager.getReporter();
  public static ExtentTest parent, child;
  
  HSSFSheet sheet;
  boolean pdpTT = false;
  boolean pdpTF = false;
  boolean pdpFT = false;
  boolean pdpFF = false;
  public boolean desc = false;
  String TTprdtId = "";
  String TFprdtId = "";
  String FTprdtId = "";
  String FFprdtId = "";
 /* String TTprdtId = "9780545685405";
  String TFprdtId = "9780545685368";
  String FTprdtId = "9780594728962";
  String FFprdtId = "9781478602590";*/
  String StoreID = "2932";
  
  //Facets Boolean Variables
  static boolean FacetFlag331 = false;
  static boolean FacetFlag332 = false;
  static boolean FacetFlag333 = false;
  static boolean FacetFlag337 = false;
  static boolean FacetFlag334 = false;
  static boolean FacetFlag335 = false;
  static boolean FacetFlag336 = false;
  static boolean FacetFlag338 = false;
  static boolean FacetFlag339 = false;
  static boolean FacetFlag440 = false;
  static boolean FacetFlag342 = false;
  static boolean Flag227 = false;
  static boolean Flag228 = false;
  static boolean Flag231 = false;
  static boolean Flag232 = false;
  static boolean Flag234 = false;
  
  
  //StorLocator Boolean Variables
  static boolean StoreLocatorFlag622 = false;
  static boolean StoreLocatorFlag625 = false;
  static boolean StoreLocatorFlag626 = false;
  static boolean StoreLocatorFlag1969 = false;
  static boolean StoreLocatorFlag1971 = false;
  static boolean StoreLocatorFlag1973 = false;
  static boolean StoreLocatorFlag1974 = false;
  static boolean StoreLocatorFlag1976 = false;
  static boolean StoreLocatorFlag1978 = false;
  static boolean StoreLocatorFlag1985 = false;
  static boolean StoreLocatorFlag1980 = false;
  static boolean StoreLocatorFlag1981 = false;
  static boolean StoreLocatorFlag1982 = false;
  static boolean StoreLocatorFlag1984 = false;
  static boolean StoreLocatorFlag1979 = false;
  static boolean StoreLocatorFlag1983 = false;
  static boolean StoreLocatorFlag1977 = false;
  
  
  
  
 
  
  public BNInstorePLP() throws Exception
  {
	  obj = new Properties(System.getProperties());
	  obj.load(BNBasicCommonMethods.loadPropertiesFile());
	 // sheet = BNBasicCommonMethods.excelsetUp("display Splash screen");
  }
  
    //@Test(priority = 1)
  public void BNInstoreComplete() throws Exception {
		
		/*BNIA42_Splash_screen();
		BNIA12_Associate_Login(); 
		BNIA06_Home_Screen();
		BNIA07_Header_navigation();
		BNIA43_Hamburger_Menu();
		BNIA11_SearchPage();*/
		//BNIA47_Store_Locator();
		//BNIA50_Store_Detail_Information();
		//BNIA46_Store_Map();
	    BNIA10_PLP_Page();
		//BNIA08_PDP_Page();
	    BNIA10_Facets_Sorting();
	    //BNIA39_Gift_Card();
		
	}
	
  
  
  
/*********************************************** BNIA-42 Ability to display Splash screen on AST ***********************************************************/

      @Test(priority=1)
	  public void BNIA42_Splash_screen()
	  {
		 /* //for kiosk
		  BNIA63_KIOSK();
		  BNIA66_KIOSK();
		  */
		  //for AST
		  BNIA64_AST();
		  BNIA67_AST();
		  BNIA68();
		  
		  //for Both AST and Kiosk
		  BNIA65();
		  
	  }
	  
	  /*  BNIA-63 Verify that splash screen should be displayed as per the creative & fit to the screen in Kiosk device*/
	  public void BNIA63_KIOSK()
	  {
		 ChildCreation("BNIA-63 Verify that splash screen should be displayed as per the creative & fit to the screen in Kiosk device");
		 try
		 {
			 log.add("Splash Screen Displayed with BN Logo");
				WebElement KioskSplashScreeen = BNBasicCommonMethods.findElement(driver, obj, "KioskSplashScreeen");
				BNBasicCommonMethods.WaitForLoading(wait);
		    	if(KioskSplashScreeen.isDisplayed())
		    	{
		    		pass("AST Splash Screen is Displayed Successfully",log);
		    	}
		    	else
		    	{
		    		fail("No AST or KIOSK Splash Screeen is Displayed",log);
		    	}
		 }
		 
		catch(Exception e) 
		{
		  exception("There is something wrong or mmismatch occured"+e.getMessage());
		}
		 
			 
	  }
	  
	  /*  BNIA-64 Verify that splash screen should be displayed as per the creative & fit to the screen in AST device*/
	  public void BNIA64_AST()
	  {
		  ChildCreation("BNIA-64 Verify that splash screen should be displayed as per the creative & fit to the screen in AST device");
			 try
			 {
				 log.add("Splash Screen Displayed with BN Logo");
					BNBasicCommonMethods.waitforElement(wait, obj, "splashScreen");
			    	WebElement Splash=BNBasicCommonMethods.findElement(driver, obj, "splashScreen");
			    	BNBasicCommonMethods.WaitForLoading(wait);
			    	if(Splash.isDisplayed())
			    	{
			    		pass("AST Splash screen is Displayed Successfully",log);
			    	}
			    	else
			    	{
			    		fail("No AST or KIOSK Splash Screeen is Displayed",log);
			    	}
			 }
			 
			catch(Exception e) 
			{
			  exception("There is something wrong or mmismatch occured"+e.getMessage());
			}
	  }
	  
	  /*  BNIA-66 Verify that while tapping the splash screen anywhere in kiosk device,it should takes to the home page */
	  public void BNIA66_KIOSK()
	  {
		  ChildCreation("BNIA-66 Verify that while tapping the splash screen anywhere in kiosk device,it should takes to the home page");
		try
		{
			WebElement KioskSplashScreeen = BNBasicCommonMethods.findElement(driver, obj, "KioskSplashScreeen");
	    	KioskSplashScreeen.click();
	    	Thread.sleep(1000);
	    	BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
	    	WebElement HomePage = BNBasicCommonMethods.findElement(driver, obj, "HomePage");
	  	    if(HomePage.isDisplayed())
	  	    {
	  	    	pass("On tapping splasscreen Home page is Displayed for KIOSK",log);
	  	    }
	  	    else 
	  	    {
	  	    	fail("On tapping SplashScreen Home Page is not Displayed for KIOSK",log);
	  	    }
			
		}
		
		catch(Exception e) 
		{
		 exception("There is something wrong or mmismatch occured"+e.getMessage());
		}
	  }
	  
	  /*BNIA-67 Verify that while tapping the splash screen anywhere in AST device,it should enabled the login overlay*/
	  public void BNIA67_AST()
	  {
		  ChildCreation("BNIA-67 Verify that while tapping the splash screen anywhere in AST device,it should enabled the login overlay");
		  
		try
		{
	    	WebElement Splash=BNBasicCommonMethods.findElement(driver, obj, "splashScreen");
	    	Splash.click();
	    	BNBasicCommonMethods.waitforElement(wait, obj, "signInEmpId");
	    	WebElement id = BNBasicCommonMethods.findElement(driver, obj, "signInEmpId");
	  	    if(id.isDisplayed())
	  	    {
	  	    	pass("On tapping splasscreen Login Ovderlay is Displayed for AST",log);
	  	    }
	  	    else 
	  	    {
	  	    	fail("On tapping SplashScreen Login Overlay is not Displayed for AST",log);
	  	    }
			
		}
		
		catch(Exception e) 
		{
		 exception("There is something wrong or mmismatch occured"+e.getMessage());
		}
	  }
	  
	  /*BNIA-65 Verify that splash screen should be displayed on every session timeout*/
	  public void BNIA65()
	  {
		  ChildCreation("BNIA-65 Verify that splash screen should be displayed on every session timeout");
		try
		{
			TempSignIn();
			Thread.sleep(1000);
			if (driver instanceof JavascriptExecutor) {
			    ((JavascriptExecutor)driver).executeScript("ski_idleTimerObj.idleTimeCount=1");
			} 
			else 
			{
			    
				throw new IllegalStateException("This driver does not support JavaScript!");
			}
			Thread.sleep(2000);
			WebElement TimerSession = BNBasicCommonMethods.findElement(driver, obj, "TimerSession");
			Thread.sleep(2000);
			if(TimerSession.isDisplayed())
			{
				pass("Session timeout overlay is displayed");
			}
			else
			{
				fail("Session timeout overlay is not displayed");
			}
			
			Thread.sleep(7000);
			BNBasicCommonMethods.waitforElement(wait, obj, "splashScreen");
			WebElement splashScreen = BNBasicCommonMethods.findElement(driver, obj, "splashScreen");
			if(splashScreen.isDisplayed())
			{
				pass("Session Logged out after session timeout and splash screen is displayed");
			}
			else
			{
				fail("Session is not Logged out after session timeout and splash screen is not  displayed");
			}
		}
		
		catch(Exception e) 
		{
		 exception("There is something wrong or mmismatch occured"+e.getMessage());
		}
		System.out.println("1/11 - BNIA42_Splash_screen is completed");
	  }
	  
	  /*BNIA-68 Verify that while selecting the Sign Out option in the menu page,it should navigate to the Splash screen page and user should be logged-out in AST Device*/
	  public void BNIA68()
	  {
		  ChildCreation("BNIA-68 Verify that while selecting the Sign Out option in the menu page,it should navigate to the Splash screen page and user should be logged-out in AST Device");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("display Splash screen");
			String Username = BNBasicCommonMethods.getExcelNumericVal("BNIA68", sheet, 3);
			//BNBasicCommonMethods.getExcelVal("BNIA68", sheet, 1);
		    String Password = BNBasicCommonMethods.getExcelVal("BNIA68", sheet, 4);
			BNBasicCommonMethods.waitforElement(wait, obj, "signInEmpId");
	    	WebElement id = BNBasicCommonMethods.findElement(driver, obj, "signInEmpId");
	  	   	WebElement pass = BNBasicCommonMethods.findElement(driver, obj, "signInEmpPass");
	    	BNBasicCommonMethods.signInclearAll(id, pass);
	    	id.sendKeys(Username);
	    	pass.sendKeys(Password);
	    	BNBasicCommonMethods.findElement(driver, obj, "signInClick").click();
	    	BNBasicCommonMethods.WaitForLoading(wait);
	    	BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
	    	Thread.sleep(2000);
	    	BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
	    	WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
	    	HamburgerMenu.click();
	    	Thread.sleep(2000);
	    	BNBasicCommonMethods.waitforElement(wait, obj, "SignOut");
	    	WebElement SignOut = BNBasicCommonMethods.findElement(driver, obj, "SignOut");
	    	SignOut.click();
	    	BNBasicCommonMethods.waitforElement(wait, obj, "splashScreen");
	    	WebElement Splash=BNBasicCommonMethods.findElement(driver, obj, "splashScreen");
	    	BNBasicCommonMethods.WaitForLoading(wait);
	    	if(Splash.isDisplayed())
	    	{
	    		log.add("Page Navigated to Splash Screen");
	    		pass("Successful Signout ",log);
	    	}
	    	else
	    	{
	    		log.add("Page is not noavigated to splash screen");
	    		fail("unSuccessful Signout ",log);
	    	}
	    	
	    	
		}
		
		catch(Exception e) 
		{
			System.out.println(e.getMessage());
		 exception("There is something wrong or mmismatch occured"+e.getMessage());
		}
	  }
	  
  
	/*********************************************** BNIA-12 Ability for an associate to Login on the AST device  *********************************************************/
  
	@Test(priority=2)
	 public void BNIA12_Associate_Login() throws Exception
	  {
		 Thread.sleep(3000);
		  // Associate Login Story 
		  BNIA100();
		  BNIA101();
		  BNIA103();
		  BNIA104();
		  BNIA106();
		  BNIA113();
		  BNIA105();
		  BNIA107();
		  BNIA108();
		  BNIA109();
		  BNIA110();
		  BNIA117();
		  BNIA116();
		  BNIA114();
		  BNIA115();
		  BNIA119();
	  }
	  
    /*BNIA-100 Verify that splash screen should be displayed as per the creative*/
    public void BNIA100() throws Exception 
     {
	   //Verify that Splash Screen should be displayed as per the creative.
	   ChildCreation("BNIA-100 Verify that Splash Screen should be displayed as per the creative.");
	   boolean eleVisible = false;
	   //wait = new WebDriverWait(driver, 30);
	   try
	   {
		BNBasicCommonMethods.WaitForLoading(wait);
		eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "splashScreen");
		if(eleVisible)
		{
			log.add("Splash Screen is Visible");
			//System.out.println("The Splash Screen is visible : " + eleVisible);
			pass("The Splash Screen is present and it is visible",log);
			WebElement splash = BNBasicCommonMethods.findElement(driver, obj, "splashScreen"); 
			splash.click();
			WebElement id = BNBasicCommonMethods.findElement(driver, obj, "signInEmpId");
			WebElement pass = BNBasicCommonMethods.findElement(driver, obj, "signInEmpPass");
			BNBasicCommonMethods.signInclearAll(id,pass);
		}
		else
		{
			fail("The Splash Screen is not visible");
			//System.out.println("The Splash Screen is not visible : " + eleVisible);
		}
	}
	catch(Exception e)
	{
		exception(e.getMessage());	
	}
  }
  		
    /*BNIA-101 Verify that while tapping the splash screen anywhere in the AST device,it should navigate to the AST Login page*/
	public void BNIA101() throws Exception
	{
		//Verify that while tapping the splash screen anywhere in the AST device,it should navigate to the AST Login page
		ChildCreation("BNIA-101 Verify that while tapping the splash screen anywhere in the AST device,it should navigate to the AST Login page.");
		boolean eleVisible = false;
		//wait = new WebDriverWait(driver, 30);
		try
		{
			eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "SignInOverlay");
			if(eleVisible)
			{
				pass("On Tapping Splash Screen,loging overlay is displayed");
			}
			else
			{
				fail("On Tapping Splash Screen,loging overlay is not displayed");
				
			}
		}
		catch(Exception e)
		{
			exception(e.getMessage());	
		}
	}
	
	/*BNIA-103 Verify that Associate login overlay should be displayed as per the creative*/
	public void BNIA103()
	{
		ChildCreation("BNIA-103 Verify that Associate login overlay should be displayed as per the creative");
		
		try
		{
			 sheet = BNBasicCommonMethods.excelsetUp("associate Login");
			 BNBasicCommonMethods.waitforElement(wait, obj, "SignInOverlay");
		     WebElement SigninMask = BNBasicCommonMethods.findElement(driver, obj, "SignInOverlay");
			    
		     // to verify that sign in mask overlay opacity and color 
			String signinmaskcolor = SigninMask.getCssValue("color");
			Color singmask = Color.fromString(signinmaskcolor);
			String HexSignMaskColor = singmask.asHex();
			String signinmaskheight = SigninMask.getCssValue("height");
			String signinmaskwidth = SigninMask.getCssValue("width");
			String SigninOpacity = SigninMask.getCssValue("opacity");
			
			String ExpectedOverlayheight = BNBasicCommonMethods.getExcelNumericVal("BNIA103", sheet, 5);
			String ExpectedOverlaywidth = BNBasicCommonMethods.getExcelNumericVal("BNIA103", sheet, 6);
			String ExpectedOverlayopacity = BNBasicCommonMethods.getExcelNumericVal("BNIA103", sheet, 7);
			String ExpectedOverlaycolor = BNBasicCommonMethods.getExcelNumericVal("BNIA103", sheet, 8);
			
			// signin overlay mask color
			if(HexSignMaskColor.equals(ExpectedOverlaycolor))
				pass("Sign in Mask Color  Current : "+HexSignMaskColor+" Expected : "+ExpectedOverlaycolor);
			else
				fail("Sign in Mask Color  Current : "+HexSignMaskColor+" Expected : "+ExpectedOverlaycolor);
					
			if(signinmaskheight.equals(ExpectedOverlayheight)&&signinmaskwidth.equals(ExpectedOverlaywidth))
				pass("Sign in Mask Height and width Current : "+signinmaskwidth+"&"+signinmaskheight+" Expected : "+ExpectedOverlaywidth+"&"+ExpectedOverlayheight);
			else
				fail("Sign in Mask Height and width Current : "+signinmaskwidth+"&"+signinmaskheight+" Expected : "+ExpectedOverlaywidth+"&"+ExpectedOverlayheight);

			if(SigninOpacity.equals(ExpectedOverlayopacity))
				pass("Sign in Mask Opacity Current : "+SigninOpacity+" Expected : "+ExpectedOverlayopacity);
			else
				fail("Sign in Mask Opacity Current : "+SigninOpacity+" Expected : "+ExpectedOverlayopacity);
			 
		}
		
		catch(Exception e)
  	    {
  	    	exception("There is something wrong,so please verify"+e.getMessage());
  	    }
	}
	
	/*BNIA-104 Verify that gray background should be displayed in the Associate login page as per the creative*/
	public void BNIA104()
	{
		ChildCreation("BNIA-104 Verify that gray background should be displayed in the Associate login page as per the creative");
		
		try
		{
			 sheet = BNBasicCommonMethods.excelsetUp("associate Login");
			 BNBasicCommonMethods.waitforElement(wait, obj, "SignInOverlay");
		     WebElement SigninMask = BNBasicCommonMethods.findElement(driver, obj, "SignInOverlay");
			 String signinmaskcolor = SigninMask.getCssValue("color");
			 Color singmask = Color.fromString(signinmaskcolor);
			 String HexSignMaskColor = singmask.asHex();
			 String ExpectedOverlaycolor = BNBasicCommonMethods.getExcelNumericVal("BNIA103", sheet, 8);
			 
			// signin overlay mask color
				if(HexSignMaskColor.equals(ExpectedOverlaycolor))
					pass("Sign in Mask Color  Current : "+HexSignMaskColor+" Expected : "+ExpectedOverlaycolor);
				else
					fail("Sign in Mask Color  Current : "+HexSignMaskColor+" Expected : "+ExpectedOverlaycolor);
		}
		
		catch(Exception e)
  	    {
  	    	exception("There is something wrong,so please verify"+e.getMessage());
  	    }
		
	}

	/*BNIA-105 Verify that Font size, Font style, alignments & padding of login page should be displayed as per the creative*/
	public void BNIA105()
	{
		ChildCreation("BNIA-105 Verify that Font size, Font style, alignments & padding of login page should be displayed as per the creative");
		
		 try
		 {
			 sheet = BNBasicCommonMethods.excelsetUp("associate Login");	
		      BNBasicCommonMethods.waitforElement(wait, obj, "SignInOverlay");
		      WebElement SigninMask = BNBasicCommonMethods.findElement(driver, obj, "SignInOverlay");
		      
		      
			    // to verify that sign in mask overlay opacity and color 
				String signinmaskcolor = SigninMask.getCssValue("color");
				Color singmask = Color.fromString(signinmaskcolor);
				String HexSignMaskColor = singmask.asHex();
				String signinmaskheight = SigninMask.getCssValue("height");
				String signinmaskwidth = SigninMask.getCssValue("width");
				String SigninOpacity = SigninMask.getCssValue("opacity");

				WebElement EmailField = BNBasicCommonMethods.findElement(driver, obj, "EmailField");
				String Fieldcolor = EmailField.getCssValue("-webkit-text-stroke-color");
				Color Filedcolors = Color.fromString(Fieldcolor);
				String hexFieldcolors = Filedcolors.asHex();
				String EmailfieldWidth = EmailField.getCssValue("width");
			    String EmailfieldHeight = EmailField.getCssValue("height");

			    // email Label 
			    WebElement EmailLabel = BNBasicCommonMethods.findElement(driver, obj, "EmailLabel");
				String EmailLabelfont = EmailLabel.getCssValue("font-family");
				String EmailLabelname = EmailLabel.getText().toString();
				String Emailfontsize = EmailLabel.getCssValue("font-size");
				String EmailLabelfontcolor = EmailLabel.getCssValue("color");
				Color Ecolors = Color.fromString(EmailLabelfontcolor);
				String hexEcolors = Ecolors.asHex();
				//System.out.println();
				
				WebElement PasswordField = BNBasicCommonMethods.findElement(driver, obj, "PasswordField");
				String pFieldcolor=PasswordField.getCssValue("-webkit-text-stroke-color");
				Color pFiledcolors = Color.fromString(pFieldcolor);
				String hexpFieldcolors = pFiledcolors.asHex();
			    String PassfieldWidth=PasswordField.getCssValue("width");
			    String PassfieldHeight=PasswordField.getCssValue("height");
				//System.out.println();
				
			    //password Label
			    WebElement PassLabel = BNBasicCommonMethods.findElement(driver, obj, "PassLabel");
				String PassLabelfont=PassLabel.getCssValue("font-family");
				String PassLabelName=PassLabel.getText().toString();
				String Passfontsize =PassLabel.getCssValue("font-size");
				String PassLabelfontcolor=PassLabel.getCssValue("color");
				Color Pcolors = Color.fromString(PassLabelfontcolor);
				String hexPcolors = Pcolors.asHex();
				//System.out.println(); 
			    
			    // to sign in button
				WebElement SnField = BNBasicCommonMethods.findElement(driver, obj, "SnField");
				String SnfieldWidth = SnField.getCssValue("width");
				String SnfieldHeight = SnField.getCssValue("height");
				
				//Sign in Label
				WebElement Signinbtn=BNBasicCommonMethods.findElement(driver, obj, "signInClick");
				String SnFieldcolor=Signinbtn.getCssValue("background-color");
				Color SnFiledcolors = Color.fromString(SnFieldcolor);
				String hexSnFieldcolors = SnFiledcolors.asHex();
				String Signinbtnfont = Signinbtn.getCssValue("font-family");
				String SigninButtonName = Signinbtn.getText().toString();
				String Signinfontsize = Signinbtn.getCssValue("font-size");
				String Signinfontcolor=Signinbtn.getCssValue("color");
				Color Sncolors = Color.fromString(Signinfontcolor);
				String hexSncolors = Sncolors.asHex();
				
				String ExpectedOverlayheight = BNBasicCommonMethods.getExcelNumericVal("BNIA103", sheet, 5);
				String ExpectedOverlaywidth = BNBasicCommonMethods.getExcelNumericVal("BNIA103", sheet, 6);
				String ExpectedOverlayopacity = BNBasicCommonMethods.getExcelNumericVal("BNIA103", sheet, 7);
				String ExpectedOverlaycolor = BNBasicCommonMethods.getExcelNumericVal("BNIA103", sheet, 8);
	
				String ExpectedEmailFieldStrokecolor = BNBasicCommonMethods.getExcelNumericVal("BNIA103", sheet, 8);
				String ExpectedEmailFieldheight = BNBasicCommonMethods.getExcelNumericVal("BNIA104", sheet, 5);
				String ExpectedEmailFieldwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA104", sheet, 6);
				String ExpectedEmailFieldLabel = BNBasicCommonMethods.getExcelVal("BNIA104", sheet, 9);
				String ExpectedEmailFieldLabelFont = BNBasicCommonMethods.getExcelVal("BNIA104", sheet, 10);
				String ExpectedEmailFieldLabelcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA104", sheet, 4);
				String ExpectedEmailFieldLabelFontsize = BNBasicCommonMethods.getExcelNumericVal("BNIA104", sheet, 11);

				String ExpectedPassFieldStrokecolor = BNBasicCommonMethods.getExcelNumericVal("BNIA103", sheet, 8);
				String ExpectedPassFieldheight = BNBasicCommonMethods.getExcelNumericVal("BNIA105", sheet, 5);
				String ExpectedPassFieldwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA105", sheet, 6);
				String ExpectedPassFieldLabel = BNBasicCommonMethods.getExcelVal("BNIA105", sheet, 9);
				String ExpectedPassFieldLabelFont = BNBasicCommonMethods.getExcelVal("BNIA105", sheet, 10);
				String ExpectedPassFieldLabelcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA105", sheet, 4);
				String ExpectedPassFieldLabelFontsize = BNBasicCommonMethods.getExcelNumericVal("BNIA105", sheet, 11);

				String ExpectedSignButtonFillcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA106", sheet, 8);
				String ExpectedSignButtonheight = BNBasicCommonMethods.getExcelNumericVal("BNIA106", sheet, 5);
				String ExpectedSignButtonwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA106", sheet, 6);
				String ExpectedSignButtonLabel = BNBasicCommonMethods.getExcelVal("BNIA106", sheet, 9);
				String ExpectedSignButtonFontName = BNBasicCommonMethods.getExcelVal("BNIA106", sheet, 10);
				String ExpectedSignButtonFontcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA106", sheet, 4);
				String ExpectedSignButtonFontsize = BNBasicCommonMethods.getExcelNumericVal("BNIA106", sheet, 11);
				
				//Separating font from font-family
				String EmailFontName = BNBasicCommonMethods.isFontName(EmailLabelfont);
				String PassFontName = BNBasicCommonMethods.isFontName(PassLabelfont);
		        String SigninBtnFontName = BNBasicCommonMethods.isFontName(Signinbtnfont);
		        
				// signin overlay mask color
				if(HexSignMaskColor.equals(ExpectedOverlaycolor))
					pass("Sign in Mask Color  Current : "+HexSignMaskColor+" Expected : "+ExpectedOverlaycolor);
				else
					fail("Sign in Mask Color  Current : "+HexSignMaskColor+" Expected : "+ExpectedOverlaycolor);
				
				if(signinmaskheight.equals(ExpectedOverlayheight)&&signinmaskwidth.equals(ExpectedOverlaywidth))
					pass("Sign in Mask Height and width Current : "+signinmaskwidth+"&"+signinmaskheight+" Expected : "+ExpectedOverlaywidth+"&"+ExpectedOverlayheight);
				else
					fail("Sign in Mask Height and width Current : "+signinmaskwidth+"&"+signinmaskheight+" Expected : "+ExpectedOverlaywidth+"&"+ExpectedOverlayheight);

				if(SigninOpacity.equals(ExpectedOverlayopacity))
					pass("Sign in Mask Opacity Current : "+SigninOpacity+" Expected : "+ExpectedOverlayopacity);
				else
					fail("Sign in Mask Opacity Current : "+SigninOpacity+" Expected : "+ExpectedOverlayopacity);

				//System.out.println();

				// to verify email filed size,color,Label font,font color
				if(hexFieldcolors.equals(ExpectedEmailFieldStrokecolor))
					pass("Email Field stroke color  Current : "+hexFieldcolors+" Expected : "+ExpectedEmailFieldStrokecolor);
				else
					fail("Email Field stroke color  Current : "+hexFieldcolors+" Expected : "+ExpectedEmailFieldStrokecolor);
				
				if(EmailfieldWidth.equals(ExpectedEmailFieldwidth) && EmailfieldHeight.equals(ExpectedEmailFieldheight))
					pass("Email Field Current Width & Height : "+EmailfieldWidth+"&"+EmailfieldHeight+" Expected : "+ExpectedEmailFieldwidth+"&"+ExpectedEmailFieldheight);
				else
					fail("Email Field Current Width & Height : "+EmailfieldWidth+"&"+EmailfieldHeight+" Expected : "+ExpectedEmailFieldwidth+"&"+ExpectedEmailFieldheight);
			
				if(EmailLabelname.equals(ExpectedEmailFieldLabel))
					pass("Email Field Label Name Current : "+EmailLabelname+" Expected : "+ExpectedEmailFieldLabel);
				else
					fail("Email Field Label Name Current : "+EmailLabelname+" Expected : "+ExpectedEmailFieldLabel);
				
				if(EmailFontName.equals(ExpectedEmailFieldLabelFont))
					pass("Email Field Label Font Current : "+EmailFontName+" Expected : "+ExpectedEmailFieldLabelFont);
				else
					fail("Email Field Label Font Current : "+EmailFontName+" Expected : "+ExpectedEmailFieldLabelFont);
				
				if(hexEcolors.equals(ExpectedEmailFieldLabelcolor))
					pass("Email Field Label Font color Current : "+hexEcolors+" Expected : "+ExpectedEmailFieldLabelcolor);
				else
					fail("Email Field Label Font color Current : "+hexEcolors+" Expected : "+ExpectedEmailFieldLabelcolor);
				
				if(Emailfontsize.equals(ExpectedEmailFieldLabelFontsize))
					pass("Email Field Label Font Size Current : "+Emailfontsize+" Expected : "+ExpectedEmailFieldLabelFontsize);
				else
					fail("Email Field Label Font Size Current : "+Emailfontsize+" Expected : "+ExpectedEmailFieldLabelFontsize);
				
			
		         //to verify Password field 	
				if(hexpFieldcolors.equals(ExpectedPassFieldStrokecolor))
					pass("Password Field stroke color Current : "+hexpFieldcolors+" Expected : "+ExpectedPassFieldStrokecolor);
				else
					fail("Password Field stroke color Current : "+hexpFieldcolors+" Expected : "+ExpectedPassFieldStrokecolor);
				
				
				if(PassfieldWidth.equals(ExpectedPassFieldwidth) && PassfieldHeight.equals(ExpectedPassFieldheight))
					pass("Password Field Current Width & Height : "+PassfieldWidth+"&"+PassfieldHeight+" Expected : "+ExpectedPassFieldwidth+"&"+ExpectedPassFieldheight);
				else
					fail("Password Field Current Width & Height : "+PassfieldWidth+"&"+PassfieldHeight+" Expected : "+ExpectedPassFieldwidth+"&"+ExpectedPassFieldheight);
			
				if(PassLabelName.equals(ExpectedPassFieldLabel))
					pass("Password Field Label Name Current : "+PassLabelName+" Expected : "+ExpectedPassFieldLabel);
				else
					fail("Password Field Label Name Current : "+PassLabelName+" Expected : "+ExpectedPassFieldLabel);
				
				if(PassFontName.equals(ExpectedPassFieldLabelFont))
					pass("Password Field Label Font Current : "+PassFontName+" Expected : "+ExpectedPassFieldLabelFont);
				else
					fail("Password Field Label Font Current : "+PassFontName+" Expected : "+ExpectedPassFieldLabelFont);
				
				if(hexPcolors.equals(ExpectedPassFieldLabelcolor))
					pass("Password Field Label Font color Current : "+hexPcolors+" Expected : "+ExpectedPassFieldLabelcolor);
				else
					fail("Password Field Label Font color Current : "+hexPcolors+" Expected : "+ExpectedPassFieldLabelcolor);
				
				if(Passfontsize.equals(ExpectedPassFieldLabelFontsize))
					pass("Password Field Label Font Size Current : "+Passfontsize+" Expected : "+ExpectedPassFieldLabelFontsize);
				else
					fail("Password Field Label Font Size Current : "+Passfontsize+" Expected : "+ExpectedPassFieldLabelFontsize);
				//System.out.println();
				
				
				// to verify sign overlay
				if(hexSnFieldcolors.equals(ExpectedSignButtonFillcolor))
					pass("Sign In Button Fill color Current : "+hexSnFieldcolors+" Expected : "+ExpectedSignButtonFillcolor);
				else
					fail("Sign In Button Fill color Current : "+hexSnFieldcolors+" Expected : "+ExpectedSignButtonFillcolor);
				
				if(SnfieldWidth.equals(ExpectedSignButtonwidth) && SnfieldHeight.equals(ExpectedSignButtonheight))
					pass("Sign In Button Current Width & Height : "+SnfieldWidth+"&"+SnfieldHeight+" Expected : "+ExpectedSignButtonwidth+"&"+ExpectedSignButtonheight);
				else
					fail("Sign In Button Current Width & Height : "+SnfieldWidth+"&"+SnfieldHeight+" Expected : "+ExpectedSignButtonwidth+"&"+ExpectedSignButtonheight);
			    
				if(SigninButtonName.equals(ExpectedSignButtonLabel))
					pass("Sign In Button Label Name Current : "+SigninButtonName+" Expected : "+ExpectedSignButtonLabel);
				else
					fail("Sign In Button Label Name Current : "+SigninButtonName+" Expected : "+ExpectedSignButtonLabel);
				
				if(SigninBtnFontName.equals(ExpectedSignButtonFontName))
					pass("Sign In Button Label Font Current : "+SigninBtnFontName+" Expected : "+ExpectedSignButtonFontName);
				else
					fail("Sign In Button Label Font Current : "+SigninBtnFontName+" Expected : "+ExpectedSignButtonFontName);
				
				if(hexSncolors.equals(ExpectedSignButtonFontcolor))
					pass("Sign In Button Label Font color Current : "+hexSncolors+" Expected : "+ExpectedSignButtonFontcolor);
				else
					fail("Sign In Button Label Font color Current : "+hexSncolors+" Expected : "+ExpectedSignButtonFontcolor);
				
				if(Signinfontsize.equals(ExpectedSignButtonFontsize))
					pass("Sign In Button Label Font Size Current : "+Signinfontsize+" Expected : "+ExpectedSignButtonFontsize);
				else
					fail("Sign In Button Label Font Size Current : "+Signinfontsize+" Expected : "+ExpectedSignButtonFontsize);
				//System.out.println();
		 }
		 
		 catch(Exception e)
	  	  {
	  	    	exception("There is something wrong,so please verify"+e.getMessage());
	  	  }
	}
	
	/*BNIA-106 Verify that email text box & password fields in the associate login overlay should be displayed as per the creative*/
	public void BNIA106()
	{
		ChildCreation("BNIA-106 Verify that email text box & password fields in the associate login overlay should be displayed as per the creative");
		
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("associate Login");
			
			WebElement EmailField = BNBasicCommonMethods.findElement(driver, obj, "EmailField");
			String Fieldcolor = EmailField.getCssValue("-webkit-text-stroke-color");
			Color Filedcolors = Color.fromString(Fieldcolor);
			String hexFieldcolors = Filedcolors.asHex();
			String EmailfieldWidth = EmailField.getCssValue("width");
		    String EmailfieldHeight = EmailField.getCssValue("height");

		    // email Label 
		    WebElement EmailLabel = BNBasicCommonMethods.findElement(driver, obj, "EmailLabel");
			String EmailLabelfont = EmailLabel.getCssValue("font-family");
			String EmailLabelname = EmailLabel.getText().toString();
			String Emailfontsize = EmailLabel.getCssValue("font-size");
			String EmailLabelfontcolor = EmailLabel.getCssValue("color");
			Color Ecolors = Color.fromString(EmailLabelfontcolor);
			String hexEcolors = Ecolors.asHex();
			//System.out.println();
			
			WebElement PasswordField = BNBasicCommonMethods.findElement(driver, obj, "PasswordField");
			String pFieldcolor=PasswordField.getCssValue("-webkit-text-stroke-color");
			Color pFiledcolors = Color.fromString(pFieldcolor);
			String hexpFieldcolors = pFiledcolors.asHex();
		    String PassfieldWidth=PasswordField.getCssValue("width");
		    String PassfieldHeight=PasswordField.getCssValue("height");
			
		    //password Label
		    WebElement PassLabel = BNBasicCommonMethods.findElement(driver, obj, "PassLabel");
			String PassLabelfont=PassLabel.getCssValue("font-family");
			String PassLabelName=PassLabel.getText().toString();
			String Passfontsize =PassLabel.getCssValue("font-size");
			String PassLabelfontcolor=PassLabel.getCssValue("color");
			Color Pcolors = Color.fromString(PassLabelfontcolor);
			String hexPcolors = Pcolors.asHex(); 
			
			String ExpectedEmailFieldStrokecolor = BNBasicCommonMethods.getExcelNumericVal("BNIA103", sheet, 8);
			String ExpectedEmailFieldheight = BNBasicCommonMethods.getExcelNumericVal("BNIA104", sheet, 5);
			String ExpectedEmailFieldwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA104", sheet, 6);
			String ExpectedEmailFieldLabel = BNBasicCommonMethods.getExcelVal("BNIA104", sheet, 9);
			String ExpectedEmailFieldLabelFont = BNBasicCommonMethods.getExcelVal("BNIA104", sheet, 10);
			String ExpectedEmailFieldLabelcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA104", sheet, 4);
			String ExpectedEmailFieldLabelFontsize = BNBasicCommonMethods.getExcelNumericVal("BNIA104", sheet, 11);
			
			String ExpectedPassFieldStrokecolor = BNBasicCommonMethods.getExcelNumericVal("BNIA103", sheet, 8);
			String ExpectedPassFieldheight = BNBasicCommonMethods.getExcelNumericVal("BNIA105", sheet, 5);
			String ExpectedPassFieldwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA105", sheet, 6);
			String ExpectedPassFieldLabel = BNBasicCommonMethods.getExcelVal("BNIA105", sheet, 9);
			String ExpectedPassFieldLabelFont = BNBasicCommonMethods.getExcelVal("BNIA105", sheet, 10);
			String ExpectedPassFieldLabelcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA105", sheet, 4);
			String ExpectedPassFieldLabelFontsize = BNBasicCommonMethods.getExcelNumericVal("BNIA105", sheet, 11);
			
			//Separating font from font-family
			String EmailFontName = BNBasicCommonMethods.isFontName(EmailLabelfont);
			String PassFontName = BNBasicCommonMethods.isFontName(PassLabelfont);
			
			// to verify email filed size,color,Label font,font color
			if(hexFieldcolors.equals(ExpectedEmailFieldStrokecolor))
				pass("Email Field stroke color  Current : "+hexFieldcolors+" Expected : "+ExpectedEmailFieldStrokecolor);
			else
				fail("Email Field stroke color  Current : "+hexFieldcolors+" Expected : "+ExpectedEmailFieldStrokecolor);
			
			if(EmailfieldWidth.equals(ExpectedEmailFieldwidth) && EmailfieldHeight.equals(ExpectedEmailFieldheight))
				pass("Email Field Current Width & Height : "+EmailfieldWidth+"&"+EmailfieldHeight+" Expected : "+ExpectedEmailFieldwidth+"&"+ExpectedEmailFieldheight);
			else
				fail("Email Field Current Width & Height : "+EmailfieldWidth+"&"+EmailfieldHeight+" Expected : "+ExpectedEmailFieldwidth+"&"+ExpectedEmailFieldheight);
		
			if(EmailLabelname.equals(ExpectedEmailFieldLabel))
				pass("Email Field Label Name Current : "+EmailLabelname+" Expected : "+ExpectedEmailFieldLabel);
			else
				fail("Email Field Label Name Current : "+EmailLabelname+" Expected : "+ExpectedEmailFieldLabel);
			
			if(EmailFontName.equals(ExpectedEmailFieldLabelFont))
				pass("Email Field Label Font Current : "+EmailFontName+" Expected : "+ExpectedEmailFieldLabelFont);
			else
				fail("Email Field Label Font Current : "+EmailFontName+" Expected : "+ExpectedEmailFieldLabelFont);
			
			if(hexEcolors.equals(ExpectedEmailFieldLabelcolor))
				pass("Email Field Label Font color Current : "+hexEcolors+" Expected : "+ExpectedEmailFieldLabelcolor);
			else
				fail("Email Field Label Font color Current : "+hexEcolors+" Expected : "+ExpectedEmailFieldLabelcolor);
			
			if(Emailfontsize.equals(ExpectedEmailFieldLabelFontsize))
				pass("Email Field Label Font Size Current : "+Emailfontsize+" Expected : "+ExpectedEmailFieldLabelFontsize);
			else
				fail("Email Field Label Font Size Current : "+Emailfontsize+" Expected : "+ExpectedEmailFieldLabelFontsize);
			
		
	         //to verify Password field 	
			if(hexpFieldcolors.equals(ExpectedPassFieldStrokecolor))
				pass("Password Field stroke color Current : "+hexpFieldcolors+" Expected : "+ExpectedPassFieldStrokecolor);
			else
				fail("Password Field stroke color Current : "+hexpFieldcolors+" Expected : "+ExpectedPassFieldStrokecolor);
			
			
			if(PassfieldWidth.equals(ExpectedPassFieldwidth) && PassfieldHeight.equals(ExpectedPassFieldheight))
				pass("Password Field Current Width & Height : "+PassfieldWidth+"&"+PassfieldHeight+" Expected : "+ExpectedPassFieldwidth+"&"+ExpectedPassFieldheight);
			else
				fail("Password Field Current Width & Height : "+PassfieldWidth+"&"+PassfieldHeight+" Expected : "+ExpectedPassFieldwidth+"&"+ExpectedPassFieldheight);
		
			if(PassLabelName.equals(ExpectedPassFieldLabel))
				pass("Password Field Label Name Current : "+PassLabelName+" Expected : "+ExpectedPassFieldLabel);
			else
				fail("Password Field Label Name Current : "+PassLabelName+" Expected : "+ExpectedPassFieldLabel);
			
			if(PassFontName.equals(ExpectedPassFieldLabelFont))
				pass("Password Field Label Font Current : "+PassFontName+" Expected : "+ExpectedPassFieldLabelFont);
			else
				fail("Password Field Label Font Current : "+PassFontName+" Expected : "+ExpectedPassFieldLabelFont);
			
			if(hexPcolors.equals(ExpectedPassFieldLabelcolor))
				pass("Password Field Label Font color Current : "+hexPcolors+" Expected : "+ExpectedPassFieldLabelcolor);
			else
				fail("Password Field Label Font color Current : "+hexPcolors+" Expected : "+ExpectedPassFieldLabelcolor);
			
			if(Passfontsize.equals(ExpectedPassFieldLabelFontsize))
				pass("Password Field Label Font Size Current : "+Passfontsize+" Expected : "+ExpectedPassFieldLabelFontsize);
			else
				fail("Password Field Label Font Size Current : "+Passfontsize+" Expected : "+ExpectedPassFieldLabelFontsize);
			//System.out.println();
			
		}
		
		catch(Exception e)
	  	 {
	  	    exception("There is something wrong,so please verify"+e.getMessage());
	     }
	}
	
	/*BNIA-113 Verify that Secure Signin button should be displayed as per the creative*/
	public void BNIA113() throws Exception
	 {
	   ChildCreation(" BNIA-113 Verify that Secure Signin button should be displayed as per the creative");
	   try
	   {
	        sheet = BNBasicCommonMethods.excelsetUp("associate Login");	
		    // to sign in button
			WebElement SnField = BNBasicCommonMethods.findElement(driver, obj, "SnField");
			String SnfieldWidth = SnField.getCssValue("width");
			String SnfieldHeight = SnField.getCssValue("height");
			
			//Sign in Label
			WebElement Signinbtn=BNBasicCommonMethods.findElement(driver, obj, "signInClick");
			String SnFieldcolor=Signinbtn.getCssValue("background-color");
			Color SnFiledcolors = Color.fromString(SnFieldcolor);
			String hexSnFieldcolors = SnFiledcolors.asHex();
			String Signinbtnfont = Signinbtn.getCssValue("font-family");
			String SigninButtonName = Signinbtn.getText().toString();
			String Signinfontsize = Signinbtn.getCssValue("font-size");
			String Signinfontcolor=Signinbtn.getCssValue("color");
			Color Sncolors = Color.fromString(Signinfontcolor);
			String hexSncolors = Sncolors.asHex();
			
			String ExpectedSignButtonFillcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA106", sheet, 8);
			String ExpectedSignButtonheight = BNBasicCommonMethods.getExcelNumericVal("BNIA106", sheet, 5);
			String ExpectedSignButtonwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA106", sheet, 6);
			String ExpectedSignButtonLabel = BNBasicCommonMethods.getExcelVal("BNIA106", sheet, 9);
			String ExpectedSignButtonFontName = BNBasicCommonMethods.getExcelVal("BNIA106", sheet, 10);
			String ExpectedSignButtonFontcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA106", sheet, 4);
			String ExpectedSignButtonFontsize = BNBasicCommonMethods.getExcelNumericVal("BNIA106", sheet, 11);
			
			//Separating font from font-family
	        String SigninBtnFontName = BNBasicCommonMethods.isFontName(Signinbtnfont);
	        
			// to verify sign overlay
			if(hexSnFieldcolors.equals(ExpectedSignButtonFillcolor))
				pass("Sign In Button Fill color Current : "+hexSnFieldcolors+" Expected : "+ExpectedSignButtonFillcolor);
			else
				fail("Sign In Button Fill color Current : "+hexSnFieldcolors+" Expected : "+ExpectedSignButtonFillcolor);
			
			if(SnfieldWidth.equals(ExpectedSignButtonwidth) && SnfieldHeight.equals(ExpectedSignButtonheight))
				pass("Sign In Button Current Width & Height : "+SnfieldWidth+"&"+SnfieldHeight+" Expected : "+ExpectedSignButtonwidth+"&"+ExpectedSignButtonheight);
			else
				fail("Sign In Button Current Width & Height : "+SnfieldWidth+"&"+SnfieldHeight+" Expected : "+ExpectedSignButtonwidth+"&"+ExpectedSignButtonheight);
		    
			if(SigninButtonName.equals(ExpectedSignButtonLabel))
				pass("Sign In Button Label Name Current : "+SigninButtonName+" Expected : "+ExpectedSignButtonLabel);
			else
				fail("Sign In Button Label Name Current : "+SigninButtonName+" Expected : "+ExpectedSignButtonLabel);
			
			if(SigninBtnFontName.equals(ExpectedSignButtonFontName))
				pass("Sign In Button Label Font Current : "+SigninBtnFontName+" Expected : "+ExpectedSignButtonFontName);
			else
				fail("Sign In Button Label Font Current : "+SigninBtnFontName+" Expected : "+ExpectedSignButtonFontName);
			
			if(hexSncolors.equals(ExpectedSignButtonFontcolor))
				pass("Sign In Button Label Font color Current : "+hexSncolors+" Expected : "+ExpectedSignButtonFontcolor);
			else
				fail("Sign In Button Label Font color Current : "+hexSncolors+" Expected : "+ExpectedSignButtonFontcolor);
			
			if(Signinfontsize.equals(ExpectedSignButtonFontsize))
				pass("Sign In Button Label Font Size Current : "+Signinfontsize+" Expected : "+ExpectedSignButtonFontsize);
			else
				fail("Sign In Button Label Font Size Current : "+Signinfontsize+" Expected : "+ExpectedSignButtonFontsize);
			//System.out.println();
	   }
	   
	   catch(Exception e)
	  	{
	  	    exception("There is something wrong,so please verify"+e.getMessage());
	    }
	 }
	  
	/*BNIA-107 Verify that email text box should accept both alphanumeric & special characters*/
	public void BNIA107()
	{
		//Verify that email text box should accept both alphanumeric & special characters
		ChildCreation("BNIA-107 Verify that email text box should accept both alphanumeric & special characters.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("associate Login");	
       //	String str1 = "qscvbhtredsawedfer@tgnhtrewsasx.vbj45@t qscvbhtredsawedfer@tgnhtrewsasx.vbj45@tre";
			String str1 = BNBasicCommonMethods.getExcelNumericVal("BNIA109", sheet, 2);
			
			//String str1 = "7879465131245 979845656 dsf543543vb 564515656";
			String[] str = str1.split("\n");
		    WebElement id = BNBasicCommonMethods.findElement(driver, obj, "signInEmpId");
	     	for (int ctr = 0; ctr < str.length; ctr++) 
		     {
				id.clear();
				id.sendKeys(str[ctr]);
				String emailcurrentVal = id.getAttribute("value").toString();
				if (!emailcurrentVal.isEmpty())
				{
					log.add(str[ctr]+"Expected Email Field Name");
					log.add(emailcurrentVal+ "Currrent Email Field Name");
					pass("Email field accepts Characters and Numbers",log);
				}
				else
				{
					log.add(str[ctr]+"Expected string");
					log.add(emailcurrentVal+ "Currrent String");
					fail("Email field is not accepts Characters and Numbers",log);
				}
		    }
		  }
		 catch(Exception e)
		 {
			exception(e.getMessage());	
		 }
	}
	
	/*BNIA-108 Verify that password field should accept both alphanumeric and special characters*/
	public void BNIA108() throws Exception
	{
		//Verify that password field should accept both alphanumeric and special characters	
	    ChildCreation("BNIA-108 Verify that password field should accept both alphanumeric and special characters.");
	   try
		{
		    sheet = BNBasicCommonMethods.excelsetUp("associate Login");
			String str1 = BNBasicCommonMethods.getExcelVal("BNIA110", sheet, 3);
			String[] str = str1.split("\n");
			WebElement pass = BNBasicCommonMethods.findElement(driver, obj, "signInEmpPass");
			for (int ctr = 0; ctr < str.length; ctr++) 
			   {
				   	pass.clear();
				   	pass.sendKeys(str[ctr]);
				   	String passcurrentVal = pass.getAttribute("value").toString();
				   	if (!passcurrentVal.isEmpty())
					{
						log.add(str[ctr]+" Expected Password");
						log.add(passcurrentVal+ "Current Password");
						pass("Password field accepts Characters and Numbers",log);
					}
					else
					{
						log.add(str[ctr]+" Expected Password");
						log.add(passcurrentVal+ "Current Password");
						fail("Password field accepts Characters and Numbers",log);
					}
			   }
		}
		catch(Exception e)
		{
			exception(e.getMessage());	
		}
	}
		
	/*BNIA-109 Verify that email text box should accept maximum 40 characers*/
	public void BNIA109() throws Exception
	{
		
		// Verify that Email text box should accept maximum 9 characers
		ChildCreation("BNIA-109 Verify that Email text box should accept maximum 9 characers.");
		
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("associate Login");	
       //	String str1 = "qscvbhtredsawedfer@tgnhtrewsasx.vbj45@t qscvbhtredsawedfer@tgnhtrewsasx.vbj45@tre";
			String str1 = BNBasicCommonMethods.getExcelNumericVal("BNIA109", sheet, 2);
			
			//String str1 = "7879465131245 979845656 dsf543543vb 564515656";
			String[] str = str1.split("\n");
		    WebElement id = BNBasicCommonMethods.findElement(driver, obj, "signInEmpId");
	     	for (int ctr = 0; ctr < str.length; ctr++) 
		     {
				id.clear();
				id.sendKeys(str[ctr]);
				String emailcurrentVal = id.getAttribute("value").toString();
				if (emailcurrentVal.length() <= 9)
				{
					log.add(str[ctr].length()+" Length");
					log.add(emailcurrentVal.length() + " Length");
					pass("Email Value Not Exceded",log);
				}
				else
				{
					log.add(str[ctr].length()+" Length");
					log.add(emailcurrentVal.length() + " Length");
					fail("Email Value Exceed the limit", log);
				}
		    }
		  }
		 catch(Exception e)
		 {
			exception(e.getMessage());	
		 }
	}
		
	/*BNIA-110 Verify that password field should accept maximum of 40 characters*/
	public void BNIA110() throws Exception
	{
		sheet = BNBasicCommonMethods.excelsetUp("associate Login");
		// Verify that Password text box should accept maximum 40 characers
		ChildCreation("BNIA-110 Verify that Password text box should accept maximum 40 characers.");
		String str1 = BNBasicCommonMethods.getExcelVal("BNIA110", sheet, 3);
		//String str1 = "qscvbhtredsawedfer@tgnhtrewsasx.vbj45@t qscvbhtredsawedfer@tgnhtrewsasx.vbj45@tre";
		String[] str = str1.split("\n");
		try
		{
		WebElement pass = BNBasicCommonMethods.findElement(driver, obj, "signInEmpPass");
		for (int ctr = 0; ctr < str.length; ctr++) 
		   {
			   	pass.clear();
			   	pass.sendKeys(str[ctr]);
			   	String passcurrentVal = pass.getAttribute("value").toString();
			   	if (passcurrentVal.length() <= 40)
				{
					log.add(str[ctr].length()+" Length");
					log.add(passcurrentVal.length() + " Length");
					pass("Password Value Not Exceded",log);
				}
				else
				{
					log.add(str[ctr].length()+" Length");
					log.add(passcurrentVal.length() + " Length");
					fail("Password Value Exceed the limit", log);
				}
		   }
		}
		catch(Exception e)
		{
			exception(e.getMessage());	
		}
	}
		
	/*BNIA-117 Verify that while selecting the "Secure SignIn" button without entering the values in the associate login overlay,the alert message should be displayed*/
	public void BNIA117()
	{
		//Verify that while selecting the "Secure SignIn" button by entering the Invalid values in the associate login overlay,the alert message should be displayed	
        ChildCreation("BNIA-117 Verify that while selecting the Secure SignIn button by entering the Invalid values in the associate login overlay,the alert message should be displayed.");
        try 
        {     
        	  sheet = BNBasicCommonMethods.excelsetUp("associate Login");
        	  String getColor = BNBasicCommonMethods.getExcelNumericVal("BNIA117", sheet, 4);
        	  WebElement pass = BNBasicCommonMethods.findElement(driver, obj, "signInEmpPass");
        	  WebElement id = BNBasicCommonMethods.findElement(driver, obj, "signInEmpId");
        	  WebElement Signin = BNBasicCommonMethods.findElement(driver, obj, "signInClick");	
	          BNBasicCommonMethods.signInclearAll(id, pass);
        	  Signin.click();
		      String  EmailError = BNBasicCommonMethods.findElement(driver, obj, "InvalidEmail").getText();
		      String  Emailboxerr = BNBasicCommonMethods.findElement(driver, obj, "EmailEmptyBoxcolor").getCssValue("border-top-color");
		      Color  boxclr = Color.fromString(Emailboxerr);
			  String  Hexbxcolor = boxclr.asHex().toUpperCase();
		      String EmailBorEr = BNBasicCommonMethods.findElement(driver, obj, "EmptyAlertSymbol").getText();
	        	if(EmailError.equals("Please enter an valid user name"))
		        {
		        	pass("Empty Alert For Email Displayed");
		        	
		        }
		        else
		        {
		        	fail("No Empty Alert For Email Displayed");
		        }
	        	if(EmailBorEr.equals("!"))
	        	{
	        	    pass("! Displayed for Email field Empty");	
	        	}
	        	else 
	        	{
	        		fail("No ! Displayed for Email Filed Empty");
	        	}
	        	if(Hexbxcolor.equals(getColor))
	        	{
	        		pass("Email TextBox Highlighted as red color for empty alert");
	        	}
	        	else
	        	{
	        		fail("Email TextBox Not Highlighted as red color for empty alert");
	        	}
        }
        catch(Exception e)
		{
			exception(e.getMessage());	
		}
        
        
	 }
		
	/*BNIA-116 Verify that while selecting the "Secure Signin" button after entering the invalid credentials,the alert message should be displayed*/
	public void BNIA116()
	{
		//Verify that while selecting the "Secure SignIn" button without entering the values in the associate login overlay,the alert message should be displayed	
        ChildCreation("BNIA-116 Verify that while selecting the Secure SignIn button without entering the values in the associate login overlay,the alert message should be displayed.");
        try 
        {     
        	  sheet = BNBasicCommonMethods.excelsetUp("associate Login");
        	  String userNm = BNBasicCommonMethods.getExcelNumericVal("BNIA116", sheet, 2);
        	  String getColor = BNBasicCommonMethods.getExcelNumericVal("BNIA116", sheet, 4);
        	  WebElement pass = BNBasicCommonMethods.findElement(driver, obj, "signInEmpPass");
        	  WebElement id = BNBasicCommonMethods.findElement(driver, obj, "signInEmpId");
        	  WebElement Signin = BNBasicCommonMethods.findElement(driver, obj, "signInClick");	
	          BNBasicCommonMethods.signInclearAll(id, pass);
	          id.click();
        	  id.sendKeys(userNm);
	          Signin.click();
		      String  PassError = BNBasicCommonMethods.findElement(driver, obj, "InvalidEmail").getText();
		      String  Passboxerr = BNBasicCommonMethods.findElement(driver, obj, "EmailEmptyBoxcolor").getCssValue("border-top-color");
		      Color  boxclr = Color.fromString(Passboxerr);
			  String  Hexbxcolor = boxclr.asHex().toUpperCase();
		      String PassBorEr = BNBasicCommonMethods.findElement(driver, obj, "PassAlertSymbol").getText();
	        	if(PassError.equals("Please enter a valid password"))
		        {
		        	pass("Invalid & Empty Alert For Password Field is Displayed");
		        	
		        }
		        else
		        {
		        	fail("No Invalid Alert For Password field is Displayed");
		        }
	        	if(PassBorEr.equals("!"))
	        	{
	        	    pass("! Displayed for Empty Password");	
	        	}
	        	else 
	        	{
	        		fail("No ! Displayed for Empty Password");
	        	}
	        	if(Hexbxcolor.equals(getColor))
	        	{
	        		pass("Password TextBox Highlighted as red color for empty alert");
	        	}
	        	else
	        	{
	        		fail("Password TextBox Not Highlighted as red color for empty alert");
	        	}
        }
        catch(Exception e)
		{
			exception(e.getMessage());	
		}
        
        
	 }

	/*BNIA-114 Verify that while selecting the "Secure Signin" button after entering the valid credentials,the associate should be logged-in successfully*/
	public void BNIA114()
	{
		//Verify that while selecting the "Secure Signin" button after entering the valid credentials,the associate should be logged-in successfully	
        ChildCreation("BNIA-114 Verify that while selecting the Secure Signin button after entering the valid credentials,the associate should be logged-in successfully.");
        boolean eleVisible = false;
        try 
        {
        	sheet = BNBasicCommonMethods.excelsetUp("associate Login");
        	String Uname  = BNBasicCommonMethods.getExcelNumericVal("BNIA114", sheet, 2);
        	String Pass = BNBasicCommonMethods.getExcelVal("BNIA114", sheet, 3);
	        WebElement id = BNBasicCommonMethods.findElement(driver, obj, "signInEmpId");
	        WebElement pass = BNBasicCommonMethods.findElement(driver, obj, "signInEmpPass");
	        WebElement Signin = BNBasicCommonMethods.findElement(driver, obj, "signInClick");	
	        BNBasicCommonMethods.signInclearAll(id, pass);
	        id.sendKeys(Uname);
	        pass.sendKeys(Pass);
	        Signin.click();
	        Thread.sleep(2000);
	        eleVisible= BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
		        if(eleVisible)
		        {
		        	pass("Successful signin after selecting Secure SignIn Button ");
		        }
		        else
		        {
		        	fail("NO Successful signin after selecting Secure SignIn Button ");
		        }
        }
        catch(Exception e)
		{
			exception(e.getMessage());	
		}
        
        
	 }
		
	/*BNIA-115 Verify that home page should be displayed after the successful associate authentication*/
	public void BNIA115()
	{
		//Verify that home page should be displayed after the successful associate authentication		
        ChildCreation("BNIA-115 Verify that home page should be displayed after the successful associate authentication.");
        boolean eleVisible = false;
        try 
        {
	        eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
		        if(eleVisible)
		        {
		        	pass("Hompage Displayed after selecting Secure SignIn Button ");
		        }
		        else
		        {
		        	fail("HomePage not Displayed after selecting Secure SignIn Button ");
		        }
        }
        catch(Exception e)
		{
			exception(e.getMessage());	
		}
        
        
	}
		
	/*BNIA-119 Verify that while selecting the "sign out" button in the Menu page,the user should be logged-out and should navigate to the splash screen*/
	public void BNIA119()
	{
		//Verify that while selecting the "sign out" button in the Menu page,the user should be logged-out and should navigate to the splash screen
		ChildCreation("BNIA-119 Verify that while selecting the sign out button in the Menu page,the user should be logged-out and should navigate to the splash screen.");
       
        
        boolean eleVisible = false;
        try 
        {
        	
        	
        	
        	
        	BNBasicCommonMethods.WaitForLoading(wait);
        	Thread.sleep(2000);
        	WebElement Hamburger = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
        	Hamburger.click();
        	BNBasicCommonMethods.waitforElement(wait, obj, "SignOut");
        	WebElement Signoutbtn = BNBasicCommonMethods.findElement(driver, obj, "SignOut");
        	Signoutbtn.click();
        	eleVisible=BNBasicCommonMethods.waitforElement(wait, obj, "splashScreen");
        	if(eleVisible)
        	{
        		 pass("Successful Logout and Splash Screen Displayed");
        	}
        	else
        	{
        		fail("No Successful Logout and Splash Screen Displayed");
        	}
        }
        catch(Exception e)
		{
			exception(e.getMessage());	
		}
        System.out.println("2/11 - BNIA12_Associate_Login is completed");
	}
	
	
/******************************************* BNIA-39  Ability to perform Gift Card balance lookup **************************************************************/
	
	
    //@Test(priority=13)
	public void BNIA39_Gift_Card() throws Exception
	{
		// GiftCard Look UP Story
	  TempSignIn();
	  BNIAGIFTNAVIGATE();
	  BNIA71();
	  BNIA72();
	  BNIA75();
	  BNIA73();
	  BNIA74();
	  BNIA76();
	  BNIA173();
	  BNIA78();
	  BNIA82();
	  BNIA83();
	  BNIA80();
	  BNIA79();
	  //TemSignOut();
	  TempSessionTimeout();
	  
	}
	  
	/*Temp Login*/ 
	public void TempSignIn()
	  {    
		  // Temp Signin  
		  //WebDriverWait wait =  new WebDriverWait(driver, 30);
		    try
		    {
		    	Thread.sleep(500);
		    	BNBasicCommonMethods.waitforElement(wait, obj, "splashScreen");
		    	WebElement Splash=BNBasicCommonMethods.findElement(driver, obj, "splashScreen");
		    	BNBasicCommonMethods.WaitForLoading(wait);
		    	Splash.click();
		    	BNBasicCommonMethods.waitforElement(wait, obj, "signInEmpId");
		    	WebElement id = BNBasicCommonMethods.findElement(driver, obj, "signInEmpId");
		  	   	WebElement pass = BNBasicCommonMethods.findElement(driver, obj, "signInEmpPass");
		    	BNBasicCommonMethods.signInclearAll(id, pass);
		    	Thread.sleep(500);
		    	id.sendKeys("234567854");
		    	pass.sendKeys("skava");
		    	Thread.sleep(3000);
		    	BNBasicCommonMethods.findElement(driver, obj, "signInClick").click();
		    	BNBasicCommonMethods.WaitForLoading(wait);
		    	BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
		    	Thread.sleep(2000);
		    }
		    catch(Exception e)
		    {
		    	 exception(e.getMessage()); 
		    }
	  }

	 
    /*Temp Signout*/
	public void TemSignOut() throws Exception 
	 {
		  //BNBasicCommonMethods.WaitForLoading(wait);	
		  Thread.sleep(2500);
	   	  BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
 	  WebElement Hamburger = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
 	  BNBasicCommonMethods.WaitForLoading(wait);
	 	  Hamburger.click();
	 	  Thread.sleep(2000);
	 	  BNBasicCommonMethods.waitforElement(wait, obj, "SignOut");
	 	  WebElement SignOut= BNBasicCommonMethods.findElement(driver, obj, "SignOut");
	 	 SignOut.click();
	 }
	
	public void TempSessionTimeout()
	{
		try
		{
			Thread.sleep(1500);
			if (driver instanceof JavascriptExecutor) {
			    ((JavascriptExecutor)driver).executeScript("ski_idleTimerObj.idleTimeCount=1");
			} 
			else 
			{
			    
				throw new IllegalStateException("This driver does not support JavaScript!");
			}
			Thread.sleep(3000);
			WebElement TimerSession = BNBasicCommonMethods.findElement(driver, obj, "TimerResetButton");
			TimerSession.click();
			Thread.sleep(5000);
		}
		catch(Exception e)
		{
			exception("There is something wrong or mmismatch occured"+e.getMessage());
		}
	}
	   
	/*Temproary Sign in code*/ 
  	public void BNIAGIFTNAVIGATE() throws Exception 
  	{
  	  Thread.sleep(2000);
  	  BNBasicCommonMethods.WaitForLoading(wait);
  	  BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
  	  WebElement Hamburger = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
  	  BNBasicCommonMethods.WaitForLoading(wait);
	  Hamburger.click();
	  Thread.sleep(2000);
	  BNBasicCommonMethods.waitforElement(wait, obj, "pancakegiftcard");
	  WebElement Giftcard= BNBasicCommonMethods.findElement(driver, obj, "pancakegiftcard");
	  Giftcard.click();
	  BNBasicCommonMethods.WaitForLoading(wait);
	}
  		
  	/*BNIA-71 Verify that Gift Card lookup page should be displayed as per the creative*/
  	public void BNIA71()
  	{
  		ChildCreation("BNIA-71 Verify that Gift Card lookup page should be displayed as per the creative");
  		try 
  		{
  			Thread.sleep(3000);
  			sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
		 	WebElement TextBox= BNBasicCommonMethods.findElement(driver, obj, "GiftCardTextBox");
			String TextBoxcolor=TextBox.getCssValue("border-top-color");// top bottom or side
			Color Giftcardtextbox = Color.fromString(TextBoxcolor);
			String Giftcardtextboxstrokecolor = Giftcardtextbox.asHex();
		    String GiftcardTextBoxWidth=TextBox.getCssValue("width");
		    String GiftCardTextBoxHeight=TextBox.getCssValue("height");
			
			
		    /*to verify gift card Label text  size and color*/ 
		    WebElement GiftLabel = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt");
			String GiftCardLabelFontName=GiftLabel.getCssValue("font-family");
			String GiftCardLabelName = GiftLabel.getAttribute("placeholder");
			//String GiftCardLabelName =GiftLabel.getText().toString();
			String GiftCardLabelFontSize=GiftLabel.getCssValue("font-size");
			//System.out.println();
			
			/*get font name from font family*/ 
			String GiftCardTextBoxLFontName=BNBasicCommonMethods.isFontName(GiftCardLabelFontName);
			String ExpectedGiftcardtextboxstrokecolor = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 3);
			String ExpectedGiftCardTextBoxHeight = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 5);
			String ExpectedGiftcardTextBoxWidth = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 6	);
		
			String ExpectedGiftCardTextBoxLFontName = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 7);
			String ExpectedGiftCardLabelFontSize = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 9);
			String ExpectedGiftCardLabeName = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 8);

				 if(Giftcardtextboxstrokecolor.equals(ExpectedGiftcardtextboxstrokecolor))
					pass("Gift Card TextField stroke color Current : "+Giftcardtextboxstrokecolor+" Expected : "+ExpectedGiftcardtextboxstrokecolor);
				else
					fail("Gift Card TextField stroke color Current : "+Giftcardtextboxstrokecolor+" Expected : "+ExpectedGiftcardtextboxstrokecolor);
					
					
			     if(GiftcardTextBoxWidth.equals(ExpectedGiftcardTextBoxWidth) && GiftCardTextBoxHeight.equals(ExpectedGiftCardTextBoxHeight))
					pass("Gift Card TextField Current Width & Height : "+GiftcardTextBoxWidth+"&"+GiftCardTextBoxHeight+" Expected : "+ExpectedGiftcardTextBoxWidth+"&"+ExpectedGiftCardTextBoxHeight);
				else
					fail("Gift Card TextField Current Width & Height : "+GiftcardTextBoxWidth+"&"+GiftCardTextBoxHeight+" Expected : "+ExpectedGiftcardTextBoxWidth+"&"+ExpectedGiftCardTextBoxHeight);
		
				if(GiftCardLabelName.equals(ExpectedGiftCardLabeName))
					pass("Gift Card TextField Label Name Current : "+GiftCardLabelName+" Expected : "+ExpectedGiftCardLabeName);
				else
					fail("Gift Card TextField Label Name Current : "+GiftCardLabelName+" Expected : "+ExpectedGiftCardLabeName);
						
				if(GiftCardTextBoxLFontName.equals(ExpectedGiftCardTextBoxLFontName))
					pass("Gift Card TextField Label Font Current : "+GiftCardTextBoxLFontName+" Expected : "+ExpectedGiftCardTextBoxLFontName);
				else
					fail("Gift Card TextField Label Font Current : "+GiftCardTextBoxLFontName+" Expected : "+ExpectedGiftCardTextBoxLFontName);
							
				if(GiftCardLabelFontSize.equals(ExpectedGiftCardLabelFontSize))
					pass("Gift Card TextField Label Font Size Current : "+GiftCardLabelFontSize+" Expected : "+ExpectedGiftCardLabelFontSize);
				else
					fail("Gift Card TextField Label Font Size Current : "+GiftCardLabelFontSize+" Expected : "+ExpectedGiftCardLabelFontSize);
						
				//System.out.println();
						
				
	     //to verify check balance button 
		WebElement ChField = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
		WebElement GiftCardCheckBalance = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
		String Chbtn = GiftCardCheckBalance.getCssValue("color");
		Color Chcolor = Color.fromString(Chbtn);
		String ChBtnColor = Chcolor.asHex();
		String CheckbuttonWidth=ChField.getCssValue("width");
		String CheckButtonHeight=ChField.getCssValue("height");
	
		String expebtnwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 6);
		String expebtnheight = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 5);
		String expecbtncolor = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 3);
		String expecbtnname = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 7);
		
		   if(ChBtnColor.equals(expecbtncolor))
		   {
			   log.add("Expected"+expecbtncolor);
			   log.add("Current"+ChBtnColor);
			   pass("CheckButton Color is Matched ",log);
		   }
		   
		   else
		   {
			   log.add("Expected"+expecbtncolor);
			   log.add("Current"+ChBtnColor);
			   fail("CheckButton Color is Not Matched ",log);
			   
		   }
		   
		   if(CheckbuttonWidth.equals(expebtnwidth) && CheckButtonHeight.equals(expebtnheight) )
		   {
			   log.add("Expected width & Height"+expebtnwidth+expebtnheight);
			   log.add("Current width & Height"+CheckbuttonWidth+expebtnheight);
			   pass("CheckButton Width and height is Matched ",log);
		   }
		   else
		   {
			   log.add("Expected width & Height "+expebtnwidth+expebtnheight);
			   log.add("Current width & Height "+CheckbuttonWidth+expebtnheight);
			   fail("CheckButton Width and Height is not Matched ",log);
		   }
		   
		   if(ChField.getText().equals(expecbtnname))
		   {
			   log.add("Expected Text "+expecbtnname);
			   log.add("Current text "+ChField.getText());
			   pass("CheckButton text is Matched ",log);
		   }
		   
		   else
		   {
			   log.add("Expected"+expecbtnname);
			   log.add("Current"+ChField.getText());
			   fail("CheckButton text is not Matched ",log);
		   }
		   
  		}
  		
  		catch(Exception e)
  		{
  			exception("There is something wrong, so please check");
  		}
  	}
  	
  	/*BNIA-72 Verify that Font size, Font style, alignments & padding of Gift Card lookup page should be displayed as per the comps*/
  	public void BNIA72()
  	{
  		ChildCreation("BNIA-72 Verify that Font size, Font style, alignments & padding of Gift Card lookup page should be displayed as per the comps");
  		try 
  		{
  			Thread.sleep(3000);
  			sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
		 	WebElement TextBox= BNBasicCommonMethods.findElement(driver, obj, "GiftCardTextBox");
			String TextBoxcolor=TextBox.getCssValue("border-top-color");// top bottom or side
			Color Giftcardtextbox = Color.fromString(TextBoxcolor);
			String Giftcardtextboxstrokecolor = Giftcardtextbox.asHex();
		    String GiftcardTextBoxWidth=TextBox.getCssValue("width");
		    String GiftCardTextBoxHeight=TextBox.getCssValue("height");
			
			
		    /*to verify gift card Label text  size and color*/ 
		    WebElement GiftLabel = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt");
			String GiftCardLabelFontName=GiftLabel.getCssValue("font-family");
			String GiftCardLabelName = GiftLabel.getAttribute("placeholder");
			//String GiftCardLabelName =GiftLabel.getText().toString();
			String GiftCardLabelFontSize=GiftLabel.getCssValue("font-size");
			//System.out.println();
			
			/*get font name from font family*/ 
			String GiftCardTextBoxLFontName=BNBasicCommonMethods.isFontName(GiftCardLabelFontName);
			String ExpectedGiftcardtextboxstrokecolor = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 3);
			String ExpectedGiftCardTextBoxHeight = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 5);
			String ExpectedGiftcardTextBoxWidth = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 6	);
		
			String ExpectedGiftCardTextBoxLFontName = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 7);
			String ExpectedGiftCardLabelFontSize = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 9);
			String ExpectedGiftCardLabeName = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 8);
			
			
		
		   // System.out.println();	
			
				 if(Giftcardtextboxstrokecolor.equals(ExpectedGiftcardtextboxstrokecolor))
					pass("Gift Card TextField stroke color Current : "+Giftcardtextboxstrokecolor+" Expected : "+ExpectedGiftcardtextboxstrokecolor);
				else
					fail("Gift Card TextField stroke color Current : "+Giftcardtextboxstrokecolor+" Expected : "+ExpectedGiftcardtextboxstrokecolor);
					
					
			     if(GiftcardTextBoxWidth.equals(ExpectedGiftcardTextBoxWidth) && GiftCardTextBoxHeight.equals(ExpectedGiftCardTextBoxHeight))
					pass("Gift Card TextField Current Width & Height : "+GiftcardTextBoxWidth+"&"+GiftCardTextBoxHeight+" Expected : "+ExpectedGiftcardTextBoxWidth+"&"+ExpectedGiftCardTextBoxHeight);
				else
					fail("Gift Card TextField Current Width & Height : "+GiftcardTextBoxWidth+"&"+GiftCardTextBoxHeight+" Expected : "+ExpectedGiftcardTextBoxWidth+"&"+ExpectedGiftCardTextBoxHeight);
		
				if(GiftCardLabelName.equals(ExpectedGiftCardLabeName))
					pass("Gift Card TextField Label Name Current : "+GiftCardLabelName+" Expected : "+ExpectedGiftCardLabeName);
				else
					fail("Gift Card TextField Label Name Current : "+GiftCardLabelName+" Expected : "+ExpectedGiftCardLabeName);
						
				if(GiftCardTextBoxLFontName.equals(ExpectedGiftCardTextBoxLFontName))
					pass("Gift Card TextField Label Font Current : "+GiftCardTextBoxLFontName+" Expected : "+ExpectedGiftCardTextBoxLFontName);
				else
					fail("Gift Card TextField Label Font Current : "+GiftCardTextBoxLFontName+" Expected : "+ExpectedGiftCardTextBoxLFontName);
							
				if(GiftCardLabelFontSize.equals(ExpectedGiftCardLabelFontSize))
					pass("Gift Card TextField Label Font Size Current : "+GiftCardLabelFontSize+" Expected : "+ExpectedGiftCardLabelFontSize);
				else
					fail("Gift Card TextField Label Font Size Current : "+GiftCardLabelFontSize+" Expected : "+ExpectedGiftCardLabelFontSize);
						
				//System.out.println();
						
				
	     //to verify check balance button 
		WebElement ChField = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
		WebElement GiftCardCheckBalance = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
		String Chbtn = GiftCardCheckBalance.getCssValue("color");
		Color Chcolor = Color.fromString(Chbtn);
		String ChBtnColor = Chcolor.asHex();
		String CheckbuttonWidth=ChField.getCssValue("width");
		String CheckButtonHeight=ChField.getCssValue("height");

		String expebtnwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 6);
		String expebtnheight = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 5);
		String expecbtncolor = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 3);
		String expecbtnname = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 7);
		
		   if(ChBtnColor.equals(expecbtncolor))
		   {
			   log.add("Expected"+expecbtncolor);
			   log.add("Current"+ChBtnColor);
			   pass("CheckButton Color is Matched ",log);
		   }
		   
		   else
		   {
			   log.add("Expected"+expecbtncolor);
			   log.add("Current"+ChBtnColor);
			   fail("CheckButton Color is Not Matched ",log);
			   
		   }
		   
		   if(CheckbuttonWidth.equals(expebtnwidth) && CheckButtonHeight.equals(expebtnheight) )
		   {
			   log.add("Expected width & Height"+expebtnwidth+expebtnheight);
			   log.add("Current width & Height"+CheckbuttonWidth+expebtnheight);
			   pass("CheckButton Width and height is Matched ",log);
		   }
		   else
		   {
			   log.add("Expected width & Height "+expebtnwidth+expebtnheight);
			   log.add("Current width & Height "+CheckbuttonWidth+expebtnheight);
			   fail("CheckButton Width and Height is not Matched ",log);
		   }
		   
		   if(ChField.getText().equals(expecbtnname))
		   {
			   log.add("Expected Text "+expecbtnname);
			   log.add("Current text "+ChField.getText());
			   pass("CheckButton text is Matched ",log);
		   }
		   
		   else
		   {
			   log.add("Expected"+expecbtnname);
			   log.add("Current"+ChField.getText());
			   fail("CheckButton text is not Matched ",log);
		   }
		   
  		}
  		
  		catch(Exception e)
  		{
  			exception("There is something wrong, so please check");
  		}
  	}
  	
  	/*BNIA-75 Verify that Gift Card text box & Check Balance button should be displayed as per the creative*/
  	public void BNIA75()
  	{
  		ChildCreation("BNIA-75 Verify that Gift Card text box & Check Balance button should be displayed as per the creative");
  		try 
  		{
  			Thread.sleep(3000);
  			sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
		 	WebElement TextBox= BNBasicCommonMethods.findElement(driver, obj, "GiftCardTextBox");
			String TextBoxcolor=TextBox.getCssValue("border-top-color");// top bottom or side
			Color Giftcardtextbox = Color.fromString(TextBoxcolor);
			String Giftcardtextboxstrokecolor = Giftcardtextbox.asHex();
		    String GiftcardTextBoxWidth=TextBox.getCssValue("width");
		    String GiftCardTextBoxHeight=TextBox.getCssValue("height");
			
			
		    /*to verify gift card Label text  size and color*/ 
		    WebElement GiftLabel = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt");
			String GiftCardLabelFontName=GiftLabel.getCssValue("font-family");
			String GiftCardLabelName = GiftLabel.getAttribute("placeholder");
			//String GiftCardLabelName =GiftLabel.getText().toString();
			String GiftCardLabelFontSize=GiftLabel.getCssValue("font-size");
			//System.out.println();
			
			/*get font name from font family*/ 
			String GiftCardTextBoxLFontName=BNBasicCommonMethods.isFontName(GiftCardLabelFontName);
			String ExpectedGiftcardtextboxstrokecolor = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 3);
			String ExpectedGiftCardTextBoxHeight = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 5);
			String ExpectedGiftcardTextBoxWidth = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 6	);
		
			String ExpectedGiftCardTextBoxLFontName = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 7);
			String ExpectedGiftCardLabelFontSize = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 9);
			String ExpectedGiftCardLabeName = BNBasicCommonMethods.getExcelNumericVal("BNIA71", sheet, 8);
			
			
		
		   // System.out.println();	
			
				 if(Giftcardtextboxstrokecolor.equals(ExpectedGiftcardtextboxstrokecolor))
					pass("Gift Card TextField stroke color Current : "+Giftcardtextboxstrokecolor+" Expected : "+ExpectedGiftcardtextboxstrokecolor);
				else
					fail("Gift Card TextField stroke color Current : "+Giftcardtextboxstrokecolor+" Expected : "+ExpectedGiftcardtextboxstrokecolor);
					
					
			     if(GiftcardTextBoxWidth.equals(ExpectedGiftcardTextBoxWidth) && GiftCardTextBoxHeight.equals(ExpectedGiftCardTextBoxHeight))
					pass("Gift Card TextField Current Width & Height : "+GiftcardTextBoxWidth+"&"+GiftCardTextBoxHeight+" Expected : "+ExpectedGiftcardTextBoxWidth+"&"+ExpectedGiftCardTextBoxHeight);
				else
					fail("Gift Card TextField Current Width & Height : "+GiftcardTextBoxWidth+"&"+GiftCardTextBoxHeight+" Expected : "+ExpectedGiftcardTextBoxWidth+"&"+ExpectedGiftCardTextBoxHeight);
		
				if(GiftCardLabelName.equals(ExpectedGiftCardLabeName))
					pass("Gift Card TextField Label Name Current : "+GiftCardLabelName+" Expected : "+ExpectedGiftCardLabeName);
				else
					fail("Gift Card TextField Label Name Current : "+GiftCardLabelName+" Expected : "+ExpectedGiftCardLabeName);
						
				if(GiftCardTextBoxLFontName.equals(ExpectedGiftCardTextBoxLFontName))
					pass("Gift Card TextField Label Font Current : "+GiftCardTextBoxLFontName+" Expected : "+ExpectedGiftCardTextBoxLFontName);
				else
					fail("Gift Card TextField Label Font Current : "+GiftCardTextBoxLFontName+" Expected : "+ExpectedGiftCardTextBoxLFontName);
							
				if(GiftCardLabelFontSize.equals(ExpectedGiftCardLabelFontSize))
					pass("Gift Card TextField Label Font Size Current : "+GiftCardLabelFontSize+" Expected : "+ExpectedGiftCardLabelFontSize);
				else
					fail("Gift Card TextField Label Font Size Current : "+GiftCardLabelFontSize+" Expected : "+ExpectedGiftCardLabelFontSize);
						
				//System.out.println();
						
				
	     //to verify check balance button 
		WebElement ChField = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
		WebElement GiftCardCheckBalance = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
		String Chbtn = GiftCardCheckBalance.getCssValue("color");
		Color Chcolor = Color.fromString(Chbtn);
		String ChBtnColor = Chcolor.asHex();
		String CheckbuttonWidth=ChField.getCssValue("width");
		String CheckButtonHeight=ChField.getCssValue("height");
		String expebtnwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 6);
		String expebtnheight = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 5);
		String expecbtncolor = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 3);
		String expecbtnname = BNBasicCommonMethods.getExcelNumericVal("BNIA72", sheet, 7);
		
		   if(ChBtnColor.equals(expecbtncolor))
		   {
			   log.add("Expected"+expecbtncolor);
			   log.add("Current"+ChBtnColor);
			   pass("CheckButton Color is Matched ",log);
		   }
		   
		   else
		   {
			   log.add("Expected"+expecbtncolor);
			   log.add("Current"+ChBtnColor);
			   fail("CheckButton Color is Not Matched ",log);
			   
		   }
		   
		   if(CheckbuttonWidth.equals(expebtnwidth) && CheckButtonHeight.equals(expebtnheight) )
		   {
			   log.add("Expected width & Height"+expebtnwidth+expebtnheight);
			   log.add("Current width & Height"+CheckbuttonWidth+expebtnheight);
			   pass("CheckButton Width and height is Matched ",log);
		   }
		   else
		   {
			   log.add("Expected width & Height "+expebtnwidth+expebtnheight);
			   log.add("Current width & Height "+CheckbuttonWidth+expebtnheight);
			   fail("CheckButton Width and Height is not Matched ",log);
		   }
		   
		   if(ChField.getText().equals(expecbtnname))
		   {
			   log.add("Expected Text "+expecbtnname);
			   log.add("Current text "+ChField.getText());
			   pass("CheckButton text is Matched ",log);
		   }
		   
		   else
		   {
			   log.add("Expected"+expecbtnname);
			   log.add("Current"+ChField.getText());
			   fail("CheckButton text is not Matched ",log);
		   }
		   
  		}
  		
  		catch(Exception e)
  		{
  			exception("There is something wrong, so please check");
  		}
  	}
  
  	/*BNIA-73 Verify that B&N Header should be displayed in the Gift Card lookup page as per the creative*/
  	public void BNIA73()
  	{
  		ChildCreation("BNIA-73 Verify that B&N Header should be displayed in the Gift Card lookup page as per the creative.");
  		// to verifying B&N header is Displayed
  		boolean eleVisible = false;
  		try
  		{
  			Thread.sleep(3000);
  			eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "BNHeader");
  			if(eleVisible)
  			{
  				pass("Header is Displayed in the GiftCard Page");
  			}
  			
  			else
  			{
  				fail("Header is Not Displayed in the GiftCard Page");
  			}
  		}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  	}
  		
  	/*BNIA-74 Verify that while selecting the icons in the B&N Header from the Gift Card lookup page,it should navigate to the respective page*/
  	public void BNIA74() throws Exception
  	{
  		Thread.sleep(3000);
  		//Verify that while selecting the icons in the B&N Header from the Gift Card lookup page,it should navigate to the respective page
  		ChildCreation("BNIA-74 Verify that while selecting the icons in the B&N Header from the Gift Card lookup page,it should navigate to the respective page.");
  		BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
  		WebElement Logo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
  		Actions act = new Actions(driver);
  		act.moveToElement(Logo).click().build().perform();
  		boolean eleVisible = false;
  		try
  		{
  			BNBasicCommonMethods.WaitForLoading(wait);
  			eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
  			if(eleVisible)
  			{
  				pass("On Tapping Header BNlogo page Navigates to home page");
  			}
  			else
  			{
  				fail("On Tapping Header BNlogo page is not Navigates to home page");
  			}
  		}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  		
  		finally
  		{   
  			
  			BNIAGIFTNAVIGATE();
  		}
  	}
  		
  	/*BNIA-76 Verify that Gift Card text box should accepts numbers only*/
  	public void BNIA76() throws Exception
  	{
  		ChildCreation("BNIA-76 Verify that Gift Card text box should accepts numbers only");
  		sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
  		String str = BNBasicCommonMethods.getExcelVal("BNIA76", sheet, 2);
  		//String str = "asrtghtvcerbgthngds 1234567890098765432 !@#$%^&*((*&^%$#@!@ FGHLJKdffjgv!@$#3453623 ";
  		String[]  val = str.split("\n");
  		try
  		{
  			BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardInputTxt");
  			
	  		WebElement GiftBox = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt");
	  		for(int ctr=0 ; ctr < val.length ; ctr++)
	  		{
	  			GiftBox.clear();
	  			GiftBox.click();
	  			
	  			GiftBox.sendKeys(val[ctr]);
	  			String currentvalue = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt").getAttribute("value");
                
	  			if(currentvalue.isEmpty() || /*currentvalue.equals(val[ctr])*/ currentvalue.matches("[0-9]+"))
	  			{
	  				log.add("Given Value "+val[ctr]+" Current Value "+currentvalue);
	  				pass("GiftCard TextBox is Accepts Numbers Only ",log);
	  			}
	  			
	  			else
	  			{
	  				log.add("Given Value "+val[ctr]+"Current Value "+currentvalue);
	  				pass("GiftCard TextBox Accepts All Data Types",log);
	  			}
	              			
	  		}
	  		
  		}
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}

  	}
  	
  	/*BNIA-173 Verify that while selecting the "Check Balance" button after entering the invalid gift card value,the alert message should be displayed*/
  	public void BNIA173() throws Exception
  	{
  		//Verify that Gift Card text box should accepts numbers only
  		ChildCreation("BNIA-173 Verify that while selecting the Check Balance button after entering the invalid gift card value,the alert message should be displayed.");
  		try
  		{
  		    sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
  			String GiftAlert = BNBasicCommonMethods.getExcelVal("BNIA76", sheet, 4);
	  		WebElement GiftCardCheckBalance = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
	  		Thread.sleep(1000);
	  		GiftCardCheckBalance.click();
	  		Thread.sleep(1000);
	  		WebElement InvalidGiftTxt = BNBasicCommonMethods.findElement(driver, obj, "InvalidGiftTxt");
	  	    if(InvalidGiftTxt.getText().equals(GiftAlert))
	  	    {
	  	    	log.add("Displayed gift card alert is "+InvalidGiftTxt.getText());
	  	    	log.add("Expected Error Alert message is "+GiftAlert);
	  	    	pass("Gift Card Error Alert Message is Displayed successfully",log);
	  	    }
	  		
	  	    else
	  	    {
	  	    	log.add("Displayed gift card alert is "+InvalidGiftTxt.getText());
	  	    	log.add("Expected Error Alert message is "+GiftAlert);
	  	    	pass("Gift Card Error Alert Message is not Displayed successfully",log);
	  	    	
	  	    }
	  	}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  	}
  		
  	/*BNIA-78 Verify that while selecting the "Check Balance" button after entering the valid gift card value,the "Gift Card Balance:$xxx" message should be displayed*/
  	public void BNIA78()
  	{
  		ChildCreation("BNIA-78 Verify that while selecting the Check Balance button after entering the valid gift card value,the Gift Card Balance:$xxx message should be displayed");
  		try
  		{
  			sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
	    	String ValidGiftcard = BNBasicCommonMethods.getExcelNumericVal("BNIA78", sheet, 2);
	    	String[] VGcard = ValidGiftcard.split("\n");
	    	Thread.sleep(1500);
	    	BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardInputTxt");
	    	for(int ctr=0 ; ctr< VGcard.length ; ctr++)
	    	{
	    	WebElement GiftCardInputTxt = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt");
	    	WebElement GiftCardCheckBalance = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
	    	GiftCardInputTxt.click();
	    	GiftCardInputTxt.clear();
  	    	if(GiftCardInputTxt.isEnabled())
  	    	{
  	    		GiftCardInputTxt.sendKeys(VGcard[ctr]);
  	    		Thread.sleep(500);
  	    		GiftCardCheckBalance.click();
  	    		Thread.sleep(1500);
  	    		log.add("Gift Card Balance is Displayed");
  	    		Thread.sleep(1500);
  	    		BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardValidBal");
  	    		WebElement GiftCardValidBal = BNBasicCommonMethods.findElement(driver, obj, "GiftCardValidBal");
  	    		//System.out.println(GiftCardValidBal.getText());
  	    		if(GiftCardValidBal.isDisplayed())
  	    		{
  	    			log.add("Gift card displayed text message is "+GiftCardValidBal.getText());
  	    			pass("Balance for Valid Giftcard Successfully Displayed ",log);
  	    		}
  	    		else
  	    		{
  	    			fail("Balance for Valid Giftcard is not Displayed ",log);
  	    		}
  	    	}
  	    	else
  	    	{
  	    		fail("Gift Card input text is not enable ");
  	    	}
  			
  		   }
  		} 	
  		
  	   catch(Exception e)
  	  {
  		 exception(e.getMessage());
  	  }
  	}
  	
  	/*BNIA-82 Verify that Gift Card balance value should be higlighted in Green color below the Gift card text box*/
  	public void BNIA82()
  	{
  		ChildCreation("BNIA-82 Verify that Gift Card balance value should be higlighted in Green color below the Gift card text box.");
  		try
  		{
  			sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
	    	String GetColorvalid = BNBasicCommonMethods.getExcelVal("BNIA78", sheet, 3);
  			WebElement GiftBalTxt = BNBasicCommonMethods.findElement(driver, obj, "GiftBalTxt");
  	    	String getColor = GiftBalTxt.getCssValue("color");
  	    	Color getBlaColor = Color.fromString(getColor);
  	    	String Currentcolor = getBlaColor.asHex();
  	    	if(Currentcolor.equals(GetColorvalid))
  	    	{
  	    		log.add("Current Color "+Currentcolor+"Expected Color "+GetColorvalid);
  	    		pass("GiftCard Balance font is Matched",log);
  	    	}
  	    	else
  	    	{
  	    		//System.out.println();
  	    		log.add("Current Color "+Currentcolor+"Expected Color "+GetColorvalid);
  	    		fail("GiftCard Balance font is not Matched",log);
  	    	}
  		}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  	}
  	
  	/*BNIA-83 Verify that Gift card balance should be higlighted in green color with Prefix $ included*/
  	public void BNIA83()
  	{
  		ChildCreation("BNIA-83 Verify that Gift card balance should be higlighted in green color with Prefix $ included.");
  		try
  		{
  			WebElement GiftBalPrefix = BNBasicCommonMethods.findElement(driver, obj, "GiftBalPrefix");
  	    	String Str = GiftBalPrefix.getText();
  	    	if(Str.startsWith("$"))
  	    	{
  	    		log.add("Giftcard Balance is Starts with "+Str);
  	    		pass("Gift card balance should is displayed with Prefix $",log);
  	    	}
  	    	
  	    	else
  	    	{
  	    		log.add("Giftcard Balance is Starts with "+Str);
  	    		fail("Gift card balance should is not displayed with Prefix $",log);
  	    	}
  		}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  	}
  	
  	/*BNIA-80 Verify that while selecting the "Check Balance" button after entering the valid Gift card details,the gift card balance values should be retrieved from the backend call*/
  	public void BNIA80()
  	{
  		ChildCreation("BNIA-80 Verify that while selecting the Check Balance button after entering the valid Gift card details,the gift card balance values should be retrieved from the backend call");
  	    try
  	    {
  	    	sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
  	    	String ValidGiftcard = BNBasicCommonMethods.getExcelNumericVal("BNIA78", sheet, 2);
  	    	String[] VGcard = ValidGiftcard.split("\n");
  	    	String GetColorvalid = BNBasicCommonMethods.getExcelVal("BNIA78", sheet, 3);
  	    	Thread.sleep(2000);
  	    	BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardInputTxt");
  	    	for(int ctr=0 ; ctr< VGcard.length ; ctr++)
  	    	{
  	    	WebElement GiftCardInputTxt = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt");
  	    	WebElement GiftCardCheckBalance = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
  	    	GiftCardInputTxt.click();
  	    	GiftCardInputTxt.clear();
	  	    	if(GiftCardInputTxt.isEnabled())
	  	    	{
	  	    		GiftCardInputTxt.sendKeys(VGcard[ctr]);
	  	    		Thread.sleep(500);
	  	    		GiftCardCheckBalance.click();
	  	    		Thread.sleep(1500);
	  	    		log.add("Gift Card Balance is Displayed");
	  	    		Thread.sleep(2000);
	  	    		BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardValidBal");
	  	    		WebElement GiftCardValidBal = BNBasicCommonMethods.findElement(driver, obj, "GiftCardValidBal");
	  	    	//	System.out.println(GiftCardValidBal.getText());
	  	    		if(GiftCardValidBal.isDisplayed())
	  	    		{
	  	    			log.add("Gift card displayed text message is "+GiftCardValidBal.getText());
	  	    			pass("Balance for Valid Giftcard Successfully Displayed ",log);
	  	    		}
	  	    		else
	  	    		{
	  	    			fail("Balance for Valid Giftcard is not Displayed ",log);
	  	    		}
	  	    	}
	  	    	else
	  	    	{
	  	    		fail("Gift Card input text is not enable ");
	  	    	}
  	    	}
  	    	
  	    }
  	    
  	    catch(Exception e)
  	    {
  	    	exception("There is something wrong,so please verify"+e.getMessage());
  	    }
  	}

  	/*BNIA-79 Verify that Gift Card text box should accept maximum of 19 digits value*/
  	public void BNIA79() throws Exception
  	{
  		sheet = BNBasicCommonMethods.excelsetUp("Gift Card");
  		ChildCreation("BNIA-79 Verify that Gift Card text box should accept maximum of 19 digits value.");
  		String str = BNBasicCommonMethods.getExcelNumericVal("BNIA79", sheet, 2);
  		//String str = "123456789098765432 1234567890987654321 12345678900987654321";
  		String[]  val = str.split("\n");
  		try
  		{
  			BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardInputTxt");
  			BNBasicCommonMethods.WaitForLoading(wait);
	  		WebElement GiftBox = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt");
	  		for(int ctr=0 ; ctr < val.length ; ctr++)
	  		{
	  			GiftBox.clear();
	  			GiftBox.click();
	  			GiftBox.sendKeys(val[ctr]);
	  			String currentvalue = BNBasicCommonMethods.findElement(driver, obj, "GiftCardInputTxt").getAttribute("value");
                
	  			if(currentvalue.length()<=val[ctr].length())
	  			{
	  				log.add("Given Value Length "+val[ctr].length()+" Current Value Length "+currentvalue.length());
	  				pass("GiftCard TextBox Accepts Maximum of 19 Digits",log);
	  			}
	  			
	  			else
	  			{
	  				log.add("Given Value Length "+val[ctr].length()+" Current Value Length "+currentvalue.length());
	  				pass("GiftCard TextBox is Exceeds Maximum of digits 19",log);
	  			}
	              			
	  		}
	  	}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  	}
  	  	
/******************************************************* AST Home Screen With Tiles ***********************************************************************/ 
 
 @Test(priority=3)
  	public void BNIA06_Home_Screen() throws Exception
  	{
  		
  		TempSignIn();
  		//for both 
  		BNIA142();
  		
  		/*for AST*/
  		BNIA143_AST();
  		BNIA146_AST();
  		BNIA144_AST();
  		
  		/*for KIOSK*/
  	/*	BNIA143_KIOSK();
  		BNIA146_KIOSK();
  		BNIA144_KIOSK();*/
  		
  		
  		/*for both AST and KIOSK*/
  		BNIA147();
  		BNIA148();
  		BNIA149();
  		BNIA150();
  		//TemSignOut();
  		TempSessionTimeout();
  	}
  	
  	/*BNIA-142 Verify that Barnes & Noble logo should be displayed at the header as per the creative in the home page*/
  	public void BNIA142()
  	{
  		ChildCreation("BNIA-142 Verify that Barnes & Noble logo should be displayed at the header as per the creative in the home page");
		// to verifying B&N header is Displayed
		boolean eleVisible = false;
		try
		{
			Thread.sleep(500);
			eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
			if(eleVisible)
			{
				pass("Barnes and Noble Logo is Displayed");
			}
			
			else
			{
				fail("Barnes and Noble Logo is not Displayed");
			}
		}
		
		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  
  	/*BNIA-143 Verify that Barnes & Noble logo should be in static at the header in the home page*/
  	public void BNIA143_AST()
  	{
  		ChildCreation("BNIA-142 Verify that Barnes & Noble logo should be displayed at the header as per the creative in the home page."
  				    + "BNIA-143 Verify that Barnes & Noble logo should be in static at the header in the home page");
  		// to verifying B&N header is Displayed
  		boolean eleVisible = false;
  		try
  		{
  			Thread.sleep(500);
  			eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
  			if(eleVisible)
  			{
  				pass("Barnes and Noble Logo is Displayed");
  			}
  			
  			else
  			{
  				fail("Barnes and Noble Logo is not Displayed");
  			}
  		}
  		
  		catch(Exception e)
  		{
  			exception("There is somwthing went wrong , please verify"+e.getMessage());
  		}
  	 }
  	
  	/*BNIA-143 Verify that Barnes & Noble logo should be in static at the header in the home page*/
  	public void BNIA143_KIOSK()
  	{
  		ChildCreation("BNIA-142 Verify that Barnes & Noble logo should be displayed at the header as per the creative in the home page."
  				    + "BNIA-143 Verify that Barnes & Noble logo should be in static at the header in the home page");
  		// to verifying B&N header is Displayed
  		boolean eleVisible = false;
  		try
  		{
  			Thread.sleep(500);
  			eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
  			if(eleVisible)
  			{
  				pass("Barnes and Noble Logo is Displayed");
  			}
  			
  			else
  			{
  				fail("Barnes and Noble Logo is not Displayed");
  			}
  		}
  		
  		catch(Exception e)
  		{
  			exception("There is somwthing went wrong , please verify"+e.getMessage());
  		}
  	 }

  	/*BNIA-146 Verify that home page tiles should be displayed as per the creative*/
  	public void BNIA146_AST()
  	{
  		ChildCreation("BNIA-146 Verify that home page tiles should be displayed as per the creative.");
		try
		{
			Thread.sleep(500);
			WebElement BrowseTile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
			if(BNBasicCommonMethods.isElementPresent(BrowseTile))
			{
				pass("Browse Tile is Displayed in the Home Page");
			}
			
			else
			{
				fail("Browse Tile is not Displayed in the Home Page");
			}
			
			WebElement SearchTile = BNBasicCommonMethods.findElement(driver, obj, "SearchTile");
			if(BNBasicCommonMethods.isElementPresent(SearchTile))
			{
				pass("Search Tile is Displayed in the Home Page");
			}
			
			else
			{
				fail("Search Tile is not Displayed in the Home Page");
			}

			WebElement StoreLocator = BNBasicCommonMethods.findElement(driver, obj, "StoreLocator");
			if(BNBasicCommonMethods.isElementPresent(StoreLocator))
			{
				pass("Store Locator Tile is Displayed in the Home Page");
			}
			
			else
			{
				fail("Store Locator Tile is not Displayed in the Home Page");
			}

			WebElement StoreMap = BNBasicCommonMethods.findElement(driver, obj, "StoreMap");
			if(BNBasicCommonMethods.isElementPresent(StoreMap))
			{
				pass("Store Map Tile is Displayed in the Home Page");
			}
			
			else
			{
				fail("Sore Map Tile is not Displayed in the Home Page");
			}

			WebElement InstaplyTile = BNBasicCommonMethods.findElement(driver, obj, "InstaplyTile");
			if(BNBasicCommonMethods.isElementPresent(InstaplyTile))
			{
				pass("Instaply Tile  is Displayed in the Home Page");
			}
			
			else
			{
				fail("Instaply Tile is not Displayed in the Home Page");
			}

			WebElement CheckoutTile = BNBasicCommonMethods.findElement(driver, obj, "CheckoutTile");
			if(BNBasicCommonMethods.isElementPresent(CheckoutTile))
			{
				pass("Checkout Tile  is Displayed in the Home Page");
			}
			
			else
			{
				fail("Checkout Tile is not Displayed in the Home Page");
			}
		}
		
		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}

	/*BNIA-146 Verify that home page tiles should be displayed as per the creative*/
  	public void BNIA146_KIOSK()
  	{
  		ChildCreation("BNIA-146 Verify that home page tiles should be displayed as per the creative.");
		try
		{
			Thread.sleep(500);
			WebElement BrowseTile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
			if(BNBasicCommonMethods.isElementPresent(BrowseTile))
			{
				pass("Browse Tile is Displayed in the Home Page");
			}
			
			else
			{
				fail("Browse Tile is not Displayed in the Home Page");
			}
			
			WebElement SearchTile = BNBasicCommonMethods.findElement(driver, obj, "SearchTile");
			if(BNBasicCommonMethods.isElementPresent(SearchTile))
			{
				pass("Search"
						+ " Tile is Displayed in the Home Page");
			}
			
			else
			{
				fail("Search Tile is not Displayed in the Home Page");
			}

			WebElement StoreLocator = BNBasicCommonMethods.findElement(driver, obj, "StoreLocator");
			if(BNBasicCommonMethods.isElementPresent(StoreLocator))
			{
				pass("Store Locator Tile is Displayed in the Home Page");
			}
			
			else
			{
				fail("Store Locator Tile is not Displayed in the Home Page");
			}

			WebElement StoreMap = BNBasicCommonMethods.findElement(driver, obj, "StoreMap");
			if(BNBasicCommonMethods.isElementPresent(StoreMap))
			{
				pass("Store Map Tile is Displayed in the Home Page");
			}
			
			else
			{
				fail("Sore Map Tile is not Displayed in the Home Page");
			}

			WebElement GiftCardTile = BNBasicCommonMethods.findElement(driver, obj, "GiftCardTile");
			if(BNBasicCommonMethods.isElementPresent(GiftCardTile))
			{
				pass("GiftCard Tile  is Displayed in the Home Page");
			}
			
			else
			{
				fail("GiftCard Tile is not Displayed in the Home Page");
			}

		}
		
		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}

  	/*BNIA-145 Verify that home page should be displayed as per the creative & fit to the screen in AST device*/
  	public void BNIA144_AST()
  	{
  		ChildCreation("BNIA-145 Verify that home page should be displayed as per the creative & fit to the screen in AST device");
  		boolean eleVisible = false;
  		try
  		{
  			//Thread.sleep(1500);
  			eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
  			if(eleVisible)
  			{
  				pass("HomePage is Displayed");
  			}
  			
  			else
  			{
  				fail("HomePage is not Displayed");
  			}
  		}
  		
  		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  	
  	/*BNIA-144 Verify that home page should be displayed as per the creative & fit to the screen in Kiosk device*/
  	public void BNIA144_KIOSK()
  	{
  		ChildCreation("BNIA-144 Verify that home page should be displayed as per the creative & fit to the screen in Kiosk device");
  		boolean eleVisible = false;
  		try
  		{
  			Thread.sleep(500);
  			eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
  			if(eleVisible)
  			{
  				pass("HomePage is Displayed");
  			}
  			
  			else
  			{
  				fail("HomePage is not Displayed");
  			}
  		}
  		
  		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  	
  	/*BNIA-147 Verify that while selecting the "Browse Catalog" authored icon in the home page,it should navigate to the browse catalog page*/
  	public void BNIA147() throws Exception
  	{
  		try
  		{
  			ChildCreation("BNIA-147 Verify that while selecting the Browse Catalog authored icon in the home page,it should navigate to the browse catalog page");
	  		WebElement BrowseTile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
	  		BrowseTile.click();
	  		Thread.sleep(500);
	  		BNBasicCommonMethods.waitforElement(wait, obj, "Browsepage");
	  		WebElement Browsepage = BNBasicCommonMethods.findElement(driver, obj, "Browsepage");
	  		if(BNBasicCommonMethods.isElementPresent(Browsepage))
	  		{
	  			log.add("Navigated to Browse Page");
	  			pass("Browse All Categories menu page is displayed successfully",log);
	  		}
	  		else
	  		{
	  			log.add("Page is Not navigate to Browse page");
	  			fail("Browse All Categories menu page is not displayed successfully",log);
	  		}
  		
  		}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  		
  		WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
  		BNLogo.click();
  		Thread.sleep(1500);
  	}
  	
  	/*BNIA-148 Verify that while selecting the "Search" icon at the home page,the search field should be enabled*/
  	public void BNIA148() throws InterruptedException
  	{
  		try
  		{
  			ChildCreation("BNIA-148 Verify that while selecting the Search icon at the home page,the search field should be enabled");
	  		BNBasicCommonMethods.waitforElement(wait, obj, "SearchTile");
	  		WebElement SearchTile = BNBasicCommonMethods.findElement(driver, obj, "SearchTile");
	  		SearchTile.click();
	  		Thread.sleep(500);
	  		BNBasicCommonMethods.waitforElement(wait, obj, "SearchPage");
	  		WebElement SearchPage = BNBasicCommonMethods.findElement(driver, obj, "SearchPage");
	  		if(BNBasicCommonMethods.isElementPresent(SearchPage))
	  		{
	  			log.add("Navigated to Search Page");
	  			pass("Search page is displayed successfully",log);
	  		}
	  		else
	  		{
	  			log.add("Page is Not navigate to Search page");
	  			fail("Search Page is not displayed successfully",log);
	  		}
  		}
  		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  	
  	/*BNIA-149 Verify that user should be able to enter the search keywords in the search field text box*/
  	public void BNIA149() throws Exception
  	{
  		ChildCreation("BNIA-149 Verify that user should be able to enter the search keywords in the search field text box");
  		try
  		{
	  		sheet = BNBasicCommonMethods.excelsetUp("Home Page");
	  		String SreachKwd = BNBasicCommonMethods.getExcelVal("BNIA149", sheet, 2);
	  		BNBasicCommonMethods.waitforElement(wait, obj, "SearchTxtBxnew");
	  		WebElement SearchTxtBx = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");
	  		if(SearchTxtBx.isDisplayed())
	  		{
	  			Thread.sleep(500);
	  			pass("Search Box is found");
	  			Thread.sleep(500);
	  			SearchTxtBx.click();
	  			Thread.sleep(500);
	  			//SearchTxtBx.clear();
	  			Actions action = new Actions(driver);
	  			action.moveToElement(SearchTxtBx).sendKeys(SreachKwd).build().perform();
	  			/*Actions act = new Actions(driver);
	  	        act.sendKeys(Keys.ENTER).build().perform();*/
	  			//SearchTxtBx.sendKeys(SreachKwd);
	  			Thread.sleep(1000);
	  			BNBasicCommonMethods.waitforElement(wait, obj, "searchSuggestioncontainer");
	  			 WebElement searchSuggestioncontainer = BNBasicCommonMethods.findElement(driver, obj, "searchSuggestioncontainer");
	    		if(searchSuggestioncontainer.isDisplayed())
	    		{
	  				log.add("Entered Search Keyword "+SreachKwd);
	  				pass("User able to enter searchKeyword in search Textbox");
	  			}
	  			else
	  			{
	  				log.add("Entered Search Keyword"+SreachKwd);
	  				pass("User not able to enter searchKeyword in search Textbox");
	  			}
	  			
	  		}
	  		else
	  		{
	  			fail("there is no Search Box is found");
	  		}	
  		}
  		catch(Exception e)
		{
  			System.out.println(e.getMessage());
			exception(" There is somwthing went wrong , please verify"+e.getMessage());
		}
  		
  		
  	}

  	/*BNIA-150 Verify that while entering the search keywords in the search field,the search suggestions should be displayed*/
  	public void BNIA150()
  	{
  		ChildCreation("BNIA-150 Verify that while entering the search keywords in the search field,the search suggestions should be displayed");
  		try
  		{
  			Thread.sleep(500);
  			BNBasicCommonMethods.waitforElement(wait, obj, "searchSuggestioncontainer");
  			WebElement searchSuggestioncontainer = BNBasicCommonMethods.findElement(driver, obj, "searchSuggestioncontainer");
  			Thread.sleep(2000);
  			if(searchSuggestioncontainer.isDisplayed())
  			{
  				pass("Search suggestions are displayed"); 
  				Thread.sleep(1000);
  				List <WebElement> SearchSugglist = driver.findElements(By.xpath("//*[@class='skMobsuggestedItem_container']//*[@class='suggestedItem ']"));
  				Thread.sleep(1000);
  				log.add("Search Suggestion count "+SearchSugglist.size());
  				for(int ctr=1 ; ctr < SearchSugglist.size()+1 ; ctr++)
  				{
  					String Suggesting = driver.findElement(By.xpath("(//*[@class='skMobsuggestedItem_container']//*[@class='suggestedItem '])["+ctr+"]")).getText();
  					Thread.sleep(500);
  					log.add("Suggestion"+(ctr) +" : "+Suggesting);
  					// System.out.println(Suggesting);
  				}
  				pass("Search Suuggestion are displayed and are listed below",log);
  		   }
  		   else
  		   {
  			  fail("Search suggestions are not displayed");
  		   }
  		}
  		
  		catch(Exception e)
		{
  	       System.out.println(e.getMessage());
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  		System.out.println("3/11 - BNIA06_Home_Screen is completed");
  	}
 
  	
 /************************************************************Display Top Menu navigation with icons *********************************************************/
  	
  @Test(priority=4)	
  	public void BNIA07_Header_navigation() throws Exception
  	{
  		TempSignIn();
  		 BNIA122();
  		 BNIA130();
  		 BNIA120();
  		 BNIA121();
  		 BNIA131();
  		 BNIA132();
  		 BNIA127();
  		 BNIA133();
  		 BNIA134();
  		 BNIA136();
  		 BNIA137();
  		 BNIA139();
  		 BNIA140();
  		 BNIA141();
  		 BNIA238();
  		 BNIA123();
  		 BNIA125();
  		 BNIA126();
  		 BNIA138(); 
  		 //TemSignOut();
  		TempSessionTimeout();
  		 
  	}
  	
  	/*BNIA-120 Verify that header should be displayed as per the creative*/
    public void BNIA120()
    {
    	ChildCreation("BNIA-120 Verify that header should be displayed as per the creative");
    	try
  		{
  			sheet = BNBasicCommonMethods.excelsetUp("TopMenu navigation");
 	        BNBasicCommonMethods.waitforElement(wait, obj, "BackArrow");
	 		
 	    // To find Back button Color
 	    
 	    	WebElement Backbtn = BNBasicCommonMethods.findElement(driver, obj, "BackArrow");
	 		String backarrowcolor = Backbtn.getCssValue("lighting-color");
	 		String Backarrowiconwidth = Backbtn.getCssValue("width");
	 		String BackarrowiconHeight = Backbtn.getCssValue("height");
	 		Color backarw = Color.fromString(backarrowcolor);
	 		String HexSignMaskColor = backarw.asHex();
	 		
	 		
	 		
	        // TO find Hamburger menu color 
	 		WebElement hamburger = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
	 		String hcolor=hamburger.getCssValue("lighting-color");
	 		String hamburgerwidth = hamburger.getCssValue("width");
	 		String hamburgerHeight = hamburger.getCssValue("height");
	 		
	 		Color hamcolor = Color.fromString(hcolor);
	 		String hexhambuColor = hamcolor.asHex();
	 		
	 	     // To find Header logo color 
	 		
	 		WebElement BNHeader = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
	 		String BNHeadercolor=BNHeader.getCssValue("lighting-color");
	 		String BNHeaderwidth = BNHeader.getCssValue("width");
	 		String BNHeaderHeight = BNHeader.getCssValue("height");
	 		Color BNcolor = Color.fromString(BNHeadercolor);
	 		String BNHeaderlogoColor = BNcolor.asHex();
	 		
	 		
	 		 
	 		//To find Scan icon color
	 		
	 		WebElement scanicon = BNBasicCommonMethods.findElement(driver, obj, "ScanIcon");
	 		String scancolor = scanicon.getCssValue("lighting-color");
	 		String scaniconwidth = scanicon.getCssValue("width");
	 		String scaniconHeight = scanicon.getCssValue("height");
	 		Color scanicolor = Color.fromString(scancolor);
	 		String hexscanColor = scanicolor.asHex();
	 		
	 		
	 	
	 		//To find Search icon color
	 		
	 		WebElement Searchicon = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
	 		String Searchcolor = Searchicon.getCssValue("lighting-color");
	 		String Searchiconwidth = Searchicon.getCssValue("width");
	 		String SearchiconHeight = Searchicon.getCssValue("height");
	 		Color Searchiconcolor = Color.fromString(Searchcolor);
	 		String hexSearchColor = Searchiconcolor.asHex();
	 		
	 		
	 		
	 		// To Find Shopping Bag icon color
	 		
	        WebElement shoppingbag = BNBasicCommonMethods.findElement(driver, obj,"ShoppingBagIcon");
	        String shoppingicon=shoppingbag.getCssValue("lighting-color");
	 		Color shop = Color.fromString(shoppingicon);
	 		String shopColor = shop.asHex();
	        String shoppingbagwidth = shoppingbag.getCssValue("width");
	 		String shoppingbagHeight = shoppingbag.getCssValue("height");
	 		
	 		
	 		//To Find Shopping bag count 
	        WebElement ShoppingCount =  BNBasicCommonMethods.findElement(driver, obj, "ShoppingBagCount");
	        String shoppingiconcount=shoppingbag.getCssValue("lighting-color");
	 		Color shopcount = Color.fromString(shoppingiconcount);
	 		String shopColorcount = shopcount.asHex();
	        String ShoppingCountwidth = ShoppingCount.getCssValue("width");
	 		String ShoppingCountHeight = ShoppingCount.getCssValue("height");
	 		String Shoppingcoutfont = ShoppingCount.getCssValue("font-family");
	 		String ShoppingcoutSize = ShoppingCount.getCssValue("font-size");

	 		String ExpectedBackArrowheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120A", sheet, 4);
			String ExpectedBackArrowwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120A", sheet, 5);
			String ExpectedBackArrowcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120A", sheet, 6);
	 		
			String ExpectedHamburgerheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120B", sheet, 4);
			String ExpectedHamburgerwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120B", sheet, 5);
			String ExpectedHamburgercolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120B", sheet, 6);

			String ExpectedBNHeaderheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120C", sheet, 4);
			String ExpectedBNHeaderwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120C", sheet, 5);
			String ExpectedBNHeadercolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120C", sheet, 6);
			
			String ExpectedSearchheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120D", sheet, 4);
			String ExpectedSearchwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120D", sheet, 5);
			String ExpectedSearchcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120D", sheet, 6);
	 	
		 	
			String ExpectedScanheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120E", sheet, 4);
			String ExpectedScanwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120E", sheet, 5);
			String ExpectedScancolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120E", sheet, 6);
				
			String ExpectedShoppingbagheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120F", sheet, 4);
			String ExpectedShoppingbagwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120F", sheet, 5);
			String ExpectedShoppingbagcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120F", sheet, 6);
	 		
			String ExpectedShoppingcountcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120G", sheet, 6);
			String ExpectedShoppingcountheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120G", sheet, 4);
			String ExpectedShoppingcountwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120G", sheet, 5);
			String ExpectedShoppingcountName = BNBasicCommonMethods.getExcelVal("BNIA120G", sheet, 7);
			String ExpectedShoppingcountSize = BNBasicCommonMethods.getExcelNumericVal("BNIA120G", sheet, 8);
			
			//Separating font from font-family
			String ShoppingBagCountFont = BNBasicCommonMethods.isFontName(Shoppingcoutfont);
			
			
			//Back Arrorw Icon color
	        
	        if(HexSignMaskColor.equals(ExpectedBackArrowcolor))
				pass("Back Arrow icon Color  Current : "+HexSignMaskColor+" Expected : "+ExpectedBackArrowcolor);
			else
				fail("Back Arrow icon Color  Current : "+HexSignMaskColor+" Expected : "+ExpectedBackArrowcolor);
			
			
			/*
			if(BackarrowiconHeight.equals(ExpectedBackArrowheight)&&Backarrowiconwidth.equals(ExpectedBackArrowwidth))
				pass("Back Arrow Icon Height and width Current : "+Backarrowiconwidth+" & "+BackarrowiconHeight+" Expected : "+ExpectedBackArrowwidth+" & "+ExpectedBackArrowheight);
			else
				fail("Back Arrow Icon Height and width Current : "+Backarrowiconwidth+" & "+BackarrowiconHeight+" Expected : "+ExpectedBackArrowwidth+" & "+ExpectedBackArrowheight);
*/
			
			//Hamburger menu Icon color
	        
	        if(hexhambuColor.equals(ExpectedHamburgercolor))
				pass("Hamburger Icon Color  Current : "+hexhambuColor+" Expected : "+ExpectedHamburgercolor);
			else
				fail("Hamburger Icon Color  Current : "+hexhambuColor+" Expected : "+ExpectedHamburgercolor);
			
			
			if(hamburgerHeight.equals(ExpectedHamburgerheight)&&hamburgerwidth.equals(ExpectedHamburgerwidth))
				pass("Hamburger Icon Height and width Current : "+hamburgerwidth+" & "+hamburgerHeight+" Expected : "+ExpectedHamburgerwidth+" & "+ExpectedHamburgerheight);
			else
				fail("hamburger Icon Height and width Current : "+hamburgerwidth+" & "+hamburgerHeight+" Expected : "+ExpectedHamburgerwidth+" & "+ExpectedHamburgerheight);

	 		
			//B & N Logo Icon color
	        
			if(BNHeaderlogoColor.equals(ExpectedBNHeadercolor))
				pass("B&N Header icon Color  Current : "+BNHeaderlogoColor+" Expected : "+ExpectedBNHeadercolor);
			else
				fail("B&N Header  icon Color  Current : "+BNHeaderlogoColor+" Expected : "+ExpectedBNHeadercolor);
			
			
			if(BNHeaderHeight.equals(ExpectedBNHeaderheight)&&BNHeaderwidth.equals(ExpectedBNHeaderwidth))
				pass("B&N Header Icon Height and width Current : "+BNHeaderwidth+" & "+BNHeaderHeight+" Expected : "+ExpectedBNHeaderwidth+" & "+ExpectedBNHeaderheight);
			else
				fail("B&N Header Icon Height and width Current : "+BNHeaderwidth+" & "+BNHeaderHeight+" Expected : "+ExpectedBNHeaderwidth+" & "+ExpectedBNHeaderheight);



			//Search Icon color
	        
			if(hexSearchColor.equals(ExpectedSearchcolor))
				pass("Search icon Color  Current : "+hexSearchColor+" Expected : "+ExpectedSearchcolor);
			else
				fail("Search icon Color  Current : "+hexSearchColor+" Expected : "+ExpectedSearchcolor);
			
			
			if(SearchiconHeight.equals(ExpectedSearchheight)&&Searchiconwidth.equals(ExpectedSearchwidth))
				pass("Search Icon Height and width Current : "+Searchiconwidth+" & "+SearchiconHeight+" Expected : "+ExpectedSearchwidth+" & "+ExpectedSearchheight);
			else
				fail("Search Icon Height and width Current : "+Searchiconwidth+" & "+SearchiconHeight+" Expected : "+ExpectedSearchwidth+" & "+ExpectedSearchheight);

			
			//Scan Icon color
	        
			if(hexscanColor.equals(ExpectedScancolor))
				pass("Scan icon Color  Current : "+hexscanColor+" Expected : "+ExpectedScancolor);
			else
				fail("Scan icon Color  Current : "+hexscanColor+" Expected : "+ExpectedScancolor);
			
			if(scaniconHeight.equals(ExpectedScanheight)&&scaniconwidth.equals(ExpectedScanwidth))
				pass("Scan Icon Height and width Current : "+scaniconwidth+" & "+scaniconHeight+" Expected : "+ExpectedScanwidth+" & "+ExpectedScanheight);
			else
				fail("Scan Icon Height and width Current : "+scaniconwidth+" & "+scaniconHeight+" Expected : "+ExpectedScanwidth+" & "+ExpectedScanheight);

			
			//Shopping Bag Icon color
	        
			if(shopColor.equals(ExpectedShoppingbagcolor))
				pass("Shopping Bag icon Color  Current : "+shopColor+" Expected : "+ExpectedShoppingbagcolor);
			else
				fail("Shopping Bag icon Color  Current : "+shopColor+" Expected : "+ExpectedShoppingbagcolor);
			
			
			if(shoppingbagHeight.equals(ExpectedShoppingbagheight)&&shoppingbagwidth.equals(ExpectedShoppingbagwidth))
				pass("Shopping Bag Icon Height and width Current : "+shoppingbagwidth+" & "+shoppingbagHeight+" Expected : "+ExpectedShoppingbagwidth+" & "+ExpectedShoppingbagheight);
			else
				fail("Shopping Bag Icon Height and width Current : "+shoppingbagwidth+" & "+shoppingbagHeight+" Expected : "+ExpectedShoppingbagwidth+" & "+ExpectedShoppingbagheight);

			//Shopping Bag Icon count color
	        
			if(shopColorcount.equals(ExpectedShoppingcountcolor))
				pass("Shopping Bag icon count Color  Current : "+shopColorcount+" Expected : "+ExpectedShoppingcountcolor);
			else
				fail("Shopping Bag icon count Color  Current : "+shopColorcount+" Expected : "+ExpectedShoppingcountcolor);
			
			
			if(ShoppingCountHeight.equals(ExpectedShoppingcountheight)&&ShoppingCountwidth.equals(ExpectedShoppingcountwidth))
				pass("Shopping Bag Icon count Height and width Current : "+ShoppingCountwidth+" & "+ShoppingCountHeight+" Expected : "+ExpectedShoppingcountwidth+" & "+ExpectedShoppingcountheight);
			else
				fail("Shopping Bag Icon count Height and width Current : "+ShoppingCountwidth+" & "+ShoppingCountHeight+" Expected : "+ExpectedShoppingcountwidth+" & "+ExpectedShoppingcountheight);
			
			
			if(ShoppingBagCountFont.equals(ExpectedShoppingcountName))
				pass("Shopping Bag Icon count Font Name Current : "+ShoppingBagCountFont+" Expected : "+ExpectedShoppingcountName);
			else
				fail("Shopping Bag Icon count Font Name Current : "+ShoppingBagCountFont+" Expected : "+ExpectedShoppingcountName);
			
			
			if(ShoppingcoutSize.equals(ExpectedShoppingcountSize))
				pass("Shopping Bag Icon count Font Size Current : "+ShoppingcoutSize+" Expected : "+ExpectedShoppingcountSize);
			else
				fail("Shopping Bag Icon count Font Size Current : "+ShoppingcoutSize+" Expected : "+ExpectedShoppingcountSize);
	    }
	    
	    catch (Exception e)
		{
			System.out.println(e.getMessage());
		
		}
    }
    
  	/*BNIA-121 Verify that header should be displayed in all the pages except "Home" and "Scan" page as per the creative*/
  	public void BNIA121()
  	{
  		ChildCreation("BNIA-121 Verify that header should be displayed in all the pages except Home and Scan page as per the creative");
    	try
    	{
    		//Thread.sleep(2000);
    		Thread.sleep(1500);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(1500);
    		BNBasicCommonMethods.waitforElement(wait, obj, "SearchLink");
    		WebElement SearchLink = BNBasicCommonMethods.findElement(driver, obj, "SearchLink");
    		SearchLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "SearchPage");
    		WebElement BNHeader = BNBasicCommonMethods.findElement(driver, obj, "BNHeader");
    		if(BNHeader.isDisplayed())
    		{
    			pass("Header is displayed in the SearchPage page ");
    		}
    		else
    		{
    			fail("Header is not displayed in the SearchPage page ");
    		}
    		
    		Thread.sleep(1500);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(1500);
    		BNBasicCommonMethods.waitforElement(wait, obj, "StoreLocatorLink");
    		WebElement StoreLocatorLink = BNBasicCommonMethods.findElement(driver, obj, "StoreLocatorLink");
    		StoreLocatorLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "StoreLocatorSearch");
    		BNHeader = BNBasicCommonMethods.findElement(driver, obj, "BNHeader");
    		if(BNHeader.isDisplayed())
    		{
    			pass("Header is displayed in the StoreLocator page");
    		}
    		else
    		{
    			fail("Header is displayed in not displayed in the StoreLocator page");
    		}
    		
    		Thread.sleep(1500);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(1500);
    		BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardLink");
    		WebElement GiftCardLink = BNBasicCommonMethods.findElement(driver, obj, "GiftCardLink");
    		GiftCardLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardCheckBtn");
    		BNHeader = BNBasicCommonMethods.findElement(driver, obj, "BNHeader");
    		if(BNHeader.isDisplayed())
    		{
    			pass("Header is displayed in the GiftCard page.");
    		}
    		else
    		{
    			fail("Header is not displayed in the GiftCard page.");
    		}
    		
    		Thread.sleep(3500);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutLink");
    		WebElement CheckoutLink = BNBasicCommonMethods.findElement(driver, obj, "CheckoutLink");
    		CheckoutLink.click();
    		Thread.sleep(2500);
    		BNBasicCommonMethods.waitforElement(wait, obj, "EmptyContinueBtn");
    		WebElement EmptyContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "EmptyContinueBtn");
    		if(EmptyContinueBtn.isDisplayed())
    		{
    			pass("Header is displayed in the Checkout page.");
    		}
    		else
    		{
    			fail("Header is not displayed in the Checkout page.");
    		}
    		
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "BrowseLink");
    		WebElement BrowseLink = BNBasicCommonMethods.findElement(driver, obj, "BrowseLink");
    		BrowseLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "BNHeader");
    		BNHeader = BNBasicCommonMethods.findElement(driver, obj, "BNHeader");
    		if(BNHeader.isDisplayed())
    		{
    			pass("Header is displayed in the browse page.");
    		}
    		else
    		{
    			fail("Header is not displayed in the browse page.");
    		}
    	}
    	
    	catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  	
  	/*BNIA-131 Verify that header icons alignment & padding in the header should be displayed as per the creative*/
  	public void BNIA131()
  	{
  		ChildCreation("BNIA-131 Verify that header icons alignment & padding in the header should be displayed as per the creative");
  		try
  		{
  			sheet = BNBasicCommonMethods.excelsetUp("TopMenu navigation");
  			WebElement Backbtn = BNBasicCommonMethods.findElement(driver, obj, "BackArrow");
  			String Backarrowiconwidth = Backbtn.getCssValue("width");
	 		String BackarrowiconHeight = Backbtn.getCssValue("height");
	 		
	 		  // TO find Hamburger menu color 
	 		WebElement hamburger = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
	 		String hamburgerwidth = hamburger.getCssValue("width");
	 		String hamburgerHeight = hamburger.getCssValue("height");    
	 		
	 	    // To find Header logo color 
	 		WebElement BNHeader = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
	 		String BNHeaderwidth = BNHeader.getCssValue("width");
	 		String BNHeaderHeight = BNHeader.getCssValue("height");
	 		
	 		//To find Scan icon color
	 		WebElement scanicon = BNBasicCommonMethods.findElement(driver, obj, "ScanIcon");
	 		String scaniconwidth = scanicon.getCssValue("width");
	 		String scaniconHeight = scanicon.getCssValue("height");
	 		
	 		//To find Search icon color
	 		WebElement Searchicon = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
	 		String Searchiconwidth = Searchicon.getCssValue("width");
	 		String SearchiconHeight = Searchicon.getCssValue("height");
	 		
	 		//To Find Shopping Bag icon color
	        WebElement shoppingbag = BNBasicCommonMethods.findElement(driver, obj,"ShoppingBagIcon");
	        String shoppingbagwidth = shoppingbag.getCssValue("width");
	 		String shoppingbagHeight = shoppingbag.getCssValue("height");
	 		
	 		
	 		//To Find Shopping bag count 
	        WebElement ShoppingCount =  BNBasicCommonMethods.findElement(driver, obj, "ShoppingBagCount");
	        String ShoppingCountwidth = ShoppingCount.getCssValue("width");
	 		String ShoppingCountHeight = ShoppingCount.getCssValue("height");
	 		
	 		String ExpectedBackArrowheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120A", sheet, 4);
			String ExpectedBackArrowwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120A", sheet, 5);
	 		
			String ExpectedHamburgerheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120B", sheet, 4);
			String ExpectedHamburgerwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120B", sheet, 5);

			String ExpectedBNHeaderheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120C", sheet, 4);
			String ExpectedBNHeaderwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120C", sheet, 5);
			
			String ExpectedSearchheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120D", sheet, 4);
			String ExpectedSearchwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120D", sheet, 5);
	 	
		 	
			String ExpectedScanheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120E", sheet, 4);
			String ExpectedScanwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120E", sheet, 5);
				
			String ExpectedShoppingbagheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120F", sheet, 4);
			String ExpectedShoppingbagwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120F", sheet, 5);
	 		
			String ExpectedShoppingcountheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120G", sheet, 4);
			String ExpectedShoppingcountwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120G", sheet, 5);
	 		
	
		/*	
			if(BackarrowiconHeight.equals(ExpectedBackArrowheight)&&Backarrowiconwidth.equals(ExpectedBackArrowwidth))
				pass("Back Arrow Icon Height and width Current : "+Backarrowiconwidth+" & "+BackarrowiconHeight+" Expected : "+ExpectedBackArrowwidth+" & "+ExpectedBackArrowheight);
			else
				fail("Back Arrow Icon Height and width Current : "+Backarrowiconwidth+" & "+BackarrowiconHeight+" Expected : "+ExpectedBackArrowwidth+" & "+ExpectedBackArrowheight);
*/
			
			if(hamburgerHeight.equals(ExpectedHamburgerheight)&&hamburgerwidth.equals(ExpectedHamburgerwidth))
				pass("Hamburger Icon Height and width Current : "+hamburgerwidth+" & "+hamburgerHeight+" Expected : "+ExpectedHamburgerwidth+" & "+ExpectedHamburgerheight);
			else
				fail("hamburger Icon Height and width Current : "+hamburgerwidth+" & "+hamburgerHeight+" Expected : "+ExpectedHamburgerwidth+" & "+ExpectedHamburgerheight);

			
			if(BNHeaderHeight.equals(ExpectedBNHeaderheight)&&BNHeaderwidth.equals(ExpectedBNHeaderwidth))
				pass("B&N Header Icon Height and width Current : "+BNHeaderwidth+" & "+BNHeaderHeight+" Expected : "+ExpectedBNHeaderwidth+" & "+ExpectedBNHeaderheight);
			else
				fail("B&N Header Icon Height and width Current : "+BNHeaderwidth+" & "+BNHeaderHeight+" Expected : "+ExpectedBNHeaderwidth+" & "+ExpectedBNHeaderheight);

			
			if(SearchiconHeight.equals(ExpectedSearchheight)&&Searchiconwidth.equals(ExpectedSearchwidth))
				pass("Search Icon Height and width Current : "+Searchiconwidth+" & "+SearchiconHeight+" Expected : "+ExpectedSearchwidth+" & "+ExpectedSearchheight);
			else
				fail("Search Icon Height and width Current : "+Searchiconwidth+" & "+SearchiconHeight+" Expected : "+ExpectedSearchwidth+" & "+ExpectedSearchheight);

			if(scaniconHeight.equals(ExpectedScanheight)&&scaniconwidth.equals(ExpectedScanwidth))
				pass("Scan Icon Height and width Current : "+scaniconwidth+" & "+scaniconHeight+" Expected : "+ExpectedScanwidth+" & "+ExpectedScanheight);
			else
				fail("Scan Icon Height and width Current : "+scaniconwidth+" & "+scaniconHeight+" Expected : "+ExpectedScanwidth+" & "+ExpectedScanheight);
			
			if(shoppingbagHeight.equals(ExpectedShoppingbagheight)&&shoppingbagwidth.equals(ExpectedShoppingbagwidth))
				pass("Shopping Bag Icon Height and width Current : "+shoppingbagwidth+" & "+shoppingbagHeight+" Expected : "+ExpectedShoppingbagwidth+" & "+ExpectedShoppingbagheight);
			else
				fail("Shopping Bag Icon Height and width Current : "+shoppingbagwidth+" & "+shoppingbagHeight+" Expected : "+ExpectedShoppingbagwidth+" & "+ExpectedShoppingbagheight);
		
			if(ShoppingCountHeight.equals(ExpectedShoppingcountheight)&&ShoppingCountwidth.equals(ExpectedShoppingcountwidth))
				pass("Shopping Bag Icon count Height and width Current : "+ShoppingCountwidth+" & "+ShoppingCountHeight+" Expected : "+ExpectedShoppingcountwidth+" & "+ExpectedShoppingcountheight);
			else
				fail("Shopping Bag Icon count Height and width Current : "+ShoppingCountwidth+" & "+ShoppingCountHeight+" Expected : "+ExpectedShoppingcountwidth+" & "+ExpectedShoppingcountheight);
			
			
			
  		}
  		
  		 catch (Exception e)
		{
			System.out.println(e.getMessage());
		
		}
  	}
  	
  	/*BNIA-132 Verify that header icons should be displayed in green color as per the creative*/
  	public void BNIA132()
  	{
  		ChildCreation("BNIA-132 Verify that header icons should be displayed in green color as per the creative");
  		try
  		{
  			sheet = BNBasicCommonMethods.excelsetUp("TopMenu navigation");
 	  /*  BNBasicCommonMethods.waitforElement(wait, obj, "BrowseTile");
 	    BNBasicCommonMethods.findElement(driver, obj, "BrowseTile").click();*/
 	    BNBasicCommonMethods.waitforElement(wait, obj, "BackArrow");
	 		
 	    // To find Back button Color
 	    
 	    	WebElement Backbtn = BNBasicCommonMethods.findElement(driver, obj, "BackArrow");
	 		String backarrowcolor = Backbtn.getCssValue("lighting-color");
	 		Color backarw = Color.fromString(backarrowcolor);
	 		String HexSignMaskColor = backarw.asHex();
	 		
	 		
	 		
	        // TO find Hamburger menu color 
	 		WebElement hamburger = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
	 		String hcolor=hamburger.getCssValue("lighting-color");
	 		String hamburgerwidth = hamburger.getCssValue("width");
	 		String hamburgerHeight = hamburger.getCssValue("height");
	 		
	 		Color hamcolor = Color.fromString(hcolor);
	 		String hexhambuColor = hamcolor.asHex();
	 		
	 	     // To find Header logo color 
	 		
	 		WebElement BNHeader = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
	 		String BNHeadercolor=BNHeader.getCssValue("lighting-color");
	 		String BNHeaderwidth = BNHeader.getCssValue("width");
	 		String BNHeaderHeight = BNHeader.getCssValue("height");
	 		Color BNcolor = Color.fromString(BNHeadercolor);
	 		String BNHeaderlogoColor = BNcolor.asHex();
	 		
	 		
	 		 
	 		//To find Scan icon color
	 		
	 		WebElement scanicon = BNBasicCommonMethods.findElement(driver, obj, "ScanIcon");
	 		String scancolor = scanicon.getCssValue("lighting-color");
	 		String scaniconwidth = scanicon.getCssValue("width");
	 		String scaniconHeight = scanicon.getCssValue("height");
	 		Color scanicolor = Color.fromString(scancolor);
	 		String hexscanColor = scanicolor.asHex();
	 		
	 		
	 	
	 		//To find Search icon color
	 		
	 		WebElement Searchicon = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
	 		String Searchcolor = Searchicon.getCssValue("lighting-color");
	 		String Searchiconwidth = Searchicon.getCssValue("width");
	 		String SearchiconHeight = Searchicon.getCssValue("height");
	 		Color Searchiconcolor = Color.fromString(Searchcolor);
	 		String hexSearchColor = Searchiconcolor.asHex();
	 		
	 		
	 		
	 		// To Find Shopping Bag icon color
	 		
	        WebElement shoppingbag = BNBasicCommonMethods.findElement(driver, obj,"ShoppingBagIcon");
	        String shoppingicon=shoppingbag.getCssValue("lighting-color");
	 		Color shop = Color.fromString(shoppingicon);
	 		String shopColor = shop.asHex();
	        String shoppingbagwidth = shoppingbag.getCssValue("width");
	 		String shoppingbagHeight = shoppingbag.getCssValue("height");
	 		
	 		
	 		//To Find Shopping bag count 
	        WebElement ShoppingCount =  BNBasicCommonMethods.findElement(driver, obj, "ShoppingBagCount");
	        String shoppingiconcount=shoppingbag.getCssValue("lighting-color");
	 		Color shopcount = Color.fromString(shoppingiconcount);
	 		String shopColorcount = shopcount.asHex();
	        String ShoppingCountwidth = ShoppingCount.getCssValue("width");
	 		String ShoppingCountHeight = ShoppingCount.getCssValue("height");
	 		String Shoppingcoutfont = ShoppingCount.getCssValue("font-family");
	 		String ShoppingcoutSize = ShoppingCount.getCssValue("font-size");
	 		
	 		
	 		
	 		
	 	/*	String ExpectedBackArrowheight = "38px";
			String ExpectedBackArrowwidth = "20px";
			String ExpectedBackArrowcolor = "#ffffff";*/
	 		String ExpectedBackArrowheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120A", sheet, 4);
			String ExpectedBackArrowwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120A", sheet, 5);
			String ExpectedBackArrowcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120A", sheet, 6);
	 		
			String ExpectedHamburgerheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120B", sheet, 4);
			String ExpectedHamburgerwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120B", sheet, 5);
			String ExpectedHamburgercolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120B", sheet, 6);

			String ExpectedBNHeaderheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120C", sheet, 4);
			String ExpectedBNHeaderwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120C", sheet, 5);
			String ExpectedBNHeadercolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120C", sheet, 6);
			
			String ExpectedSearchheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120D", sheet, 4);
			String ExpectedSearchwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120D", sheet, 5);
			String ExpectedSearchcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120D", sheet, 6);
	 	
		 	
			String ExpectedScanheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120E", sheet, 4);
			String ExpectedScanwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120E", sheet, 5);
			String ExpectedScancolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120E", sheet, 6);
				
			String ExpectedShoppingbagheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120F", sheet, 4);
			String ExpectedShoppingbagwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120F", sheet, 5);
			String ExpectedShoppingbagcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120F", sheet, 6);
	 		
			String ExpectedShoppingcountcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120G", sheet, 6);
			String ExpectedShoppingcountheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120G", sheet, 4);
			String ExpectedShoppingcountwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120G", sheet, 5);
			String ExpectedShoppingcountName = BNBasicCommonMethods.getExcelVal("BNIA120G", sheet, 7);
			String ExpectedShoppingcountSize = BNBasicCommonMethods.getExcelNumericVal("BNIA120G", sheet, 8);
			
			//Separating font from font-family
			String ShoppingBagCountFont = BNBasicCommonMethods.isFontName(Shoppingcoutfont);
			
			
			//Back Arrorw Icon color
	        
	        if(HexSignMaskColor.equals(ExpectedBackArrowcolor))
				pass("Back Arrow icon Color  Current : "+HexSignMaskColor+" Expected : "+ExpectedBackArrowcolor);
			else
				fail("Back Arrow icon Color  Current : "+HexSignMaskColor+" Expected : "+ExpectedBackArrowcolor);
			
			//Hamburger menu Icon color
	        
	        if(hexhambuColor.equals(ExpectedHamburgercolor))
				pass("Hamburger Icon Color  Current : "+hexhambuColor+" Expected : "+ExpectedHamburgercolor);
			else
				fail("Hamburger Icon Color  Current : "+hexhambuColor+" Expected : "+ExpectedHamburgercolor);
			
			//B & N Logo Icon color
	        
			if(BNHeaderlogoColor.equals(ExpectedBNHeadercolor))
				pass("B&N Header icon Color  Current : "+BNHeaderlogoColor+" Expected : "+ExpectedBNHeadercolor);
			else
				fail("B&N Header  icon Color  Current : "+BNHeaderlogoColor+" Expected : "+ExpectedBNHeadercolor);
		
			//Search Icon color
	        
			if(hexSearchColor.equals(ExpectedSearchcolor))
				pass("Search icon Color  Current : "+hexSearchColor+" Expected : "+ExpectedSearchcolor);
			else
				fail("Search icon Color  Current : "+hexSearchColor+" Expected : "+ExpectedSearchcolor);
			
			//Scan Icon color
	        
			if(hexscanColor.equals(ExpectedScancolor))
				pass("Scan icon Color  Current : "+hexscanColor+" Expected : "+ExpectedScancolor);
			else
				fail("Scan icon Color  Current : "+hexscanColor+" Expected : "+ExpectedScancolor);
			
			//Shopping Bag Icon color
	        
			if(shopColor.equals(ExpectedShoppingbagcolor))
				pass("Shopping Bag icon Color  Current : "+shopColor+" Expected : "+ExpectedShoppingbagcolor);
			else
				fail("Shopping Bag icon Color  Current : "+shopColor+" Expected : "+ExpectedShoppingbagcolor);
			
			//Shopping Bag Icon count color
	        
			if(shopColorcount.equals(ExpectedShoppingcountcolor))
				pass("Shopping Bag icon count Color  Current : "+shopColorcount+" Expected : "+ExpectedShoppingcountcolor);
			else
				fail("Shopping Bag icon count Color  Current : "+shopColorcount+" Expected : "+ExpectedShoppingcountcolor);
			
			if(ShoppingBagCountFont.equals(ExpectedShoppingcountName))
				pass("Shopping Bag Icon count Font Name Current : "+ShoppingBagCountFont+" Expected : "+ExpectedShoppingcountName);
			else
				fail("Shopping Bag Icon count Font Name Current : "+ShoppingBagCountFont+" Expected : "+ExpectedShoppingcountName);
			
			
			if(ShoppingcoutSize.equals(ExpectedShoppingcountSize))
				pass("Shopping Bag Icon count Font Size Current : "+ShoppingcoutSize+" Expected : "+ExpectedShoppingcountSize);
			else
				fail("Shopping Bag Icon count Font Size Current : "+ShoppingcoutSize+" Expected : "+ExpectedShoppingcountSize);
	    }
	    
	    catch (Exception e)
		{
			System.out.println(e.getMessage());
		
		}
  	}
  	
  	/*BNIA-122 Verify that Barnes & Noble logo should be displayed at center of the header as per the creative*/
  	public void BNIA122()
  	{
  		ChildCreation("BNIA-122 Verify that Barnes & Noble logo should be displayed at center of the header as per the creative.");
  		// to verifying B&N header is Displayed
  		boolean eleVisible = false;
  		try
  		{
  			WebElement BrowseTile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
  			BrowseTile.click();
  			Thread.sleep(1500);
  			eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
  			if(eleVisible)
  			{
  				pass("Barnes and Noble Logo is Displayed");
  			}
  			
  			else
  			{
  				fail("Barnes and Noble Logo is not Displayed");
  			}
  		}
  		
  		catch(Exception e)
  		{
  			exception("There is somwthing went wrong , please verify"+e.getMessage());
  		}
  	 }
  	
  	/*BNIA-123 Verify that while selecting the Barnes & Noble logo at the header,it should navigate to the homepage*/
  	public void BNIA123() throws Exception
  	{
  		ChildCreation("BNIA-123 Verify that while selecting the Barnes & Noble logo at the header,it should navigate to the homepage.");
  		BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
  		WebElement Logo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
  		Actions act = new Actions(driver);
  		act.moveToElement(Logo).click().build().perform();
  		boolean eleVisible = false;
  		try
  		{
  			BNBasicCommonMethods.WaitForLoading(wait);
  			eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
  			if(eleVisible)
  			{
  				pass("On Tapping Header BNlogo page Navigates to home page");
  			}
  			else
  			{
  				fail("On Tapping Header BNlogo page is not Navigates to home page");
  			}
  			
  		}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  	}
  		
  	/*BNIA-125 Verify that while navigating the pages in the app,the back arrow icon should be displayed at the header*/
  	public void BNIA125()
  	{
  		ChildCreation("BNIA-125 Verify that while navigating the pages in the app,the back arrow icon should be displayed at the header");
  		try
  		{
  			Thread.sleep(1000);
  			BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
  			WebElement HomePage = BNBasicCommonMethods.findElement(driver, obj, "HomePage");
    		BNBasicCommonMethods.waitforElement(wait, obj, "SearchTile");
  		   	WebElement SearchTile = BNBasicCommonMethods.findElement(driver, obj, "SearchTile");
  		   	SearchTile.click();
  			Thread.sleep(1000);
  			WebElement BackIcon = BNBasicCommonMethods.findElement(driver, obj, "BackIcon");
  			BackIcon.click();
  			Thread.sleep(1000);
  			HomePage = BNBasicCommonMethods.findElement(driver, obj, "HomePage");
  			if(HomePage.isDisplayed())
  			{
  				pass("Back Icon is displayed in the header");
  			}
  			else
  			{
  				fail("Back Icon is not displayed in the header");
  			}
  			
  		}
  		
  		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  	
  	/*BNIA-126 Verify that while tapping the back arrow icon,it should navigated to the previous page*/
  	public void BNIA126()
  	{
  	   ChildCreation("BNIA-125 Verify that while navigating the pages in the app,the back arrow icon should be displayed at the header."
  	   +System.lineSeparator()+ "2. BNIA-126 Verify that while tapping the back arrow icon,it should navigated to the previous page");
  		try
  		{
  			Thread.sleep(1000);
  			BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
  			WebElement HomePage = BNBasicCommonMethods.findElement(driver, obj, "HomePage");
    		BNBasicCommonMethods.waitforElement(wait, obj, "SearchTile");
  		   	WebElement SearchTile = BNBasicCommonMethods.findElement(driver, obj, "SearchTile");
  		   	SearchTile.click();
  			Thread.sleep(2000);
  			WebElement BackIcon = BNBasicCommonMethods.findElement(driver, obj, "BackIcon");
  			BackIcon.click();
  			Thread.sleep(1500);
  			HomePage = BNBasicCommonMethods.findElement(driver, obj, "HomePage");
  			if(HomePage.isDisplayed())
  			{
  				pass("On tapping back icon page navigates to previous page");
  			}
  			else
  			{
  				fail("On tapping back icon page is not navigating to previous page");
  			}
  			
  		}
  		
  		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  	
  	/*BNIA-127 Verify that while scrolling the menu pages vertically,the header should be displayed as static and the page should be scrolled*/
  	public void BNIA127()
  	{
  		ChildCreation("BNIA-127 Verify that while scrolling the menu pages vertically,the header should be displayed as static and the page should be scrolled");
  		try
  		{
  			Thread.sleep(2000);
  			Actions act = new Actions(driver);
  			for(int ctr=0 ; ctr<15 ;ctr++)
  		  	{
  		  		act.sendKeys(Keys.CONTROL).sendKeys(Keys.ARROW_DOWN).build().perform();
  		  	}
  			//WebElement MenuWalkingDeadhot = BNBasicCommonMethods.findElement(driver, obj,"MenuWalkingDeadhot");
  			//BNBasicCommonMethods.scrolldown(MenuWalkingDeadhot, driver);
  			WebElement BNHeader = BNBasicCommonMethods.findElement(driver, obj, "BNHeader");
  			if(BNHeader.isDisplayed())
  			{
  				pass("Header is displayed while scrolling the page vertically");
  			}
  			else
  			{
  				fail("Header is not displaying while scrolling the page vertically");
  			}
  			
  		}
  		
  		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  	
  	/*BNIA-133 Verify that while selecting the hamburger menu icon at the header,the pancake flyover should be displayed*/
  	public void BNIA133()
  	{
  		ChildCreation("BNIA-133 Verify that while selecting the hamburger menu icon at the header,the pancake flyover should be displayed");
  		
  		try
  		{
  			Thread.sleep(1000);
  			WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
  			if(HamburgerMenu.isDisplayed())
  			{
  				log.add("Hamburger menu is displayed");
  				HamburgerMenu.click();
  				Thread.sleep(1000);
  				WebElement PancakeFlyover = BNBasicCommonMethods.findElement(driver, obj, "PancakeFlyover");
  				if(PancakeFlyover.isDisplayed())
  				{
  					pass("Pancake Flyover is Displayed on selecting hamburger menu",log);
  				}
  				else
  				{
  					fail("Pacake flyover is not displayed on selecting the hamburger menu",log);
  				}
  			}
  			else
  			{
  				fail("Hamburger menu is not displayed");
  			}
  		}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  		
  	}
  	
  	/*BNIA-134 Verify that hamburger menu icon should be in disabled state at the header on enabling the category menu page*/
  	public void BNIA134()
  	{
  		ChildCreation("BNIA-134 Verify that hamburger menu icon should be in disabled state at the header on enabling the category menu page");
  		
  		try
  		{
  		   WebElement SearchMask = BNBasicCommonMethods.findElement(driver, obj, "SearchMask");
  		   if(SearchMask.isDisplayed())
  		   {
  			   log.add("Search Page Mask was displayed successful");
  			   pass("mask Displayed successfully",log);
  		   }
  		   else
  		   {
  			   fail("Mask is not displayed successfully");
  		   }
  		 SearchMask.click();
  		}
  		
  		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  	
  	/*BNIA-136 Verify that by default empty shopping bag icon should be displayed at the header*/
  	public void BNIA136()
  	{
  		ChildCreation("BNIA-136 Verify that by default empty shopping bag icon should be displayed at the header");
  		try
  		{
  			Thread.sleep(2000);
  		    WebElement BagIconHeader = BNBasicCommonMethods.findElement(driver, obj, "BagIconHeader");
  		    if(BagIconHeader.isDisplayed())
  		    {
  		    	pass("Bag Icon in Header is Displayed");
  		    	WebElement EmptyBagCountHead = BNBasicCommonMethods.findElement(driver, obj, "BagIconHeader");
  		    	//WebElement EmptyBagCountHead = driver.findElement(By.xpath("//*[@id='bag_icon']//*[@id='bag_count']"));
  		    	//System.out.println(EmptyBagCountHead.getText());
  		    	//String script = "return document.getElementById('bag_count').innerHTML";
  		    	//String script1 = "return arguments[0].innerHTML";
  		    	String script = "return $('#bag_count').text()";
  		    	String n = (String) ((JavascriptExecutor) driver).executeScript(script, EmptyBagCountHead);
  		    	String []prdtcnt = n.split(" ");
  		    	//System.out.println(prdtcnt[0]);
  		    	
  		    	//System.out.println(Integer.parseInt(EmptyBagCountHead.getText().toString()));
  		    	if(Integer.parseInt(prdtcnt[0]) ==  0 )
  		    	{
  		    		log.add("Bag count is "+prdtcnt[0]);
  		    		pass("Empty Shopping bag icon is displayed in the header",log);
  		    	}
  		    	else
  		    	{
  		    		log.add("Bag count is "+Integer.parseInt(EmptyBagCountHead.getText().toString()));
  		    	   fail("Empty shopping bag is not displayed in the header",log);
  		    	}
  		    }
  		    else
  		    {
  		    	
  		    }
  			
  		}
  		
  		catch(Exception e)
		{
  			System.out.println(e.getMessage());
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  	
  	/*BNIA-137 Verify that while selecting the empty "Bag" icon at the header,the empty shopping bag page should be displayed with the "Continue Shopping" button*/
  	public void BNIA137()
  	{
  		ChildCreation("BNIA-137 Verify that while selecting the empty Bag icon at the header,the empty shopping bag page should be displayed with the Continue Shopping button");
  		try
  		{
  			BNBasicCommonMethods.waitforElement(wait, obj, "BagIconHeader");
  			BNBasicCommonMethods.findElement(driver, obj, "BagIconHeader");
  			WebElement BagIconHeader = BNBasicCommonMethods.findElement(driver, obj, "BagIconHeader");
  			if(BagIconHeader.isDisplayed())
  			{
  				BagIconHeader.click();
  				Thread.sleep(3000);
  				pass("Sopping bag icon displyed in the header");
  				BNBasicCommonMethods.waitforElement(wait, obj, "emptyShoppingpage");
  				WebElement emptyShoppingpage = BNBasicCommonMethods.findElement(driver, obj, "emptyShoppingpage");
  				WebElement EmptyContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "EmptyContinueBtn");
  				if(EmptyContinueBtn.isDisplayed() && emptyShoppingpage.isDisplayed() )
  				{
  					log.add("Page and continue shopping icon is displayed");
  					pass("Shopping bag page is displayed with continue shopping button",log);
  				}
  				else
  				{
  					fail("Shopping bag page is not displayed",log);
  				}
  			 }
  			 else
  			 {
  				 System.out.println();
  				fail("Shopping bag icon is not displayed in the header");
  			 }
  			
  		}
  		
  		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  	
  	/*BNIA-138 Verify that while selecting the "Bag" icon at the header,the added items to the shopping bag should be displayed with the product details*/
  	public void BNIA138()
  	{
  		ChildCreation("BNIA-138 Verify that while selecting the Bag icon at the header,the added items to the shopping bag should be displayed with the product details");
  		try
  		{
  			sheet = BNBasicCommonMethods.excelsetUp("TopMenu navigation");
	  		String EANs = BNBasicCommonMethods.getExcelVal("BNIA126", sheet, 3);
	  		String[] EAN = EANs.split(":");
  		   	Thread.sleep(1500);
  		   	BNBasicCommonMethods.waitforElement(wait, obj, "SearchTile");
  		   	WebElement SearchTile = BNBasicCommonMethods.findElement(driver, obj, "SearchTile");
  		   	SearchTile.click();
  		   	for(int ctr = 0 ; ctr<EAN.length ;ctr++)
  		   	{
	  		   	Thread.sleep(1000);
	  		   	BNBasicCommonMethods.waitforElement(wait, obj, "SearchTxtBx");
	  		    WebElement SearchTxtBx = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBx");
	  		    WebElement SearchTxtBxnew = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");	
	  		    Actions action = new Actions(driver);	
	  		    	SearchTxtBx.click();
		  			action.moveToElement(SearchTxtBx).click();
		  			SearchTxtBxnew.clear();
		  			Thread.sleep(500);
		  			SearchTxtBxnew.sendKeys(EAN[ctr]);
		  			//action.moveToElement(SearchTxtBx).sendKeys(EAN[ctr]).build().perform();
		  			//System.out.println(EAN[ctr]);
		  			Thread.sleep(1500);	
		  			SearchTxtBxnew.sendKeys(Keys.ENTER);
		  		    Thread.sleep(2000);
		  			String str = driver.getCurrentUrl();
  					driver.get(str);
		  			Thread.sleep(2000);
		  				int counter=1;
		  				do
		  				{
			  				try
			  				{
			  					Thread.sleep(3000);
			  					BNBasicCommonMethods.waitforElement(wait, obj, "ATBBtn");
					  			WebElement ATBBtn = BNBasicCommonMethods.findElement(driver, obj, "ATBBtn");
					  			ATBBtn.click();
			  					Thread.sleep(1000);
			  					BNBasicCommonMethods.waitforElement(wait, obj, "ATBSuccessAlertOverlay");
				  				WebElement ATBSuccessAlertOverlay = BNBasicCommonMethods.findElement(driver, obj, "ATBSuccessAlertOverlay");
				  				counter=5;
			  				}
			  				
			  				catch(Exception e)
			  				{
			  					WebElement ATBErrorAlt = driver.findElement(By.xpath("//*[@id='skMob_ErrorsDiv_id']"));
			  					if(ATBErrorAlt.isDisplayed())
			  					{
			  						WebElement ErroBtn = driver.findElement(By.xpath("//*[@id='skMob_ErrorsOK_id']"));
			  						ErroBtn.click();
			  					}
			  					String str1 = driver.getCurrentUrl();
			  					driver.get(str1);
			  			    }
			  				counter++;
		  				} while(counter == 5 );
		  				
		  				WebElement ATBSuccessAlertOverlay = BNBasicCommonMethods.findElement(driver, obj, "ATBSuccessAlertOverlay");
		  				if(BNBasicCommonMethods.isElementPresent(ATBSuccessAlertOverlay))
		  				{
		  					BNBasicCommonMethods.waitforElement(wait, obj, "ContineShoppingBtn");
		  					WebElement ContineShoppingBtn = BNBasicCommonMethods.findElement(driver, obj, "ContineShoppingBtn");
		  					ContineShoppingBtn.click();
		  					BNBasicCommonMethods.waitforElement(wait, obj, "BackIcon");
		  					WebElement BackIcon = BNBasicCommonMethods.findElement(driver, obj, "BackIcon");
		  					if(BackIcon.isDisplayed())
		  					{
		  						log.add("Product added to bag");
		  						BackIcon.click();
		  						Thread.sleep(2000);
		  						BNBasicCommonMethods.waitforElement(wait, obj, "SearchTxtBx");
		  					}
		  					else
		  					{
		  						fail("Not added to Bag",log);
		  					}
		  				}
		  				
		  				else
		  				{
		  					fail("Add to Bag Success alert overlay is not displayed",log);
		  				}		  			
  		   	}
  		   	pass("Items added to Bag",log);
  /*			sheet = BNBasicCommonMethods.excelsetUp("TopMenu navigation");
	  		String EANs = BNBasicCommonMethods.getExcelNumericVal("BNIA126", sheet, 3);
            String [] EAN = EANs.split(":");*/
  		   	BNBasicCommonMethods.waitforElement(wait, obj, "BagIconHeader");
            WebElement BagIconHeader = BNBasicCommonMethods.findElement(driver, obj, "BagIconHeader");
            BagIconHeader.click();
            Thread.sleep(4000);
            WebElement EmptyBagCountHead = BNBasicCommonMethods.findElement(driver, obj, "EmptyBagCountHead");
            String script = "return $('#bag_count').text()";
		    String n = (String) ((JavascriptExecutor) driver).executeScript(script, EmptyBagCountHead);
		    String []prdtcnt = n.split(" ");
            Thread.sleep(2500);
  			int Count = Integer.parseInt(prdtcnt[0]);
  			if(Count > 0 )
  			{
  				log.add("Bag count is greater than zero :"+Count);
  				/*BagIconHeader = BNBasicCommonMethods.findElement(driver, obj, "BagIconHeader");
  				BagIconHeader.click();*/
  				Thread.sleep(2500);
  				BNBasicCommonMethods.waitforElement(wait, obj, "ItemsHeading");
  				List <WebElement> Productcount = driver.findElements(By.xpath("//*[@class='product-title']")); 
  				//System.out.println(Productcount.size());
  				for(int ctr = 1 ; ctr < Productcount.size()+1  ; ctr++)
  				{
  					for(int ctr1 = 0 ; ctr1 < EAN.length; ctr1++)
  					{
	  					WebElement ProductsCount = driver.findElement(By.xpath("(//*[@class='product-title'])["+ctr+"]"));
	  					String prodhref = ProductsCount.getAttribute("href");
	  					String[] compareean = prodhref.split("ean=");
	  					//System.out.println(compareean[1] +"Provided EAN"+EAN[ctr1]);
	  					//System.out.println();
	  					if(compareean[1].contains(EAN[ctr1]))
	  					{
	  						log.add("Added product is available in the bag and the respective product EAN is"+EAN[ctr1]);
	  					}
	  					else
	  					{
	  						//log.add("Added product is not available in the bag and the respective product EAN is"+EAN[ctr1]);
	  					}
  					}
  				}
  				
  				pass("Added product is displayed in the bag",log);
  				Thread.sleep(3000);
  			}
  			
  			else
  			{
  				fail(" Bag count is not dislpayed :"+Count);
  			}
  			
  			WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
  			BNLogo.click();
  			Thread.sleep(2000);
  		}
  		
  		catch(Exception e)
		{
  			System.out.println(e.getMessage());
			exception("There is something went wrong , please verify"+e.getMessage());
		}
  		System.out.println("4/11 - BNIA07_Header_navigation is completed");
  	}
  	
  	/*BNIA-130 Verify that search icon should be displayed at the header as per the creative*/
    public void BNIA130()
    {
    	ChildCreation("BNIA-130 Verify that search icon should be displayed at the header as per the creative");
    	try
    	{
    		BNBasicCommonMethods.waitforElement(wait, obj, "SearchIconHeader");
	  		WebElement SearchIconHeader = BNBasicCommonMethods.findElement(driver, obj, "SearchIconHeader");
	  		if(BNBasicCommonMethods.isElementPresent(SearchIconHeader))
	  		{
	  			pass("search icon is displayed in the header");
	  		}
	  		
	  		else
	  		{
	  			fail("Search icon is not displayed in the header");
	  		}
    	}
    	
    	catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
    }
  	
  	/*BNIA-139 Verify that while selecting the "Search" icon at the header,the search field should be enabled*/
  	public void BNIA139() throws Exception
  	{
  		try
  		{
  			ChildCreation("BNIA-130 Verify that search icon should be displayed at the header as per the creative,2 BNIA-139 Verify that while selecting the Search icon at the header,the search field should be enabled.");
	  		BNBasicCommonMethods.waitforElement(wait, obj, "SearchIconHeader");
	  		WebElement SearchIconHeader = BNBasicCommonMethods.findElement(driver, obj, "SearchIconHeader");
	  		if(BNBasicCommonMethods.isElementPresent(SearchIconHeader))
	  		{
	  			pass("Search Icon is Displayed in the header");
		  		SearchIconHeader.click();
		  		Thread.sleep(1500);
		  		WebElement SearchPage = BNBasicCommonMethods.findElement(driver, obj, "SearchPage");
		  		if(BNBasicCommonMethods.isElementPresent(SearchPage))
		  		{
		  			log.add("Navigated to Search Page");
		  			pass("Search page is displayed successfully",log);
		  		}
		  		else
		  		{
		  			log.add("Page is Not navigate to Search page");
		  			fail("Search Page is not displayed successfully",log);
		  		}
	  		}
	  		else
	  		{
	  			fail("Search Icon is not displayed in the header");
	  		}
  		}
  		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  		
  			BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
			WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			logo.click();
			Thread.sleep(500);
  	}
  	
  	/*BNIA-140 Verify that user should be able to enter the search keywords in the search field text box*/
  	public void BNIA140() throws Exception
  	{
  		ChildCreation("BNIA-140 Verify that user should be able to enter the search keywords in the search field text box");
  		try
  		{
  			Thread.sleep(1500);
  			BNBasicCommonMethods.waitforElement(wait, obj, "SearchTile");
  			WebElement stile = BNBasicCommonMethods.findElement(driver, obj, "SearchTile");
  			stile.click();
  			Thread.sleep(1000);
	  		sheet = BNBasicCommonMethods.excelsetUp("TopMenu navigation");
	  		//String SreachKwd = BNBasicCommonMethods.getExcelVal("BNIA140", sheet, 2);
	  		WebElement searchbox1 = BNBasicCommonMethods.findElement(driver, obj, "searchbox1");
			/*sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String EAN = BNBasicCommonMethods.getExcelNumericVal("BNIA346", sheet, 3);*/
			boolean entered = false;
			do
			{
				
				String SreachKwd = BNBasicCommonMethods.getExcelVal("BNIA140", sheet, 2);
				Actions act = new Actions(driver);
				searchbox1.click();
				searchbox1.sendKeys(SreachKwd);
				entered = searchbox1.getAttribute("value").isEmpty();
			}while(entered==true);
			Thread.sleep(500);
			/*Actions actt = new Actions(driver);
		    actt.sendKeys(Keys.ENTER).build().perform();*/
		    
	  		/*if(SearchTxtBx.isDisplayed())
	  		{
	  			pass("Search Box is found");
	  			
	  			Thread.sleep(6000);
	  			//BNBasicCommonMethods.waitforElement(wait, obj, "SearchTextbox");
	  			System.out.println("BNIA-140");
	  			Actions act = new Actions(driver);
	  			act.moveToElement(SearchTxtBx).click().perform();
	  			act.moveToElement(SearchTxtBxnew1).sendKeys(SreachKwd).build().perform();
	  			//SearchTxtBx.sendKeys(SreachKwd);
*/	  			BNBasicCommonMethods.waitforElement(wait, obj, "searchSuggestioncontainer");
	  			 WebElement searchSuggestioncontainer = BNBasicCommonMethods.findElement(driver, obj, "searchSuggestioncontainer");
	    		if(searchSuggestioncontainer.isDisplayed())
	    		{
	  				
	  				pass("User able to enter searchKeyword in search Textbox");
	  			}
	  			else
	  			{
	  				
	  				pass("User not able to enter searchKeyword in search Textbox");
	  			}
	  	}
  		catch(Exception e)
		{
  			System.out.println(e.getMessage());
			exception(" There is somwthing went wrong , please verify"+e.getMessage());
		}
  		
  		
  	}
  	
  	/*BNIA-141 Verify that while entering the search keywords in the search field,the search suggestions should be displayed*/
  	public void BNIA141()
  	{
  		ChildCreation("BNIA-141 Verify that while entering the search keywords in the search field,the search suggestions should be displayed.");
  		try
  		{
  		   BNBasicCommonMethods.waitforElement(wait, obj, "searchSuggestioncontainer");
  		   WebElement searchSuggestioncontainer = BNBasicCommonMethods.findElement(driver, obj, "searchSuggestioncontainer");
  		   if(searchSuggestioncontainer.isDisplayed())
  		   {
  			  pass("Search suggestions are displayed"); 
  			  Thread.sleep(2000);
  			  List <WebElement> SearchSugglist = driver.findElements(By.xpath("//*[@class='skMobsuggestedItem_container']//*[@class='suggestedItem ']"));
  			  log.add("Search Suggestion count "+SearchSugglist.size());
  			  for(int ctr=1 ; ctr < SearchSugglist.size()+1 ; ctr++)
  			  {
  				  String Suggesting = driver.findElement(By.xpath("(//*[@class='skMobsuggestedItem_container']//*[@class='suggestedItem '])["+ctr+"]")).getText();
  				  log.add("Suggestion"+(ctr) +" : "+Suggesting);
  				//  System.out.println(Suggesting);
  				  
  			  }
  			  pass("Search Suuggestion are displayed and are listed below",log);
  		   }
  		   else
  		   {
  			  fail("Search suggestions are not displayed");
  		   }
  		   
  		}
  		
  		catch(Exception e)
		{
  	       System.out.println(e.getMessage());
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  		
  	}
  	
  	/*BNIA-238 Verify that Search suggestion should not shown after clearing the search keywords in search page*/
  	public void BNIA238()
  	{
  		try
  		{
  			ChildCreation("BNIA-238 Verify that Search suggestion should not shown after clearing the search keywords in search page");
  			Thread.sleep(1000);
  			BNBasicCommonMethods.waitforElement(wait, obj, "SearchClearIcon");
  			WebElement SearchClearIcon = BNBasicCommonMethods.findElement(driver, obj, "SearchClearIcon");
  			if(SearchClearIcon.isDisplayed())
  			{
  				log.add("Search Clear icon is displayed.");
  				SearchClearIcon.click();
  				Thread.sleep(1000);
  				WebElement SearchTextbox = BNBasicCommonMethods.findElement(driver, obj, "SearchTextbox");
  				if(SearchTextbox.getText().isEmpty())
  				{
  					log.add("Search Keyword is Cleared");
  					try
  					{
  					WebElement searchSuggestioncontainer = BNBasicCommonMethods.findElement(driver, obj, "searchSuggestioncontainer");
  					fail("Search Suggestion is showing after clearing the search keyword in search field",log);
  					}
  					catch(Exception e)
  					{
  					pass("Search Suggestion is not shown after clearing the Search keyword in Search field",log);
  					}
  				}
  				else
  				{
  					fail("Search Keyword is not clearing");
  				}
  			}
  			else
  			{
  				fail("Search Clear Icon is not Displayed");
  			}
  			
  			
  		}
  		
  		catch(Exception e)
		{
  	       System.out.println(e.getMessage());
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  		
  	}
  	
/************************************************************************ Menu Browse Catalogue <---> ( Hamburger Menu)**********************************/
  	
  	@Test(priority=5)	
   
  	public void BNIA43_Hamburger_Menu() throws Exception
    {
  		TempSignIn();
        AST_B();
      //KIOSK_B();
    }
  	
    public void AST_B() throws Exception
    {
    	BNIA166();
    	BNIA155();
    	BNIA222();
    	BNIA223();
    	BNIA235();
    	BNIA236();
    	BNIA156();
    	BNIA158_AST();
    	BNIA227();
    	BNIA228();
    	BNIA231();
    	BNIA232();
    	BNIA234();
    	//BNIA159_AST();
    	BNIA159();
    	//BNIA165_AST();
    	BNIA165();
    	BNIA224_AST();
    	BNIA160_AST();
    	BNIA161_AST();
    	BNIA225_AST();
    }
    
    /*BNIA-159 Verify that while selecting the hamburger menu option at the header in all the pages,it should display the category menu page*/
    public void BNIA159()
    {
    	ChildCreation("BNIA-159 Verify that while selecting the hamburger menu option at the header in all the pages,it should display the category menu page");
    	try
    	{
    		if(Flag227  && Flag228 && Flag231 && Flag232 && Flag234)
    		{
    			pass("Category Menu Page is displayed in all the pages");
    		}
    		else
    		{
    			fail("Category Menu Page is not displayed in all the pages");
    		}
    	}
    	catch(Exception e)
    	{
    		exception("There is something wrong please verify "+e.getMessage());
    	}
    	
    }
    
    /*BNIA-165 Verify that while selecting the categories and subcategories option in the category menu page,it should navigate to the respective PLP page*/
    public void BNIA165()
    {
    	ChildCreation("BNIA-165 Verify that while selecting the categories and subcategories option in the category menu page,it should navigate to the respective PLP page");
    	try
    	{
    		if(Flag227  && Flag228 && Flag231 && Flag232 && Flag234)
    		{
    			pass("Categories is displayed in all the pages");
    		}
    		else
    		{
    			fail("Categories not displayed in all the pages");
    		}
    	}
    	catch(Exception e)
    	{
    		exception("There is something wrong please verify "+e.getMessage());
    	}
    }
    
    
  	public void KIOSK_B() throws Exception
  	{
  		BNIA166();
    	BNIA155();
    	BNIA222();
    	BNIA223();
    	BNIA235();
    	BNIA236();
    	BNIA156();
    	BNIA157_KIOSK();
    	BNIA227();
    	BNIA228();
    	BNIA231();
    	BNIA232();
    	//BNIA234();
    	BNIA165_KIOSK();
    	BNIA162_KIOSK();
  	}
  	
    /*BNIA-155 Verify that Barnes & Noble logo should be displayed at the header in the Menu page*/
	public void BNIA155()
  	{
  		ChildCreation("BNIA-155 Verify that Barnes & Noble logo should be displayed at the header in the Menu page.");
  		// to verifying B&N header is Displayed
  		boolean eleVisible = false;
  		try
  		{
  			/*WebElement BrowseTile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
  			BrowseTile.click();*/
  			Thread.sleep(1500);
  			eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
  			if(eleVisible)
  			{
  				pass("Barnes and Noble Logo is Displayed");
  			}
  			
  			else
  			{
  				fail("Barnes and Noble Logo is not Displayed");
  			}
  		}
  		
  		catch(Exception e)
  		{
  			exception("There is somwthing went wrong , please verify"+e.getMessage());
  		}
  	 }
    
	/*BNIA-156 Verify that while selecting the Barnes & Noble logo at the header in the Menu page,it should navigate to the home page*/
	public void BNIA156() throws Exception
  	{
  		ChildCreation("BNIA-156 Verify that while selecting the Barnes & Noble logo at the header in the Menu page,it should navigate to the home page.");
  		BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
  		WebElement Logo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
  		Actions act = new Actions(driver);
  		act.moveToElement(Logo).click().build().perform();
  		boolean eleVisible = false;
  		try
  		{
  			BNBasicCommonMethods.WaitForLoading(wait);
  			eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
  			if(eleVisible)
  			{
  				pass("On Tapping Header BNlogo page Navigates to home page");
  			}
  			else
  			{
  				fail("On Tapping Header BNlogo page is not Navigates to home page");
  			}
  		}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  	}
	
	/*BNIA-157 Verify that home page should be displayed as per the creative & fit to the screen in Kiosk device*/
  	public void BNIA157_KIOSK()
  	{
  		ChildCreation("BNIA-157 Verify that home page should be displayed as per the creative & fit to the screen in Kiosk device");
  		boolean eleVisible = false;
  		try
  		{
  			Thread.sleep(1000);
  			eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
  			if(eleVisible)
  			{
  				pass("HomePage is Displayed");
  			}
  			
  			else
  			{
  				fail("HomePage is not Displayed");
  			}
  		}
  		
  		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  	
	/*BNIA-158 Verify that home page should be displayed as per the creative & fit to the screen in AST device*/
  	public void BNIA158_AST()
  	{
  		ChildCreation("BNIA-158 Verify that home page should be displayed as per the creative & fit to the screen in AST device.");
  		boolean eleVisible = false;
  		try
  		{
  			//Thread.sleep(1500);
  			eleVisible = BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
  			if(eleVisible)
  			{
  				pass("HomePage is Displayed");
  			}
  			
  			else
  			{
  				fail("HomePage is not Displayed");
  			}
  		}
  		
  		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  	
  	/*BNIA-224 Verify that Associate name and Signout should be displayed at the Top in the pancake flyover for the AST*/
  	public void BNIA224_AST()
  	{
  		ChildCreation("BNIA-224 Verify that Associate name and Signout should be displayed at the Top in the pancake flyover for the AST.");
  		try
  		{
  			WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
  			HamburgerMenu.click();
  			Thread.sleep(1000);
  			BNBasicCommonMethods.waitforElement(wait, obj, "AssociateName");
  			WebElement AssociateName = BNBasicCommonMethods.findElement(driver, obj, "AssociateName");
  			WebElement SignOutBnt = BNBasicCommonMethods.findElement(driver, obj, "SignOutBnt");
  			if(AssociateName.isDisplayed() && SignOutBnt.isDisplayed())
  			{
  				log.add("Asssociate name is "+AssociateName.getText());
  				log.add("Sign out Button is "+SignOutBnt.getText());
  				pass("Associate and signout button is displayed successfully",log);
  			}
  			
  			else
  			{
  				log.add("Asssociate name is "+AssociateName.getText());
  				log.add("Sign out Button is "+SignOutBnt.getText());
  				pass("Associate and signout button is not displayed successfully",log);
  			}
  		}
  		catch(Exception e)
		{
  			System.out.println(e.getMessage());
			exception("There is something went wrong , please verify"+e.getMessage());
		}
  		
  	}
  	
  	/*BNIA-160 Verify that associate user name should be displayed at the top left in the category menu page in AST device*/
  	public void BNIA160_AST()
  	{
  		ChildCreation("BNIA-160 Verify that associate user name should be displayed at the top left in the category menu page in AST device");
  		
  		try
  		{
  			BNBasicCommonMethods.waitforElement(wait, obj, "AssociateName");
  			WebElement AssociateName = BNBasicCommonMethods.findElement(driver, obj, "AssociateName");
  			WebElement SignOutBnt = BNBasicCommonMethods.findElement(driver, obj, "SignOutBnt");
  			if(AssociateName.isDisplayed() && SignOutBnt.isDisplayed())
  			{
  				log.add("Asssociate name is "+AssociateName.getText());
  				log.add("Sign out Button is "+SignOutBnt.getText());
  				pass("Associate and signout button is displayed successfully",log);
  			}
  			
  			else
  			{
  				log.add("Asssociate name is "+AssociateName.getText());
  				log.add("Sign out Button is "+SignOutBnt.getText());
  				pass("Associate and signout button is not displayed successfully",log);
  			}
  			
  		}
  		
  		catch(Exception e)
		{
  			System.out.println(e.getMessage());
			exception("There is something went wrong , please verify"+e.getMessage());
		}
  		
  	}
  	
  	/*BNIA-161 Verify that while selecting the "Signout" option at the category menu page,the associate user should be logged-out*/
  	public void BNIA161_AST()
  	{
  		ChildCreation("BNIA-161 Verify that while selecting the Signout option at the category menu page,the associate user should be logged-out.");
  		try
  		{
  				WebElement SignOutBnt = BNBasicCommonMethods.findElement(driver, obj, "SignOutBnt");
  				SignOutBnt.click();
				Thread.sleep(1000);
				BNBasicCommonMethods.waitforElement(wait, obj, "splashScreen");
				WebElement splashScreen = BNBasicCommonMethods.findElement(driver, obj, "splashScreen");
				if(splashScreen.isDisplayed())
				{
					log.add("splash screen is displayed successfully");
					pass("Current user is logged out on tapping signout button in pancake flyover",log);
				}
				else
				{
					fail("Splash screen is not displayed.");
				}
		  TempSignIn();
  		}
  		catch(Exception e)
		{
  			System.out.println(e.getMessage());
			exception("There is something went wrong , please verify"+e.getMessage());
		}
  		
  	}
  	
  	/*BNIA-225 Verify that while selecting the "Sign Out" link in pancake flyover, the Associate user should be signed out and splash screen displayed*/
  	public void BNIA225_AST()
  	{
  		ChildCreation("BNIA-225 Verify that while selecting the Sign Out link in pancake flyover, the Associate user should be signed out and splash screen displayed");
  		
  		try
  		{
  			BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
  			WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
  			HamburgerMenu.click();
  			Thread.sleep(1000);
  			BNBasicCommonMethods.waitforElement(wait, obj, "AssociateName");
  			WebElement AssociateName = BNBasicCommonMethods.findElement(driver, obj, "AssociateName");
  			WebElement SignOutBnt = BNBasicCommonMethods.findElement(driver, obj, "SignOut");
  			if(AssociateName.isDisplayed() && SignOutBnt.isDisplayed())
  			{
  				log.add("Associate and signout button is displayed successfully");
  				log.add("Asssociate name is "+AssociateName.getText());
  				log.add("Sign out Button is "+SignOutBnt.getText());
  				SignOutBnt.click();
  				Thread.sleep(1000);
  				BNBasicCommonMethods.waitforElement(wait, obj, "splashScreen");
  				WebElement splashScreen = BNBasicCommonMethods.findElement(driver, obj, "splashScreen");
  				if(splashScreen.isDisplayed())
  				{
  					log.add("splash screen is displayed successfully");
  					pass("Current user is logged out on tapping signout button in pancake flyover",log);
  				}
  				else
  				{
  					fail("Splash screen is not displayed.");
  				}
  				
  			}
  			
  			else
  			{
  				fail("Associate and signout button is not displayed");
  			}
  			System.out.println("5/11 - BNIA43_Hamburger_Menu is completed");
  		}
  		
  		catch(Exception e)
		{
  			System.out.println(e.getMessage());
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  	
  	/*BNIA-162 Verify that associated username should not be displayed in the category menu page for the Kiosk device*/
    public void BNIA162_KIOSK()
    {
    	ChildCreation("BNIA-162 Verify that associated username should not be displayed in the category menu page for the Kiosk device");
    	try
  		{
    		WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
  			Thread.sleep(1000);
  			HamburgerMenu.click();
  			WebElement KioskWelName = BNBasicCommonMethods.findElement(driver, obj, "KioskWelName");
  			if(KioskWelName.isDisplayed())
  			{
  				log.add("Kisok displayed text is "+KioskWelName);
  				pass("Kiosk welcome text is displyed "+KioskWelName);
  			}
  			else
  			{
  				fail("Kiosk Welcome text is not displayed ");
  			}
  			
  		}
  		
  		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
    }
    
  	/*BNIA-163 Verify that while selecting the hamburger menu at the header on enabling the category page,the authored category page should be closed*/
	public void BNIA163()
  	{
  		ChildCreation("BNIA-163 Verify that while selecting the hamburger menu at the header on enabling the category page,the authored category page should be closed.");
  		
  		try
  		{
  		   WebElement SearchMask = BNBasicCommonMethods.findElement(driver, obj, "SearchMask");
  		   if(SearchMask.isDisplayed())
  		   {
  			   log.add("Browse page Mask was displayed successful");
  			   pass("mask Displayed so menu page is hidden successfully",log);
  		   }
  		   else
  		   {
  			   fail("Mask is not displayed successfully");
  		   }
  		}
  		
  		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  	
  	/*BNIA-164 Verify that while selecting the back button at the header in the category menu page,it should navigate to the previous page*/
    public void BNIA164()
    {
    	ChildCreation("BNIA-164 Verify that while selecting the back button at the header in the category menu page,it should navigate to the previous page.");
    	try
  		{
    		WebElement HomePage = BNBasicCommonMethods.findElement(driver, obj, "HomePage");
    		WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
  			Thread.sleep(1500);
  			HamburgerMenu.click();
  			WebElement BrowseLink = BNBasicCommonMethods.findElement(driver, obj, "BrowseLink");
  			BrowseLink.click();
  			Thread.sleep(2000);
  			WebElement BackIcon = BNBasicCommonMethods.findElement(driver, obj, "BackIcon");
  			BackIcon.click();
  			Thread.sleep(1500);
  			HomePage = BNBasicCommonMethods.findElement(driver, obj, "HomePage");
  			if(HomePage.isDisplayed())
  			{
  				pass("On tapping back icon page navigates to previous page");
  			}
  			else
  			{
  				fail("On tapping back icon page is not navigating to previous page");
  			}
  		}
  		
  		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
    }
    
    /*BNIA-159 Verify that while selecting the hamburger menu option at the header in all the pages,it should display the category menu page*/
  	public void BNIA159_AST()
  	{
  		ChildCreation("BNIA-159 Verify that while selecting the hamburger menu option at the header in all the pages,it should display the category menu page");
    	try
    	{
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement BrowseLink = BNBasicCommonMethods.findElement(driver, obj, "BrowseLink");
    		BrowseLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "Browsepage");
    		WebElement Browsepage = BNBasicCommonMethods.findElement(driver, obj, "Browsepage");
    		if(Browsepage.isDisplayed())
    		{
    			pass("browse page is displayed on tapping browse link in Pancake flyover");
    		}
    		else
    		{
    			fail("browse page is not displayed on tapping browse link in Pancake flyover");
    		}
    		
    		
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement SearchLink = BNBasicCommonMethods.findElement(driver, obj, "SearchLink");
    		SearchLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "SearchPage");
    		WebElement SearchPage = BNBasicCommonMethods.findElement(driver, obj, "SearchPage");
    		if(SearchPage.isDisplayed())
    		{
    			pass("SearchPage page is displayed on tapping Search link in Pancake flyover");
    		}
    		else
    		{
    			fail("SearchPage page is not displayed on tapping Search link in Pancake flyover");
    		}
    		
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement StoreLocatorLink = BNBasicCommonMethods.findElement(driver, obj, "StoreLocatorLink");
    		StoreLocatorLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "StoreLocatorSearch");
    		WebElement StoreLocatorSearch = BNBasicCommonMethods.findElement(driver, obj, "StoreLocatorSearch");
    		if(StoreLocatorSearch.isDisplayed())
    		{
    			pass("StoreLocator page is displayed on tapping StoreLocator link in Pancake flyover");
    		}
    		else
    		{
    			fail("StoreLocator page is not displayed on tapping StoreLocator link in Pancake flyover");
    		}
    		
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement GiftCardLink = BNBasicCommonMethods.findElement(driver, obj, "GiftCardLink");
    		GiftCardLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardCheckBtn");
    		WebElement GiftCardCheckBtn = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
    		if(GiftCardCheckBtn.isDisplayed())
    		{
    			pass("GiftCard page is displayed on tapping StoreLocator GiftCard link in Pancake flyover");
    		}
    		else
    		{
    			fail("GiftCard page is not displayed on tapping StoreLocator GiftCard Link in Pancake flyover");
    		}
    		
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement CheckoutLink = BNBasicCommonMethods.findElement(driver, obj, "CheckoutLink");
    		CheckoutLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "EmptyContinueBtn");
    		WebElement EmptyContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "EmptyContinueBtn");
    		if(EmptyContinueBtn.isDisplayed())
    		{
    			pass("Checkout page is displayed on tapping StoreLocator Checkout link in Pancake flyover");
    		}
    		else
    		{
    			fail("Checkout page is not displayed on tapping StoreLocator Checkout Link in Pancake flyover");
    		}
    	}
    	
    	catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
    
    /*BNIA-165 Verify that while selecting the categories and subcategories option in the category menu page,it should navigate to the respective PLP page*/
    public void BNIA165_AST()
    {
    	ChildCreation("BNIA-165 Verify that while selecting the categories and subcategories option in the category menu page,it should navigate to the respective page");
    	try
    	{
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement BrowseLink = BNBasicCommonMethods.findElement(driver, obj, "BrowseLink");
    		BrowseLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "Browsepage");
    		WebElement Browsepage = BNBasicCommonMethods.findElement(driver, obj, "Browsepage");
    		if(Browsepage.isDisplayed())
    		{
    			pass("browse page is displayed on tapping browse link in Pancake flyover");
    		}
    		else
    		{
    			fail("browse page is not displayed on tapping browse link in Pancake flyover");
    		}
    		
    		
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement SearchLink = BNBasicCommonMethods.findElement(driver, obj, "SearchLink");
    		SearchLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "SearchPage");
    		WebElement SearchPage = BNBasicCommonMethods.findElement(driver, obj, "SearchPage");
    		if(SearchPage.isDisplayed())
    		{
    			pass("SearchPage page is displayed on tapping Search link in Pancake flyover");
    		}
    		else
    		{
    			fail("SearchPage page is not displayed on tapping Search link in Pancake flyover");
    		}
    		
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement StoreLocatorLink = BNBasicCommonMethods.findElement(driver, obj, "StoreLocatorLink");
    		StoreLocatorLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "StoreLocatorSearch");
    		WebElement StoreLocatorSearch = BNBasicCommonMethods.findElement(driver, obj, "StoreLocatorSearch");
    		if(StoreLocatorSearch.isDisplayed())
    		{
    			pass("StoreLocator page is displayed on tapping StoreLocator link in Pancake flyover");
    		}
    		else
    		{
    			fail("StoreLocator page is not displayed on tapping StoreLocator link in Pancake flyover");
    		}
    		
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement GiftCardLink = BNBasicCommonMethods.findElement(driver, obj, "GiftCardLink");
    		GiftCardLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardCheckBtn");
    		WebElement GiftCardCheckBtn = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
    		if(GiftCardCheckBtn.isDisplayed())
    		{
    			pass("GiftCard page is displayed on tapping StoreLocator GiftCard link in Pancake flyover");
    		}
    		else
    		{
    			fail("GiftCard page is not displayed on tapping StoreLocator GiftCard Link in Pancake flyover");
    		}
    		
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement CheckoutLink = BNBasicCommonMethods.findElement(driver, obj, "CheckoutLink");
    		CheckoutLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "EmptyContinueBtn");
    		WebElement EmptyContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "EmptyContinueBtn");
    		if(EmptyContinueBtn.isDisplayed())
    		{
    			pass("Checkout page is displayed on tapping StoreLocator Checkout link in Pancake flyover");
    		}
    		else
    		{
    			fail("Checkout page is not displayed on tapping StoreLocator Checkout Link in Pancake flyover");
    		}
    	}
    	
    	catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
    }
    
    public void BNIA165_KIOSK()
    {
    	ChildCreation("1. BNIA-159 Verify that while selecting the hamburger menu option at the header in all the pages,it should display the category menu page,"
    			+ "2. BNIA-165 Verify that while selecting the categories and subcategories option in the category menu page,it should navigate to the respective page");
    	try
    	{
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement BrowseLink = BNBasicCommonMethods.findElement(driver, obj, "BrowseLink");
    		BrowseLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "Browsepage");
    		WebElement Browsepage = BNBasicCommonMethods.findElement(driver, obj, "Browsepage");
    		if(Browsepage.isDisplayed())
    		{
    			pass("browse page is displayed on tapping browse link in Pancake flyover");
    		}
    		else
    		{
    			fail("browse page is not displayed on tapping browse link in Pancake flyover");
    		}
    		
    		
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement SearchLink = BNBasicCommonMethods.findElement(driver, obj, "SearchLink");
    		SearchLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "SearchPage");
    		WebElement SearchPage = BNBasicCommonMethods.findElement(driver, obj, "SearchPage");
    		if(SearchPage.isDisplayed())
    		{
    			pass("SearchPage page is displayed on tapping Search link in Pancake flyover");
    		}
    		else
    		{
    			fail("SearchPage page is not displayed on tapping Search link in Pancake flyover");
    		}
    		
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement StoreLocatorLink = BNBasicCommonMethods.findElement(driver, obj, "StoreLocatorLink");
    		StoreLocatorLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "StoreLocatorSearch");
    		WebElement StoreLocatorSearch = BNBasicCommonMethods.findElement(driver, obj, "StoreLocatorSearch");
    		if(StoreLocatorSearch.isDisplayed())
    		{
    			pass("StoreLocator page is displayed on tapping StoreLocator link in Pancake flyover");
    		}
    		else
    		{
    			fail("StoreLocator page is not displayed on tapping StoreLocator link in Pancake flyover");
    		}
    		
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement GiftCardLink = BNBasicCommonMethods.findElement(driver, obj, "GiftCardLink");
    		GiftCardLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardCheckBtn");
    		WebElement GiftCardCheckBtn = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
    		if(GiftCardCheckBtn.isDisplayed())
    		{
    			pass("GiftCard page is displayed on tapping StoreLocator GiftCard link in Pancake flyover");
    		}
    		else
    		{
    			fail("GiftCard page is not displayed on tapping StoreLocator GiftCard Link in Pancake flyover");
    		}
    	}
    	
    	catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
    }
   
  	/*BNIA-166 Verify that user should be able to scroll the categories vertically in the authored category page*/
	public void BNIA166()
  	{
  		ChildCreation("BNIA-166 Verify that user should be able to scroll the categories vertically in the authored category page.");
  		try
  		{
  			Thread.sleep(1500);
  			BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
  			WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
  			HamburgerMenu.click();
  			Thread.sleep(1500);
  			BNBasicCommonMethods.waitforElement(wait, obj, "BrowseLink");
  			WebElement BrowseLink = BNBasicCommonMethods.findElement(driver, obj, "BrowseLink");
  			BrowseLink.click();
  			Thread.sleep(2000);
  			BNBasicCommonMethods.waitforElement(wait, obj, "MenuWalkingDeadhot");
  			WebElement MenuWalkingDeadhot = BNBasicCommonMethods.findElement(driver, obj,"MenuWalkingDeadhot");
  			BNBasicCommonMethods.scrolldown(MenuWalkingDeadhot, driver);
  			if(MenuWalkingDeadhot.isEnabled())
  			{
  				pass("Menu Page is scrolled vertically");
  			}
  			else
  			{
  				fail("Menu page is not scrolled vertically");
  			}
  			
  		}
  		
  		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  	
  	/*BNIA-222 Verify that on selecting pancake menu in the header, the pancake flyover should be displayed*/
	public void BNIA222()
  	{
  		ChildCreation("BNIA-222 Verify that on selecting pancake menu in the header, the pancake flyover should be displayed.");
  		
  		try
  		{
  			Thread.sleep(1500);
  			BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
  			WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
  			if(HamburgerMenu.isDisplayed())
  			{
  				log.add("Hamburger menu is displayed");
  				HamburgerMenu.click();
  				Thread.sleep(1500);
  				WebElement PancakeFlyover = BNBasicCommonMethods.findElement(driver, obj, "PancakeFlyover");
  				if(PancakeFlyover.isDisplayed())
  				{
  					pass("Pancake Flyover is Displayed on selecting hamburger menu",log);
  				}
  				else
  				{
  					fail("Pacake flyover is not displayed on selecting the hamburger menu",log);
  				}
  			}
  			else
  			{
  				fail("Hamburger menu is not displayed");
  			}
  		}
  		
  		catch(Exception e)
  		{
  			exception(e.getMessage());
  		}
  		
  	}
  
	/*BNIA-223 Verify that pancake flyover should display the following menu : "Browse Catalogue", "Search", "Scan Items", "Store Map", "Store Locator", "Check Gift card Balance", "Open Instaply", "Checkout"*/
    public void BNIA223()
    {
    	ChildCreation("BNIA-223 Verify that pancake flyover should display the following menu : Browse Catalogue, Search, Scan Items, Store Map, Store Locator, Check Gift card Balance, Open Instaply, Checkout");
    	try
    	{
    		/*WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();*/
    		Thread.sleep(1000);
    		List <WebElement> PancakeList = driver.findElements(By.xpath("//*[@class='sk_additionalLinksContainer ']"));
    		for(int ctr = 2 ; ctr < PancakeList.size()+1 ; ctr++)
    		{
    			WebElement NamesInPancake = driver.findElement(By.xpath("(//*[@class='sk_additionalLinksContainer '])["+ctr+"]"));
    			//System.out.println(NamesInPancake.getText().toString());
    			log.add(NamesInPancake.getText());
    		}
    		pass("Name on the Pancake flyover are displayed",log);
    	}
    	catch(Exception e)
		{
    		System.out.println(e.getMessage());
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
    }
  	
    /*BNIA-227 Verify that when "Browse Catalogue" menu in pancake flyover is clicked then it should navigate to Browse Page*/
    public void BNIA227()
    {
    	ChildCreation("BNIA-227 Verify that when Browse Catalogue menu in pancake flyover is clicked then it should navigate to Browse Page");
    	try
    	{
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement BrowseLink = BNBasicCommonMethods.findElement(driver, obj, "BrowseLink");
    		BrowseLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "Browsepage");
    		WebElement Browsepage = BNBasicCommonMethods.findElement(driver, obj, "Browsepage");
    		if(Browsepage.isDisplayed())
    		{
    			Flag227 = true;
    			pass("browse page is displayed on tapping browse link in Pancake flyover");
    		}
    		else
    		{
    			fail("browse page is not displayed on tapping browse link in Pancake flyover");
    		}
    		
    	}
    	
    	catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
    }
    
  	/*BNIA-228 Verify that when "Search" menu is clicked then it should navigate to Search Page*/
    public void BNIA228()
    {
    	ChildCreation("BNIA-228 Verify that when Search menu is clicked then it should navigate to Search Page");
    	try
    	{
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement SearchLink = BNBasicCommonMethods.findElement(driver, obj, "SearchLink");
    		SearchLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "SearchPage");
    		WebElement SearchPage = BNBasicCommonMethods.findElement(driver, obj, "SearchPage");
    		if(SearchPage.isDisplayed())
    		{
    			Flag228 = true;
    			pass("SearchPage page is displayed on tapping Search link in Pancake flyover");
    		}
    		else
    		{
    			fail("SearchPage page is not displayed on tapping Search link in Pancake flyover");
    		}
    		
    	}
    	
    	catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
    }

  	/*BNIA-231 Verify that when "Store Locator" menu is clicked then it should navigate to Store Locator Page*/
    public void BNIA231()
    {
    	ChildCreation("BNIA-231 Verify that when Store Locator menu is clicked then it should navigate to Store Locator Page.");
    	try
    	{
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement StoreLocatorLink = BNBasicCommonMethods.findElement(driver, obj, "StoreLocatorLink");
    		StoreLocatorLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "StoreLocatorSearch");
    		WebElement StoreLocatorSearch = BNBasicCommonMethods.findElement(driver, obj, "StoreLocatorSearch");
    		if(StoreLocatorSearch.isDisplayed())
    		{
    			Flag231 = true;
    			pass("StoreLocator page is displayed on tapping StoreLocator link in Pancake flyover");
    		}
    		else
    		{
    			fail("StoreLocator page is not displayed on tapping StoreLocator link in Pancake flyover");
    		}
    		
    	}
    	
    	catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
    }

  	/*BNIA-232 Verify that when "Check Gift Card Balance" menu is clicked then it should navigate to Gift Card Page*/
    public void BNIA232()
    {
    	ChildCreation("BNIA-232 Verify that when Check Gift Card Balance menu is clicked then it should navigate to Gift Card Page.");
    	try
    	{
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement GiftCardLink = BNBasicCommonMethods.findElement(driver, obj, "GiftCardLink");
    		GiftCardLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "GiftCardCheckBtn");
    		WebElement GiftCardCheckBtn = BNBasicCommonMethods.findElement(driver, obj, "GiftCardCheckBtn");
    		if(GiftCardCheckBtn.isDisplayed())
    		{
    			Flag232 = true;
    			pass("GiftCard page is displayed on tapping StoreLocator GiftCard link in Pancake flyover");
    		}
    		else
    		{
    			fail("GiftCard page is not displayed on tapping StoreLocator GiftCard Link in Pancake flyover");
    		}
    		
    	}
    	
    	catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
    }
        
  	/*BNIA-234 Verify that when "Checkout" menu is clicked then it should navigate to Checkout Page*/
    public void BNIA234()
    {
    	ChildCreation("BNIA-234 Verify that when Checkout menu is clicked then it should navigate to Checkout Page.");
    	try
    	{
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");
    		WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(2000);
    		WebElement CheckoutLink = BNBasicCommonMethods.findElement(driver, obj, "CheckoutLink");
    		CheckoutLink.click();
    		Thread.sleep(1000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "EmptyContinueBtn");
    		WebElement EmptyContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "EmptyContinueBtn");
    		if(EmptyContinueBtn.isDisplayed())
    		{
    			Flag234 = true;
    			pass("Checkout page is displayed on tapping StoreLocator Checkout link in Pancake flyover");
    		}
    		else
    		{
    			fail("Checkout page is not displayed on tapping StoreLocator Checkout Link in Pancake flyover");
    		}
    		
    	}
    	
    	catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
    }
     
  	/* BNIA-235 Verify that on tapping "Close" icon in the pancake flyover,the pancake flyover should be closed*/
    public void BNIA235()
    {
    	ChildCreation("BNIA-235 Verify that on tapping Close icon in the pancake flyover,the pancake flyover should be closed");
    	try
    	{
    		//Thread.sleep(1500);
    		/*BNBasicCommonMethods.waitforElement(wait, obj, "HamburgerMenu");*/
    		/*WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();*/
    		Thread.sleep(1000);
    		BNBasicCommonMethods.findElement(driver, obj, "PancakeCloseIcon");
    		WebElement PancakeCloseIcon = BNBasicCommonMethods.findElement(driver, obj, "PancakeCloseIcon");;
    		if(PancakeCloseIcon.isDisplayed())
    		{
    			log.add("Pancake close icon is displayed");
    			//Thread.sleep(2000);
    			PancakeCloseIcon.click();
    			try
    			{
    				WebElement BrowseLink = BNBasicCommonMethods.findElement(driver, obj, "PancakeFlyover");
    				Thread.sleep(2000);
    				BrowseLink.click();
    				//Thread.sleep(2000);
    				fail("Pancake flyover is not closed");
    			}
    			catch(Exception e)
    			{
    				pass("Pancake flyover is closed");
    			}
    		}
    		else
    		{
    			fail("Pancake close icon is not displayed");
    		}
    	}
    	
    	catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
    	
    }

  	/*BNIA-236 Verify that on tapping outside the pancake flyover, the flyover should be closed*/
    public void BNIA236()
  	{
  		ChildCreation("BNIA-236 Verify that on tapping outside the pancake flyover, the flyover should be closed.");
  		
  		try
  		{
  			WebElement HamburgerMenu = BNBasicCommonMethods.findElement(driver, obj, "HamburgerMenu");
    		HamburgerMenu.click();
    		Thread.sleep(1000);
  		    WebElement SearchMask = BNBasicCommonMethods.findElement(driver, obj, "SearchMask");
  		    SearchMask.click();
  		    try
			 {
				WebElement BrowseLink = BNBasicCommonMethods.findElement(driver, obj, "BrowseLink");
				BrowseLink.click();
				fail("Pancake flyover is not closed");
			 }
			catch(Exception e)
			{
				pass("Pancake flyover is closed");
			}
  		 
  		}
  		
  		catch(Exception e)
		{
			exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}

/******************************************************** (Product List Page)PLP **********************************************************************/
 //Initial Launch of the Page and Clearing the Sign In fields.
   //Initial Launch of the Page and Clearing the Sign In fields.
   @Test(priority=10)
    public void BNIA10_PLP_Page() throws Exception
    {
  	   //TempSignIn();
  	   BNIA308();
  	   BNIA309();
  	   BNIA310();
  	   BNIA311();
  	   BNIA327();
  	   BNIA328();
  	   BNIA312();
  	   BNIA313();
  	   BNIA314();
  	   BNIA315();
  	   BNIA316();
  	   BNIA317();
  	   BNIA318();
  	   BNIA319();
  	   BNIA320();
  	   BNIA321();
  	   BNIA322();
  	   BNIA323();
  	   BNIA326();
  	   BNIA330();
  	   BNIA331();
  	   BNIA332();
  	   BNIA333();
  	   BNIA339();
  	   BNIA340();
  	   BNIA342();
  	   BNIA343();
  	   //BNIA344(); - Image Not Available Test Case
  	   BNIA345();
  	   BNIA529();
  	   //TemSignOut();
  	 TempSessionTimeout();
   }

    
    
    public void BNIA308() throws Exception 
    {
   	ChildCreation("BNIA308 - Verify that while selecting the categories in the Browse all categories page,the product list page should be displayed");
   	try
   	{
   		BNBasicCommonMethods.waitforElement(wait, obj, "BrowseTile");
   	 	WebElement browse_tile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
   	 	browse_tile.click();
   	 	//Thread.sleep(3000);
   	 	BNBasicCommonMethods.waitforElement(wait, obj, "browsepage");
   	 	WebElement browsepg = BNBasicCommonMethods.findElement(driver, obj, "browsepage");
   	 	wait.until(ExpectedConditions.visibilityOf(browsepg));
   	 	List<WebElement> browse_pg = driver.findElements(By.xpath("//*[contains(@class, 'hotSpotOverlay')]"));
   	 	/*for(int j = 1; j<=browse_pg.size();j++)
   	 	{
   	 		String name = driver.findElements(By.xpath("//*[contains(@class, 'hotSpotOverlay')]")).get(j).getAttribute("class");
   	 		System.out.println(name.replace("hotSpotOverlay", ""));
   	 	}*/
   	 	Random ran = new Random();
   	 	int sel = ran.nextInt(browse_pg.size());
   	 	sel = 5;
   	 	WebElement menu = driver.findElement(By.xpath("(//*[contains(@class, 'hotSpotOverlay')])["+sel+"]"));
   	 	menu.click();
   	 	//books_biography.click();
   	 	Thread.sleep(2000);
   	 	boolean facets = false;
   	 	facets = BNBasicCommonMethods.findElement(driver, obj, "PLP_facets").isDisplayed();
   	 	if(facets)
   	 	{
   	 		pass("PLP page is shown");
   	 	}
   	 	else
   	 	{
   	 		fail("PLP page is not shown"); 
   	 	}
   	}
   	catch(Exception e)
   	{
   		e.getMessage();
  		System.out.println("Something went wrong"+e.getMessage());
  		exception(e.getMessage());
   	}
    }
    
    public void BNIA309() throws InterruptedException 
   {
  	
  	ChildCreation("BNIA309 - Verify that on tapping any product in product list page,it should navigate to respective product detail page");
  	try
  	{
  		List<WebElement> PLP = driver.findElements(By.xpath("//*[@class = 'skMob_productImg showimg scTrack scLink']"));
  		Random r = new Random();
  		int new1 = r.nextInt(PLP.size());
  		WebElement plp_prd = driver.findElement(By.xpath("(//*[@class = 'skMob_productImg showimg scTrack scLink' ])["+new1+"]"));
  		//wait.until(ExpectedConditions.visibilityOf(plp_prd));
  		plp_prd.click();
  		Thread.sleep(1000);
  		//BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
  		
  		
  		boolean PrdName = false;
  		BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductName");
  		Thread.sleep(1000);
  		 PrdName = driver.findElement(By.xpath("//*[@class = 'prdName']")).isDisplayed();
  		//PDP_ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
  		if(PrdName)
  		{
  			pass("on tapping any product in product list page,it navigates to respective product detail page");
  		}
  		else
  		{
  			fail("on tapping any product in product list page,it does not navigates to respective product detail page");
  		}
  	}
  	catch(Exception e)
  	{
  		e.getMessage();
  		System.out.println("Something went wrong"+e.getMessage());
  		exception(e.getMessage());
  	}
  	//Thread.sleep(2000);
   }
  		/*//if(driver.findElement(By.xpath("//*[@class = 'pdpAddtoBagBtn']")).isDisplayed())
  		{
  			pass("on tapping any product in product list page,it navigates to respective product detail page");
  		}
  		else
  		{
  			fail("on tapping any product in product list page,it does not navigates to respective product detail page");
  		}
  	}
  	
  	catch(Exception e)
  	{
  		exception(e.getMessage());
  	}*/
   
  	
  	public void BNIA310() throws Exception
  	{
  		ChildCreation("BNIA310 - Verify that header in the product list page should have BN Logo, Search,Scan, Add to Bag, Pancake menu and  Back arrow icons");
  		try
  		{
  			BNBasicCommonMethods.findElement(driver, obj, "backarrow").click();
  			Thread.sleep(2000);
  			WebElement search_icon = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
  			WebElement shop_icon = BNBasicCommonMethods.findElement(driver, obj, "ShopIcon");
  			WebElement bckarrow = BNBasicCommonMethods.findElement(driver, obj, "backarrow");
  			WebElement pancake = BNBasicCommonMethods.findElement(driver, obj, "PancakeMenu");
  			WebElement scan = BNBasicCommonMethods.findElement(driver, obj, "ScanIcon");
  			
  			
  			if(((search_icon).isDisplayed()) && ((shop_icon).isDisplayed()) && ((bckarrow).isDisplayed()))
  			{
  			//pass("product list page have BN Logo, Search,Scan, Add to Bag, Pancake menu and  Back arrow icons");
  			if(((pancake).isDisplayed()) && ((scan).isDisplayed()))
  			{
  			pass("product list page have BN Logo, Search,Scan, Add to Bag, Pancake menu and  Back arrow icons");
  			}
  			else
  			{
  				fail("");
  			}
  			}
  			 else
  	     	{
  			fail("product list page does not have BN Logo, Search,Scan, Add to Bag, Pancake menu and  Back arrow icons");
  			}
  		 }
  			catch(Exception e)
  			{
  				e.getMessage();
  				System.out.println("Something went wrong"+e.getMessage());
  				exception(e.getMessage());
  			}
  		}
  		
  		
  	
  		
  		
  	public void BNIA311() throws InterruptedException
  	{
  		try
  		{
  			
  		ChildCreation("BNIA311 - Verify that  icons in the header should be in green color as per the creative");
  		sheet = BNBasicCommonMethods.excelsetUp("TopMenu navigation");
  	        BNBasicCommonMethods.waitforElement(wait, obj, "backarrow");
   		
  	    // To find Back button Color
  	    
  	    WebElement Backbtn = BNBasicCommonMethods.findElement(driver, obj, "backarrow");
   		String backarrowcolor = Backbtn.getCssValue("lighting-color");
   		//String Backarrowiconwidth = Backbtn.getCssValue("width");
   		//String BackarrowiconHeight = Backbtn.getCssValue("height");
   		Color backarw = Color.fromString(backarrowcolor);
   		String HexSignMaskColor = backarw.asHex();
   		
   		
   		
          // TO find Hamburger menu color 
   		WebElement hamburger = BNBasicCommonMethods.findElement(driver, obj, "PancakeMenu");
   		String hcolor=hamburger.getCssValue("lighting-color");
   		String hamburgerwidth = hamburger.getCssValue("width");
   		String hamburgerHeight = hamburger.getCssValue("height");
   		
   		Color hamcolor = Color.fromString(hcolor);
   		String hexhambuColor = hamcolor.asHex();
   		
   	     // To find Header logo color 
   		
   		WebElement BNHeader = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
   		String BNHeadercolor=BNHeader.getCssValue("lighting-color");
   		String BNHeaderwidth = BNHeader.getCssValue("width");
   		String BNHeaderHeight = BNHeader.getCssValue("height");
   		Color BNcolor = Color.fromString(BNHeadercolor);
   		String BNHeaderlogoColor = BNcolor.asHex();
   		
   		
   		 
   		//To find Scan icon color
   		
   		WebElement scanicon = BNBasicCommonMethods.findElement(driver, obj, "ScanIcon");
   		String scancolor = scanicon.getCssValue("lighting-color");
   		String scaniconwidth = scanicon.getCssValue("width");
   		String scaniconHeight = scanicon.getCssValue("height");
   		Color scanicolor = Color.fromString(scancolor);
   		String hexscanColor = scanicolor.asHex();
   		
   		
   	
   		//To find Search icon color
   		
   		WebElement Searchicon = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
   		String Searchcolor = Searchicon.getCssValue("lighting-color");
   		String Searchiconwidth = Searchicon.getCssValue("width");
   		String SearchiconHeight = Searchicon.getCssValue("height");
   		Color Searchiconcolor = Color.fromString(Searchcolor);
   		String hexSearchColor = Searchiconcolor.asHex();
   		
   		
   		
   		// To Find Shopping Bag icon color
   		
          WebElement shoppingbag = BNBasicCommonMethods.findElement(driver, obj,"ShopIcon");
          String shoppingicon=shoppingbag.getCssValue("lighting-color");
   		Color shop = Color.fromString(shoppingicon);
   		String shopColor = shop.asHex();
          String shoppingbagwidth = shoppingbag.getCssValue("width");
   		String shoppingbagHeight = shoppingbag.getCssValue("height");
   		
   		
   		//To Find Shopping bag count 
          WebElement ShoppingCount =  BNBasicCommonMethods.findElement(driver, obj, "ShoppingBagIcon");
          String shoppingiconcount=shoppingbag.getCssValue("lighting-color");
   		Color shopcount = Color.fromString(shoppingiconcount);
   		String shopColorcount = shopcount.asHex();
          String ShoppingCountwidth = ShoppingCount.getCssValue("width");
   		String ShoppingCountHeight = ShoppingCount.getCssValue("height");
   		String Shoppingcoutfont = ShoppingCount.getCssValue("font-family");
   		String ShoppingcoutSize = ShoppingCount.getCssValue("font-size");

   		//String ExpectedBackArrowheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120A", sheet, 4);
  		//String ExpectedBackArrowwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120A", sheet, 5);
  		String ExpectedBackArrowcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120A", sheet, 6);
   		
  		String ExpectedHamburgerheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120B", sheet, 4);
  		String ExpectedHamburgerwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120B", sheet, 5);
  		String ExpectedHamburgercolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120B", sheet, 6);

  		String ExpectedBNHeaderheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120C", sheet, 4);
  		String ExpectedBNHeaderwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120C", sheet, 5);
  		String ExpectedBNHeadercolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120C", sheet, 6);
  		
  		String ExpectedSearchheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120D", sheet, 4);
  		String ExpectedSearchwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120D", sheet, 5);
  		String ExpectedSearchcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120D", sheet, 6);
   	
  	 	
  		String ExpectedScanheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120E", sheet, 4);
  		String ExpectedScanwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120E", sheet, 5);
  		String ExpectedScancolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120E", sheet, 6);
  			
  		String ExpectedShoppingbagheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120F", sheet, 4);
  		String ExpectedShoppingbagwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120F", sheet, 5);
  		String ExpectedShoppingbagcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120F", sheet, 6);
   		
  		String ExpectedShoppingcountcolor = BNBasicCommonMethods.getExcelNumericVal("BNIA120G", sheet, 6);
  		String ExpectedShoppingcountheight = BNBasicCommonMethods.getExcelNumericVal("BNIA120G", sheet, 4);
  		String ExpectedShoppingcountwidth = BNBasicCommonMethods.getExcelNumericVal("BNIA120G", sheet, 5);
  		String ExpectedShoppingcountName = BNBasicCommonMethods.getExcelVal("BNIA120G", sheet, 7);
  		String ExpectedShoppingcountSize = BNBasicCommonMethods.getExcelNumericVal("BNIA120G", sheet, 8);
  		
  		//Separating font from font-family
  		String ShoppingBagCountFont = BNBasicCommonMethods.isFontName(Shoppingcoutfont);
  		
  		
  		//Back Arrorw Icon color
          
          if(HexSignMaskColor.equals(ExpectedBackArrowcolor))
  			pass("Back Arrow icon Color  Current : "+HexSignMaskColor+" Expected : "+ExpectedBackArrowcolor);
  		else
  			fail("Back Arrow icon Color  Current : "+HexSignMaskColor+" Expected : "+ExpectedBackArrowcolor);
  		
  		
  		//Hamburger menu Icon color
          
          if(hexhambuColor.equals(ExpectedHamburgercolor))
  			pass("Hamburger Icon Color  Current : "+hexhambuColor+" Expected : "+ExpectedHamburgercolor);
  		else
  			fail("Hamburger Icon Color  Current : "+hexhambuColor+" Expected : "+ExpectedHamburgercolor);
  		
  		
  		if(hamburgerHeight.equals(ExpectedHamburgerheight)&&hamburgerwidth.equals(ExpectedHamburgerwidth))
  			pass("Hamburger Icon Height and width Current : "+hamburgerwidth+" & "+hamburgerHeight+" Expected : "+ExpectedHamburgerwidth+" & "+ExpectedHamburgerheight);
  		else
  			fail("hamburger Icon Height and width Current : "+hamburgerwidth+" & "+hamburgerHeight+" Expected : "+ExpectedHamburgerwidth+" & "+ExpectedHamburgerheight);

   		
  		//B & N Logo Icon color
          
  		if(BNHeaderlogoColor.equals(ExpectedBNHeadercolor))
  			pass("B&N Header icon Color  Current : "+BNHeaderlogoColor+" Expected : "+ExpectedBNHeadercolor);
  		else
  			fail("B&N Header  icon Color  Current : "+BNHeaderlogoColor+" Expected : "+ExpectedBNHeadercolor);
  		
  		
  		if(BNHeaderHeight.equals(ExpectedBNHeaderheight)&&BNHeaderwidth.equals(ExpectedBNHeaderwidth))
  			pass("B&N Header Icon Height and width Current : "+BNHeaderwidth+" & "+BNHeaderHeight+" Expected : "+ExpectedBNHeaderwidth+" & "+ExpectedBNHeaderheight);
  		else
  			fail("B&N Header Icon Height and width Current : "+BNHeaderwidth+" & "+BNHeaderHeight+" Expected : "+ExpectedBNHeaderwidth+" & "+ExpectedBNHeaderheight);



  		//Search Icon color
          
  		if(hexSearchColor.equals(ExpectedSearchcolor))
  			pass("Search icon Color  Current : "+hexSearchColor+" Expected : "+ExpectedSearchcolor);
  		else
  			fail("Search icon Color  Current : "+hexSearchColor+" Expected : "+ExpectedSearchcolor);
  		
  		
  		if(SearchiconHeight.equals(ExpectedSearchheight)&&Searchiconwidth.equals(ExpectedSearchwidth))
  			pass("Search Icon Height and width Current : "+Searchiconwidth+" & "+SearchiconHeight+" Expected : "+ExpectedSearchwidth+" & "+ExpectedSearchheight);
  		else
  			fail("Search Icon Height and width Current : "+Searchiconwidth+" & "+SearchiconHeight+" Expected : "+ExpectedSearchwidth+" & "+ExpectedSearchheight);

  		
  		//Scan Icon color
          
  		if(hexscanColor.equals(ExpectedScancolor))
  			pass("Scan icon Color  Current : "+hexscanColor+" Expected : "+ExpectedScancolor);
  		else
  			fail("Scan icon Color  Current : "+hexscanColor+" Expected : "+ExpectedScancolor);
  		
  		if(scaniconHeight.equals(ExpectedScanheight)&&scaniconwidth.equals(ExpectedScanwidth))
  			pass("Scan Icon Height and width Current : "+scaniconwidth+" & "+scaniconHeight+" Expected : "+ExpectedScanwidth+" & "+ExpectedScanheight);
  		else
  			fail("Scan Icon Height and width Current : "+scaniconwidth+" & "+scaniconHeight+" Expected : "+ExpectedScanwidth+" & "+ExpectedScanheight);

  		
  		//Shopping Bag Icon color
          
  		if(shopColor.equals(ExpectedShoppingbagcolor))
  			pass("Shopping Bag icon Color  Current : "+shopColor+" Expected : "+ExpectedShoppingbagcolor);
  		else
  			fail("Shopping Bag icon Color  Current : "+shopColor+" Expected : "+ExpectedShoppingbagcolor);
  		
  		
  		if(shoppingbagHeight.equals(ExpectedShoppingbagheight)&&shoppingbagwidth.equals(ExpectedShoppingbagwidth))
  			pass("Shopping Bag Icon Height and width Current : "+shoppingbagwidth+" & "+shoppingbagHeight+" Expected : "+ExpectedShoppingbagwidth+" & "+ExpectedShoppingbagheight);
  		else
  			fail("Shopping Bag Icon Height and width Current : "+shoppingbagwidth+" & "+shoppingbagHeight+" Expected : "+ExpectedShoppingbagwidth+" & "+ExpectedShoppingbagheight);

  		//Shopping Bag Icon count color
          
  		if(shopColorcount.equals(ExpectedShoppingcountcolor))
  			pass("Shopping Bag icon count Color  Current : "+shopColorcount+" Expected : "+ExpectedShoppingcountcolor);
  		else
  			fail("Shopping Bag icon count Color  Current : "+shopColorcount+" Expected : "+ExpectedShoppingcountcolor);
  		
  		
  		if(ShoppingCountHeight.equals(ExpectedShoppingcountheight)&&ShoppingCountwidth.equals(ExpectedShoppingcountwidth))
  			pass("Shopping Bag Icon count Height and width Current : "+ShoppingCountwidth+" & "+ShoppingCountHeight+" Expected : "+ExpectedShoppingcountwidth+" & "+ExpectedShoppingcountheight);
  		else
  			fail("Shopping Bag Icon count Height and width Current : "+ShoppingCountwidth+" & "+ShoppingCountHeight+" Expected : "+ExpectedShoppingcountwidth+" & "+ExpectedShoppingcountheight);
  		
  		
  		if(ShoppingBagCountFont.equals(ExpectedShoppingcountName))
  			pass("Shopping Bag Icon count Font Name Current : "+ShoppingBagCountFont+" Expected : "+ExpectedShoppingcountName);
  		else
  			fail("Shopping Bag Icon count Font Name Current : "+ShoppingBagCountFont+" Expected : "+ExpectedShoppingcountName);
  		
  		
  		if(ShoppingcoutSize.equals(ExpectedShoppingcountSize))
  			pass("Shopping Bag Icon count Font Size Current : "+ShoppingcoutSize+" Expected : "+ExpectedShoppingcountSize);
  		else
  			fail("Shopping Bag Icon count Font Size Current : "+ShoppingcoutSize+" Expected : "+ExpectedShoppingcountSize);
      }
      
      catch (Exception e)
  	{
  		System.out.println(e.getMessage());
  	}
  }
  	
  	public void BNIA312() throws Exception
  	{
  		ChildCreation("BNIA312 - Verify that  BN Logo should be aligned center at the header in the product list page");
  		try
  		{
  			WebElement BNlogo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
  			BNlogo.getSize();
  			//System.out.println(" BNLogo Width and Height is "+BNlogo.getSize());
  			//System.out.println("Location points is "+BNlogo.getLocation());
  			String Loc = BNlogo.getLocation().toString();
  			String LogolocationWidthHeight = "(401, 23)";
  			if(LogolocationWidthHeight.equals(Loc))
  			 {
  				pass("BN Logo aligned center at the header in the product list page");
  	    	 }
  			else
  			{
  				fail("BN Logo is not aligned center at the header in the product list page");
  			}
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  		
  	}
  	
  	public void BNIA313() throws Exception
  	{
  	ChildCreation("BNIA313 - Verify that Back arrow icon should be shown in the top left most corner of the header except home page");
  	try
  	{
  		WebElement bckarrow = BNBasicCommonMethods.findElement(driver, obj, "backarrow");
  		BNBasicCommonMethods.waitforElement(wait, obj, "backarrow");
  		Thread.sleep(1000);
  		//BNBasicCommonMethods.findElement(driver, obj, "logoicon").click();
  		Thread.sleep(2000);
  		//WebElement bkarrow = BNBasicCommonMethods.findElement(driver, obj, "backarrow");
  		if((bckarrow).isDisplayed())
  		{
  			log.add("back arrow icon present");
  			pass("Back arrow icon shown in the top left most corner of the header except home page",log);
  		}
  		else
  		{
  			fail("Back arrow icon is not shown in the top left most corner of the header except home page",log);
  		}
  	}
  	catch(Exception e)
  	{
  		e.getMessage();
  		System.out.println("Something went wrong"+e.getMessage());
  		exception(e.getMessage());
  	}
  	
  	}
  	
  	public void BNIA314() throws Exception
  	{
  		ChildCreation("BNIA314 - Verify that Pancake icon should be displayed in top left corner of the header");
  		try
  		{
  			WebElement pan = BNBasicCommonMethods.findElement(driver, obj, "PancakeMenu");
  			//BNBasicCommonMethods.findElement(driver, obj, "logoicon");
  			Thread.sleep(1000);
  			if((pan).isDisplayed())
  			{
  				pass("PASS");
  			}
  			else
  			{
  				fail("Pancake icon is not displayed in top left corner of the header");
  			}
  			
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  		}
  		
  	}
  	
  	
  	public void BNIA315() throws Exception
  	{
  		ChildCreation("BNIA315 - Verify that Shopping Bag icon should be displayed in the top most right corner in the header");
  		try
  		{
  			WebElement shopicon = BNBasicCommonMethods.findElement(driver, obj, "ShopIcon");
  			if((shopicon).isDisplayed())
  			{
  			pass("Shopping Bag icon displayed in the top most right corner in the header");
  			}
  			else
  			{
  			fail("Shopping Bag icon is not displayed in the top most right corner in the header");
  			}
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  	}
  	
  	public void BNIA316() throws Exception
  	{
  		ChildCreation("BNIA316 - Verify that Scan icon should be shown at the top right corner in the header for the product list page");
  		try
  		{
  			WebElement scanicon = BNBasicCommonMethods.findElement(driver, obj, "ScanIcon");
  			if((scanicon).isDisplayed())
  			{
  			pass("Scan icon shown at the top right corner in the header for the product list page");
  			}
  			else
  			{
  			fail("Scan icon is not shown at the top right corner in the header for the product list page");
  			}
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  		}
  	}
  	
  	public void BNIA317() throws Exception
  	{
  		ChildCreation("BNIA317 - Verify that Search icon should be displayed at the header in the product list page ");
  		try
  		{
  			WebElement SearchIcon = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
  			if((SearchIcon).isDisplayed())
  			{
  			pass("Search icon is displayed at the header in the product list page");
  			}
  			else
  			{
  			fail("Search icon is not displayed at the header in the product list page");
  			}
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  		
  	
  	}
  	
  	public void BNIA318() throws Exception
  	{
  		ChildCreation("BNIA318 - Verify that while selecting the BN Logo at the header in the product list page,it should navigate to the home page");
  		try
  		{
  			WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
  			logo.click();
  			BNBasicCommonMethods.waitforElement(wait, obj, "Homepage");
  			//WebElement home = BNBasicCommonMethods.findElement(driver, obj, "Homepage");
  			
  			
  			if((BNBasicCommonMethods.findElement(driver, obj, "Homepage")).isDisplayed())
  			{
  				pass("while selecting the BN Logo at the header in the product list page,it navigates to the home page");
  			}
  			else
  			{
  				fail("while selecting the BN Logo at the header in the product list page,it does not navigates to the home page");
  			}
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  		
  	}
  	
  	public void BNIA319() throws Exception
  	{
  		ChildCreation("BNIA319 - Verify that while selecting the Pancake icon at the header in the product list page,the pancake flyover should be displayed ");
  		try
  		{
  			BNBasicCommonMethods.waitforElement(wait, obj, "BrowseTile");
  		 	WebElement browse_tile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
  		 	browse_tile.click();
  		 	BNBasicCommonMethods.waitforElement(wait, obj, "browsepage");
  		 	WebElement browsepg = BNBasicCommonMethods.findElement(driver, obj, "browsepage");
  		 	wait.until(ExpectedConditions.visibilityOf(browsepg));
  		 	List<WebElement> browse_pg = driver.findElements(By.xpath("//*[contains(@class, 'hotSpotOverlay')]"));
  		 	Random ran = new Random();
  		 	int sel = ran.nextInt(browse_pg.size());
  		 	sel = 5;
  		 	WebElement menu = driver.findElement(By.xpath("(//*[contains(@class, 'hotSpotOverlay')])["+sel+"]"));
  		 	menu.click();
  		 	Thread.sleep(2000);
  		 	/*boolean facets = false;
  		 	try
  		 	{
  		 		facets = driver.findElement(By.xpath("//*[@class = 'skMob_plpSelFacetsLabel']")).isDisplayed();
  		 		if(facets)
  		 		{
  		 			pass("PLP page is shown");
  		 		}
  		 		else
  		 		{
  		 			fail("PLP page is not shown"); 
  		 		}
  		 	}
  		 	catch(Exception e)
  		 	{
  		 		exception(e.getMessage());
  		 	}*/
  		 	WebElement pan = BNBasicCommonMethods.findElement(driver, obj, "PancakeMenu");
  		 	pan.click();
  		 	if((BNBasicCommonMethods.findElement(driver, obj, "Pancakeflyover")).isDisplayed())
  		 	{
  		 		pass("while selecting the Pancake icon at the header in the product list page,the pancake flyover is displayed");
  		 	}
  		 	else
  		 	{
  		 		fail("while selecting the Pancake icon at the header in the product list page,the pancake flyover is not displayed");
  		 	}
  		WebElement mask = BNBasicCommonMethods.findElement(driver, obj, "Mask");
  		mask.click();
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  		
   }
  	  
  	public void BNIA320() throws Exception
  	{
  		ChildCreation("BNIA320 - Verify that while selecting the Back arrow button at the header in the product list page,it should navigate to the previous page");
  		try
  		{
  			WebElement bckarrow = BNBasicCommonMethods.findElement(driver, obj, "backarrow");
  			bckarrow.click();
  			BNBasicCommonMethods.waitforElement(wait, obj, "browsepage");
  			WebElement browse_page = BNBasicCommonMethods.findElement(driver, obj, "browsepage");
  			if((browse_page).isDisplayed())
  			{
  				pass("while selecting the Back arrow button at the header in the product list page,it navigates to the previous page");
  			}
  			else
  			{
  				fail("while selecting the Back arrow button at the header in the product list page,it does not navigates to the previous page");
  			}
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  		
  	}

  	public void BNIA321() throws Exception
  	{
  		ChildCreation("BNIA321 - Verify that while selecting the Bag icon at the header,it should navigate to the Add to Bag page");
  		try
  		{
  			BNBasicCommonMethods.waitforElement(wait, obj, "ShopIcon");
  			WebElement bagicon = BNBasicCommonMethods.findElement(driver, obj, "ShopIcon");
  			bagicon.click();
  			BNBasicCommonMethods.waitforElement(wait, obj, "shoppingbag_emptypage");
  			boolean shoppage = false;
  			shoppage = BNBasicCommonMethods.findElement(driver, obj, "shoppingbag_emptypage").isDisplayed();
  			if(shoppage)
  			{
  				log.add("Shopping bag page is shown");
  				pass("while selecting the Bag icon at the header,it navigates to the Add to Bag page",log);
  			}
  			else
  			{
  				fail("while selecting the Bag icon at the header,it does not navigates to the Add to Bag page",log);
  			}
  			BNBasicCommonMethods.findElement(driver, obj, "backarrow");
  			Thread.sleep(1000);
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  		
  	}
  	
  	public void BNIA322() throws Exception
  	{
  		ChildCreation("BNIA322 - Verify that while selecting the Search icon at the header in the product list page,the search page should be displayed ");
  		try
  		{
  			WebElement search = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
  			search.click();
  			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
  			WebElement search_box = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
  			if((search_box).isDisplayed())
  			{
  				pass("while selecting the Search icon at the header in the product list page,the search page is displayed");
  			}
  			else
  			{
  				fail("while selecting the Search icon at the header in the product list page,the search page is not displayed");
  			}
  			Thread.sleep(2000);
  			search_box.sendKeys("cruise");
  			Thread.sleep(1000);
  			 WebElement suggestion = BNBasicCommonMethods.findElement(driver, obj, "searchsuggestion");
  			 suggestion.click();
  			 Thread.sleep(1000);
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  		
  	}
  		
  	public void BNIA323() throws Exception
  	{
  		ChildCreation("BNIA323 - Verify that  search results STRING Eg: search results for harper lee should be displayed at the top left in the product list page");
  		try
  		{
  			BNBasicCommonMethods.waitforElement(wait, obj, "searchresults");
  			WebElement SearchString = BNBasicCommonMethods.findElement(driver, obj, "searchresults");
  			 String search_string = SearchString.getText();
  			if(BNBasicCommonMethods.isElementPresent(SearchString))
  			{
  				log.add("The Search Result String is "+search_string);
  				pass("search results STRING Eg: search results for harper lee displayed at the top left in the product list page",log);
  			}
  			else
  			{
  				fail("search results STRING Eg: search results for harper lee is not displayed at the top left in the product list page");
  			}
  		}
  		catch(Exception e)
  		{
  			try
  			{
  				WebElement SearchKeywordAlert = BNBasicCommonMethods.findElement(driver, obj, "SearchKeywordNoProductFoundAlert");
  				if(SearchKeywordAlert.isDisplayed())
  				{
  					fail("Sorry, no results could be found. alert is shown");
  				}
  				else
  				{
  					return;
  				}
  			}
  			catch(Exception e1)
  			{
  				WebElement SearchEAN = BNBasicCommonMethods.findElement(driver, obj, "SearchEANNoProductFoundAlert");
  				if(SearchEAN.isDisplayed())
  				{
  					fail("The entered EAN is not working, No Product Found Alert is shown");
  				}
  				else
  				{
  					return;
  				}
  			}
  		}
  		
  		
  	}
  	
  	public void BNIA326() throws Exception
  	{
  		ChildCreation("BNIA326 - Verify that total number of product Total Product(XXX) text should be displayed at the top right corner in the product list page");
  		try
  		{
  			BNBasicCommonMethods.waitforElement(wait, obj, "PLP_productcount");
  			WebElement prdcount = BNBasicCommonMethods.findElement(driver, obj, "PLP_productcount");
  			String count = prdcount.getText();
  			
  			if(BNBasicCommonMethods.isElementPresent(prdcount))
  			{
  				log.add("The product count is "+count);
  				pass("total number of product Total Product(XXX) text displayed at the top right corner in the product list page",log);
 			}
 			else
  			{
  				fail("total number of product Total Product(XXX) text is not displayed at the top right corner in the product list page");
  			}
  			List<WebElement> PLP = driver.findElements(By.xpath("//*[@class = 'skMob_productImg showimg scTrack scLink']"));
  			for(int j = 0; j<PLP.size();j++)
  			 {
  			 	Thread.sleep(500);
  			 	JavascriptExecutor js = (JavascriptExecutor) driver;
  				js.executeScript("arguments[0].scrollIntoView(true);",PLP.get(j));
  			 }
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  	}
  	
  	public void BNIA327() throws InterruptedException
  	{
  		ChildCreation("BNIA327 - Verify that user should be able to scroll the products horizontally in the product list page");
  		try
  		{
  			List<WebElement> productListContainer = driver.findElements(By.xpath("//*[@class='skMob_productListItemOuterCont']"));
  		 	if(BNBasicCommonMethods.isListElementPresent(productListContainer))
  		 	{
  		 		log.add("PLP Page is Scrolled and the element is present");
  			for(int j = 0; j<productListContainer.size();j++)
  		 	{
  				BNBasicCommonMethods.scrollup(productListContainer.get(j), driver);
  		 		Thread.sleep(1000);
  		 		/*JavascriptExecutor js = (JavascriptExecutor) driver;
  				js.executeScript("arguments[0].scrollIntoView(true);",PLP.get(j));*/
  				
  		 	}
  			pass("user able to scroll the products horizontally in the product list page ",log);
  		 	}
  		 	else
  		 	{
  		 		fail("user is not able to scroll the products horizontally in the product list page",log);
  		 	}
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  		
  		
  	}
  	
  	public void BNIA328()
   	{
   		ChildCreation("BNIA-328 - Verify that  > or Show More icon should be shown after scrolling of 30 products in the product list page");
   	    try
   	    {
   	    	/*WebElement search = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
  			search.click();
  			WebElement search_box = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
  			search_box.sendKeys("dhoni");
   	    	//BNBasicCommonMethods.waitforElement(wait, obj, "Scrollmore");
   	  // boolean scrollmore_prd = false;*/
   	    	BNBasicCommonMethods.waitforElement(wait, obj, "Scrollmore");
   	WebElement scrollmore_prd1 = BNBasicCommonMethods.findElement(driver, obj, "Scrollmore");
  	 	
  	 		if(scrollmore_prd1.isDisplayed())
  	 		{
  	 			pass("Show More icon shown after scrolling of 30 products in the product list page");
  	 		}
  	 		else
  	 		{
  	 			fail("Show More icon is not shown after scrolling of 30 products in the product list page");
  	 		}
  	 	}
   	    catch(Exception e)
   	    {
   	    	e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
   	    }
   	    	
  	 	
   	}
  	 
  	public void BNIA330() throws Exception
  	{
  		ChildCreation("BNIA330 - Verify that > or See More icon should not shown when the total products in the product list page gets displayed");
  		try
  		{
  			
  			
  			BNBasicCommonMethods.findElement(driver, obj, "SearchIcon").click();
  			WebElement search_box = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
  			search_box.sendKeys("dhoni");
  			BNBasicCommonMethods.findElement(driver, obj, "searchsuggestion").click();
  			//boolean showmore = false;
  			//showmore = BNBasicCommonMethods.findElement(driver, obj, "Scrollmore").isDisplayed();
  			WebElement Scrollmore = BNBasicCommonMethods.findElement(driver, obj, "Scrollmore");
  			if(!BNBasicCommonMethods.isElementPresent(Scrollmore))
  			{
  				fail("> or See More icon is shown when the total products in the product list page gets displayed");
  			}
  			else
  			{
  				pass("> or See More icon is not shown when the total products in the product list page gets displayed");
  			}
  		}
  		catch(Exception e)
  		{
  			pass("Show More icon is not available for below 30 products in PLP section");
  			//e.getMessage();
  		}
  		
  	}
  	
  	
  	//Facets
  	/*public void BNIA331() throws Exception
  	{
  		ChildCreation("BNIA331 - Verify that  FILTER option should be displayed in the left side of product list page");
  		try
  		{
  			WebElement PLP_facets = BNBasicCommonMethods.findElement(driver, obj, "PLP_emptyfactes");
  		    boolean plpfacets = false;
  			
  			plpfacets = BNBasicCommonMethods.isElementPresent(PLP_facets);
  			if(plpfacets == true)
  			{
  				pass("FILTER option displayed in the left side of product list page");
  			}
  			else
  			{
  				fail("FILTER option are not displayed in the left side of product list page");
  			}
  				
  		}
  		
  		catch(Exception e)
  		{
  			System.out.println(e.getMessage());
  			exception(""+e.getMessage());
  		}
  	}*/
  	
  	
  	//Facets
  	/*public void BNIA332()
  	{
  		ChildCreation("BNIA332 - Verify that  FILTER section should display the filter options Subjects  Formats  Ages  with Sort By menu option");
  		List<WebElement> FilterOptions = driver.findElements(By.xpath("//*[@class = 'skMob_filterContentWrapper']"));
  		for(int i=1;i<=FilterOptions.size();i++)
  		{
  			String str = driver.findElement(By.xpath("(//*[@class='skMob_filterContentWrapper'])["+i+"]")).getText();
  			System.out.println(""+str);
  			if(driver.findElement(By.xpath("(//*[@class='skMob_filterContentWrapper'])["+i+"]")).isDisplayed())
  			{
  				log.add(""+str+" is shown");
  				pass("FILTER section displays the available filter options subjects  Formats  Ages  with Sort By menu option",log);
  			}
  			else
  			{
  				fail("FILTER section does not displays the available filter options subjects  Formats  Ages  with Sort By menu option");
  			}
  		}
  	}*/

  	
  	//Facets
  	/*public void BNIA333() throws Exception
  	{
  		ChildCreation("BNIA333 - Verify that on selecting the + icon in filter section,the filter section should be expanded");
  		try
  		{
  			WebElement plusicon = BNBasicCommonMethods.findElement(driver, obj, "plpfacets_plusicon");
  			plusicon.click();
  			boolean filteropen1;
  			WebElement filteropen = BNBasicCommonMethods.findElement(driver, obj, "plpfacets_open");
  			filteropen1 = BNBasicCommonMethods.isElementPresent(filteropen);
  			if(filteropen1 == true)
  			{
  				pass("on selecting the + icon in filter section,the filter section gets expanded");
  			}
  			else
  			{
  				fail("on selecting the + icon in filter section,the filter section does not gets expanded");
  			}
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  	}*/

  	public void BNIA334() throws Exception
  	{
  		ChildCreation("BNIA334 - Verify that  Show More text link should  shown in the FILTER section");
  		try
  		{
  			BNBasicCommonMethods.waitforElement(wait, obj, "showmore_link");
  			WebElement showmore = BNBasicCommonMethods.findElement(driver, obj, "showmore_link");
  			boolean show_more = false;
  			show_more = BNBasicCommonMethods.isElementPresent(showmore);
  			if(show_more)
  			{
  				pass("Show More text link should is shown in the FILTER section");
  			}
  			else
  			{
  				fail("Show More text link is not shown in the FILTER section");
  			}
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  		
  		
  	}
  	
  	public void BNIA335() throws Exception
  	{
  		ChildCreation("BNIA335 - Verify that  on tapping  Show More text link in FILTER section,the filter section should be expanded");
  		try
  		{
  			WebElement showmore = BNBasicCommonMethods.findElement(driver, obj, "showmore_link");
  			showmore.click();
  			WebElement showless = BNBasicCommonMethods.findElement(driver, obj, "showless_link");
  			boolean showles = BNBasicCommonMethods.isElementPresent(showless);
  			if(showles)
  			{
  				pass("On tapping Show More text link in FILTER section,the filter section gets expanded");
  			}
  			else
  			{
  				fail("On tapping Show More text link in FILTER section,the filter section does not gets expanded");
  			}
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  		
  		
  	}
  	
  	public void BNIA336() throws Exception
  	{
  		ChildCreation("BNIA336 - Verify that  on tapping Show Less text link in FILTER section it should be contracted");
  		try
  		{
  			WebElement showless = BNBasicCommonMethods.findElement(driver, obj, "showless_link");
  			showless.click();
  			BNBasicCommonMethods.waitforElement(wait, obj, "showmore1");
  			WebElement showmore = BNBasicCommonMethods.findElement(driver, obj, "showmore1");
  			boolean show_more = false;
  			show_more = BNBasicCommonMethods.isElementPresent(showmore);
  			if(show_more)
  			{
  				pass("on tapping Show Less text link in FILTER section gets contracted");
  			}
  			else
  			{
  				fail("on tapping Show Less text link in FILTER section does not gets contracted");
  			}
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  		
  	}
  	
  	//Facets
  	/*public void BNIA339() throws Exception
  	{
  		ChildCreation("BNIA339 - Verify that  in the FILTER section, the selected filtered items should be shown in the top of the FILTER section with the cross symbol");
  		try
  		{
  			WebElement plpfacet = driver.findElement(By.xpath("(//*[@class = 'skMob_filterContentsBtnIcon'])[2]"));
  			plpfacet.click();
  			Thread.sleep(2000);
  			WebElement selectfacet = driver.findElement(By.xpath("(//*[@class = 'skMob_filterItemContent'])[1]"));
  			selectfacet.click();
  			Thread.sleep(2000);
  			WebElement selectfilter = BNBasicCommonMethods.findElement(driver, obj, "selectedfilter");	
  			boolean selfilter = false;
  			selfilter = BNBasicCommonMethods.isElementPresent(selectfilter);
  			if(selfilter)
  			{
  				pass("the selected filtered items shown in the top of the FILTER section with the cross symbol");
  			}
  			else
  			{
  				fail("the selected filtered items is not shown in the top of the FILTER section with the cross symbol");
  			}
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  		
  	}*/
  	
  	public void BNIA343() throws Exception
  	{
  		ChildCreation("BNIA343 - Verify that product in the product list  page should shown its Product Image,Product Title and  Author details");
  		try
  		{
  			Thread.sleep(1500);
  			BNBasicCommonMethods.waitforElement(wait, obj, "plpprd_image");
  			WebElement plpprdimage = BNBasicCommonMethods.findElement(driver, obj, "plpprd_image");
  			BNBasicCommonMethods.waitforElement(wait, obj, "plpprd_title");
  			WebElement plpprdtitle = BNBasicCommonMethods.findElement(driver, obj, "plpprd_title");
  			BNBasicCommonMethods.waitforElement(wait, obj, "plpprd_autname");
  			WebElement plpprd_autname = BNBasicCommonMethods.findElement(driver, obj, "plpprd_autname");
  			/*boolean prd_image = false;
  			boolean prd_title = false;
  			boolean prd_autname = false;*/
  			/*prd_image = BNBasicCommonMethods.isElementPresent(plpprdimage);
  			prd_title = BNBasicCommonMethods.isElementPresent(plpprdtitle);
  			prd_autname = BNBasicCommonMethods.isElementPresent(plpprd_autname);*/
  			if(((plpprdimage).isDisplayed()) && ((plpprdtitle).isDisplayed()) && ((plpprd_autname).isDisplayed()))
  			{
  				pass("product in the product list  page shown its Product Image,Product Title and  Author details");
  			}
  			else
  			{
  				fail("product in the product list  page does not shown its Product Image,Product Title and  Author details");
  			}
  		}
  		
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  			
  	} 
  	
  	/*public void BNIA344() throws Exception
  	{
  		ChildCreation("BNIA344 - Verify that when the product in PLP page has no image then image not available should be shown");
  		try
  		{
  			BNBasicCommonMethods.findElement(driver, obj, "SearchIcon").click();
  			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
  			WebElement search = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
  			search.sendKeys("sachin");
  			search.sendKeys(Keys.ENTER);
  			Thread.sleep(1000);
  			
  			
  			//BNBasicCommonMethods.findElement(driver, obj, "searchsuggestion2").click();
  			//Thread.sleep(4000);
  			List<WebElement> PLP = driver.findElements(By.xpath("//*[@class = 'skMob_productImg showimg scTrack scLink']"));
  			for(int i=1;i<=PLP.size();i++)
  			{
  				WebElement scrollele = BNBasicCommonMethods.findElement(driver, obj, "(//*[@class='skMob_productImgDiv'])[20]");
  				BNBasicCommonMethods.scrolldown(scrollele, driver);
  			}
  			WebElement img_notavail = BNBasicCommonMethods.findElement(driver, obj, "plp_ImgNotAvail");
  			boolean noimage = false;
  			noimage = BNBasicCommonMethods.isElementPresent(img_notavail);
  			if(noimage)
  			{
  				pass("when the product in PLP page has no image then image not available is shown");
  			}
  			else
  			{
  				fail("when the product in PLP page has no image then image not available is not shown");
  			}
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  	}*/
  	
  	public void BNIA345() throws Exception
  	{
  		ChildCreation("BNIA-345 Verify that Font Size  Font Color Padding should be shown as per the creative");
  		try
  		{
  			String titlename_font = "Poynter_Semibold";
  			String title_size = "16px";
  			String titlename_color = "#000000";
  			String autname_font = "GillSansMTPro-Book";
  			String autname_size = "14px";
  			String authname_color = "#000000";
  			String showmore_font = "GillSansMTPro-Medium";
  			String showmore_size = "16px";
  			String showmore_color = "#346250";
  			String selectedfacet_font = "GillSansMTPro-Book";
  			String selectedfacet_size = "18px";
  			String selectedfacet_color = "#000000";
  			
  			BNBasicCommonMethods.waitforElement(wait, obj, "plpfacets_plusicon");
  			WebElement plusicon = BNBasicCommonMethods.findElement(driver, obj, "plpfacets_plusicon");
  			Thread.sleep(1000);
  			plusicon.click();
  			Thread.sleep(1000);
  			boolean filteropen1;
  			BNBasicCommonMethods.waitforElement(wait, obj, "plpfacets_open");
  			WebElement filteropen = BNBasicCommonMethods.findElement(driver, obj, "plpfacets_open");
  			filteropen1 = BNBasicCommonMethods.isElementPresent(filteropen);
  			if(filteropen1 == true)
  			{
  				pass("on selecting the + icon in filter section,the filter section gets expanded");
  			}
  			else
  			{
  				fail("on selecting the + icon in filter section,the filter section does not gets expanded");
  			}
  			BNBasicCommonMethods.waitforElement(wait, obj, "plpprd_title");
  			WebElement plpprd_title = BNBasicCommonMethods.findElement(driver, obj, "plpprd_title");
  			BNBasicCommonMethods.waitforElement(wait, obj, "plpprd_autname");
  			WebElement plpauthor_name = BNBasicCommonMethods.findElement(driver, obj, "plpprd_autname");
  			BNBasicCommonMethods.waitforElement(wait, obj, "showmore_link");
  			WebElement showmore = BNBasicCommonMethods.findElement(driver, obj, "showmore_link");
  			BNBasicCommonMethods.waitforElement(wait, obj, "plpprd_selectedfacet1");
  			WebElement selectedfacettext = BNBasicCommonMethods.findElement(driver, obj, "plpprd_selectedfacet1");
  			String plpprodtitle_size = plpprd_title.getCssValue("font-size");
  			String plpprdtitle_font = plpprd_title.getCssValue("font-family");
  			//System.out.println(""+plpprdtitle_font);
  			//System.out.println(""+plpprodtitle_size);
  			String plpprodtitle_color = plpprd_title.getCssValue("color");
  			Color plpprodtitlecolor = Color.fromString(plpprodtitle_color);
  			String hexplpprodtitlecolors = plpprodtitlecolor.asHex();
  			//System.out.println(""+hexplpprodtitlecolors);
  			
  			String plpauthorname_size = plpauthor_name.getCssValue("font-size");
  			String plpauthorname_font = plpauthor_name.getCssValue("font-family");
  			//System.out.println(""+plpauthorname_font);
  			//System.out.println(""+plpauthorname_size);
  			String plpauthorname_color = plpauthor_name.getCssValue("color");
  			Color plpauthornamecolor = Color.fromString(plpauthorname_color);
  			String hexplpauthornamecolors = plpauthornamecolor.asHex();
  			//System.out.println(""+hexplpauthornamecolors);
  			
  			String plpfactesShowMore_size = showmore.getCssValue("font-size");
  			String plpfactesShowMore_font = showmore.getCssValue("font-family");
  			//System.out.println(""+plpfactesShowMore_font);
  			//System.out.println(""+plpfactesShowMore_size);
  			String plpfactesShowMore_color = showmore.getCssValue("color");
  			Color plpfactesShowMorecolor = Color.fromString(plpfactesShowMore_color);
  			String hexplpfactesShowMorecolors = plpfactesShowMorecolor.asHex();
  			//System.out.println(""+hexplpfactesShowMorecolors);
  			
  			String plpselectedfacettext_size = selectedfacettext.getCssValue("font-size");
  			String plpselectedfacettext_font = selectedfacettext.getCssValue("font-family");
  			//System.out.println(""+plpselectedfacettext_font);
  			//System.out.println(""+plpselectedfacettext_size);
  			String plpselectedfacettext_color = selectedfacettext.getCssValue("outline-color");
  			Color plpselectedfacettextcolor = Color.fromString(plpselectedfacettext_color);
  			String hexselectedfacettextcolors = plpselectedfacettextcolor.asHex();
  			//System.out.println(""+hexselectedfacettextcolors);
  			
  			if((plpprdtitle_font.contains(titlename_font)) && (plpprodtitle_size.equals(title_size)) && (hexplpprodtitlecolors.equals(titlename_color)))
  			{
  				log.add("The Product Title font, size and color is "+"Current font name, size and color "+plpprdtitle_font +plpprodtitle_size +hexplpprodtitlecolors +"Expected font name, size and color is "+titlename_font +title_size +titlename_color);
  				pass(" Title font family, font size, font color is as per the creative",log);
  			}
  			else
  			{
  				log.add("The Product Title font, size and color is "+"Current font name, size and color "+plpprdtitle_font +plpprodtitle_size +hexplpprodtitlecolors +"Expected font name, size and color is "+titlename_font +title_size +titlename_color);
  				fail("font family, font size, font color is not as per the creative",log); 
  			}
  			if(plpauthorname_size.equals(autname_size) && (plpauthorname_font.contains(autname_font)) && (hexplpauthornamecolors.equals(authname_color)))
  			{
  				log.add("The Author Name font, size and color is "+"Current font name, size and color "+plpauthorname_font +plpauthorname_size +hexplpauthornamecolors +"Expected font name, size and color is "+autname_font +autname_size +authname_color);
  				pass("Author Name font, font size, font color is as per the creative",log);
  			}
  			else
  			{
  				log.add("The Author Name font, size and color is "+"Current font name, size and color "+plpauthorname_font +plpauthorname_size +hexplpauthornamecolors +"Expected font name, size and color is "+autname_font +autname_size +authname_color);
  				fail("Author Name font, font size, font color is not as per the creative",log);
  			}
  			if((plpfactesShowMore_font.contains(showmore_font)) && (plpfactesShowMore_size.equals(showmore_size)) && (hexplpfactesShowMorecolors.equals(showmore_color)))
  			{
  				log.add("The Show More Text font, size and color is "+"Current font name, size and color "+plpfactesShowMore_font +plpfactesShowMore_size +hexplpfactesShowMorecolors +"Expected font name, size and color is "+showmore_font +showmore_size +showmore_color);
  				pass("Show More Text font, font size, font color is as per the creative",log);
  			}
  			else
  			{
  				log.add("The Show More Text font, size and color is "+"Current font name, size and color "+plpfactesShowMore_font +plpfactesShowMore_size +hexplpfactesShowMorecolors +"Expected font name, size and color is "+showmore_font +showmore_size +showmore_color);
  				fail("Show More Text font, font size, font color is not as per the creative",log);
  			}
  			
  			if((plpselectedfacettext_font.contains(selectedfacet_font)) && (plpselectedfacettext_size.equals(selectedfacet_size)) && (hexselectedfacettextcolors.equals(selectedfacet_color)))
  			{
  				log.add("The Product Title font, size and color is "+"Current font name, size and color "+plpselectedfacettext_font +plpselectedfacettext_size +hexselectedfacettextcolors +"Expected font name, size and color is "+titlename_font +title_size +titlename_color);
  				pass(" Title font family, font size, font color is as per the creative",log);
  			}
  			else
  			{
  				log.add("The Product Title font, size and color is "+"Current font name, size and color "+plpprdtitle_font +plpprodtitle_size +hexplpprodtitlecolors +"Expected font name, size and color is "+selectedfacet_font +selectedfacet_size +selectedfacet_color);
  				fail("font family, font size, font color is not as per the creative",log); 
  			}
  		}
  		
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  	}
  	
  	public void BNIA529() throws Exception
  	{
  		ChildCreation("BNIA529 - Verify that on tapping outside the PLP page, the hamburger menu should be closed.");
  		try
  		{
  			WebElement pan = BNBasicCommonMethods.findElement(driver, obj, "PancakeMenu");
  		 	pan.click();
  		 	Thread.sleep(1000);
  		 	WebElement mask = BNBasicCommonMethods.findElement(driver, obj, "Mask");
  			mask.click();
  			Thread.sleep(2000);
  			WebElement PLP_prd = BNBasicCommonMethods.findElement(driver, obj, "PLP");
  			boolean plp = BNBasicCommonMethods.isElementPresent(PLP_prd);
  			if(plp)
  			{
  				pass("On tapping outside the PLP page, the hamburger menu gets closed");
  			}
  			else
  			{
  				fail("On tapping outside the PLP page, the hamburger menu does not get closed");
  			}
  			BNBasicCommonMethods.findElement(driver, obj, "logoicon").click();
  			System.out.println("10/11 - BNIA10_PLP_Page is completed");
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong in 529"+e.getMessage());
  			exception(e.getMessage());
  		}
  	}
  	
  	
  	/*BNIA331 - Verify that  FILTER option should be displayed in the left side of product list page*/
  	public void BNIA331()
  	{
  		ChildCreation("BNIA331 - Verify that  FILTER option should be displayed in the left side of product list page");
  		try
  	 	{
  			int ctr=1;
  			do
  			{
  				Thread.sleep(3000);
  				BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
  				WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
  				logo.click();
  				BNBasicCommonMethods.waitforElement(wait, obj, "BrowseTile");
  		 	 	WebElement browse_tile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
  		 	 	browse_tile.click();
  		 	 	BNBasicCommonMethods.waitforElement(wait, obj, "browsepage");
  		 	 	WebElement browsepg = BNBasicCommonMethods.findElement(driver, obj, "browsepage");
  		 	 	wait.until(ExpectedConditions.visibilityOf(browsepg));
  		 	 	List<WebElement> browse_pg = driver.findElements(By.xpath("//*[contains(@class, 'hotSpotOverlay')]"));
  		 	 	Random ran = new Random();
  		   	 	int sel = ran.nextInt(browse_pg.size());
  		   	 	WebElement menu = driver.findElement(By.xpath("(//*[contains(@class, 'hotSpotOverlay')])["+sel+"]"));
  		   	    JavascriptExecutor js = (JavascriptExecutor) driver;
  				js.executeScript("arguments[0].scrollIntoView(true);",menu);
  				menu = driver.findElement(By.xpath("(//*[contains(@class, 'hotSpotOverlay')])["+sel+"]"));
  				Thread.sleep(4000);
  		   	 	menu.click();
  		   	 	try
  		   	 	{
  			   	 	Thread.sleep(4500);
  			   	 	boolean facets = false;
  			   	 	BNBasicCommonMethods.waitforElement(wait, obj, "PLP_emptyfactes");
  			   	 	facets = BNBasicCommonMethods.findElement(driver, obj, "PLP_emptyfactes").isDisplayed();
  				   	 if(facets)
  				   	 {
  				   		 FacetFlag331 = true;
  				   	 	log.add("PLP facets is shown");
  				   	 	pass("FILTER option is displayed in the left side of product list page",log);
  				   	 	ctr=3;
  				   	 	Thread.sleep(2500);
  				     }
  				   	 else
  				   	 {
  				   		 FacetFlag331 = false;
  				   	 	log.add("PLP facets is not found");
  				   	 	fail("FILTER option is not displayed in the left side of product list page",log);
  				   	 }
  		   	 	}
  		   	 	catch(Exception e)
  		   	 	{
  		   	 		String url = driver.getCurrentUrl();
  			   	 	if(url.contains("pdp?"))
  			   	 	{
  			   	 		WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
  			   	 	    BNLogo.click();
  			   	 	    Thread.sleep(1000);
  			   	 	}
  		   	 	}
  		   	}while(ctr<=2);
  	 	}	
           catch(Exception e)
  		{
          	 e.getMessage();
   			System.out.println("Something went wrong"+e.getMessage());
   			exception(e.getMessage());
  		}
  	}
  	
  	/*BNIA332 - Verify that  FILTER section should display the filter options Availability  Subjects  Formats  Ages  with Sort By menu option*/
  	public void BNIA332() throws InterruptedException
  	{
  		ChildCreation("BNIA332 - Verify that  FILTER section should display the filter options Availability  Subjects  Formats  Ages  with Sort By menu option");
  		try
  		{
  			Thread.sleep(2500);
  	  		List<WebElement> FilterOptions = driver.findElements(By.xpath("//*[@class = 'skMob_filterContentWrapper']"));
  	  		for(int i=1;i<=FilterOptions.size();i++)
  	  		{
  	  			String str = driver.findElement(By.xpath("(//*[@class='skMob_filterContentWrapper'])["+i+"]")).getText();
  	  			//System.out.println(""+str);
  	  			if(driver.findElement(By.xpath("(//*[@class='skMob_filterContentWrapper'])["+i+"]")).isDisplayed())
  	  			{
  	  				FacetFlag332 = true;
  	  				log.add(""+str+" is shown");
  	  				pass("FILTER section displays the available filter options subjects  Formats  Ages  with Sort By menu option",log);
  	  			}
  	  			else
  	  			{
  	  				FacetFlag332 = false;
  	  				fail("FILTER section does not displays the available filter options subjects  Formats  Ages  with Sort By menu option");
  	  			}
  	  		}
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
   			System.out.println("Something went wrong"+e.getMessage());
   			exception(e.getMessage());
  		}
  		
  	}
  	
  	/*BNIA333 - Verify that on selecting the + icon in filter section,the filter section should be expanded*/
  	/*BNIA337 - Verify that  when the + icon is tapped in the Filter section then it should be expanded and + icon should  changed to the - icon*/
  	/*BNIA334 - Verify that  Show More text link should  shown in the FILTER section*/
  	/*BNIA335 - Verify that  on tapping  Show More text link in FILTER section,the filter section should be expanded*/
  	/*"BNIA-336 Verify that on tapping Show Less text link in FILTER section it should be contracted"*/
  	/*BNIA338 - Verify that  when the - icon is tapped in the Filter section then it should be contracted and - icon should be changed to + icon*/
  	public void BNIA333() throws Exception
  	{
  		ChildCreation("BNIA333 - Verify that on selecting the + icon in filter section,the filter section should be expanded");
  		int sel = 0;
  		//int ctr=1;
  		try
  			{
  				Random r = new Random();
  				List<WebElement> plusicon = driver.findElements(By.xpath("//*[@class = 'skMob_filterContentsBtnIcon']"));
  				sel = r.nextInt(plusicon.size());
  				plusicon.get(sel).click();
  				Thread.sleep(1500);
  				boolean filter_open;
  				WebElement filteropen = BNBasicCommonMethods.findElement(driver, obj, "plpfacets_open");
  				filter_open = BNBasicCommonMethods.isElementPresent(filteropen);
  				if(filter_open == true)
  				{
  					FacetFlag333 = true;
  					pass("on selecting the + icon in filter section,the filter section gets expanded");
  				}
  				else
  				{
  					FacetFlag333 = false;
  					fail("on selecting the + icon in filter section,the filter section does not gets expanded");
  				}
  			}
  			catch(Exception e)
  			{
  				e.getMessage();
  				System.out.println("Something went wrong in BNIA333\n"+e.getMessage());
  				exception(e.getMessage());
  			}
  		
  		/*BNIA337 - Verify that  when the + icon is tapped in the Filter section then it should be expanded and + icon should  changed to the - icon*/
  		ChildCreation("BNIA337 - Verify that  when the + icon is tapped in the Filter section then it should be expanded and + icon should  changed to the - icon");
  		try
  		{
  			WebElement ActivePlusIcon = driver.findElement(By.xpath("(//*[@class = 'skMob_filterContentContainer']//*[@class = 'skMob_filterItemContainer skMob_filterOpen'])"));
  			wait.until(ExpectedConditions.attributeContains(ActivePlusIcon, "style", "block"));
  			boolean PlusOk = ActivePlusIcon.getAttribute("style").contains("block");
  			if(PlusOk==true)
  			{
  				FacetFlag337 = true;
  				pass("When the + icon is tapped in the Filter section then it gets expanded and + icon is changed to the - icon");
  			}
  			else
  			{
  				FacetFlag337 = false;
  				fail("When the + icon is tapped in the Filter section then it does not gets expanded and + icon is not changed to the - icon");
  			}
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong in BNIA333\n"+e.getMessage());
  			exception(e.getMessage());
  		}
  		
  		/*BNIA334 - Verify that  Show More text link should  shown in the FILTER section*/
  		ChildCreation("BNIA334 - Verify that  Show More text link should  shown in the FILTER section");
  		    boolean show_more = false;
  		   // boolean activetab = false;
  		    ArrayList<Number> al1 = new ArrayList<>();
  		    do
  			{
  				try
  				{
  						//WebElement showmore = BNBasicCommonMethods.findElement(driver, obj, "showmore_link");
  						Thread.sleep(1000);
  						WebElement showmore  = driver.findElement(By.xpath("(//*[@class = 'skMob_showMoreTxt'])["+(sel+1)+"]"));
  						/*WebElement Activefacet = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(sel+1)+"]"));
  						activetab = Activefacet.getAttribute("style").contains("display: block");
  						if(activetab)
  						{*/
  						showmore.click();
  						Thread.sleep(1500);
  						al1.clear();
  						show_more = BNBasicCommonMethods.isElementPresent(showmore);
  						show_more=true;
  						if(show_more)
  						{
  							//ctr=4;
  							FacetFlag334 = true;
  							pass("Show More text link should is shown in the FILTER section");
  						}
  						else
  						{
  							FacetFlag334 = false;
  							fail("Show More text link is not shown in the FILTER section");
  						}
  						
  				}
  				catch(Exception e)
  				{
  					Random r = new Random();
  					List<WebElement> plusicon = driver.findElements(By.xpath("//*[@class = 'skMob_filterContentsBtnIcon']"));
  				    sel = r.nextInt(plusicon.size());
  					ArrayList<Number> al = new ArrayList<>();
  					while(al.contains(sel))
  					{
  						sel = r.nextInt(plusicon.size()-1);
  					}
  					al.add(sel);
  					plusicon.get(sel).click();
  					Thread.sleep(1500);
  				}
  				
  			}while(show_more!=true);
  				//BNIA335 - Verify that  on tapping  Show More text link in FILTER section,the filter section should be expanded
  			ChildCreation("BNIA335 - Verify that  on tapping  Show More text link in FILTER section,the filter section should be expanded");
  			boolean show_lessbool=false;
  			if(show_more == true)	
  			{
  				try
  				{
  					/*WebElement showmore  = driver.findElement(By.xpath("(//*[@class = 'skMob_showMoreTxt'])["+(sel+1)+"]"));
  					showmore.click();
  					Thread.sleep(1500);*/
  					WebElement showless = BNBasicCommonMethods.findElement(driver, obj, "showless_link");
  					boolean showles = BNBasicCommonMethods.isElementPresent(showless);
  					if(showles)
  					{
  						show_lessbool=true;
  						//System.out.println(show_lessbool);
  						FacetFlag335 = true;
  						pass("On tapping Show More text link in FILTER section,the filter section gets expanded");
  					}
  					else
  					{
  						System.out.println(show_lessbool);
  						FacetFlag335 = false;
  						fail("On tapping Show More text link in FILTER section,the filter section does not gets expanded");
  					}
  				}
  				catch(Exception e)
  				{
  					e.getMessage();
  					System.out.println("Something went wrong"+e.getMessage());
  					exception(e.getMessage());
  				}
  			}
  			
  			else
  			{
  				FacetFlag335 = false;
  				fail("Show More link is not availabe for the particular sort option");
  			}
  			
  			/*"BNIA-336 Verify that on tapping Show Less text link in FILTER section it should be contracted"*/
  			ChildCreation("BNIA-336 Verify that on tapping Show Less text link in FILTER section it should be contracted");
  			if(show_lessbool=true)
  				{
  					try
  					{
  						//WebElement showless = BNBasicCommonMethods.findElement(driver, obj, "showless_link");
  						Thread.sleep(1500);
  						WebElement showless =  driver.findElement(By.xpath("(//*[@class = 'skMob_showMoreFacets'])["+(sel+1)+"]"));
  						boolean showles = BNBasicCommonMethods.isElementPresent(showless);
  						Actions act = new Actions(driver);
  						for(int ctr=0 ; ctr<20 ;ctr++)
  						{
  							act.sendKeys(Keys.CONTROL).sendKeys(Keys.ARROW_DOWN).build().perform();
  						}
  						Thread.sleep(1000);
  						if(showles)
  						{
  							showless.click();
  							Thread.sleep(1500);
  							FacetFlag336 = true;
  							pass("On tapping Show Less Link in FILTER section,the filter section gets closed ");
  						}
  						else
  						{
  							FacetFlag336 = false;
  							fail("On tapping Show Less text link in FILTER section,the filter section does not gets Closed");
  						}
  					}
  					catch(Exception e)
  					{
  						e.getMessage();
  						System.out.println("Something went wrong"+e.getMessage());
  						exception(e.getMessage());
  					}
  				}
  				else
  				{
  					FacetFlag336 = false;
  					fail("show more link is not available for the particular sort option");
  				}
  			
  			/*BNIA338 - Verify that  when the - icon is tapped in the Filter section then it should be contracted and - icon should be changed to + icon*/
  			ChildCreation("BNIA338 - Verify that  when the - icon is tapped in the Filter section then it should be contracted and - icon should be changed to + icon");
  			try
  			{
  				Thread.sleep(1500);
  				List<WebElement> Facets_Open = driver.findElements(By.xpath("//*[@class = 'skMob_filterContentWrapper skMob_filterOpen']//*[@class = 'skMob_filterContentsBtnIcon']"));
		 	 	Random ran = new Random();
		   	 	int sele = ran.nextInt(Facets_Open.size());
		   	 	if(sele<1)
		   	 	{
		   	 		sele=sele+1;
		   	 	}
		   	 	WebElement Facets = driver.findElement(By.xpath("(//*[@class = 'skMob_filterContentWrapper skMob_filterOpen']//*[@class = 'skMob_filterContentsBtnIcon'])["+sele+"]"));
		   	 	Facets.click();
  				/*WebElement ActivePlusIcon = driver.findElement(By.xpath("(//*[@class = 'skMob_filterContentWrapper skMob_filterOpen']//*[@class = 'skMob_filterContentsBtnIcon'])["+sele+"]"));
  				ActivePlusIcon.click();*/
  				Thread.sleep(3500);
  				try
  				{
  					WebElement InActivePlus = driver.findElement(By.xpath("(//*[@class = 'skMob_filterContentContainer']//*[@class = 'skMob_filterItemContainer'])["+sele+"]"));
  					Thread.sleep(500);
  	  				//wait.until(ExpectedConditions.attributeContains(InActivePlus, "style", "none"));
  	  				boolean InactivePlusOk = InActivePlus.getAttribute("style").contains("none");
  	  				if(InactivePlusOk==true)
  	  				{
  	  					FacetFlag338 = true;
  	  					pass("When the - icon is tapped in the Filter section then it gets contracted and - icon is changed to + icon");
  	  				}
  	  				else
  	  				{
  	  					FacetFlag338 = false;
  	  					fail("When the - icon is tapped in the Filter section then it does not gets contracted and - icon is not changed to + icon");
  	  				}
  				}
  				catch(Exception e1)
  				{
  					Skip("There is no - icon to tap in the filter section");
  				}
  				
  			}
  			catch(Exception e1)
  			{
  				e1.getMessage();
  				System.out.println("Something went wrong"+e1.getMessage());
  				exception(e1.getMessage());
  			}
  			
  		}
  	
  	/*BNIA339 - Verify that  in the FILTER section, the selected filtered items should be shown in the top of the FILTER section with the cross symbol*/
  	public void BNIA339() throws Exception
  	{
  		ChildCreation("BNIA339 - Verify that  in the FILTER section, the selected filtered items should be shown in the top of the FILTER section with the cross symbol");
  		try
  		{
  			List<WebElement> SelectedFacets = driver.findElements(By.xpath("//*[@class = 'skMob_plpSelFacetCont']"));
  			//System.out.println(SelectedFacets);
  			//WebElement fa = BNBasicCommonMethods.findElement(driver, obj, "PLP_FilterContent");
  			//WebElement fa  = SelectedFacets.get(0); 
  			SelectedFacets.get(0);
  			for(int i=1;i<=SelectedFacets.size();i++)
  			{
  				String str = driver.findElement(By.xpath("(//*[@class = 'skMob_plpSelFacetCont'])["+i+"]")).getText();
  				//System.out.println(" "+str);
  				if(driver.findElement(By.xpath("(//*[@class='skMob_plpSelFacetCont'])["+i+"]")).isDisplayed())
  				{
  					FacetFlag339 = true;
  					log.add(""+str +"Facet is shown");
  					pass("In the FILTER section, the selected filtered items is shown in the top of the FILTER section with the cross symbol",log);
  				}
  				else
  				{
  					FacetFlag339 = false;
  					fail("In the FILTER section, the selected filtered items is not shown in the top of the FILTER section with the cross symbol");
  				}
  			}
  		}
  		catch(Exception e)
  		{
  			Random r = new Random();
  			List<WebElement> plusicon = driver.findElements(By.xpath("//*[@class = 'skMob_filterContentsBtnIcon']"));
  			int sel = r.nextInt(plusicon.size());
  			if(sel<=0)
  			{
  				sel=1;
  			}
  			plusicon.get(sel).click();
  			Thread.sleep(1500);
  			boolean filter_open;
  			WebElement filteropen = BNBasicCommonMethods.findElement(driver, obj, "plpfacets_open");
  			filter_open = BNBasicCommonMethods.isElementPresent(filteropen);
  			if(filter_open == true)
  			{
  				log.add("on selecting the + icon in filter section,the filter section gets expanded");
  			}
  			else
  			{
  				log.add("on selecting the + icon in filter section,the filter section does not gets expanded");
  			}
  			WebElement ExpandedFacetSection = driver.findElement(By.xpath("//*[@class = 'skMob_filterItemContainer skMob_filterOpen']"));
  			wait.until(ExpectedConditions.attributeContains(ExpandedFacetSection, "style", "block"));
  			boolean PlusOk = ExpandedFacetSection.getAttribute("style").contains("block");
  			if(PlusOk==true)
  			{
  				WebElement FacetsContent = driver.findElement(By.xpath("(//*[@class = 'skMob_filterItemContainer skMob_filterOpen']//*[@class = 'skMob_filterItemContent'])["+(sel+1)+"]"));
  				try
  				{
  					WebElement showmore  = driver.findElement(By.xpath("(//*[@class = 'skMob_showMoreTxt'])["+(sel+1)+"]"));
  					showmore.click();
  					Thread.sleep(1500);
  					FacetsContent.click();
  					Thread.sleep(2500);
  					List<WebElement> SelectedFacets = driver.findElements(By.xpath("//*[@class = 'skMob_plpSelFacetCont']"));
  					for(int i=1;i<=SelectedFacets.size();i++)
  					{
  						String str = driver.findElement(By.xpath("(//*[@class = 'skMob_plpSelFacetCont'])["+i+"]")).getText();
  						//System.out.println(""+str);
  						if(driver.findElement(By.xpath("(//*[@class='skMob_plpSelFacetCont'])["+i+"]")).isDisplayed())
  						{
  							FacetFlag339 = true;
  							log.add(""+str +"Facet is shown");
  							pass("In the FILTER section, the selected filtered items is shown in the top of the FILTER section with the cross symbol",log);
  						}
  						else
  						{
  							FacetFlag339 = false;
  							fail("In the FILTER section, the selected filtered items is not shown in the top of the FILTER section with the cross symbol");
  						}
  					}	
  				}
  				catch(Exception e1)
  				{
  					FacetsContent.click();
  					Thread.sleep(2000);
  					List<WebElement> SelectedFacets = driver.findElements(By.xpath("//*[@class = 'skMob_plpSelFacetCont']"));
  					for(int i=1;i<=SelectedFacets.size();i++)
  					{
  						String str = driver.findElement(By.xpath("(//*[@class = 'skMob_plpSelFacetCont'])["+i+"]")).getText();
  						//System.out.println(""+str);
  						if(driver.findElement(By.xpath("(//*[@class='skMob_plpSelFacetCont'])["+i+"]")).isDisplayed())
  						{
  							FacetFlag339 = true;
  							log.add(""+str +"Facet is shown");
  							pass("In the FILTER section, the selected filtered items is shown in the top of the FILTER section with the cross symbol",log);
  						}
  						else
  						{
  							FacetFlag339 = false;
  							fail("In the FILTER section, the selected filtered items is not shown in the top of the FILTER section with the cross symbol");
  						}
  					}	
  				}
  			}
  		}
  	}
  	
  	/*BNIA340 - Verify that cross symbol should be shown  in the left side of the selected filtered items*/
  	public void BNIA340()
  	{
  		ChildCreation("BNIA340 - Verify that cross symbol should be shown  in the left side of the selected filtered items");
  		String val = "return window.getComputedStyle(document.querySelector('.skMob_plpSelFacet'),':before').getPropertyValue('content')";
  		JavascriptExecutor js = (JavascriptExecutor)driver;
  		String content = (String) js.executeScript(val);
  		log.add("The Facet was Selected and the displayed value near the facet is : " +content);
  		if(content.contains("X"))
  		{
  			FacetFlag440 = true;
  			pass("The X mark is displayed.",log);
  		}
  		else
  		{
  			FacetFlag440 = false;
  			fail("The Facet was selected but there is mismatch in the symbol selected near the facet.",log);
  		}
  	}
  	
  	/*BNIA342 - Verify that  on tapping cross symbol in filtered items the product list page should be updated*/
  	public void BNIA342()
  	{
		ChildCreation("BNIA342 - Verify that  on tapping cross symbol in filtered items the product list page should be updated");
		try
		{
			//boolean selectedfacet= false;
			Random r = new Random();
			List<WebElement> SelectedFacets = driver.findElements(By.xpath("//*[@class = 'skMob_plpSelFacetCont']"));
			int sel = r.nextInt(SelectedFacets.size());
			if(sel<1)
			{
				sel=1;
			}
			WebElement Selfacet = driver.findElement(By.xpath("(//*[@class = 'skMob_plpSelFacetCont'])["+sel+"]"));
			String fac = Selfacet.getText();
			Thread.sleep(1000);
			Selfacet.click();
			//SelectedFacets.get(sel).click();
			Thread.sleep(5000);
			WebElement Selfacet1 = driver.findElement(By.xpath("(//*[@class = 'skMob_plpSelFacetCont'])["+sel+"]"));
			String str = Selfacet1.getText();
			//selectedfacet = driver.findElement(By.xpath("(//*[@class = 'skMob_plpSelFacetCont'])["+(sel)+"]")).isDisplayed();
			
			
			if(str.equals(fac))
			{
				FacetFlag342 = false;
				fail("On tapping cross symbol in filtered items the product list page does not gets updated");
			}
			else
			{
				FacetFlag342 = true;
				log.add("The Selected Factes is removed :" +fac);
				pass("On tapping cross symbol in filtered items the product list page gets updated",log);
			}
		}
		catch(Exception e)
		{
			Skip("There is no Selected Facets for the selected PLP");
		}
	}
  	
  	
  	
  	
  	/*BNIA768 - Verify that While selecting more Faceted sorts(reach product count 1),page navigates to respective PDP Page */
  	public void BNIA768()
  	{
  		ChildCreation("BNIA768 - Verify that While selecting more Faceted sorts(reach product count 1),page navigates to respective PDP Page ");
  		try
  		{
  			Thread.sleep(1500);
  			BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
  			WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
  			Thread.sleep(1500);
  			logo.click();
  			BNBasicCommonMethods.waitforElement(wait, obj, "BrowseTile");
  			WebElement Browsetile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
  			Thread.sleep(1500);
  			Browsetile.click();
  			BNBasicCommonMethods.waitforElement(wait, obj, "BrowsePage_FindingDoryLink");
  			WebElement Browse_FindingDory = BNBasicCommonMethods.findElement(driver, obj, "BrowsePage_FindingDoryLink");
  			Thread.sleep(1500);
  			Browse_FindingDory.click();
  			BNBasicCommonMethods.waitforElement(wait, obj, "PLP_FacetsContainer");
  			try
  			{
  				WebElement facets = driver.findElement(By.xpath("(//*[@class = 'skMob_filterContentWrapper'])[2]"));
  				facets.click();
  				Thread.sleep(500);
  				try
  				{
  					WebElement FacetSelected = driver.findElement(By.xpath("//*[@class = 'skMob_filterItemContainer skMob_filterOpen']//*[@class='skMob_filterItemContent' and contains(text(), 'Animation')]"));
  					FacetSelected.click();
  					Thread.sleep(2000);
  					WebElement Facets = driver.findElement(By.xpath("(//*[@class = 'skMob_filterContentWrapper'])[2]"));
  					Facets.click();
  					Thread.sleep(500);
  					ArrayList<String> productId = new ArrayList<String>();
  					List<WebElement> productListContainer = driver.findElements(By.xpath("//*[@class='skMob_productListItemOuterCont']"));
  					for(int j = 0;j<productListContainer.size();j++)
  				 	{
  				 		productId.add(productListContainer.get(j).getAttribute("prodid"));
  				 	} 
  					WebElement facetselected = driver.findElement(By.xpath("//*[@class = 'skMob_filterItemContainer skMob_filterOpen']//*[@class='skMob_filterItemContent' and contains(text(), 'Animation - Features')]"));
  					facetselected.click();
  					Thread.sleep(3500);
  					try
  					{
  						BNBasicCommonMethods.waitforElement(wait, obj, "PDPContainer");
  						String pdp = driver.getCurrentUrl();
  						String [] newstr = pdp.split("Param=");
  						String EANStr =  newstr[1];
  						if(pdp.contains("pdp?navParam="))
  						{
  							if(productId.contains(EANStr))
  							{
  							pass("While selecting more Faceted sorts(reach product count 1),page navigates to respective PDP Page");
  							}
  							else
  							{
  								System.out.println(EANStr);
  								fail("While selecting more Faceted sorts(reach product count 1),page not navigates to respective PDP Page");
  							}
  						}
  						else
  						{
  							fail("Page does not navigates to pdp");
  						}
  					}
  					catch(Exception e3)
  					{
  						Skip("On selecting facets, it does not navigates to PDP page");
  					}
  				}
  				catch(Exception e2)
  				{
  					Skip("Animation facet is not shown");
  				}
  			}
  			catch(Exception e1)
  			{
  				Skip("Subjects facets is not shown");
  			}
  			
  		}
  		catch(Exception e)
  		{
  			e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  		}
  	}

 /************************************************************** Product Description Page (PDP) **********************************************************************************/
	
  	//@Test(priority= 11)
	public void BNIA08_PDP_Page() throws Exception
	{
		TempSignIn();
		BNIA346();
		BNIA348();
		PDPLogic();
		BNIA418();
		BNIA419();
		BNIA420();
		BNIA411();
		BNIA347();
		BNIA349();
		BNIA350();
		BNIA351();
		BNIA354();
		BNIA353();
		BNIA352();
		BNIA355();
		BNIA356();
		BNIA357();
		BNIA359();
		BNIA360();
		BNIA361();
		BNIA363();
		BNIA364();
		BNIA365();
		BNIA366();
		BNIA367();
		BNIA573();
		BNIA368();
		BNIA369();
		BNIA370();
		BNIA371();
		BNIA372();
		BNIA373();
		BNIA374();
		BNIA375();
		BNIA376();
		BNIA377();
		BNIA378();
		BNIA380();
		BNIA381();
		BNIA382();
	    BNIA383();
		BNIA384();
		BNIA385();
		BNIA386();
		BNIA387();
		BNIA388();
		BNIA389();
		BNIA390();
		BNIA391();
		BNIA392();
		BNIA393();
		BNIA394();
		BNIA395();
		BNIA396();
		BNIA397();
		BNIA398();
		BNIA399();
		BNIA400();
		BNIA401();
		BNIA402();
		//BNIA403(); // not able to automate
		BNIA405();
		BNIA406();
		BNIA719();
		BNIA407();
		BNIA408();
		BNIA409();
		BNIA410();
		BNIA423();
		BNIA412();
		BNIA413();
		BNIA414();
		BNIA415();
		BNIA417();
		BNIA421();
		BNIA422();
		BNIA424();
		BNIA430();
		BNIA431();
		BNIA432();
		BNIA433();
		BNIA434();
		BNIA435();
		BNIA436();
		BNIA437();
		BNIA438();
		BNIA440();
		BNIA447();
		BNIA449();
		BNIA714();
		BNIA715();
		BNIA716();
		BNIA717();
		BNIA528();
		//BNIA561(); - Email Mask - need to check
		BNIA571();
		BNIA576();
		BNIA577();
		BNIA579();
		BNIA580();
		BNIA425();
		BNIA426();
		BNIA427();
		BNIA428();
		BNIA429();
		//TemSignOut();	
		TempSessionTimeout();
		
	}
	
	/*BNIA346 - Verify that  Product detail page should be displayed as per the creative*/
	public void BNIA346()
	{
		ChildCreation("BNIA346 - Verify that  Product detail page should be displayed as per the creative");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "SearchTile");
			BNBasicCommonMethods.findElement(driver, obj, "SearchTile").click();
			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			WebElement searchbox1 = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String EAN = BNBasicCommonMethods.getExcelNumericVal("BNIA346", sheet, 3);
			searchbox1.sendKeys(EAN);
			Thread.sleep(500);
			Actions actt = new Actions(driver);
		    actt.sendKeys(Keys.ENTER).build().perform();
			///searchbox1.sendKeys(Keys.ENTER);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductName");
			WebElement ProductTitle = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductName");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AuthorName");
			WebElement AuthorName = BNBasicCommonMethods.findElement(driver, obj, "PDP_AuthorName");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_Reviews");
			WebElement Reviews = BNBasicCommonMethods.findElement(driver, obj, "PDP_Reviews");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SelectedFormatText");
			WebElement SelectedFormatText = BNBasicCommonMethods.findElement(driver, obj, "PDP_SelectedFormatText");
			WebElement SalePrice = BNBasicCommonMethods.findElement(driver, obj, "PDP_SalePrice");
			WebElement ATBButton = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
			WebElement EmailButton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			WebElement OverviewLinkText = BNBasicCommonMethods.findElement(driver, obj, "PDPOverviewLink");
			/*WebElement CustomerAlsoBoughtText = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomeralsoBoughtLink");
			WebElement CustomerReviewsTitle1 = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsTitle");
			WebElement customeralsobought = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomeralsoBoughtLink");*/
			//WebElement customerreview = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerReviewsLink");
			Actions act = new Actions(driver);
			SelectedFormatText.click();
			for(int ctr=0 ; ctr<50 ;ctr++)
			{
				act.sendKeys(Keys.CONTROL).sendKeys(Keys.ARROW_RIGHT).build().perform();
			}
			//BNBasicCommonMethods.scrollup(customerreview, driver);
			
			/*String url = driver.getCurrentUrl();
			driver.get(url);
			Thread.sleep(2000);
			customerreview = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerReviewsLink");
			BNBasicCommonMethods.scrollup(customerreview, driver);*/
			Thread.sleep(3500);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerAlsoBoughtThisSection_Products1");
			WebElement CustomerBoughtThisAlsoBoughtProductName = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerAlsoBoughtThisSection_Products1");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerAlsoBoughtThisSection_AuthorName1");
			WebElement CustomerBoughtThisAlsoBoughtAuthorName = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerAlsoBoughtThisSection_AuthorName1");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerReviewsTitle");
			WebElement CustomerReviewsTitle = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsTitle");
			
			//WebElement CustomerReviewsText = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsText1");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_EditorialReviews_PublisherText");
			WebElement EditorialReviewsPublisherText = BNBasicCommonMethods.findElement(driver, obj, "PDP_EditorialReviews_PublisherText");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductDetails_Text1");
			WebElement ProductDetails_Text = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductDetails_Text1");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_RelatedSubjects_Text1");
			WebElement RelatedSubjects_Text = BNBasicCommonMethods.findElement(driver, obj, "PDP_RelatedSubjects_Text1");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String title = BNBasicCommonMethods.getExcelVal("BNIA346", sheet, 4);
			String Title_Values[] = title.split("\n");
			String color = BNBasicCommonMethods.getExcelVal("BNIA346", sheet, 6);
			String Color_Values[] = color.split("\n");
			String size = BNBasicCommonMethods.getExcelVal("BNIA346", sheet, 5);
			String Size_Values[] = size.split("\n");
			
			//Product Title
			
			String Prodtitlename_font = Title_Values[0];
			String Prodtitle_size = Size_Values[0];
			String Prodtitlename_color = Color_Values[0];
			
			String PDPprdtitle_font = ProductTitle.getCssValue("font-family");
			String PDPprodtitle_size = ProductTitle.getCssValue("font-size");
			//System.out.println(""+PDPprdtitle_font);
			//System.out.println(""+PDPprodtitle_size);
			String PDPprodtitle_color = ProductTitle.getCssValue("color");
			Color PDPprodtitlecolor = Color.fromString(PDPprodtitle_color);
			String hexPDPprodtitlecolors = PDPprodtitlecolor.asHex();
			//System.out.println(""+hexPDPprodtitlecolors);
			
			if((PDPprdtitle_font.contains(Prodtitlename_font)) && (PDPprodtitle_size.equals(Prodtitle_size)) && (hexPDPprodtitlecolors.equals(Prodtitlename_color)))
			{
				log.add("The PDP Product Title Text font, size and color is "+"Current font name, size and color "+PDPprdtitle_font +PDPprodtitle_size +hexPDPprodtitlecolors +"Expected font name, size and color is "+Prodtitlename_font +Prodtitle_size +Prodtitlename_color);
				pass("PDP Product Title font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The PDP Product Title Text font, size and color is "+"Current font name, size and color "+PDPprdtitle_font +PDPprodtitle_size +hexPDPprodtitlecolors +"Expected font name, size and color is "+Prodtitlename_font +Prodtitle_size +Prodtitlename_color);
				fail("PDP Product Title font, font size, font color is not as per the creative",log);
			}	
			
			//Author Name
			String PDPAuthorname_font = Title_Values[1];
			String PDPAuthorname_size = Size_Values[1];
			String PDPAuthorname_color = Color_Values[0];
			String PDPAuthornamefont = AuthorName.getCssValue("font-family");
			String PDPAuthornamesize = AuthorName.getCssValue("font-size");
		//	System.out.println(""+PDPAuthornamefont);
		//	System.out.println(""+PDPAuthorname_size);
			String PDPAuthorname_size_color = AuthorName.getCssValue("color");
			Color PDPAuthorname_sizecolor = Color.fromString(PDPAuthorname_size_color);
			String hexPDPAuthorname_sizecolors = PDPAuthorname_sizecolor.asHex();
	//		System.out.println(""+hexPDPAuthorname_sizecolors);
			
			if((PDPAuthornamefont.contains(PDPAuthorname_font)) && (PDPAuthornamesize.equals(PDPAuthorname_size)) && (hexPDPAuthorname_sizecolors.equals(PDPAuthorname_color)))
			{
				log.add("The PDP Author Name Text font, size and color is "+"Current font name, size and color "+PDPAuthornamefont +PDPAuthornamesize +hexPDPAuthorname_sizecolors +"Expected font name, size and color is "+PDPAuthorname_font +PDPAuthorname_size +PDPAuthorname_color);
				pass("PDP Author Name font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The Author Name Text font, size and color is "+"Current font name, size and color "+PDPAuthornamefont +PDPAuthornamesize +hexPDPAuthorname_sizecolors +"Expected font name, size and color is "+PDPAuthorname_font +PDPAuthorname_size +PDPAuthorname_color);
				fail("PDP Author Name font, font size, font color is not as per the creative",log);
			}
			
			//Reviews
			String PDPReviews_font = Title_Values[1];
			String PDPReviews_size = Size_Values[1];
			String PDPReviews_color = Color_Values[0];
			//String PDPReviews_lineheight = "16px";
			
			String PDPReviewsfont = Reviews.getCssValue("font-family");
			String PDPReviewssize = Reviews.getCssValue("font-size");
			//String PDPReviewslineheight = Reviews.getCssValue("line-height");
		//	System.out.println(""+PDPReviewsfont);
			//System.out.println(""+PDPReviewssize);
			//System.out.println(""+PDPReviewslineheight);
			String PDPReviews_size_color = Reviews.getCssValue("color");
			Color PDPReviews_sizecolor = Color.fromString(PDPReviews_size_color);
			String hexPDPReviews_sizecolors = PDPReviews_sizecolor.asHex();
			//System.out.println(""+hexPDPReviews_sizecolors);
			
			if((PDPReviewsfont.contains(PDPReviews_font)) && (PDPReviewssize.equals(PDPReviews_size)) && (hexPDPReviews_sizecolors.equals(PDPReviews_color)))
			{
				log.add("The PDP Reviews Text font, size and color is "+"Current font name, size and color "+PDPReviewsfont +PDPReviewssize +hexPDPReviews_sizecolors +"Expected font name, size and color is "+PDPReviews_font +PDPReviews_size +PDPReviews_color);
				pass("PDP Reviews font, font size, font color is as per the creative",log);
				/*if(PDPReviewslineheight.equals(PDPReviews_lineheight))
				{
					log.add("The PDP Reviews Text Line Height is "+"Current "+PDPReviewslineheight +"Expected "+PDPReviews_lineheight);
					pass("PDP Reviews Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP Reviews Text Line Height is "+"Current "+PDPReviewslineheight +"Expected "+PDPReviews_lineheight);
					fail("PDP Reviews Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Reviews Text font, size and color is "+"Current font name, size and color "+PDPReviewsfont +PDPReviewssize +hexPDPReviews_sizecolors +"Expected font name, size and color is "+PDPReviews_font +PDPReviews_size +PDPReviews_color);
				fail("PDP Reviews font, font size, font color is not as per the creative",log);
			}
			
			//SelectedFormatText
			String PDPSelectedFormatText_font = Title_Values[2];
			String PDPSelectedFormatText_size = Size_Values[1];
			String PDPSelectedFormatText_color = Color_Values[0];
			//String PDPSelectedFormatText_lineheight = "28px";
			//String lineheight = "normal";
			
			String PDPSelectedFormatTextfont = SelectedFormatText.getCssValue("font-family");
			String PDPSelectedFormatTextsize = SelectedFormatText.getCssValue("font-size");
			//String PDPSelectedFormatTextlineheight = SelectedFormatText.getCssValue("line-height");
			//System.out.println(""+PDPSelectedFormatTextfont);
			//System.out.println(""+PDPSelectedFormatTextsize);
			//System.out.println(""+PDPSelectedFormatTextlineheight);
			String PDPSelectedFormatText_size_color = SelectedFormatText.getCssValue("color");
			Color PDPSelectedFormatText_sizecolor = Color.fromString(PDPSelectedFormatText_size_color);
			String hexPDPSelectedFormatText_sizecolors = PDPSelectedFormatText_sizecolor.asHex();
			//System.out.println(""+hexPDPSelectedFormatText_sizecolors);
			
			if((PDPSelectedFormatTextfont.contains(PDPSelectedFormatText_font)) && (PDPSelectedFormatTextsize.equals(PDPSelectedFormatText_size)) && (hexPDPSelectedFormatText_sizecolors.equals(PDPSelectedFormatText_color)))
			{
				log.add("The PDP Selected Format Text font, size and color is "+"Current font name, size and color "+PDPSelectedFormatTextfont +PDPSelectedFormatTextsize +hexPDPSelectedFormatText_sizecolors +"Expected font name, size and color is "+PDPSelectedFormatText_font +PDPSelectedFormatText_size +PDPSelectedFormatText_color);
				pass("PDP Selected Format font, font size, font color is as per the creative",log);
				/*if(PDPSelectedFormatTextlineheight.equals(PDPSelectedFormatText_lineheight))
				{
					log.add("The PDP Selected Format Text Line Height is "+"Current "+PDPSelectedFormatTextlineheight +"Expected "+PDPSelectedFormatText_lineheight);
					pass("PDP Selected Format Line height is as per the creative",log);
				}
				else if(PDPSelectedFormatTextlineheight.equals(lineheight))
				{
					log.add("The PDP Selected Format Text Line Height is "+"Current "+PDPSelectedFormatTextlineheight +"Expected "+PDPSelectedFormatText_lineheight);
					fail("PDP Selected Format Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Selected Format Text font, size and color is "+"Current font name, size and color "+PDPSelectedFormatTextfont +PDPSelectedFormatTextsize +hexPDPSelectedFormatText_sizecolors +"Expected font name, size and color is "+PDPSelectedFormatText_font +PDPSelectedFormatText_size +PDPSelectedFormatText_color);
				fail("PDP Reviews font, font size, font color is not as per the creative",log);
			}
			
			
			//SalePrice
			String PDPSalePrice_font = Title_Values[2];
			String PDPSalePrice_size = Size_Values[2];
			String PDPSalePrice_color = Color_Values[0];
			//String PDPSalePrice_lineheight = "24px";
			
			String PDPSalePricefont = SalePrice.getCssValue("font-family");
			String PDPSalePricesize = SalePrice.getCssValue("font-size");
			//String PDPSalePricelineheight = SalePrice.getCssValue("line-height");
			//System.out.println(""+PDPSalePricefont);
			//System.out.println(""+PDPSalePricesize);
			//System.out.println(""+PDPSalePricelineheight);
			String PDPSalePrice_size_color = SalePrice.getCssValue("color");
			Color PDPSalePrice_sizecolor = Color.fromString(PDPSalePrice_size_color);
			String hexPDPSalePrice_sizecolors = PDPSalePrice_sizecolor.asHex();
			//System.out.println(""+hexPDPSalePrice_sizecolors);
			
			if((PDPSalePricefont.contains(PDPSalePrice_font)) && (PDPSalePricesize.equals(PDPSalePrice_size)) && (hexPDPSalePrice_sizecolors.equals(PDPSalePrice_color)))
			{
				log.add("The PDP Sale Price Text font, size and color is \n"+"Current font name, size and color \n"+PDPSalePricefont +"\n"+PDPSalePricesize+"\n" +hexPDPSalePrice_sizecolors+"\n"+"Expected font name, size and color is "+PDPSalePrice_font +PDPSalePrice_size +PDPSalePrice_color);
				pass("PDP Sale Price font, font size, font color is as per the creative",log);
				/*if(PDPSalePricelineheight.equals(PDPSalePrice_lineheight))
				{
					log.add("The PDP Sale Price Text Line Height is "+"Current "+PDPSalePricelineheight +"Expected "+PDPSalePrice_lineheight);
					pass("PDP Sale Price Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP Sale Price Text Line Height is "+"Current "+PDPSalePricelineheight +"Expected "+PDPSalePrice_lineheight);
					fail("PDP Sale Price Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Sale Price Text font, size and color is "+"Current font name, size and color "+PDPSalePricefont +PDPSalePricesize +hexPDPSalePrice_sizecolors +"Expected font name, size and color is "+PDPSalePrice_font +PDPSalePrice_size +PDPSalePrice_color);
				fail("PDP Sale Price font, font size, font color is not as per the creative",log);
			}
			
			//ATBButton
			String PDPATBButton_font = Title_Values[1];
			String PDPATBButton_size = Size_Values[3];
			String PDPATBButtonText_color = Color_Values[1];
			//String PDPATBButton_color = Color_Values[2];
			//String PDPATBButton_lineheight = "16px";
			
			String PDPATBButtonfont = ATBButton.getCssValue("font-family");
			String PDPATBButtonsize = ATBButton.getCssValue("font-size");
			//String PDPATBButtonlineheight = ATBButton.getCssValue("line-height");
			//System.out.println(""+PDPATBButtonfont);
			//System.out.println(""+PDPATBButtonsize);
			//System.out.println(""+PDPSalePricelineheight);
			String PDPATBButtonText1_color = ATBButton.getCssValue("color");
			Color PDPATBButtonTexcolor = Color.fromString(PDPATBButtonText1_color);
			String hexPDPATBButton_sizecolors = PDPATBButtonTexcolor.asHex();
		//	System.out.println(""+hexPDPATBButton_sizecolors);
			String PDPATBButton1_color = ATBButton.getCssValue("background-color");
			Color PDPATBButton_sizecolor = Color.fromString(PDPATBButton1_color);
			String hexPDPATBButton1_sizecolors = PDPATBButton_sizecolor.asHex();
			//System.out.println(""+hexPDPATBButton1_sizecolors);
			
			if((PDPATBButtonfont.contains(PDPATBButton_font)) && (PDPATBButtonsize.equals(PDPATBButton_size)) && (hexPDPATBButton_sizecolors.equals(PDPATBButtonText_color)))
			{
				log.add("The PDP ATB Button Text font, size and color is "+"Current font name, size and color "+PDPATBButtonfont +PDPATBButtonsize +hexPDPATBButton_sizecolors +"Expected font name, size and color is "+PDPATBButton_font +PDPATBButton_size +PDPATBButtonText_color);
				pass("PDP ATB Button font, font size, font color is as per the creative",log);
				/*if((PDPATBButtonlineheight.equals(PDPATBButton_lineheight)) && (hexPDPATBButton1_sizecolors.equals(PDPATBButton_color)))
				{
					log.add("The PDP ATB Button Text Line Height and background color is "+"Current "+PDPATBButtonlineheight +hexPDPATBButton1_sizecolors +"Expected "+PDPATBButton_lineheight +PDPATBButton_color);
					pass("PDP ATB Button Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP ATB Button Text Line Height and background color is "+"Current "+PDPATBButtonlineheight +hexPDPATBButton1_sizecolors +"Expected "+PDPATBButton_lineheight +PDPATBButton_color);
					fail("PDP ATB Button Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP ATB Button Text font, size and color is "+"Current font name, size and color "+PDPATBButtonfont +PDPATBButtonsize +hexPDPATBButton_sizecolors +"Expected font name, size and color is "+PDPATBButton_font +PDPATBButton_size +PDPATBButtonText_color);
				fail("PDP ATB Button font, font size, font color is not as per the creative",log);
			}
			
			//EmailButton
			String PDPEmailButton_font = Title_Values[1];
			String PDPEmailButton_size = Size_Values[1];
			String PDPEmailButton_color = Color_Values[3];
			//String PDPEmailButton_lineheight = "16px";
			String PDPEmailButtonBorder = Color_Values[3];
			
			String PDPEmailButtonfont = EmailButton.getCssValue("font-family");
			String PDPEmailButtonsize = EmailButton.getCssValue("font-size");
			//String PDPEmailButtonlineheight = SalePrice.getCssValue("line-height");
			String PDPEmailButtonBordercolor = EmailButton.getCssValue("border");
			String PDPEmailcolorRGBColor  = PDPEmailButtonBordercolor.replace("1px solid ", "");
			Color PDPbordercolor = Color.fromString(PDPEmailcolorRGBColor);
			String hexborder_colors = PDPbordercolor.asHex();
			//System.out.println(""+hexborder_colors);
			//System.out.println(""+PDPEmailButtonfont);
			//System.out.println(""+PDPEmailButtonsize);
			//System.out.println(""+PDPSalePricelineheight);
			String PDPEmailButton1_color = EmailButton.getCssValue("color");
			Color PDPEmailButtoncolor = Color.fromString(PDPEmailButton1_color);
			String hexPDPEmailButton_colors = PDPEmailButtoncolor.asHex();
			//System.out.println(""+hexPDPEmailButton_colors);
			
			if((PDPEmailButtonfont.contains(PDPEmailButton_font)) && (PDPEmailButtonsize.equals(PDPEmailButton_size)) && (hexPDPEmailButton_colors.equals(PDPEmailButton_color)))
			{
				log.add("The PDP EmailButton Text font, size and color is "+"Current font name, size and color "+PDPEmailButtonfont +PDPEmailButtonsize +hexPDPEmailButton_colors +"Expected font name, size and color is "+PDPEmailButton_font +PDPEmailButton_size +PDPEmailButton_color);
				pass("PDP Sale Price font, font size, font color is as per the creative",log);
				if((PDPEmailButtonBorder.contains(hexborder_colors)))
				{
					log.add("The PDP EmailButton Text Line Height and border color is "+"Current "+PDPEmailButtonBordercolor +"Expected "+PDPEmailButtonBorder);
					pass("PDP EmailButton Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP EmailButton Text Line Height and border color is "+"Current "+PDPEmailButtonBordercolor +"Expected " +PDPEmailButtonBorder);
					fail("PDP EmailButton Line height is not as per the creative",log);
				}
			}
			else
			{
				log.add("The PDP EmailButton Text font, size and color is "+"Current font name, size and color "+PDPEmailButtonfont +PDPEmailButtonsize +hexPDPEmailButton_colors +"Expected font name, size and color is "+PDPEmailButton_font +PDPEmailButton_size +PDPEmailButton_color);
				fail("PDP EmailButton font, font size, font color is not as per the creative",log);
			}
			
			
			//OverviewLinkText
			String PDPOverviewLinkText_font = Title_Values[2];
			String PDPOverviewLinkText_size = Size_Values[1];
			String PDPOverviewLinkText_color = Color_Values[3];
			
			String PDPOverviewLinkTextfont = OverviewLinkText.getCssValue("font-family");
			String PDPOverviewLinkTextsize = OverviewLinkText.getCssValue("font-size");
			//System.out.println(""+PDPOverviewLinkTextfont);
			//System.out.println(""+PDPOverviewLinkTextsize);
			String PDPOverviewLinkText_size_color = OverviewLinkText.getCssValue("color");
			Color PDPOverviewLinkText_sizecolor = Color.fromString(PDPOverviewLinkText_size_color);
			String hexPDPOverviewLinkText_sizecolors = PDPOverviewLinkText_sizecolor.asHex();
			//System.out.println(""+hexPDPOverviewLinkText_sizecolors);
			
			if((PDPOverviewLinkTextfont.contains(PDPOverviewLinkText_font)) && (PDPOverviewLinkTextsize.equals(PDPOverviewLinkText_size)) && (hexPDPOverviewLinkText_sizecolors.equals(PDPOverviewLinkText_color)))
			{
				log.add("The PDP OverviewLink Text font, size and color is "+"Current font name, size and color "+PDPOverviewLinkTextfont +PDPOverviewLinkTextsize +hexPDPOverviewLinkText_sizecolors +"Expected font name, size and color is "+PDPOverviewLinkText_font +PDPOverviewLinkText_size +PDPOverviewLinkText_color);
				pass("PDP OverviewLink font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The PDP OverviewLink Text font, size and color is "+"Current font name, size and color "+PDPOverviewLinkTextfont +PDPOverviewLinkTextsize +hexPDPOverviewLinkText_sizecolors +"Expected font name, size and color is "+PDPOverviewLinkText_font +PDPOverviewLinkText_size +PDPOverviewLinkText_color);
				fail("PDP OverviewLink font, font size, font color is not as per the creative",log);
			}
			
			
			//CustomerBoughtThisAlsoBoughtProductName
			
			String PDPCusBoughtPrdName_font = Title_Values[2];
			String PDPCusBoughtPrdName_size = Size_Values[3];
			String PDPCusBoughtPrdName_color = Color_Values[4];
			//String PDPCusBoughtPrdName_lineheight = "20px";
			
			String PDPCusBoughtPrdNamefont = CustomerBoughtThisAlsoBoughtProductName.getCssValue("font-family");
			String PDPCusBoughtPrdNamesize = CustomerBoughtThisAlsoBoughtProductName.getCssValue("font-size");
			//String PDPCusBoughtPrdNamelineheight = CustomerBoughtThisAlsoBoughtProductName.getCssValue("line-height");
		//	System.out.println(""+PDPCusBoughtPrdNamefont);
			//System.out.println(""+PDPCusBoughtPrdNamesize);
			//System.out.println(""+PDPCusBoughtPrdNamelineheight);
			String PDPCusBoughtPrdName_size_color = CustomerBoughtThisAlsoBoughtProductName.getCssValue("color");
			Color PDPCusBoughtPrdName_sizecolor = Color.fromString(PDPCusBoughtPrdName_size_color);
			String hexPDPCusBoughtPrdName_sizecolors = PDPCusBoughtPrdName_sizecolor.asHex();
			//System.out.println(""+hexPDPCusBoughtPrdName_sizecolors);
			
			if((PDPCusBoughtPrdNamefont.contains(PDPCusBoughtPrdName_font)) && (PDPCusBoughtPrdNamesize.equals(PDPCusBoughtPrdName_size)) && (hexPDPCusBoughtPrdName_sizecolors.equals(PDPCusBoughtPrdName_color)))
			{
				log.add("The PDP Customer bought Product Name Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtPrdNamefont +PDPCusBoughtPrdNamesize +hexPDPCusBoughtPrdName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtPrdName_font +PDPCusBoughtPrdName_size +PDPCusBoughtPrdName_color);
				pass("PDP Customer bought Product Name font, font size, font color is as per the creative",log);
				/*if(PDPCusBoughtPrdNamelineheight.equals(PDPCusBoughtPrdName_lineheight))
				{
					log.add("The PDP Customer bought Product Name Text Line Height is "+"Current "+PDPCusBoughtPrdNamelineheight +"Expected "+PDPCusBoughtPrdName_lineheight);
					pass("PDP Customer bought Product Name Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP Customer bought Product Name Text Line Height is "+"Current "+PDPCusBoughtPrdNamelineheight +"Expected "+PDPCusBoughtPrdName_lineheight);
					fail("PDP Customer bought Product Name Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Customer bought Product Name Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtPrdNamefont +PDPCusBoughtPrdNamesize +hexPDPCusBoughtPrdName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtPrdName_font +PDPCusBoughtPrdName_size +PDPCusBoughtPrdName_color);
				fail("PDP Customer bought Product Name font, font size, font color is not as per the creative",log);
			}
			
			
			//CustomerBoughtThisAlsoBoughtAuthorName
			
			String PDPCusBoughtAuthorName_font = Title_Values[1];
			String PDPCusBoughtAuthorName_size = Size_Values[1];
			String PDPCusBoughtAuthorName_color = Color_Values[5];
			//String PDPCusBoughtAuthorName_lineheight = "20px";
			
			String PDPCusBoughtAuthorNamefont = CustomerBoughtThisAlsoBoughtAuthorName.getCssValue("font-family");
			String PDPCusBoughtAuthorNamesize = CustomerBoughtThisAlsoBoughtAuthorName.getCssValue("font-size");
			//String PDPCusBoughtAuthorNamelineheight = CustomerBoughtThisAlsoBoughtAuthorName.getCssValue("line-height");
			//System.out.println(""+PDPCusBoughtAuthorNamefont);
			//System.out.println(""+PDPCusBoughtAuthorNamesize);
			//System.out.println(""+PDPCusBoughtAuthorNamelineheight);
			String PDPCusBoughtAuthorName_size_color = CustomerBoughtThisAlsoBoughtAuthorName.getCssValue("color");
			Color PDPCusBoughtAuthorName_sizecolor = Color.fromString(PDPCusBoughtAuthorName_size_color);
			String hexPDPCusBoughtAuthorName_sizecolors = PDPCusBoughtAuthorName_sizecolor.asHex();
			//System.out.println(""+hexPDPCusBoughtAuthorName_sizecolors);
			
			if((PDPCusBoughtAuthorNamefont.contains(PDPCusBoughtAuthorName_font)) && (PDPCusBoughtAuthorNamesize.equals(PDPCusBoughtAuthorName_size)) && (hexPDPCusBoughtAuthorName_sizecolors.equals(PDPCusBoughtAuthorName_color)))
			{
				log.add("The PDP Customer bought Author Name Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtAuthorNamefont +PDPCusBoughtAuthorNamesize +hexPDPCusBoughtAuthorName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtAuthorName_font +PDPCusBoughtAuthorName_size +PDPCusBoughtAuthorName_color);
				pass("PDP Customer bought Author Name font, font size, font color is as per the creative",log);
				/*if(PDPCusBoughtAuthorNamelineheight.equals(PDPCusBoughtAuthorName_lineheight))
				{
					log.add("The PDP Customer bought Author Name Text Line Height is "+"Current "+PDPCusBoughtAuthorNamelineheight +"Expected "+PDPCusBoughtAuthorName_lineheight);
					pass("PDP Customer bought Author Name Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP Customer bought Author Name Text Line Height is "+"Current "+PDPCusBoughtAuthorNamelineheight +"Expected "+PDPCusBoughtAuthorName_lineheight);
					fail("PDP Customer bought Author Name Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Customer bought Author Name Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtAuthorNamefont +PDPCusBoughtAuthorNamesize +hexPDPCusBoughtAuthorName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtAuthorName_font +PDPCusBoughtAuthorName_size +PDPCusBoughtAuthorName_color);
				fail("PDP Customer bought Author Name font, font size, font color is not as per the creative",log);
			}
			
			
			//CustomerReviewsTitle
			
			String PDPCustomerReviewTitle_font = Title_Values[0];
			String PDPCustomerReviewTitle_size = Size_Values[2];
			String PDPCustomerReviewTitle_color = Color_Values[0];
			//String PDPCustomerReviewTitle_lineheight = "27px";
			
			String PDPCustomerReviewTitlefont = CustomerReviewsTitle.getCssValue("font-family");
			String PDPCustomerReviewTitlesize = CustomerReviewsTitle.getCssValue("font-size");
			//String PDPCustomerReviewTitlelineheight = CustomerReviewsTitle.getCssValue("line-height");
			//System.out.println(""+PDPCustomerReviewTitlefont);
			//System.out.println(""+PDPCustomerReviewTitlesize);
			//System.out.println(""+PDPCustomerReviewTitlelineheight);
			String PDPCustomerReviewTitle_size_color = CustomerReviewsTitle.getCssValue("color");
			Color PDPCustomerReviewTitle_sizecolor = Color.fromString(PDPCustomerReviewTitle_size_color);
			String hexPDPCustomerReviewTitlee_sizecolors = PDPCustomerReviewTitle_sizecolor.asHex();
			//System.out.println(""+hexPDPCustomerReviewTitlee_sizecolors);
			
			if((PDPCustomerReviewTitlefont.contains(PDPCustomerReviewTitle_font)) && (PDPCustomerReviewTitlesize.equals(PDPCustomerReviewTitle_size)) && (hexPDPCustomerReviewTitlee_sizecolors.equals(PDPCustomerReviewTitle_color)))
			{
				log.add("The PDP CustomerReviewsTitle Text font, size and color is "+"Current font name, size and color "+PDPCustomerReviewTitlefont +PDPCustomerReviewTitlesize +hexPDPCustomerReviewTitlee_sizecolors +"Expected font name, size and color is "+PDPCustomerReviewTitle_font +PDPCustomerReviewTitle_size +PDPCustomerReviewTitle_color);
				pass("PDP CustomerReviewsTitlee font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP CustomerReviewsTitle Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtAuthorNamefont +PDPCusBoughtAuthorNamesize +hexPDPCusBoughtAuthorName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtAuthorName_font +PDPCusBoughtAuthorName_size +PDPCusBoughtAuthorName_color);
				fail("PDP CustomerReviewsTitle font, font size, font color is not as per the creative",log);
			}
			
			
			//CustomerReviewsText
			
			String PDPCustomerReviewText_font = Title_Values[0];
			String PDPCustomerReviewText_size = Size_Values[1];
			String PDPCustomerReviewText_color = Color_Values[0];
			//String PDPCustomerReviewText_lineheight = "22px";
			
			String PDPCustomerReviewTextfont = CustomerReviewsTitle.getCssValue("font-family");
			String PDPCustomerReviewTextsize = CustomerReviewsTitle.getCssValue("font-size");
			//String PDPCustomerReviewTextlineheight = CustomerReviewsTitle.getCssValue("line-height");
			//System.out.println(""+PDPCustomerReviewTextfont);
			//System.out.println(""+PDPCustomerReviewTextsize);
			//System.out.println(""+PDPCustomerReviewTextlineheight);
			String PDPCustomerReviewText_size_color = CustomerReviewsTitle.getCssValue("color");
			Color PDPCustomerReviewText_sizecolor = Color.fromString(PDPCustomerReviewText_size_color);
			String hexPDPCustomerReviewText_sizecolors = PDPCustomerReviewText_sizecolor.asHex();
			//System.out.println(""+hexPDPCustomerReviewText_sizecolors);
			
			if((PDPCustomerReviewTextfont.contains(PDPCustomerReviewText_font)) && (PDPCustomerReviewTextsize.equals(PDPCustomerReviewText_size)) && (hexPDPCustomerReviewText_sizecolors.equals(PDPCustomerReviewText_color)))
			{
				log.add("The PDP CustomerReviewsTitle Text font, size and color is "+"Current font name, size and color "+PDPCustomerReviewTextfont +PDPCustomerReviewTextsize +hexPDPCustomerReviewText_sizecolors +"Expected font name, size and color is "+PDPCustomerReviewText_font +PDPCustomerReviewText_size +PDPCustomerReviewText_color);
				pass("PDP CustomerReviewsTitle font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP CustomerReviewsTitle Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtAuthorNamefont +PDPCusBoughtAuthorNamesize +hexPDPCusBoughtAuthorName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtAuthorName_font +PDPCusBoughtAuthorName_size +PDPCusBoughtAuthorName_color);
				fail("PDP CustomerReviewsTitle font, font size, font color is not as per the creative",log);
			}
			
			
			//EditorialReviewsPublisherText
			String PDPEditorialReviewsPublisherText_font = Title_Values[2];
			String PDPEditorialReviewsPublisherText_size = Size_Values[3];
			String PDPEditorialReviewsPublisherText_color = Color_Values[6];
			//String PDPEditorialReviewsPublisherText_lineheight = "22px";
			
			String PDPEditorialReviewsPublisherTextfont = EditorialReviewsPublisherText.getCssValue("font-family");
			String PDPEditorialReviewsPublisherTextsize = EditorialReviewsPublisherText.getCssValue("font-size");
			//String PDPEditorialReviewsPublisherTextlineheight = EditorialReviewsPublisherText.getCssValue("line-height");
			//System.out.println(""+PDPEditorialReviewsPublisherTextfont);
			//System.out.println(""+PDPEditorialReviewsPublisherTextsize);
			//System.out.println(""+PDPEditorialReviewsPublisherTextlineheight);
			String PDPEditorialReviewsPublisherText_size_color = EditorialReviewsPublisherText.getCssValue("color");
			Color PDPEditorialReviewsPublisherText_sizecolor = Color.fromString(PDPEditorialReviewsPublisherText_size_color);
			String hexPDPEditorialReviewsPublisherText_sizecolors = PDPEditorialReviewsPublisherText_sizecolor.asHex();
			//System.out.println(""+hexPDPEditorialReviewsPublisherText_sizecolors);
			
			if((PDPEditorialReviewsPublisherTextfont.contains(PDPEditorialReviewsPublisherText_font)) && (PDPEditorialReviewsPublisherTextsize.equals(PDPEditorialReviewsPublisherText_size)) && (hexPDPEditorialReviewsPublisherText_sizecolors.equals(PDPEditorialReviewsPublisherText_color)))
			{
				log.add("The PDP Editorial Review Publisher Text font, size and color is "+"Current font name, size and color "+PDPEditorialReviewsPublisherTextfont +PDPEditorialReviewsPublisherTextsize +hexPDPEditorialReviewsPublisherText_sizecolors +"Expected font name, size and color is "+PDPEditorialReviewsPublisherText_font +PDPEditorialReviewsPublisherText_size +PDPEditorialReviewsPublisherText_color);
				pass("PDP Editorial Review Publisher font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP Editorial Review Publisher Text font, size and color is "+"Current font name, size and color "+PDPEditorialReviewsPublisherTextfont +PDPEditorialReviewsPublisherTextsize +hexPDPEditorialReviewsPublisherText_sizecolors +"Expected font name, size and color is "+PDPEditorialReviewsPublisherText_font +PDPEditorialReviewsPublisherText_size +PDPEditorialReviewsPublisherText_color);
				fail("PDP Editorial Review Publishere font, font size, font color is not as per the creative",log);
			}
			
			
			
			//ProductDetails_Text
			
			String PDPProductDetailsText_font = Title_Values[1];
			String PDPProductDetailsText_size = Size_Values[3];
			String PDPProductDetailsText_color = Color_Values[6];
			//String PDPProductDetailsText_lineheight = "22px";
			
			String PDPProductDetailsTextfont = ProductDetails_Text.getCssValue("font-family");
			String PDPProductDetailsTextsize = ProductDetails_Text.getCssValue("font-size");
			//String PDPProductDetailsTextlineheight = ProductDetails_Text.getCssValue("line-height");
			//System.out.println(""+PDPProductDetailsTextfont);
			//System.out.println(""+PDPProductDetailsTextsize);
			//System.out.println(""+PDPProductDetailsTextlineheight);
			String PDPProductDetailsText_size_color = ProductDetails_Text.getCssValue("color");
			Color PDPProductDetailsText_sizecolor = Color.fromString(PDPProductDetailsText_size_color);
			String hexPDPProductDetailsText_sizecolors = PDPProductDetailsText_sizecolor.asHex();
			//System.out.println(""+hexPDPProductDetailsText_sizecolors);
			
			if((PDPProductDetailsTextfont.contains(PDPProductDetailsText_font)) && (PDPProductDetailsTextsize.equals(PDPProductDetailsText_size)) && (hexPDPProductDetailsText_sizecolors.equals(PDPProductDetailsText_color)))
			{
				log.add("The PDP Product details Text font, size and color is "+"Current font name, size and color "+PDPProductDetailsTextfont +PDPProductDetailsTextsize +hexPDPProductDetailsText_sizecolors +"Expected font name, size and color is "+PDPProductDetailsText_font +PDPProductDetailsText_size +PDPProductDetailsText_color);
				pass("PDP Product details font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP Product details Text font, size and color is "+"Current font name, size and color "+PDPProductDetailsTextfont +PDPProductDetailsTextsize +hexPDPProductDetailsText_sizecolors +"Expected font name, size and color is "+PDPProductDetailsText_font +PDPProductDetailsText_size +PDPProductDetailsText_color);
				fail("PDP Product details font, font size, font color is not as per the creative",log);
			}
			
			
			//RelatedSubjects_Text
			
			String PDPRelatedSubjectsText_font = Title_Values[1];
			String PDPRelatedSubjectsText_size = Size_Values[4];
			String PDPRelatedSubjectsText_color = Color_Values[1];
			//String PDPRelatedSubjectsText_lineheight = "22px";
			
			String PDPRelatedSubjectsTextfont = RelatedSubjects_Text.getCssValue("font-family");
			String PDPRelatedSubjectsTextsize = RelatedSubjects_Text.getCssValue("font-size");
			//String PDPRelatedSubjectsTextlineheight = RelatedSubjects_Text.getCssValue("line-height");
			//System.out.println(""+PDPRelatedSubjectsTextfont);
			//System.out.println(""+PDPRelatedSubjectsTextsize);
			//System.out.println(""+PDPRelatedSubjectsTextlineheight);
			String PDPRelatedSubjectsText_size_color = RelatedSubjects_Text.getCssValue("color");
			Color PDPRelatedSubjectsText_sizecolor = Color.fromString(PDPRelatedSubjectsText_size_color);
			String hexPDPRelatedSubjectsText_sizecolors = PDPRelatedSubjectsText_sizecolor.asHex();
			//System.out.println(""+hexPDPRelatedSubjectsText_sizecolors);
			
			if((PDPRelatedSubjectsTextfont.contains(PDPRelatedSubjectsText_font)) && (PDPRelatedSubjectsTextsize.equals(PDPRelatedSubjectsText_size)) && (hexPDPRelatedSubjectsText_sizecolors.equals(PDPRelatedSubjectsText_color)))
			{
				log.add("The PDP Related Subject Text font, size and color is "+"Current font name, size and color "+PDPRelatedSubjectsTextfont +PDPRelatedSubjectsTextsize +hexPDPRelatedSubjectsText_sizecolors +"Expected font name, size and color is "+PDPRelatedSubjectsText_font +PDPRelatedSubjectsText_size +PDPRelatedSubjectsText_color);
				pass("PDP Related Subject font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP Related Subject Text font, size and color is "+"Current font name, size and color "+PDPRelatedSubjectsTextfont +PDPRelatedSubjectsTextsize +hexPDPRelatedSubjectsText_sizecolors +"Expected font name, size and color is "+PDPRelatedSubjectsText_font +PDPRelatedSubjectsText_size +PDPRelatedSubjectsText_color);
				fail("PDP Related Subjects font, font size, font color is not as per the creative",log);
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA346"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA348 - Verify that  font size, font color, padding in the product detail page should be as per the creative*/
	public void BNIA348()
	{
		ChildCreation("BNIA348 - Verify that  font size, font color, padding in the product detail page should be as per the creative");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductName");
			WebElement ProductTitle = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductName");
			WebElement AuthorName = BNBasicCommonMethods.findElement(driver, obj, "PDP_AuthorName");
			WebElement Reviews = BNBasicCommonMethods.findElement(driver, obj, "PDP_Reviews");
			WebElement SelectedFormatText = BNBasicCommonMethods.findElement(driver, obj, "PDP_SelectedFormatText");
			WebElement SalePrice = BNBasicCommonMethods.findElement(driver, obj, "PDP_SalePrice");
			WebElement ATBButton = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
			WebElement EmailButton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			WebElement OverviewLinkText = BNBasicCommonMethods.findElement(driver, obj, "PDPOverviewLink");
			/*WebElement CustomerAlsoBoughtText = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomeralsoBoughtLink");
			WebElement CustomerReviewsTitle1 = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsTitle");
			WebElement customeralsobought = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomeralsoBoughtLink");*/
			//WebElement customerreview = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerReviewsLink");
			Actions act = new Actions(driver);
			SelectedFormatText.click();
			for(int ctr=0 ; ctr<50 ;ctr++)
			{
				act.sendKeys(Keys.CONTROL).sendKeys(Keys.ARROW_RIGHT).build().perform();
			}
			//BNBasicCommonMethods.scrollup(customerreview, driver);
			
			/*String url = driver.getCurrentUrl();
			driver.get(url);
			Thread.sleep(2000);
			customerreview = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerReviewsLink");
			BNBasicCommonMethods.scrollup(customerreview, driver);*/
			Thread.sleep(2000);
			WebElement CustomerBoughtThisAlsoBoughtProductName = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerAlsoBoughtThisSection_Products1");
			WebElement CustomerBoughtThisAlsoBoughtAuthorName = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerAlsoBoughtThisSection_AuthorName1");
			WebElement CustomerReviewsTitle = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsTitle");
			//WebElement CustomerReviewsText = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsText1");
			WebElement EditorialReviewsPublisherText = BNBasicCommonMethods.findElement(driver, obj, "PDP_EditorialReviews_PublisherText");
			WebElement ProductDetails_Text = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductDetails_Text1");
			WebElement RelatedSubjects_Text = BNBasicCommonMethods.findElement(driver, obj, "PDP_RelatedSubjects_Text1");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String title = BNBasicCommonMethods.getExcelVal("BNIA346", sheet, 4);
			String Title_Values[] = title.split("\n");
			String color = BNBasicCommonMethods.getExcelVal("BNIA346", sheet, 6);
			String Color_Values[] = color.split("\n");
			String size = BNBasicCommonMethods.getExcelVal("BNIA346", sheet, 5);
			String Size_Values[] = size.split("\n");
			
			//Product Title
			
			String Prodtitlename_font = Title_Values[0];
			String Prodtitle_size = Size_Values[0];
			String Prodtitlename_color = Color_Values[0];
			
			String PDPprdtitle_font = ProductTitle.getCssValue("font-family");
			String PDPprodtitle_size = ProductTitle.getCssValue("font-size");
			//System.out.println(""+PDPprdtitle_font);
			//System.out.println(""+PDPprodtitle_size);
			String PDPprodtitle_color = ProductTitle.getCssValue("color");
			Color PDPprodtitlecolor = Color.fromString(PDPprodtitle_color);
			String hexPDPprodtitlecolors = PDPprodtitlecolor.asHex();
			//System.out.println(""+hexPDPprodtitlecolors);
			
			if((PDPprdtitle_font.contains(Prodtitlename_font)) && (PDPprodtitle_size.equals(Prodtitle_size)) && (hexPDPprodtitlecolors.equals(Prodtitlename_color)))
			{
				log.add("The PDP Product Title Text font, size and color is "+"Current font name, size and color "+PDPprdtitle_font +PDPprodtitle_size +hexPDPprodtitlecolors +"Expected font name, size and color is "+Prodtitlename_font +Prodtitle_size +Prodtitlename_color);
				pass("PDP Product Title font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The PDP Product Title Text font, size and color is "+"Current font name, size and color "+PDPprdtitle_font +PDPprodtitle_size +hexPDPprodtitlecolors +"Expected font name, size and color is "+Prodtitlename_font +Prodtitle_size +Prodtitlename_color);
				fail("PDP Product Title font, font size, font color is not as per the creative",log);
			}	
			
			//Author Name
			String PDPAuthorname_font = Title_Values[1];
			String PDPAuthorname_size = Size_Values[1];
			String PDPAuthorname_color = Color_Values[0];
			String PDPAuthornamefont = AuthorName.getCssValue("font-family");
			String PDPAuthornamesize = AuthorName.getCssValue("font-size");
			//System.out.println(""+PDPAuthornamefont);
			//System.out.println(""+PDPAuthorname_size);
			String PDPAuthorname_size_color = AuthorName.getCssValue("color");
			Color PDPAuthorname_sizecolor = Color.fromString(PDPAuthorname_size_color);
			String hexPDPAuthorname_sizecolors = PDPAuthorname_sizecolor.asHex();
			//System.out.println(""+hexPDPAuthorname_sizecolors);
			
			if((PDPAuthornamefont.contains(PDPAuthorname_font)) && (PDPAuthornamesize.equals(PDPAuthorname_size)) && (hexPDPAuthorname_sizecolors.equals(PDPAuthorname_color)))
			{
				log.add("The PDP Author Name Text font, size and color is "+"Current font name, size and color "+PDPAuthornamefont +PDPAuthornamesize +hexPDPAuthorname_sizecolors +"Expected font name, size and color is "+PDPAuthorname_font +PDPAuthorname_size +PDPAuthorname_color);
				pass("PDP Author Name font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The Author Name Text font, size and color is "+"Current font name, size and color "+PDPAuthornamefont +PDPAuthornamesize +hexPDPAuthorname_sizecolors +"Expected font name, size and color is "+PDPAuthorname_font +PDPAuthorname_size +PDPAuthorname_color);
				fail("PDP Author Name font, font size, font color is not as per the creative",log);
			}
			
			//Reviews
			String PDPReviews_font = Title_Values[1];
			String PDPReviews_size = Size_Values[1];
			String PDPReviews_color = Color_Values[0];
			//String PDPReviews_lineheight = "16px";
			
			String PDPReviewsfont = Reviews.getCssValue("font-family");
			String PDPReviewssize = Reviews.getCssValue("font-size");
			//String PDPReviewslineheight = Reviews.getCssValue("line-height");
			//System.out.println(""+PDPReviewsfont);
			//System.out.println(""+PDPReviewssize);
			//System.out.println(""+PDPReviewslineheight);
			String PDPReviews_size_color = Reviews.getCssValue("color");
			Color PDPReviews_sizecolor = Color.fromString(PDPReviews_size_color);
			String hexPDPReviews_sizecolors = PDPReviews_sizecolor.asHex();
			//System.out.println(""+hexPDPReviews_sizecolors);
			
			if((PDPReviewsfont.contains(PDPReviews_font)) && (PDPReviewssize.equals(PDPReviews_size)) && (hexPDPReviews_sizecolors.equals(PDPReviews_color)))
			{
				log.add("The PDP Reviews Text font, size and color is "+"Current font name, size and color "+PDPReviewsfont +PDPReviewssize +hexPDPReviews_sizecolors +"Expected font name, size and color is "+PDPReviews_font +PDPReviews_size +PDPReviews_color);
				pass("PDP Reviews font, font size, font color is as per the creative",log);
				/*if(PDPReviewslineheight.equals(PDPReviews_lineheight))
				{
					log.add("The PDP Reviews Text Line Height is "+"Current "+PDPReviewslineheight +"Expected "+PDPReviews_lineheight);
					pass("PDP Reviews Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP Reviews Text Line Height is "+"Current "+PDPReviewslineheight +"Expected "+PDPReviews_lineheight);
					fail("PDP Reviews Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Reviews Text font, size and color is "+"Current font name, size and color "+PDPReviewsfont +PDPReviewssize +hexPDPReviews_sizecolors +"Expected font name, size and color is "+PDPReviews_font +PDPReviews_size +PDPReviews_color);
				fail("PDP Reviews font, font size, font color is not as per the creative",log);
			}
			
			//SelectedFormatText
			String PDPSelectedFormatText_font = Title_Values[2];
			String PDPSelectedFormatText_size = Size_Values[1];
			String PDPSelectedFormatText_color = Color_Values[0];
			//String PDPSelectedFormatText_lineheight = "28px";
			//String lineheight = "normal";
			
			String PDPSelectedFormatTextfont = SelectedFormatText.getCssValue("font-family");
			String PDPSelectedFormatTextsize = SelectedFormatText.getCssValue("font-size");
			//String PDPSelectedFormatTextlineheight = SelectedFormatText.getCssValue("line-height");
			//System.out.println(""+PDPSelectedFormatTextfont);
			//System.out.println(""+PDPSelectedFormatTextsize);
			//System.out.println(""+PDPSelectedFormatTextlineheight);
			String PDPSelectedFormatText_size_color = SelectedFormatText.getCssValue("color");
			Color PDPSelectedFormatText_sizecolor = Color.fromString(PDPSelectedFormatText_size_color);
			String hexPDPSelectedFormatText_sizecolors = PDPSelectedFormatText_sizecolor.asHex();
			//System.out.println(""+hexPDPSelectedFormatText_sizecolors);
			
			if((PDPSelectedFormatTextfont.contains(PDPSelectedFormatText_font)) && (PDPSelectedFormatTextsize.equals(PDPSelectedFormatText_size)) && (hexPDPSelectedFormatText_sizecolors.equals(PDPSelectedFormatText_color)))
			{
				log.add("The PDP Selected Format Text font, size and color is "+"Current font name, size and color "+PDPSelectedFormatTextfont +PDPSelectedFormatTextsize +hexPDPSelectedFormatText_sizecolors +"Expected font name, size and color is "+PDPSelectedFormatText_font +PDPSelectedFormatText_size +PDPSelectedFormatText_color);
				pass("PDP Selected Format font, font size, font color is as per the creative",log);
				/*if(PDPSelectedFormatTextlineheight.equals(PDPSelectedFormatText_lineheight))
				{
					log.add("The PDP Selected Format Text Line Height is "+"Current "+PDPSelectedFormatTextlineheight +"Expected "+PDPSelectedFormatText_lineheight);
					pass("PDP Selected Format Line height is as per the creative",log);
				}
				else if(PDPSelectedFormatTextlineheight.equals(lineheight))
				{
					log.add("The PDP Selected Format Text Line Height is "+"Current "+PDPSelectedFormatTextlineheight +"Expected "+PDPSelectedFormatText_lineheight);
					fail("PDP Selected Format Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Selected Format Text font, size and color is "+"Current font name, size and color "+PDPSelectedFormatTextfont +PDPSelectedFormatTextsize +hexPDPSelectedFormatText_sizecolors +"Expected font name, size and color is "+PDPSelectedFormatText_font +PDPSelectedFormatText_size +PDPSelectedFormatText_color);
				fail("PDP Reviews font, font size, font color is not as per the creative",log);
			}
			
			
			//SalePrice
			String PDPSalePrice_font = Title_Values[2];
			String PDPSalePrice_size = Size_Values[2];
			String PDPSalePrice_color = Color_Values[0];
			//String PDPSalePrice_lineheight = "24px";
			
			String PDPSalePricefont = SalePrice.getCssValue("font-family");
			String PDPSalePricesize = SalePrice.getCssValue("font-size");
			//String PDPSalePricelineheight = SalePrice.getCssValue("line-height");
			//System.out.println(""+PDPSalePricefont);
			//System.out.println(""+PDPSalePricesize);
			//System.out.println(""+PDPSalePricelineheight);
			String PDPSalePrice_size_color = SalePrice.getCssValue("color");
			Color PDPSalePrice_sizecolor = Color.fromString(PDPSalePrice_size_color);
			String hexPDPSalePrice_sizecolors = PDPSalePrice_sizecolor.asHex();
			//System.out.println(""+hexPDPSalePrice_sizecolors);
			
			if((PDPSalePricefont.contains(PDPSalePrice_font)) && (PDPSalePricesize.equals(PDPSalePrice_size)) && (hexPDPSalePrice_sizecolors.equals(PDPSalePrice_color)))
			{
				log.add("The PDP Sale Price Text font, size and color is \n"+"Current font name, size and color \n"+PDPSalePricefont +"\n"+PDPSalePricesize+"\n" +hexPDPSalePrice_sizecolors+"\n"+"Expected font name, size and color is "+PDPSalePrice_font +PDPSalePrice_size +PDPSalePrice_color);
				pass("PDP Sale Price font, font size, font color is as per the creative",log);
				/*if(PDPSalePricelineheight.equals(PDPSalePrice_lineheight))
				{
					log.add("The PDP Sale Price Text Line Height is "+"Current "+PDPSalePricelineheight +"Expected "+PDPSalePrice_lineheight);
					pass("PDP Sale Price Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP Sale Price Text Line Height is "+"Current "+PDPSalePricelineheight +"Expected "+PDPSalePrice_lineheight);
					fail("PDP Sale Price Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Sale Price Text font, size and color is "+"Current font name, size and color "+PDPSalePricefont +PDPSalePricesize +hexPDPSalePrice_sizecolors +"Expected font name, size and color is "+PDPSalePrice_font +PDPSalePrice_size +PDPSalePrice_color);
				fail("PDP Sale Price font, font size, font color is not as per the creative",log);
			}
			
			//ATBButton
			String PDPATBButton_font = Title_Values[1];
			String PDPATBButton_size = Size_Values[3];
			String PDPATBButtonText_color = Color_Values[1];
			//String PDPATBButton_color = Color_Values[2];
			//String PDPATBButton_lineheight = "16px";
			
			String PDPATBButtonfont = ATBButton.getCssValue("font-family");
			String PDPATBButtonsize = ATBButton.getCssValue("font-size");
			//String PDPATBButtonlineheight = ATBButton.getCssValue("line-height");
			//System.out.println(""+PDPATBButtonfont);
			//System.out.println(""+PDPATBButtonsize);
			//System.out.println(""+PDPSalePricelineheight);
			String PDPATBButtonText1_color = ATBButton.getCssValue("color");
			Color PDPATBButtonTexcolor = Color.fromString(PDPATBButtonText1_color);
			String hexPDPATBButton_sizecolors = PDPATBButtonTexcolor.asHex();
			//System.out.println(""+hexPDPATBButton_sizecolors);
			String PDPATBButton1_color = ATBButton.getCssValue("background-color");
			Color PDPATBButton_sizecolor = Color.fromString(PDPATBButton1_color);
			String hexPDPATBButton1_sizecolors = PDPATBButton_sizecolor.asHex();
			//System.out.println(""+hexPDPATBButton1_sizecolors);
			
			if((PDPATBButtonfont.contains(PDPATBButton_font)) && (PDPATBButtonsize.equals(PDPATBButton_size)) && (hexPDPATBButton_sizecolors.equals(PDPATBButtonText_color)))
			{
				log.add("The PDP ATB Button Text font, size and color is "+"Current font name, size and color "+PDPATBButtonfont +PDPATBButtonsize +hexPDPATBButton_sizecolors +"Expected font name, size and color is "+PDPATBButton_font +PDPATBButton_size +PDPATBButtonText_color);
				pass("PDP ATB Button font, font size, font color is as per the creative",log);
				/*if((PDPATBButtonlineheight.equals(PDPATBButton_lineheight)) && (hexPDPATBButton1_sizecolors.equals(PDPATBButton_color)))
				{
					log.add("The PDP ATB Button Text Line Height and background color is "+"Current "+PDPATBButtonlineheight +hexPDPATBButton1_sizecolors +"Expected "+PDPATBButton_lineheight +PDPATBButton_color);
					pass("PDP ATB Button Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP ATB Button Text Line Height and background color is "+"Current "+PDPATBButtonlineheight +hexPDPATBButton1_sizecolors +"Expected "+PDPATBButton_lineheight +PDPATBButton_color);
					fail("PDP ATB Button Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP ATB Button Text font, size and color is "+"Current font name, size and color "+PDPATBButtonfont +PDPATBButtonsize +hexPDPATBButton_sizecolors +"Expected font name, size and color is "+PDPATBButton_font +PDPATBButton_size +PDPATBButtonText_color);
				fail("PDP ATB Button font, font size, font color is not as per the creative",log);
			}
			
			//EmailButton
			String PDPEmailButton_font = Title_Values[1];
			String PDPEmailButton_size = Size_Values[1];
			String PDPEmailButton_color = Color_Values[3];
			//String PDPEmailButton_lineheight = "16px";
			String PDPEmailButtonBorder = Color_Values[3];
			
			String PDPEmailButtonfont = EmailButton.getCssValue("font-family");
			String PDPEmailButtonsize = EmailButton.getCssValue("font-size");
			//String PDPEmailButtonlineheight = SalePrice.getCssValue("line-height");
			String PDPEmailButtonBordercolor = EmailButton.getCssValue("border");
			String PDPEmailcolorRGBColor  = PDPEmailButtonBordercolor.replace("1px solid ", "");
			Color PDPbordercolor = Color.fromString(PDPEmailcolorRGBColor);
			String hexborder_colors = PDPbordercolor.asHex();
			//System.out.println(""+hexborder_colors);
			//System.out.println(""+PDPEmailButtonfont);
			//System.out.println(""+PDPEmailButtonsize);
			//System.out.println(""+PDPSalePricelineheight);
			String PDPEmailButton1_color = EmailButton.getCssValue("color");
			Color PDPEmailButtoncolor = Color.fromString(PDPEmailButton1_color);
			String hexPDPEmailButton_colors = PDPEmailButtoncolor.asHex();
			//System.out.println(""+hexPDPEmailButton_colors);
			
			if((PDPEmailButtonfont.contains(PDPEmailButton_font)) && (PDPEmailButtonsize.equals(PDPEmailButton_size)) && (hexPDPEmailButton_colors.equals(PDPEmailButton_color)))
			{
				log.add("The PDP EmailButton Text font, size and color is "+"Current font name, size and color "+PDPEmailButtonfont +PDPEmailButtonsize +hexPDPEmailButton_colors +"Expected font name, size and color is "+PDPEmailButton_font +PDPEmailButton_size +PDPEmailButton_color);
				pass("PDP Sale Price font, font size, font color is as per the creative",log);
				if((PDPEmailButtonBorder.contains(hexborder_colors)))
				{
					log.add("The PDP EmailButton Text Line Height and border color is "+"Current "+PDPEmailButtonBordercolor +"Expected "+PDPEmailButtonBorder);
					pass("PDP EmailButton Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP EmailButton Text Line Height and border color is "+"Current "+PDPEmailButtonBordercolor +"Expected " +PDPEmailButtonBorder);
					fail("PDP EmailButton Line height is not as per the creative",log);
				}
			}
			else
			{
				log.add("The PDP EmailButton Text font, size and color is "+"Current font name, size and color "+PDPEmailButtonfont +PDPEmailButtonsize +hexPDPEmailButton_colors +"Expected font name, size and color is "+PDPEmailButton_font +PDPEmailButton_size +PDPEmailButton_color);
				fail("PDP EmailButton font, font size, font color is not as per the creative",log);
			}
			
			
			//OverviewLinkText
			String PDPOverviewLinkText_font = Title_Values[2];
			String PDPOverviewLinkText_size = Size_Values[1];
			String PDPOverviewLinkText_color = Color_Values[3];
			
			String PDPOverviewLinkTextfont = OverviewLinkText.getCssValue("font-family");
			String PDPOverviewLinkTextsize = OverviewLinkText.getCssValue("font-size");
			//System.out.println(""+PDPOverviewLinkTextfont);
			//System.out.println(""+PDPOverviewLinkTextsize);
			String PDPOverviewLinkText_size_color = OverviewLinkText.getCssValue("color");
			Color PDPOverviewLinkText_sizecolor = Color.fromString(PDPOverviewLinkText_size_color);
			String hexPDPOverviewLinkText_sizecolors = PDPOverviewLinkText_sizecolor.asHex();
			//System.out.println(""+hexPDPOverviewLinkText_sizecolors);
			
			if((PDPOverviewLinkTextfont.contains(PDPOverviewLinkText_font)) && (PDPOverviewLinkTextsize.equals(PDPOverviewLinkText_size)) && (hexPDPOverviewLinkText_sizecolors.equals(PDPOverviewLinkText_color)))
			{
				log.add("The PDP OverviewLink Text font, size and color is "+"Current font name, size and color "+PDPOverviewLinkTextfont +PDPOverviewLinkTextsize +hexPDPOverviewLinkText_sizecolors +"Expected font name, size and color is "+PDPOverviewLinkText_font +PDPOverviewLinkText_size +PDPOverviewLinkText_color);
				pass("PDP OverviewLink font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The PDP OverviewLink Text font, size and color is "+"Current font name, size and color "+PDPOverviewLinkTextfont +PDPOverviewLinkTextsize +hexPDPOverviewLinkText_sizecolors +"Expected font name, size and color is "+PDPOverviewLinkText_font +PDPOverviewLinkText_size +PDPOverviewLinkText_color);
				fail("PDP OverviewLink font, font size, font color is not as per the creative",log);
			}
			
			
			//CustomerBoughtThisAlsoBoughtProductName
			
			String PDPCusBoughtPrdName_font = Title_Values[2];
			String PDPCusBoughtPrdName_size = Size_Values[3];
			String PDPCusBoughtPrdName_color = Color_Values[4];
			//String PDPCusBoughtPrdName_lineheight = "20px";
			
			String PDPCusBoughtPrdNamefont = CustomerBoughtThisAlsoBoughtProductName.getCssValue("font-family");
			String PDPCusBoughtPrdNamesize = CustomerBoughtThisAlsoBoughtProductName.getCssValue("font-size");
			//String PDPCusBoughtPrdNamelineheight = CustomerBoughtThisAlsoBoughtProductName.getCssValue("line-height");
			//System.out.println(""+PDPCusBoughtPrdNamefont);
			//System.out.println(""+PDPCusBoughtPrdNamesize);
			//System.out.println(""+PDPCusBoughtPrdNamelineheight);
			String PDPCusBoughtPrdName_size_color = CustomerBoughtThisAlsoBoughtProductName.getCssValue("color");
			Color PDPCusBoughtPrdName_sizecolor = Color.fromString(PDPCusBoughtPrdName_size_color);
			String hexPDPCusBoughtPrdName_sizecolors = PDPCusBoughtPrdName_sizecolor.asHex();
			//System.out.println(""+hexPDPCusBoughtPrdName_sizecolors);
			
			if((PDPCusBoughtPrdNamefont.contains(PDPCusBoughtPrdName_font)) && (PDPCusBoughtPrdNamesize.equals(PDPCusBoughtPrdName_size)) && (hexPDPCusBoughtPrdName_sizecolors.equals(PDPCusBoughtPrdName_color)))
			{
				log.add("The PDP Customer bought Product Name Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtPrdNamefont +PDPCusBoughtPrdNamesize +hexPDPCusBoughtPrdName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtPrdName_font +PDPCusBoughtPrdName_size +PDPCusBoughtPrdName_color);
				pass("PDP Customer bought Product Name font, font size, font color is as per the creative",log);
				/*if(PDPCusBoughtPrdNamelineheight.equals(PDPCusBoughtPrdName_lineheight))
				{
					log.add("The PDP Customer bought Product Name Text Line Height is "+"Current "+PDPCusBoughtPrdNamelineheight +"Expected "+PDPCusBoughtPrdName_lineheight);
					pass("PDP Customer bought Product Name Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP Customer bought Product Name Text Line Height is "+"Current "+PDPCusBoughtPrdNamelineheight +"Expected "+PDPCusBoughtPrdName_lineheight);
					fail("PDP Customer bought Product Name Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Customer bought Product Name Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtPrdNamefont +PDPCusBoughtPrdNamesize +hexPDPCusBoughtPrdName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtPrdName_font +PDPCusBoughtPrdName_size +PDPCusBoughtPrdName_color);
				fail("PDP Customer bought Product Name font, font size, font color is not as per the creative",log);
			}
			
			
			//CustomerBoughtThisAlsoBoughtAuthorName
			
			String PDPCusBoughtAuthorName_font = Title_Values[1];
			String PDPCusBoughtAuthorName_size = Size_Values[1];
			String PDPCusBoughtAuthorName_color = Color_Values[5];
			//String PDPCusBoughtAuthorName_lineheight = "20px";
			
			String PDPCusBoughtAuthorNamefont = CustomerBoughtThisAlsoBoughtAuthorName.getCssValue("font-family");
			String PDPCusBoughtAuthorNamesize = CustomerBoughtThisAlsoBoughtAuthorName.getCssValue("font-size");
			//String PDPCusBoughtAuthorNamelineheight = CustomerBoughtThisAlsoBoughtAuthorName.getCssValue("line-height");
			//System.out.println(""+PDPCusBoughtAuthorNamefont);
			//System.out.println(""+PDPCusBoughtAuthorNamesize);
			//System.out.println(""+PDPCusBoughtAuthorNamelineheight);
			String PDPCusBoughtAuthorName_size_color = CustomerBoughtThisAlsoBoughtAuthorName.getCssValue("color");
			Color PDPCusBoughtAuthorName_sizecolor = Color.fromString(PDPCusBoughtAuthorName_size_color);
			String hexPDPCusBoughtAuthorName_sizecolors = PDPCusBoughtAuthorName_sizecolor.asHex();
			//System.out.println(""+hexPDPCusBoughtAuthorName_sizecolors);
			
			if((PDPCusBoughtAuthorNamefont.contains(PDPCusBoughtAuthorName_font)) && (PDPCusBoughtAuthorNamesize.equals(PDPCusBoughtAuthorName_size)) && (hexPDPCusBoughtAuthorName_sizecolors.equals(PDPCusBoughtAuthorName_color)))
			{
				log.add("The PDP Customer bought Author Name Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtAuthorNamefont +PDPCusBoughtAuthorNamesize +hexPDPCusBoughtAuthorName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtAuthorName_font +PDPCusBoughtAuthorName_size +PDPCusBoughtAuthorName_color);
				pass("PDP Customer bought Author Name font, font size, font color is as per the creative",log);
				/*if(PDPCusBoughtAuthorNamelineheight.equals(PDPCusBoughtAuthorName_lineheight))
				{
					log.add("The PDP Customer bought Author Name Text Line Height is "+"Current "+PDPCusBoughtAuthorNamelineheight +"Expected "+PDPCusBoughtAuthorName_lineheight);
					pass("PDP Customer bought Author Name Line height is as per the creative",log);
				}
				else
				{
					log.add("The PDP Customer bought Author Name Text Line Height is "+"Current "+PDPCusBoughtAuthorNamelineheight +"Expected "+PDPCusBoughtAuthorName_lineheight);
					fail("PDP Customer bought Author Name Line height is not as per the creative",log);
				}*/
			}
			else
			{
				log.add("The PDP Customer bought Author Name Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtAuthorNamefont +PDPCusBoughtAuthorNamesize +hexPDPCusBoughtAuthorName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtAuthorName_font +PDPCusBoughtAuthorName_size +PDPCusBoughtAuthorName_color);
				fail("PDP Customer bought Author Name font, font size, font color is not as per the creative",log);
			}
			
			
			//CustomerReviewsTitle
			
			String PDPCustomerReviewTitle_font = Title_Values[0];
			String PDPCustomerReviewTitle_size = Size_Values[2];
			String PDPCustomerReviewTitle_color = Color_Values[0];
			//String PDPCustomerReviewTitle_lineheight = "27px";
			
			String PDPCustomerReviewTitlefont = CustomerReviewsTitle.getCssValue("font-family");
			String PDPCustomerReviewTitlesize = CustomerReviewsTitle.getCssValue("font-size");
			//String PDPCustomerReviewTitlelineheight = CustomerReviewsTitle.getCssValue("line-height");
			System.out.println(""+PDPCustomerReviewTitlefont);
			System.out.println(""+PDPCustomerReviewTitlesize);
			//System.out.println(""+PDPCustomerReviewTitlelineheight);
			String PDPCustomerReviewTitle_size_color = CustomerReviewsTitle.getCssValue("color");
			Color PDPCustomerReviewTitle_sizecolor = Color.fromString(PDPCustomerReviewTitle_size_color);
			String hexPDPCustomerReviewTitlee_sizecolors = PDPCustomerReviewTitle_sizecolor.asHex();
			System.out.println(""+hexPDPCustomerReviewTitlee_sizecolors);
			
			if((PDPCustomerReviewTitlefont.contains(PDPCustomerReviewTitle_font)) && (PDPCustomerReviewTitlesize.equals(PDPCustomerReviewTitle_size)) && (hexPDPCustomerReviewTitlee_sizecolors.equals(PDPCustomerReviewTitle_color)))
			{
				log.add("The PDP CustomerReviewsTitle Text font, size and color is "+"Current font name, size and color "+PDPCustomerReviewTitlefont +PDPCustomerReviewTitlesize +hexPDPCustomerReviewTitlee_sizecolors +"Expected font name, size and color is "+PDPCustomerReviewTitle_font +PDPCustomerReviewTitle_size +PDPCustomerReviewTitle_color);
				pass("PDP CustomerReviewsTitlee font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP CustomerReviewsTitle Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtAuthorNamefont +PDPCusBoughtAuthorNamesize +hexPDPCusBoughtAuthorName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtAuthorName_font +PDPCusBoughtAuthorName_size +PDPCusBoughtAuthorName_color);
				fail("PDP CustomerReviewsTitle font, font size, font color is not as per the creative",log);
			}
			
			
			//CustomerReviewsText
			
			String PDPCustomerReviewText_font = Title_Values[0];
			String PDPCustomerReviewText_size = Size_Values[1];
			String PDPCustomerReviewText_color = Color_Values[0];
			//String PDPCustomerReviewText_lineheight = "22px";
			
			String PDPCustomerReviewTextfont = CustomerReviewsTitle.getCssValue("font-family");
			String PDPCustomerReviewTextsize = CustomerReviewsTitle.getCssValue("font-size");
			//String PDPCustomerReviewTextlineheight = CustomerReviewsTitle.getCssValue("line-height");
			System.out.println(""+PDPCustomerReviewTextfont);
			System.out.println(""+PDPCustomerReviewTextsize);
			//System.out.println(""+PDPCustomerReviewTextlineheight);
			String PDPCustomerReviewText_size_color = CustomerReviewsTitle.getCssValue("color");
			Color PDPCustomerReviewText_sizecolor = Color.fromString(PDPCustomerReviewText_size_color);
			String hexPDPCustomerReviewText_sizecolors = PDPCustomerReviewText_sizecolor.asHex();
			System.out.println(""+hexPDPCustomerReviewText_sizecolors);
			
			if((PDPCustomerReviewTextfont.contains(PDPCustomerReviewText_font)) && (PDPCustomerReviewTextsize.equals(PDPCustomerReviewText_size)) && (hexPDPCustomerReviewText_sizecolors.equals(PDPCustomerReviewText_color)))
			{
				log.add("The PDP CustomerReviewsTitle Text font, size and color is "+"Current font name, size and color "+PDPCustomerReviewTextfont +PDPCustomerReviewTextsize +hexPDPCustomerReviewText_sizecolors +"Expected font name, size and color is "+PDPCustomerReviewText_font +PDPCustomerReviewText_size +PDPCustomerReviewText_color);
				pass("PDP CustomerReviewsTitle font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP CustomerReviewsTitle Text font, size and color is "+"Current font name, size and color "+PDPCusBoughtAuthorNamefont +PDPCusBoughtAuthorNamesize +hexPDPCusBoughtAuthorName_sizecolors +"Expected font name, size and color is "+PDPCusBoughtAuthorName_font +PDPCusBoughtAuthorName_size +PDPCusBoughtAuthorName_color);
				fail("PDP CustomerReviewsTitle font, font size, font color is not as per the creative",log);
			}
			
			
			//EditorialReviewsPublisherText
			String PDPEditorialReviewsPublisherText_font = Title_Values[2];
			String PDPEditorialReviewsPublisherText_size = Size_Values[3];
			String PDPEditorialReviewsPublisherText_color = Color_Values[6];
			//String PDPEditorialReviewsPublisherText_lineheight = "22px";
			
			String PDPEditorialReviewsPublisherTextfont = EditorialReviewsPublisherText.getCssValue("font-family");
			String PDPEditorialReviewsPublisherTextsize = EditorialReviewsPublisherText.getCssValue("font-size");
			//String PDPEditorialReviewsPublisherTextlineheight = EditorialReviewsPublisherText.getCssValue("line-height");
			System.out.println(""+PDPEditorialReviewsPublisherTextfont);
			System.out.println(""+PDPEditorialReviewsPublisherTextsize);
			//System.out.println(""+PDPEditorialReviewsPublisherTextlineheight);
			String PDPEditorialReviewsPublisherText_size_color = EditorialReviewsPublisherText.getCssValue("color");
			Color PDPEditorialReviewsPublisherText_sizecolor = Color.fromString(PDPEditorialReviewsPublisherText_size_color);
			String hexPDPEditorialReviewsPublisherText_sizecolors = PDPEditorialReviewsPublisherText_sizecolor.asHex();
			System.out.println(""+hexPDPEditorialReviewsPublisherText_sizecolors);
			
			if((PDPEditorialReviewsPublisherTextfont.contains(PDPEditorialReviewsPublisherText_font)) && (PDPEditorialReviewsPublisherTextsize.equals(PDPEditorialReviewsPublisherText_size)) && (hexPDPEditorialReviewsPublisherText_sizecolors.equals(PDPEditorialReviewsPublisherText_color)))
			{
				log.add("The PDP Editorial Review Publisher Text font, size and color is "+"Current font name, size and color "+PDPEditorialReviewsPublisherTextfont +PDPEditorialReviewsPublisherTextsize +hexPDPEditorialReviewsPublisherText_sizecolors +"Expected font name, size and color is "+PDPEditorialReviewsPublisherText_font +PDPEditorialReviewsPublisherText_size +PDPEditorialReviewsPublisherText_color);
				pass("PDP Editorial Review Publisher font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP Editorial Review Publisher Text font, size and color is "+"Current font name, size and color "+PDPEditorialReviewsPublisherTextfont +PDPEditorialReviewsPublisherTextsize +hexPDPEditorialReviewsPublisherText_sizecolors +"Expected font name, size and color is "+PDPEditorialReviewsPublisherText_font +PDPEditorialReviewsPublisherText_size +PDPEditorialReviewsPublisherText_color);
				fail("PDP Editorial Review Publishere font, font size, font color is not as per the creative",log);
			}
			
			
			
			//ProductDetails_Text
			
			String PDPProductDetailsText_font = Title_Values[1];
			String PDPProductDetailsText_size = Size_Values[3];
			String PDPProductDetailsText_color = Color_Values[6];
			//String PDPProductDetailsText_lineheight = "22px";
			
			String PDPProductDetailsTextfont = ProductDetails_Text.getCssValue("font-family");
			String PDPProductDetailsTextsize = ProductDetails_Text.getCssValue("font-size");
			//String PDPProductDetailsTextlineheight = ProductDetails_Text.getCssValue("line-height");
			System.out.println(""+PDPProductDetailsTextfont);
			System.out.println(""+PDPProductDetailsTextsize);
			//System.out.println(""+PDPProductDetailsTextlineheight);
			String PDPProductDetailsText_size_color = ProductDetails_Text.getCssValue("color");
			Color PDPProductDetailsText_sizecolor = Color.fromString(PDPProductDetailsText_size_color);
			String hexPDPProductDetailsText_sizecolors = PDPProductDetailsText_sizecolor.asHex();
			System.out.println(""+hexPDPProductDetailsText_sizecolors);
			
			if((PDPProductDetailsTextfont.contains(PDPProductDetailsText_font)) && (PDPProductDetailsTextsize.equals(PDPProductDetailsText_size)) && (hexPDPProductDetailsText_sizecolors.equals(PDPProductDetailsText_color)))
			{
				log.add("The PDP Product details Text font, size and color is "+"Current font name, size and color "+PDPProductDetailsTextfont +PDPProductDetailsTextsize +hexPDPProductDetailsText_sizecolors +"Expected font name, size and color is "+PDPProductDetailsText_font +PDPProductDetailsText_size +PDPProductDetailsText_color);
				pass("PDP Product details font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP Product details Text font, size and color is "+"Current font name, size and color "+PDPProductDetailsTextfont +PDPProductDetailsTextsize +hexPDPProductDetailsText_sizecolors +"Expected font name, size and color is "+PDPProductDetailsText_font +PDPProductDetailsText_size +PDPProductDetailsText_color);
				fail("PDP Product details font, font size, font color is not as per the creative",log);
			}
			
			
			//RelatedSubjects_Text
			
			String PDPRelatedSubjectsText_font = Title_Values[1];
			String PDPRelatedSubjectsText_size = Size_Values[4];
			String PDPRelatedSubjectsText_color = Color_Values[1];
			//String PDPRelatedSubjectsText_lineheight = "22px";
			
			String PDPRelatedSubjectsTextfont = RelatedSubjects_Text.getCssValue("font-family");
			String PDPRelatedSubjectsTextsize = RelatedSubjects_Text.getCssValue("font-size");
			//String PDPRelatedSubjectsTextlineheight = RelatedSubjects_Text.getCssValue("line-height");
			System.out.println(""+PDPRelatedSubjectsTextfont);
			System.out.println(""+PDPRelatedSubjectsTextsize);
			//System.out.println(""+PDPRelatedSubjectsTextlineheight);
			String PDPRelatedSubjectsText_size_color = RelatedSubjects_Text.getCssValue("color");
			Color PDPRelatedSubjectsText_sizecolor = Color.fromString(PDPRelatedSubjectsText_size_color);
			String hexPDPRelatedSubjectsText_sizecolors = PDPRelatedSubjectsText_sizecolor.asHex();
			System.out.println(""+hexPDPRelatedSubjectsText_sizecolors);
			
			if((PDPRelatedSubjectsTextfont.contains(PDPRelatedSubjectsText_font)) && (PDPRelatedSubjectsTextsize.equals(PDPRelatedSubjectsText_size)) && (hexPDPRelatedSubjectsText_sizecolors.equals(PDPRelatedSubjectsText_color)))
			{
				log.add("The PDP Related Subject Text font, size and color is "+"Current font name, size and color "+PDPRelatedSubjectsTextfont +PDPRelatedSubjectsTextsize +hexPDPRelatedSubjectsText_sizecolors +"Expected font name, size and color is "+PDPRelatedSubjectsText_font +PDPRelatedSubjectsText_size +PDPRelatedSubjectsText_color);
				pass("PDP Related Subject font, font size, font color is as per the creative",log);
				
			}
			else
			{
				log.add("The PDP Related Subject Text font, size and color is "+"Current font name, size and color "+PDPRelatedSubjectsTextfont +PDPRelatedSubjectsTextsize +hexPDPRelatedSubjectsText_sizecolors +"Expected font name, size and color is "+PDPRelatedSubjectsText_font +PDPRelatedSubjectsText_size +PDPRelatedSubjectsText_color);
				fail("PDP Related Subjects font, font size, font color is not as per the creative",log);
			}
			BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
			WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			logo.click();
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA346"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA-347 Verify that Header should be displayed in the product detail page*/
	public void BNIA347()
	{
		ChildCreation("BNIA-347 Verify that Header should be displayed in the product detail page");
		try
	 	{
			Thread.sleep(2500);
			int ctr=1;
			//boolean facets1 = true;
			/*if(facets1!=true)*/
				
			do
			{
				BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
				WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
				logo.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "BrowseTile");
		 	 	WebElement browse_tile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
		 	 	browse_tile.click();
		 	 	BNBasicCommonMethods.waitforElement(wait, obj, "browsepage");
		 	 	WebElement browsepg = BNBasicCommonMethods.findElement(driver, obj, "browsepage");
		 	 	wait.until(ExpectedConditions.visibilityOf(browsepg));
		 	 	List<WebElement> browse_pg = driver.findElements(By.xpath("//*[contains(@class, 'hotSpotOverlay')]"));
		 	 	Random ran = new Random();
		   	 	int sel = ran.nextInt(browse_pg.size());
		   	 	WebElement menu = driver.findElement(By.xpath("(//*[contains(@class, 'hotSpotOverlay')])["+sel+"]"));
		   	    JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView(true);",menu);
				Thread.sleep(1000);
		   	 	menu.click();
		   	 	Thread.sleep(2000);
		   	 	
		   	 	try
		   	 	{
			   	 	Thread.sleep(2000);
			   	 	boolean facets = false;
			   	 	facets = BNBasicCommonMethods.findElement(driver, obj, "PLP_emptyfactes").isDisplayed();
				   	 	if(facets)
				   	 	{
				   	 		log.add("PLP page is shown");
				   	 		ctr=2;
				   	 	Thread.sleep(2500);
				        List<WebElement> PLP = driver.findElements(By.xpath("//*[@class = 'skMob_productImg showimg scTrack scLink']"));
						Random r = new Random();
						int new1 = r.nextInt(PLP.size());
						Thread.sleep(2500);
						WebElement plp_prd = driver.findElement(By.xpath("(//*[@class = 'skMob_productImg showimg scTrack scLink' ])["+new1+"]"));
						plp_prd.click();
						Thread.sleep(1000);
						log.add("PDP page is shown");
						WebElement head = BNBasicCommonMethods.findElement(driver, obj, "Header");
						boolean header = BNBasicCommonMethods.isElementPresent(head);
						if(header)
						{
							pass("Header is displayed in the product detail page",log);
						}
						else
						{
							fail("Header is not displayed in the product detail page",log);
						}
				   	 		
				   	 	}
				   	 	else
				   	 	{
				   	 		log.add("PLP facets is not found");
				   	 		
				   	 	}
		   	 	}
		   	 	catch(Exception e)
		   	 	{
		   	 	String url = driver.getCurrentUrl();
			   	 	if(url.contains("pdp?"))
			   	 	{
			   	 		WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			   	 	    BNLogo.click();
			   	 	    Thread.sleep(1000);
			   	 	}
		   	 	}
		   	 	
			}while(ctr <  2);
	 	}	
         catch(Exception e)
		{
        	e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA349 - Verify that barnes and noble logo should be shown in the header and aligned center in product detail page*/
	public void BNIA349()
	{
		ChildCreation("BNIA349 - Verify that barnes and noble logo should be shown in the header and aligned center in product detail page");
		try
		{
			WebElement BNlogo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			BNlogo.getSize();
			System.out.println(" BNLogo Width and Height is "+BNlogo.getSize());
			System.out.println("Location points is "+BNlogo.getLocation());
			String Loc = BNlogo.getLocation().toString();
			String LogolocationWidthHeight = "(401, 23)";
			boolean logo = BNBasicCommonMethods.isElementPresent(BNlogo);
			if(LogolocationWidthHeight.equals(Loc) && (logo))
			 {
				pass("barnes and noble logo shown in the header and aligned center in product detail page");
	    	 }
			else
			{
				fail("barnes and noble logo is not shown in the header and aligned center in product detail page");
			}
		
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA350 - Verify that on tapping barnes and noble logo, it should navigate to home page */
	public void BNIA350() throws Exception
	{
		ChildCreation("BNIA350 - Verify that on tapping barnes and noble logo, it should navigate to home page ");
		try
		{
			WebElement BNlogo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			BNlogo.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Homepage");
			WebElement home = BNBasicCommonMethods.findElement(driver, obj, "Homepage");
			boolean home_page = BNBasicCommonMethods.isElementPresent(home);
			if(home_page)
			{
				pass("On tapping barnes and noble logo, it navigates to home page");
			}
			else
			{
				fail("On tapping barnes and noble logo, it does not navigates to home page");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	
	public void SearchNavigatepdp() throws Exception
	{
		/*WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
		logo.click();*/
		BNBasicCommonMethods.waitforElement(wait, obj, "Searchtile");
		BNBasicCommonMethods.findElement(driver, obj, "Searchtile").click();
		BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
		WebElement searchbox1 = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
		searchbox1.sendKeys("9780316407021");
		searchbox1.sendKeys(Keys.ENTER);
	}
	
	/*BNIA-351 Verify that Back Arrow icon should be diplayed at the header in the product detail page*/
	public void BNIA351()
	{
		ChildCreation("BNIA-351 Verify that Back Arrow icon should be diplayed at the header in the product detail page");
		
		try
	 	{
			int ctr=1;
			//boolean facets1 = true;
			/*if(facets1!=true)*/
				
			do
			{
				BNBasicCommonMethods.waitforElement(wait, obj, "BrowseTile");
		 	 	WebElement browse_tile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
		 	 	browse_tile.click();
		 	 	BNBasicCommonMethods.waitforElement(wait, obj, "browsepage");
		 	 	WebElement browsepg = BNBasicCommonMethods.findElement(driver, obj, "browsepage");
		 	 	wait.until(ExpectedConditions.visibilityOf(browsepg));
		 	 	List<WebElement> browse_pg = driver.findElements(By.xpath("//*[contains(@class, 'hotSpotOverlay')]"));
		 	 	Random ran = new Random();
		   	 	int sel = ran.nextInt(browse_pg.size());
		   	 	WebElement menu = driver.findElement(By.xpath("(//*[contains(@class, 'hotSpotOverlay')])["+sel+"]"));
		   	    JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView(true);",menu);
				Thread.sleep(1000);
		   	 	menu.click();
		   	 	Thread.sleep(2000);
		   	 	
		   	 	try
		   	 	{
			   	 	Thread.sleep(2000);
			   	 	boolean facets = false;
			   	 	facets = BNBasicCommonMethods.findElement(driver, obj, "PLP_emptyfactes").isDisplayed();
				   	 	if(facets)
				   	 	{
				   	 		log.add("PLP page is shown");
				   	 		ctr=2;
					   	 	Thread.sleep(2500);
					        List<WebElement> PLP = driver.findElements(By.xpath("//*[@class = 'skMob_productImg showimg scTrack scLink']"));
							Random r = new Random();
							int new1 = r.nextInt(PLP.size());
							Thread.sleep(2500);
							WebElement plp_prd = driver.findElement(By.xpath("(//*[@class = 'skMob_productImg showimg scTrack scLink' ])["+new1+"]"));
							plp_prd.click();
							Thread.sleep(1000);
							log.add("PDP page is shown");
							WebElement head = BNBasicCommonMethods.findElement(driver, obj, "Header");
							boolean header = BNBasicCommonMethods.isElementPresent(head);
							if(header)
							{
								pass("Header is displayed in the product detail page",log);
							}
							else
							{
								fail("Header is not displayed in the product detail page",log);
							}
					   	 		
					   	 	}
					   	 	else
				   	 	{
				   	 		log.add("PLP facets is not found");
				   	 		
				   	 	}
		   	 	}
		   	 	catch(Exception e)
		   	 	{
		   	 	String url = driver.getCurrentUrl();
			   	 	if(url.contains("pdp?"))
			   	 	{
			   	 		WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			   	 	    BNLogo.click();
			   	 	    Thread.sleep(1000);
			   	 	}
		   	 	}
		   	 	
			}while(ctr <  2);
			WebElement bckarrow = BNBasicCommonMethods.findElement(driver, obj, "backarrow");
			boolean barrow = BNBasicCommonMethods.isElementPresent(bckarrow);
			if(barrow)
			{
				pass("Back Arrow icon is diplayed at the header in the product detail page",log);
			}
			else
			{
				fail("Back Arrow icon is not diplayed at the header in the product detail page",log);
			}
	 	}	
         catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA351\n"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA352 - Verify that on tapping barnes and noble logo at the header, it should not navigate to any other page except home page.*/
	public void BNIA352()
	{
		ChildCreation("BNIA352 - Verify that on tapping barnes and noble logo at the header, it should not navigate to any other page except home page.");
		try
		{
			Thread.sleep(2000);
			WebElement BNlogo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			BNlogo.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Homepage");
			WebElement home = BNBasicCommonMethods.findElement(driver, obj, "Homepage");
			boolean home_page = BNBasicCommonMethods.isElementPresent(home);
			if(home_page)
			{
				pass("On tapping barnes and noble logo at the header, it does not navigates to any other page except home page.");
			}
			else
			{
				fail("On tapping barnes and noble logo at the header, it navigates to any other page except home page.");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA353 - Verify that  on selecting the back arrow button at the header, it should navigates to previous page*/
	public void BNIA353() throws Exception
	{
		ChildCreation("BNIA353 - Verify that  on selecting the back arrow button at the header, it should navigates to previous page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "backarrow");
			WebElement bckarrow = BNBasicCommonMethods.findElement(driver, obj, "backarrow");
			bckarrow.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "PLP");
			WebElement plp = BNBasicCommonMethods.findElement(driver, obj, "PLP");
			boolean bool = false;
			bool = plp.isDisplayed();
			if(bool)
			{
				pass("On selecting the back arrow button at the header, it navigates to previous page");
			}
			else
			{
				fail("On selecting the back arrow button at the header, it does not navigates to previous page");
			}
		}
		catch(Exception e)
		{
			e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
		
	}
	
	/*BNIA354 - Verify that hamburger icon should be displayed next to back arrow button in the product detail page*/
	public void BNIA354() throws Exception
	{
		ChildCreation("BNIA354 - Verify that hamburger icon should be displayed next to back arrow button in the product detail page");
		try
		{  
			/*BNBasicCommonMethods.findElement(driver, obj, "logoicon").click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Searchtile");
			BNBasicCommonMethods.findElement(driver, obj, "Searchtile").click();
			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			WebElement searchbox1 = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			//Thread.sleep(2000);
			
			searchbox1.sendKeys("9780316407021");*/
			WebElement pancake = BNBasicCommonMethods.findElement(driver, obj, "PancakeMenu");
			boolean pan = BNBasicCommonMethods.isElementPresent(pancake);
			if(pan)
			{
				pass("Hamburger icon is displayed next to back arrow button in the product detail page");
			}
			else
			{
				fail("Hamburger icon is not displayed next to back arrow button in the product detail page");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
		
	}
	
	/*BNIA355 - Verify that  while tapping hamburger menu at the header,the Pancake Flyover should be displayed*/
	public void BNIA355() throws Exception
	{
		ChildCreation("BNIA355 - Verify that  while tapping hamburger menu at the header,the Pancake Flyover should be displayed");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PancakeMenu");
			WebElement pancake = BNBasicCommonMethods.findElement(driver, obj, "PancakeMenu");
			pancake.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Pancakeflyover");
			WebElement panflyover = BNBasicCommonMethods.findElement(driver, obj, "Pancakeflyover");
			boolean flyover = BNBasicCommonMethods.isElementPresent(panflyover);
			if(flyover)
			{
				pass("while tapping hamburger menu at the header,the Pancake Flyover gets displayed");
			}
			else
			{
				fail("while tapping hamburger menu at the header,the Pancake Flyover does not gets displayed");
			}
			Thread.sleep(1000);
			BNBasicCommonMethods.findElement(driver, obj, "Mask").click();
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA356 - Verify that Search icon should be displayed at the header in the product detail page*/
	public void BNIA356() throws Exception
	{
		ChildCreation("BNIA356 - Verify that Search icon should be displayed at the header in the product detail page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "BrowseTile");
			WebElement menu = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
			menu.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "SearchIcon");
			WebElement searchicon = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
			boolean ser_icon = BNBasicCommonMethods.isElementPresent(searchicon);
			if(ser_icon)
			{
				pass("Search icon is displayed at the header in the product detail page");
			}
			else
			{
				fail("Search icon is not displayed at the header in the product detail page");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
		
	}
	
	/*BNIA357 - Verify that while Selecting search icon at the header, it should navigate to search page */
	public void BNIA357() throws Exception
	{
		ChildCreation("BNIA357 - Verify that while Selecting search icon at the header, it should navigate to search page ");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "SearchIcon");
			WebElement searchicon = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
			searchicon.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			WebElement searchbox1 = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
			boolean searchbx = BNBasicCommonMethods.isElementPresent(searchbox1);
			WebElement scanbtn = BNBasicCommonMethods.findElement(driver, obj, "scanbutton");
			boolean scnbutton = BNBasicCommonMethods.isElementPresent(scanbtn);
	 	 	if(searchbx && scnbutton)
	 	 	{
	 	 		pass(" while Selecting search icon at the header, it navigates to search page ");
	 	 	}
	 	 	else
	 	 	{
	 	 		fail(" while Selecting search icon at the header, it does not navigates to search page ");
	 	 	}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA359 - Verify that bag icon should be displayed at the header in the product detail page*/
	public void BNIA359() throws Exception
	{
		ChildCreation("BNIA359 - Verify that bag icon should be displayed at the header in the product detail page");
		try
		{
			int ctr=1;
			do
			{
				if(!TFprdtId.equals(""))
				{
					String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+TFprdtId;
					driver.get(PDPURL);
					try
					{
						ctr=6;
						BNBasicCommonMethods.waitforElement(wait, obj, "ShopIcon");
						BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
						WebElement shop_icon = BNBasicCommonMethods.findElement(driver, obj, "ShopIcon");
						boolean shop = BNBasicCommonMethods.isElementPresent(shop_icon);
						if(shop)
						{
							pass("bag icon is displayed at the header in the product detail page ");
						}
						else
						{
							fail("bag icon is not displayed at the header in the product detail page ");
						}
					}
					catch(Exception e)
					{
						System.out.println("Shop icon & ATB button is not shown");
						ctr++;
					}
				}
				else
				{
					System.out.println("TF Product ID is not shown");
				}
			}while(ctr<=5);
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA359\n"+e.getMessage());
 			exception(e.getMessage());
		}
		
	}
	
	public void logoclick() throws Exception
	{
		BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
		WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
		logo.click();
	}
	
	/*public boolean PDPTTLogic(ArrayList<String> eanNo) throws Exception
	{
		boolean inventory = false;
		boolean browseOK= browse();
		if(browseOK==true)
		{
			 ArrayList<String> productId = new ArrayList<>();
			 for(int i=1;i<=productId.size();i++)
			  {
				  	String Stream_OnlineInventoryURL = "http://bninstore.skavaone.com/skavastream/core/v5/bandnkiosk/product/"+productId.get(i-1)+"?campaignId=1";
					String Stream_StoreInventoryURL = "http://bninstore.skavaone.com/skavastream/xact/v5/bandnkiosk/getinventory?campaignId=1&storeid=2932&skuid="+productId.get(i-1);
					//String skavaStreamURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam=9780545791328";
					String urlResp = IOUtils.toString(new URI(Stream_OnlineInventoryURL));
					String urlResp1 = IOUtils.toString(new URI(Stream_StoreInventoryURL));
					JSONObject urlObjProp = new JSONObject(urlResp);
					JSONObject prop = urlObjProp.getJSONObject("properties").getJSONObject("buyinfo");
					JSONArray Onlineavailability = prop.getJSONArray("availability");
					System.out.println(Onlineavailability);
					String str = Onlineavailability.toString();
					JSONObject urlObjProp1 = new JSONObject(urlResp1);
					JSONObject prop1 = urlObjProp1.getJSONObject("properties");
					System.out.println(prop1.length());
					JSONArray storeInfo  = prop1.getJSONArray("storeinfo");
					String invent = storeInfo.getJSONObject(0).getString("inventory");
					System.out.println(Onlineavailability);
					System.out.println(invent);
					boolean StroreInventory  = false;
					if(invent.equals("y"))
					{
						StroreInventory = true;
					}
					String [] stringary = str.split(":");
					String str1 = stringary[stringary.length-1];
					boolean Onlineinventory  = false;
					if(str1.contains("true"))
					{
						Onlineinventory = true;
					}
					
					if(StroreInventory && Onlineinventory)
					{
						
					}
					else
					{
						
					}
			}
		}
	return inventory;
		
	}*/
	
	public boolean browse()
	{
		int i=1;
		boolean facet = false;
		do
		{
			try
			{
				try
				{
					boolean facets = BNBasicCommonMethods.findElement(driver, obj, "PLP_facets").isDisplayed();
					if(facets==true)
					{
						String HomeUrl = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/home";
						driver.get(HomeUrl);
						/*BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
						Thread.sleep(1000);
						WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
						JavascriptExecutor executor = (JavascriptExecutor)driver;
						executor.executeScript("arguments[0].click();", logo);
						//logo.click();
*/						Thread.sleep(1000);
					}
					
					BNBasicCommonMethods.waitforElement(wait, obj, "BrowseTile");
			 	 	WebElement browse_tile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
			 	 	browse_tile.click();
			 	 	BNBasicCommonMethods.waitforElement(wait, obj, "browsepage");
			 	 	WebElement browsepg = BNBasicCommonMethods.findElement(driver, obj, "browsepage");
			 	 	wait.until(ExpectedConditions.visibilityOf(browsepg));
			 	 	Thread.sleep(1000);
			 	 	if(BNBasicCommonMethods.isElementPresent(browsepg))
			 	 	{
			 	 		facet = true;
			 	 		break;
			 	 	}
			 	 	else
			 	 	{
			 	 		facet = false;
			 	 		i++;
			 	 		continue;
			 	 	}
				}
				catch(Exception e)
				{
					String HomeUrl = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/home";
					driver.get(HomeUrl);
					BNBasicCommonMethods.waitforElement(wait, obj, "BrowseTile");
			 	 	WebElement browse_tile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
			 	 	browse_tile.click();
			 	 	BNBasicCommonMethods.waitforElement(wait, obj, "browsepage");
			 	 	WebElement browsepg = BNBasicCommonMethods.findElement(driver, obj, "browsepage");
			 	 	wait.until(ExpectedConditions.visibilityOf(browsepg));
			 	 	Thread.sleep(1000);
			 	 	if(BNBasicCommonMethods.isElementPresent(browsepg))
			 	 	{
			 	 		facet = true;
			 	 		break;
			 	 	}
			 	 	else
			 	 	{
			 	 		facet = false;
			 	 		i++;
			 	 		continue;
			 	 	}
				}
			}
		 	catch(Exception e)
			{
		 		e.getMessage();
		 		System.out.println("Something went wrong"+e.getMessage());
		 		exception(e.getMessage());
			}
		}while((facet!=true)||(i<5));
		return facet;
	}
	public void PDPATB() throws Exception
	{
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
			WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
			boolean atb = BNBasicCommonMethods.isElementPresent(ATB);
			if(atb == true)
			{
				
				pass("ATB button is shown");
			}
			else
			{
				fail("ATb button is not shown");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
		
	}
	
	public void PDPAllformats()
	{
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "Allformats");
			WebElement allformats = BNBasicCommonMethods.findElement(driver, obj, "Allformats");
			boolean all_formats = BNBasicCommonMethods.isElementPresent(allformats);
			if(all_formats == true)
			{
				
				pass("All Formats dropdown is shown");
			}
			/*else if(check_nearby == true)
			{
				pass("Check Near By Stores for Stock button is shown");
				logoclick();
				browse();
			}*/
			else
			{
				fail("All Formats dropdown is not shown");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	public void PDPmarketplace()
	{
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "Marketplace");
			WebElement marketplace = BNBasicCommonMethods.findElement(driver, obj, "Marketplace");
			boolean market_place = BNBasicCommonMethods.isElementPresent(marketplace);
			if(market_place == true)
			{
				
				pass("All Formats dropdown is shown");
			}
			/*else if(check_nearby == true)
			{
				pass("Check Near By Stores for Stock button is shown");
				logoclick();
				browse();
			}*/
			else
			{
				fail("All Formats dropdown is not shown");
			}
	    }
		catch(Exception e)
	     {
			e.getMessage();
			System.out.println("Something went wrong"+e.getMessage());
			exception(e.getMessage());
	     }
	}
	
	public void PDPchecknerbystores()
	{
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "Nearbystore");
			//WebElement checknearby = BNBasicCommonMethods.findElement(driver, obj, "Nearbystore");
			//boolean check_nearby = BNBasicCommonMethods.isElementPresent(checknearby);
		}
		catch(Exception e)
	     {
			e.getMessage();
			System.out.println("Something went wrong"+e.getMessage());
			exception(e.getMessage());
	     }
	}
		/*public void PDPATBClick()
		{
			try
			{
				Thread.sleep(1000);
				BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
				WebElement atbclick = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
				atbclick.click();
			}
			catch(Exception e)
		     {
				e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		     }
		}*/
		
		
	
	/*BNIA360 - Verify that product count should be displayed on Bag icon at the header in the product detail page */
	public void BNIA360()
	{
		ChildCreation("BNIA360 - Verify that product count should be displayed on Bag icon at the header in the product detail page ");
		try
		{
			//WebElement shopcount = BNBasicCommonMethods.findElement(driver, obj, "shopicon_count");
			String script = "return $('#bag_count').text()";
		    	String n = (String) ((JavascriptExecutor) driver).executeScript(script, "shopcount");
		    	String []prdtcnt = n.split(" ");
		    	System.out.println(prdtcnt[0]);
		    	if(Integer.parseInt(prdtcnt[0]) == 0)
		    	{
		    		log.add("The Bag count is 0, On clicking the ATB Button, the bag count will be shown "+prdtcnt[0]);
		    		BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
		    		WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
		    		int ctr=1;
		    		do
		    		{
		    			ATB.click();
			    		try
			    		{
			    			ctr=4;
			    			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay_ContinueButton");
				    		WebElement continuebtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ContinueButton");
				    		continuebtn.click();
				    		Thread.sleep(500);
				    		//WebElement shopcount1 = BNBasicCommonMethods.findElement(driver, obj, "shopicon_count");
							String script1 = "return $('#bag_count').text()";
						    	String n1 = (String) ((JavascriptExecutor) driver).executeScript(script1, "shopcount1");
						    	String []prdtcnt1 = n1.split(" ");
						    	System.out.println(prdtcnt1[0]);
						    	log.add("The Bag count is "+prdtcnt[0]);
						    	pass("Product count is displayed on Bag icon at the header in the product detail page",log);
			    		}
			    		catch(Exception e)
			    		{
			    			WebElement ATBErrorAlt = driver.findElement(By.xpath("//*[@id='skMob_ErrorsDiv_id']"));
		  					if(ATBErrorAlt.isDisplayed())
		  					{
		  						WebElement ErroBtn = driver.findElement(By.xpath("//*[@id='skMob_ErrorsOK_id']"));
		  						ErroBtn.click();
		  						ctr++;
		  					}
			    		}
		    		
		    		}while(ctr<=3);
		    	}
		    	else if(!(Integer.parseInt(prdtcnt[0]) == 0))
		    	{
		    		System.out.println("The Product count in the bag is "+prdtcnt[0]);
		    		log.add("The Bag count is "+prdtcnt[0]);
		    		pass("Product count is displayed on Bag icon at the header in the product detail page",log);
		    	}
		    	else
		    	{
		    		fail("Product count is not displayed on Bag icon at the header in the product detail page");
		    	}
			
			/*boolean prdtadded = false;
			int count = 1;
			do
			{
					WebElement ShoppingBagCount = BNBasicCommonMethods.findElement(driver, obj, "shopicon_count");
					System.out.println(ShoppingBagCount.getText());
					String str  = ShoppingBagCount.getText();
					int prdtval = Integer.parseInt(str);
				    WebElement ShoppingBagIcon = BNBasicCommonMethods.findElement(driver, obj, "EmptyShoppingBagCount");
				
				if(prdtval>0)
				{
					prdtadded=true;
					ShoppingBagIcon = BNBasicCommonMethods.findElement(driver, obj, "EmptyShoppingBagCount");
					log.add("Product Count is shown");
					pass("product count is displayed on Bag icon at the header in the product detail page",log);
					ShoppingBagIcon.click();
					Thread.sleep(4000);
					BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
					WebElement LoginCheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "LoginCheckoutBtn");
					wait.until(ExpectedConditions.visibilityOf(LoginCheckoutBtn));
					Thread.sleep(1000);
					LoginCheckoutBtn.click();
					Thread.sleep(3000);
					WebElement signInIframe = BNBasicCommonMethods.findElement(driver, obj, "signInIframe");
					driver.switchTo().frame(signInIframe);
					wait.until(ExpectedConditions.attributeToBe(miniCartLoadingGauge, "style", "display: none;"));
				    wait.until(ExpectedConditions.visibilityOf(secureCheckOutBtn));
					break;
				}
				else
				{
					fail("product count is not displayed on Bag icon at the header in the product detail page");
					count++;
					prdtadded=false;
					PDPATBClick();
					continue;
				}
			}
			while(prdtadded==true||count<3);*/
			
		}
		catch(Exception e)
		{
			//pass("0 product count in the shopping bag icon is shown");
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
		
	/*BNIA361 - Verify that while tapping bag icon,it should navigate to shopping bag page */
	public void BNIA361()
	{
		ChildCreation("BNIA361 - Verify that while tapping bag icon,it should navigate to shopping bag page ");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
			WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			logo.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Searchtile");
			WebElement searchtile = BNBasicCommonMethods.findElement(driver, obj, "Searchtile");
			searchtile.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "ShopIcon");
			WebElement shopicon = BNBasicCommonMethods.findElement(driver, obj, "ShopIcon");
			shopicon.click();
			Thread.sleep(2500);
			try
			{
				BNBasicCommonMethods.waitforElement(wait, obj, "ShoppingBag_OrderSummary");
				WebElement ShoppingBagPage_Ordersummery =  BNBasicCommonMethods.findElement(driver, obj, "ShoppingBag_OrderSummary");
				if(ShoppingBagPage_Ordersummery.isDisplayed())
				{
					log.add("The Product Containing Shopping Bag page is shown");
					pass("while tapping bag icon,it navigates to shopping bag page",log);
				}
				else
				{
					fail("The Product Containing Shopping Bag page is not shown");
				}
			}
			catch(Exception e)
			{
				BNBasicCommonMethods.waitforElement(wait, obj, "ContinueShoppingbutton");
				WebElement ShoppingBag_ContinueButton = BNBasicCommonMethods.findElement(driver, obj, "ContinueShoppingbutton");
				if(ShoppingBag_ContinueButton.isDisplayed())
				{
					log.add("Empty Shopping Bag Page is Shown");
					
					pass("while tapping bag icon,it navigates to shopping bag page",log);
				}
				else
				{
					fail("while tapping bag icon,it does not navigates to shopping bag page");
				}
			}
			
				
			/*BNBasicCommonMethods.waitforElement(wait, obj, "ContinueShoppingbutton");
			WebElement continueshopping = BNBasicCommonMethods.findElement(driver, obj, "ContinueShoppingbutton");*/
			//WebElement coupon_code = BNBasicCommonMethods.findElement(driver, obj, "couponcode");
			/*boolean continueshopping = 
			boolean coupon_code = 
					BNBasicCommonMethods.isElementPresent(ContinueShoppingbutton);
					BNBasicCommonMethods.isElementPresent(couponcode);*/
			/*if((BNBasicCommonMethods.isElementPresent(continueshopping)))
			{
				pass("while tapping bag icon,it navigates to shopping bag page");
			}
			else
				fail("while tapping bag icon,it does not navigates to shopping bag page");*/
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA361\n"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA363 - Verify that  product image should be shown and fit to screen*/
	public void BNIA363()
	{
		ChildCreation("BNIA363 - Verify that  product image should be shown and fit to screen");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
			WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			logo.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Searchtile");
			BNBasicCommonMethods.findElement(driver, obj, "Searchtile").click();
			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			WebElement searchbox1 = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
			Thread.sleep(2000);
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String SearchEan  = BNBasicCommonMethods.getExcelNumericVal("BNIA363", sheet, 3);
			searchbox1.sendKeys(SearchEan);
			searchbox1.sendKeys(Keys.ENTER);
			/*BNBasicCommonMethods.waitforElement(wait, obj, "backarrow");
			BNBasicCommonMethods.findElement(driver, obj, "backarrow").click();
			BNBasicCommonMethods.waitforElement(wait, obj, element)*/
			BNBasicCommonMethods.waitforElement(wait, obj, "pdp_image");
			WebElement image = BNBasicCommonMethods.findElement(driver, obj, "pdp_image");
			image.click();
			Thread.sleep(1000);
			//BNBasicCommonMethods.waitforElement(wait, obj, "pdp_imageMask");
			WebElement Imask = BNBasicCommonMethods.findElement(driver, obj, "ImageClickEvent1");
			Actions actions = new Actions(driver);
			actions.moveToElement(Imask).click().build().perform();
			Imask.click();
			if(BNBasicCommonMethods.isElementPresent(image))
			{
				pass("product image is shown and fit to screen");
			}
			else
			{
				fail("product image is not shown and fit to screen");
			}
			/*Thread.sleep(1000);
			WebElement imageZoomed = BNBasicCommonMethods.findElement(driver, obj, "pdp_imageZoomed");
			if(BNBasicCommonMethods.isElementPresent(imageZoomed))
			{
				
			}*/
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA364 - Verify that  while tapping the image in the product detail page,it should be zoomed */
	public void BNIA364()
	{
		ChildCreation("BNIA364 - Verify that  while tapping the image in the product detail page,it should be zoomed ");
		try
		{
			WebElement image = BNBasicCommonMethods.findElement(driver, obj, "pdp_image");
			image.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "pdp_imageZoomed");
			WebElement imageZoomed = BNBasicCommonMethods.findElement(driver, obj, "pdp_imageZoomed");
			
			if(BNBasicCommonMethods.isElementPresent(imageZoomed))
			{
				pass("while tapping the image in the product detail page,it gets zoomed");
			}
			else
			{
				fail("while tapping the image in the product detail page,it does not gets zoomed");
			}
			
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA365 - Verify that while tapping anywhere in the product detail page,the image overlay should be closed*/
	public void BNIA365()
	{
		ChildCreation("BNIA365 - Verify that while tapping anywhere in the product detail page,the image overlay should be closed");
		try
		{
			/*WebElement image = BNBasicCommonMethods.findElement(driver, obj, "pdp_image");
			image.click();*/
			WebElement Imask = BNBasicCommonMethods.findElement(driver, obj, "ImageClickEvent1");
			Actions actions = new Actions(driver);
			actions.moveToElement(Imask).click().build().perform();
			Imask.click();
			/*BNBasicCommonMethods.waitforElement(wait, obj, "pdp_imageZoomed");
			WebElement imageZoomed = BNBasicCommonMethods.findElement(driver, obj, "pdp_imageZoomed");
			BNBasicCommonMethods.findElement(driver, obj, "pdp_imageMask").click();*/
			WebElement imagezoom_notactive = BNBasicCommonMethods.findElement(driver, obj, "pdp_imagenotactive");
			if(BNBasicCommonMethods.isElementPresent(imagezoom_notactive))
			{
				pass("while tapping anywhere in the product detail page,the image overlay gets closed");
			}
			else
			{
				fail("while tapping anywhere in the product detail page,the image overlay does not gets closed");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA366 - Verify that on tapping outside the image ,it should close the zoomed Image */
	public void BNIA366()
	{
		ChildCreation("BNIA366 - Verify that on tapping outside the image ,it should close the zoomed Image ");
		try
		{
			WebElement image = BNBasicCommonMethods.findElement(driver, obj, "pdp_image");
			image.click();
			WebElement Imask = BNBasicCommonMethods.findElement(driver, obj, "ImageClickEvent1");
			Actions actions = new Actions(driver);
			actions.moveToElement(Imask).click().build().perform();
			Imask.click();
			WebElement imagezoom_notactive = BNBasicCommonMethods.findElement(driver, obj, "pdp_imagenotactive");
			if(BNBasicCommonMethods.isElementPresent(imagezoom_notactive))
			{
				pass("On tapping outside the image ,it closed the zoomed Image");
			}
			else
			{
				fail("On tapping outside the image ,it does not closed the zoomed Image");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA367 - Verify that other images of the product should be displayed below the primary image in the product detail page*/
	public void BNIA367()
	{
		ChildCreation("BNIA367 - Verify that other images of the product should be displayed below the primary image in the product detail page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
			WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			logo.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Searchtile");
			WebElement searchtile =  BNBasicCommonMethods.findElement(driver, obj, "Searchtile");
			searchtile.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			WebElement sbox = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String SearchEAN = BNBasicCommonMethods.getExcelNumericVal("BNIA367", sheet, 3);
			sbox.sendKeys(SearchEAN);
			Thread.sleep(500);
			Actions act = new Actions(driver);
		    act.sendKeys(Keys.ENTER).build().perform();
			//sbox.sendKeys(Keys.ENTER);
			BNBasicCommonMethods.waitforElement(wait, obj, "secondaryimages");
			WebElement secImages = BNBasicCommonMethods.findElement(driver, obj, "secondaryimages");
			if(BNBasicCommonMethods.isElementPresent(secImages))
			{
				pass("other images of the product is displayed below the primary image in the product detail page");
			}
			else
			{
				fail("other images of the product is not displayed below the primary image in the product detail page");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA367 "+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA368 - Verify that tapped image should be shown enlarged when the image is selected from the list*/
	public void BNIA368()
	{
		ChildCreation("BNIA368 - Verify that tapped image should be shown enlarged when the image is selected from the list");
		try
		{
			Thread.sleep(1000);
			List<WebElement> PDPsec_images = driver.findElements(By.xpath("//*[@class='pdpImgAltImageCont']"));
			Random r = new Random();
			int new1 = r.nextInt(PDPsec_images.size());
			if(new1<1)
			{
				new1 = 1;
			}
			Thread.sleep(2000);
			WebElement Secondaryimage = driver.findElement(By.xpath("(//*[@class='pdpImgAltImage'])["+new1+"]"));
			Secondaryimage.click();
			WebElement PrimaryImage = BNBasicCommonMethods.findElement(driver, obj, "pdp_image");
			String PrimaryImageUrl = PrimaryImage.getAttribute("src");
			System.out.println(""+PrimaryImageUrl);
			String SecImageUrl = Secondaryimage.getAttribute("src");
			System.out.println(""+SecImageUrl);
			
			if(SecImageUrl.equals(PrimaryImageUrl))
			{
				pass("Selected image is displayed in the primary Image in PDP Page.");
			}
			else
			{
				fail("Selected image is not displayed in the Primay Image in PDP page ");
			}
			
			
			/*String str = "//prodimage.images-bn.com/pimages/0700304046703_p1_v3_s670x700.jpg";
			WebElement pdpimage = BNBasicCommonMethods.findElement(driver, obj, "pdp_image");*/
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA368 "+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA369 - Verify that Email this Item button should be displayed in the product detail page*/
	public void BNIA369()
	{
		ChildCreation("BNIA369 - Verify that Email this Item button should be displayed in the product detail page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "EmailButton");
			WebElement emailbutton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			if(BNBasicCommonMethods.isElementPresent(emailbutton))
			{
				pass("Email this Item button is displayed in the product detail page");
			}
			else
			{
				fail("Email this Item button is not displayed in the product detail page");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA369 "+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA370 - Verify that while tapping Email this Item button, the Email this item overlay should be displayed*/
	public void BNIA370()
	{
		ChildCreation("BNIA370 - Verify that while tapping Email this Item button, the Email this item overlay should be displayed");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "EmailButton");
			WebElement emailbutton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			emailbutton.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "EmailOverlay");
			WebElement emailoverlay = BNBasicCommonMethods.findElement(driver, obj, "EmailOverlay");
			if(BNBasicCommonMethods.isElementPresent(emailoverlay))
			{
				pass("while tapping Email this Item button, the Email this item overlay is displayed");
			}
			else
			{
				fail("while tapping Email this Item button, the Email this item overlay is not displayed");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA370 "+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA371 - Verify that  Email this Item overlay should be displayed in center at the product detail page*/
	public void BNIA371()
	{
		ChildCreation("BNIA371 - Verify that  Email this Item overlay should be displayed in center at the product detail page");
		try
		{
			//BNBasicCommonMethods.waitforElement(wait, obj, "EmailButton");
			//WebElement emailbutton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			//emailbutton.click();
			WebElement email_overlay = BNBasicCommonMethods.findElement(driver, obj, "EmailOverlay");
			System.out.println("The Email this Item Overlay Location is "+email_overlay.getLocation());
			
			WebElement ToEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailToAddress");
			WebElement FromEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailFromAddress");
			WebElement ToNamefield = BNBasicCommonMethods.findElement(driver, obj, "EmailToName");
			WebElement FromNameField = BNBasicCommonMethods.findElement(driver, obj, "EmailFromName");
			WebElement messagefield = BNBasicCommonMethods.findElement(driver, obj, "EmailMessageField");
			if((BNBasicCommonMethods.isElementPresent(ToEmailfield)) && (BNBasicCommonMethods.isElementPresent(FromEmailfield)) && (BNBasicCommonMethods.isElementPresent(ToNamefield)))
			{
				if((BNBasicCommonMethods.isElementPresent(FromNameField)) && (BNBasicCommonMethods.isElementPresent(messagefield)))
				{
					pass("Email this Item overlay is displayed in center at the product detail page");
				}
				else
				{
					fail("Email this Item overlay is not displayed in center at the product detail page");
				}
			}
			else
			{
				fail("Email this Item overlay is not displayed in center at the product detail page");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA371 "+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA372 - Verify that Email this item overlay should contains email text field & name text field for both the From & To section.*/
	public void BNIA372()
	{
		ChildCreation("BNIA372 - Verify that Email this item overlay should contains email text field & name text field for both the From & To section.");
		try
		{
			WebElement ToEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailToAddress");
			WebElement FromEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailFromAddress");
			WebElement ToNamefield = BNBasicCommonMethods.findElement(driver, obj, "EmailToName");
			WebElement FromNameField = BNBasicCommonMethods.findElement(driver, obj, "EmailToName");
			WebElement messagefield = BNBasicCommonMethods.findElement(driver, obj, "EmailMessageField");
			if((BNBasicCommonMethods.isElementPresent(ToEmailfield)) && (BNBasicCommonMethods.isElementPresent(FromEmailfield)) && (BNBasicCommonMethods.isElementPresent(ToNamefield)))
			{
				if((BNBasicCommonMethods.isElementPresent(FromNameField)) && (BNBasicCommonMethods.isElementPresent(messagefield)))
				{
					pass("Email this item overlay contains email text field & name text field for both the From & To section.");
				}
				else
				{
					fail("Email this item overlay does not contains email text field & name text field for both the From & To section.");
				}
			}
			else
			{
				fail("Email this item overlay does not contains email text field & name text field for both the From & To section.");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA372 "+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA373 - Verify that user able to enter in the text fields and it should accepts only valid formats (for email address fields: alphanumeric characters,symbols and special characters)(for name fields :characters and symbols)*/
	public void BNIA373()
	{		ChildCreation("BNIA373 - Verify that user able to enter in the text fields and it should accepts only valid formats (for email address fields: alphanumeric characters,symbols and special characters)(for name fields :characters and symbols)");
		try
		{
			WebElement ToEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailToAddress");
			WebElement FromEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailFromAddress");
			WebElement ToNamefield = BNBasicCommonMethods.findElement(driver, obj, "EmailToName");
			WebElement FromNameField = BNBasicCommonMethods.findElement(driver, obj, "EmailFromName");
			WebElement Alert1 = BNBasicCommonMethods.findElement(driver, obj, "EmailErrorAlert1");
			WebElement Alert2 = BNBasicCommonMethods.findElement(driver, obj, "EmailErrorAlert2");
			WebElement Alert3 = BNBasicCommonMethods.findElement(driver, obj, "EmailErrorAlert3");
			WebElement Alert4 = BNBasicCommonMethods.findElement(driver, obj, "EmailErrorAlert4");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String new1 = BNBasicCommonMethods.getExcelVal("BNIA373", sheet, 2);
			String newary[] = new1.split("\n");
			Actions act = new Actions(driver);
			act.click(ToEmailfield).sendKeys(newary[4]).build().perform();
			act.click(FromEmailfield).sendKeys(newary[5]).build().perform();
			WebElement emailbutton = BNBasicCommonMethods.findElement(driver, obj, "EmailShareButton");
			emailbutton.click();
			if((Alert1.getText().equals(newary[0])) && (!Alert3.getText().equals(newary[2])))
			{
				log.add("To Email Address field is invalid & From Email Address field is valid");
			}
			else
			{
				log.add("Something went wrong in To Email Address field & From Email Address field\n"+newary[0] +"\n"+newary[2]);
			}
		    ToEmailfield.clear();
			FromEmailfield.clear();
			act.click(ToEmailfield).sendKeys(newary[5]).build().perform();
			act.click(FromEmailfield).sendKeys(newary[6]).build().perform();
			emailbutton.click();
			if((Alert3.getText().equals(newary[2])) && (!Alert1.getText().equals(newary[0])))
			{
				log.add("To Email Address field is valid & From Email Address field is invalid");
			}
			else
			{
				log.add("Something went wrong in To Email Address field & From Email Address field\n"+newary[2] +"\n"+newary[1]);
			}
			ToEmailfield.clear();
			FromEmailfield.clear();
			act.click(ToEmailfield).sendKeys(newary[7]).build().perform();
			act.click(FromEmailfield).sendKeys(newary[8]).build().perform();
			emailbutton.click();
			if((Alert3.getText().equals(newary[2])) && (Alert1.getText().equals(newary[0])))
			{
				log.add("Both To Email Address field & From Email Address field is invalid");
			}
			else
			{
				log.add("Something went wrong in To Email Address field & From Email Address field\n"+newary[2] +"\n"+newary[0]);
			}
			ToEmailfield.clear();
			FromEmailfield.clear();
			
			act.click(ToNamefield).sendKeys(newary[9]).build().perform();
			act.click(FromNameField).sendKeys(newary[10]).build().perform();
			emailbutton.click();
			if((ToNamefield.getAttribute("value").isEmpty()) && (!Alert4.getText().equals(newary[3])))
			{
				log.add("To Name field is does not accepts special characters(empty) & From Name field is valid");
				
			}
			else
			{
				log.add("Something went wrong in To Name field & From Name field field\n"+newary[3]);
			}
			ToNamefield.clear();
			FromNameField.clear();
			
			act.click(ToNamefield).sendKeys(newary[11]).build().perform();
			act.click(FromNameField).sendKeys(newary[12]).build().perform();
			emailbutton.click();
			if((!Alert2.getText().equals(newary[1])) && (FromNameField.getAttribute("value").isEmpty()))
			{
				log.add("To Name field is valid & From Name field does not accepts numbers(empty)");
				
			}
			else
			{
				log.add("Something went wrong in To Name field & From Name field field\n"+newary[1]);
			}
			ToNamefield.clear();
			FromEmailfield.clear();
			
			act.click(ToNamefield).sendKeys(newary[13]).build().perform();
			act.click(FromEmailfield).sendKeys(newary[14]).build().perform();
			emailbutton.click();
			if((ToNamefield.getAttribute("value").isEmpty()) && (FromNameField.getAttribute("value").isEmpty()))
			{
				log.add("Both To Name field & From Name field does not accepts special characters and numbers(i.e.user is not able to enter special characters and numbers)");
				
				
			}
			else
			{
				log.add("Something went wrong in To Name field & From Name field field\n");
			}
			ToNamefield.clear();
			FromEmailfield.clear();
			ToEmailfield.clear();
			FromEmailfield.clear();
			emailbutton.click();
			if((Alert1.getText().equals(newary[0])) && (Alert3.getText().equals(newary[2])))
			{
				log.add("Both To Email Address and From Email Address is Empty");
				if((Alert2.getText().equals(newary[1])) && (Alert4.getText().equals(newary[3])))
				{
					log.add("Both To Name Field and From Name Field is Empty");
					pass("user able to enter in the text fields and it accepts only valid formats (for email address fields: alphanumeric characters,symbols and special characters)(for name fields :characters and symbols)",log);
				}
				else
				{
					fail("user not able to enter in the text fields and it does not accepts only valid formats (for email address fields: alphanumeric characters,symbols and special characters)(for name fields :characters and symbols)",log);
				}
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA373\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA374 - Verify that Message text box should be displayed with inline text optional in the PDP page*/
	public void BNIA374()
	{
		ChildCreation("BNIA374 - Verify that Message text box should be displayed with inline text optional in the PDP page");
		try
		{
			WebElement messagefield = BNBasicCommonMethods.findElement(driver, obj, "EmailMessageField");
			String EmailInlinetext = messagefield.getAttribute("placeholder");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String EmailTextboxText = BNBasicCommonMethods.getExcelVal("BNIA374", sheet, 2);
			System.out.println(""+EmailTextboxText);
			if((BNBasicCommonMethods.isElementPresent(messagefield)) && (EmailInlinetext.equals(EmailTextboxText)))
			{
				log.add("Message text box is present and inline text is shown\n"+EmailTextboxText);
				pass("Message text box is displayed with inline text optional in the PDP page",log);
			}
			else
			{
				fail("Message text box is not displayed with inline text optional in the PDP page");
			}
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong in BNIA374\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA375 - Verify that inline optional field should accept maximum of 250 characters*/
	public void BNIA375()
	{
		ChildCreation("BNIA375 - Verify that inline optional field should accept maximum of 250 characters");
		try
		{
			WebElement messagefield = BNBasicCommonMethods.findElement(driver, obj, "EmailMessageField");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String MessageFieldText = BNBasicCommonMethods.getExcelVal("BNIA375", sheet, 2); 
			messagefield.sendKeys(MessageFieldText);
			int fieldlength = messagefield.getAttribute("value").length();
			if(fieldlength<=250)
			{
				log.add("The inline field accepts less than or maximum of 250 characters");
			}
			else
			{
				log.add("The inline field accepts more than 250 characters");
			}
			messagefield.clear();
			Thread.sleep(1000);
			//String val = "Optional (Maximum 250 characters)";
			if(messagefield.getAttribute("value").isEmpty())
			{
				log.add("The inline field is empty");
			}
			else
			{
				return;
			}
			messagefield.sendKeys("Software testing is an investigation conducted to provide stakeholders with information "
					+ "about the quality of the product or service under test.Software testing can also provide an objective,"
					+ "For example, in a phased process,most testing occurs systemsSoftware testing involves the execution of "
					+ "a software component or system component to evaluate one or more properties of interest. In general, these"
					+ " properties indicate the extent to which the component or system under test:");
			int fieldlength1 = messagefield.getAttribute("value").length();
			if(fieldlength1==250)
			{
				log.add("The inline field does not accepts more than 250 characters");
				pass("The inline optional field accepts maximum of 250 characters",log);
			}
			else
			{
				fail("The inline optional field does not accepts maximum of 250 characters");
			}
			messagefield.clear();
		}
		catch(Exception e)
		{
        	 e.getMessage();
 			System.out.println("Something went wrong"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA376 - Verify that on selecting send button after entering valid email address and name, success alert should be displayed.*/
	public void BNIA376()
	{
		ChildCreation("BNIA376 - Verify that on selecting send button after entering valid email address and name, success alert should be displayed.");
		try
		{
			WebElement ToEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailToAddress");
			WebElement FromEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailFromAddress");
			WebElement ToNamefield = BNBasicCommonMethods.findElement(driver, obj, "EmailToName");
			WebElement FromNameField = BNBasicCommonMethods.findElement(driver, obj, "EmailFromName");
			WebElement messagefield = BNBasicCommonMethods.findElement(driver, obj, "EmailMessageField");
			WebElement EmailButton = BNBasicCommonMethods.findElement(driver, obj, "EmailShareButton");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String values = BNBasicCommonMethods.getExcelVal("BNIA376", sheet, 2);
			String val[]  = values.split("\n");
			ToEmailfield.sendKeys(val[0]);
			ToNamefield.sendKeys(val[1]);
			Thread.sleep(1000);
			final String ToName_field = ToNamefield.getAttribute("value");
			FromEmailfield.sendKeys(val[2]);
			FromNameField.sendKeys(val[3]);
			messagefield.sendKeys(val[4]);
			EmailButton.click();
			Thread.sleep(2000);
			WebElement emailonthewayOverlay = BNBasicCommonMethods.findElement(driver, obj, "EmailOntheWayOverlay");
			WebElement emailonthewayalert = BNBasicCommonMethods.findElement(driver, obj, "EmailOntheWayAlert");
			WebElement SuccessAlertText = BNBasicCommonMethods.findElement(driver, obj, "EmailSentTextAlert");
			String ErrorAlert = val[5]; 
			String techAlert = emailonthewayalert.getText();
			String success_Alert = SuccessAlertText.getText();
			//ToNamefield = BNBasicCommonMethods.findElement(driver, obj, "EmailToName");
			System.out.println(""+success_Alert);
			String Successalert = "Your email to "+ToName_field+" has been sent successfully.";
			//String onthewayAlert = emailonthewayalert.getText();
			//String onthewayAlert1 = val[6];
			if(BNBasicCommonMethods.isElementPresent(emailonthewayOverlay))
			{
				if(Successalert.equals(success_Alert))
				{
					log.add("Email Sent Successfully");
					pass("On selecting send button after entering valid email address and name, success alert gets displayed.",log);
				}
				else if(techAlert.equals(ErrorAlert))
				{
						fail("Email not sent" +ErrorAlert+"Alert is shown");
				}
				else
				{
					fail("Email not sent - fail");
				}
			}
			else 
			{
				fail("Email is not sent");
			}
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA376\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA377 - Verify that  while tapping send by entering invalid email address and  names then Invalid message should be displayed as per the creative*/
	public void BNIA377()
	{
		ChildCreation("BNIA377 - Verify that  while tapping send by entering invalid email address and  names then Invalid message should be displayed as per the creative");
		try
		{
			WebElement Emailcloseicon = BNBasicCommonMethods.findElement(driver, obj, "EmailSuccessOverlay_Closeicon");
			Emailcloseicon.click();
			WebElement emailbutton1 = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			emailbutton1.click();
			WebElement ToEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailToAddress");
			WebElement FromEmailfield = BNBasicCommonMethods.findElement(driver, obj, "EmailFromAddress");
			WebElement ToNamefield = BNBasicCommonMethods.findElement(driver, obj, "EmailToName");
			WebElement FromNameField = BNBasicCommonMethods.findElement(driver, obj, "EmailFromName");
			WebElement Alert1 = BNBasicCommonMethods.findElement(driver, obj, "EmailErrorAlert1");
			//WebElement Alert2 = BNBasicCommonMethods.findElement(driver, obj, "EmailErrorAlert2");
			WebElement Alert3 = BNBasicCommonMethods.findElement(driver, obj, "EmailErrorAlert3");
			//WebElement Alert4 = BNBasicCommonMethods.findElement(driver, obj, "EmailErrorAlert4");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String new1 = BNBasicCommonMethods.getExcelVal("BNIA373", sheet, 2);
			String newary[] = new1.split("\n");
			ToEmailfield.sendKeys(newary[15]);
			FromEmailfield.sendKeys(newary[16]);
			String alert1 = newary[0];
			String alert2 = newary[2];
			//String alert3 = newary[3];
			//String alert4 = newary[4];
			WebElement emailbutton = BNBasicCommonMethods.findElement(driver, obj, "EmailShareButton");
			emailbutton.click();
			//String alt1 = Alert1.getText();
			//String alt3 = Alert3.getText();
			if((Alert1.getText().equals(alert1)) && (Alert3.getText().equals(alert2)))
			{
				log.add("Both To Email Address and From Email Address is Empty");
				if((ToNamefield.getAttribute("value").isEmpty()) && ((FromNameField.getAttribute("value").isEmpty())))
				{
					log.add("Both To Name Field and From Name Field is Empty");
					pass("user able to enter in the text fields and it accepts only valid formats (for email address fields: alphanumeric characters,symbols and special characters)(for name fields :characters and symbols)",log);
				}
				else
				{
					fail("user not able to enter in the text fields and it does not accepts only valid formats (for email address fields: alphanumeric characters,symbols and special characters)(for name fields :characters and symbols)",log);
				}
			}
			ToEmailfield.clear();
			FromEmailfield.clear();
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA377\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA378 - Verify that send button should be highlighted by default in the PDP page*/
	public void BNIA378()
	{
		ChildCreation("BNIA378 - Verify that send button should be highlighted by default in the PDP page");
		try
		{
			WebElement EmailButton = BNBasicCommonMethods.findElement(driver, obj, "EmailShareButton");
			String email_sharebutton = EmailButton.getCssValue("background-color");
			System.out.println(""+email_sharebutton);
			Color emailsharebuttoncolor = Color.fromString(email_sharebutton);
			String hexemail_sharebutton = emailsharebuttoncolor.asHex();
			System.out.println(""+hexemail_sharebutton);
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String btncolor = BNBasicCommonMethods.getExcelVal("BNIA378", sheet, 6);
			if(hexemail_sharebutton.equals(btncolor))
			{
				log.add("Email Share Button is highlighted in green color" +"("+btncolor+")");
				pass("Send button is highlighted by default in the PDP page",log);
			}
			else
			{
				fail("Send button is not highlighted by default in the PDP page",log);
			}
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA378\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA380 - Verify that while tapping cancel button, the overlay should be closed and remains in the PDP page*/
	public void BNIA380()
	{
		ChildCreation("BNIA380 - Verify that while tapping cancel button, the overlay should be closed and remains in the PDP page");
		try
		{
			WebElement cancelbtn = BNBasicCommonMethods.findElement(driver, obj, "EmailCancelButton");
			cancelbtn.click();
			WebElement pdpimage = BNBasicCommonMethods.findElement(driver, obj, "pdp_image");
			if(BNBasicCommonMethods.isElementPresent(pdpimage))
			{
				pass("while tapping cancel button, the overlay gets closed and remains in the PDP page");
			}
			else
			{
				fail("while tapping cancel button, the overlay does not gets closed");
			}
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA380\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA381 - Verify that overview link should be displayed with right arrow symbol in the product detail page*/
	public void BNIA381()
	{
		ChildCreation("BNIA381 - Verify that overview link should be displayed with right arrow symbol in the product detail page");
		try
		{
			 BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
			 WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
			 logo.click();
			 BNBasicCommonMethods.waitforElement(wait, obj, "Searchtile");
			 BNBasicCommonMethods.findElement(driver, obj, "Searchtile").click();
			 BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			 WebElement searchbox1 = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
			 sheet = BNBasicCommonMethods.excelsetUp("PDP");
			 String EAN = BNBasicCommonMethods.getExcelNumericVal("BNIA363", sheet, 3);
			 searchbox1.sendKeys(EAN);
			 Thread.sleep(500);
			 Actions act = new Actions(driver);
			 act.sendKeys(Keys.ENTER).build().perform();
			// searchbox1.sendKeys(Keys.ENTER);
			 BNBasicCommonMethods.waitforElement(wait, obj, "PDPOverviewLink");
			 WebElement overviewlink = BNBasicCommonMethods.findElement(driver, obj, "PDPOverviewLink");
			 BNBasicCommonMethods.waitforElement(wait, obj, "PDPOverviewLink_Arrow");
			 WebElement overviewlink_arrow = BNBasicCommonMethods.findElement(driver, obj, "PDPOverviewLink_Arrow");
			 if((BNBasicCommonMethods.isElementPresent(overviewlink)) && (BNBasicCommonMethods.isElementPresent(overviewlink_arrow)))
			 {
				 pass("overview link is displayed with right arrow symbol in the product detail page");
			 }
			 else
			 {
				 fail("overview link is not displayed with right arrow symbol in the product detail page");
			 }
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA381\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA382 - Verify that while tapping Customer also Bought link the Customer Who Bought This also Bought section should be displayed*/
	public void BNIA382()
	{
		ChildCreation("BNIA382 - Verify that while tapping Customer also Bought link the Customer Who Bought This also Bought section should be displayed");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomeralsoBoughtLink");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomeralsoBoughtLink_Arrow");
			WebElement customeralsoboughtlink = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomeralsoBoughtLink");
			Thread.sleep(1500);
			customeralsoboughtlink.click();
			Thread.sleep(4500);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerAlsoBoughtThisSection");
			WebElement customeralsoboughtthissection = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerAlsoBoughtThisSection");
				if((BNBasicCommonMethods.isElementPresent(customeralsoboughtthissection)))
				{
					pass("while tapping Customer also Bought link the Customer Who Bought This also Bought section is displayed");
				}
				else
				{
					fail("while tapping  Customer also Bought link the Customer Who Bought This also Bought section is not displayed");
				}
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA382\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA383 - Verify that Customer Who Bought This also Bought section should contains list of Products with product image,product name,Author name & rating stars */
	public void BNIA383()
	{
		ChildCreation("BNIA383 - Verify that Customer Who Bought This also Bought section should contains list of Products with product image,product name,Author name & rating stars ");
		try
		{
			Thread.sleep(3000);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerAlsoBoughtThisSection_ShowMoreLink");
			WebElement PDP_cusalsobought_showmore = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerAlsoBoughtThisSection_ShowMoreLink");
			PDP_cusalsobought_showmore.click();
			Thread.sleep(2000);
			//List<WebElement> CustomeralsoboughtProductsImg = driver.findElements(By.xpath("//*[contains(@class, 'sk_mobRecommendedItemImgLink')]"));
			List<WebElement> CustomeralsoboughtProductsTitle = driver.findElements(By.xpath("//*[contains(@class,  'sktab_pdtGridItemTitle')]"));
			List<WebElement> CustomeralsoboughtProductsAuthorName = driver.findElements(By.xpath("//*[contains(@class,  'pdpSuggestedBkAuthor')]"));
			//List<WebElement> CustomeralsoboughtProductsRatings = driver.findElements(By.xpath("//*[contains(@class,  'sktab_pdtGrid_zeroRatingStar')]"));
			for(int i = 1;i<=CustomeralsoboughtProductsTitle.size();i++)
			{
				String str = driver.findElement(By.xpath("(//*[@class='sktab_pdtGridItemTitle'])["+i+"]")).getText();
				//(//*[@class='sktab_pdtGridItemTitle'])[+"i"+]
				System.out.println(""+str);
				if(driver.findElement(By.xpath("(//*[@class='sktab_pdtGridItemTitle'])["+i+"]")).isDisplayed())
				{
					log.add(""+str +"Title is shown");
					pass("Product Title is shown for the corresponding products in Customer Who Bought This also Bought Section",log);
				}
				else
				{
					fail("Product Title is not shown");
				}
			}	
					
				for(int j = 1;j<=CustomeralsoboughtProductsAuthorName.size();j++)
				{
					String str1 = driver.findElement(By.xpath("(//*[@class='pdpSuggestedBkAuthor'])["+j+"]")).getText();
					System.out.println(""+str1);
					if(driver.findElement(By.xpath("(//*[@class='pdpSuggestedBkAuthor'])["+j+"]")).isDisplayed())
					{
						log.add("+str1+" +"Author Name is shown");
						pass("Author Name is shown for the corresponding products in Customer Who Bought This also Bought Section",log);
					}
					else
					{
						fail("Author Name is not shown");
					}
				}
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA383\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA384 - Verify that Customer Review link should be displayed with right arrow symbol in Customer Who Bought This also Bought section*/
	public void BNIA384()
	{
		ChildCreation("BNIA384 - Verify that Customer Review link should be displayed with right arrow symbol in Customer Who Bought This also Bought section");
		try
		{
			 BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomerReviewsLink");
			 WebElement CustomerReviewsLink = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerReviewsLink");
			 BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomerReviewsLink_Arrow");
			 WebElement customerReviewsLink_arrow = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerReviewsLink_Arrow");
			 if((BNBasicCommonMethods.isElementPresent(CustomerReviewsLink)) && (BNBasicCommonMethods.isElementPresent(customerReviewsLink_arrow)))
			 {
				 pass("Customer Review link is displayed with right arrow symbol in Customer Who Bought This also Bought section");
			 }
			 else
			 {
				 fail("Customer Review link is not displayed with right arrow symbol in Customer Who Bought This also Bought section");
			 }
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA384\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	//BNIA385 - Verify that while tapping Customer Review link,the Customer Review section should be displayed
	public void BNIA385()
	{
		ChildCreation("BNIA385 - Verify that while tapping Customer Review link,the Customer Review section should be displayed");
		try
		{
			WebElement CustomerReviewsLink = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerReviewsLink");
			CustomerReviewsLink.click();
			WebElement customerReviewsSection = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsSection");
			if(BNBasicCommonMethods.isElementPresent(customerReviewsSection))
			{
				pass("while tapping  Customer Review link,the Customer Review section is displayed");
			}
			else
			{
				fail("while tapping  Customer Review link,the Customer Review section is not displayed");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA385\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	//BNIA386 - Verify that Customer Review Section should contains Rating stars(with Highlighted ratings),Review Description,username and Date of review in list view
	public void BNIA386()
	{
		ChildCreation("BNIA386 - Verify that Customer Review Section should contains Rating stars(with Highlighted ratings),Review Description,username and Date of review in list view");
		try
		{
			List<WebElement> CustomerReviewsRatings = driver.findElements(By.xpath("//*[contains(@class, 'pdpReviewListRatingsImg')]"));
			List<WebElement> CustomerReviewsDescription = driver.findElements(By.xpath("//*[contains(@class, 'pdpReviewListValueTxt')]"));
			List<WebElement> CustomerReviews_Name = driver.findElements(By.xpath("//*[contains(@class, 'pdpReviewListLabelTxt')]"));
			List<WebElement> CustomerReviews_Date = driver.findElements(By.xpath("//*[contains(@class, 'pdpReviewListDateTxt')]"));
			
			
			for(int i=1;i<=CustomerReviewsRatings.size();i++)
			{
				String str = driver.findElement(By.xpath("(//*[@class='pdpReviewListRatingsImg'])["+i+"]")).getAttribute("aria-label");
				
				System.out.println(""+str);
				if(driver.findElement(By.xpath("(//*[@class='pdpReviewListRatingsImg'])["+i+"]")).isDisplayed())
				{
					log.add("+str+"+ "is shown");
					pass("Customer Ratings is shown in the Customer Reviews Section",log);
				}
				else
				{
					fail("Customer Ratings is not shown Customer Reviews Section");
				}
			}	
				for(int j=1;j<=CustomerReviewsDescription.size();j++)
				{
					String str1 = driver.findElement(By.xpath("(//*[@class='pdpReviewListValueTxt'])["+j+"]")).getText();
					System.out.println(""+str1);
					if(driver.findElement(By.xpath("(//*[@class='pdpReviewListValueTxt'])["+j+"]")).isDisplayed())
					{
						log.add("+str1+\n" +"is shown in the Customer reviews Section");
						pass("Customer Reviews is shown in the Customer Reviews Section",log);
					}
					else
					{
						fail("Customer Reviews is not shown Customer Reviews Section");
					}
				}
			
			for(int m=1;m<=CustomerReviews_Name.size();m++)
			{
				String str = driver.findElement(By.xpath("(//*[@class='pdpReviewListLabelTxt'])["+m+"]")).getText();
				System.out.println(""+str);
				if(driver.findElement(By.xpath("(//*[@class='pdpReviewListLabelTxt'])["+m+"]")).isDisplayed())
				{
					log.add("+str+\n"+ "is shown");
					pass("Customer Name is shown in the Customer Reviews Section",log);
				}
				else
				{
					fail("Customer Name is not shown Customer Reviews Section");
				}
			}	
				for(int n=1;n<=CustomerReviews_Date.size();n++)
				{
					String str1 = driver.findElement(By.xpath("(//*[@class='pdpReviewListDateTxt'])["+n+"]")).getText();
					System.out.println(""+str1);
					if(driver.findElement(By.xpath("(//*[@class='pdpReviewListDateTxt'])["+n+"]")).isDisplayed())
					{
						log.add("+str1+\n"+"is shown");
						pass("Customer Name is shown in the Customer Reviews Section",log);
					}
					else
					{
						fail("Customer Name is not shown Customer Reviews Section");
					}
				}
			}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA386\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	//BNIA387 - Verify that  "Editorial Reviews" link should be displayed with right arrow symbol in "Customer Review" section
	public void BNIA387()
	{
		ChildCreation("BNIA387 - Verify that Editorial Reviews link should be displayed with right arrow symbol in Customer Review section");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomerEditorialReviewsLink");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomerEditorialReviewsLink_Arrow");
			WebElement editorialreviewslink = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerEditorialReviewsLink");
			WebElement editorialreviewsarrow = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerEditorialReviewsLink_Arrow");
			if((BNBasicCommonMethods.isElementPresent(editorialreviewslink)) && (BNBasicCommonMethods.isElementPresent(editorialreviewsarrow)))
			{
				pass("Editorial Reviews link is displayed with right arrow symbol in Customer Review section");
			}
			else
			{
				fail("Editorial Reviews link is not displayed with right arrow symbol in Customer Review section");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA387\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	//BNIA388 - Verify that while tapping Editorial Reviews link ,the Editorial Reviews section should be displayed.
	public void BNIA388()
	{
		ChildCreation("BNIA388 - Verify that while tapping Editorial Reviews link ,the Editorial Reviews section should be displayed");
		try
		{
			WebElement editorialreviewslink = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerEditorialReviewsLink");
			editorialreviewslink.click();
			Thread.sleep(2000);
			WebElement editorialreviewssection = BNBasicCommonMethods.findElement(driver, obj, "PDP_EditorialReviewsSection");
			if((BNBasicCommonMethods.isElementPresent(editorialreviewssection)))
			{
				pass("While tapping Editorial Reviews link ,the Editorial Reviews section is displayed");
			}
			else
			{
				fail("While tapping Editorial Reviews link ,the Editorial Reviews section is not displayed");
			}
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA388\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	//BNIA389 - Verify that Editorial Reviews  section should contains Editorial Reviews Description.
	public void BNIA389()
	{
		ChildCreation("BNIA389 - Verify that Editorial Reviews  section should contains Editorial Reviews Description");
		try
		{
			WebElement editorialreviews_title = BNBasicCommonMethods.findElement(driver, obj, "PDP_EditorialReviewsSection_Title");
			WebElement editorialreviews_description = BNBasicCommonMethods.findElement(driver, obj, "PDP_EditorialReviewsSection_Description");
			if((BNBasicCommonMethods.isElementPresent(editorialreviews_title)) && (BNBasicCommonMethods.isElementPresent(editorialreviews_description)))
			{
				log.add("Editorial reviews description and Title is shown");
				pass("Editorial Reviews section contains Editorial Reviews Description",log);
			}
			else
			{
				fail("Editorial Reviews section does not contains Editorial Reviews Description");
			}
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA389"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA390 - Verify that Meet the Author link should be displayed with right arrow symbol in Editorial Reviews section.*/	
	public void BNIA390()
	{
		ChildCreation("BNIA390 - Verify that Meet the Author link should be displayed with right arrow symbol in Editorial Reviews section");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPMeetTheAuthorLink");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPMeetTheAuthorLink_Arrow");
			WebElement meettheauthor = BNBasicCommonMethods.findElement(driver, obj, "PDPMeetTheAuthorLink");
			WebElement meettheauthorarrow = BNBasicCommonMethods.findElement(driver, obj, "PDPMeetTheAuthorLink_Arrow");
			if((BNBasicCommonMethods.isElementPresent(meettheauthor)) && (BNBasicCommonMethods.isElementPresent(meettheauthorarrow)))
			{
				pass("Meet the Author link is displayed with right arrow symbol in Editorial Reviews section");
			}
			else
			{
				fail("Meet the Author link is not displayed with right arrow symbol in Editorial Reviews section");
			}
		}
		catch(Exception e)
		{
            System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA390\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA391 - Verify that while tapping Meet the Author link,the Meet the Author section should be displayed*/
	public void BNIA391()
	{
		ChildCreation("BNIA391 - Verify that while tapping Meet the Author link,the Meet the Author section should be displayed");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPMeetTheAuthorLink");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPMeetTheAuthorLink_Arrow");
			WebElement meettheauthorlink = BNBasicCommonMethods.findElement(driver, obj, "PDPMeetTheAuthorLink");
			meettheauthorlink.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_MeetTheAuthorSection");
			WebElement meettheauthorsection = BNBasicCommonMethods.findElement(driver, obj, "PDP_MeetTheAuthorSection");
			WebElement meettheauthorsection_desc = BNBasicCommonMethods.findElement(driver, obj, "PDP_MeetTheAuthorSection_Description");
			if(BNBasicCommonMethods.isElementPresent(meettheauthorsection) && (BNBasicCommonMethods.isElementPresent(meettheauthorsection_desc)))
			{
				log.add("Author Description is shown");
				pass("While tapping Meet the Author link,the Meet the Author section is displayed",log);
			}
			else
			{
				fail("While tapping Meet the Author link,the Meet the Author section is not displayed");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA391\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA392 - Verify that Meet the Author section should contains Author image & Meet The Author description*/
	public void BNIA392()
	{
		ChildCreation("BNIA392 - Verify that Meet the Author section should contains Author image & Meet The Author description");
		try
		{
			WebElement meettheauthorsection_desc = BNBasicCommonMethods.findElement(driver, obj, "PDP_MeetTheAuthorSection_Description");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_MeetTheAuthorSection_Image");
			WebElement meettheauthorsection_img = BNBasicCommonMethods.findElement(driver, obj, "PDP_MeetTheAuthorSection_Image");
			if((BNBasicCommonMethods.isElementPresent(meettheauthorsection_desc)) && (BNBasicCommonMethods.isElementPresent(meettheauthorsection_img)))
			{
				pass("Meet the Author section contains Author image & Meet The Author description");
			}
			else
			{
				fail("Meet the Author section does not contains Author image & Meet The Author description");
			}
			
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA392\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	public void BNIA393()
	{
		ChildCreation("BNIA393 - Verify that Product Details link should be displayed with right arrow symbol in Meet the Author section");
		try
		{
			WebElement prdDetailsLink = BNBasicCommonMethods.findElement(driver, obj, "PDPProductDetailsLink");
			WebElement prdDetailsArrow = BNBasicCommonMethods.findElement(driver, obj, "PDPProductDetailsLink_Arrow");
			if((BNBasicCommonMethods.isElementPresent(prdDetailsLink)) && (BNBasicCommonMethods.isElementPresent(prdDetailsArrow)))
			{
				pass("Product Details link is displayed with right arrow symbol in Meet the Author section");
			}
			else
			{
				fail("Product Details link is not displayed with right arrow symbol in Meet the Author section");
			}
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA393\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA394 - Verify that while tapping Product Details link,the Product Details section should be displayed*/
	public void BNIA394()
	{
		ChildCreation("BNIA394 - Verify that while tapping Product Details link,the Product Details section should be displayed");
		try
		{
			WebElement prdDetailsLink = BNBasicCommonMethods.findElement(driver, obj, "PDPProductDetailsLink");
			prdDetailsLink.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductDetailsSection");
			WebElement prddetailssection = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductDetailsSection");
			if((BNBasicCommonMethods.isElementPresent(prddetailssection)))
			{
				pass("While tapping Product Details link,the Product Details section is displayed");
			}
			else
			{
				fail("While tapping Product Details link,the Product Details section is not displayed");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA394\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA395 - Verify that Product Details link should contains product description*/
	public void BNIA395()
	{
		ChildCreation("BNIA395 - Verify that Product Details link should contains product description");
		try
		{
			List<WebElement> prddetailssection_prddetailsLabel = driver.findElements(By.xpath("//*[contains(@class,  'sk_productDetailsLabel')]"));
			List<WebElement> prddetailssection_prddetailsValue = driver.findElements(By.xpath("//*[contains(@class,  'sk_productDetailsValue')]"));
			for(int i=1 , j=1 ; (i<=prddetailssection_prddetailsLabel.size()) &&(j<= prddetailssection_prddetailsValue.size()) ;i++ ,j++)
			{
				String str1 = driver.findElement(By.xpath("(//*[@class='sk_productDetailsLabel'])["+i+"]")).getText();
				String str2 = driver.findElement(By.xpath("(//*[@class='sk_productDetailsValue'])["+i+"]")).getText();
				System.out.println(""+str1);
				System.out.println(""+str2);
				if(driver.findElement(By.xpath("(//*[@class='sk_productDetailsLabel'])["+i+"]")).isDisplayed() && driver.findElement(By.xpath("(//*[@class='sk_productDetailsValue'])["+j+"]")).isDisplayed())
				{
					log.add(str1+" : "+str2);
					pass("Product Details is shown in the Product Details Section",log);
				}
				else
				{
					fail("Product Details Label is not shown in the Product Details Section");
				}
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA395\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA396 - Verify that Related Subject link should be displayed with right arrow symbol in Product Details section*/
	public void BNIA396()
	{
		ChildCreation("BNIA396 - Verify that Related Subject link should be displayed with right arrow symbol in Product Details section");
		try
		{
			WebElement relatedsubjectslink = BNBasicCommonMethods.findElement(driver, obj, "PDPRelatedSubjectsLink");
			WebElement relatedsubjectsArrow = BNBasicCommonMethods.findElement(driver, obj, "PDPRelatedSubjectsLink_Arrow");
			if((BNBasicCommonMethods.isElementPresent(relatedsubjectslink)) && (BNBasicCommonMethods.isElementPresent(relatedsubjectsArrow)))
			{
				pass("Related Subject link is displayed with right arrow symbol in Product Details section");
			}
			else
			{
				fail("Related Subject link is not displayed with right arrow symbol in Product Details section");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA396"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA397 - Verify that while tapping Related Subject link,the Related Subject section should be displayed*/
	public void BNIA397()
	{
		ChildCreation("BNIA397 - Verify that while tapping Related Subject link,the Related Subject section should be displayed");
		try
		{
			Thread.sleep(1500);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPRelatedSubjectsLink");
			WebElement relatedsubjectslink = BNBasicCommonMethods.findElement(driver, obj, "PDPRelatedSubjectsLink");
			relatedsubjectslink.click();
			Thread.sleep(3000);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_RelatedSubjectsSection_Link");
			WebElement relatedsubjectslink_link = BNBasicCommonMethods.findElement(driver, obj, "PDP_RelatedSubjectsSection_Link");
			if((BNBasicCommonMethods.isElementPresent(relatedsubjectslink)) && (BNBasicCommonMethods.isElementPresent(relatedsubjectslink_link)))
			{
				pass("While tapping Related Subject link,the Related Subject section is displayed");
			}
			else
			{
				fail("While tapping Related Subject link,the Related Subject section is not displayed");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA397\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA398 - Verify that Related Subject section should contains Related Subject description(links)*/
	public void BNIA398()
	{
		ChildCreation("BNIA398 - Verify that Related Subject section should contains Related Subject description(links)");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_RelatedSubjectsSection_Link");
			WebElement relatedsubjectslink_link = BNBasicCommonMethods.findElement(driver, obj, "PDP_RelatedSubjectsSection_Link");
			if((BNBasicCommonMethods.isElementPresent(relatedsubjectslink_link)))
			{
				pass("Related Subject section contains Related Subject description(links)");
			}
			else
				fail("Related Subject section does not contains Related Subject description(links)");
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA398n\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA399 - Verify that product name should be displayed at the top center in the product detail page*/
	public void BNIA399()
	{
		ChildCreation("BNIA399 - Verify that product name should be displayed at the top center in the product detail page");
		try
		{
			WebElement EmailButton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView(true);",EmailButton);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductName");
			WebElement PDP_ProductName = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductName");
			if(PDP_ProductName.isDisplayed())
			{
			   log.add("Displayed product title is "+PDP_ProductName.getText());
			   pass("Product title is displayed successfully",log);
				
			}
			else
			{
			   fail("product title is not displayed successfully");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA399\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA400 - Verify that Author name should be displayed below the product title in the product detail page*/
	public void BNIA400()
	{
		ChildCreation("BNIA400 - Verify that Author name should be displayed below the product title in the product detail page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AuthorName");
			WebElement pdp_prdname = BNBasicCommonMethods.findElement(driver, obj, "PDP_AuthorName");
			if((BNBasicCommonMethods.isElementPresent(pdp_prdname)))
			{
				log.add("Displayed Author Name is "+pdp_prdname.getText());
				pass("Author name is displayed below the product title in the product detail page",log);
			}
			else
			{
				fail("Author name is not displayed below the product title in the product detail page");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA400\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA401 - Verify that  product name should be highlighted in Bold*/
	public void BNIA401()
	{
		ChildCreation("BNIA401 - Verify that  product name should be highlighted in Bold");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductName");
			WebElement prdName = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductName");
			String val = prdName.getCssValue("font-family");
			System.out.println(""+val);
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String str = BNBasicCommonMethods.getExcelVal("BNIA401", sheet, 4);
			if(str.contains(val))
			{
				log.add("The Product Title Font is\n"+val);
				pass("Product name is highlighted in Bold",log);
			}
			else
			{
				fail("Product name is not highlighted in Bold");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA401\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA402 - Verify that Rating and Review Link should be displayed in the product detail page*/
	public void BNIA402()
	{
		ChildCreation("BNIA402 - Verify that Rating and Review Link should be displayed in the product detail page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_Ratings");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_Reviews");
			WebElement pdpRatings = BNBasicCommonMethods.findElement(driver, obj, "PDP_Ratings");
			WebElement pdpReviews = BNBasicCommonMethods.findElement(driver, obj, "PDP_Reviews");
			if((BNBasicCommonMethods.isElementPresent(pdpRatings)) && (BNBasicCommonMethods.isElementPresent(pdpReviews)))
			{
				pass("Rating and Review Link is displayed in the product detail page");
			}
			else
			{
				fail("Rating and Review Link is not displayed in the product detail page");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA402\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	public void BNIA403()
	{
		ChildCreation("BNIA403 - Verify that rating should have maximum of 5 stars and it should be highlighted depends on rating");
		try
		{
			WebElement pdpRatings = BNBasicCommonMethods.findElement(driver, obj, "PDP_Ratings");
			String size = pdpRatings.getSize().toString();
			System.out.println(""+size);
			String count = pdpRatings.getAttribute("aria-label");
			if(size=="5")
			{
				log.add("The Current raing is\n"+count);
				pass("Rating have maximum of 5 stars and it is highlighted depends on rating",log);
			}
			else
				fail("Rating does not have maximum of 5 stars and it is not highlighted depends on rating");
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA403\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA405 - Verify that All Formats & Editions page should display Paperback, Hardcover, NOOK Book, Audiobook tab switching options.*/
	public void BNIA405()
	{
		ChildCreation("BNIA405 - Verify that All Formats & Editions page should display Paperback, Hardcover, NOOK Book, Audiobook tab switching options.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOption");
			WebElement pdpAllformats = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOption");
			pdpAllformats.click();
			Thread.sleep(1000);
			List<WebElement> DropdownList = driver.findElements(By.xpath("//*[@class = 'sk_bn_formatOption']"));
			
			for(int i=1;i<=DropdownList.size();i++)
			//for(WebElement dropdown : DropdownList)
			{
				WebElement dropdownoptions = driver.findElement(By.xpath("(//*[@class = 'sk_bn_formatOption'])["+i+"]"));
				String drdplist = dropdownoptions.getAttribute("value");
				System.out.println(""+drdplist);
				if(drdplist.equals("allFormat"))
				{
					dropdownoptions.click();
				}
				else
				{
					System.out.println("There is no All Fromats Option in the dropdown menu for the Selected product");
					//return;
				}
			}
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOverlay");
			//WebElement allformatsOverlay = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOverlay");
			List<WebElement> AllFormatsOverlayOptions = driver.findElements(By.xpath("//*[@class = 'sk_formatTitleTxt']"));
			for(int i=1;i<=AllFormatsOverlayOptions.size();i++)
			{
				String str = driver.findElement(By.xpath("(//*[@class='sk_formatTitleTxt'])["+i+"]")).getText();
				System.out.println(""+str);
				if(driver.findElement(By.xpath("(//*[@class='sk_formatTitleTxt'])["+i+"]")).isDisplayed())
				{
					log.add("All Formats & Editions page displayes "+str+" options");
					pass("All Formats & Editions page displays Paperback, Hardcover, NOOK Book, Audiobook tab switching options.",log);
				}
				else
				{
					fail("All Formats & Editions page is not displays Paperback, Hardcover, NOOK Book, Audiobook tab switching options.");
				}
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA405\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA406 - Verify that all the tab switching options should display Product Name, Pub.Date, Publisher, See complete product details link, Add to Bag button*/
	public void BNIA406() throws Exception
	{
		ChildCreation("BNIA406 - Verify that all the tab switching options should display Product Name, Pub.Date, Publisher, See complete product details link, Add to Bag button");
		try
		{
			boolean PgOk = BNBasicCommonMethods.waitforElement(wait, obj, "PdpPageAllFormatList");
			if(PgOk==true)
			{
				Thread.sleep(1000);
				List<WebElement> formatSize = driver.findElements(By.xpath("//*[@class='sk_allFormatTitleCont']//*[contains(@class,'allFormatProductTitle')]"));
				for(int i = 0; i<formatSize.size();i++)
				{
					formatSize.get(i).click();
					Thread.sleep(2500);
					WebElement formatprdt = driver.findElement(By.xpath("(//*[@class='sk_allFormatContent']//*[@class='sk_allFormatDetailsCont'])["+(i+1)+"]"));
					wait.until(ExpectedConditions.attributeContains(formatprdt, "style", "block"));
					List<WebElement> prdtDesc = driver.findElements(By.xpath("(//*[@class='sk_allFormatContent']//*[@class='sk_allFormatDetailsCont'])["+(i+1)+"]//*[contains(@class,'allFormatProductDetails')]"));
					boolean formatOk = formatprdt.getAttribute("style").contains("block");
					if(formatOk==true)
					{
						for(int j = 1;j<=prdtDesc.size();j++)
						{
							WebElement prdtTtle = driver.findElement(By.xpath("((//*[@class='sk_allFormatContent']//*[@class='sk_allFormatDetailsCont'])["+(i+1)+"]//*[contains(@class,'allFormatProductDetails')]//*[contains(@class,'ProductTitleTxt')])["+j+"]"));
							BNBasicCommonMethods.scrolldown(prdtTtle, driver);
							String prdtTitle = prdtTtle.getText();
							if(prdtTitle.isEmpty())
							{
								fail("The product title is not displayed");
							}
							else
							{
								System.out.println(prdtTitle);
								pass("The product title is displayed.The displayed product title is :\n" + prdtTitle);
							}
							
							List<WebElement> prdtPublisher = driver.findElements(By.xpath("((//*[@class='sk_allFormatContent']//*[@class='sk_allFormatDetailsCont'])["+(i+1)+"]//*[contains(@class,'allFormatProductDetails')])["+j+"]//*[@class='sk_formatProductPublish']"));
							for(int k = 1; k<=prdtPublisher.size();k++)
							{
								String prdtPubDate = driver.findElement(By.xpath("(((//*[@class='sk_allFormatContent']//*[@class='sk_allFormatDetailsCont'])["+(i+1)+"]//*[contains(@class,'allFormatProductDetails')])["+j+"]//*[contains(@class,'ProductPublish')])["+k+"]")).getText();
								if(prdtPubDate.isEmpty())
								{
									fail("The publish date / details is not displayed");
								}
								else
								{
									System.out.println(prdtPubDate);
									pass("The product publish date / details is displayed.The displayed product date is :\n" + prdtPubDate);
								}
							}
							
							String completePrdtDet = driver.findElement(By.xpath("((//*[@class='sk_allFormatContent']//*[@class='sk_allFormatDetailsCont'])["+(i+1)+"]//*[contains(@class,'allFormatProductDetails')])["+j+"]//*[contains(@class,'sk_allFormatSeeFullDetail')]")).getText();
							if(completePrdtDet.isEmpty())
							{
								fail("The Complete All Format Link is not displayed");
							}
							else
							{
								System.out.println(completePrdtDet);
								pass("The Complete All Format Link is displayed.The displayed product date is :\n" + completePrdtDet);
							}
						}
					}
					else
					{
						fail("Failed to load the all format page.");
					}
				}
			}
			else
			{
				fail("The Page failed to load.");
			}
			WebElement paperback = BNBasicCommonMethods.findElement(driver, obj, "AllFormatsOverlay_PaperBackTab");
			paperback.click();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA406\n"+e.getMessage());
 			exception(e.getMessage());
		}
	}
	
	/*BNIA407 - Verift that when See complete product details link is clicked, then it should display the product details section in the corresponding PDP page.*/
	public void BNIA407()
	{
		ChildCreation("BNIA407 - Verift that when See complete product details link is clicked, then it should display the product details section in the corresponding PDP page.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOverlay_SeeCompleteProdDetails1");
			WebElement seecompleteproddetails = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOverlay_SeeCompleteProdDetails1");
			//WebElement allformatsoverlayImg = driver.findElement(By.xpath("(//*[@class = 'sk_allFormatProductDetails sk_allFormatProductDetails_0'])[1]"));
			WebElement allformatsoverlay_atb = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOverlay_ATB1");
			String atbEANno = allformatsoverlay_atb.getAttribute("identifier");
			System.out.println(""+atbEANno);
			//String prdImg = allformatsoverlayImg.getAttribute("src");
			//WebElement pdppage = BNBasicCommonMethods.findElement(driver, obj, "PDPContainer");
			seecompleteproddetails.click();
			Thread.sleep(2000);
			BNBasicCommonMethods.waitforElement(wait, obj, "pdp_image");
			WebElement pdpean = driver.findElement(By.xpath("//*[@id = 'id_pdpWrapper']"));
			String pdp = pdpean.getAttribute("identifier");
			System.out.println(""+pdp);
			//WebElement PDP_Image = BNBasicCommonMethods.findElement(driver, obj, "pdp_image");
			//String src = "//prodimage.images-bn.com/pimages/9781455584987_p0_v2_s391x700-mobile.jpg";
			if(atbEANno.equals(pdp))
			{
				pass("When See complete product details link is clicked, then it displays the product details section in the corresponding PDP page.");
			}
			else
				fail("When See complete product details link is clicked, then it does not displays the product details section in the corresponding PDP page.");
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA407\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA408 - Verify that when X icon is clicked in overlay then it should navigate to prevoius page*/
	public void BNIA408() throws InterruptedException
	{
		ChildCreation("BNIA408 - Verify that when X icon is clicked in overlay then it should navigate to prevoius page");
		
		try
		{
			Thread.sleep(2000);

			int ctr=1;
			do
			{
				try
				{
					
					BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOption");
					WebElement pdpAllformats = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOption");
					pdpAllformats.click();
					Thread.sleep(2000);
					List<WebElement> DropdownList = driver.findElements(By.xpath("//*[@class = 'sk_bn_formatOption']"));
					for(int i=1;i<=DropdownList.size();i++)
					{
							
						WebElement dropdownoptions = driver.findElement(By.xpath("(//*[@class = 'sk_bn_formatOption'])["+i+"]"));
						String drdplist = dropdownoptions.getAttribute("value");
						System.out.println(""+drdplist);
						if(drdplist.equals("allFormat"))
						{
							dropdownoptions.click();
							i=DropdownList.size();
						
						}
						else
						{
							System.out.println("There is no All Fromats Option in the dropdown menu for the Selected product");
						}
					}
					BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOverlay");
					WebElement closeicon = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOverlay_Closeicon");
					closeicon.click();
					WebElement pdppage = BNBasicCommonMethods.findElement(driver, obj, "PDPContainer");
					if((BNBasicCommonMethods.isElementPresent(pdppage)))
					{
						pass("when X icon is clicked in overlay then it navigates to prevoius page");
						ctr=4;
						break;
					}
					else
					{
						fail("when X icon is clicked in overlay then it does not navigates to prevoius page");
					}
				}
				catch(Exception e)
				{
		        	String URL = driver.getCurrentUrl();
		        	Thread.sleep(2000);
		        	driver.get(URL);
		        	Thread.sleep(1500);
		        	ctr++;
		 		}	
			
			}while(ctr<=3);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA410\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}	
	
	/*BNIA409 - Verify that while selecting  the Format type in  PDP page,the selected format should be displayed  as Format:XXXXX*/
	private void BNIA409()
	{
		ChildCreation("BNIA409 - Verify that while selecting  the Format type in  PDP page,the selected format should be displayed  as Format:XXXXX");
		try
		{
			WebElement selectedformat = BNBasicCommonMethods.findElement(driver, obj, "PDP_SelectedFormatOption");
			String str = selectedformat.getAttribute("formattype");
			String formatoption = selectedformat.getText();
			System.out.println(""+formatoption);
			//WebElement selectedFormatText = BNBasicCommonMethods.findElement(driver, obj, "PDP_SelectedFormatText");
			//String seltext = selectedFormatText.getText();
			String str1 = "Format: "+str;
			System.out.println("Format: "+str);
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			//String selformattext = BNBasicCommonMethods.getExcelVal("BNIA409", sheet, 1);
			if(formatoption.equals(str1))
			{
				log.add("The Selected format is displayed as\n"+formatoption);
				pass("While selecting the Format type in PDP page,the selected format is displayed  as Format:XXXXX\n",log);
			}
			else
			{
				fail("While selecting the Format type in PDP page,the selected format is not displayed  as Format:XXXXX");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA409\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA410 - Verify that product availability either In Stock or Available Only in Online or Not Available at this time should be displayed in the product detail page*/
	public void BNIA410()
	{
		ChildCreation("BNIA410 - Verify that product availability either In Stock or Available Only in Online or Not Available at this time should be displayed in the product detail page");
		try
		{
			int ctr=1;
			do
			{
				if(!FFprdtId.isEmpty())
				{
					ctr=6;
					String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+FFprdtId;
					driver.get(PDPURL);
					/*sheet = BNBasicCommonMethods.excelsetUp("PDP");
					String EAN = BNBasicCommonMethods.getExcelNumericVal("BNIA410", sheet, 3);
					search.sendKeys(EAN);
					search.sendKeys(Keys.ENTER);*/
					try
					{
						BNBasicCommonMethods.waitforElement(wait, obj, "PDPContainer");
						BNBasicCommonMethods.waitforElement(wait, obj, "PDP_OutOfStockText");
						WebElement outofstocktext = BNBasicCommonMethods.findElement(driver, obj, "PDP_OutOfStockText");
						String outofstock_text = outofstocktext.getText();
						if(BNBasicCommonMethods.isElementPresent(outofstocktext))
						{
							log.add("The Product Availability Status is \n"+outofstock_text);
							pass("Product availability either In Stock or Available Only in Online or Not Available at this time is displayed in the product detail page",log);
						}
						else
						{
							fail("Product availability either In Stock or Available Only in Online or Not Available at this time is not displayed in the product detail page");
						}
					}
					catch(Exception e)
					{
						System.out.println("Out of Stock Text Message is not shown");
						ctr++;
					}
					
				}
				else
				{
					System.out.println("FF Product ID is not shown");
				}
			}while(ctr<=5);
			
			/*WebElement searchicon = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
			searchicon.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			WebElement search = BNBasicCommonMethods.findElement(driver, obj, "searchbox");*/
			
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA410\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	
	/*BNIA412 - Verify that selected format option for the product should be shown in the product detail page*/
	public void BNIA412()
	{
		ChildCreation("BNIA412 - Verify that selected format option for the product should be shown in the product detail page");
		try
		{
			String SearchURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/search";
			driver.get(SearchURL);
			/*WebElement search = BNBasicCommonMethods.findElement(driver, obj, "Searchtile");
			search.click();*/
			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
			WebElement serbox = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
			serbox.clear();
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String EAN = BNBasicCommonMethods.getExcelNumericVal("BNIA363", sheet, 3);
			serbox.sendKeys(EAN);
			serbox.sendKeys(Keys.ENTER);
			//bckarrow.click();
			//bckarrow.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOption");
			WebElement selectedformat = BNBasicCommonMethods.findElement(driver, obj, "PDP_SelectedFormatOption");
			String str = selectedformat.getAttribute("formattype");
			//String formatoption = selectedformat.getText();
			System.out.println(""+str);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SelectedFormatText");
			WebElement selectedFormatText = BNBasicCommonMethods.findElement(driver, obj, "PDP_SelectedFormatText");
			String seltext = selectedFormatText.getText();
			System.out.println(""+seltext);
			if(str.equals(seltext))
			{
				pass("Selected format option for the product is shown in the product detail page");
			}
			else
			{
				fail("Selected format option for the product is not shown in the product detail page");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA412\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA413 - Verify that product price should be displayed in the product detail page*/
	public void BNIA413()
	{
		ChildCreation("BNIA413 - Verify that product price should be displayed in the product detail page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductPrice");
			WebElement actualprice = BNBasicCommonMethods.findElement(driver, obj, "PDP_SalePrice");
			WebElement saleprice = BNBasicCommonMethods.findElement(driver, obj, "PDP_ActualPrice");
			String saleprice1 = saleprice.getAttribute("pricetype");
			String sale_price = saleprice.getAttribute("price");
			System.out.println("saleprice :"  +sale_price);
			String actualprice1 = actualprice.getAttribute("pricetype");
			String actual_price = actualprice.getAttribute("price");
			System.out.println(""+actualprice1+ " : " +actual_price);
			if((BNBasicCommonMethods.isElementPresent(saleprice)) && (BNBasicCommonMethods.isElementPresent(actualprice)))
			{
				log.add("Sale Price of the product is \n"+saleprice1+ " : " +sale_price);
				log.add("Actual Price of the Product is \n"+actualprice1+ " : " +actual_price);
				pass("Product price is in the product detail page\n",log);
			}
			else
			{
				fail("Product price is not in the product detail page");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA413\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA414 - Verify that product price should include prefix $ symbol*/
	public void BNIA414()
	{
		ChildCreation("BNIA414 - Verify that product price should include prefix $ symbol");
		try
		{
			WebElement salepricedollar = BNBasicCommonMethods.findElement(driver, obj, "PDP_SalePriceDollar");
			if(BNBasicCommonMethods.isElementPresent(salepricedollar))
			{
				pass("product price includes prefix $ symbol");
			}
			else
			{
				fail("product price does not includes prefix $ symbol");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA414\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA415 - Verify that sale price ,original pice & save XX%  should be displayed only when the product is available in online*/
	public void BNIA415()
	{
		ChildCreation("BNIA415 - Verify that sale price ,original pice & save XX%  should be displayed only when the product is available in online");
		try
		{
			if(!TFprdtId.isEmpty())
			{
				String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+TFprdtId;
				driver.get(PDPURL);
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
				WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
				if((BNBasicCommonMethods.isElementPresent(ATB)))
				{
					log.add("The product is available in Online");
				}
				else
				{
					return;
				}
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductPrice");
				try
				{
					WebElement actualprice = BNBasicCommonMethods.findElement(driver, obj, "PDP_SalePrice");
					WebElement saleprice = BNBasicCommonMethods.findElement(driver, obj, "PDP_ActualPrice");
					WebElement savepercentage = BNBasicCommonMethods.findElement(driver, obj, "PDP_SavePercentage");
					String saleprice1 = saleprice.getAttribute("pricetype");
					String sale_price = saleprice.getAttribute("price");
					String savepercentage1 = savepercentage.getText();
					//System.out.println(""+saleprice+ " : " +sale_price);
					String actualprice1 = actualprice.getAttribute("pricetype");
					String actual_price = actualprice.getAttribute("price");
					//System.out.println(""+actualprice1+ " : " +actual_price);
					if((BNBasicCommonMethods.isElementPresent(saleprice)) && (BNBasicCommonMethods.isElementPresent(actualprice)) && (BNBasicCommonMethods.isElementPresent(savepercentage)))
					{
						log.add("Sale Price of the product is "+saleprice1+ " : " +sale_price);
						log.add("Actual Price of the Product is "+actualprice1+ " : " +actual_price);
						log.add("Save Percentage Value is "+savepercentage1);
						pass("sale price ,original pice & save XX%  should be displayed only when the product is available in online\n",log);
					}
					else
					{
						fail("Product price is not in the product detail page");
					}
				}
				catch(Exception e1)
				{
					Skip("There is no Actual price and Save Percentage for this Product");
				}
				
			}
			else
			{
				System.out.println("FT Product ID is not shown");
			}
			
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA415\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA417 - Verify that sale price of the product should be highlighted as BOLD */
	public void BNIA417()
	{
		ChildCreation("BNIA417 - Verify that sale price of the product should be highlighted as BOLD ");
		try
		{
			WebElement Saleprice = BNBasicCommonMethods.findElement(driver, obj, "PDP_SalePrice");
			String sale_price = Saleprice.getCssValue("font-size");
			String sale_price1 = Saleprice.getCssValue("font-weight");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String sale_price2 = BNBasicCommonMethods.getExcelVal("BNIA417", sheet, 9);
			if(sale_price1.equals(sale_price2))
			{
				log.add("The font size of the Sale Price is \n"+sale_price);
				log.add("The font weight is \n"+sale_price1);
				pass("Sale price of the product is highlighted as BOLD\n",log);
			}
			else
			{
				fail("Sale price of the product is not highlighted as BOLD");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA417\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA421 - Verify that Add to Bag buttton should be shown only when the product is available in Online*/
	public void BNIA421() throws Exception
	{
		ChildCreation("BNIA421 - Verify that Add to Bag buttton should be shown only when the product is available in Online");
		try
		{
			WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
			if(BNBasicCommonMethods.isElementPresent(ATB))
			{
				log.add("The Product is available in Online");
				pass("Add to Bag buttton is shown, when the product is available in Online",log);
			}
			else
			{
				fail("ATB Button is not shown");
			}
		}
		catch(Exception e)
		{
			WebElement outofstocktext = BNBasicCommonMethods.findElement(driver, obj, "PDP_OutOfStockText");
			String str1 = outofstocktext.getText();
			if(BNBasicCommonMethods.isElementPresent(outofstocktext))
			{
				pass("The Product is not available in online,\n" +str1+"\n text is shown");
			}
			else
			{
				fail("The product is not available in online");
			}
        }
	}
	
	/*BNIA422 - Verify that Add To Bag button should be shown only when the product is available in online*/
	public void BNIA422() throws Exception
	{
		ChildCreation("BNIA422 - Verify that Add To Bag button should be shown only when the product is available in online");
		try
		{
			WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
			if(BNBasicCommonMethods.isElementPresent(ATB))
			{
				log.add("The Product is available in Online");
				pass("Add to Bag buttton is shown, when the product is available in Online",log);
			}
			else
			{
				fail("ATB Button is not shown");
			}
		}
		catch(Exception e)
		{
			WebElement outofstocktext = BNBasicCommonMethods.findElement(driver, obj, "PDP_OutOfStockText");
			String str1 = outofstocktext.getText();
			if(BNBasicCommonMethods.isElementPresent(outofstocktext))
			{
				pass("The Product is not available in online,\n" +str1+ "\n text is shown");
			}
			else
			{
				fail("The product is not available in online");
			}
        }
	}
	
	/*BNIA423 - Verify that SHOW IN STORE & ADD TO BAG buttons should hidden when the product out of stock (Not Available at this time)*/
	public void BNIA423()
	{
		ChildCreation("BNIA423 - Verify that SHOW IN STORE & ADD TO BAG buttons should hidden when the product out of stock (Not Available at this time)");
		try
		{
			
			if(!FFprdtId.isEmpty())
			{
				String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+FFprdtId;
				driver.get(PDPURL);
				try
				{
					BNBasicCommonMethods.waitforElement(wait, obj, "PDP_OutOfStockText");
					WebElement outofstocktext = BNBasicCommonMethods.findElement(driver, obj, "PDP_OutOfStockText");
					String outofstock_text = outofstocktext.getText();
					if(BNBasicCommonMethods.isElementPresent(outofstocktext))
					{
						log.add("Show in Store & Add to Bag buttons is not shown instead  \n"+outofstock_text+ "\n text is shown");
						pass("SHOW IN STORE & ADD TO BAG buttons is hidden when the product out of stock (Not Available at this time)");
					}
					else
					{
						fail("SHOW IN STORE & ADD TO BAG buttons is not hidden when the product out of stock (Not Available at this time)");
					}
				}
				catch(Exception e)
				{
					System.out.println("Not Available at this Time Text is not Shown");
				}
			}
				
			else
			{
				System.out.println("FF Product ID is not shown");
			}
			
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA423\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA424 - Verify that market place from XXXX should be shown below to ADD TO BAG button*/
	public void BNIA424()
	{
		ChildCreation("BNIA424 - Verify that market place from XXXX should be shown below to ADD TO BAG button");
		try
		{
			if(!TFprdtId.isEmpty())
			{
				String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+TFprdtId;
				driver.get(PDPURL);
				try
				{
					BNBasicCommonMethods.waitforElement(wait, obj, "Marketplace");
					WebElement marketplace = BNBasicCommonMethods.findElement(driver, obj, "Marketplace");
					//String marketplacetext = marketplace.getText();
					WebElement marketplaceprice = BNBasicCommonMethods.findElement(driver, obj, "MarketplacePrice");
					String marketplace_price = marketplaceprice.getText();
					if((BNBasicCommonMethods.isElementPresent(marketplace)) && (BNBasicCommonMethods.isElementPresent(marketplaceprice)))
					{
						log.add("+marketplacetext+\n"+marketplace_price);
						pass("Market place from XXXX is shown below to ADD TO BAG button",log);
					}
					else
					{
						fail("Market place from XXXX is not shown below to ADD TO BAG button");
					}
				}
				catch(Exception e)
				{
					System.out.println("Market Place Link is not available for the\n"+TTprdtId+ "\nEAN Number" );
				}
			}
			else
			{
				System.out.println("TF Product ID is not Shown");
			}
			
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA424\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA425 - Verify that on Selecting ADD TO BAG button,it shows success alert in the overlay*/
	public void BNIA425() throws Exception
	{
		ChildCreation("BNIA425 - Verify that on Selecting ADD TO BAG button,it shows success alert in the overlay");
		try
		{
			Thread.sleep(1500);
			if(!TTprdtId.isEmpty())
			{
				int k=1;
				do
				{
					String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+TTprdtId;
					driver.get(PDPURL);
					((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
					((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
					((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
					Thread.sleep(2500);
					
					try
					{
						Thread.sleep(3000);
						//BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AlsoAvailableOnlineTextLink");
						WebElement AlsoAvailOnlineText = BNBasicCommonMethods.findElement(driver, obj, "PDP_AlsoAvailableOnlineTextLink");
						Actions act = new Actions(driver);
						for(int ctr=0 ; ctr<5 ;ctr++)
						  {
						  	act.sendKeys(Keys.CONTROL).sendKeys(Keys.ARROW_DOWN).build().perform();
						  }
						AlsoAvailOnlineText.click();
						k=11;
						BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
						WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
						int ctr=1;
						do
						{
							if(ATB.isDisplayed())
							{
								ctr=4;
								ATB.click();
								try
								{
									Thread.sleep(1500);
									//ctr=4;
									try
									{
										BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay");
										WebElement successoverlay = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay");
										WebElement successoverlay_continuebtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ContinueButton");
										WebElement successoverlay_viewbagbtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ViewBagButton");
										if(BNBasicCommonMethods.isElementPresent(successoverlay))
										{
											log.add("On Selecting ADD TO BAG button,it shows success alert in the overlay");
											if((BNBasicCommonMethods.isElementPresent(successoverlay_continuebtn)) && (BNBasicCommonMethods.isElementPresent(successoverlay_viewbagbtn)))
											{
												log.add("In Success Overlay, Continue Button & View Bag Button is Shown");
												pass("On Selecting ADD TO BAG button,it shows success alert in the overlay",log);
											}
											else
											{
												fail("In Success Overlay, Continue Button & View Bag Button is not Shown");
											}
										}
										else
										{
											fail("On Selecting ADD TO BAG button,it does not shows success alert in the overlay");
										}
									}
									catch(Exception e)
									{
										WebElement nookfailureAlert = BNBasicCommonMethods.findElement(driver, obj, "PDP_NookFailureAlertOverlay");
										if(BNBasicCommonMethods.isElementPresent(nookfailureAlert))
										{
											log.add("Only one Nook Product can able to add in the shopping bag");
											fail("On Selecting ADD TO BAG button,it does not shows success alert in the overlay",log);
										}
									}
									
								}
								catch(Exception e)
								{
									WebElement ErrorAlertOverlay = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATBclick_ErrorAlert");
									if(ErrorAlertOverlay.isDisplayed())
									{
										WebElement ErrorAlertOkButton = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATBclick_ErrorAlertOkButton");
										ErrorAlertOkButton.click();
										driver.getCurrentUrl();
									}
								}
							}
							else
							{
								System.out.println("ATB button is not shown");
							}
						}while(ctr<=3);
					}
					catch(Exception e)
					{
						System.out.println("ATB link is not shown");
						k++;
						/*WebElement nookfailureAlert = BNBasicCommonMethods.findElement(driver, obj, "PDP_NookFailureAlertOverlay");
						if(BNBasicCommonMethods.isElementPresent(nookfailureAlert))
						{
							log.add("Only one Nook Product can able to add in the shopping bag");
							fail("On Selecting ADD TO BAG button,it does not shows success alert in the overlay",log);
						}*/
			        }
				}while(k<=10);
				
			}
			else
			{
				System.out.println("TT Product ID is not Shown");
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA425\n"+e.getMessage());
 			exception(e.getMessage());
		}
		
		
	}
	
	/*BNIA426 - Verify that Add to bag overlay should display product image, product title, author name,format option & selling price along with Continue Shopping & View Bag button*/
	public void BNIA426()
	{
		ChildCreation("BNIA426 - Verify that Add to bag overlay should display product image, product title, author name,format option & selling price along with Continue Shopping & View Bag button");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay");
			WebElement successoverlay = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay_ContinueButton");
			WebElement successoverlay_continuebtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ContinueButton");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay_ViewBagButton");
			WebElement successoverlay_viewbagbtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ViewBagButton");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay_PrdImage");
			WebElement PrdImage = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_PrdImage");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay_PrdTitle");
			WebElement PrdTitle = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_PrdTitle");
			//WebElement PrdAuthorName = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_PrdAuthorName");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay_PrdFormat");
			WebElement PrdFormat = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_PrdFormat");
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay_PrdPrice");
			WebElement PrdPrice = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_PrdPrice");
			boolean continuebutton = false;
			continuebutton = successoverlay_continuebtn.isDisplayed();
			boolean viewbagbutton = false;
			viewbagbutton = successoverlay_viewbagbtn.isDisplayed();
			boolean image = false;
			image = PrdImage.isDisplayed();
			boolean title = false;
			title = PrdTitle.isDisplayed();
			/*boolean authorname = false;
			authorname = PrdAuthorName.isDisplayed();*/
			boolean Prdformat = false;
			Prdformat = PrdFormat.isDisplayed();
			boolean price = false;
			price = PrdPrice.isDisplayed();
			if(BNBasicCommonMethods.isElementPresent(successoverlay))
			{
				log.add("Add to Bag Success Overlay is shown");
				if((image) && (title))
				{
					log.add("Product Image, Product Title, is shown in the ATB Success Overlay");
					if((Prdformat) & (price))
					{
						log.add("Product Format, Product Price is Shown in the ATB Success Overlay");
						if((continuebutton) && (viewbagbutton))
						{
							log.add("Continue Button & VIew Bag Button is shown in the ATB Overlay");
							pass("Add to bag overlay displays product image, product title, author name,format option & selling price along with Continue Shopping & View Bag button",log);
						}
						else
						{
							 fail("Add to bag overlay does not displays product image, product title, author name,format option & selling price along with Continue Shopping & View Bag button");
						}
					}
					else
					{
						fail("Product Format and Price is not shown");
					}
				}
				else
				{
					fail("Image, Title, is not shown");
				}
			}
			else
			{
				fail("ATB Success Overlay is not shown");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA426\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA427 - Verify that View Bag button should be highlighted in RED color */
	public void BNIA427()
	{
		ChildCreation("BNIA427 - Verify that View Bag button should be highlighted in RED color");
		try
		{
			WebElement successoverlay_viewbagbtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ViewBagButton");
			String viewbag = successoverlay_viewbagbtn.getCssValue("background-color");
			Color viewbagcolors = Color.fromString(viewbag);
			String hexviewbagcolors = viewbagcolors.asHex();
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String viewbagColor = BNBasicCommonMethods.getExcelVal("BNIA427", sheet, 6);
			if(hexviewbagcolors.equals(viewbagColor))
			{
				log.add("The hexa value of RED color is \n"+viewbag);
				pass("View Bag button is highlighted in RED color",log);
			}
			else
			{
				fail("View Bag button is not highlighted in RED color");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA427\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA428 - Verify that while tapping continue shopping button the overlay should be closed and it remains in PDP page */
	public void BNIA428()
	{
		ChildCreation("BNIA428 - Verify that while tapping continue shopping button the overlay should be closed and it remains in PDP page ");
		try
		{
			WebElement successoverlay_continuebtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ContinueButton");
			successoverlay_continuebtn.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
			WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
			if(BNBasicCommonMethods.isElementPresent(ATB))
			{
				pass("While tapping continue shopping button, the overlay closed and it remains in PDP page");
			}
			else
			{
				fail("While tapping continue shopping button, the overlay does not get closed");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA428\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA429 - Verify that while tapping View bag button,it should shown shopping bag*/
	public void BNIA429() throws Exception
	{
		ChildCreation("BNIA429 - Verify that while tapping View bag button,it should shown shopping bag");
		try
		{
			WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
			ATB.click();
			try
			{
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay");
				WebElement successoverlay_viewbagbtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ViewBagButton");
				successoverlay_viewbagbtn.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "ShoppingBag_OrderSummary");
				WebElement  shoppingbag = BNBasicCommonMethods.findElement(driver, obj, "ShoppingBag_OrderSummary");
				if(BNBasicCommonMethods.isElementPresent(shoppingbag))
				{
					pass("While tapping View bag button,it shows shopping bag");
				}
				else
				{
					fail("While tapping View bag button,it does not shows shopping bag");
				}
			}
			catch(Exception e1)
			{
				WebElement ErrorAlert = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATBclick_ErrorAlert");
				if(ErrorAlert.isDisplayed())
				{
					System.out.println("Rental product cannot be added twice");
					sheet = BNBasicCommonMethods.excelsetUp("PDP");
					String EAN = BNBasicCommonMethods.getExcelNumericVal("BNIA363", sheet, 3);
					String URL = "http://"+"bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+EAN;
					driver.get(URL);
					Thread.sleep(2000);
					/*WebElement ErrorOk = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATBclick_ErrorAlertOkButton");
					ErrorOk.click();*/
					
					//Skip("This item cannot be added to the cart");
				}
			}
			
		}
		catch(Exception e)
		{
        	 System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA429\n"+e.getMessage());
 			exception(e.getMessage());
 		}
		
		BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
		WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
		BNLogo.click();
		Thread.sleep(1000);
	}
	
	/*BNIA430 - Verify that while tapping Market place button Market place new and used overlay should be shown*/
	public void BNIA430()
	{
		ChildCreation("BNIA430 - Verify that while tapping Market place button Market place new and used overlay should be shown");
		try
		{
			WebElement marketplacelink = BNBasicCommonMethods.findElement(driver, obj, "Marketplace");
			marketplacelink.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "MarketplaceOverlay");
			WebElement marketplace_overlay = BNBasicCommonMethods.findElement(driver, obj, "MarketplaceOverlay");
			if(BNBasicCommonMethods.isElementPresent(marketplace_overlay))
			{
				pass("While tapping Market place button Market place new and used overlay is shown");
			}
			else
			{
				fail("While tapping Market place button Market place new and used overlay is not shown");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA430\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA431 - Verify that  Market place new and used title should be highlighted by bold*/
	public void BNIA431()
	{
		ChildCreation("BNIA431 - Verify that  Market place new and used title should be highlighted by bold");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String ExpectedFontName = BNBasicCommonMethods.getExcelVal("BNIA431", sheet, 4);
			WebElement marketplacetext = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_Title");
			String title = marketplacetext.getCssValue("font-family");
			System.out.println(title);
			String[] title1 = title.split(",");
			if(title1[0].equals(ExpectedFontName))
			{
				log.add("Expected font name : \n"+ExpectedFontName);
				log.add("Current font name : \n"+title1[0]);
				pass("Market place new and used title is highlighted by bold",log);
			}
			else
			{
				log.add("Expected font name : \n"+ExpectedFontName);
				log.add("Current font name : \n"+title1[0]);
				pass("Market place new and used title is not highlighted by bold",log);
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA431\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA432 - Verify that Note description should be displayed below the Market place new and used text*/
	public void BNIA432()
	{
		ChildCreation("BNIA432 - Verify that Note description should be displayed below the Market place new and used text");
		try
		{
			WebElement marketplace_notetext1 = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_NoteText");
			/*WebElement marketplace_notetext = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_NoteText1");
			String noteText = marketplace_notetext.getText();*/
			//String notetext1 = marketplace_notetext1.getText();
			if(BNBasicCommonMethods.isElementPresent(marketplace_notetext1))
			{
				log.add("+notetext1+");
				pass("Note description is displayed below the Market place new and used text",log);
			}
			else
			{
				fail("Note description is not displayed below the Market place new and used text");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA432\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA433 - Verify that product name and author name should be displayed below Note in  the overlay*/
	public void BNIA433()
	{
		ChildCreation("BNIA433 - Verify that product name and author name should be displayed below Note in  the overlay");
		try
		{
			WebElement title = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_PrdTitle");
			WebElement authorname = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_AuthorName");
			String Title = title.getText();
			String Authorname = authorname.getText();
			if((BNBasicCommonMethods.isElementPresent(title)) && (BNBasicCommonMethods.isElementPresent(authorname)))
			{
				log.add("The Product Title and the Author Name is \n"+Title +"\n" +Authorname);
				pass("Product name and author name is displayed below Note in the overlay",log);
			}
			else
			{
				fail("Product name and author name is not displayed below Note in the overlay");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA433\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA434 - Verify that while tapping review link,it should navigate to review section in the product detail page*/
	public void BNIA434()
	{
		ChildCreation("BNIA434 - Verify that while tapping review link,it should navigate to review section in the product detail page");
		try
		{
			WebElement reviewlink = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ReviewLink");
			reviewlink.click();
			try
			{
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerReviewsSection");
				WebElement customerreviews = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsSection");
				if(BNBasicCommonMethods.isElementPresent(customerreviews))
				{
					pass("While tapping review link,it navigates to review section in the product detail page");
				}
				else
				{
					fail("While tapping review link,it does not navigates to review section in the product detail page");
				}
				WebElement marketplacelink = BNBasicCommonMethods.findElement(driver, obj, "Marketplace");
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView(true);",marketplacelink);
				marketplacelink.click();
			}
			catch(Exception e)
			{
				System.out.println("O Reviews is available for the selected product");
				WebElement marketplacelink = BNBasicCommonMethods.findElement(driver, obj, "Marketplace");
				marketplacelink.click();
				Skip("O Reviews is available for the selected product");
			}
			
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA434\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA435 - Verify that all,new,used tab switching with product count should be displayed below the product title */
	public void BNIA435()
	{
		ChildCreation("BNIA435 - Verify that all,new,used tab switching with product count should be displayed below the product title ");
		try
		{
			Thread.sleep(1500);
			List<WebElement> MarketPlaceTabs = driver.findElements(By.xpath("//*[@class = 'sk_mkt_rollUpTitleTxt']"));
			for(int i=1;i<=MarketPlaceTabs.size();i++)
			{
				//BNBasicCommonMethods.waitforElement(wait, obj, "tabs1");
				WebElement tabs1 = driver.findElement(By.xpath("(//*[@class='sk_mkt_rollUpTitleTxt'])["+i+"]"));
				                                               // (//*[@class='sk_mkt_rollUpTitleTxt'])
				String tabs = tabs1.getText();
				System.out.println(""+tabs);
				if(driver.findElement(By.xpath("(//*[@class='sk_mkt_rollUpTitleTxt'])["+i+"]")).isDisplayed())
				{
					log.add("+tabs+\n" +"tab is shown");
					pass("All,New,Used tab switching with product count is displayed below the product title",log);
				}
				else
				{
					fail("All,New,Used tab switching with product count is displayed below the product title");
				}
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA435\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA436 - Verify that Product Sorting dropdown should be displayed with various sorting option(Eg: low to high,high to low,popularity,newly added)*/
	public void BNIA436()
	{
		ChildCreation("BNIA436 - Verify that Product Sorting dropdown should be displayed with various sorting option(Eg: low to high,high to low,popularity,newly added)");
		try
		{
			WebElement dropdown = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_Dropdown");
			dropdown.click();
			Thread.sleep(500);
			WebElement dropdown_Option1 = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_Dropdown_Option1");
			WebElement dropdown_option2 = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_Dropdown_Option2");
			WebElement dropdown_option3 = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_Dropdown_Option3");
			WebElement dropdown_option4 = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_Dropdown_Option4");
			String Option1 = dropdown_Option1.getText();
			String Option2 =dropdown_option2.getText();
			String Option3 =dropdown_option3.getText();
			String Option4 =dropdown_option4.getText();
			
			
				if((dropdown_Option1.isDisplayed()) && (dropdown_option2.isDisplayed()))
				{
					log.add("Dropdown options shown are \n"+Option1+"\n" +Option2);
					if((dropdown_option3.isDisplayed()) && (dropdown_option4.isDisplayed()))
					{
						
					log.add("Dropdown options shown are \n"+Option3+ "\n" + Option4);
					pass("Product Sorting dropdown is displayed with various sorting option(Eg: low to high,high to low,popularity,newly added)",log);
					}
					else
					{
						return;
					}
				}
				else
				{
					fail("Product Sorting dropdown is not displayed with various sorting option(Eg: low to high,high to low,popularity,newly added)");
				}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA436\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA437 - Verify that product sorting dropdown should be selected in Price low to  High option  by default*/
	public void BNIA437()
	{
		ChildCreation("BNIA437 - Verify that product sorting dropdown should be selected in Price low to  High option  by default");
		try
		{
			WebElement dropdown = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_Dropdown");
			String dropdwonText = dropdown.getText();
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String dropdownDefText = BNBasicCommonMethods.getExcelVal("BNIA437", sheet, 1);
			if(dropdwonText.equals(dropdownDefText))
			{
				log.add("The Default Text in the Dropdown is \n"+dropdwonText);
				pass("Product sorting dropdown is selected in Price low to  High option by default",log);
			}
			else
			{
				fail("Product sorting dropdown is not selected in Price low to  High option by default");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA437\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	

	/*BNIA438 - Verify that listed product should contains product price,name of the seller,seller since,rating,review count,review link,condition, seller details,shipping option with + icon and add to bag button with price*/
	public void BNIA438()
	{
		ChildCreation("BNIA438 - Verify that listed product should contains product price,name of the seller,seller since,rating,review count,review link,condition, seller details,shipping option with + icon and add to bag button with price");
		
	 try
		{
		 Thread.sleep(1500);
		/* String pdp = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam=9780316407021";
		 driver.get(pdp);
		 Thread.sleep(3000);
		 driver.get(pdp);
		 Thread.sleep(3000);
		 BNBasicCommonMethods.findElement(driver, obj, "Marketplace").click();
			//boolean PgOk = BNBasicCommonMethods.waitforElement(wait, obj, "PdpPageAllFormatList");
*/			if(true)
			{
				Thread.sleep(1000);
				List<WebElement> MarketPlaceText = driver.findElements(By.xpath("//*[@class='sk_allRollupTitleCont']//*[contains(@class,'sk_allRollupCondTitle')]"));
				for(int i = 0; i<MarketPlaceText.size();i++)
				{
					MarketPlaceText.get(i).click();
					Thread.sleep(2500);
					
					WebElement formatprdt = driver.findElement(By.xpath("(//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(i+1)+"]"));
					wait.until(ExpectedConditions.attributeContains(formatprdt, "style", "block"));
					List<WebElement> prdtDesc = driver.findElements(By.xpath("(//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(i+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]"));
					//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent']//*[contains(@class,'sk_bn_mktSingleItemCont')]
					boolean ProductOk = formatprdt.getAttribute("style").contains("block");
					int ProductCounter = prdtDesc.size();
					if(ProductOk==true)
					{
						if(prdtDesc.size()>5)
						{
							ProductCounter = 5;
						}
						for(int j = 1;j<=ProductCounter;j++)
						{
							WebElement ProductLeftPricedetails = driver.findElement(By.xpath("((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(i+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktLeftCont')])["+j+"]"));
																																																				
							
							BNBasicCommonMethods.scrolldown(ProductLeftPricedetails, driver);
							String prdtTitle = ProductLeftPricedetails.getText();
							if(prdtTitle.isEmpty())
							{
								fail("The product title is not displayed");
							}
							else
							{
								System.out.println(prdtTitle.toString());
								log.add("Product Price With Description  : "+prdtTitle);
							}
							
							WebElement ProductCenterdetails = driver.findElement(By.xpath("((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(i+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+j+"]"));
							BNBasicCommonMethods.scrolldown(ProductLeftPricedetails, driver);
							String prdtTitle1 = ProductCenterdetails.getText();
							
							if(prdtTitle1.isEmpty())
							{
								fail("The product title is not displayed");
							}
							else
							{
								System.out.println(prdtTitle1);
								log.add("Product Condition descrption with Read More Link : "+prdtTitle1);
							}
							
							WebElement ProductLeftdetails = driver.findElement(By.xpath("((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(i+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktRightCont')])["+j+"]"));
							BNBasicCommonMethods.scrolldown(ProductLeftPricedetails, driver);
							String prdtTitle2 = ProductLeftdetails.getText();
							if(prdtTitle2.isEmpty())
							{
								fail("The product title is not displayed");
							}
							else
							{
								System.out.println(prdtTitle2);
								log.add("Add to Bag button with Price details "+prdtTitle2);
							}
							
						}
						
						pass("Product Details In Market Place OVerlay",log);
					}
					else
					{
						fail("Failed to load the all format page.");
					}
				}
			}
			
			
		}
		catch(Exception e)
		{
			System.out.println("Something went wrong in BNIA406\n"+e.getMessage());
			exception(e.getMessage());
		}
   }
	
	
	
	/*BNIA440 - Verify that price should be highlighted in Bold for the listed products.*/
	/*BNIA441 - Verify that product condition should be shown with description*/
	/*BNIA442 - Verify that product condition description is lengthy then it should be shown in  ellipses with  Read more link and down arrow*/
	/*BNIA443 - Verify that while tapping Read more link that should shown more about description */
	/*BNIA444 - Verify that + icon  should be shown for shipping option */
	/*BNIA445 - Verify that while tapping "+" icon that should display shipping options in tooltip-container*/
	/*BNIA446 - Verify that + icon should be highlighted(greyed out) while displaying shipping options in tooltip-container*/
	/*BNIA448 - Verify that listed products should be scrolled vertically*/
	public void BNIA440() throws Exception
	{
		
		/*String pdp = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam=9780316407021";
		driver.get(pdp);
		Thread.sleep(3000);
		driver.get(pdp);
		Thread.sleep(3000);
		BNBasicCommonMethods.findElement(driver, obj, "Marketplace").click();*/
		Thread.sleep(1000);
		Random r = new Random();
		List<WebElement> MarketPlaceText = driver.findElements(By.xpath("//*[@class='sk_allRollupTitleCont']//*[contains(@class,'sk_allRollupCondTitle')]"));
		int sel = r.nextInt(MarketPlaceText.size());
      	MarketPlaceText.get(sel).click();
		   Thread.sleep(2500);
			WebElement formatprdt = driver.findElement(By.xpath("(//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]"));
			wait.until(ExpectedConditions.attributeContains(formatprdt, "style", "block"));
			List<WebElement> prdtDesc = driver.findElements(By.xpath("(//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]"));
			//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent']//*[contains(@class,'sk_bn_mktSingleItemCont')]
			//boolean ProductOk = formatprdt.getAttribute("style").contains("block");
			//int ProductCounter = prdtDesc.size();
			int productsel = r.nextInt(prdtDesc.size());
			if(productsel<1)
			{
				productsel = 1;
			}
			//WebElement ProductCondition = driver.findElement(By.xpath("((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktLeftCont')])["+productsel+"]"));
			WebElement ProductConditionScroll = driver.findElement(By.xpath("((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktLeftCont')])["+(productsel)+"]"));
				/*BNIA440 - Verify that price should be highlighted in Bold for the listed products.*/	
				ChildCreation("BNIA440 - Verify that price should be highlighted in Bold for the listed products.");	
				try
				{
					WebElement price = driver.findElement(By.xpath("(((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktLeftCont')])["+productsel+"]//*[@class = 'sk_bn_mktPdtPrice'])"));
					String pricesize = price.getCssValue("font-size");
					sheet = BNBasicCommonMethods.excelsetUp("PDP");
					String Expfont = BNBasicCommonMethods.getExcelVal("BNIA440", sheet, 4);
					String priceFont = price.getCssValue("font-family");
					String[] pricefontsplit = priceFont.split(",");
					String pricefontsize = BNBasicCommonMethods.getExcelVal("BNIA440", sheet, 5);
					if((pricefontsplit[0].equals(Expfont)) && (pricesize.equals(pricefontsize)))
					{
						log.add("Current font name and font size is : /n"+pricefontsplit[0]+ "\n"+pricesize);
						pass("Price is highlighted in Bold for the listed products.",log);
					}
					else
					{
						fail("Price is not highlighted in Bold for the listed products.",log);
					}
				}
				catch(Exception e)
				{
		        	System.out.println(e.getMessage());
		 			System.out.println("Something went wrong in BNIA440\n"+e.getMessage());
		 			exception(e.getMessage());
		 		}
			
				/*BNIA442 - Verify that product condition description is lengthy then it should be shown in  ellipses with  Read more link and down arrow*/
				ChildCreation("BNIA442 - Verify that product condition description is lengthy then it should be shown in  ellipses with  Read more link and down arrow");
				try
				{
						try
						{
							
							WebElement ShortDesc = driver.findElement(By.xpath("(((//*[@class='sk_marketPlaceCont']//*[@class = 'sk_marketPlaceContent'])[1]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+productsel+"]//*[@class = 'sk_bn_pdtReviewComment'])//*[@class = 'sk_bn_mktReviewVal']"));
							BNBasicCommonMethods.scrolldown(ShortDesc, driver);
							String elipses = ShortDesc.getText();
							WebElement readmorelink = driver.findElement(By.xpath("(((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])[1]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])[1]//*[@class = 'sk_bn_mktReviewVal'])//*[@class = 'sk_bn_readMoreReviewArr']"));
							BNBasicCommonMethods.scrolldown(readmorelink, driver);
							String link = readmorelink.getText();
							System.out.println(link);
							if((BNBasicCommonMethods.isElementPresent(ShortDesc)) && (BNBasicCommonMethods.isElementPresent(readmorelink)))
							{
								log.add("The Elipses and Read More link is shown, when the description is lengthy\n" +elipses+ "\n");
								pass("Product condition description is lengthy then it showns in  ellipses with Read more link and down arrow",log);
							}
							else
							{
								fail("Product condition description is lengthy then it does not showns in  ellipses with Read more link and down arrow");
							}
						}
						catch(Exception e)
						{
							System.out.println("Description is not shown for this product");
							try
							{
								WebElement readmorelink = driver.findElement(By.xpath("(((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+productsel+"]//*[@class = 'sk_bn_mktReviewVal'])"));
								String link = readmorelink.getText();
								pass("readmore link is shown and elipses are available"+link);
							}
							catch(Exception e1)
							{
								System.out.println("Read more link is not shown for this product");
								Skip("Read more Link and elipses is not shown for this product");
							}
							
						}
				}
				catch(Exception e)
				{
		        	System.out.println(e.getMessage());
		 			System.out.println("Something went wrong in BNIA442\n"+e.getMessage());
		 			exception(e.getMessage());
		 		}
		
		
				/*BNIA441 - Verify that product condition should be shown with description*/
				ChildCreation("BNIA441 - Verify that product condition should be shown with description");
				desc = false;
				try
				{	
						try
						{
						WebElement readmore = driver.findElement(By.xpath("((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+productsel+"]//*[@class='sk_bn_readMoreReview']"));
						readmore.click();
						String ConditionDesc = driver.findElement(By.xpath("((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+productsel+"]//*[@class = 'sk_bn_mktReviewVal']")).getText();
						System.out.println(" "+ConditionDesc);
						if(!ConditionDesc.isEmpty())
						{
							log.add("The Seller Name is \n"+ConditionDesc);
							pass("Product condition is shown with description",log);
							desc=true;
						}
						else
						{
							fail("The seller Name is not shown");
							desc = false;
						}
						}
						catch(Exception e)
						{
							Skip("Read more link is not shown for this product");
							log.add("readmore link is not available");
							try
							{
								String ConditionDesc = driver.findElement(By.xpath("((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+productsel+"]//*[@class = 'sk_bn_mktReviewVal']")).getText();
								System.out.println(""+ConditionDesc);
								if(desc)
								{
									log.add("The Seller Name is \n"+ConditionDesc);
									pass("Product condition is shown with description",log);
									desc=true;
								}
								else
								{
									fail("The seller Name is not shown");
									desc = false;
								}
							}
							catch(Exception ee)
							{
								System.out.println("Condition is not shown for this product");
								Skip("product condition is not shown with description for this product");
							}
							
						}
				  }     
				  catch(Exception e)
				  {
				        System.out.println(e.getMessage());
				 		System.out.println("Something went Wrong in BNIA441 "+e.getMessage());
				 		exception(e.getMessage());
				 		desc = false;
				 }
				
				
				/*BNIA443 - Verify that while tapping Read more link that should shown more about description */
				ChildCreation("BNIA443 - Verify that while tapping Read more link that should shown more about description ");
				try
				{
					if(desc == true)
					{
						pass("while tapping Read more link it shows more about the description");
					}
					else
					{
						Skip("Read more link is not shown");
						//fail("while tapping Read more link it does not shows more about the description");
					}
				}
				catch(Exception e)
				{
					System.out.println(e.getMessage());
		 			System.out.println("Something went wrong in BNIA443\n"+e.getMessage());
		 			exception(e.getMessage());
		        }
				
				/*BNIA444 - Verify that + icon  should be shown for shipping option */
				ChildCreation("BNIA444 - Verify that + icon  should be shown for shipping option ");
				try
				{
					WebElement plusicon1 = driver.findElement(By.xpath("(((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+productsel+"]//*[@class='sk_bn_mktShippingIcon'])"));
					if(BNBasicCommonMethods.isElementPresent(plusicon1))
					{
						log.add("Plus icon is shown");
						pass("+ icon is shown for shipping option",log);
					}
					else
					{
						fail("+ icon is not shown for shipping option");
					}
				}
				catch(Exception e)
				{
		        	System.out.println(e.getMessage());
		 			System.out.println("Something went wrong in BNIA444\n"+e.getMessage());
		 			exception(e.getMessage());
		 		}
				
				/*BNIA445 - Verify that while tapping "+" icon that should display shipping options in tooltip-container*/
				ChildCreation("BNIA445 - Verify that while tapping "+" icon that should display shipping options in tooltip-container");
				try
				{
					WebElement plusicon1 = driver.findElement(By.xpath("(((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+productsel+"]//*[@class='sk_bn_mktShippingIcon'])"));
					plusicon1.click();
					//*[@class='sk_bn_mktSingleItemCont Used shippingOptEnabled']
					WebElement plusActive = driver.findElement(By.xpath("//*[@class='sk_bn_mktShippingIcon activeShippingIcon']"));
					if(plusActive.isDisplayed())
					{
						log.add("Tooltip Container is shown");
						pass("While tapping + icon it displays shipping options in tooltip-container",log);
					}
					else
					{
						fail("While tapping + icon it does not displays shipping options in tooltip-container");
					}
				}
				catch(Exception e)
				{
		        	System.out.println(e.getMessage());
		 			System.out.println("Something went wrong in BNIA444 : "+e.getMessage());
		 			exception(e.getMessage());
		 		}
				
				/*BNIA446 - Verify that + icon should be highlighted(greyed out) while displaying shipping options in tooltip-container*/
				ChildCreation("BNIA446 - Verify that + icon should be highlighted(greyed out) while displaying shipping options in tooltip-container");
				try
				{
					/*WebElement plusicon1 = driver.findElement(By.xpath("(((//*[@class='sk_marketPlaceCont']//*[@class='sk_marketPlaceContent'])["+(sel+1)+"]//*[contains(@class,'sk_bn_mktSingleItemCont')]//*[contains(@class,'sk_bn_mktMiddleCont')])["+productsel+"]//*[@class='sk_bn_mktShippingIcon'])"));
					plusicon1.click();*/
					//*[@class='sk_bn_mktSingleItemCont Used shippingOptEnabled']
					WebElement plusActive = driver.findElement(By.xpath("//*[@class='sk_bn_mktShippingIcon activeShippingIcon']"));
					String activepluscolor = plusActive.getCssValue("background-position");
					sheet = BNBasicCommonMethods.excelsetUp("PDP");
					String color = BNBasicCommonMethods.getExcelVal("BNIA446", sheet, 5);
					if(activepluscolor.equals(color))
					{
						log.add("The Active Plus Icon Color is\n"+activepluscolor);
						pass("+ icon is highlighted(greyed out) while displaying shipping options in tooltip-container",log);
					}
					else
					{
						fail("+ icon is not highlighted(greyed out) while displaying shipping options in tooltip-container");
					}
				}
				catch(Exception e)
				{
		        	System.out.println(e.getMessage());
		 			System.out.println("Something went wrong in BNIA446\n"+e.getMessage());
		 			exception(e.getMessage());
		 		}
				
				/*BNIA448 - Verify that listed products should be scrolled vertically*/
				ChildCreation("BNIA448 - Verify that listed products should be scrolled vertically");
				try
				{
				
				     if(ProductConditionScroll.isDisplayed())
				     {
				    	 pass("Market place listed products are scrolling vertically"); 
				     }
				     
				     else
				     {
				    	 fail("Market palce listed products are not scrolling vertically");
				     }
				}
				catch(Exception e)
				{
			    	System.out.println(e.getMessage());
					System.out.println("Something went wrong in BNIA448\n"+e.getMessage());
					exception(e.getMessage());
				}
	}
	
	/*BNIA447 - Verify that while selecting the All,New and Used sections in the Marketplace overlay,the respective tab should be displayed */
	public void BNIA447()
	{
		ChildCreation("BNIA447 - Verify that while selecting the All,New and Used sections in the Marketplace overlay,the respective tab should be displayed ");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "MarketplaceOverlay");
			Thread.sleep(1000);
			try
			{
				
				/*AllTab.click();
				Thread.sleep(1000);*/
				BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveAllTab");
			    WebElement AllTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveAllTab");
			    WebElement NewTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_NewTab");
			    WebElement UsedTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_UsedTab");
				String alltab = AllTabActive.getText();
				WebElement Details = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_TabDetails");
				if(BNBasicCommonMethods.isElementPresent(AllTabActive) && BNBasicCommonMethods.isElementPresent(Details))
				{
					log.add("The Active Tab is\n"+alltab);
				}
				else
				{
					return;
				}
				NewTab.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveNewTab");
				WebElement NewTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveNewTab");
				String newtab = NewTabActive.getText();
				if((BNBasicCommonMethods.isElementPresent(NewTabActive)) && (BNBasicCommonMethods.isElementPresent(Details)))
				{
					log.add("The Active Tab is \n"+newtab);
				}
				else
				{
					return;
				}
				UsedTab.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveUsedTab");
				WebElement UsedTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveUsedTab");
				String usedtab = UsedTabActive.getText();
				if(BNBasicCommonMethods.isElementPresent(UsedTabActive) && (BNBasicCommonMethods.isElementPresent(Details)))
				{
					log.add("The Active tab is\n"+usedtab);
					pass("While selecting the All,New and Used sections in the Marketplace overlay,the respective tab is displayed",log);
				}
				else
				{
					fail("While selecting the All,New and Used sections in the Marketplace overlay,the respective tab is not displayed");
				}
				WebElement AllTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_AllTab");
				AllTab.click();
			}
			catch(Exception e1)
			{
				try
				{
					BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveNewTab");
					WebElement NewTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveNewTab");
					WebElement Details = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_TabDetails");
					String newtab = NewTabActive.getText();
					if((BNBasicCommonMethods.isElementPresent(NewTabActive)) && (BNBasicCommonMethods.isElementPresent(Details)))
					{
						log.add("The Active Tab is \n"+newtab);
					}
					else
					{
						return;
					}
					WebElement AllTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_AllTab");
					AllTab.click();
					Thread.sleep(1000);
					BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveAllTab");
				    WebElement AllTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveAllTab");
				   String alltab = AllTabActive.getText();
				   if(BNBasicCommonMethods.isElementPresent(AllTabActive) && BNBasicCommonMethods.isElementPresent(Details))
					{
						log.add("The Active Tab is\n"+alltab);
					}
					else
					{
						return;
					}
					WebElement UsedTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_UsedTab");
					UsedTab.click();
					BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveUsedTab");
					WebElement UsedTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveUsedTab");
					String usedtab = UsedTabActive.getText();
					if(BNBasicCommonMethods.isElementPresent(UsedTabActive) && (BNBasicCommonMethods.isElementPresent(Details)))
					{
						log.add("The Active tab is\n"+usedtab);
						pass("While selecting the All,New and Used sections in the Marketplace overlay,the respective tab is displayed",log);
					}
					else
					{
						fail("While selecting the All,New and Used sections in the Marketplace overlay,the respective tab is not displayed");
					}
					
				}
				catch(Exception e2)
				{
					BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveUsedTab");
					WebElement UsedTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveUsedTab");
					String usedtab = UsedTabActive.getText();
					WebElement Details = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_TabDetails");
					if(BNBasicCommonMethods.isElementPresent(UsedTabActive) && (BNBasicCommonMethods.isElementPresent(Details)))
					{
						log.add("The Active tab is\n"+usedtab);
						pass("While selecting the All,New and Used sections in the Marketplace overlay,the respective tab is displayed",log);
					}
					else
					{
						fail("While selecting the All,New and Used sections in the Marketplace overlay,the respective tab is not displayed");
					}
					 WebElement NewTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_NewTab");
					NewTab.click();
					BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveNewTab");
					WebElement NewTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveNewTab");
					String newtab = NewTabActive.getText();
					if((BNBasicCommonMethods.isElementPresent(NewTabActive)) && (BNBasicCommonMethods.isElementPresent(Details)))
					{
						log.add("The Active Tab is \n"+newtab);
					}
					else
					{
						return;
					}
					WebElement AllTab = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_AllTab");
					AllTab.click();
					Thread.sleep(1000);
					BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ActiveAllTab");
				    WebElement AllTabActive = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ActiveAllTab");
				    String alltab = AllTabActive.getText();
					
					if(BNBasicCommonMethods.isElementPresent(AllTabActive) && BNBasicCommonMethods.isElementPresent(Details))
					{
						log.add("The Active Tab is\n"+alltab);
					}
					else
					{
						return;
					}
				}
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA447\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
		
	/*BNIA449 - Verify that Market place overlay should be close on tapping the close button.*/
	public void BNIA449()
	{
		ChildCreation("BNIA449 - Verify that Market place overlay should be close on tapping the close button.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_CloseIcon");
			WebElement closeIcon = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_CloseIcon");
			closeIcon.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "EmailButton");
			WebElement pdpemailbutton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			if(pdpemailbutton.isDisplayed())
			{
				pass("Market place overlay is closed on tapping the close button.");
			}
			else
			{
				fail("Market place overlay is not closed on tapping the close button.");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA449\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA714 - Verify that Market place overlay Price of the Product should be displayed with $(ie $XX.X)*/
	public void BNIA714()
	{
		ChildCreation("BNIA714 - Verify that Market place overlay Price of the Product should be displayed with $(ie $XX.X)");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "Marketplace");
			WebElement marketplacelink = BNBasicCommonMethods.findElement(driver, obj, "Marketplace");
			marketplacelink.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "MarketplaceOverlay");
			WebElement marketplaceprice1 = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_Price1");
			WebElement marketplaceprice2 = driver.findElement(By.xpath("//*[@class = 'sk_bn_currencyCode']"));
			
			String price = marketplaceprice1.getText();
			String str = price.replace("$", "");
			String price1 = marketplaceprice2.getText();
			String str1 = ""+price1+str;
			if(price.equals(str1))
			{
				log.add("The Price is shown as\n"+str1);
				pass("Market place overlay Price of the Product is displayed with $(ie $XX.X)",log);
				
			}
			else
			{
				fail("Market place overlay Price of the Product is not displayed with $(ie $XX.X)");
			}
			Thread.sleep(1000);
			BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_CloseIcon");
			WebElement closeicon = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_CloseIcon");
			closeicon.click();
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA714\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA715 -  Verify that when Add to Bag button is clicked in overlay, then it should navigate to the corresponding page.*/
	public void BNIA715()
	{
		ChildCreation("BNIA715 -  Verify that when Add to Bag button is clicked in overlay, then it should navigate to the corresponding page.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String EAN = BNBasicCommonMethods.getExcelNumericVal("BNIA363", sheet, 3);
			String URL = "http://"+"bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+EAN;
			Thread.sleep(1000);
			driver.get(URL);
			Thread.sleep(3000);
			int ctr=1;
			do
			{
				try
				{
					BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOption");
					WebElement AllFormats = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOption");
					if(BNBasicCommonMethods.isElementPresent(AllFormats))
					{
						Thread.sleep(2000);
						AllFormats.click();
					}
					Thread.sleep(2000);
					List<WebElement> DropdownList = driver.findElements(By.xpath("//*[@class = 'sk_bn_formatOption']"));
					for(int i=1;i<=DropdownList.size();i++)
					{
						WebElement dropdownoptions = driver.findElement(By.xpath("(//*[@class = 'sk_bn_formatOption'])["+i+"]"));
						String drdplist = dropdownoptions.getAttribute("value");
						System.out.println(""+drdplist);
						if(drdplist.equals("allFormat"))
						{
							Thread.sleep(3000);
							dropdownoptions.click();
							Thread.sleep(3000);
							
						}
						else
						{
							System.out.println("There is no All Fromats Option in the dropdown menu for the Selected product");
						}
					}
					
					try
					{
						Thread.sleep(2000);
						BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOverlay");
						JavascriptExecutor js = (JavascriptExecutor) driver;
					    js.executeScript("skMob.hideLoadingSpinner();");
						List<WebElement> formatSize = driver.findElements(By.xpath("(//*[@class='sk_allFormatContent']//*[@class='sk_allFormatDetailsCont'])[1]"));
						for(int i = 0; i<formatSize.size();i++)
						{
						Random r = new Random();
						List<WebElement> ATB = driver.findElements(By.xpath("(//*[@class='sk_allFormatContent']//*[@class='sk_allFormatDetailsCont'])["+(i+1)+"]//*[@class = 'sk_allFromatAddToBagBtn ']"));
						int sel = r.nextInt(ATB.size());
						Thread.sleep(500);
						ATB.get(sel).click();
						
						Thread.sleep(2500);
						if(BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay").isDisplayed())
						{
							ctr=5;
							break;
						}
						}
						/*WebElement ATB1 = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOverlay_ATB1");
						ATB1.click();*/
						try
						{
							BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay");
							WebElement ATBSuccessalert = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay");
							Thread.sleep(1000);
							WebElement ATBSuccessalert_ContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ContinueButton");
							if((ATBSuccessalert.isDisplayed()) && (ATBSuccessalert_ContinueBtn.isDisplayed()))
							{
								log.add("Continue Button is shown in the Success Overlay");
								pass("When Add to Bag button is clicked in overlay, then it navigate to the corresponding page or it shows success overlay",log);
							}
							else
							{
								fail("When Add to Bag button is clicked in overlay, then it does not navigate to the corresponding page or it shows success overlay");
							}
						}
						catch(Exception e)
						{
							System.out.println("ATB Success Overlay is not shown");
						}
					}
					catch(Exception e1)
					{
						BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATBclick_ErrorAlert");
						WebElement OkButton = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATBclick_ErrorAlertOkButton");
						OkButton.click();
					}
					
					
					
					/*BNBasicCommonMethods.waitforElement(wait, obj, "MarketPlace_ATBButton1");
					WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ATBButton1");
					ATB.click();
					BNBasicCommonMethods.waitforElement(wait, obj, "PDP_SuccessOverlay");
					WebElement ATBSuccessalert = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay");
					WebElement ATBSuccessalert_ContinueBtn = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ContinueButton");
					if((ATBSuccessalert.isDisplayed()) && (ATBSuccessalert_ContinueBtn.isDisplayed()))
					{
						log.add("Continue Button is shown in the Success Overlay");
						pass("When Add to Bag button is clicked in overlay, then it navigate to the corresponding page or it shows success overlay",log);
					}
					else
					{
						fail("When Add to Bag button is clicked in overlay, then it does not navigate to the corresponding page or it shows success overlay");
					}*/
						
				}
				catch(Exception e1)
				{
					System.out.println("Format Dropdown is not shown");
					String URL1 = "http://"+"bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+EAN;
					driver.get(URL1);
					Thread.sleep(3000);
					ctr++;
					//continue;
				}
			}while(ctr<=4);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA715\n"+e.getMessage());
 			exception("There is something went wrong"+e.getMessage());
 		}
	}		
	
	/*BNIA716 - Verify that Add to Bag button should be highlighted in bold.*/
	public void BNIA716()
	{
		ChildCreation("BNIA716 - Verify that Add to Bag button should be highlighted in bold.");
		try
		{
			WebElement continueshop = BNBasicCommonMethods.findElement(driver, obj, "PDP_SuccessOverlay_ContinueButton");
			continueshop.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOverlay_ATB1");
			WebElement ATB1 = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOverlay_ATB1");
			String atb = ATB1.getCssValue("background-color");
			Color bagcolors = Color.fromString(atb);
			String hexbagcolors = bagcolors.asHex();
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String bagColor = BNBasicCommonMethods.getExcelVal("BNIA716", sheet, 6);
			if(hexbagcolors.equals(bagColor))
			{
				log.add("The hexa value of RED color is\n"+bagcolors);
				pass("Add to Bag button is highlighted in bold(RED COLOR)",log);
			}
			else
			{
				fail("Add to Bag button is not highlighted in bold(RED COLOR)");
			}
			
			/*WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "MarketPlace_ATBButton1");
			String atb = ATB.getCssValue("background-color");
			Color bagcolors = Color.fromString(atb);
			String hexbagcolors = bagcolors.asHex();
			String bagColor = "#000000";
			if(hexbagcolors.equals(bagColor))
			{
				log.add("The hexa value of RED color is "+bagcolors);
				pass("Add to Bag button is highlighted in bold(WHITE COLOR)",log);
			}
			else
			{
				fail("Add to Bag button is not highlighted in bold(WHITE COLOR)");
			}*/
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA716\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}	
	
	/*BNIA717 - Verify that in Add to Bag button the Add to Bag - $X.XX inline text should be shown.*/
	public void BNIA717()
	{
		ChildCreation("BNIA717 - Verify that in Add to Bag button the Add to Bag - $X.XX inline text should be shown.");
		try
		{
			WebElement ATB1 = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOverlay_ATB1");
			String atb = ATB1.getText();
			//String atbsplit = atb.replace("$", "");
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String atbText = BNBasicCommonMethods.getExcelVal("BNIA717", sheet, 1);
			if(atb.contains(atbText))
			{
				log.add("The ATB Inline text is\n"+atb);
				pass("In Add to Bag button the Add to Bag - $X.XX inline text is shown",log);
			}
			else
			{
				fail("In Add to Bag button the Add to Bag - $X.XX inline text is not shown");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA717\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA719 - Verify that each tab switching options should display Product count as Paperback(XX), Hardcovers(XX), and so on...*/
	public void BNIA719()
	{
		ChildCreation("BNIA719 - Verify that each tab switching options should display Product count as Paperback(XX), Hardcovers(XX), and so on...");
		try
		{
			boolean PgOk = BNBasicCommonMethods.waitforElement(wait, obj, "PdpPageAllFormatList");
			if(PgOk==true)
			{
				Thread.sleep(1000);
				List<WebElement> formatSize = driver.findElements(By.xpath("//*[@class='sk_allFormatTitleCont']//*[contains(@class,'allFormatProductTitle')]"));
				for(int i = 0; i<formatSize.size();i++)
				{
					formatSize.get(i).click();
					Thread.sleep(2500);
					String str = driver.findElement(By.xpath("//*[@class='sk_allFormatTitleCont']//*[contains(@class,'allFormatProductTitle')]["+(i+1)+"]")).getText();
					System.out.println(""+str);
					if(driver.findElement(By.xpath("//*[@class='sk_allFormatTitleCont']//*[contains(@class,'allFormatProductTitle')]["+(i+1)+"]")).isDisplayed())
					{
						log.add("+str+\n" +"Title is shown");
						pass("Each tab switching options displays Product count as Paperback(XX), Hardcovers(XX), and so on...",log);
					}
					else
					{
						fail("Each tab switching options displays Product count as Paperback(XX), Hardcovers(XX), and so on...");
					}	
				}
				WebElement paperback = BNBasicCommonMethods.findElement(driver, obj, "AllFormatsOverlay_PaperBackTab");
				paperback.click();
			}			 
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA719\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA528 - Verify that on tapping outside the PDP page, the hamburger menu should be closed. */
	public void BNIA528()
	{
		ChildCreation("BNIA528 - Verify that on tapping outside the PDP page, the hamburger menu should be closed. ");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOverlay_Closeicon");
			WebElement close = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOverlay_Closeicon");
			close.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "PancakeMenu");
			WebElement hamburger = BNBasicCommonMethods.findElement(driver, obj, "PancakeMenu");
			hamburger.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Mask");
			WebElement mask = BNBasicCommonMethods.findElement(driver, obj, "Mask");
			mask.click();
			if(!mask.isDisplayed())
			{
				pass("On tapping outside the PDP page, the hamburger menu is closed");
			}
			else
			{
				fail("On tapping outside the PDP page, the hamburger menu should is not closed");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA528\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA561 - Verify that  On tapping outside the Email this Item overlay ,that should be closed*/
	public void BNIA561()
	{
		ChildCreation("BNIA561 - Verify that  On tapping outside the Email this Item overlay ,that should be closed");
		try
		{
			WebElement emailbutton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			emailbutton.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPContainer");
			//WebElement emailbutton1 = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			WebElement emailbutton2 = BNBasicCommonMethods.findElement(driver, obj, "EmailMask");
			emailbutton2.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductName");
			if(BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductName").isDisplayed())
			{
				log.add("The PDP page is shown");
				pass("On tapping outside the Email this Item overlay ,it gets closed",log);
			}
			else
			{
				fail("On tapping outside the Email this Item overlay ,it does not gets closed");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA561"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA571 - Verify that empty section should not be shown in the PDP page, when the respective products does not have certain sections or links.*/
	public void BNIA571()
	{
		ChildCreation("BNIA571 - Verify that empty section should not be shown in the PDP page, when the respective products does not have certain sections or links.");
		try
		{
			Thread.sleep(2000);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPContainer");
			//WebElement overview = BNBasicCommonMethods.findElement(driver, obj, "PDPOverviewLink");
			try
			{
				WebElement overviewsection = BNBasicCommonMethods.findElement(driver, obj, "PDPOverviewSection");
				if(BNBasicCommonMethods.isElementPresent(overviewsection))
				{
					log.add("Overview section is shown");
				}
				else
				{
					return;
				}
			}
			catch(Exception e1)
			{
				Skip("There is no Overview Link for the Selected product");
			}
			
			try
			{
				
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomeralsoBoughtLink");
				WebElement cuslink = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomeralsoBoughtLink");
				cuslink.click();
				Thread.sleep(3000);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerAlsoBoughtThisSection");
				WebElement showmorelink = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerAlsoBoughtThisSection_ShowMoreLink");
				showmorelink.click();
				
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerAlsoBoughtThisSection_Products");
				WebElement customeralsoboughtsection = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerAlsoBoughtThisSection_Products");
				if(BNBasicCommonMethods.isElementPresent(customeralsoboughtsection))
				{
					log.add("The customer also bought section is shown");
				}
				else
				{
					return;
				}
			}
			catch(Exception e2)
			{
				Skip("There is no Customer Also Bought Section for the selected product");
			}
			
			try
			{
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomerReviewsLink");
				WebElement customerreviewslink = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerReviewsLink");
				customerreviewslink.click();
				
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_CustomerReviewsSection");
				WebElement reviews = BNBasicCommonMethods.findElement(driver, obj, "PDP_CustomerReviewsSection_Reviews");
				if(BNBasicCommonMethods.isElementPresent(reviews))
				{
					log.add("THe Customer Reviews section is shown");
				}
				else
				{
					return;
				}
			}
			catch(Exception e3)
			{
				Skip("There is no Customer Reviews section for the selected product");
			}
			
			try
			{
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDPCustomerEditorialReviewsLink");
				WebElement editoriallink = BNBasicCommonMethods.findElement(driver, obj, "PDPCustomerEditorialReviewsLink");
				editoriallink.click();
				
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_EditorialReviewsSection");
				WebElement editorialReviews = BNBasicCommonMethods.findElement(driver, obj, "PDP_EditorialReviewsSection_Reviews");
				if(BNBasicCommonMethods.isElementPresent(editorialReviews))
				{
					log.add("Editorial Reviews is Shown");
				}
				else
				{
					return;
				}
			}
			catch(Exception e4)
			{
				Skip("There is no Editorial Reviews Section for the selected product");
			}
			
			try
			{
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDPMeetTheAuthorLink");
				WebElement authorlink = BNBasicCommonMethods.findElement(driver, obj, "PDPMeetTheAuthorLink");
				authorlink.click();
			
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_MeetTheAuthorSection_Text");
				WebElement authortext = BNBasicCommonMethods.findElement(driver, obj, "PDP_MeetTheAuthorSection_Text");
				if(BNBasicCommonMethods.isElementPresent(authortext))
				{
					log.add("Meet the Author Section is shown ");
				}
				else
				{
					return;
				}
			}
			catch(Exception e5)
			{
				Skip("There is no Meet the Author section for the selected product");
			}
			
			try
			{
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDPProductDetailsLink");
				WebElement prddetailslink = BNBasicCommonMethods.findElement(driver, obj, "PDPProductDetailsLink");
				prddetailslink.click();
				
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ProductDetailsSection");
				WebElement prddetails = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductDetailsSection_Content");
				if(BNBasicCommonMethods.isElementPresent(prddetails))
				{
					log.add("The Product Details Section is Shown");
				}
				else
				{
					return;
				}
			}
			catch(Exception e6)
			{
				Skip("There is no Product Details Section for the Selected product");
			}
			
			try
			{
				Thread.sleep(2000);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDPRelatedSubjectsLink");
				WebElement relatedsublink = BNBasicCommonMethods.findElement(driver, obj, "PDPRelatedSubjectsLink");
				relatedsublink.click();
				
				BNBasicCommonMethods.waitforElement(wait, obj, "PDP_RelatedSubjectsSection_Link");
				WebElement relatessublinks = BNBasicCommonMethods.findElement(driver, obj, "PDP_RelatedSubjectsSection_Link");
				if(BNBasicCommonMethods.isElementPresent(relatessublinks))
				{
					log.add("The Related Subjects Links is shown");
					pass("Empty section is not shown in the PDP page, when the respective products does not have certain sections or links.");
				}
				else
				{
					fail("Empty section is shown in the PDP page, when the respective products does not have certain sections or links.");
				}
			}
			catch(Exception e7)
			{
				Skip("There is no Related subjects link for the selected product");
			}
			WebElement scroll = BNBasicCommonMethods.findElement(driver, obj, "PDP_ProductName");
			BNBasicCommonMethods.scrollup(scroll, driver);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA571\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA573 - Verify that  other images of the product displayed below the primary image should be as per creative*/
	public void BNIA573()
	{
		ChildCreation("BNIA573 - Verify that  other images of the product displayed below the primary image should be as per creative");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "secondaryimages");
			WebElement secImages = BNBasicCommonMethods.findElement(driver, obj, "secondaryimages");
			if(BNBasicCommonMethods.isElementPresent(secImages))
			{
				pass("other images of the product is displayed below the primary image in the product detail page");
			}
			else
			{
				fail("other images of the product is not displayed below the primary image in the product detail page");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA573\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA576 - Verify that Format option in PDP page should be as per creative*/
	public void BNIA576()
	{
		ChildCreation("BNIA576 - Verify that Format option in PDP page should be as per creative");
		try
		{
			Thread.sleep(2000);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AllFormatsOption");
			WebElement allformats = BNBasicCommonMethods.findElement(driver, obj, "PDP_AllFormatsOption");
			String inactiveallformats = allformats.getCssValue("color");
			Color inactiveallformatscolors = Color.fromString(inactiveallformats);
			String hexinactiveformatscolors = inactiveallformatscolors.asHex();
			Thread.sleep(2000);
			allformats.click();
			Thread.sleep(2000);
			String activeformats = allformats.getCssValue("background-color");
			Color activeallformatscolors = Color.fromString(activeformats);
			String hexactiveformatscolors = activeallformatscolors.asHex();
			sheet = BNBasicCommonMethods.excelsetUp("PDP");
			String inactive_formats = BNBasicCommonMethods.getExcelVal("BNIA378", sheet, 6);
			String active_formats = BNBasicCommonMethods.getExcelVal("BNIA378", sheet, 6);
			if((hexinactiveformatscolors.equals(inactive_formats)) && (hexactiveformatscolors.equals(active_formats)))
			{
				log.add("The Inactive Allformats Text Color is\n"+hexinactiveformatscolors);
				log.add("The Active Allformats Button Color is\n"+hexactiveformatscolors);
				pass("Format option in PDP page is as per creative",log);
			}
			else
			{
				fail("Format option in PDP page is not as per creative");
			}
			allformats.click();
			Thread.sleep(2000);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA576\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA577 -  Verify that  Email this Item overlay is displayed user should not able to access the PDP page */
	public void BNIA577() throws Exception
	{
		ChildCreation("BNIA577 -  Verify that Email this Item overlay is displayed user should not able to access the PDP page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "EmailButton");
			WebElement emailbutton = BNBasicCommonMethods.findElement(driver, obj, "EmailButton");
			Thread.sleep(2000);
			emailbutton.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "EmailOverlay");
			if(BNBasicCommonMethods.findElement(driver, obj, "EmailButton").isEnabled())
			{
				Thread.sleep(2000);
				emailbutton.click();
				fail("Email this Item overlay is displayed user is able to access the PDP page");
			}
			else
			{
				return;
			}
			
		}
		catch(Exception e)
		{
			pass("Email this Item overlay is displayed user not able to access the PDP page");
			WebElement cancelbtn = BNBasicCommonMethods.findElement(driver, obj, "EmailCancelButton");
			Thread.sleep(2000);
			cancelbtn.click();
			Thread.sleep(2000);
        }
	}
	
	/*BNIA579 - Verify that overview section should display overview title with overview description*/
	public void BNIA579()
	{
		ChildCreation("BNIA579 - Verify that overview section should display overview title with overview description");
		try
		{
			//WebElement overview = BNBasicCommonMethods.findElement(driver, obj, "PDPOverviewLink");
			WebElement overviewsection = BNBasicCommonMethods.findElement(driver, obj, "PDPOverviewSection");
			WebElement overviewsection_Title = BNBasicCommonMethods.findElement(driver, obj, "PDPOverview_TitleText");
			if((BNBasicCommonMethods.isElementPresent(overviewsection)) && (BNBasicCommonMethods.isElementPresent(overviewsection_Title)))
			{
				pass("Overview section displays overview title with overview description");
			}
			else
			{
				fail("Overview section not displays overview title with overview description");
			}
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA579\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	public void BNIA580()
	{
		ChildCreation("BNIA580 - Verify that  while tapping hamburger menu at the header,the PDP page should not be enabled.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "PancakeMenu");
			WebElement hamburgermenu = BNBasicCommonMethods.findElement(driver, obj, "PancakeMenu");
			hamburgermenu.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "Mask");
			WebElement mask = BNBasicCommonMethods.findElement(driver, obj, "Mask");
			if(BNBasicCommonMethods.isElementPresent(mask))
			{
				pass("while tapping hamburger menu at the header,the PDP page is not enabled.");
			}
			else
			{
				fail("while tapping hamburger menu at the header,the PDP page is enabled.");
			}
			
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA579\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA418 - Verify that Product ONLINE or SHOW IN STORE should be displayed  based on the inventort details*/
	public void BNIA418()
	{
		ChildCreation("BNIA418 - Verify that Product ONLINE or SHOW IN STORE should be displayed  based on the inventort details");
		try
		{
			int ctr=1,count = 0;
			boolean btnFound = false;
			do
			{
				if(!FTprdtId.equals(""))
				{
					do
					{
						String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+FTprdtId;
						driver.get(PDPURL); 
						((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
						((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
						((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
						Thread.sleep(1500);
						try
						{
							//BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ShowInStoreButton");
							//BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AlsoAvailableOnlineTextLink");
							Thread.sleep(2000);
							WebElement ShowInStore = BNBasicCommonMethods.findElement(driver, obj, "PDP_ShowInStoreButton");
							if(BNBasicCommonMethods.isElementPresent(ShowInStore))
							{
								btnFound = true;
								System.out.println(btnFound);
								break;
							}
							else
							{
								btnFound = false;
								count++;
								continue;
							}
						}
						catch(Exception e)
						{
							btnFound = false;
							count++;
							continue;
						}
					}while(count<=10);
					
					try
					{
						BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ShowInStoreButton");
						//BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AlsoAvailableOnlineTextLink");
						WebElement ShowInStore = BNBasicCommonMethods.findElement(driver, obj, "PDP_ShowInStoreButton");
						if(BNBasicCommonMethods.isElementPresent(ShowInStore))
						{
							ctr=6;
							String StoreURL = "http://bnstoremaps.com/product.php?storeNbr="+StoreID+"&itemId="+FTprdtId+"&mode=ast";
							ShowInStore.click();
							driver.get(StoreURL);
							Thread.sleep(2000);
							//WebElement ean = BNBasicCommonMethods.findElement(driver, obj, "PDP_ShowInStore_EAN");
							//String eanno = ean.getText();
							driver.navigate().back();
							if (driver instanceof JavascriptExecutor) 
							{
							    ((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
							    ((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
							    ((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
							} 
							else 
							{
							    throw new IllegalStateException("This driver does not support JavaScript!");
							}
							
							pass("Product ONLINE or SHOW IN STORE button is displayed  based on the inventort details");
						}
						else
						{
							System.out.println("Show in Store button is not shown");
						}
						
					}
					catch(Exception e)
					{
						System.out.println("Show in Store Button is not shown");
						ctr++;
					}
				}
			
			else
			{
			System.out.println("FTProduct ID is not found");
			}
		  }while(ctr <= 5);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA418\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA419 - Verify that SHOW IN STORE & ADD TO BAG buttons should be shown when the product available in both store & online */
	public void BNIA419()
	{
		ChildCreation("BNIA419 - Verify that SHOW IN STORE & ADD TO BAG buttons should be shown when the product available in both store & online ");
		try
		{
			Thread.sleep(1500);
			int ctr=1;
			do
			{
				if(!TTprdtId.equals(""))
				{
					String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+TTprdtId;
					driver.get(PDPURL); 
					((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
					((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
					((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
				try
					{
						Thread.sleep(2500);
						//BNBasicCommonMethods.waitforElement(wait, obj, "PDP_AlsoAvailableOnlineTextLink");
						WebElement AlsoAvailableOnlineText = BNBasicCommonMethods.findElement(driver, obj, "PDP_AlsoAvailableOnlineTextLink");
						WebElement ShowInStore = BNBasicCommonMethods.findElement(driver, obj, "PDP_ShowInStoreButton");
						if(BNBasicCommonMethods.isElementPresent(AlsoAvailableOnlineText))
						{
							ctr=6;
							AlsoAvailableOnlineText.click();
							Thread.sleep(1000);
							BNBasicCommonMethods.waitforElement(wait, obj, "pdp_ATB");
							WebElement ATB = BNBasicCommonMethods.findElement(driver, obj, "pdp_ATB");
							if(BNBasicCommonMethods.isElementPresent(ATB) && BNBasicCommonMethods.isElementPresent(ShowInStore))
							{
								pass("SHOW IN STORE & ADD TO BAG buttons is shown when the product available in both store & online");
							}
							else
							{
								fail("SHOW IN STORE & ADD TO BAG buttons is not shown when the product available in both store & online");
							}
						}
					}
					catch(Exception e)
					{
						System.out.println("Show in Store Button is not shown");
						ctr++;
					}
				}
				else
				{
				System.out.println("FTProduct ID is not found");
				}
			}while(ctr <=5);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA419\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA420 - Verify that SHOW IN STORE button should be shown only when the product is available in Store*/
	public void BNIA420()
	{
		ChildCreation("BNIA420 - Verify that SHOW IN STORE button should be shown only when the product is available in Store");
		try
		{
			int ctr=1;
			do
			{
				if(!FTprdtId.equals(""))
				{
					String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+FTprdtId;
					driver.get(PDPURL); 
				    ((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
				    ((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
				    ((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
				try
					{
						Thread.sleep(2500);
						//BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ShowInStoreButton");
						WebElement ShowInStore = BNBasicCommonMethods.findElement(driver, obj, "PDP_ShowInStoreButton");
						if(BNBasicCommonMethods.isElementPresent(ShowInStore))
						{
							ctr=11;
							Thread.sleep(1000);
							
							if(BNBasicCommonMethods.isElementPresent(ShowInStore))
							{
								pass("SHOW IN STORE button is shown only when the product is available in Store");
							}
							else
							{
								fail("SHOW IN STORE button is not shown only when the product is available in Store");
							}
						}
					}
					catch(Exception e)
					{
						System.out.println("Show in Store Button is not shown");
						ctr++;
					}
				}
				else
				{
				System.out.println("FTProduct ID is not found");
				}
			}while(ctr <=10);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA419\n"+e.getMessage());
 			exception(e.getMessage());
 		}
	}
	
	/*BNIA411 - Verify that product availability should be shown on after selecting format option */
	/*BNIA416 - Verify that Sale Price alone should be shown, when the product is available only  in store*/ 
	public void BNIA411()
	{
		boolean childflag = false;
		ChildCreation("BNIA411 - Verify that product availability should be shown on after selecting format option ");
		try
		{
			int ctr=1,count = 1;
			boolean btnFound = false;
			do
			{
				if(!FTprdtId.equals(""))
				{
					do
					{ 
						childflag = true; 
						String PDPURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam="+FTprdtId;
						driver.get(PDPURL); 
						((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
						((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
						Thread.sleep(1500);
						try
						{
							Thread.sleep(3000);
							WebElement ShowInStore = BNBasicCommonMethods.findElement(driver, obj, "PDP_ShowInStoreButton");
							if(BNBasicCommonMethods.isElementPresent(ShowInStore))
							{
								count=11;
								WebElement Storeprice = BNBasicCommonMethods.findElement(driver, obj, "PDP_StorePrice");
								if(Storeprice.isDisplayed())
								{
									try
									{
										WebElement Saleprice = BNBasicCommonMethods.findElement(driver, obj, "PDP_SalePrice");
										if(Saleprice.isDisplayed())
										{
											fail("Sale Price alone is not shown, when the product is available only in store");
										}
									}
									catch(Exception e1)
									{
										childflag = true; 
										//pass("Sale Price alone is shown, when the product is available only in store");
									}
								}
								btnFound = true;
								System.out.println(btnFound);
								break;
							}
							else
							{
								btnFound = false;
								count++;
								continue;
							}
						}
						catch(Exception e)
						{
							btnFound = false;
							count++;
							continue;
						}
					}while(count<=10);
					
					try
					{
						BNBasicCommonMethods.waitforElement(wait, obj, "PDP_ShowInStoreButton");
						WebElement AvailabilityText = BNBasicCommonMethods.findElement(driver, obj, "PDP_PrdAvailabilityText");
						if(BNBasicCommonMethods.isElementPresent(AvailabilityText))
						{
						ctr=6;
						pass("product availability is shown on after selecting format option");
						}
						else
						{
							System.out.println("Show in Store button is not shown");
						}
					}
					catch(Exception e)
					{
						System.out.println("Show in Store Button is not shown");
						ctr++;
					}
				}
			
			else
			{
			System.out.println("FTProduct ID is not found");
			}
		  }while(ctr <= 5);
		}
		catch(Exception e)
		{
        	System.out.println(e.getMessage());
 			System.out.println("Something went wrong in BNIA418\n"+e.getMessage());
 			exception(e.getMessage());
 		}
		
		
		ChildCreation("BNIA416 - Verify that Sale Price alone should be shown, when the product is available only  in store ");
		if(childflag)
			pass("Sale Price alone is shown, when the product is available only in store");
		else
			fail("Sale Price alone is not shown, when the product is available only in store");
	}
	
	
	
 
	public void PDPLogic( )
	{
		//boolean allEanNum = false;
		boolean facets,samerandom;
		int sel = 0;
		ArrayList<Integer> random = new ArrayList<>();
		ArrayList<String> productId = new ArrayList<String>();
		do
		{
			boolean browseOk = browse();
			facets = false;
			if(browseOk==true)
			{
				try
				{
					List<WebElement> browse_pg = driver.findElements(By.xpath("//*[contains(@class, 'hotSpotOverlay')]"));
			 	 	Random ran = new Random();
			 	 	do
			 	 	{
			 	 		sel = ran.nextInt(browse_pg.size());
			 	 		if(sel<1)
			 	 		{
			 	 			sel = 1;
			 	 		}
			 	 		samerandom = false;
			 	 		if(random.contains(sel))
			 	 		{
			 	 			samerandom = false;
			 	 			continue;
			 	 		}
			 	 		else
			 	 		{
			 	 			samerandom = true;
			 	 			random.add(sel);
			 	 		}
			 	 	}while(samerandom!=true);
			   	 	
			 	 	if(random.size()!=browse_pg.size())
			 	 	{
				 	 	wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("(//*[contains(@class, 'hotSpotOverlay')])["+sel+"]")));
				 	 	Thread.sleep(500);
				   	 	WebElement menu = driver.findElement(By.xpath("(//*[contains(@class, 'hotSpotOverlay')])["+sel+"]"));
				   	 	BNBasicCommonMethods.scrolldown(menu, driver);
				   	 	Thread.sleep(1000);
				   	 	menu.click();
				   	 	Thread.sleep(2000);
				   	 	String url = driver.getCurrentUrl();
				   	 	if(url.contains("pdp?"))
				   	 	{
				   	 		facets = false;
				   	 		//driver.navigate().back();
					   	 	BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
							Thread.sleep(1000);
							WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
							logo.click();
							Thread.sleep(1000);
				   	 	    continue;
				   	 	}
				   	 	else
				   	 	{
					   	 	Thread.sleep(1000);
					   	 	facets = false;
					   	 	facets = BNBasicCommonMethods.findElement(driver, obj, "PLP_emptyfactes").isDisplayed();
				   	 		if(facets==true)
				   	 		{
				   	 			log.add("PLP page is shown");
				   	 			Thread.sleep(1000);
				   	 			List<WebElement> productListContainer = driver.findElements(By.xpath("//*[@class='skMob_productListItemOuterCont']"));
				   	 			productId.removeAll(productId);
							 	for(int j = 0;j<productListContainer.size();j++)
							 	{
							 		//System.out.println(productListContainer.get(j).getAttribute("prodid"));
							 		productId.add(productListContainer.get(j).getAttribute("prodid"));
							 	} 
							 	
							 	for(int j = 0; j<productId.size();j++)
							 	{
							 		System.out.println(j);
							 		URL onlineInvurl = new URL("http://bninstore.skavaone.com/skavastream/core/v5/bandnkiosk/product/"+productId.get(j)+"?campaignId=1");
							 		String Invjson = IOUtils.toString(onlineInvurl);
							 		JSONObject urlJson = new JSONObject(Invjson);
							 		JSONObject available = urlJson.getJSONObject("properties").getJSONObject("buyinfo").getJSONArray("availability").getJSONObject(0);
							 		//String instore = available.getString("instore");
							 		String onlineinve = available.getString("onlineinventory");
							 		JSONObject marketPlace = urlJson.getJSONObject("properties").getJSONObject("iteminfo").getJSONArray("flags").getJSONObject(1);
							 		String mp = marketPlace.getString("value");
							 		//Need to set the storeId value instead of the storeId
							 		URL storeInvurl = new URL("http://bninstore.skavaone.com/skavastream/xact/v5/bandnkiosk/getinventory?campaignId=1&storeid=2932&skuid="+productId.get(j));
							 		String Strjson = IOUtils.toString(storeInvurl);
							 		JSONObject strUrlJson = new JSONObject(Strjson);
							 		JSONObject state = strUrlJson.getJSONObject("properties").getJSONObject("state");
							 		String stStatus = state.getString("status");
							 		String strIventory = null;
							 		if(stStatus.contains("success"))
							 		{
							 			JSONObject stInfo = strUrlJson.getJSONObject("properties").getJSONArray("storeinfo").getJSONObject(0);
								 		strIventory = stInfo.getString("inventory");
							 		}
							 		
							 		if(pdpTT==false)
								 	{
								 		if((onlineinve.equals("true"))&&(stStatus.equals("success"))&&(strIventory.equals("y")))
								 		{
								 			TTprdtId = productId.get(j);
								 			pdpTT = true;
								 			System.out.println(" TT Products : " + TTprdtId);
								 		}
								 	}
							 		if(pdpTF==false)
								 	{
							 			if((onlineinve.equals("true"))&&(stStatus.equals("success"))&&(strIventory.equals("n")&&(mp.equals("true"))))
								 		{
							 				TFprdtId = productId.get(j);
								 			pdpTF = true;
								 			System.out.println(" TF Products : " + TFprdtId);
								 		}
							 			else if((onlineinve.equals("true"))&&(stStatus.equals("failure"))&&(mp.equals("true")))
							 			{
								 			TFprdtId = productId.get(j);
								 			pdpTF = true;
								 			System.out.println(" TF Products : " + TFprdtId);
								 		}
							 	 	}
							 		if(pdpFT==false)
								 	{
							 			if((onlineinve.equals("false"))&&(stStatus.equals("success"))&&(strIventory.equals("y")))
								 		{
							 				FTprdtId = productId.get(j);
								 			pdpFT = true;
								 			System.out.println(" FT Products : " + FTprdtId);
								 		}
							 			
							 	 	}
							 		if(pdpFF==false)
								 	{
								 		if((onlineinve.equals("false")&&(stStatus.equals("failure"))))
								 		{
								 			FFprdtId = productId.get(j);
								 			pdpFF = true;
								 			System.out.println(" FF Products : " + FFprdtId);
								 		}
								 		else if((onlineinve.equals("false"))&&(stStatus.equals("success"))&&(strIventory.equals("n")))
								 		{
								 			FFprdtId = productId.get(j);
								 			pdpFF = true;
								 			System.out.println(" FF Products : " + FFprdtId);
								 		}
								 	}
							 	}
							}
				   	 		else
				   	 		{
				   	 			facets = false;
				   	 			continue;
				   	 		}
				   	 	}
			 	 	}
			 	 	else
			 	 	{
			 	 		break;
			 	 	}
				}
				catch(Exception e)
				{
					System.out.println(e.getMessage());
				}
			}
			else
			{
				System.out.println("Not in Browse page.");
				break;
			}
		}while((pdpTT!=true)||(pdpTF!=true)||(pdpFT!=true)||(pdpFF!=true));
			
		
		System.out.println(" TT Products : " + TTprdtId);
		System.out.println(" TF Products : " + TFprdtId);
		System.out.println(" FT Products : " + FTprdtId);
		System.out.println(" FF Products : " + FFprdtId);
	}
		
 


/*************************************************************** Facets And Sorting Option **************************************/
	@Test(priority = 12)
	
	public void BNIA10_Facets_Sorting() throws Exception
	  {
		   TempSignIn();
		   BNIA331A();
		   BNIA332A();
		   BNIA333A();
		   BNIA339A();
		   BNIA340A();
		   BNIA342A();
		   BNIA768A();	
		   //TemSignOut();
		   TempSessionTimeout();
		   
	 }

	 
	  
		/*BNIA331 - Verify that  FILTER option should be displayed in the left side of product list page*/
		public void BNIA331A()
		{
			ChildCreation("BNIA331 - Verify that  FILTER option should be displayed in the left side of product list page");
			try
		 	{
				if(FacetFlag331)
				{
					log.add("PLP facets is shown");
			   	 	pass("FILTER option is displayed in the left side of product list page",log);
				}
				else
				{
					log.add("PLP facets is not found");
			   	 	fail("FILTER option is not displayed in the left side of product list page",log);
				}
				/*int ctr=1;
				do
				{
					BNBasicCommonMethods.waitforElement(wait, obj, "browsetile");
			 	 	WebElement browse_tile1 = BNBasicCommonMethods.findElement(driver, obj, "browsetile");
			 	 	Thread.sleep(1500);
			 	 	browse_tile1.click();
			 	 	BNBasicCommonMethods.waitforElement(wait, obj, "browsepage");
			 	 	WebElement browsepg1 = BNBasicCommonMethods.findElement(driver, obj, "browsepage");
			 	 	wait.until(ExpectedConditions.visibilityOf(browsepg1));
			 	 	List<WebElement> browse_pg1 = driver.findElements(By.xpath("//*[contains(@class, 'hotSpotOverlay')]"));
			 	 	Random ran = new Random();
			   	 	int sel = ran.nextInt(browse_pg1.size());
			   	 	WebElement menu1 = driver.findElement(By.xpath("(//*[contains(@class, 'hotSpotOverlay')])["+sel+"]"));
			   	    JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("arguments[0].scrollIntoView(true);",menu1);
			   	 	menu1.click();
			   	 	try
			   	 	{
				   	 	Thread.sleep(2500);
				   	 	boolean facets1 = false;
				   	 	facets1 = BNBasicCommonMethods.findElement(driver, obj, "PLP_emptyfactes").isDisplayed();
					   	 if(facets1)
					   	 {
					   	 	log.add("PLP facets is shown");
					   	 	pass("FILTER option is displayed in the left side of product list page",log);
					   	 	ctr=3;
					   	 	Thread.sleep(2500);
					     }
					   	 else
					   	 {
					   	 	log.add("PLP facets is not found");
					   	 	fail("FILTER option is not displayed in the left side of product list page",log);
					   	 }
			   	 	}
			   	 	catch(Exception e)
			   	 	{
			   	 		String url = driver.getCurrentUrl();
				   	 	if(url.contains("pdp?"))
				   	 	{
				   	 		WebElement BNLogo1 = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
				   	 	    BNLogo1.click();
				   	 	    Thread.sleep(1000);
				   	 	}
			   	 	}
			   	}while(ctr<=2);*/
		 	}	
	         catch(Exception e)
			{
	        	 e.getMessage();
	 			System.out.println("Something went wrong"+e.getMessage());
	 			exception(e.getMessage());
			}
		}
		
		/*BNIA332 - Verify that  FILTER section should display the filter options Availability  Subjects  Formats  Ages  with Sort By menu option*/
		public void BNIA332A()
		{
			ChildCreation("BNIA332 - Verify that  FILTER section should display the filter options Availability  Subjects  Formats  Ages  with Sort By menu option");
			try
			{
				if(FacetFlag332)
				{
					pass("FILTER section displays the available filter options subjects  Formats  Ages  with Sort By menu option");
				}
				else
				{
					fail("FILTER section does not displays the available filter options subjects  Formats  Ages  with Sort By menu option");
				}
				
				/*Thread.sleep(1000);
				List<WebElement> FilterOptions = driver.findElements(By.xpath("//*[@class = 'skMob_filterContentWrapper']"));
				for(int i=1;i<=FilterOptions.size();i++)
				{
					String str = driver.findElement(By.xpath("(//*[@class='skMob_filterContentWrapper'])["+i+"]")).getText();
					System.out.println(""+str);
					if(driver.findElement(By.xpath("(//*[@class='skMob_filterContentWrapper'])["+i+"]")).isDisplayed())
					{
						log.add(""+str+" is shown");
						pass("FILTER section displays the available filter options subjects  Formats  Ages  with Sort By menu option",log);
					}
					else
					{
						fail("FILTER section does not displays the available filter options subjects  Formats  Ages  with Sort By menu option");
					}
				}*/
			}
			catch(Exception e)
			{
				e.getMessage();
		 		System.out.println("Something went wrong"+e.getMessage());
		 		exception(e.getMessage());
			}
			
		}
		
		/*BNIA333 - Verify that on selecting the + icon in filter section,the filter section should be expanded*/
		public void BNIA333A() throws Exception
		{
			try
			{
				ChildCreation("BNIA333 - Verify that on selecting the + icon in filter section,the filter section should be expanded");
		  		int sel = 0;
		  		//int ctr=1;
		  		try
		  			{
		  			
		  				if(FacetFlag333)
		  				{
		  					pass("on selecting the + icon in filter section,the filter section gets expanded");
		  				}
		  				else
		  				{
		  					fail("on selecting the + icon in filter section,the filter section does not gets expanded");
		  				}
		  				/*Random r = new Random();
		  				List<WebElement> plusicon = driver.findElements(By.xpath("//*[@class = 'skMob_filterContentsBtnIcon']"));
		  				sel = r.nextInt(plusicon.size());
		  				plusicon.get(sel).click();
		  				Thread.sleep(1500);
		  				boolean filter_open;
		  				WebElement filteropen = BNBasicCommonMethods.findElement(driver, obj, "plpfacets_open");
		  				filter_open = BNBasicCommonMethods.isElementPresent(filteropen);
		  				if(filter_open == true)
		  				{
		  					pass("on selecting the + icon in filter section,the filter section gets expanded");
		  				}
		  				else
		  				{
		  					fail("on selecting the + icon in filter section,the filter section does not gets expanded");
		  				}*/
		  			}
		  			catch(Exception e)
		  			{
		  				e.getMessage();
		  				System.out.println("Something went wrong in BNIA333\n"+e.getMessage());
		  				exception(e.getMessage());
		  			}
		  		
		  		/*BNIA337 - Verify that  when the + icon is tapped in the Filter section then it should be expanded and + icon should  changed to the - icon*/
		  		ChildCreation("BNIA337 - Verify that  when the + icon is tapped in the Filter section then it should be expanded and + icon should  changed to the - icon");
		  		try
		  		{
		  			if(FacetFlag337)
		  			{
		  				pass("When the + icon is tapped in the Filter section then it gets expanded and + icon is changed to the - icon");
		  			}
		  			else
		  			{
		  				fail("When the + icon is tapped in the Filter section then it does not gets expanded and + icon is not changed to the - icon");
		  			}
		  			
		  			
		  			/*WebElement ActivePlusIcon = driver.findElement(By.xpath("(//*[@class = 'skMob_filterContentContainer']//*[@class = 'skMob_filterItemContainer skMob_filterOpen'])"));
		  			wait.until(ExpectedConditions.attributeContains(ActivePlusIcon, "style", "block"));
		  			boolean PlusOk = ActivePlusIcon.getAttribute("style").contains("block");
		  			if(PlusOk==true)
		  			{
		  				pass("When the + icon is tapped in the Filter section then it gets expanded and + icon is changed to the - icon");
		  			}
		  			else
		  			{
		  				fail("When the + icon is tapped in the Filter section then it does not gets expanded and + icon is not changed to the - icon");
		  			}*/
		  		}
		  		catch(Exception e)
		  		{
		  			e.getMessage();
		  			System.out.println("Something went wrong in BNIA333\n"+e.getMessage());
		  			exception(e.getMessage());
		  		}
		  		
		  		/*BNIA334 - Verify that  Show More text link should  shown in the FILTER section*/
		  		ChildCreation("BNIA334 - Verify that  Show More text link should  shown in the FILTER section");
		  		   // boolean show_more = false;
		  		   // boolean activetab = false;
		  		   // ArrayList<Number> al1 = new ArrayList<>();
		  		   	try
		  				{
		  					if(FacetFlag334)
		  					{
		  						pass("Show More text link should is shown in the FILTER section");
	  						}
	  						else
	  						{
	  							fail("Show More text link is not shown in the FILTER section");
	  						}
		  					
		  					
		  						/*//WebElement showmore = BNBasicCommonMethods.findElement(driver, obj, "showmore_link");
		  						Thread.sleep(1000);
		  						WebElement showmore  = driver.findElement(By.xpath("(//*[@class = 'skMob_showMoreTxt'])["+(sel+1)+"]"));
		  						WebElement Activefacet = driver.findElement(By.xpath("(//*[contains(@class,'skMob_filterItemContainer')])["+(sel+1)+"]"));
		  						activetab = Activefacet.getAttribute("style").contains("display: block");
		  						if(activetab)
		  						{
		  						showmore.click();
		  						Thread.sleep(1500);
		  						al1.clear();
		  						show_more = BNBasicCommonMethods.isElementPresent(showmore);
		  						show_more=true;
		  						if(show_more)
		  						{
		  							//ctr=4;
		  							pass("Show More text link should is shown in the FILTER section");
		  						}
		  						else
		  						{
		  							fail("Show More text link is not shown in the FILTER section");
		  						}*/
		  						
		  				}
		  				catch(Exception e)
		  				{
		  					Random r = new Random();
		  					List<WebElement> plusicon = driver.findElements(By.xpath("//*[@class = 'skMob_filterContentsBtnIcon']"));
		  				    sel = r.nextInt(plusicon.size());
		  					ArrayList<Number> al = new ArrayList<>();
		  					while(al.contains(sel))
		  					{
		  						sel = r.nextInt(plusicon.size()-1);
		  					}
		  					al.add(sel);
		  					plusicon.get(sel).click();
		  					Thread.sleep(1500);
		  				}
		  				
		  			
		  				//BNIA335 - Verify that  on tapping  Show More text link in FILTER section,the filter section should be expanded
		  			ChildCreation("BNIA335 - Verify that  on tapping  Show More text link in FILTER section,the filter section should be expanded");
		  			//boolean show_lessbool=false;
		  			//if(show_more == true)	
		  			//{
		  				try
		  				{
		  					
		  					if(FacetFlag335)
		  					{
		  						pass("On tapping Show More text link in FILTER section,the filter section gets expanded");
		  					}
		  					else
		  					{
		  						fail("On tapping Show More text link in FILTER section,the filter section does not gets expanded");
		  					}
		  					
		  					/*WebElement showmore  = driver.findElement(By.xpath("(//*[@class = 'skMob_showMoreTxt'])["+(sel+1)+"]"));
		  					showmore.click();
		  					Thread.sleep(1500);
		  					WebElement showless = BNBasicCommonMethods.findElement(driver, obj, "showless_link");
		  					boolean showles = BNBasicCommonMethods.isElementPresent(showless);
		  					if(showles)
		  					{
		  						show_lessbool=true;
		  						System.out.println(show_lessbool);
		  						pass("On tapping Show More text link in FILTER section,the filter section gets expanded");
		  					}
		  					else
		  					{
		  						System.out.println(show_lessbool);
		  						fail("On tapping Show More text link in FILTER section,the filter section does not gets expanded");
		  					}*/
		  				}
		  				catch(Exception e)
		  				{
		  					e.getMessage();
		  					System.out.println("Something went wrong"+e.getMessage());
		  					exception(e.getMessage());
		  				}
		  			
		  			
		  			
		  			
		  			/*"BNIA-336 Verify that on tapping Show Less text link in FILTER section it should be contracted"*/
		  			ChildCreation("BNIA-336 Verify that on tapping Show Less text link in FILTER section it should be contracted");
		  			
		  					try
		  					{
		  						
		  						if(FacetFlag336)
		  						{
		  							pass("On tapping Show Less Link in FILTER section,the filter section gets closed ");
		  						}
		  						else
		  						{
		  							fail("On tapping Show Less text link in FILTER section,the filter section does not gets Closed");
		  						}
		  						
		  						/*//WebElement showless = BNBasicCommonMethods.findElement(driver, obj, "showless_link");
		  						Thread.sleep(1500);
		  						WebElement showless =  driver.findElement(By.xpath("(//*[@class = 'skMob_showMoreFacets'])["+(sel+1)+"]"));
		  						boolean showles = BNBasicCommonMethods.isElementPresent(showless);
		  						Actions act = new Actions(driver);
		  						for(int ctr=0 ; ctr<20 ;ctr++)
		  						{
		  							act.sendKeys(Keys.CONTROL).sendKeys(Keys.ARROW_DOWN).build().perform();
		  						}
		  						Thread.sleep(1000);
		  						if(showles)
		  						{
		  							showless.click();
		  							Thread.sleep(1500);
		  							pass("On tapping Show Less Link in FILTER section,the filter section gets closed ");
		  						}
		  						else
		  						{
		  							fail("On tapping Show Less text link in FILTER section,the filter section does not gets Closed");
		  						}*/
		  					}
		  					catch(Exception e)
		  					{
		  						e.getMessage();
		  						System.out.println("Something went wrong"+e.getMessage());
		  						exception(e.getMessage());
		  					}
		  				
		  			
		  			/*BNIA338 - Verify that  when the - icon is tapped in the Filter section then it should be contracted and - icon should be changed to + icon*/
		  			ChildCreation("BNIA338 - Verify that  when the - icon is tapped in the Filter section then it should be contracted and - icon should be changed to + icon");
		  			try
		  			{
		  				
		  				if(FacetFlag338)
		  				{
		  					pass("When the - icon is tapped in the Filter section then it gets contracted and - icon is changed to + icon");
	  	  				}
	  	  				else
	  	  				{
	  	  					fail("When the - icon is tapped in the Filter section then it does not gets contracted and - icon is not changed to + icon");
	  	  				}
		  				/*Thread.sleep(1500);
		  				List<WebElement> Facets_Open = driver.findElements(By.xpath("//*[@class = 'skMob_filterContentWrapper skMob_filterOpen']//*[@class = 'skMob_filterContentsBtnIcon']"));
				 	 	Random ran = new Random();
				   	 	int sele = ran.nextInt(Facets_Open.size());
				   	 	if(sele<1)
				   	 	{
				   	 		sele=sele+1;
				   	 	}
				   	 	WebElement Facets = driver.findElement(By.xpath("(//*[@class = 'skMob_filterContentWrapper skMob_filterOpen']//*[@class = 'skMob_filterContentsBtnIcon'])["+sele+"]"));
				   	 	Facets.click();
		  				WebElement ActivePlusIcon = driver.findElement(By.xpath("(//*[@class = 'skMob_filterContentWrapper skMob_filterOpen']//*[@class = 'skMob_filterContentsBtnIcon'])["+sele+"]"));
		  				ActivePlusIcon.click();
		  				Thread.sleep(2000);
		  				try
		  				{
		  					WebElement InActivePlus = driver.findElement(By.xpath("(//*[@class = 'skMob_filterContentContainer']//*[@class = 'skMob_filterItemContainer'])["+sele+"]"));
		  	  				//wait.until(ExpectedConditions.attributeContains(InActivePlus, "style", "none"));
		  	  				boolean InactivePlusOk = InActivePlus.getAttribute("style").contains("none");
		  	  				if(InactivePlusOk==true)
		  	  				{
		  	  					pass("When the - icon is tapped in the Filter section then it gets contracted and - icon is changed to + icon");
		  	  				}
		  	  				else
		  	  				{
		  	  					fail("When the - icon is tapped in the Filter section then it does not gets contracted and - icon is not changed to + icon");
		  	  				}*/
		  				}
		  				catch(Exception e1)
		  				{
		  					Skip("There is no - icon to tap in the filter section");
		  				}
		  				
		  			}
		  			catch(Exception e1)
		  			{
		  				e1.getMessage();
		  				System.out.println("Something went wrong"+e1.getMessage());
		  				exception(e1.getMessage());
		  			}
			
		}
		
		/*BNIA339 - Verify that  in the FILTER section, the selected filtered items should be shown in the top of the FILTER section with the cross symbol*/
		public void BNIA339A() throws Exception
		{
			ChildCreation("BNIA339 - Verify that  in the FILTER section, the selected filtered items should be shown in the top of the FILTER section with the cross symbol");
			try
			{
				if(FacetFlag339)
				{
					pass("In the FILTER section, the selected filtered items is shown in the top of the FILTER section with the cross symbol",log);
				}
				else
				{
					fail("In the FILTER section, the selected filtered items is not shown in the top of the FILTER section with the cross symbol");
				}
				
				/*List<WebElement> SelectedFacets = driver.findElements(By.xpath("//*[@class = 'skMob_plpSelFacetCont']"));
				//WebElement fa = BNBasicCommonMethods.findElement(driver, obj, "PLP_FilterContent");
				WebElement fa  = SelectedFacets.get(0); 
				for(int i=1;i<=SelectedFacets.size();i++)
				{
					String str = driver.findElement(By.xpath("(//*[@class = 'skMob_plpSelFacetCont'])["+i+"]")).getText();
					System.out.println(" "+str);
					if(driver.findElement(By.xpath("(//*[@class='skMob_plpSelFacetCont'])["+i+"]")).isDisplayed())
					{
						log.add(""+str +"Facet is shown");
						pass("In the FILTER section, the selected filtered items is shown in the top of the FILTER section with the cross symbol",log);
					}
					else
					{
						fail("In the FILTER section, the selected filtered items is not shown in the top of the FILTER section with the cross symbol");
					}
				}*/
			}
			catch(Exception e)
			{
				e.getMessage();
  				System.out.println("Something went wrong"+e.getMessage());
  				exception(e.getMessage());
			}
		}
		
		/*BNIA340 - Verify that cross symbol should be shown  in the left side of the selected filtered items*/
		public void BNIA340A()
		{
			ChildCreation("BNIA340 - Verify that cross symbol should be shown  in the left side of the selected filtered items");
			try
			{
				if(FacetFlag440)
				{
					pass("The X mark is displayed.",log);
				}
				else
				{
					fail("The Facet was selected but there is mismatch in the symbol selected near the facet.",log);
				}
				
				/*String val = "return window.getComputedStyle(document.querySelector('.skMob_plpSelFacet'),':before').getPropertyValue('content')";
				JavascriptExecutor js = (JavascriptExecutor)driver;
				String content = (String) js.executeScript(val);
				log.add("The Facet was Selected and the displayed value near the facet is : " +content);
				if(content.contains("X"))
				{
					pass("The X mark is displayed.",log);
				}
				else
				{
					fail("The Facet was selected but there is mismatch in the symbol selected near the facet.",log);
				}*/
			}
			catch(Exception e)
			{
				e.getMessage();
  				System.out.println("Something went wrong"+e.getMessage());
  				exception(e.getMessage());
			}
			
		}
		
		/*BNIA 342 - Verify that  on tapping cross symbol in filtered items the product list page should be updated*/
		/*public void BNIA342A()
		{
 			ChildCreation("BNIA342 - Verify that  on tapping cross symbol in filtered items the product list page should be updated");
			try
			{
				boolean selectedfacet= false;
				Random r = new Random();
				List<WebElement> SelectedFacets = driver.findElements(By.xpath("//*[@class = 'skMob_plpSelFacetCont']"));
				int sel = r.nextInt(SelectedFacets.size());
				SelectedFacets.get(sel).click();
				Thread.sleep(5000);
				selectedfacet = driver.findElement(By.xpath("(//*[@class = 'skMob_plpSelFacetCont'])["+(sel)+"]")).isDisplayed(); 
				if(selectedfacet)
				{
					fail("On tapping cross symbol in filtered items the product list page does not gets updated");
				}
			}
			catch(Exception e)
			{
				log.add("The Selected Factes is removed");
				pass("On tapping cross symbol in filtered items the product list page gets updated",log);
			}
		}*/
		
		public void BNIA342A()
		{
 			ChildCreation("BNIA342 - Verify that  on tapping cross symbol in filtered items the product list page should be updated");
			try
			{
				if(FacetFlag342)
				{
					pass("On tapping cross symbol in filtered items the product list page gets updated",log);
				}
				else
				{
					
					fail("On tapping cross symbol in filtered items the product list page does not gets updated");
				
				}
				
				/*//boolean selectedfacet= false;
				Random r = new Random();
				List<WebElement> SelectedFacets = driver.findElements(By.xpath("//*[@class = 'skMob_plpSelFacetCont']"));
				int sel = r.nextInt(SelectedFacets.size());
				if(sel<1)
				{
					sel=1;
				}
				WebElement Selfacet = driver.findElement(By.xpath("(//*[@class = 'skMob_plpSelFacetCont'])["+sel+"]"));
				String fac = Selfacet.getText();
				Selfacet.click();
				//SelectedFacets.get(sel).click();
				Thread.sleep(5000);
				WebElement Selfacet1 = driver.findElement(By.xpath("(//*[@class = 'skMob_plpSelFacetCont'])["+sel+"]"));
				String str = Selfacet1.getText();
				//selectedfacet = driver.findElement(By.xpath("(//*[@class = 'skMob_plpSelFacetCont'])["+(sel)+"]")).isDisplayed();
				
				
				if(str.equals(fac))
				{
					fail("On tapping cross symbol in filtered items the product list page does not gets updated");
				}
				else
				{
					log.add("The Selected Factes is removed :" +fac);
					pass("On tapping cross symbol in filtered items the product list page gets updated",log);
				}*/
			}
			catch(Exception e)
			{
				Skip("There is no Selected Facets for the selected PLP");
			}
		}
		
		
		
		
		/*BNIA768 - Verify that While selecting more Faceted sorts(reach product count 1),page navigates to respective PDP Page */
		public void BNIA768A() throws Exception
		{
			ChildCreation("BNIA768 - Verify that While selecting more Faceted sorts(reach product count 1),page navigates to respective PDP Page ");
			try
			{
				Thread.sleep(3000);
				BNBasicCommonMethods.waitforElement(wait, obj, "logoicon");
				WebElement logo = BNBasicCommonMethods.findElement(driver, obj, "logoicon");
				logo.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "BrowseTile");
				WebElement Browsetile = BNBasicCommonMethods.findElement(driver, obj, "BrowseTile");
				Browsetile.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "BrowsePage_FindingDoryLink");
				WebElement Browse_FindingDory = BNBasicCommonMethods.findElement(driver, obj, "BrowsePage_FindingDoryLink");
				Browse_FindingDory.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "PLP_FacetsContainer");
				try
				{
					WebElement facets = driver.findElement(By.xpath("(//*[@class = 'skMob_filterContentWrapper'])[2]"));
					facets.click();
					Thread.sleep(500);
					try
					{
						WebElement FacetSelected = driver.findElement(By.xpath("//*[@class = 'skMob_filterItemContainer skMob_filterOpen']//*[@class='skMob_filterItemContent' and contains(text(), 'Animation')]"));
						FacetSelected.click();
						Thread.sleep(2000);
						WebElement Facets = driver.findElement(By.xpath("(//*[@class = 'skMob_filterContentWrapper'])[2]"));
						Facets.click();
						Thread.sleep(500);
						ArrayList<String> productId = new ArrayList<String>();
						List<WebElement> productListContainer = driver.findElements(By.xpath("//*[@class='skMob_productListItemOuterCont']"));
						for(int j = 0;j<productListContainer.size();j++)
					 	{
					 		productId.add(productListContainer.get(j).getAttribute("prodid"));
					 	} 
						WebElement facetselected = driver.findElement(By.xpath("//*[@class = 'skMob_filterItemContainer skMob_filterOpen']//*[@class='skMob_filterItemContent' and contains(text(), 'Animation - Features')]"));
						facetselected.click();
						Thread.sleep(3500);
						try
						{
							BNBasicCommonMethods.waitforElement(wait, obj, "PDPContainer");
							String pdp = driver.getCurrentUrl();
							String [] newstr = pdp.split("Param=");
							String EANStr =  newstr[1];
							if(pdp.contains("pdp?navParam="))
							{
								if(productId.contains(EANStr))
								{
								pass("While selecting more Faceted sorts(reach product count 1),page navigates to respective PDP Page");
								}
								else
								{
									System.out.println(EANStr);
									fail("While selecting more Faceted sorts(reach product count 1),page not navigates to respective PDP Page");
								}
							}
							else
							{
								fail("Page does not navigates to pdp");
							}
						}
						catch(Exception e3)
						{
							Skip("On selecting facets, it does not navigates to PDP page");
						}
					}
					catch(Exception e2)
					{
						Skip("Animation facet is not shown");
					}
				}
				catch(Exception e1)
				{
					Skip("Subjects facets is not shown");
				}
				System.out.println("11/11 - BNIA10_Facets_Sorting is completed");
			}
			catch(Exception e)
			{
				e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
			}
			BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
			WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
			BNLogo.click();
			Thread.sleep(1000);
		}

/*************************************************Search page******************************************************************/

//Initial Launch of the Page and Clearing the Sign In fields.7
   @Test(priority=6)
    public void BNIA11_SearchPage() throws Exception
    {
  	   TempSignIn();
  	   BNIA450();
  	   BNIA451();
  	   BNIA452();
  	   BNIA453();
  	   BNIA500();
  	   BNIA501();
  	   BNIA456();
  	   BNIA459();
  	   BNIA457();
  	   BNIA462();
  	   BNIA467();
  	   BNIA468();
  	   BNIA568();
	   BNIA472();
  	   BNIA458();
  	   BNIA463();
  	   BNIA465();
  	   BNIA475();
	   BNIA570();
	   BNIA600();
  	   //TemSignOut();
  	   TempSessionTimeout();
  	   /*BNIA461();*/
	  
  }
    
    /*BNIA450 - Verify that while selecting the Search icon at the header or in the home page,the search page should be displayed*/
    public void BNIA450() throws Exception 
    {
   	ChildCreation("BNIA450 - Verify that while selecting the Search icon at the header or in the home page,the search page should be displayed ");
   	try
   	{
   		BNBasicCommonMethods.waitforElement(wait, obj, "Searchtile");
   	 	WebElement search = BNBasicCommonMethods.findElement(driver, obj, "Searchtile");
   	 	search.click();
   	 	BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
   	 	WebElement searchpage = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
   	 	boolean search1 = BNBasicCommonMethods.isElementPresent(searchpage);
   	 	if(search1)
   	 	{
   	 		pass(" while selecting the Search icon at the header or in the home page,the search page is displayed ");
   	 	}
   	 	else
   	 	{
   	 		fail(" while selecting the Search icon at the header or in the home page,the search page is not displayed ");
   	 	}
   	}
   	catch(Exception e)
  	{
      	 e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  	}
    }	
    
    /*BNIA451 - Verify that search page should consists of Search field and Scan Items option*/
    public void BNIA451() throws Exception 
    {
   	ChildCreation("BNIA451 - Verify that search page should consists of Search field and Scan Items option  ");
   	try
   	{
   		/*BNBasicCommonMethods.waitforElement(wait, obj, "Searchtile");
   	 	WebElement search = BNBasicCommonMethods.findElement(driver, obj, "Searchtile");
   	 	search.click();*/
   		
   	 	BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
   	 	WebElement searchpage = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
   	 	boolean search2 = BNBasicCommonMethods.isElementPresent(searchpage);
   	 	Thread.sleep(1000);
   	 	BNBasicCommonMethods.waitforElement(wait, obj, "scanbutton");
   	 	WebElement scanbtn = BNBasicCommonMethods.findElement(driver, obj, "scanbutton");
   	 	boolean scanbtton = BNBasicCommonMethods.isElementPresent(scanbtn);
   	 	if(search2 && scanbtton)
   	 	{
   	 		pass(" search page should consists of Search field and Scan Items option ");
   	 	}
   	 	else
   	 	{
   	 		fail(" search page does not consists of Search field and Scan Items option ");
   	 	}
   	 	
   	 	
   	}
   	catch(Exception e)
  	{
      	 e.getMessage();
  			System.out.println("Something went wrong"+e.getMessage());
  			exception(e.getMessage());
  	}
    }	
    
    /*BNIA452 - Verify that  while selecting the Scan Items option in the search page,the scan page should be displayed*/
    public void BNIA452() throws Exception 
    {
   	ChildCreation("BNIA452 - Verify that  while selecting the Scan Items option in the search page,the scan page should be displayed");
   	try
   	{
   		/*BNBasicCommonMethods.waitforElement(wait, obj, "Searchtile");
   	WebElement search = BNBasicCommonMethods.findElement(driver, obj, "Searchtile");
   	search.click();*/
   	BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
  	 	WebElement searchpage = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
  	 	boolean search2 = BNBasicCommonMethods.isElementPresent(searchpage);
  	 	WebElement scanbtn = BNBasicCommonMethods.findElement(driver, obj, "scanbutton");
  	 	boolean scanbutton = BNBasicCommonMethods.isElementPresent(scanbtn);
  	 	if(search2 && scanbutton)
  	 	{
  	 		pass(" search page should consists of Search field and Scan Items option ");
  	 	}
  	 	else
  	 	{
  	 		fail(" search page does not consists of Search field and Scan Items option ");
  	 	}
   	}
  	 	catch(Exception e)
  		{
  	    	 e.getMessage();
  				System.out.println("Something went wrong"+e.getMessage());
  				exception(e.getMessage());
  		}
   	}
    
    /*BNIA-453 Verify that search field should consists of "Search" help text and "All" drop down option*/
    public void BNIA453() throws Exception 
    {
   	ChildCreation("BNIA-453 Verify that search field should consists of Search help text should be displayed");
   	try
   	{
   		BNBasicCommonMethods.waitforElement(wait, obj, "SearchInlineText");
   		WebElement SearchInlineText = BNBasicCommonMethods.findElement(driver, obj, "SearchInlineText");
   		if(SearchInlineText.getAttribute("placeholder").equals("Search"))
   		{
   			log.add("Inline text is"+SearchInlineText.getAttribute("placeholder").toString());
   			pass("Search Inline text is displayed");
   		}
   		else
   		{
   			fail("Search Inline Text is not dsiplayed");
   		}
   		
   	}
  	 	catch(Exception e)
  		{
  	    	 e.getMessage();
  				System.out.println("Something went wrong"+e.getMessage());
  				exception(e.getMessage());
  		}
   	}
    
    /*BNIA-500 Verify that search icon should be displayed at the header as per the creative & while selecting the Search icon at the header,the search field should be enabled.*/
    public void BNIA500() throws InterruptedException
  	{
  		try
  		{
  			ChildCreation("BNIA-500 Verify that search icon should be displayed at the header as per the creative & while selecting the Search icon at the header,the search field should be enabled.");
  	  		BNBasicCommonMethods.waitforElement(wait, obj, "SearchIconHeader");
  	  		WebElement SearchIconHeader = BNBasicCommonMethods.findElement(driver, obj, "SearchIconHeader");
  	  		if(BNBasicCommonMethods.isElementPresent(SearchIconHeader))
  	  		{
  	  			pass("Search Icon is Displayed in the header");
  		  		SearchIconHeader.click();
  		  		Thread.sleep(1500);
  		  		WebElement SearchPage = BNBasicCommonMethods.findElement(driver, obj, "SearchPage");
  		  		if(BNBasicCommonMethods.isElementPresent(SearchPage))
  		  		{
  		  			log.add("Navigated to Search Page");
  		  			pass("Search page is displayed successfully",log);
  		  		}
  		  		else
  		  		{
  		  			log.add("Page is Not navigate to Search page");
  		  			fail("Search Page is not displayed successfully",log);
  		  		}
  	  		}
  	  		else
  	  		{
  	  			fail("Search Icon is not displayed in the header");
  	  		}
  		}
  		catch(Exception e)
  		{
  			exception("There is somwthing went wrong , please verify"+e.getMessage());
  		}
  	}
  	
  	/*BNIA-501 Verify that user should be able to enter the search keywords in the search field text box*/
  	public void BNIA501() throws Exception
  	{
  		ChildCreation("BNIA-501 Verify that user should be able to enter the search keywords in the search field text box");
  		try
  		{
  			sheet = BNBasicCommonMethods.excelsetUp("Search");
  	  		String SreachKwd1 = BNBasicCommonMethods.getExcelVal("BNIA501", sheet, 2);
  		//	String SreachKwd1 = "harry potter";
  	  	    BNBasicCommonMethods.waitforElement(wait, obj, "searchboxwithval");
  			WebElement searchboxwithval = BNBasicCommonMethods.findElement(driver, obj, "searchboxwithval");
  	  		if(searchboxwithval.isDisplayed())
  	  		{
  	  			pass("Search Box is found");
  	  		    BNBasicCommonMethods.waitforElement(wait, obj, "searchboxwithval");
  	  		    WebElement searchboxwithval4 = BNBasicCommonMethods.findElement(driver, obj, "searchboxwithval");
  	  		    Thread.sleep(1500);
  	  			searchboxwithval4.click();
  	  		    Thread.sleep(1500);
  	  		    BNBasicCommonMethods.waitforElement(wait, obj, "SearchTxtBxnew");
  	  			WebElement SearchTxtBxnew2 = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");
  	  		    SearchTxtBxnew2.clear();
  	  		    WebElement searchboxwithval1 = BNBasicCommonMethods.findElement(driver, obj, "searchboxwithval");
  	  			searchboxwithval1.clear();
  	  			Actions action = new Actions(driver);
  	  			action.moveToElement(searchboxwithval).sendKeys(SreachKwd1).build().perform();
  	  			Thread.sleep(1000);
  	  			BNBasicCommonMethods.waitforElement(wait, obj, "searchSuggestioncontainer");
  	  			 WebElement searchSuggestioncontainer = BNBasicCommonMethods.findElement(driver, obj, "searchSuggestioncontainer");
  	    		if(searchSuggestioncontainer.isDisplayed())
  	    		{
  	  				log.add("Entered Search Keyword "+SreachKwd1);
  	  				pass("User able to enter searchKeyword in search Textbox");
  	  			}
  	  			else
  	  			{
  	  				log.add("Entered Search Keyword"+SreachKwd1);
  	  				pass("User not able to enter searchKeyword in search Textbox");
  	  			}
  	  			
  	  		}
  	  		else
  	  		{
  	  			fail("There is no Search Box is found");
  	  		}	
  		}
  		catch(Exception e)
  		{
  			System.out.println(e.getMessage());
  			exception(" There is somwthing went wrong , please verify"+e.getMessage());
  		}
  		
  		
  	}
  	
  	/*BNIA-456 Verify that while entering the search keywords in the search field,the search suggestions should be displayed*/
  	public void BNIA456()
  	{
  		ChildCreation("BNIA-456 - Verify that while entering the search keywords in the search field,the search suggestions should be displayed.");
  		try
  		{
  		   BNBasicCommonMethods.waitforElement(wait, obj, "searchSuggestioncontainer");
  		   WebElement searchSuggestioncontainer = BNBasicCommonMethods.findElement(driver, obj, "searchSuggestioncontainer");
  		   if(searchSuggestioncontainer.isDisplayed())
  		   {
  			  pass("Search suggestions are displayed"); 
  			  Thread.sleep(2000);
  			  List <WebElement> SearchSugglist = driver.findElements(By.xpath("//*[@class='skMobsuggestedItem_container']//*[@class='suggestedItem ']"));
  			  log.add("Search Suggestion count "+SearchSugglist.size());
  			Thread.sleep(2000);
  			  for(int ctr=1 ; ctr < SearchSugglist.size()+1 ; ctr++)
  			  {
  				Thread.sleep(500);
  				  String Suggesting = driver.findElement(By.xpath("(//*[@class='skMobsuggestedItem_container']//*[@class='suggestedItem '])["+ctr+"]")).getText();
  				  log.add("Suggestion"+(ctr) +" : "+Suggesting);
  				 // System.out.println(Suggesting);
  				  
  			  }
  			  pass("Search Suggestion are displayed and are listed below",log);
  		   }
  		   else
  		   {
  			  fail("Search suggestions are not displayed");
  		   }
  		}
  		
  		catch(Exception e)
  		{
  	       System.out.println(e.getMessage());
  			exception("There is somwthing went wrong , please verify"+e.getMessage());
  		}
  		
  	}
  	
  	/*BNIA-459 Verify that while entering the search keywords in the search field,the entered characters should be highlighted in inline popup*/
  	public void BNIA459()
  	{
  		ChildCreation("BNIA-459 -Verify that while entering the search keywords in the search field,the entered characters should be highlighted in inline popup.");
  		try
  		{
  		   BNBasicCommonMethods.waitforElement(wait, obj, "searchsuggbold");
  		   WebElement searchsuggbold = BNBasicCommonMethods.findElement(driver, obj, "searchsuggbold");
  		   
  		   if(searchsuggbold.isDisplayed())
  		   {
  			  pass("while entering the search keywords in the search field,the entered characters are highlighted in inline popup.");
  		   }
  		   else
  		   {
  			  fail("while entering the search keywords in the search field,the entered characters are not highlighted in inline popup.");
  		   }
  		}
  		
  		catch(Exception e)
  		{
  	       System.out.println(e.getMessage());
  			exception("There is somwthing went wrong , please verify"+e.getMessage());
  		}
  		
  	}
  	
  	/*BNIA-457 Verify that while entering the keywords in the search field,the "X" cancel option should be enabled*/
  	public void BNIA457()
  	{
  		ChildCreation("BNIA-457 - Verify that while entering the keywords in the search field,the X cancel option should be enabled.");
  		try
  		{
  		   BNBasicCommonMethods.waitforElement(wait, obj, "SearchClearIcon");
  		   WebElement SearchClearIcon1 = BNBasicCommonMethods.findElement(driver, obj, "SearchClearIcon");
  		   
  		   if(SearchClearIcon1.isDisplayed())
  		   {
  			  pass("While entering the keywords in the search field,the X cancel option is enabled.");
  		   }
  		   else
  		   {
  			  fail("While entering the keywords in the search field,the X cancel option is not enabled.");
  		   }
  		}
  		
  		catch(Exception e)
  		{
  	       System.out.println(e.getMessage());
  			exception("There is somwthing went wrong , please verify"+e.getMessage());
  		}
  		
  	}
  	
  	/*BNIA462 -Verify that while selecting the search suggestions, search results page should be displayed*/
  	public void BNIA462() throws Exception
  	{
  		ChildCreation("BNIA462 -Verify that while selecting the search suggestions, search results page should be displayed.");
  		try
  		{
  			Thread.sleep(2000);
  		    BNBasicCommonMethods.waitforElement(wait, obj, "Searchsuggdiv");
  	 	 	WebElement searchsuggdivv = BNBasicCommonMethods.findElement(driver, obj, "Searchsuggdiv");
  	 	 	searchsuggdivv.click();
  	 	 	BNBasicCommonMethods.waitforElement(wait, obj, "Harryplp");
  	 	 	WebElement harryplp = BNBasicCommonMethods.findElement(driver, obj, "Harryplp");
  	 	 	if(harryplp.isDisplayed())
      		{
    				pass("While selecting the search suggestions, search results page is displayed");
    			}
    			else
    			{
    				pass("While selecting the search suggestions, search results page is not displayed");
    			}
  		}
  		
  		catch(Exception e)
  		{
  			try
  			{
  				WebElement SearchKeywordAlert = BNBasicCommonMethods.findElement(driver, obj, "SearchKeywordNoProductFoundAlert");
  				if(SearchKeywordAlert.isDisplayed())
  				{
  					fail("Sorry, no results could be found. alert is shown");
  				}
  				else
  				{
  					return;
  				}
  			}
  			catch(Exception e1)
  			{
  				WebElement SearchEAN = BNBasicCommonMethods.findElement(driver, obj, "SearchEANNoProductFoundAlert");
  				if(SearchEAN.isDisplayed())
  				{
  					fail("The entered EAN is not working, No Product Found Alert is shown");
  				}
  				else
  				{
  					return;
  				}
  			}
  		}
  	}
  	
  	/*BNIA-467 Verify that while selecting the back button after entering search keywords in the search field,the search field value should be closed and navigate to previous page*/
  	public void BNIA467()
  	{
  		ChildCreation("BNIA-467 Verify that while selecting the back button from search results page, should navigate to previous page");
	  		try
	  		{
	  			Thread.sleep(2000);
	  			BNBasicCommonMethods.waitforElement(wait, obj, "PLPbackbtn");
	  			WebElement PLPbckbtn = BNBasicCommonMethods.findElement(driver, obj, "PLPbackbtn");
	  			PLPbckbtn.click();
	  			BNBasicCommonMethods.waitforElement(wait, obj, "searchbox1");
	  			WebElement searchbox1 = BNBasicCommonMethods.findElement(driver, obj, "searchbox1");
	  			if(searchbox1.isDisplayed())
	      		{
	    				pass("While selecting the back button from search results page, it navigate to previous page");
	    			}
	    			else
	    			{
	    				pass("While selecting the back button from search results page, it does not navigate to previous page");
	    			}
	  		}
	  	    catch(Exception e)
	  		{
	  		     System.out.println(e.getMessage());
	  		     exception("There is somwthing went wrong , please verify"+e.getMessage());
	  		}
  			
  	 }
    
  	/*BNIA-468 Search Validation*/
  	public void BNIA468()
  	{
  		ChildCreation("BNIA-468 Search Validation");
  		try
  		{
  			Thread.sleep(2000);
  			sheet = BNBasicCommonMethods.excelsetUp("Search");
  			String SearchKeywordList = BNBasicCommonMethods.getExcelVal("BNIA468", sheet,2);
  			String[] SearchKeyword = SearchKeywordList.split("\n");
  			BNBasicCommonMethods.waitforElement(wait, obj, "SearchTxtBxnew");
  			WebElement SearchTxtBxnew = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");
  			for(int ctr = 0; ctr < SearchKeyword.length ; ctr++)
  			{
  				
  				SearchTxtBxnew.click();
  				SearchTxtBxnew.clear();
  				Thread.sleep(500);
  				SearchTxtBxnew.sendKeys(SearchKeyword[ctr]);
  				Thread.sleep(2000);
  				try
  				{
  					WebElement InVaildSearchSuggestion = BNBasicCommonMethods.findElement(driver, obj, "InVaildSearchSuggestion");
  					if(InVaildSearchSuggestion.getText().contains("No suggested search for"))
  					{
  						pass("Inline Error Message is found"+InVaildSearchSuggestion.getText());
  					}
  					else
  					{
  						fail("Inline Error Message is not found"+InVaildSearchSuggestion.getText());
  					}
  				}
  				catch(Exception e)
  		  		{
  		           try
  		           {
  		        	 
	  		  		     WebElement searchSuggestioncontainer = BNBasicCommonMethods.findElement(driver, obj, "searchSuggestioncontainer");
	  		  		     if(searchSuggestioncontainer.isDisplayed())
	  		  		     {
	  		  			    pass("Search suggestions are displayed"); 
	  		  			    Thread.sleep(500);
	  		  			    List <WebElement> SearchSugglist = driver.findElements(By.xpath("//*[@class='skMobsuggestedItem_container']//*[@class='suggestedItem ']"));
	  		  			    log.add("Search Suggestion count "+SearchSugglist.size());
	  		  			    for(int ctr1=1 ; ctr1 < SearchSugglist.size()+1 ; ctr1++)
	  		  			    {
	  		  				  String Suggesting = driver.findElement(By.xpath("(//*[@class='skMobsuggestedItem_container']//*[@class='suggestedItem '])["+ctr1+"]")).getText();
	  		  				  log.add("Suggestion"+(ctr1) +" : "+Suggesting);
	  		  				 // System.out.println(Suggesting);
	  		  				  
	  		  			    }
	  		  			  		pass("Search Suuggestion are displayed and are listed below",log);
	  		  		     }
	  		  		     else
	  		  		     {
	  		  		    	     fail("Search suggestions are not displayed");
	  		  		     }  
  		           }
  		           
  		           catch(Exception e1)
  	  		  	   {
  		        	      pass("No Search Result found for the search keyword"+SearchKeyword[ctr]);
  	  		  	   }
  		  		}
  			}
  		}
  		catch(Exception e)
  		{
           exception("Something went wrong, so please verify"+e.getMessage());  			
  		}
  	}
  	
  	/*BNIA-568 Verify that on selecting the "Search" icon at the header in the search page. the search page should not suppose to load again.*/
  	public void BNIA568()
  	{
  		ChildCreation("BNIA-568 Verify that on selecting the Search icon at the header in the search page. the search page should not suppose to load again.");
  		try
  		{
  			Thread.sleep(2000);
  			BNBasicCommonMethods.waitforElement(wait, obj, "SearchIcon");
  			WebElement SearchIcon = BNBasicCommonMethods.findElement(driver, obj, "SearchIcon");
  			if(SearchIcon.isDisplayed())
  			{
  				SearchIcon.click();
  				WebElement searchSuggestioncontainer = BNBasicCommonMethods.findElement(driver, obj, "searchSuggestioncontainer");
  				if(searchSuggestioncontainer.isDisplayed())
  				{
  					pass("Search page is not loaded");
  				}
  				
  				else
  				{
  					fail("Search Page is loaded on tapping search icon form search page");
  				}
  			}
  			else
  			{
  				fail("Search icon is not displayed in search page");
  			}
  		}
  		
  		catch(Exception e)
		{
		     System.out.println(e.getMessage());
			 exception("There is somwthing went wrong , please verify"+e.getMessage());
		}
  	}
  	
  	/*BNIA-472 Verify that while entering the invalid search keywords in the search field after selecting the categories in the dropdown,the "No suggested search for XXXX" alert message should be displayed*/
	public void BNIA472() throws Exception
	{
		ChildCreation("BNIA-472 Verify that while entering the invalid search keywords in the search field after selecting the categories in the dropdown,the No suggested search for : XXXX alert message should be displayed");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Search");
  			String SearchKeyword = BNBasicCommonMethods.getExcelVal("BNIA472", sheet,2);
  			Thread.sleep(3000);
  			BNBasicCommonMethods.waitforElement(wait, obj, "SearchTxtBxnew");
  			WebElement SearchTxtBxnew = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");
  			Thread.sleep(3000);
  			SearchTxtBxnew.click();
  			Thread.sleep(3000);
  			WebElement SearchTxtBxnew2 = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");
  			SearchTxtBxnew2.clear();
  			Thread.sleep(3000);
  			WebElement SearchTxtBxnew1 = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");
  			SearchTxtBxnew1.sendKeys(SearchKeyword);
  			Thread.sleep(2000);
  			
  					WebElement InVaildSearchSuggestion = BNBasicCommonMethods.findElement(driver, obj, "InVaildSearchSuggestion");
  					if(InVaildSearchSuggestion.getText().contains("No suggested search for"))
  					{
  						pass("Inline Error Message is found"+InVaildSearchSuggestion.getText());
  					}
  					else
  					{
  						fail("Inline Error Message is not found"+InVaildSearchSuggestion.getText());
  					}
		}
		
		catch(Exception e)
		{
			try
  			{
  				WebElement SearchKeywordAlert = BNBasicCommonMethods.findElement(driver, obj, "SearchKeywordNoProductFoundAlert");
  				if(SearchKeywordAlert.isDisplayed())
  				{
  					fail("Sorry, no results could be found. alert is shown");
  				}
  				else
  				{
  					return;
  				}
  			}
  			catch(Exception e1)
  			{
  				WebElement SearchEAN = BNBasicCommonMethods.findElement(driver, obj, "SearchEANNoProductFoundAlert");
  				if(SearchEAN.isDisplayed())
  				{
  					fail("The entered EAN is not working, No Product Found Alert is shown");
  				}
  				else
  				{
  					return;
  				}
  			}
		}
		
	}

  	/*BNIA-458 Verify that Search suggestion should not shown after clearing the search keywords in search page*/
  	public void BNIA458()
  	{
  		try
  		{
  			//String SreachKwd1 = "harry potter";
  			sheet = BNBasicCommonMethods.excelsetUp("Search");
  			String SreachKwd1 = BNBasicCommonMethods.getExcelVal("BNIA458", sheet,2);
  			WebElement SearchTxtBxnew = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");
  			WebElement SearchTxtBx1 = BNBasicCommonMethods.findElement(driver, obj, "searchboxwithval");
  			BNBasicCommonMethods.waitforElement(wait, obj, "SearchClearIcon");
  			WebElement SearchClearIcon = BNBasicCommonMethods.findElement(driver, obj, "SearchClearIcon");
  			Actions action = new Actions(driver);
    			action.moveToElement(SearchTxtBx1).sendKeys(SreachKwd1).build().perform();
    			Thread.sleep(1500);
  			if(SearchClearIcon.isDisplayed())
  			{
  				log.add("Search Clear icon is displayed.");
  				SearchClearIcon.click();
  				SearchTxtBxnew.clear();
  				Thread.sleep(500);
  				WebElement SearchTextbox = BNBasicCommonMethods.findElement(driver, obj, "SearchTextbox");
  				if(SearchTextbox.getText().isEmpty())
  				{
  					log.add("Search Keyword is Cleared");
  					try
  					{
  					WebElement searchSuggestioncontainer = BNBasicCommonMethods.findElement(driver, obj, "searchSuggestioncontainer");
  					fail("Search Suggestion is showing after clearing the search keyword in search field",log);
  					}
  					catch(Exception e)
  					{
  					pass("Search Suggestion is not shown after clearing the Search keyword in Search field",log);
  					}
  				}
  				else
  				{
  					fail("Search Keyword is not clearing");
  				}
  			}
  			else
  			{
  				fail("Search Clear Icon is not Displayed");
  			}
  			
  			
  		}
  		
  		catch(Exception e)
  		{
  	       System.out.println(e.getMessage());
  			exception("There is somwthing went wrong , please verify"+e.getMessage());
  		}
  		
  	}
  	  	
  	/*BNIA-463 - Verify that while entering the invalid search keyword in the search field,the No suggested search for XXXX alert message should be displayed*/
  	public void BNIA463()
  	{
  		ChildCreation("BNIA-463 - Verify that while entering the invalid search keyword in the search field,the No suggested search for XXXX alert message should be displayed");
  		try
  		{
  			Thread.sleep(2000);
  			sheet = BNBasicCommonMethods.excelsetUp("Search");
  			String SreachKwd1 = BNBasicCommonMethods.getExcelVal("BNIA463", sheet, 2);
  			WebElement searchboxwithval2 = BNBasicCommonMethods.findElement(driver, obj, "searchboxwithval2");
  	  		if(searchboxwithval2.isDisplayed())
  	  		{
  	  			pass("Search Box is found");
  	  		    Thread.sleep(2000);
  	  			searchboxwithval2.click();
  	  			Actions action = new Actions(driver);
  	  			action.moveToElement(searchboxwithval2).sendKeys(SreachKwd1).build().perform();
  	  			Thread.sleep(1500);
  	  			BNBasicCommonMethods.waitforElement(wait, obj, "searchsuggerr"); 
  	  			WebElement searchsuggerror = BNBasicCommonMethods.findElement(driver, obj, "searchsuggerr");
  	  			
  	    		if(searchsuggerror.isDisplayed())
  	    		{
  	  				pass("while entering the invalid search keyword in the search field,the No suggested search for XXXX alert message is displayed");
  	  			}
  	  			else
  	  			{
  	  				pass("while entering the invalid search keyword in the search field,the No suggested search for XXXX alert message not displayed");
  	  			}
  	  			
  	  		}
  	  		else
  	  		{
  	  			fail("there is no Search Box is found");
  	  		}	
  	  		BNBasicCommonMethods.waitforElement(wait, obj, "srchclosebtn"); 
    			WebElement srchclosebtn = BNBasicCommonMethods.findElement(driver, obj, "srchclosebtn");
    			srchclosebtn.click();
  		}
  		catch(Exception e)
  		{
  			System.out.println(e.getMessage());
  			exception(" There is somwthing went wrong , please verify"+e.getMessage());
  		}
  		
  	}
	
  	/*BNIA-465 - Verify that  while entering the search keywords as HTML tags, the input value should be treated as a string and the search suggestions should be displayed*/
  	public void BNIA465()
  	{
  		ChildCreation("BNIA-465 - Verify that  while entering the search keywords as HTML tags, the input value should be treated as a string and the search suggestions should be displayed");
  		try
  		{
  			Thread.sleep(3000);
  			sheet = BNBasicCommonMethods.excelsetUp("Search");
  			String SreachKwd2 = BNBasicCommonMethods.getExcelVal("BNIA465", sheet, 2);
  			//String SreachKwd2 = "<hilter/>";
  			WebElement SearchTxtBxnew1 = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");
  	  		if(SearchTxtBxnew1.isDisplayed())
  	  		{
  	  			pass("Search Box is found");
  	  		    Thread.sleep(2000);
  	  	      	SearchTxtBxnew1.click();
  	  	        WebElement SearchTxtBxnew2 = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");
  	  	      	SearchTxtBxnew2.clear();
  	  	      	Actions action = new Actions(driver);
  	  			action.moveToElement(SearchTxtBxnew2).sendKeys(SreachKwd2).build().perform();
  	  			/*Thread.sleep(1500);*/
  	  			BNBasicCommonMethods.waitforElement(wait, obj, "searchsuggerror1"); 
  	  			WebElement searchsuggerror1 = BNBasicCommonMethods.findElement(driver, obj, "searchsuggerror1");
  	  			
  	    		if(searchsuggerror1.isDisplayed())
  	    		{
  	  				pass("Verify that  while entering the search keywords as HTML tags, the input value should be treated as a string and the search suggestions are displayed");
  	  			}
  	  			else
  	  			{
  	  				pass("Verify that  while entering the search keywords as HTML tags, the input value should be treated as a string and the search suggestions not displayed");
  	  			} 
  	  			
  	  		}
  	  		else
  	  		{
  	  			fail("there is no Search Box is found");
  	  		}	
  		}
  		catch(Exception e)
  		{
  			System.out.println(e.getMessage());
  			exception(" There is somwthing went wrong , please verify"+e.getMessage());
  		}
  		
  	}
  	
	/*BNIA-475 - Verify that while entering the EAN number,the respective product details page should be displayed*/
	public void BNIA475()
	{
		ChildCreation("BNIA-475 - Verify that while entering the EAN number,the respective product details page should be displayed");
		try
		{
			Thread.sleep(2500);
			sheet = BNBasicCommonMethods.excelsetUp("Search");
  			String SreachKwd3 = BNBasicCommonMethods.getExcelNumericVal("BNIA475", sheet,2);
			//String SreachKwd3 = "9780316407021";
  			WebElement SearchTxtBxnew = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");
	  		if(SearchTxtBxnew.isDisplayed())
	  		{
	  			pass("Search Box is found");
	  			Thread.sleep(2000);
	  			SearchTxtBxnew.click();
	  			SearchTxtBxnew.clear();
	  			Actions action = new Actions(driver);
	  			action.moveToElement(SearchTxtBxnew).sendKeys(SreachKwd3).build().perform();
	  			Thread.sleep(1500);
	  			SearchTxtBxnew.sendKeys(Keys.ENTER);
	  			BNBasicCommonMethods.waitforElement(wait, obj, "PDPcontainer"); 
	  			WebElement PDPcontainer = BNBasicCommonMethods.findElement(driver, obj, "PDPcontainer");
	    		if(PDPcontainer.isDisplayed())
	    		{
	  				pass("While entering the EAN number,the respective product details page is displayed");
	  			}
	  			else
	  			{
	  				pass("While entering the EAN number,the respective product details page not displayed");
	  			} 
	  			
	  		}
	  		else
	  		{
	  			fail("there is no Search Box is found");
	  		}	
	  		BNBasicCommonMethods.waitforElement(wait, obj, "PDPcontainerbck"); 
  			WebElement PDPcontainerbck = BNBasicCommonMethods.findElement(driver, obj, "PDPcontainerbck");
  			PDPcontainerbck.click(); 
  			Thread.sleep(1500);
  			BNBasicCommonMethods.waitforElement(wait, obj, "SearchTxtBxnew"); 
  			SearchTxtBxnew = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");
  			SearchTxtBxnew.clear();
  			/*BNBasicCommonMethods.waitforElement(wait, obj, "srchclosebtn2"); 
  			WebElement srchclosebtn2 = BNBasicCommonMethods.findElement(driver, obj, "srchclosebtn2");
  			srchclosebtn2.click();
  			Thread.sleep(1500);*/
  			
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			exception(" There is somwthing went wrong , please verify"+e.getMessage());
		}
		
	}
	
	/*BNIA-570 - Verify that while entering the EAN number,the respective alert should be displayed*/
	public void BNIA570()
	{
		ChildCreation("BNIA-570 - Verify that while entering the Invalid EAN number,the respective alert should be displayed");
		try
		{
			Thread.sleep(2500);
			/*String SreachKwd4 = "233455677899";*/
			sheet = BNBasicCommonMethods.excelsetUp("Search");
  			String SreachKwd4 = BNBasicCommonMethods.getExcelNumericVal("BNIA570", sheet,2);
  			WebElement SearchTxtBxnew = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");
	  		if(SearchTxtBxnew.isDisplayed())
	  		{
	  			pass("Search Box is found");
	  			Thread.sleep(2000);
	  			SearchTxtBxnew.click();
	  			SearchTxtBxnew.clear();
	  			Actions action = new Actions(driver);
	  			action.moveToElement(SearchTxtBxnew).sendKeys(SreachKwd4).build().perform();
	  			Thread.sleep(1500);
	  			SearchTxtBxnew.sendKeys(Keys.ENTER);
	  			Thread.sleep(2500);
	  			BNBasicCommonMethods.waitforElement(wait, obj, "noproudctfound"); 
	  			WebElement noproductfound = BNBasicCommonMethods.findElement(driver, obj, "noproudctfound");
	    		if(noproductfound.isDisplayed())
		    	{
		  			log.add("Entered Search Keyword "+SreachKwd4);
		  			pass("while entering the Invalid EAN number,the respective alert is displayed");
		  		}
		  		else
		  		{
		  			log.add("Entered Search Keyword"+SreachKwd4);
		  			pass("while entering the Invalid EAN number,the respective alert is not displayed");
		  		}
	  			
	  		}
	  		else
	  		{
	  			fail("there is no Search Box is found");
	  		}	
	  		BNBasicCommonMethods.waitforElement(wait, obj, "Noproductbck"); 
  			WebElement Noproductbck = BNBasicCommonMethods.findElement(driver, obj, "Noproductbck");
  			Noproductbck.click(); 
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			exception(" There is somwthing went wrong , please verify"+e.getMessage());
		}
		
	}
	
	/*BNIA-600 - Verify that while selecting the back button from search ,the home page should be displayed*/
	public void BNIA600()
	{
		ChildCreation("BNIA-600 - Verify that while selecting the back button from search ,the home page should be displayed");
		try
		{
			Thread.sleep(1500);
			BNBasicCommonMethods.waitforElement(wait, obj, "PLPbackbtn"); 
  			WebElement searchbck = BNBasicCommonMethods.findElement(driver, obj, "PLPbackbtn");
  			searchbck.click();
  			Thread.sleep(500);
  			BNBasicCommonMethods.waitforElement(wait, obj, "Homepage1"); 
  			WebElement Homepage1 = BNBasicCommonMethods.findElement(driver, obj, "Homepage1");
  			if(Homepage1.isDisplayed())
	    	{
	  			pass("while selecting the back button from search ,the home page is displayed");
	  		}
	  		else
	  		{
	  			pass("while selecting the back button from search ,the home page not displayed");
	  		}
  			System.out.println("6/11 - BNIA11_SearchPage is completed");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			exception(" There is somwthing went wrong , please verify"+e.getMessage());
		}
	}

		
/******************************************************************************** Store Locator *****************************************************************/

 //Initial Launch of the Page and Clearing the Sign In fields.
 @Test(priority=7)
  public void BNIA47_Store_Locator() throws Exception
    {
		 TempSignIn();
		 BNIA619();
		 BNIA621();
		 /*BNIA631();*/
		 BNIA622();
		 BNIA1970();
		 BNIA1969();
		 BNIA625();
		 BNIA626();
		 BNIA630();
		 BNIA632();
		 BNIA634();
		 BNIA1971();
		 BNIA1972();
		 BNIA1973();
		 BNIA1974();
		 BNIA1975();
		 BNIA1976();
		 BNIA1985();
		 BNIA1977();
		 BNIA1978();
		 BNIA1980();
		 BNIA1984();
		 BNIA1982();
		 BNIA1981();
		 BNIA1979();
		 BNIA1983();
		 BNIA631();
		 BNIA620();
		 //TemSignOut();
		 TempSessionTimeout();
		 
		 
	  }

	
    /*BNIA619 - Verify that Store Locator icon should be shown in the home page*/
	public void BNIA619() throws Exception 
	   {
	  	ChildCreation("BNIA619 - Verify that Store Locator icon should be shown in the home page");
		  	try
		  	{
		  		BNBasicCommonMethods.waitforElement(wait, obj, "storeloctile");
		  	 	WebElement storeloctile = BNBasicCommonMethods.findElement(driver, obj, "storeloctile");
		  	 	if(storeloctile.isDisplayed())
		  	 	{
		  	 		pass(" Store Locator icon is showing in the home page ");
		  	 	}
		  	 	else
		  	 	{
		  	 		fail(" Store Locator is not showing in the home page ");
		  	 	}
		  	}
		  	catch(Exception e)
		 	{
		     	 e.getMessage();
		 			System.out.println("Something went wrong"+e.getMessage());
		 			exception(e.getMessage());
		 	}
	  	
	   }
	   
	/*BNIA620 - Verify that Store Locator icon should be shown in the Pancake flyover.*/
	public void BNIA620() throws Exception 
	   {
	  	ChildCreation("BNIA620 - Verify that Store Locator icon should be shown in the Pancake flyover.");
	  	try
	  	{
	  		BNBasicCommonMethods.waitforElement(wait, obj, "pancakeicon");
	  	 	WebElement pancakeicon = BNBasicCommonMethods.findElement(driver, obj, "pancakeicon");
	  	 	pancakeicon.click();
	  	 	Thread.sleep(1000);
	  	 	BNBasicCommonMethods.waitforElement(wait, obj, "storelocfrompancake");
	  	 	WebElement storelocfrompancake = BNBasicCommonMethods.findElement(driver, obj, "storelocfrompancake");
	  	 	if(storelocfrompancake.isDisplayed())
	  	 	{
	  	 		pass(" Store Locator icon is showing in the pancake ");
	  	 	}
	  	 	else
	  	 	{
	  	 		fail(" Store Locator is not showing in the panckae ");
	  	 	}
	  	 	BNBasicCommonMethods.waitforElement(wait, obj, "pancakeiconclose");
	  	 	Thread.sleep(1000);;
	  	 	WebElement pancakeiconclose = BNBasicCommonMethods.findElement(driver, obj, "pancakeiconclose");
	  	 	pancakeiconclose.click();
	  	 	System.out.println("7/11 - BNIA47_Store_Locator is completed");
		  	}
	  	catch(Exception e)
	 	{
	      e.getMessage();
	 	  System.out.println("Something went wrong"+e.getMessage());
			exception(e.getMessage());
		}
	  }	
	  	
	/*BNIA621 - Verify that on tapping the Store locator icon, Store Locator page should be shown*/
	public void BNIA621() throws Exception 
	{
		ChildCreation("BNIA621 - Verify that on tapping the Store locator icon, Store Locator page should be shown.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "storeloctile");
		 	WebElement storeloctile = BNBasicCommonMethods.findElement(driver, obj, "storeloctile");
		 	if(storeloctile.isDisplayed())
		 	{
		 		pass(" Store Locator icon is showing in the home page ");
		 		storeloctile.click();
		 		BNBasicCommonMethods.waitforElement(wait, obj, "storelocContainer");
		  	WebElement storelocContainer = BNBasicCommonMethods.findElement(driver, obj, "storelocContainer");
		  	if(storelocContainer.isDisplayed())
		  	{
		 			pass(" Store Locator page is showing on selecting store locator icon from the home page ");	
		 		}
		 		else
		 		{
		 			fail("Store Locator page is showing on selecting store locator icon from the home page ");
		 		}
		 	}
		 	else
		 	{
		 		fail(" Store Locator is not showing in the home page ");
		 	}
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA622 - Verify that Store locator page should contain Search field*/
	public void BNIA622() throws Exception 
	{
		ChildCreation("BNIA622 - Verify that Store locator page should contain Search field.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "storelocsearchicon");
		 	WebElement storelocsearchicon = BNBasicCommonMethods.findElement(driver, obj, "storelocsearchicon");
		 	WebElement Inlinetext = BNBasicCommonMethods.findElement(driver, obj, "InlineText");
		 	String inline = Inlinetext.getAttribute("placeholder");
		 	//System.out.println(""+inline);
		 	String text = "Enter a store name, location, or zipcode ";
		 	if(inline.equals(text))
		 	{
		 		
		 		pass("Inline text is showing fine now");
		 	}
		 	else
		 	{
		 		
		 		fail("inline text is not shown");
		 	}
		    if(storelocsearchicon.isDisplayed())
		    {
		    	StoreLocatorFlag622 = true;
		    	pass(" Store locator page contains Search field ");	
		 	}
		 	else
		 	{
		 		StoreLocatorFlag622 = false;
		 		fail("Store locator page does not contains Search field ");
		 	}
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA1970 - Verify that Store Search field should contain Search inline text*/
	public void BNIA1970() throws Exception 
	{
		ChildCreation("BNIA1970 - Verify that Store Search field should contain Search inline text.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "InlineText");
		 	WebElement Inlinetext = BNBasicCommonMethods.findElement(driver, obj, "InlineText");
		 	String inline = Inlinetext.getAttribute("placeholder");
		 	//System.out.println(""+inline);
		 	String text = "Enter a store name, location, or zipcode ";
		 	if(inline.equals(text))
		 	{
		 		pass("Inline text is showing fine now");
		 	}
		 	else
		 	{
		 		fail("inline text is not shown");
		 	}
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}

	/*BNIA1969 - Verify that Store locator page should contain header & its icons*/
	public void BNIA1969() throws Exception 
	{
		ChildCreation("BNIA1969 - Verify that Store locator page should contain header & its icons");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "storepageheader");
		 	WebElement storepageheader = BNBasicCommonMethods.findElement(driver, obj, "storepageheader");
		    if(storepageheader.isDisplayed())
		{
		    	
		    	pass(" Store locator page contains header ");
		    	BNBasicCommonMethods.waitforElement(wait, obj, "storepageheaderpancake");
		 	WebElement storepageheaderpancake = BNBasicCommonMethods.findElement(driver, obj, "storepageheaderpancake");
		 	boolean storepageheaderpancake1 = BNBasicCommonMethods.isElementPresent(storepageheaderpancake);
		 	BNBasicCommonMethods.waitforElement(wait, obj, "storepageheaderbck");
		 	WebElement storepageheaderbck = BNBasicCommonMethods.findElement(driver, obj, "storepageheaderbck");
		 	boolean storepageheaderbck1 = BNBasicCommonMethods.isElementPresent(storepageheaderbck);
		 	BNBasicCommonMethods.waitforElement(wait, obj, "storepageheaderlogo");
		 	WebElement storepageheaderlogo = BNBasicCommonMethods.findElement(driver, obj, "storepageheaderlogo");
		 	boolean storepageheaderlogo1 = BNBasicCommonMethods.isElementPresent(storepageheaderlogo);
		 	BNBasicCommonMethods.waitforElement(wait, obj, "storepageheadersearch");
		 	WebElement storepageheadersearch = BNBasicCommonMethods.findElement(driver, obj, "storepageheadersearch");
		 	boolean storepageheadersearch1 = BNBasicCommonMethods.isElementPresent(storepageheadersearch);
		 	BNBasicCommonMethods.waitforElement(wait, obj, "storepageheaderscan");
		 	WebElement storepageheaderscan = BNBasicCommonMethods.findElement(driver, obj, "storepageheaderscan");
		 	boolean storepageheaderscan1 = BNBasicCommonMethods.isElementPresent(storepageheaderscan);
		 	BNBasicCommonMethods.waitforElement(wait, obj, "storepageheaderbag");
		 	WebElement storepageheaderbag = BNBasicCommonMethods.findElement(driver, obj, "storepageheaderbag");
		 	boolean storepageheaderbag1 = BNBasicCommonMethods.isElementPresent(storepageheaderbag);
		 	if(storepageheaderpancake1 && storepageheaderbck1 && storepageheaderlogo1 && storepageheadersearch1 && storepageheaderscan1 && storepageheaderbag1)
			{
		 		StoreLocatorFlag1969 = true;
		 		pass("Store locator page contains header & its icons(Back button, Pancake, Home page logo, Search, Scan & Bag Icons ");
			}
		 	else 
		 	{
		 		StoreLocatorFlag1969 = false;
		 		fail("Store locator page does not contains header & its icons ");
		 	}
		 	}
		 	else
		 	{
		 		
		 		fail("Store locator page does not contains header");
		 	}
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA625 - Verify that, the "X" icon should be shown, when a character is entered in the search field.*/
	public void BNIA625() throws Exception 
	{
		ChildCreation("BNIA625 - Verify that, the X icon should be shown, when a character is entered in the search field.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("storelocator");
		String SreachKwd6 = BNBasicCommonMethods.getExcelNumericVal("BNIA625", sheet,2);
		BNBasicCommonMethods.waitforElement(wait, obj, "storesearchbox6");
		 	WebElement storesearchbox6 = BNBasicCommonMethods.findElement(driver, obj, "storesearchbox6");
			if(storesearchbox6.isDisplayed())
			{
				pass("Search Box is found");
				Thread.sleep(500);
				storesearchbox6.clear();
				storesearchbox6.click();
				Thread.sleep(1000);
				Actions action = new Actions(driver);
				Thread.sleep(500);
				action.moveToElement(storesearchbox6).sendKeys(SreachKwd6).build().perform();
				Thread.sleep(1000);
				/*searchboxwithval4.sendKeys(Keys.ENTER);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDPcontainer"); 
				WebElement PDPcontainer = BNBasicCommonMethods.findElement(driver, obj, "PDPcontainer");*/
				BNBasicCommonMethods.waitforElement(wait, obj, "storelocsearchclearicon1");
		 	WebElement storelocsearchclearicon1 = BNBasicCommonMethods.findElement(driver, obj, "storelocsearchclearicon1");
		        if(storelocsearchclearicon1.isDisplayed())
		        {
		        	StoreLocatorFlag625 = true;
		        	pass(" X icon is showing when a character is entered in the search field ");	
		 	    }
		 	    else
		 	    {
		 	    	StoreLocatorFlag625 = false;
		 	    	fail("X icon is not shown when a character is entered in the search field ");
		 	    }
			}
			else
		 	{
				
		 		fail("X icon is not shown when a character is entered in the search field ");
		 	}
		}
		
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA626 - Verify that,on tapping the X close icon in the search field, the entered character should be cleared. */
	public void BNIA626() throws Exception 
	{
		ChildCreation("BNIA626 - Verify that, on tapping the X close icon in the search field,the entered character should be cleared. ");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "storelocsearchclearicon2");
			Thread.sleep(1000);
		 	WebElement storelocsearchclearicon2 = BNBasicCommonMethods.findElement(driver, obj, "storelocsearchclearicon2");
		 	storelocsearchclearicon2.click();
		 	Thread.sleep(1000);
		 	BNBasicCommonMethods.waitforElement(wait, obj, "storesearchbox1");
		 	WebElement storesearchbox1 = BNBasicCommonMethods.findElement(driver, obj, "storesearchbox1");
			if(storesearchbox1.isDisplayed())
			{
				pass(" Empty Search Box is found");
				Thread.sleep(1000);
				storesearchbox1.click();
				Thread.sleep(1000);
				/*searchboxwithval4.sendKeys(Keys.ENTER);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDPcontainer"); 
				WebElement PDPcontainer = BNBasicCommonMethods.findElement(driver, obj, "PDPcontainer");*/
		    if(storesearchbox1.isDisplayed())
		{
		    	StoreLocatorFlag626 = true;
		    	pass(" On tapping the X close icon in the search field,the entered character is cleared ");	
		 	}
		 	else
		 	{
		 		StoreLocatorFlag626 = false;
		 		fail("On tapping the X close icon in the search field,the entered character should be cleared");
		 	}
			}
			else
		 	{
		 		fail("Still Search box is found with value ");
		 	}
		}
		
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA630 -Verify that on entering the invalid State or City or Zip code or Store Id, the corresponding alert message should be shown.*/
	public void BNIA630()
		{
			ChildCreation("BNIA630 - Verify that on entering the invalid State or City or Zip code or Store Id, the corresponding alert message should be shown.\n");
			try
			{
				sheet = BNBasicCommonMethods.excelsetUp("storelocator");
				String SearchKeywordList = BNBasicCommonMethods.getExcelVal("BNIA630", sheet,2);
				String[] SearchKeyword = SearchKeywordList.split("\n");
				BNBasicCommonMethods.waitforElement(wait, obj, "storesearchbox3");
			//	WebElement StoreBoxNew =  driver.findElement(By.xpath("//*[@id='storeSearchForm']"));
				
				for(int ctr = 0; ctr < SearchKeyword.length ; ctr++)
				{
					WebElement storesearchbox3 = BNBasicCommonMethods.findElement(driver, obj, "storesearchbox3");
					storesearchbox3.click();
					storesearchbox3.clear();
					Thread.sleep(500);
					storesearchbox3.sendKeys(SearchKeyword[ctr]);
					Thread.sleep(500);
					storesearchbox3.sendKeys(Keys.ENTER);
					Thread.sleep(2500);
					try
					{
						BNBasicCommonMethods.waitforElement(wait, obj, "storesearcherror");
						WebElement storesearcherror = BNBasicCommonMethods.findElement(driver, obj, "storesearcherror");
						if(storesearcherror.isDisplayed())
						{
							pass("\n"+SearchKeyword[ctr]);
							pass("\n"+storesearcherror.getText());
							pass("On entering the invalid State or City or Zip code or Store Id, the corresponding alert message is showing now\n");
							
							
						}
						else
						{
							fail("On entering the invalid State or City or Zip code or Store Id, the corresponding alert message is not shown\n"+storesearcherror.getText());
						}
					}
					catch(Exception e)
				 	{
				     	 e.getMessage();
				     	System.out.println("Something went wrong"+e.getMessage());
		 			exception(e.getMessage());
				 	}
					
				}
				
			}
					catch(Exception e)
				 	{
				     	 e.getMessage();
				 			System.out.println("Something went wrong"+e.getMessage());
				 			exception(e.getMessage());
				 	}
				  	
				   }
	
	/*BNIA632 -Verify that, when the invalid character or numbers is entered in the search field & "Go" button is clicked then it should display the alert message.*/
	public void BNIA632()
		{
			ChildCreation("BNIA632 - Verify that, when the invalid character or numbers is entered in the search field & Go button is clicked then it should display the alert message.");
			try
			{
				sheet = BNBasicCommonMethods.excelsetUp("storelocator");
				String SearchKeywordList = BNBasicCommonMethods.getExcelVal("BNIA632", sheet,2);
				String[] SearchKeyword = SearchKeywordList.split("\n");
				BNBasicCommonMethods.waitforElement(wait, obj, "storesearchbox7");
			//	WebElement StoreBoxNew =  driver.findElement(By.xpath("//*[@id='storeSearchForm']"));
				
				for(int ctr = 0; ctr < SearchKeyword.length ; ctr++)
				{
					WebElement storesearchbox7 = BNBasicCommonMethods.findElement(driver, obj, "storesearchbox7");
					storesearchbox7.click();
					storesearchbox7.clear();
					Thread.sleep(500);
					storesearchbox7.sendKeys(SearchKeyword[ctr]);
					Thread.sleep(500);
					storesearchbox7.sendKeys(Keys.ENTER);
					Thread.sleep(2500);
					try
					{
						BNBasicCommonMethods.waitforElement(wait, obj, "storesearcherror1");
						WebElement storesearcherror1 = BNBasicCommonMethods.findElement(driver, obj, "storesearcherror1");
						if(storesearcherror1.isDisplayed())
						{
							pass("\n"+SearchKeyword[ctr]);
							pass("\n"+storesearcherror1.getText());
							pass("when entering the invalid characters or invalid numbers, it displays the alert message.\n");
							
							
						}
						else
						{
							fail("when entering the invalid characters or invalid numbers, it does not displays the alert message.");
						}
					}
					catch(Exception e)
				 	{
				     	 e.getMessage();
				     	System.out.println("Something went wrong"+e.getMessage());
		 			exception(e.getMessage());
				 	}
					
				}
				
			}
					catch(Exception e)
				 	{
				     	 e.getMessage();
				 			System.out.println("Something went wrong"+e.getMessage());
				 			exception(e.getMessage());
				 	}
				  	
				   }
	
	/*BNIA634 - Verify that on entering the valid "State" or "City" or "Zip code" or "Store Id", it should navigate to "Store Details" page.*/
	public void BNIA634() throws Exception 
	{
		ChildCreation("BNIA634 - Verify that on entering the valid State or City or Zip code or Store Id, it should navigate to Store Details page.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("storelocator");
		String SearchKeywordList = BNBasicCommonMethods.getExcelVal("BNIA634", sheet,2);
		String[] SearchKeyword1 = SearchKeywordList.split("\n");
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchbox8");
			//	WebElement StoreBoxNew =  driver.findElement(By.xpath("//*[@id='storeSearchForm']"));
				
				for(int ctr = 0; ctr < SearchKeyword1.length ; ctr++)
				{
					
					BNBasicCommonMethods.waitforElement(wait, obj, "storesearchbox8");
					WebElement storesearchbox8 = BNBasicCommonMethods.findElement(driver, obj, "storesearchbox8");
					storesearchbox8.click();
					storesearchbox8.clear();
					Thread.sleep(1000);
					storesearchbox8.sendKeys(SearchKeyword1[ctr]);
					Thread.sleep(1000);
					storesearchbox8.sendKeys(Keys.ENTER);
					Thread.sleep(2500);
					try
					{
						BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresults");
						WebElement storesearchresults = BNBasicCommonMethods.findElement(driver, obj, "storesearchresults");
						if(storesearchresults.isDisplayed())
						{
							pass("\n"+SearchKeyword1[ctr]);
							pass("On entering the valid State or City or Zip code or Store Id, the corresponding store results sre showing now\n");	
						}
						else
						{
							fail("On entering the valid State or City or Zip code or Store Id, the corresponding store resutls are not showing now\n");
						} 
					}
				
			catch(Exception e)
		{
	  	 e.getMessage();
	  	System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
	}
	}
		
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA-1971 - Verify that Store image should be shown in store results page */
	public void BNIA1971() throws Exception 
	{
		ChildCreation("BNIA1971 - Verify that Store image should be shown in store results page");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultsimg");
		 	WebElement storesearchresultsimg = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultsimg");
		 	if(storesearchresultsimg.isDisplayed())
		 	{
		 		StoreLocatorFlag1971 = true;
		 		pass("  Store image is showing in store results page ");
		 	}
		 	else
		 	{
		 		StoreLocatorFlag1971 = false;
		 		fail(" Store image is not showing in store results page ");
		 	}
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA-1972 - Verify that store name and address should shown in store results page. */
	public void BNIA1972() throws Exception 
	{
		ChildCreation("BNIA1972 - Verify that store name and address should shown in store results page.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultsdtl");
		 	WebElement storesearchresultsdtl = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultsdtl");
		 	if(storesearchresultsdtl.isDisplayed())
		 	{
		 		pass("  Store name & address is showing in store results page ");
		 	}
		 	else
		 	{
		 		fail(" Store name & address is not showing in store results page ");
		 	}
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA-1973 - Verify that store location icon should shown in store results page.. */
	public void BNIA1973() throws Exception 
	{
		ChildCreation("BNIA1973 - Verify that store location icon should shown in store results page.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultsicon");
		 	WebElement storesearchresultsicon = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultsicon");
		 	if(storesearchresultsicon.isDisplayed())
		 	{
		 		StoreLocatorFlag1973 = true;
		 		pass("  Store location icon is showing in store results page ");
		 	}
		 	else
		 	{
		 		StoreLocatorFlag1973 = false;
		 		fail(" Store location icon is not showing in store results page ");
		 	}
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA-1974 - Verify that store results count should shown in store results page. */
	public void BNIA1974() throws Exception 
	{
		ChildCreation("BNIA1974 - Verify that store results count should shown in store results page.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultslocicon");
		 	WebElement storesearchresultslocicon = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultslocicon");
		 	if(storesearchresultslocicon.isDisplayed())
		 	{
		 		StoreLocatorFlag1974 = true;
		 		pass("  Store results counts are  showing in store results page ");
		 	}
		 	else
		 	{
		 		StoreLocatorFlag1974 = false;
		 		fail(" Store results counts are not showing in store results page ");
		 	}
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA-1975 - Verify that store timings & days should shown in store results page. */
	public void BNIA1975() throws Exception 
	{
		ChildCreation("BNIA1975 - Verify that store timings & days should shown in store results page..");
		try
		{
		 	WebElement storesearchresultshours = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultshours");
		 	boolean storesearchresultshours1 = BNBasicCommonMethods.isElementPresent(storesearchresultshours);
		 	WebElement storesearchresultshoursdtl = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultshoursdtl");
		 	boolean storesearchresultshoursdtl1 = BNBasicCommonMethods.isElementPresent(storesearchresultshoursdtl);
		 	if(storesearchresultshours1&&storesearchresultshoursdtl1)
		{
		 		pass("  Store timings & working days  are  showing in store results page ");
		 	}
		 	else
		 	{
		 		fail(" Store timings & working days  are not showing in store results page ");
		 	}
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
		
	/*BNIA-1976 - Verify that pagination should shown in store results page.. */
	public void BNIA1976() throws Exception 
	{
		ChildCreation("BNIA1976 - Verify that pagination should shown in store results page.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultpagination");
		 	WebElement storesearchresultpagination = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultpagination");
		 	if(storesearchresultpagination.isDisplayed())
		 	{
		 		StoreLocatorFlag1976 = true;
		 		pass("  Pagination is showing in store results page ");
		 	}
		 	else
		 	{
		 		StoreLocatorFlag1976 = false;
		 		fail(" Pagination is not showing in store results page ");
		 	}
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA-1985 - Verify that pagination between the pages should works in store results page. */
	public void BNIA1985() throws Exception 
	{
		ChildCreation("BNIA1985 - Verify that pagination between the pages should works in store results page.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "storeresultspage2");
		 	WebElement storeresultspage2 = BNBasicCommonMethods.findElement(driver, obj, "storeresultspage2");
		 	if(storeresultspage2.isDisplayed())
		 	{
		 		storeresultspage2.click();
		 		StoreLocatorFlag1985 = true;
		 		pass("  Pagination buttons are working in store results page ");
		 	}
		 	else
		 	{
		 		StoreLocatorFlag1985 = false;
		 		fail(" Pagination buttons are not working showing in store results page ");
		 	}
		 	BNBasicCommonMethods.waitforElement(wait, obj, "storeresultspage1");
		 	WebElement storeresultspage1 = BNBasicCommonMethods.findElement(driver, obj, "storeresultspage1");
		 	Thread.sleep(1000);
		 	storeresultspage1.click();
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA-1977 - Verify that Store features should be shown in store results page  */
	public void BNIA1977() throws Exception 
	{
		ChildCreation("BNIA1977 - Verify that Store features should be shown in store results page .");
		try
		{
			WebElement storesearchresultfeature = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultfeature");
		 	boolean storesearchresultfeature1 = BNBasicCommonMethods.isElementPresent(storesearchresultfeature);
		 	WebElement storesearchresultfeatureitem = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultfeatureitem");
		 	boolean storesearchresultfeatureitem1 = BNBasicCommonMethods.isElementPresent(storesearchresultfeatureitem);
		 	if(storesearchresultfeature1&&storesearchresultfeatureitem1)
		 	{
		 		StoreLocatorFlag1977 = true;
		 		pass("  Store features are showing in store results page ");
		 	}
		 	else
		 	{
		 		StoreLocatorFlag1977 = false;
		 		fail(" Store features are not showing in store results page ");
		 	}
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA-1978 - Verify that MAPS overlay should be shown in store results page on selecting maps & directions link.*/
	public void BNIA1978() throws Exception 
	{
		ChildCreation("BNIA1978 - Verify that MAPS overlay should be shown in store results page on selecting maps & directions link.");
		try
		{
			Thread.sleep(3000);
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultmapsdrctn");
		 	WebElement storesearchresultmapsdrctn = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultmapsdrctn");
		 	if(storesearchresultmapsdrctn.isDisplayed())
		{
		 		Thread.sleep(500);
		 		storesearchresultmapsdrctn.click();
		 		pass("MAPS & direction link is clicked");
		 		BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultmapoverlay");
		 	WebElement storesearchresultmapoverlay = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultmapoverlay");
		 	if(storesearchresultmapoverlay.isDisplayed())
		 		{
		 			StoreLocatorFlag1978 = true;
		 			pass("MAPS overlay is showing in store results page on selecting maps & directions link");
		 	    }
		 	   else
		 	    {	StoreLocatorFlag1978 = false;
		 		    fail("MAPS overlay is not shown in store results page on selecting maps & directions link");
		 	    }
		}
		else
		 	{
		 		fail("MAPS & direction link is not working");
		 	}
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		Thread.sleep(1000);
		
	}
	
	/*BNIA-1980 - Verify that MAPS overlay should consists of A - soruce & B - destination text fields.*/
	public void BNIA1980() throws Exception 
	{
		ChildCreation("BNIA1980 - Verify that MAPS overlay should consists of A - soruce & B - destination text fields.");
		try
		{
			Thread.sleep(1500);
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultmapsdrctn");
			WebElement mapsdirectionlink = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultmapsdrctn");
			
			Thread.sleep(1500);
			/*mapsdirectionlink.click();
			Thread.sleep(1500);*/
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultmapsA");
			WebElement storesearchresultmapsA = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultmapsA");
		 	boolean storesearchresultmapsAa = BNBasicCommonMethods.isElementPresent(storesearchresultmapsA);
		 	BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultmapsB");
		 	WebElement storesearchresultmapsB = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultmapsB");
		 	boolean storesearchresultmapsBb = BNBasicCommonMethods.isElementPresent(storesearchresultmapsB);
		 	if(storesearchresultmapsAa&&storesearchresultmapsBb)
		 	{
		 		StoreLocatorFlag1980 = true;
		 		pass("MAPS overlay consists of A - soruce & B - destination text fields");
		 	}
		 	else
		 	{
		 		StoreLocatorFlag1980 = false;
		 		fail("MAPS overlay doesnot consists of A - soruce & B - destination text fields");
		 	}
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA1984 - Verify that A source & B destination text fields should consists of inline texts.*/
	public void BNIA1984() throws Exception 
	{
		ChildCreation("BNIA1984 - Verify that A source & B destination text fields should consists of inline text.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "InlineTextMap");
		 	WebElement InlineTextMap = BNBasicCommonMethods.findElement(driver, obj, "InlineTextMap");
		 	String inline = InlineTextMap.getAttribute("placeholder");
		 	//System.out.println(""+inline);
		 	String text = "Enter City, State or Zip Code";
		 	if(inline.equals(text))
		 	{
		 		StoreLocatorFlag1984 = true;
		 		pass("Inline text is showing fine now");
		 	}
		 	else
		 	{
		 		StoreLocatorFlag1984 = false;
		 		fail("inline text is not shown");
		 	}
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}

	/*BNIA1982 -Verify that MAPS routes & details should be shown on entering valid A - soruce & B - destinations.*/
	public void BNIA1982()
		{
			ChildCreation("BNIA1982 - Verify that MAPS routes & details should be shown on entering valid A soruce & B destinations.");
			try
			{
				sheet = BNBasicCommonMethods.excelsetUp("storelocator");
				String SearchKeywordList = BNBasicCommonMethods.getExcelVal("BNIA1982", sheet,2);
				String[] SearchKeyword = SearchKeywordList.split("\n");
				BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultmapsAinput");
			    /*WebElement StoreBoxNew =  driver.findElement(By.xpath("//*[@id='storeSearchForm']"));*/
				
				for(int ctr = 0; ctr < SearchKeyword.length ; ctr++)
				{
					WebElement storesearchresultmapsAinput = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultmapsAinput");
					storesearchresultmapsAinput.click();
					storesearchresultmapsAinput.clear();
					Thread.sleep(500);
					storesearchresultmapsAinput.sendKeys(SearchKeyword[ctr]);
					Thread.sleep(500);
					/*storesearchresultmapsAinput.sendKeys(Keys.ENTER);*/
					BNBasicCommonMethods.waitforElement(wait, obj, "getdirectionbtn");
				WebElement getdirectionbtn = BNBasicCommonMethods.findElement(driver, obj, "getdirectionbtn");
					getdirectionbtn.click();
					Thread.sleep(2500);
					try
					{
						BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultgetdirectionerr");
						WebElement storesearchresultgetdirectionerr = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultgetdirectionerr");
						if(storesearchresultgetdirectionerr.isDisplayed())
						{
							StoreLocatorFlag1982 = true;
							pass("\n"+SearchKeyword[ctr]);
							pass("\n"+storesearchresultgetdirectionerr.getText());
							pass("when entering the invalid address or invalid characters or invalid numbers, it displays the alert with RED box.\n");
							
							
						}
						else
						{
							StoreLocatorFlag1982 = false;
							fail("when entering the invalid characters or invalid numbers, it does not displays the alert.");
						}
					}
					catch(Exception e)
				 	{
				     	 e.getMessage();
				     	System.out.println("Something went wrong"+e.getMessage());
		 			exception(e.getMessage());
				 	}
					
				}
				
			}
					catch(Exception e)
				 	{
				     	 e.getMessage();
				 			System.out.println("Something went wrong"+e.getMessage());
				 			exception(e.getMessage());
				 	}
				  	
				   }
	
	/*BNIA1981 - Verify that MAPS routes & details should be shown on entering valid A soruce & B destinations.*/
	public void BNIA1981() throws Exception 
	{
		ChildCreation("BNIA1981 - Verify that MAPS routes & details should be shown on entering valid A soruce & B destinations.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("storelocator");
		String SearchKeywordList = BNBasicCommonMethods.getExcelVal("BNIA1981", sheet,2);
		String[] SearchKeyword1 = SearchKeywordList.split("\n");
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultmapsAinputValid");
			/*WebElement StoreBoxNew =  driver.findElement(By.xpath("//*[@id='storeSearchForm']"));*/
				
				for(int ctr = 0; ctr < SearchKeyword1.length ; ctr++)
				{
					WebElement storesearchresultmapsAinputValid = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultmapsAinputValid");
					storesearchresultmapsAinputValid.click();
					storesearchresultmapsAinputValid.clear();
					Thread.sleep(500);
					storesearchresultmapsAinputValid.sendKeys(SearchKeyword1[ctr]);
					Thread.sleep(500);
					/*storesearchresultmapsAinputValid.sendKeys(Keys.ENTER);*/
					BNBasicCommonMethods.waitforElement(wait, obj, "getdirectionbtn");
				WebElement getdirectionbtn = BNBasicCommonMethods.findElement(driver, obj, "getdirectionbtn");
					getdirectionbtn.click();
					Thread.sleep(2500);
					try
					{
						BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultDir");
						WebElement storesearchresultDir = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultDir");
						if(storesearchresultDir.isDisplayed())
						{
							StoreLocatorFlag1981 = true;
							pass("\n"+SearchKeyword1[ctr]);
							pass("\n"+storesearchresultDir.getText());
							pass("MAPS routes & details are showing on entering valid A soruce & B destinations.\n");	
						}
						else
						{
							StoreLocatorFlag1981 = false;
							fail("MAPS routes & details are not showing on entering valid A soruce & B destinations\n");
						} 
					}
				
			catch(Exception e)
		{
	  	 e.getMessage();
	  	System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
	}
	}
		
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA-1979 - Verify that MAPS overlay should be closed in store results page on selecting close button. */
	public void BNIA1979() throws Exception 
	{
		ChildCreation("BNIA1979 - Verify that MAPS overlay should be closed in store results page on selecting close button.");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultMapcloseBtn");
		 	WebElement storesearchresultMapcloseBtn = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultMapcloseBtn");
		 	if(storesearchresultMapcloseBtn.isDisplayed())
		 	{
		 		storesearchresultMapcloseBtn.click();
		 		StoreLocatorFlag1979 =true;
		 		pass("  MAPS overlay is closing in store results page on selecting close button ");
		 	}
		 	else
		 	{
		 		StoreLocatorFlag1979 =false;
		 		fail(" MAPS overlay is not closing in store results page on selecting close button ");
		 	}
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA-1983 -Verify that On selecting back from store results page it should takes to home page. */
	public void BNIA1983() throws Exception 
	{
		ChildCreation("BNIA1983 - Verify that On selecting back from store results page it should takes to home page. ");
		try
		{
			BNBasicCommonMethods.waitforElement(wait, obj, "storepagebckbtn");
		 	WebElement storepagebckbtn = BNBasicCommonMethods.findElement(driver, obj, "storepagebckbtn");
		 	storepagebckbtn.click();
		 	Thread.sleep(500);
		 	/*BNBasicCommonMethods.waitforElement(wait, obj, "Splashscreenagain");
		 	WebElement Splashscreenagain = BNBasicCommonMethods.findElement(driver, obj, "Splashscreenagain");*/
		 	BNBasicCommonMethods.waitforElement(wait, obj, "Homepageagain");
		 	WebElement Homepageagain = BNBasicCommonMethods.findElement(driver, obj, "Homepageagain");
		 	if(Homepageagain.isDisplayed())
		 	{
		 		StoreLocatorFlag1983 = true;
		 		pass(" On selecting back from store results page, home page is displayed. ");
		 	}
		 	else
		 	{
		 		StoreLocatorFlag1983 = false;
		 		fail(" On selecting back from store results page, it does not takes to home page. ");
		 	}
		 	System.out.println("8/11 - BNIA50_Store_Detail_Information is completed");
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA-631 - Verify that, when the Search field is left empty, and "Go" button is clicked, then it should thrown the alert message.*/
	public void BNIA631() throws Exception 
	{
		ChildCreation("BNIA-631 - Verify that, when the Search field is left empty, and Go button is clicked, then it should thrown the alert message.");
	  	try
	  	{
	  		/*sheet = BNBasicCommonMethods.excelsetUp("storelocator");
  			String SreachKwd7 = BNBasicCommonMethods.getExcelNumericVal("BNIA631", sheet,2);*/
		   BNBasicCommonMethods.waitforElement(wait, obj, "storeloctile");
		    WebElement storeloctile = BNBasicCommonMethods.findElement(driver, obj, "storeloctile");
			storeloctile.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchbox4");
			WebElement storesearchbox4 = BNBasicCommonMethods.findElement(driver, obj, "storesearchbox4");
		  	Thread.sleep(500);
			if(storesearchbox4.isDisplayed())
				{
					pass("Search Box is found");
				  	/*Actions action = new Actions(driver);
				  	Thread.sleep(1500);
				  	action.moveToElement(storesearchbox4).sendKeys(SreachKwd7).build().perform();
				  	Thread.sleep(1500);
				  	storesearchbox4.sendKeys(Keys.ENTER);*/
				  	BNBasicCommonMethods.waitforElement(wait, obj, "storelocsearchicon1");
					WebElement storelocsearchicon1 = BNBasicCommonMethods.findElement(driver, obj, "storelocsearchicon1");
			  		Thread.sleep(500);
				  	storelocsearchicon1.click();
				  	BNBasicCommonMethods.waitforElement(wait, obj, "storesearchemptyerr"); 
				  	WebElement storesearchemptyerr = BNBasicCommonMethods.findElement(driver, obj, "storesearchemptyerr");
				  	Thread.sleep(1500);
				  	if(storesearchemptyerr.isDisplayed())
				  		{
				  			pass(" when the Search field is left empty, and Go button is clicked, alert message is throwing now ");	
				  		}
				  	else
				  		{
				  	 		fail("when the Search field is left empty, and Go button is clicked, alert message is not throwing now ");
				  		}
				  	}
				  	else
				  	{
				  	 	fail("Search box not found ");
				  	}
				  }
				  	
		catch(Exception e)
		{
			e.getMessage();
			System.out.println("Something went wrong"+e.getMessage());
				 			exception(e.getMessage());
		}
				  	
  } 
		
/********************************************************* Store Detail Information **********************************/
	 //Initial Launch of the Page and Clearing the Sign In fields.
	 @Test(priority=8)
	  public void BNIA50_Store_Detail_Information() throws Exception
	  {
		 TempSignIn();
		 BNIA619();
		 BNIA621();
		 BNIA622();
		 BNIA646();
		 BNIA647();
		 BNIA634();
		 BNIA636();
		 BNIA635();
		 BNIA639();
		 BNIA640();
		 BNIA638();
		 BNIA637();
		 BNIA643();
		 BNIA1972();
		 BNIA652();
		 BNIA653();
		 BNIA654();
		 BNIA1975();
		 BNIA1977A();
		 BNIA641();
		 BNIA1980A();
		 BNIA1984A();
		 BNIA1982A();
		 BNIA1981A();
		 BNIA1979A();
		 BNIA1983A();
		 //TemSignOut();
		 TempSessionTimeout();
		 
	  }
	  
	/*BNIA646 - Verify that, the "X" icon should be shown, when a character is entered in the search field.*/
    public void BNIA646() throws Exception 
	 {
	  ChildCreation("BNIA646 - Verify that, the X icon should be shown, when a character is entered in the search field.");
	  	try
	  	{
	  		if(StoreLocatorFlag625)
	  		{
	  			pass(" X icon is showing when a character is entered in the search field ");	
  	 	    }
  	 	    else
  	 	    {
  	 	    	fail("X icon is not shown when a character is entered in the search field ");
  	 	    }
	  		
	  		
	  		
	  		/*sheet = BNBasicCommonMethods.excelsetUp("storelocator");
 			String SreachKwd6 = BNBasicCommonMethods.getExcelNumericVal("BNIA625", sheet,2);
 			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchbox6");
	  	 	WebElement storesearchbox6 = BNBasicCommonMethods.findElement(driver, obj, "storesearchbox6");
	  		if(storesearchbox6.isDisplayed())
	  		{
	  			pass("Search Box is found");
	  			storesearchbox6.clear();
	  			storesearchbox6.click();
	  			Actions action = new Actions(driver);
	  			Thread.sleep(1500);
	  			action.moveToElement(storesearchbox6).sendKeys(SreachKwd6).build().perform();
	  			Thread.sleep(1500);
	  			searchboxwithval4.sendKeys(Keys.ENTER);
	  			BNBasicCommonMethods.waitforElement(wait, obj, "PDPcontainer"); 
	  			WebElement PDPcontainer = BNBasicCommonMethods.findElement(driver, obj, "PDPcontainer");
	  			BNBasicCommonMethods.waitforElement(wait, obj, "storelocsearchclearicon1");
		  	 	WebElement storelocsearchclearicon1 = BNBasicCommonMethods.findElement(driver, obj, "storelocsearchclearicon1");
	  	        if(storelocsearchclearicon1.isDisplayed())
			    {
	  	    	pass(" X icon is showing when a character is entered in the search field ");	
	  	 	    }
	  	 	    else
	  	 	    {
	  	 		fail("X icon is not shown when a character is entered in the search field ");
	  	 	    }
	  		}
	  		else
	  	 	{
	  	 		fail("X icon is not shown when a character is entered in the search field ");
	  	 	}*/
	  	}
	  	
	  catch(Exception e)
	  {
	     	 e.getMessage();
	 			System.out.println("Something went wrong"+e.getMessage());
	 			exception(e.getMessage());
	  }
	  	
	}
    
    /*BNIA647 - Verify that,on tapping the X close icon in the search field, the entered character should be cleared. */
	public void BNIA647() throws Exception 
	 {
	  	ChildCreation("BNIA647 - Verify that, on tapping the X close icon in the search field,the entered character should be cleared. ");
	  	try
	  	{
	  		
	  		if(StoreLocatorFlag626)
	  		{
	  			pass(" On tapping the X close icon in the search field,the entered character is cleared ");	
	  	 	}
	  	 	else
	  	 	{
	  	 		fail("On tapping the X close icon in the search field,the entered character should be cleared");
	  	 	}
	  		
	  		
	  		
	  		/*BNBasicCommonMethods.waitforElement(wait, obj, "storelocsearchclearicon2");
	  	 	WebElement storelocsearchclearicon2 = BNBasicCommonMethods.findElement(driver, obj, "storelocsearchclearicon2");
	  	 	storelocsearchclearicon2.click();
	  	 	BNBasicCommonMethods.waitforElement(wait, obj, "storesearchbox1");
	  	 	WebElement storesearchbox1 = BNBasicCommonMethods.findElement(driver, obj, "storesearchbox1");
	  		if(storesearchbox1.isDisplayed())
	  		{
	  			pass(" Empty Search Box is found");
	  			storesearchbox1.click();
	  			searchboxwithval4.sendKeys(Keys.ENTER);
	  			BNBasicCommonMethods.waitforElement(wait, obj, "PDPcontainer"); 
	  			WebElement PDPcontainer = BNBasicCommonMethods.findElement(driver, obj, "PDPcontainer");
	  	    if(storesearchbox1.isDisplayed())
			{
	  	    	pass(" On tapping the X close icon in the search field,the entered character is cleared ");	
	  	 	}
	  	 	else
	  	 	{
	  	 		fail("On tapping the X close icon in the search field,the entered character should be cleared");
	  	 	}
	  		}
	  		else
	  	 	{
	  	 		fail("Still Search box is found with value ");
	  	 	}*/
	  	}
	  	
	  	catch(Exception e)
	 	{
	     	 e.getMessage();
	 			System.out.println("Something went wrong"+e.getMessage());
	 			exception(e.getMessage());
	 	}
	  	
	 }  

	 /*BNIA636 - Verify that "Store Information" page should have Search field*/
	public void BNIA636() throws Exception 
	 {
	   ChildCreation("BNIA636 - Verify that Store Information page should contain Search field.");
	  	try
	  	{
	  		if(StoreLocatorFlag622)
	  		{
	  			pass(" Store locator page contains Search field ");	
	  	 	}
	  	 	else
	  	 	{
	  	 		fail("Store locator page does not contains Search field ");
	  	 	}
	  		
	  		
	  		
	  		/*BNBasicCommonMethods.waitforElement(wait, obj, "storelocsearchicon");
	  	 	WebElement storelocsearchicon = BNBasicCommonMethods.findElement(driver, obj, "storelocsearchicon");
	  	 	WebElement Inlinetext = BNBasicCommonMethods.findElement(driver, obj, "InlineText");
	  	 	String inline = Inlinetext.getAttribute("placeholder");
	  	 	System.out.println(""+inline);
	  	 	String text = "Enter a store name, location, or zipcode ";
	  	 	if(inline.equals(text))
	  	 	{
	  	 		pass("Inline text is showing fine now");
	  	 	}
	  	 	else
	  	 	{
	  	 		fail("inline text is not shown");
	  	 	}
	  	    if(storelocsearchicon.isDisplayed())
			{
	  	    	pass(" Store locator page contains Search field ");	
	  	 	}
	  	 	else
	  	 	{
	  	 		fail("Store locator page does not contains Search field ");
	  	 	}*/
	  	}
	  	catch(Exception e)
	 	{
	     	 e.getMessage();
	 			System.out.println("Something went wrong"+e.getMessage());
	 			exception(e.getMessage());
	 	}
	  	
	  }
	
	/*BNIA635 - Verify that "Store Information" page should display certain contents & it should match with the creatives.*/
	public void BNIA635() throws Exception 
	   {
	  	ChildCreation("BNIA635 - Verify that Store Information page should display certain contents & it should match with the creatives.");
	  	try
	  	{
	  		sheet = BNBasicCommonMethods.excelsetUp("storelocator");
			String title = BNBasicCommonMethods.getExcelVal("BNIA639", sheet,8);
			String Title_Values[] = title.split("\n");
			sheet = BNBasicCommonMethods.excelsetUp("storelocator");
			String size = BNBasicCommonMethods.getExcelVal("BNIA639", sheet,9);
			String Size_Values[] = size.split("\n");
			sheet = BNBasicCommonMethods.excelsetUp("storelocator");
			String color = BNBasicCommonMethods.getExcelVal("BNIA639", sheet,10);
			String Color_Values[] = color.split("\n");
			
			
			//Store name
			String Prodtitlename_font = Title_Values[0];
			String Prodtitle_size = Size_Values[0];
			String Prodtitlename_color = Color_Values[0];
			BNBasicCommonMethods.waitforElement(wait, obj, "StoreName");
	  	 	WebElement StoreName = BNBasicCommonMethods.findElement(driver, obj, "StoreName");
			String Storelocatorprdtitle_font = StoreName.getCssValue("font-family");
			String Storelocatorprodtitle_size = StoreName.getCssValue("font-size");
			//System.out.println(""+ Storelocatorprdtitle_font);
			//System.out.println(""+Storelocatorprodtitle_size);
			String Storelocatorprodtitle_color = StoreName.getCssValue("color");
			Color Storelocatorprodtitlecolor = Color.fromString(Storelocatorprodtitle_color);
			String hexStorelocatorprodtitlecolors = Storelocatorprodtitlecolor.asHex();
			//System.out.println(""+hexStorelocatorprodtitlecolors);
			
			if((Storelocatorprdtitle_font.contains(Prodtitlename_font)) && (Storelocatorprodtitle_size.equals(Prodtitle_size)) && (hexStorelocatorprodtitlecolors.equals(Prodtitlename_color)))
			{
				log.add("The Store Title Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font +Storelocatorprodtitle_size +hexStorelocatorprodtitlecolors +"Expected Store font name, size and color is "+Prodtitlename_font +Prodtitle_size +Prodtitlename_color);
				pass("The store Title font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The Store Title Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font +Storelocatorprodtitle_size +hexStorelocatorprodtitlecolors +"Expected Store font name, size and color is "+Prodtitlename_font +Prodtitle_size +Prodtitlename_color);
				fail("The Store Title font, font size, font color is not as per the creative",log);
			}
			
			
			//Store address
			String Prodtitlename_font01 = Title_Values[2];
			String Prodtitle_size01 = Size_Values[2];
			String Prodtitlename_color01 = Color_Values[0];
			BNBasicCommonMethods.waitforElement(wait, obj, "StoreAddress");
	  	 	WebElement StoreAddress = BNBasicCommonMethods.findElement(driver, obj, "StoreAddress");
			String Storelocatorprdtitle_font01 = StoreAddress.getCssValue("font-family");
			String Storelocatorprodtitle_size01 = StoreAddress.getCssValue("font-size");
			//System.out.println(""+ Storelocatorprdtitle_font01);
			//System.out.println(""+Storelocatorprodtitle_size01);
			String Storelocatorprodtitle_color01 = StoreAddress.getCssValue("color");
			Color Storelocatorprodtitlecolor01 = Color.fromString(Storelocatorprodtitle_color01);
			String hexStorelocatorprodtitlecolors01 = Storelocatorprodtitlecolor01.asHex();
			//System.out.println(""+hexStorelocatorprodtitlecolors01);
			
			if((Storelocatorprdtitle_font01.contains(Prodtitlename_font01)) && (Storelocatorprodtitle_size01.equals(Prodtitle_size01)) && (hexStorelocatorprodtitlecolors01.equals(Prodtitlename_color01)))
			{
				log.add("The Store Address Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font01 +Storelocatorprodtitle_size01 +hexStorelocatorprodtitlecolors01 +"Expected Store font name, size and color is "+Prodtitlename_font01 +Prodtitle_size01 +Prodtitlename_color01);
				pass("The store Address font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The Store Address Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font01 +Storelocatorprodtitle_size01 +hexStorelocatorprodtitlecolors01 +"Expected Store font name, size and color is "+Prodtitlename_font01 +Prodtitle_size01 +Prodtitlename_color01);
				fail("The Store Address font, font size, font color is not as per the creative",log);
			}
			
			
			//Store maps & directions
			String Prodtitlename_font02 = Title_Values[1];
			String Prodtitle_size02 = Size_Values[1];
			String Prodtitlename_color02 = Color_Values[1];
			BNBasicCommonMethods.waitforElement(wait, obj, "Storemapdir");
	  	 	WebElement Storemapdir = BNBasicCommonMethods.findElement(driver, obj, "Storemapdir");
			String Storelocatorprdtitle_font02 = Storemapdir.getCssValue("font-family");
			String Storelocatorprodtitle_size02 = Storemapdir.getCssValue("font-size");
			//System.out.println(""+ Storelocatorprdtitle_font02);
			//System.out.println(""+Storelocatorprodtitle_size02);
			String Storelocatorprodtitle_color02 = Storemapdir.getCssValue("color");
			Color Storelocatorprodtitlecolor02 = Color.fromString(Storelocatorprodtitle_color02);
			String hexStorelocatorprodtitlecolors02 = Storelocatorprodtitlecolor02.asHex();
			//System.out.println(""+hexStorelocatorprodtitlecolors02);
			
			if((Storelocatorprdtitle_font02.contains(Prodtitlename_font02)) && (Storelocatorprodtitle_size02.equals(Prodtitle_size02)) && (hexStorelocatorprodtitlecolors02.equals(Prodtitlename_color02)))
			{
				log.add("The Store Map & Direction link Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font02 +Storelocatorprodtitle_size02 +hexStorelocatorprodtitlecolors02 +"Expected Store font name, size and color is "+Prodtitlename_font02 +Prodtitle_size02 +Prodtitlename_color02);
				pass("The Store Map & Direction link font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The Store Map & Direction link Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font02 +Storelocatorprodtitle_size02 +hexStorelocatorprodtitlecolors02 +"Expected Store font name, size and color is "+Prodtitlename_font02 +Prodtitle_size02 +Prodtitlename_color02);
				fail("The Store Map & Direction link font, font size, font color is not as per the creative",log);
			}
			
			
			//Store features
			String Prodtitlename_font1 = Title_Values[1];
			String Prodtitle_size1 = Size_Values[1];
			String Prodtitlename_color1 = Color_Values[0];
			BNBasicCommonMethods.waitforElement(wait, obj, "StoreFeature");
	  	 	WebElement Storefeature = BNBasicCommonMethods.findElement(driver, obj, "StoreFeature");
			String Storelocatorprdtitle_font1 = Storefeature.getCssValue("font-family");
			String Storelocatorprodtitle_size1 = Storefeature.getCssValue("font-size");
			//System.out.println(""+ Storelocatorprdtitle_font1);
			//System.out.println(""+Storelocatorprodtitle_size1);
			String Storelocatorprodtitle_color1 = Storefeature.getCssValue("color");
			Color Storelocatorprodtitlecolor1 = Color.fromString(Storelocatorprodtitle_color1);
			String hexStorelocatorprodtitlecolors1 = Storelocatorprodtitlecolor1.asHex();
			//System.out.println(""+hexStorelocatorprodtitlecolors1);
			
			if((Storelocatorprdtitle_font1.contains(Prodtitlename_font1)) && (Storelocatorprodtitle_size1.equals(Prodtitle_size1)) && (hexStorelocatorprodtitlecolors1.equals(Prodtitlename_color1)))
			{
				log.add("The Store Feautre Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font1 +Storelocatorprodtitle_size1 +hexStorelocatorprodtitlecolors1 +"Expected Store font name, size and color is "+Prodtitlename_font1 +Prodtitle_size1 +Prodtitlename_color1);
				pass("The store Feautre font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The Store Feature Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font1 +Storelocatorprodtitle_size1 +hexStorelocatorprodtitlecolors1 +"Expected Store font name, size and color is "+Prodtitlename_font1 +Prodtitle_size1 +Prodtitlename_color1);
				fail("The Store Feature font, font size, font color is not as per the creative",log);
			}
			
			//Store Hours
			String Prodtitlename_font2 = Title_Values[1];
			String Prodtitle_size2 = Size_Values[1];
			String Prodtitlename_color2 = Color_Values[0];
			BNBasicCommonMethods.waitforElement(wait, obj, "StoreHours");
	  	 	WebElement StoreHours = BNBasicCommonMethods.findElement(driver, obj, "StoreHours");
			String Storelocatorprdtitle_font2 = StoreHours.getCssValue("font-family");
			String Storelocatorprodtitle_size2 = StoreHours.getCssValue("font-size");
			//System.out.println(""+ Storelocatorprdtitle_font2);
			//System.out.println(""+Storelocatorprodtitle_size2);
			String Storelocatorprodtitle_color2 = StoreHours.getCssValue("color");
			Color Storelocatorprodtitlecolor2 = Color.fromString(Storelocatorprodtitle_color2);
			String hexStorelocatorprodtitlecolors2 = Storelocatorprodtitlecolor2.asHex();
			//System.out.println(""+hexStorelocatorprodtitlecolors2);
			
			if((Storelocatorprdtitle_font2.contains(Prodtitlename_font2)) && (Storelocatorprodtitle_size2.equals(Prodtitle_size2)) && (hexStorelocatorprodtitlecolors2.equals(Prodtitlename_color2)))
			{
				log.add("The Store Hours Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font2 +Storelocatorprodtitle_size2 +hexStorelocatorprodtitlecolors2 +"Expected Store font name, size and color is "+Prodtitlename_font2 +Prodtitle_size2 +Prodtitlename_color2);
				pass("The Store Hours font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The Store Hours Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font2 +Storelocatorprodtitle_size2 +hexStorelocatorprodtitlecolors2 +"Expected Store font name, size and color is "+Prodtitlename_font2 +Prodtitle_size2 +Prodtitlename_color2);
				fail("The Store Hours font, font size, font color is not as per the creative",log);
			}
			
			/*//Store pagination arrow left 
			String Prodtitlename_color4 = Color_Values[0];
			BNBasicCommonMethods.waitforElement(wait, obj, "StoreName");
	  	 	WebElement StoreHours = BNBasicCommonMethods.findElement(driver, obj, "StoreHours");
			String Storelocatorprodtitle_color2 = StoreHours.getCssValue("color");
			Color Storelocatorprodtitlecolor2 = Color.fromString(Storelocatorprodtitle_color2);
			String hexStorelocatorprodtitlecolors2 = Storelocatorprodtitlecolor2.asHex();
			System.out.println(""+hexStorelocatorprodtitlecolors2);
			
			if((hexStorelocatorprodtitlecolors2.equals(Prodtitlename_color2)))
			{
				log.add("The Store pagination left arrow color is "+"Current color "+hexStorelocatorprodtitlecolors2 +"Expected Store left arrow color is "+Prodtitlename_color2);
				pass("The Store pagination left arrow color is as per the creative",log);
			}
			else
			{
				log.add("The Store pagination left arrow color is "+"Current color "+hexStorelocatorprodtitlecolors2 +"Expected Store left arrow color is "+Prodtitlename_color2);
				fail("Store pagination left arrow color is not as per the creative",log);
			}*/
			
			//Store pagination arrow right 
			String Prodtitlename_color5 = Color_Values[1];
			BNBasicCommonMethods.waitforElement(wait, obj, "Storepagerightarrow");
	  	 	WebElement Storepagerightarrow = BNBasicCommonMethods.findElement(driver, obj, "Storepagerightarrow");
			String Storelocatorprodtitle_color5 = Storepagerightarrow.getCssValue("color");
			Color Storelocatorprodtitlecolor5 = Color.fromString(Storelocatorprodtitle_color5);
			String hexStorelocatorprodtitlecolors5 = Storelocatorprodtitlecolor5.asHex();
			//System.out.println(""+hexStorelocatorprodtitlecolors5);
			
			if((hexStorelocatorprodtitlecolors5.equals(Prodtitlename_color5)))
			{
				log.add("The Store pagination right arrow color is "+"Current color "+hexStorelocatorprodtitlecolors5 + "Expected right arrow color is " +Prodtitlename_color5);
				pass("The Store pagination right arrow color is as per the creative",log);
			}
			else
			{
				log.add("The Store pagination right arrow color is "+"Current color " +hexStorelocatorprodtitlecolors2 +"Expected color is "+Prodtitlename_color5);
				fail("Store pagination right arrow color is not as per the creative",log);
			}
			
			
			//Store pagination selected
			String Prodtitlename_font6 = Title_Values[2];
			String Prodtitle_size6 = Size_Values[1];
			String Prodtitlename_color6 = Color_Values[0];
			BNBasicCommonMethods.waitforElement(wait, obj, "Storepaginationactive");
	  	 	WebElement Storepaginationactive = BNBasicCommonMethods.findElement(driver, obj, "Storepaginationactive");
			String Storelocatorprdtitle_font6 = Storepaginationactive.getCssValue("font-family");
			String Storelocatorprodtitle_size6 = Storepaginationactive.getCssValue("font-size");
			//System.out.println(""+ Storelocatorprdtitle_font6);
			//System.out.println(""+Storelocatorprodtitle_size6);
			String Storelocatorprodtitle_color6 = StoreHours.getCssValue("color");
			Color Storelocatorprodtitlecolor6 = Color.fromString(Storelocatorprodtitle_color6);
			String hexStorelocatorprodtitlecolors6 = Storelocatorprodtitlecolor6.asHex();
			//System.out.println(""+hexStorelocatorprodtitlecolors6);
			
			if((Storelocatorprdtitle_font6.contains(Prodtitlename_font6)) && (Storelocatorprodtitle_size6.equals(Prodtitle_size6)) && (hexStorelocatorprodtitlecolors6.equals(Prodtitlename_color6)))
			{
				log.add("The Store pagination selected Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font6 +Storelocatorprodtitle_size6 +hexStorelocatorprodtitlecolors6 +"Expected Store font name, size and color is "+Prodtitlename_font6 +Prodtitle_size6 +Prodtitlename_color6);
				pass("The Store pagination selected font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The Store pagination selected Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font6 +Storelocatorprodtitle_size6 +hexStorelocatorprodtitlecolors6 +"Expected Store font name, size and color is "+Prodtitlename_font6 +Prodtitle_size6 +Prodtitlename_color6);
				fail("The Store pagination selected  font, font size, font color is not as per the creative",log);
			}
			
			
			//Store pagination not selected 
			String Prodtitlename_font7 = Title_Values[2];
			String Prodtitle_size7 = Size_Values[1];
			String Prodtitlename_color7 = Color_Values[1];
			BNBasicCommonMethods.waitforElement(wait, obj, "Storepaginationnormal");
	  	 	WebElement Storepaginationnormal = BNBasicCommonMethods.findElement(driver, obj, "Storepaginationnormal");
			String Storelocatorprdtitle_font7 = Storepaginationnormal.getCssValue("font-family");
			String Storelocatorprodtitle_size7 = Storepaginationnormal.getCssValue("font-size");
			//System.out.println(""+ Storelocatorprdtitle_font7);
			//System.out.println(""+Storelocatorprodtitle_size7);
			String Storelocatorprodtitle_color7 = Storepaginationnormal.getCssValue("color");
			Color Storelocatorprodtitlecolor7 = Color.fromString(Storelocatorprodtitle_color7);
			String hexStorelocatorprodtitlecolors7 = Storelocatorprodtitlecolor7.asHex();
			//System.out.println(""+hexStorelocatorprodtitlecolors7);
			
			if((Storelocatorprdtitle_font7.contains(Prodtitlename_font7)) && (Storelocatorprodtitle_size7.equals(Prodtitle_size7)) && (hexStorelocatorprodtitlecolors7.equals(Prodtitlename_color7)))
			{
				log.add("The Store pagination normal Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font7 +Storelocatorprodtitle_size7 +hexStorelocatorprodtitlecolors7 +"Expected Store font name, size and color is "+Prodtitlename_font7 +Prodtitle_size7 +Prodtitlename_color7);
				pass("The Store pagination normal font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The Store pagination normal Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font7 +Storelocatorprodtitle_size7 +hexStorelocatorprodtitlecolors7 +"Expected Store font name, size and color is "+Prodtitlename_font7 +Prodtitle_size7 +Prodtitlename_color7);
				fail("The Store pagination normal font, font size, font color is not as per the creative",log);
			}
			
			
			
			
			
			
	  	}
		  	catch(Exception e)
		 	{
		     	 e.getMessage();
		 			System.out.println("Something went wrong"+e.getMessage());
		 			exception(e.getMessage());
		 	}
		  	
		   }	

	/*BNIA639 - Verify that  "Store Name", "Store Features, & "Store Hours" text should be highlighted as per the creatives.*/
    public void BNIA639() throws Exception 
	   {
	  	ChildCreation("BNIA639 - Verify that  Store Name, Store Features, & Store Hours text should be highlighted as per the creatives..");
	  	try
	  	{
	  		sheet = BNBasicCommonMethods.excelsetUp("storelocator");
			String title = BNBasicCommonMethods.getExcelVal("BNIA639", sheet,8);
			String Title_Values[] = title.split("\n");
			sheet = BNBasicCommonMethods.excelsetUp("storelocator");
			String size = BNBasicCommonMethods.getExcelVal("BNIA639", sheet,9);
			String Size_Values[] = size.split("\n");
			sheet = BNBasicCommonMethods.excelsetUp("storelocator");
			String color = BNBasicCommonMethods.getExcelVal("BNIA639", sheet,10);
			String Color_Values[] = color.split("\n");
			//Store name
			String Prodtitlename_font = Title_Values[0];
			String Prodtitle_size = Size_Values[0];
			String Prodtitlename_color = Color_Values[0];
			BNBasicCommonMethods.waitforElement(wait, obj, "StoreName");
	  	 	WebElement StoreName = BNBasicCommonMethods.findElement(driver, obj, "StoreName");
			String Storelocatorprdtitle_font = StoreName.getCssValue("font-family");
			String Storelocatorprodtitle_size = StoreName.getCssValue("font-size");
			//System.out.println(""+ Storelocatorprdtitle_font);
			//System.out.println(""+Storelocatorprodtitle_size);
			String Storelocatorprodtitle_color = StoreName.getCssValue("color");
			Color Storelocatorprodtitlecolor = Color.fromString(Storelocatorprodtitle_color);
			String hexStorelocatorprodtitlecolors = Storelocatorprodtitlecolor.asHex();
			//System.out.println(""+hexStorelocatorprodtitlecolors);
			
			if((Storelocatorprdtitle_font.contains(Prodtitlename_font)) && (Storelocatorprodtitle_size.equals(Prodtitle_size)) && (hexStorelocatorprodtitlecolors.equals(Prodtitlename_color)))
			{
				log.add("The Store Name Title Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font +Storelocatorprodtitle_size +hexStorelocatorprodtitlecolors +"Expected Store font name, size and color is "+Prodtitlename_font +Prodtitle_size +Prodtitlename_color);
				pass("The store Name Title font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The Store Name Title Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font +Storelocatorprodtitle_size +hexStorelocatorprodtitlecolors +"Expected Store font name, size and color is "+Prodtitlename_font +Prodtitle_size +Prodtitlename_color);
				fail("The Store Name Title font, font size, font color is not as per the creative",log);
			}
			
			//Store features
			String Prodtitlename_font1 = Title_Values[1];
			String Prodtitle_size1 = Size_Values[1];
			String Prodtitlename_color1 = Color_Values[0];
			BNBasicCommonMethods.waitforElement(wait, obj, "StoreName");
	  	 	WebElement Storefeature = BNBasicCommonMethods.findElement(driver, obj, "StoreFeature");
			String Storelocatorprdtitle_font1 = Storefeature.getCssValue("font-family");
			String Storelocatorprodtitle_size1 = Storefeature.getCssValue("font-size");
			//System.out.println(""+ Storelocatorprdtitle_font1);
			//System.out.println(""+Storelocatorprodtitle_size1);
			String Storelocatorprodtitle_color1 = Storefeature.getCssValue("color");
			Color Storelocatorprodtitlecolor1 = Color.fromString(Storelocatorprodtitle_color1);
			String hexStorelocatorprodtitlecolors1 = Storelocatorprodtitlecolor1.asHex();
			//System.out.println(""+hexStorelocatorprodtitlecolors1);
			
			if((Storelocatorprdtitle_font1.contains(Prodtitlename_font1)) && (Storelocatorprodtitle_size1.equals(Prodtitle_size1)) && (hexStorelocatorprodtitlecolors1.equals(Prodtitlename_color1)))
			{
				log.add("The Store Feature Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font1 +Storelocatorprodtitle_size1 +hexStorelocatorprodtitlecolors1 +"Expected Store font name, size and color is "+Prodtitlename_font1 +Prodtitle_size1 +Prodtitlename_color1);
				pass("The store Feautre font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The Store Feature Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font1 +Storelocatorprodtitle_size1 +hexStorelocatorprodtitlecolors1 +"Expected Store font name, size and color is "+Prodtitlename_font1 +Prodtitle_size1 +Prodtitlename_color1);
				fail("The Store Feature font, font size, font color is not as per the creative",log);
			}
			
			//Store Hours
			String Prodtitlename_font2 = Title_Values[1];
			String Prodtitle_size2 = Size_Values[1];
			String Prodtitlename_color2 = Color_Values[0];
			BNBasicCommonMethods.waitforElement(wait, obj, "StoreName");
	  	 	WebElement StoreHours = BNBasicCommonMethods.findElement(driver, obj, "StoreHours");
			String Storelocatorprdtitle_font2 = StoreHours.getCssValue("font-family");
			String Storelocatorprodtitle_size2 = StoreHours.getCssValue("font-size");
			//System.out.println(""+ Storelocatorprdtitle_font2);
			//System.out.println(""+Storelocatorprodtitle_size2);
			String Storelocatorprodtitle_color2 = StoreHours.getCssValue("color");
			Color Storelocatorprodtitlecolor2 = Color.fromString(Storelocatorprodtitle_color2);
			String hexStorelocatorprodtitlecolors2 = Storelocatorprodtitlecolor2.asHex();
			//System.out.println(""+hexStorelocatorprodtitlecolors2);
			
			if((Storelocatorprdtitle_font2.contains(Prodtitlename_font2)) && (Storelocatorprodtitle_size2.equals(Prodtitle_size2)) && (hexStorelocatorprodtitlecolors2.equals(Prodtitlename_color2)))
			{
				log.add("The Store Hour Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font2 +Storelocatorprodtitle_size2 +hexStorelocatorprodtitlecolors2 +"Expected Store font name, size and color is "+Prodtitlename_font2 +Prodtitle_size2 +Prodtitlename_color2);
				pass("The store Hour font, font size, font color is as per the creative",log);
			}
			else
			{
				log.add("The Store Hour Text font, size and color is "+"Current font name, size and color "+Storelocatorprdtitle_font2 +Storelocatorprodtitle_size2 +hexStorelocatorprodtitlecolors2 +"Expected Store font name, size and color is "+Prodtitlename_font2 +Prodtitle_size2 +Prodtitlename_color2);
				fail("The Store Hour font, font size, font color is not as per the creative",log);
			}
			
			
			
	  	}
		  	catch(Exception e)
		 	{
		     	 e.getMessage();
		 			System.out.println("Something went wrong"+e.getMessage());
		 			exception(e.getMessage());
		 	}
		  	
	}
    
    /*BNIA-640 - Verify that Store image should be shown in store results page */
	public void BNIA640() throws Exception 
	   {
	  	ChildCreation("BNIA640 - Verify that Store image should be shown in store results page");
	  	try
	  	{
	  		if(StoreLocatorFlag1971)
	  		{
	  			pass("  Store image is showing in store results page ");
	  	 	}
	  	 	else
	  	 	{
	  	 		fail(" Store image is not showing in store results page ");
	  	 	}
	  		
	  		
	  		/*BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultsimg");
	  	 	WebElement storesearchresultsimg = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultsimg");
	  	 	if(storesearchresultsimg.isDisplayed())
	  	 	{
	  	 		pass("  Store image is showing in store results page ");
	  	 	}
	  	 	else
	  	 	{
	  	 		fail(" Store image is not showing in store results page ");
	  	 	}*/
	  	}
	  	catch(Exception e)
	 	{
	     	 e.getMessage();
	 			System.out.println("Something went wrong"+e.getMessage());
	 			exception(e.getMessage());
	 	}
	  	
	   }    
	
	/*BNIA-638 - Verify that store location icon should shown in store results page.. */
	public void BNIA638() throws Exception 
	  {
	  	ChildCreation("BNIA638 - Verify that store location icon should shown in store results page.");
	  	try
	  	{
	  		if(StoreLocatorFlag1973)
	  		{
	  			pass("  Store location icon is showing in store results page ");
	  	 	}
	  	 	else
	  	 	{
	  	 		fail(" Store location icon is not showing in store results page ");
	  	 	}
	  		
	  		
	  		/*BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultsicon");
	  	 	WebElement storesearchresultsicon = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultsicon");
	  	 	if(storesearchresultsicon.isDisplayed())
	  	 	{
	  	 		pass("  Store location icon is showing in store results page ");
	  	 	}
	  	 	else
	  	 	{
	  	 		fail(" Store location icon is not showing in store results page ");
	  	 	}*/
	  	}
	  	catch(Exception e)
	 	{
	     	 e.getMessage();
	 			System.out.println("Something went wrong"+e.getMessage());
	 			exception(e.getMessage());
	 	}
	  	
	  }	

	/*BNIA-637 - Verify that store results count should shown in store results page. */
	public void BNIA637() throws Exception 
	  {
	  	ChildCreation("BNIA637 - Verify that store results count should shown in store results page.");
	  	try
	  	{
	  		if(StoreLocatorFlag1974)
	  		{
	  			pass("  Store results counts are  showing in store results page ");
	  	 	}
	  	 	else
	  	 	{
	  	 		fail(" Store results counts are not showing in store results page ");
	  	 	}
	  		
	  		
	  		/*BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultslocicon");
	  	 	WebElement storesearchresultslocicon = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultslocicon");
	  	 	if(storesearchresultslocicon.isDisplayed())
	  	 	{
	  	 		pass("  Store results counts are  showing in store results page ");
	  	 	}
	  	 	else
	  	 	{
	  	 		fail(" Store results counts are not showing in store results page ");
	  	 	}*/
	  	}
	  	catch(Exception e)
	 	{
	     	 e.getMessage();
	 			System.out.println("Something went wrong"+e.getMessage());
	 			exception(e.getMessage());
	 	}
	  	
	  }
	
	/*BNIA643 - Verify that Store Information page should contain header & its icons*/
	public void BNIA643() throws Exception 
	 {
	  	ChildCreation("BNIA643 - Verify that Store Information page should contain header & its icons");
	  	try
	  	{
	  		if(StoreLocatorFlag1969)
	  		{
	  			pass("Store Information page contains header & its icons(Back button, Pancake, Home page logo, Search, Scan & Bag Icons ");
			}
	  	 	else 
	  	 	{
	  	 		fail("Store Information page does not contains header & its icons ");
	  	 	}
	  		
	  		
	  		
	  		/*BNBasicCommonMethods.waitforElement(wait, obj, "storepageheader");
	  	 	WebElement storepageheader = BNBasicCommonMethods.findElement(driver, obj, "storepageheader");
	  	    if(storepageheader.isDisplayed())
			{
	  	    	pass(" Store Information page contains header ");
	  	    	BNBasicCommonMethods.waitforElement(wait, obj, "storepageheaderpancake");
		  	 	WebElement storepageheaderpancake = BNBasicCommonMethods.findElement(driver, obj, "storepageheaderpancake");
		  	 	boolean storepageheaderpancake1 = BNBasicCommonMethods.isElementPresent(storepageheaderpancake);
		  	 	BNBasicCommonMethods.waitforElement(wait, obj, "storepageheaderbck");
		  	 	WebElement storepageheaderbck = BNBasicCommonMethods.findElement(driver, obj, "storepageheaderbck");
		  	 	boolean storepageheaderbck1 = BNBasicCommonMethods.isElementPresent(storepageheaderbck);
		  	 	BNBasicCommonMethods.waitforElement(wait, obj, "storepageheaderlogo");
		  	 	WebElement storepageheaderlogo = BNBasicCommonMethods.findElement(driver, obj, "storepageheaderlogo");
		  	 	boolean storepageheaderlogo1 = BNBasicCommonMethods.isElementPresent(storepageheaderlogo);
		  	 	BNBasicCommonMethods.waitforElement(wait, obj, "storepageheadersearch");
		  	 	WebElement storepageheadersearch = BNBasicCommonMethods.findElement(driver, obj, "storepageheadersearch");
		  	 	boolean storepageheadersearch1 = BNBasicCommonMethods.isElementPresent(storepageheadersearch);
		  	 	BNBasicCommonMethods.waitforElement(wait, obj, "storepageheaderscan");
		  	 	WebElement storepageheaderscan = BNBasicCommonMethods.findElement(driver, obj, "storepageheaderscan");
		  	 	boolean storepageheaderscan1 = BNBasicCommonMethods.isElementPresent(storepageheaderscan);
		  	 	BNBasicCommonMethods.waitforElement(wait, obj, "storepageheaderbag");
		  	 	WebElement storepageheaderbag = BNBasicCommonMethods.findElement(driver, obj, "storepageheaderbag");
		  	 	boolean storepageheaderbag1 = BNBasicCommonMethods.isElementPresent(storepageheaderbag);
		  	 	if(storepageheaderpancake1 && storepageheaderbck1 && storepageheaderlogo1 && storepageheadersearch1 && storepageheaderscan1 && storepageheaderbag1)
				{
		  	 		pass("Store Information page contains header & its icons(Back button, Pancake, Home page logo, Search, Scan & Bag Icons ");
				}
		  	 	else 
		  	 	{
		  	 		fail("Store Information page does not contains header & its icons ");
		  	 	}
	  	 	}
	  	 	else
	  	 	{
	  	 		fail("Store Information page does not contains header");
	  	 	}*/
	  	}
	  	catch(Exception e)
	 	{
	     	 e.getMessage();
	 			System.out.println("Something went wrong"+e.getMessage());
	 			exception(e.getMessage());
	 	}
	  	
	 }	

	/*BNIA-652 - Verify that pagination should shown in store results page. */
    public void BNIA652() throws Exception 
	   {
	  	ChildCreation("BNIA652 - Verify that pagination should shown in store results page.");
	  	try
	  	{
	  		if(StoreLocatorFlag1976)
	  		{
	  			pass("  Pagination is showing in store results page ");
	  	 	}
	  	 	else
	  	 	{
	  	 		fail(" Pagination is not showing in store results page ");
	  	 	}
	  		
	  		
	  		/*BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultpagination");
	  	 	WebElement storesearchresultpagination = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultpagination");
	  	 	if(storesearchresultpagination.isDisplayed())
			{
	  	 		pass("  Pagination is showing in store results page ");
	  	 	}
	  	 	else
	  	 	{
	  	 		fail(" Pagination is not showing in store results page ");
	  	 	}*/
	  	}
	  	catch(Exception e)
	 	{
	     	 e.getMessage();
	 			System.out.println("Something went wrong"+e.getMessage());
	 			exception(e.getMessage());
	 	}
	  	
	   }	

    /*BNIA-653 - Verify that pagination between the pages should works in store results page. */
	public void BNIA653() throws Exception 
	   {
	  	ChildCreation("BNIA653 - Verify that pagination between the pages should works in store results page.");
	  	try
	  	{
	  		if(StoreLocatorFlag1985)
	  		{
	  			pass("  Pagination buttons are working in store results page ");
	  	 	}
	  	 	else
	  	 	{
	  	 		fail(" Pagination buttons are not working showing in store results page ");
	  	 	}
	  		
	  		
	  		/*BNBasicCommonMethods.waitforElement(wait, obj, "storeresultspage2");
	  	 	WebElement storeresultspage2 = BNBasicCommonMethods.findElement(driver, obj, "storeresultspage2");
	  	 	if(storeresultspage2.isDisplayed())
			{
	  	 		storeresultspage2.click();
	  	 		pass("  Pagination buttons are working in store results page ");
	  	 	}
	  	 	else
	  	 	{
	  	 		fail(" Pagination buttons are not working showing in store results page ");
	  	 	}
	  	 	BNBasicCommonMethods.waitforElement(wait, obj, "storeresultspage1");
	  	 	WebElement storeresultspage1 = BNBasicCommonMethods.findElement(driver, obj, "storeresultspage1");
	  	 	Thread.sleep(1000);
	  	 	storeresultspage1.click();*/
	  	}
	  	catch(Exception e)
	 	{
	     	 e.getMessage();
	 			System.out.println("Something went wrong"+e.getMessage());
	 			exception(e.getMessage());
	 	}
	  	
	}
	   
	/*BNIA-654 - Verify that the selected number should be highlighted in the pagination. */
	public void BNIA654() throws Exception 
	 {
	  	ChildCreation("BNIA654 - Verify that the selected page number should be highlighted in the pagination.");
	  	try
	  	{
	  		BNBasicCommonMethods.waitforElement(wait, obj, "storeresultspageactive");
	  	 	WebElement storeresultspageactive = BNBasicCommonMethods.findElement(driver, obj, "storeresultspageactive");
	  	 	if(storeresultspageactive.isDisplayed())
			{
	  	 		pass("  Selected page number is highlighted in the pagination ");
	  	 	}
	  	 	else
	  	 	{
	  	 		fail(" Selected page number is not highlighted in the pagination ");
	  	 	}
	  	 	
	  	}
	  	catch(Exception e)
	 	{
	     	 e.getMessage();
	 			System.out.println("Something went wrong"+e.getMessage());
	 			exception(e.getMessage());
	 	}
	  	
	 }
	
	/*BNIA-641 - Verify that MAPS overlay should be shown in store results page on selecting maps & directions link.*/
    public void BNIA641() throws Exception 
	  {
	  	ChildCreation("BNIA641 - Verify that MAPS overlay should be shown in store results page on selecting maps & directions link.");
	  	try
	  	{
	  		if(StoreLocatorFlag1978)
	  		{
	  			pass("MAPS overlay is showing in store results page on selecting maps & directions link");
  	 	    }
  	 	   else
  	 	    {
  	 		   fail("MAPS overlay is not shown in store results page on selecting maps & directions link");
  	 	   }
	  		
	  		
	  		/*BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultmapsdrctn");
	  	 	WebElement storesearchresultmapsdrctn = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultmapsdrctn");
	  	 	if(storesearchresultmapsdrctn.isDisplayed())
			{
	  	 		storesearchresultmapsdrctn.click();
	  	 		pass("MAPS & direction link is clicked");
	  	 		BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultmapoverlay");
		  	 	WebElement storesearchresultmapoverlay = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultmapoverlay");
		  	 	if(storesearchresultmapoverlay.isDisplayed())
		  	 	{
	  	 		pass("MAPS overlay is showing in store results page on selecting maps & directions link");
	  	 	    }
	  	 	   else
	  	 	    {
	  	 		fail("MAPS overlay is not shown in store results page on selecting maps & directions link");
	  	 	   }
			}
		  	else
	  	 	{
	  	 		fail("MAPS & direction link is not working");
	  	 	}*/
	  	}
	  	catch(Exception e)
	 	{
	     	 e.getMessage();
	 			System.out.println("Something went wrong"+e.getMessage());
	 			exception(e.getMessage());
	 	}
	  	
	  }
    
    public void BNIA1980A() throws Exception 
	{
		ChildCreation("BNIA1980 - Verify that MAPS overlay should consists of A - soruce & B - destination text fields.");
		try
		{
			if(StoreLocatorFlag1980)
			{
				pass("MAPS overlay consists of A - soruce & B - destination text fields");
		 	}
		 	else
		 	{
		 		fail("MAPS overlay doesnot consists of A - soruce & B - destination text fields");
		 	}
			
			
			/*Thread.sleep(1500);
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultmapsdrctn");
			WebElement mapsdirectionlink = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultmapsdrctn");
			
			Thread.sleep(1500);
			mapsdirectionlink.click();
			Thread.sleep(1500);
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultmapsA");
			WebElement storesearchresultmapsA = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultmapsA");
		 	boolean storesearchresultmapsAa = BNBasicCommonMethods.isElementPresent(storesearchresultmapsA);
		 	BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultmapsB");
		 	WebElement storesearchresultmapsB = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultmapsB");
		 	boolean storesearchresultmapsBb = BNBasicCommonMethods.isElementPresent(storesearchresultmapsB);
		 	if(storesearchresultmapsAa&&storesearchresultmapsBb)
		 	{
		 		StoreLocatorFlag1980 = true;
		 		pass("MAPS overlay consists of A - soruce & B - destination text fields");
		 	}
		 	else
		 	{
		 		StoreLocatorFlag1980 = false;
		 		fail("MAPS overlay doesnot consists of A - soruce & B - destination text fields");
		 	}*/
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
    
    public void BNIA1981A() throws Exception 
	{
		ChildCreation("BNIA1981 - Verify that MAPS routes & details should be shown on entering valid A soruce & B destinations.");
		try
		{
			
			if(StoreLocatorFlag1981)
			{
				pass("MAPS routes & details are showing on entering valid A soruce & B destinations.\n");	
			}
			else
			{
				fail("MAPS routes & details are not showing on entering valid A soruce & B destinations\n");
			}
			
			/*sheet = BNBasicCommonMethods.excelsetUp("storelocator");
			String SearchKeywordList = BNBasicCommonMethods.getExcelVal("BNIA1981", sheet,2);
			String[] SearchKeyword1 = SearchKeywordList.split("\n");
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultmapsAinputValid");
			WebElement StoreBoxNew =  driver.findElement(By.xpath("//*[@id='storeSearchForm']"));
				
				for(int ctr = 0; ctr < SearchKeyword1.length ; ctr++)
				{
					WebElement storesearchresultmapsAinputValid = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultmapsAinputValid");
					storesearchresultmapsAinputValid.click();
					storesearchresultmapsAinputValid.clear();
					Thread.sleep(500);
					storesearchresultmapsAinputValid.sendKeys(SearchKeyword1[ctr]);
					Thread.sleep(500);
					storesearchresultmapsAinputValid.sendKeys(Keys.ENTER);
					BNBasicCommonMethods.waitforElement(wait, obj, "getdirectionbtn");
				WebElement getdirectionbtn = BNBasicCommonMethods.findElement(driver, obj, "getdirectionbtn");
					getdirectionbtn.click();
					Thread.sleep(2500);
					try
					{
						BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultDir");
						WebElement storesearchresultDir = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultDir");
						if(storesearchresultDir.isDisplayed())
						{
							StoreLocatorFlag1981 = true;
							pass("\n"+SearchKeyword1[ctr]);
							pass("\n"+storesearchresultDir.getText());
							pass("MAPS routes & details are showing on entering valid A soruce & B destinations.\n");	
						}
						else
						{
							StoreLocatorFlag1981 = false;
							fail("MAPS routes & details are not showing on entering valid A soruce & B destinations\n");
						} 
					}
				
			catch(Exception e)
		{
	  	 e.getMessage();
	  	System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
				}*/
	}
		
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
    
    public void BNIA1982A()
	{
		ChildCreation("BNIA1982 - Verify that MAPS routes & details should be shown on entering valid A soruce & B destinations.");
		try
		{
			if(StoreLocatorFlag1982)
			{
				pass("when entering the invalid address or invalid characters or invalid numbers, it displays the alert with RED box.\n");
			}
			else
			{
				fail("when entering the invalid characters or invalid numbers, it does not displays the alert.");
			}
			
			
			/*sheet = BNBasicCommonMethods.excelsetUp("storelocator");
			String SearchKeywordList = BNBasicCommonMethods.getExcelVal("BNIA1982", sheet,2);
			String[] SearchKeyword = SearchKeywordList.split("\n");
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultmapsAinput");
		    WebElement StoreBoxNew =  driver.findElement(By.xpath("//*[@id='storeSearchForm']"));
			
			for(int ctr = 0; ctr < SearchKeyword.length ; ctr++)
			{
				WebElement storesearchresultmapsAinput = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultmapsAinput");
				storesearchresultmapsAinput.click();
				storesearchresultmapsAinput.clear();
				Thread.sleep(500);
				storesearchresultmapsAinput.sendKeys(SearchKeyword[ctr]);
				Thread.sleep(500);
				storesearchresultmapsAinput.sendKeys(Keys.ENTER);
				BNBasicCommonMethods.waitforElement(wait, obj, "getdirectionbtn");
			WebElement getdirectionbtn = BNBasicCommonMethods.findElement(driver, obj, "getdirectionbtn");
				getdirectionbtn.click();
				Thread.sleep(2500);
				try
				{
					BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultgetdirectionerr");
					WebElement storesearchresultgetdirectionerr = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultgetdirectionerr");
					if(storesearchresultgetdirectionerr.isDisplayed())
					{
						StoreLocatorFlag1982 = true;
						pass("\n"+SearchKeyword[ctr]);
						pass("\n"+storesearchresultgetdirectionerr.getText());
						pass("when entering the invalid address or invalid characters or invalid numbers, it displays the alert with RED box.\n");
						
						
					}
					else
					{
						StoreLocatorFlag1982 = false;
						fail("when entering the invalid characters or invalid numbers, it does not displays the alert.");
					}
				}
				catch(Exception e)
			 	{
			     	 e.getMessage();
			     	System.out.println("Something went wrong"+e.getMessage());
	 			exception(e.getMessage());
			 	}
				
			}*/
			
		}
				catch(Exception e)
			 	{
			     	 e.getMessage();
			 			System.out.println("Something went wrong"+e.getMessage());
			 			exception(e.getMessage());
			 	}
			  	
			   }
    
    /*BNIA-1983 -Verify that On selecting back from store results page it should takes to home page. */
	public void BNIA1983A() throws Exception 
	{
		ChildCreation("BNIA1983 - Verify that On selecting back from store results page it should takes to home page. ");
		try
		{
			if(StoreLocatorFlag1983)
			{
				pass(" On selecting back from store results page, home page is displayed. ");
		 	}
		 	else
		 	{
		 		fail(" On selecting back from store results page, it does not takes to home page. ");
		 	}
			
			
			/*BNBasicCommonMethods.waitforElement(wait, obj, "storepagebckbtn");
		 	WebElement storepagebckbtn = BNBasicCommonMethods.findElement(driver, obj, "storepagebckbtn");
		 	storepagebckbtn.click();
		 	Thread.sleep(500);
		 	BNBasicCommonMethods.waitforElement(wait, obj, "Splashscreenagain");
		 	WebElement Splashscreenagain = BNBasicCommonMethods.findElement(driver, obj, "Splashscreenagain");
		 	BNBasicCommonMethods.waitforElement(wait, obj, "Homepageagain");
		 	WebElement Homepageagain = BNBasicCommonMethods.findElement(driver, obj, "Homepageagain");
		 	if(Homepageagain.isDisplayed())
		 	{
		 		StoreLocatorFlag1983 = true;
		 		pass(" On selecting back from store results page, home page is displayed. ");
		 	}
		 	else
		 	{
		 		StoreLocatorFlag1983 = false;
		 		fail(" On selecting back from store results page, it does not takes to home page. ");
		 	}
		 	System.out.println("8/11 - BNIA50_Store_Detail_Information is completed");*/
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	
	/*BNIA1984 - Verify that A source & B destination text fields should consists of inline texts.*/
	public void BNIA1984A() throws Exception 
	{
		ChildCreation("BNIA1984 - Verify that A source & B destination text fields should consists of inline text.");
		try
		{
			if(StoreLocatorFlag1984)
			{
				pass("Inline text is showing fine now");
		 	}
		 	else
		 	{
		 		fail("inline text is not shown");
		 	}
			
			
			/*BNBasicCommonMethods.waitforElement(wait, obj, "InlineTextMap");
		 	WebElement InlineTextMap = BNBasicCommonMethods.findElement(driver, obj, "InlineTextMap");
		 	String inline = InlineTextMap.getAttribute("placeholder");
		 	//System.out.println(""+inline);
		 	String text = "Enter City, State or Zip Code";
		 	if(inline.equals(text))
		 	{
		 		StoreLocatorFlag1984 = true;
		 		pass("Inline text is showing fine now");
		 	}
		 	else
		 	{
		 		StoreLocatorFlag1984 = false;
		 		fail("inline text is not shown");
		 	}*/
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	/*BNIA-1977 - Verify that Store features should be shown in store results page  */
	public void BNIA1977A() throws Exception 
	{
		ChildCreation("BNIA1977 - Verify that Store features should be shown in store results page .");
		try
		{
			if(StoreLocatorFlag1977)
			{
				pass("  Store features are showing in store results page ");
		 	}
		 	else
		 	{
		 		fail(" Store features are not showing in store results page ");
		 	}
			
			
			/*WebElement storesearchresultfeature = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultfeature");
		 	boolean storesearchresultfeature1 = BNBasicCommonMethods.isElementPresent(storesearchresultfeature);
		 	WebElement storesearchresultfeatureitem = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultfeatureitem");
		 	boolean storesearchresultfeatureitem1 = BNBasicCommonMethods.isElementPresent(storesearchresultfeatureitem);
		 	if(storesearchresultfeature1&&storesearchresultfeatureitem1)
		 	{
		 		StoreLocatorFlag1977 = true;
		 		pass("  Store features are showing in store results page ");
		 	}
		 	else
		 	{
		 		StoreLocatorFlag1977 = false;
		 		fail(" Store features are not showing in store results page ");
		 	}*/
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
	
	
	/*BNIA-1978 - Verify that MAPS overlay should be shown in store results page on selecting maps & directions link.*/
	public void BNIA1978A() throws Exception 
	{
		ChildCreation("BNIA1978 - Verify that MAPS overlay should be shown in store results page on selecting maps & directions link.");
		try
		{
			if(StoreLocatorFlag1978)
			{
				pass("MAPS overlay is showing in store results page on selecting maps & directions link");
	 	    }
	 	   else
	 	    {	
	 		   fail("MAPS overlay is not shown in store results page on selecting maps & directions link");
	 	    }
			
			
			/*Thread.sleep(3000);
			BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultmapsdrctn");
		 	WebElement storesearchresultmapsdrctn = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultmapsdrctn");
		 	if(storesearchresultmapsdrctn.isDisplayed())
		{
		 		Thread.sleep(500);
		 		storesearchresultmapsdrctn.click();
		 		pass("MAPS & direction link is clicked");
		 		BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultmapoverlay");
		 	WebElement storesearchresultmapoverlay = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultmapoverlay");
		 	if(storesearchresultmapoverlay.isDisplayed())
		 		{
		 			StoreLocatorFlag1978 = true;
		 			pass("MAPS overlay is showing in store results page on selecting maps & directions link");
		 	    }
		 	   else
		 	    {	StoreLocatorFlag1978 = false;
		 		    fail("MAPS overlay is not shown in store results page on selecting maps & directions link");
		 	    }
		}
		else
		 	{
		 		fail("MAPS & direction link is not working");
		 	}*/
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		Thread.sleep(1000);
		
	}
	
	
	/*BNIA-1979 - Verify that MAPS overlay should be closed in store results page on selecting close button. */
	public void BNIA1979A() throws Exception 
	{
		ChildCreation("BNIA1979 - Verify that MAPS overlay should be closed in store results page on selecting close button.");
		try
		{
			
			if(StoreLocatorFlag1979)
			{
				pass("  MAPS overlay is closing in store results page on selecting close button ");
		 	}
		 	else
		 	{
		 		fail(" MAPS overlay is not closing in store results page on selecting close button ");
		 	}
			
			/*BNBasicCommonMethods.waitforElement(wait, obj, "storesearchresultMapcloseBtn");
		 	WebElement storesearchresultMapcloseBtn = BNBasicCommonMethods.findElement(driver, obj, "storesearchresultMapcloseBtn");
		 	if(storesearchresultMapcloseBtn.isDisplayed())
		 	{
		 		storesearchresultMapcloseBtn.click();
		 		StoreLocatorFlag1979 =true;
		 		pass("  MAPS overlay is closing in store results page on selecting close button ");
		 	}
		 	else
		 	{
		 		StoreLocatorFlag1979 =false;
		 		fail(" MAPS overlay is not closing in store results page on selecting close button ");
		 	}*/
		}
		catch(Exception e)
		{
		 e.getMessage();
				System.out.println("Something went wrong"+e.getMessage());
				exception(e.getMessage());
		}
		
	}
    
    
	
/********************************************************************Store Map ******************************************/

  @Test(priority=9)
    public void BNIA46_Store_Map() throws Exception
    {
    	 TempSignIn();
    	 BNIA610();
    	 BNIA611();
    	 BNIA1991();
    	 BNIA1001();
    	 BNIA1002();
    	 //BNIA1003();
    	 BNIA1992();
    	 BNIA1993();
    	 BNIA1994();
    	 BNIA1995();
    	 BNIA1996();
    	 BNIA1997();
    	 BNIA1998();
    	 BNIA1999();
    	 BNIA2000();
    	 BNIA2001();
    	 BNIA2002();
    	 BNIA2003();
    	 BNIA2004();
    	 BNIA2005();
    	 BNIA2006();
    	 BNIA2007();
    	 BNIA2008();
    	 BNIA2012();
    	// TemSignOut();
    	 
    	 
    	 
    	 
    }    
    
    /*BNIA610 - Verify that Store Map icon should be shown in the home page*/
    public void BNIA610() throws Exception 
    {
    ChildCreation("BNIA610 - Verify that Store Map icon should be shown in the home page");
    try
    {
    	Thread.sleep(2000);
    	BNBasicCommonMethods.waitforElement(wait, obj, "StoreMapIcon");
     	WebElement StoreMapicon = BNBasicCommonMethods.findElement(driver, obj, "StoreMapIcon");
     	if(StoreMapicon.isDisplayed())
     	{
     		pass(" Store Map icon is showing in the home page ");
     	}
     	else
     	{
     		fail(" Store Map is not showing in the home page ");
     	}
    }
    catch(Exception e)
    {
      	 e.getMessage();
    		System.out.println("Something went wrong"+e.getMessage());
    		exception(e.getMessage());
    }

    }
    	   
    /*BNIA611 - Verify that Store Map icon should be shown in the Pancake flyover.*/
    public void BNIA611() throws Exception 
       {
      	ChildCreation("BNIA611 - Verify that Store Map icon should be shown in the Pancake flyover.");
      	try
      	{
      		BNBasicCommonMethods.waitforElement(wait, obj, "pancakeicon");
      	 	WebElement pancakeicon = BNBasicCommonMethods.findElement(driver, obj, "pancakeicon");
      	 	pancakeicon.click();
      	 	BNBasicCommonMethods.waitforElement(wait, obj, "StoreMapiconfrompancake");
      	 	WebElement StoreMapiconfrompancake = BNBasicCommonMethods.findElement(driver, obj, "StoreMapiconfrompancake");
      	 	if(StoreMapiconfrompancake.isDisplayed())
      	 	{
      	 		pass(" Store Map icon is showing in the pancake ");
      	 	}
      	 	else
      	 	{
      	 		fail(" Store Map is not showing in the panckae ");
      	 	}
      	 	BNBasicCommonMethods.waitforElement(wait, obj, "pancakeiconclose");
      	 	WebElement pancakeiconclose = BNBasicCommonMethods.findElement(driver, obj, "pancakeiconclose");
      	 	pancakeiconclose.click();
      	}
      	catch(Exception e)
     	{
         	 e.getMessage();
     			System.out.println("Something went wrong"+e.getMessage());
     			exception(e.getMessage());
     	}
    }
    	  	
    /*BNIA1991 - Verify that Store Map should be shown on selecting the store icon from home page.*/
    public void BNIA1991() throws Exception 
       {
      	ChildCreation("BNIA1991 - Verify that Store Map should be shown on selecting the store icon from home page.");
      	try
      	{
      		Thread.sleep(2000);
      		sheet = BNBasicCommonMethods.excelsetUp("storemap");
    		//String StoreID = BNBasicCommonMethods.getExcelNumericVal("BNIA1000", sheet,3);
      		String StoreID = BNConstant.StoreID;
      		BNBasicCommonMethods.waitforElement(wait, obj, "StoreMapIcon");
      		Thread.sleep(2000);
      		WebElement StoreMapicon1 = BNBasicCommonMethods.findElement(driver, obj, "StoreMapIcon");
      		if(BNBasicCommonMethods.isElementPresent(StoreMapicon1))
      		{
    		log.add("Store Buton is showing fine now");
    		pass("",log);
    		Thread.sleep(2000);
    		StoreMapicon1.click();
    		Thread.sleep(2000);
    		String StoreURL = "http://bnstoremaps.com/product.php?storeNbr="+StoreID;
    		driver.get(StoreURL);
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "Storemapcontainer");
    		WebElement Storemapcontainer = BNBasicCommonMethods.findElement(driver, obj, "Storemapcontainer");
    		Thread.sleep(2500);
    		if(Storemapcontainer.isDisplayed())
    		{
    			log.add("Empty Store Map is showing fine");
    			pass("",log);
    			
    		}
    		else
    		{
    			log.add("Empty Store Map is not showing");
    			fail("",log);
    		}
      		}
      		Thread.sleep(1000);;
      		String homeURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/home";
    		driver.get(homeURL);
    		Thread.sleep(2000);
    		BNBasicCommonMethods.waitforElement(wait, obj, "Homepage2");
    		WebElement Homepage2 = BNBasicCommonMethods.findElement(driver, obj, "Homepage2");
    		if(Homepage2.isDisplayed())
    		{
    			log.add("Home page is showing fine");
    			pass("",log);
    			
    		}
    		else
    		{
    			log.add("Home page is not showing");
    			fail("",log);
    		}
      		
      	}
      	 	catch(Exception e)
    	 	{
    	     	 e.getMessage();
    	 			System.out.println("Something went wrong"+e.getMessage());
    	 			exception(e.getMessage());
    	 	}
       }

    /*BNIA1001 - Verify that while selecting the Search icon,the search page should be displayed.*/
    public void BNIA1001() throws Exception 
      {
     	ChildCreation("BNIA1001 - Verify that while selecting the Search icon,the search page should be displayed ");
     	try
     	{
     		BNBasicCommonMethods.waitforElement(wait, obj, "Searchtile");
     	 	WebElement search = BNBasicCommonMethods.findElement(driver, obj, "Searchtile");
     	 	search.click();
     	 	BNBasicCommonMethods.waitforElement(wait, obj, "searchbox");
     	 	WebElement searchpage = BNBasicCommonMethods.findElement(driver, obj, "searchbox");
     	 	boolean search1 = BNBasicCommonMethods.isElementPresent(searchpage);
     	 	if(search1)
     	 	{
     	 		pass(" while selecting Search icon at the header or in the home page,the search page is displayed ");
     	 	}
     	 	else
     	 	{
     	 		fail(" while selecting the Search icon at the header or in the home page,the search page is not displayed ");
     	 	}
     	}
     	catch(Exception e)
    	{
        	 e.getMessage();
    			System.out.println("Something went wrong"+e.getMessage());
    			exception(e.getMessage());
    	}
      }	
        
    /*BNIA-1002 - Verify that while entering the EAN number,the respective product details page should be displayed*/
    public void BNIA1002()
    {
    	ChildCreation("BNIA1002 - Verify that while entering the EAN number,the respective product details page should be displayed");
    	try
    	{
    		sheet = BNBasicCommonMethods.excelsetUp("storemap");
    		String SreachKwd2 = BNBasicCommonMethods.getExcelNumericVal("BNIA1001", sheet,2);
    		//String SreachKwd3 = "9780316407021";
    		WebElement searchboxwithval4 = BNBasicCommonMethods.findElement(driver, obj, "searchboxwithval4");
      		if(searchboxwithval4.isDisplayed())
      		{
      			pass("Search Box is found");
      			searchboxwithval4.click();
      			Actions action = new Actions(driver);
      			action.moveToElement(searchboxwithval4).sendKeys(SreachKwd2).build().perform();
      			Thread.sleep(1500);
      			searchboxwithval4.sendKeys(Keys.ENTER);
      			BNBasicCommonMethods.waitforElement(wait, obj, "PDPcontainer"); 
      			WebElement PDPcontainer = BNBasicCommonMethods.findElement(driver, obj, "PDPcontainer");
        		if(PDPcontainer.isDisplayed())
        		{
      				pass("While entering the EAN number,the respective product details page is displayed");
      			}
      			else
      			{
      				pass("While entering the EAN number,the respective product details page not displayed");
      			} 
      			
      		}
      		else
      		{
      			fail("there is no Search Box is found");
      		}	
      		BNBasicCommonMethods.waitforElement(wait, obj, "PDPcontainerbck"); 
    		WebElement PDPcontainerbck = BNBasicCommonMethods.findElement(driver, obj, "PDPcontainerbck");
    		PDPcontainerbck.click(); 
    		Thread.sleep(1500);
    		BNBasicCommonMethods.waitforElement(wait, obj, "searchboxwithval6"); 
    		WebElement searchboxwithval6 = BNBasicCommonMethods.findElement(driver, obj, "searchboxwithval6");
    		searchboxwithval6.clear();
    		/*BNBasicCommonMethods.waitforElement(wait, obj, "srchclosebtn2"); 
    		WebElement srchclosebtn2 = BNBasicCommonMethods.findElement(driver, obj, "srchclosebtn2");
    		srchclosebtn2.click();
    		Thread.sleep(1500);*/
    		
    	}
    	catch(Exception e)
    	{
    		System.out.println(e.getMessage());
    		exception(" There is somwthing went wrong , please verify"+e.getMessage());
    	}
    	
    }

    /*BNIA-1003 - Verify that SHow in store button should be shown in PDP & on selecting it ,the respective store map page should be displayed*/
    /*public void BNIA1003()
    {
    	ChildCreation("BNIA1003 - Verify that Show in store button should be shown in PDP & on selecting it ,the respective store map page should be displayed");
    	try
    	{
    		sheet = BNBasicCommonMethods.excelsetUp("storemap");
    		String SreachKwd3 = BNBasicCommonMethods.getExcelNumericVal("BNIA1002", sheet,2);
    		sheet = BNBasicCommonMethods.excelsetUp("storemap");
    		String StoreID = BNBasicCommonMethods.getExcelNumericVal("BNIA1002", sheet,3);
    		
    		//String SreachKwd3 = "9780316407021";
    		WebElement searchboxwithval4 = BNBasicCommonMethods.findElement(driver, obj, "searchboxwithval4");
      		if(searchboxwithval4.isDisplayed())
      		{
      			pass("Search Box is found");
      			searchboxwithval4.click();
      			Actions action = new Actions(driver);
      			action.moveToElement(searchboxwithval4).sendKeys(SreachKwd3).build().perform();
      			Thread.sleep(1500);
      			searchboxwithval4.sendKeys(Keys.ENTER);
      			Thread.sleep(1500);
      			/*String url = driver.getCurrentUrl();
      			if(url.contains(SreachKwd3))
      			{
      				driver.get(url);
      				//((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
      			}
      			Thread.sleep(1000);
      			int ctr=1;
      			do
      			{
      				/*if (driver instanceof JavascriptExecutor)
      				 {
      				 ((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
      				 ((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
      				 }
      				 else 
      				 {
      				 throw new IllegalStateException("This driver does not support JavaScript!");
      				 }
      				
      				
      				//((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
      				((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
      				((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
      				//((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
      				try
      				{
    	 	    		Thread.sleep(3500);
      					//BNBasicCommonMethods.waitforElement(wait, obj, "ShowInStoreButton");
      					WebElement ShowInStore = BNBasicCommonMethods.findElement(driver, obj, "ShowInStoreButton");
     	  				if(BNBasicCommonMethods.isElementPresent(ShowInStore))
     	  				{
     	  					ctr=11;
     	  					log.add("SHOW in Store Buton is showing fine now");
     	  					pass("",log);
     	  					ShowInStore.click();
     	  					String StoreURL = "http://bnstoremaps.com/product.php?storeNbr="+StoreID+"&itemId="+SreachKwd3+"&mode=ast";
     	  					driver.get(StoreURL);
     	  					BNBasicCommonMethods.waitforElement(wait, obj, "ShowInStore_EANNo");
     	  					WebElement ShowInStoreEan = BNBasicCommonMethods.findElement(driver, obj, "ShowInStore_EANNo");
     	  					if(ShowInStoreEan.isDisplayed())
     	  					{
     	  						log.add("Store Map is showing fine");
     	  						pass("",log);
     	  						
     	  					}
     	  					else
     	  					{
     	  						log.add("Store Map is not showing");
     	  						fail("",log);
     	  					}
     	  				}	
     	  				else
     	  				{
     	  					log.add("SHOW in Store Buton is not showing fine now");
     	  					fail("",log);
     	  				}
      				}	
      				
      				catch(Exception e)
      				{
      					System.out.println("Show in Store Button is not shown");
      					String new11 = driver.getCurrentUrl();
     	  				driver.get(new11);
     	  				ctr++;
     	  				for(int i=0;i<=4;i++)
     	  				{
     	  					((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID+");}");
     	  					
     	  				}
     	  				
      				}
      				
      				
      			}while(ctr<=10);
      			
      		}
      		else
      		{
      			fail("There is no Search Box is found");
      		}	
    		
    	}
    	catch(Exception e)
    	{
    		System.out.println(e.getMessage());
    		exception(" There is something went wrong , please verify"+e.getMessage());
    	}
    	
    }*/

    /*BNIA-1992 - Verify that SHow in store button should be shown in PDP & on selecting it ,the respective store map page should be displayed*/
    public void BNIA1992()
    {
    	ChildCreation("BNIA1992 - Verify that Show in store button should be shown in PDP & on selecting it ,the respective store map page should be displayed");
    	try
    	{
    		sheet = BNBasicCommonMethods.excelsetUp("storemap");
    		String SreachKwd5 = BNBasicCommonMethods.getExcelNumericVal("BNIA1004", sheet,2);
    		/*sheet = BNBasicCommonMethods.excelsetUp("storemap");
    		String StoreID1 = BNBasicCommonMethods.getExcelNumericVal("BNIA1004", sheet,3);*/
    		String StoreID1 = BNConstant.StoreID;
    		
    		//String SreachKwd3 = "9780316407021";
    		WebElement searchboxwithval4 = BNBasicCommonMethods.findElement(driver, obj, "searchboxwithval4");
      		if(searchboxwithval4.isDisplayed())
      		{
      			pass("Search Box is found");
      			searchboxwithval4.click();
      			Actions action = new Actions(driver);
      			action.moveToElement(searchboxwithval4).sendKeys(SreachKwd5).build().perform();
      			Thread.sleep(1500);
      			searchboxwithval4.sendKeys(Keys.ENTER);
      			Thread.sleep(1500);
      			String url = driver.getCurrentUrl();
      			if(url.contains(SreachKwd5))
      			{
      				/*((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID1+");}");
      				Thread.sleep(1000);
      				driver.get(url);*/
      				pass("PDP is showing fine");
      				String StoremapURL = "http://bnstoremaps.com/product.php?storeNbr="+StoreID1+"&itemId="+SreachKwd5+"&mode=ast";
    				driver.get(StoremapURL); 
    			    /*((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID1+");}");
    			    ((JavascriptExecutor)driver).executeScript("skMobileTemplate.prototype.getStoreid = function(cbk){cbk("+StoreID1+");}");*/
    			    try
    				{
    					Thread.sleep(2500);
    					BNBasicCommonMethods.waitforElement(wait, obj, "ShowInStore_EANNo");
    					WebElement ShowInStore_EANNo = BNBasicCommonMethods.findElement(driver, obj, "ShowInStore_EANNo");
    					if(BNBasicCommonMethods.isElementPresent(ShowInStore_EANNo))
    					{
    							pass("STORE MAP is showing now");
    						}
    						else
    						{
    							fail("STORE MAP is not shown");
    						}
    				}
    			    catch(Exception e)
    		 		{
    		 			System.out.println(e.getMessage());
    		 			exception(" There is something went wrong , please verify"+e.getMessage());
    		 		}	
    			    
      			}
      			else
      			{
      				fail("PDP is not shown");
      			}
      			
      		}
      		else
      		{
      			fail("Search Box is not found");
      		}
      			
      	}
     		catch(Exception e)
     		{
     			System.out.println(e.getMessage());
     			exception(" There is something went wrong , please verify"+e.getMessage());
     		}
     		
     	}
      	
    /*BNIA-1993 - Verify that store name/location should be shown at top right of store map page.*/
    public void BNIA1993()
    {
    	ChildCreation("BNIA1993- Verify that store name/location should be shown at top right of store map page.");
    	try
    	{
    		BNBasicCommonMethods.waitforElement(wait, obj, "Storenametopright");
    		WebElement Storenametopright = BNBasicCommonMethods.findElement(driver, obj, "Storenametopright");
    		if(BNBasicCommonMethods.isElementPresent(Storenametopright))
    		{
    		 pass("Store name/location is showing at top right of store map page.");
    		}
    		else
    		{
    		fail("Store name/location is not showing at top right of store map page.");
    		}	
      	}
     		catch(Exception e)
     		{
     			System.out.println(e.getMessage());
     			exception(" There is something went wrong , please verify"+e.getMessage());
     		}
     		
     	}

    /*BNIA-1994 - Verify that Barnes & Nobles logo should be shown at top left of store map page.*/
    public void BNIA1994()
    {
    	ChildCreation("BNIA1994 - Verify that Barnes & Nobles logo should be shown at top left of store map page.");
    	try
    	{
    		BNBasicCommonMethods.waitforElement(wait, obj, "Storelogoleft");
    		WebElement Storelogoleft = BNBasicCommonMethods.findElement(driver, obj, "Storelogoleft");
    		if(BNBasicCommonMethods.isElementPresent(Storelogoleft))
    		{
    		 pass("Barnes & Nobles logo is showing at top left of store map page.");
    		}
    		else
    		{
    		fail("Barnes & Nobles logo is not shown at top left of store map page.");
    		}	
      	}
     		catch(Exception e)
     		{
     			System.out.println(e.getMessage());
     			exception(" There is something went wrong , please verify"+e.getMessage());
     		}
     		
     	}

    /*BNIA-1995 - Verify that Search box should be shown at store map page.*/
    public void BNIA1995()
    	 	{
    	 		ChildCreation("BNIA1995 - Verify that Search box should be shown at store map page.");
    	 		try
    	 		{
    	 			BNBasicCommonMethods.waitforElement(wait, obj, "Storemapsearchbox");
    				WebElement Storemapsearchbox = BNBasicCommonMethods.findElement(driver, obj, "Storemapsearchbox");
    				if(BNBasicCommonMethods.isElementPresent(Storemapsearchbox))
    				{
    				 pass("Search box is showing at top left of store map page.");
    				}
    				else
    				{
    				fail("Search box is not shown at top left of store map page.");
    				}	
    	 	  	}
    		 		catch(Exception e)
    		 		{
    		 			System.out.println(e.getMessage());
    		 			exception(" There is something went wrong , please verify"+e.getMessage());
    		 		}
    		 		
    		 	}
    	 	 	
   /*BNIA1996 - Verify that Store Search field should contain Search inline text*/
    public void BNIA1996() throws Exception 
      {
         ChildCreation("BNIA1996 - Verify that Store Search field should contain Search inline text.");
    		try
             {
    		  		BNBasicCommonMethods.waitforElement(wait, obj, "StoremapsearchboxInlineText");
    		  	 	WebElement StoremapsearchboxInlineText = BNBasicCommonMethods.findElement(driver, obj, "StoremapsearchboxInlineText");
    		  	 	String inline = StoremapsearchboxInlineText.getAttribute("placeholder");
    		  	 //	System.out.println(""+inline);
    		  	 	String text = "Search for an item";
    		  	 	if(inline.equals(text))
    		  	 	{
    		  	 		pass("Inline text is showing fine now");
    		  	 	}
    		  	 	else
    		  	 	{
    		  	 		fail("inline text is not shown");
    		  	 	}
    		  	}
    		  	catch(Exception e)
    		 	{
    		     	 e.getMessage();
    		 			System.out.println("Something went wrong"+e.getMessage());
    		 			exception(e.getMessage());
    		 	}
    		  	
    		 }
    	 	
     /*BNIA1997 - Verify that product info (Name, author, Format & Reviews) should be shown at store map page.*/
     public void BNIA1997() throws Exception 
     {
       ChildCreation("BNIA1997 - Verify that product info (Name, author, Format & Reviews) should be shown at store map page.");
    		try
    		{
    			BNBasicCommonMethods.waitforElement(wait, obj, "Iteminfo");
    		WebElement Iteminfo = BNBasicCommonMethods.findElement(driver, obj, "Iteminfo");
    		if(BNBasicCommonMethods.isElementPresent(Iteminfo))
    		{
    			pass("Product info is showing in store map page.");
    			 BNBasicCommonMethods.waitforElement(wait, obj, "Itemname");
    			 WebElement Itemname = BNBasicCommonMethods.findElement(driver, obj, "Itemname");
    			 boolean itemname = BNBasicCommonMethods.isElementPresent(Itemname);
    			 BNBasicCommonMethods.waitforElement(wait, obj, "Authorname");
    			 WebElement Authorname = BNBasicCommonMethods.findElement(driver, obj, "Authorname");
    			 boolean authorname = BNBasicCommonMethods.isElementPresent(Authorname);
    			 BNBasicCommonMethods.waitforElement(wait, obj, "Covertype");
    			 WebElement Covertype = BNBasicCommonMethods.findElement(driver, obj, "Covertype");
    			 boolean covertype = BNBasicCommonMethods.isElementPresent(Covertype);
    			 BNBasicCommonMethods.waitforElement(wait, obj, "Ean");
    			 WebElement Ean = BNBasicCommonMethods.findElement(driver, obj, "Ean");
    			 boolean ean = BNBasicCommonMethods.isElementPresent(Ean);
    			 BNBasicCommonMethods.waitforElement(wait, obj, "Rating");
    			 WebElement Rating = BNBasicCommonMethods.findElement(driver, obj, "Rating");
    			 boolean rating = BNBasicCommonMethods.isElementPresent(Rating);
    			 BNBasicCommonMethods.waitforElement(wait, obj, "moreinfobutton");
    			 WebElement moreinfobutton = BNBasicCommonMethods.findElement(driver, obj, "moreinfobutton");
    			 moreinfobutton.click();
    			 BNBasicCommonMethods.waitforElement(wait, obj, "productoverview");
    			 WebElement productoverview = BNBasicCommonMethods.findElement(driver, obj, "productoverview");
    			 boolean Productoverview = BNBasicCommonMethods.isElementPresent(productoverview);
    			 if(itemname && authorname && covertype && ean && rating && Productoverview)
    			 {
    				 pass("The item name is "+Itemname.getText());
    				 pass("The Author name is "+Authorname.getText());
    				 pass("The cover type of product is "+Covertype.getText());
    				 pass("The EAN number of the product is "+Ean.getText());
    				 pass("The rating of the product is "+Rating.getText());
    				 pass("The overview of the product is "+productoverview.getText());
    				 
    				 
    			 }
    			 else
    			 {
    				 fail("Product info (Name, author, Format & Reviews) is not shown at store map page");
    			 }
    		}
    		else
    		{
    		fail("Product info is not showing in store map page.");
    		}	
    	  	}
     		catch(Exception e)
     		{
     			System.out.println(e.getMessage());
     			exception(" There is something went wrong , please verify"+e.getMessage());
     		}
     		
     	}

     /*BNIA1998 - Verify that Store map view should be shown in store map page */
     public void BNIA1998() throws Exception 
     {
    	ChildCreation("BNIA1998 - Verify that Store map view should be shown in store map page.");
    	try
    	{
    		BNBasicCommonMethods.waitforElement(wait, obj, "Storemapview");
    	 	WebElement Storemapview = BNBasicCommonMethods.findElement(driver, obj, "Storemapview");
    	 	if(BNBasicCommonMethods.isElementPresent(Storemapview))
    	{
    	 pass("Search Map view is showing at store map page.");
    	}
    	else
    	{
    	fail("Search Map view is not shown at store map page.");
    	}	
    	}
    		catch(Exception e)
    		{
    			System.out.println(e.getMessage());
    			exception(" There is something went wrong , please verify"+e.getMessage());
    		}
    		
    	}

     /*BNIA1999 - Verify that Floor 1 & Floor 2 buttons should be shown in store map page */
     public void BNIA1999() throws Exception 
     {
    	ChildCreation("BNIA1999 - Verify that Floor 1 & Floor 2 buttons should be shown in store map page.");
    	try
    	{
    		BNBasicCommonMethods.waitforElement(wait, obj, "Storemaplevel1");
    	 	WebElement Storemaplevel1 = BNBasicCommonMethods.findElement(driver, obj, "Storemaplevel1");
    	 	boolean storemaplevel1 = BNBasicCommonMethods.isElementPresent(Storemaplevel1);
    	 	BNBasicCommonMethods.waitforElement(wait, obj, "Storemaplevel2");
    	 	WebElement Storemaplevel2 = BNBasicCommonMethods.findElement(driver, obj, "Storemaplevel2");
    	 	boolean storemaplevel2 = BNBasicCommonMethods.isElementPresent(Storemaplevel2);
    	 	if(storemaplevel1 && storemaplevel2)
    	{
    	 pass("Floor 1 & Floor 2 buttons are showing at store map page.");
    	}
    	else
    	{
    	fail("Floor 1 & Floor 2 buttons are not shown at store map page.");
    	}	
    	}
    		catch(Exception e)
    		{
    			System.out.println(e.getMessage());
    			exception(" There is something went wrong , please verify"+e.getMessage());
    		}
    		
    	}
     
     /*BNIA2000 - Verify that Zoom In & Zoom out buttons should be shown in Floor 1 of store map page */
     public void BNIA2000() throws Exception 
     {
    	ChildCreation("BNIA2000 - Verify that Zoom In & Zoom out buttons should be shown in Floor 1 of  store map page.");
    	try
    	{
    		BNBasicCommonMethods.waitforElement(wait, obj, "Storezoomin");
    	 	WebElement Storezoomin = BNBasicCommonMethods.findElement(driver, obj, "Storezoomin");
    	 	boolean storezoomin = BNBasicCommonMethods.isElementPresent(Storezoomin);
    	 	BNBasicCommonMethods.waitforElement(wait, obj, "Storezoomout");
    	 	WebElement Storezoomout = BNBasicCommonMethods.findElement(driver, obj, "Storezoomout");
    	 	boolean storezoomout = BNBasicCommonMethods.isElementPresent(Storezoomout);
    	 	if(storezoomin && storezoomout)
    	{
    	 pass("Zoom In & Zoom out buttons are showing at Floor 1 of store map page.");
    	}
    	else
    	{
    	fail("Zoom In & Zoom out buttons are not shown at Floor 1 of store map page.");
    	}	
    	}
    		catch(Exception e)
    		{
    			System.out.println(e.getMessage());
    			exception(" There is something went wrong , please verify"+e.getMessage());
    		}
    		
    	}
     
     /*BNIA2001 - Verify that Zoom In & Zoom out buttons should works at floor of store map page */
     public void BNIA2001() throws Exception 
     {
    	ChildCreation("BNIA2001 - Verify that Zoom In & Zoom out buttons should works at floor 1 of store map page.");
    	try
    	{
    		BNBasicCommonMethods.waitforElement(wait, obj, "Storezoomin");
    	 	WebElement Storezoomin = BNBasicCommonMethods.findElement(driver, obj, "Storezoomin");
    	 	Storezoomin.click();
    	 	BNBasicCommonMethods.waitforElement(wait, obj, "Storezoominloaded");
    	 	WebElement Storezoominloaded = BNBasicCommonMethods.findElement(driver, obj, "Storezoominloaded");
    	 	if(Storezoominloaded.isDisplayed())
    	 	{
    	 		pass("Zoom In button is working in floor 1 of store map page.");
    	 	}
    	 	else
    	 	{
    	 		pass("Zoom In button is not working in floor 1 of store map page.");
    	 	}
    	 	BNBasicCommonMethods.waitforElement(wait, obj, "Storezoomoutactive");
    	 	WebElement Storezoomoutactive = BNBasicCommonMethods.findElement(driver, obj, "Storezoomoutactive");
    	 	Storezoomoutactive.click();
    	 	BNBasicCommonMethods.waitforElement(wait, obj, "Storezoomoutloaded");
    	 	WebElement Storezoomoutloaded = BNBasicCommonMethods.findElement(driver, obj, "Storezoomoutloaded");
    	 	if(Storezoomoutloaded.isDisplayed())
    	{
    	 pass("Zoom out button is working in floor 1 of store map page.");
    	}
    	else
    	{
    	fail("Zoom out button id not working in floor 1 of store map page.");
    	}	
    	}
    		catch(Exception e)
    		{
    			System.out.println(e.getMessage());
    			exception(" There is something went wrong , please verify"+e.getMessage());
    		}
    		
    	}

     /*BNIA2002 - Verify that floor 1 & floor 2 buttons are working in  store map page. */
     public void BNIA2002() throws Exception 
     {
    	ChildCreation("BNIA2002 - Verify that floor 2 button is working in  store map page.");
    	try
    	{
    		BNBasicCommonMethods.waitforElement(wait, obj, "Storemaplevel2button");
    	 	WebElement Storemaplevel2button = BNBasicCommonMethods.findElement(driver, obj, "Storemaplevel2button");
    	 	Storemaplevel2button.click();
    	 	BNBasicCommonMethods.waitforElement(wait, obj, "Storemaplevel2buttonselect");
    	 	WebElement Storemaplevel2buttonselect = BNBasicCommonMethods.findElement(driver, obj, "Storemaplevel2buttonselect");
    	 	if(Storemaplevel2buttonselect.isDisplayed())
    	 	{
    	 		pass("floor 2 button is working in store map page.");
    	 	}
    	 	else
    	 	{
    	 		fail("floor2 buttons is not working in store map page.");
    	 	}
    	}
    		catch(Exception e)
    		{
    			System.out.println(e.getMessage());
    			exception(" There is something went wrong , please verify"+e.getMessage());
    		}
    		
    	}
     
     /*BNIA2003 - Verify that Zoom In & Zoom out buttons should be shown in Floor 2 of store map page */
     public void BNIA2003() throws Exception 
     {
    	ChildCreation("BNIA2003 - Verify that Zoom In & Zoom out buttons should be shown in Floor 2 of  store map page.");
    	try
    	{
    		BNBasicCommonMethods.waitforElement(wait, obj, "Storezoomin");
    	 	WebElement Storezoomin = BNBasicCommonMethods.findElement(driver, obj, "Storezoomin");
    	 	boolean storezoomin = BNBasicCommonMethods.isElementPresent(Storezoomin);
    	 	BNBasicCommonMethods.waitforElement(wait, obj, "Storezoomout");
    	 	WebElement Storezoomout = BNBasicCommonMethods.findElement(driver, obj, "Storezoomout");
    	 	boolean storezoomout = BNBasicCommonMethods.isElementPresent(Storezoomout);
    	 	if(storezoomin && storezoomout)
    	{
    	 pass("Zoom In & Zoom out buttons are showing at Floor 2 of store map page.");
    	}
    	else
    	{
    	fail("Zoom In & Zoom out buttons are not shown at Floor 2 of store map page.");
    	}	
    	}
    		catch(Exception e)
    		{
    			System.out.println(e.getMessage());
    			exception(" There is something went wrong , please verify"+e.getMessage());
    		}
    		
    	}
     
     /*BNIA2004 - Verify that Zoom In & Zoom out buttons should works at floor 2 of store map page */
     public void BNIA2004() throws Exception 
     {
    	ChildCreation("BNIA2004 - Verify that Zoom In & Zoom out buttons should works at floor 2 of store map page.");
    	try
    	{
    		BNBasicCommonMethods.waitforElement(wait, obj, "Storezoomin");
    	 	WebElement Storezoomin = BNBasicCommonMethods.findElement(driver, obj, "Storezoomin");
    	 	Storezoomin.click();
    	 	BNBasicCommonMethods.waitforElement(wait, obj, "Storezoominloaded");
    	 	WebElement Storezoominloaded = BNBasicCommonMethods.findElement(driver, obj, "Storezoominloaded");
    	 	if(Storezoominloaded.isDisplayed())
    	 	{
    	 		pass("Zoom In button is working in floor 2 of store map page.");
    	 	}
    	 	else
    	 	{
    	 		pass("Zoom In button is not working in floor 2 of store map page.");
    	 	}
    	 	BNBasicCommonMethods.waitforElement(wait, obj, "Storezoomoutactive");
    	 	WebElement Storezoomoutactive = BNBasicCommonMethods.findElement(driver, obj, "Storezoomoutactive");
    	 	Storezoomoutactive.click();
    	 	BNBasicCommonMethods.waitforElement(wait, obj, "Storezoomoutloaded");
    	 	WebElement Storezoomoutloaded = BNBasicCommonMethods.findElement(driver, obj, "Storezoomoutloaded");
    	 	if(Storezoomoutloaded.isDisplayed())
    	{
    	 pass("Zoom out button is working in floor 2 of store map page.");
    	}
    	else
    	{
    	fail("Zoom out button id not working in floor 2 of store map page.");
    	}	
    	}
    		catch(Exception e)
    		{
    			System.out.println(e.getMessage());
    			exception(" There is something went wrong , please verify"+e.getMessage());
    		}
    		
    	}
     
     /*BNIA2005 - Verify that floor 1 button is are working in  store map page. */
     public void BNIA2005() throws Exception 
     {
    	ChildCreation("BNIA2002 - Verify that floor 1 button is working in  store map page.");
    	try
    	{
    		BNBasicCommonMethods.waitforElement(wait, obj, "Storemaplevel1button");
    	 	WebElement Storemaplevel1button = BNBasicCommonMethods.findElement(driver, obj, "Storemaplevel1button");
    	 	Storemaplevel1button.click();
    	 	BNBasicCommonMethods.waitforElement(wait, obj, "Storemaplevel1buttonselect");
    	 	WebElement Storemaplevel1buttonselect = BNBasicCommonMethods.findElement(driver, obj, "Storemaplevel1buttonselect");
    	 	if(Storemaplevel1buttonselect.isDisplayed())
    	 	{
    	 		pass("Floor 1 button is working in store map page.");
    	 	}
    	 	else
    	 	{
    	 		fail("Floor1 buttons is not working in store map page.");
    	 	}
    	}
    		catch(Exception e)
    		{
    			System.out.println(e.getMessage());
    			exception(" There is something went wrong , please verify"+e.getMessage());
    		}
    		
    	}
     
     /*BNIA2006 - Verify that locate a store tab should shown & works in store map page.. */
     public void BNIA2006() throws Exception 
     {
    	ChildCreation("BNIA2006 - Verify that locate a store tab should shown & works in store map page.");
    	try
    	{
    		BNBasicCommonMethods.waitforElement(wait, obj, "Storemaptab2");
    	 	WebElement Storemaptab2 = BNBasicCommonMethods.findElement(driver, obj, "Storemaptab2");
    	 	if(Storemaptab2.isDisplayed())
    	 	{
    	 		pass("Locate a store tab is showing in store map page.");
    	 	}
    	 	else
    	 	{
    	 		fail("Locate a store tab is not showing in store map page.");
    	 	}
    	}
    		catch(Exception e)
    		{
    			System.out.println(e.getMessage());
    			exception(" There is something went wrong , please verify"+e.getMessage());
    		}
    		
    	}
     
     /*BNIA2007 - Verify that locate a store tab should works in store map page.. */
     public void BNIA2007() throws Exception 
     {
    	ChildCreation("BNIA2007 - Verify that locate a store tab should works in store map page.");
    	try
    	{
    		BNBasicCommonMethods.waitforElement(wait, obj, "Storemaptab2");
    	 	WebElement Storemaptab2 = BNBasicCommonMethods.findElement(driver, obj, "Storemaptab2");
    	 	Storemaptab2.click();
    	 	BNBasicCommonMethods.waitforElement(wait, obj, "Storelocateastorepage");
    	 	WebElement Storelocateastorepage = BNBasicCommonMethods.findElement(driver, obj, "Storelocateastorepage");
    	 	if(Storelocateastorepage.isDisplayed())
    	 	{
    	 		pass("Locate a store tab is working & takes to full map view store map page.");
    	 	}
    	 	else
    	 	{
    	 		fail("Locate a store tab is not working & does not takes to full map view store map page.");
    	 	}
    	}
    		catch(Exception e)
    		{
    			System.out.println(e.getMessage());
    			exception(" There is something went wrong , please verify"+e.getMessage());
    		}
    		
    	}
     
     /*BNIA2008 - Verify that Drop down should works in store map page.. */
     public void BNIA2008() throws Exception 
     {
    	ChildCreation("BNIA2008 - Verify that Drop down should works in store map page.");
    	try
    	{
    		BNBasicCommonMethods.waitforElement(wait, obj, "Storelocateastorepage");
    	 	WebElement Storelocateastorepage = BNBasicCommonMethods.findElement(driver, obj, "Storelocateastorepage");
    	 	Storelocateastorepage.click();
    	 	BNBasicCommonMethods.waitforElement(wait, obj, "Storelocateastorepagedropdown");
    	 	WebElement Storelocateastorepagedropdown = BNBasicCommonMethods.findElement(driver, obj, "Storelocateastorepagedropdown");
    	 	if(Storelocateastorepagedropdown.isDisplayed())
    	 	{
    	 		pass("Drop down is showing in store map page.");
    	 	}
    	 	else
    	 	{
    	 		fail("Drop down is not showing in store map page");
    	 	}
    	}
    		catch(Exception e)
    		{
    			System.out.println(e.getMessage());
    			exception(" There is something went wrong , please verify"+e.getMessage());
    		}
    		
    	}
     
     /*BNIA2012 - Verify that when user selects any option in dropdown, the result map should be in store map page. */
     public void BNIA2012() throws Exception 
    		   {
    		  	ChildCreation("BNIA2012 -  Verify that when user selects any option in dropdown, the result map should be in store map page..");
    		  	try
    		  	{
    		   
    		   Thread.sleep(2000);
    		   List<WebElement> DropdownList = driver.findElements(By.xpath("//*[@class = 'dropdown-menu']//li"));
    			Random r = new Random();
    			
    			//List<WebElement> MarketPlaceText = driver.findElements(By.xpath("//*[@class='sk_allRollupTitleCont']//*[contains(@class,'sk_allRollupCondTitle')]"));
    			int sel = r.nextInt(DropdownList.size());
    			//WebElement text = driver.findElement(By.xpath("//*[@class = 'dropdown-menu']//li"));
    			//DropdownList.get(sel).click();
    			//WebElement text = driver.findElement(By.xpath("(//*[@role = 'presentation'])["+sel+"]"));
    			String str = DropdownList.get(sel).getText();
    			DropdownList.get(sel).click();
    			//System.out.println(str);
    			//String str1 = str.replace("bn", "");
    			//System.out.println(""+str1);
    			WebElement text1 = driver.findElement(By.xpath("//*[@id = 'locate_item_selection']"));
    			String str1 = text1.getText();
    			
    			if(str.equals(str1))
    			{
    				log.add(""+str1 +"category is selected & its store location is showing fine");
    				pass("Drop down works in store map page.",log);
    			}
    			else
    			{
    				fail("Drop down is not works in store map page.");
    			}
    		  	
    		  	String HomepageURL = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/home";
				driver.get(HomepageURL);
				System.out.println("9/11 - BNIA46_Store_Map is completed");
    		  	}
    			catch(Exception e)
    	 		{
    	 			System.out.println(e.getMessage());
    	 			exception("There is something went wrong , please verify"+e.getMessage());
    	 		}
    	 		
    	 	}


    
     @BeforeClass
     public void beforeTest() throws Exception
     {
    	 //Thread.sleep(5000);
    	 driver = bnEmulationSetup.bnASTEmulationSetup();
    	 wait = new WebDriverWait(driver, 30);
     }
     
     @AfterClass
     public void afterTest()
     {
         driver.close();
     }
     
     @BeforeMethod
    	public static void beforeTest(Method name)
    	{
    		parent = reporter.startTest(name.getName());
    		
    	}
    	
    	@org.testng.annotations.AfterMethod
    	public static void AfterMethod(Method name)
    	{
    		reporter.endTest(parent);
    		reporter.flush();
    	}
    	
    	/*@AfterSuite
    	public static void closeReporter() 
    	{
    		reporter.flush();
    	}*/
    	
    	
    	public static void ChildCreation(String name)
    	{
    		child = reporter.startTest(name);
    		parent.appendChild(child);
    	}
    	
    	public static void endTest()
    	{
    		reporter.endTest(child);
    	}
    	
    	public static void createlog(String val)
    	{
    		child.log(LogStatus.INFO, val);
    	}
    	
    	public static void pass(String val)
    	{
    		child.log(LogStatus.PASS, val);
    		endTest();
    	}
    	
    	public static void pass(String val,ArrayList<String> logval)
    	{
    		child.log(LogStatus.PASS, val);
    		for(String lval:logval)
    		{
    			createlog(lval);
    		}
    		logval.removeAll(logval);
    		endTest();
    	}
    	
    	public static void fail(String val)
    	{
    		child.log(LogStatus.FAIL, val);
    		endTest();
    	}
    	
    	public static void fail(String val,ArrayList<String> logval)
    	{
    		child.log(LogStatus.FAIL, val);
    		for(String lval:logval)
    		{
    			createlog(lval);
    		}
    		logval.removeAll(logval);
    		endTest();
    	}
    	
    	public static  void exception(String val)
    	{
    		child.log(LogStatus.ERROR,val);
    		endTest();
    	}
    	
    	public static void Skip(String val)
    	{
    		child.log(LogStatus.SKIP, val);
    		endTest();
    	}
    	
 
 
}