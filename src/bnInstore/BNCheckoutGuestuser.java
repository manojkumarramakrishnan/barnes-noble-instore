package bnInstore;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import bnEmulation.bnEmulationSetup;
import bnInstoreBasicFeatures.BNBasicCommonMethods;
import extentReport.extentRptManager;
public class BNCheckoutGuestuser 
{
	WebDriver driver;
	 public Properties obj;
	  public static ArrayList<String> log = new ArrayList<>();
	  WebDriverWait wait;
	  public static ExtentReports reporter = extentRptManager.getReporter();
	  public static ExtentTest parent, child;
	HSSFSheet sheet;
	int sel;
	
	public BNCheckoutGuestuser() throws IOException, Exception
	 {
	     obj = new Properties(System.getProperties());
		 obj.load(BNBasicCommonMethods.loadPropertiesFile());
		 sheet = BNBasicCommonMethods.excelsetUp("Edit ShipandBill Address");
		 
	 }
	
 
 /*********************************Temporary Sign In and Common Methods used in the functions**************************************/  
     
 	/*Temp signIn for Employee to Checkout page*/
  	public void TempSignIn()
	 {    
		  // Temp Signin  
		  WebDriverWait wait =  new WebDriverWait(driver, 90);
		    try
		    {
		    	WebElement Splash=BNBasicCommonMethods.findElement(driver, obj, "splashScreen");
		    	BNBasicCommonMethods.WaitForLoading(wait);
		    	Splash.click();
		    	BNBasicCommonMethods.waitforElement(wait, obj, "signInEmpId");
		    	WebElement id = BNBasicCommonMethods.findElement(driver, obj, "signInEmpId");
		  	   	WebElement pass = BNBasicCommonMethods.findElement(driver, obj, "signInEmpPass");
		    	BNBasicCommonMethods.signInclearAll(id, pass);
		    	id.sendKeys("999999999");
		    	pass.sendKeys("skava");
		    	BNBasicCommonMethods.findElement(driver, obj, "signInClick").click();
		    	BNBasicCommonMethods.waitforElement(wait, obj, "HomePage");
		    	//Thread.sleep(2000);
		    }
		    
	    catch(Exception e)
		    {
	    	System.out.println(e.getMessage());
	    	
		    	 exception(e.getMessage()); 
		    }
		    
		  }

  	/*clear all fields*/
  	public void clearAll() throws Exception
	{
  		WebElement fName = BNBasicCommonMethods.findElement(driver, obj, "fName");
		WebElement lName = BNBasicCommonMethods.findElement(driver, obj, "lName");
		WebElement stAddress = BNBasicCommonMethods.findElement(driver, obj, "stAddress");
		WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
		WebElement zipCode = BNBasicCommonMethods.findElement(driver, obj, "zipCode");
		WebElement contactNo = BNBasicCommonMethods.findElement(driver, obj, "contactNo");
		fName.clear();
		lName.clear();
		//BNBasicCommonMethods.scrolldown(stAddress,driver);
		stAddress.clear();
	   //aptSuiteAddress.clear();
		city.clear();
		zipCode.clear();
		contactNo.clear();
		//companyName.clear();
		//BNBasicCommonMethods.scrollup(fName,driver);
	}
     
  	
  	/*BNIA-806 Verify that on selecting the "Checkout" button in the shopping bag page,the Checkout page should be shown for registered user*/
	public void signInExistingCustomer() throws IOException, Exception
	{
		//ChildCreation("Add a new Shipping Address overlay should be Displayed");
		BNBasicCommonMethods.WaitForLoading(wait);
		BNBasicCommonMethods.waitforElement(wait, obj, "checkoutTile");
		WebElement CheckoutTile = BNBasicCommonMethods.findElement(driver, obj, "checkoutTile");
		wait.until(ExpectedConditions.elementToBeClickable(CheckoutTile));
		CheckoutTile.click();
	//	Thread.sleep(1500);
		//driver.navigate().refresh();
		//BNBasicCommonMethods.WaitForLoading(wait);
	    ChildCreation(" BNIA-806Verify that on selecting the Checkout button in the shopping bag page,the Checkout page should be shown for registered user");	
	
		BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
		WebElement CheckoutSignIn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutSignIn");
		//wait.until(ExpectedConditions.visibilityOf(CheckoutSignIn));
		//Thread.sleep(1000);
		CheckoutSignIn.click();
		
		log.add("The user is navigated to the Sign In Page.");
		BNBasicCommonMethods.waitforElement(wait, obj, "signInIframe");
		//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInIframe));
		WebElement signInIframe = BNBasicCommonMethods.findElement(driver, obj, "signInIframe");
		driver.switchTo().frame(signInIframe);
		WebElement SignInEmail = BNBasicCommonMethods.findElement(driver, obj, "SignInEmail");
		WebElement SignInPass = BNBasicCommonMethods.findElement(driver, obj, "SignInPass");
		WebElement secureSignIn = BNBasicCommonMethods.findElement(driver, obj, "secureSignIn");
		sheet = BNBasicCommonMethods.excelsetUp("Edit ShipandBill Address");
		int bniakey = sheet.getLastRowNum();
		for(int i = 0; i <= bniakey; i++)
		{	
			String tcid = "BNIA Sign In";
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				String Username = sheet.getRow(i).getCell(2).getStringCellValue().toString();
				String passw = sheet.getRow(i).getCell(3).getStringCellValue().toString();
				try 
				{
					if(BNBasicCommonMethods.isElementPresent(SignInEmail))
					{
						SignInEmail.sendKeys(Username);
						log.add("The User Name is successfully entered.");
						if(BNBasicCommonMethods.isElementPresent(SignInPass))
						{
							SignInPass.sendKeys(passw);
							log.add("The password is successfully entered.");
								 if(BNBasicCommonMethods.isElementPresent(secureSignIn))
								 {
									//Thread.sleep(1000);
									secureSignIn.click();
									log.add("Successful enter in Checkout page.");
									BNBasicCommonMethods.waitforElement(wait, obj, "Signoutbtn");
									//Thread.sleep(1000);
										BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutBtn");
										WebElement CheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutBtn");
										if(BNBasicCommonMethods.isElementPresent(CheckoutBtn))
										{
	
											//System.out.println("In Checkout Flow");
											//Thread.sleep(1000);
										   //CheckoutBtn.click();
											log.add("Successful Enter in Add a Shipping and Billing Address page.");
											/*try 
											{*/
										    	pass("Page navigated to checkout page ",log);
												//driver.navigate().refresh();
												CheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutBtn");
												//Thread.sleep(1000);
												((JavascriptExecutor) driver).executeScript("arguments[0].click();", CheckoutBtn);
										}
								 	}
								 else
								 {
									 fail("NO Successfull login to Checkout page");
								 }
						}
						else
						{
							fail("password field not found.");
							
						}
					}
					else
					{
						fail("Email Id field not found.");
						
					}
				}
				catch(Exception e)
				{
					System.out.println(e.getMessage());
					exception("Sign In to the User Account in Checkout Exception");
					fail(e.getMessage());
				}
			}
		}
	}

	/* Sign In to the New User Account in Checkout */
	public void signInNewCustomer() throws IOException, Exception
	{
		ChildCreation("Add a new Shipping Address overlay should be Displayed");
		BNBasicCommonMethods.WaitForLoading(wait);
		BNBasicCommonMethods.waitforElement(wait, obj, "checkoutTile");
		WebElement CheckoutTile = BNBasicCommonMethods.findElement(driver, obj, "checkoutTile");
		wait.until(ExpectedConditions.elementToBeClickable(CheckoutTile));
		CheckoutTile.click();
		//Thread.sleep(1500);
		//driver.navigate().refresh();
		//BNBasicCommonMethods.WaitForLoading(wait);
		BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
		WebElement CheckoutSignIn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutSignIn");
		/*wait.until(ExpectedConditions.visibilityOf(CheckoutSignIn));
		Thread.sleep(1000);*/
		CheckoutSignIn.click();
		
		log.add("The user is navigated to the Sign In Page.");
		BNBasicCommonMethods.waitforElement(wait, obj, "signInIframe");
		//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInIframe));
		WebElement signInIframe = BNBasicCommonMethods.findElement(driver, obj, "signInIframe");
		driver.switchTo().frame(signInIframe);
		WebElement SignInEmail = BNBasicCommonMethods.findElement(driver, obj, "SignInEmail");
		WebElement SignInPass = BNBasicCommonMethods.findElement(driver, obj, "SignInPass");
		WebElement secureSignIn = BNBasicCommonMethods.findElement(driver, obj, "secureSignIn");
		//sheet = BNBasicCommonMethods.excelsetUp("Edit ShipandBill Address");
		int bniakey = sheet.getLastRowNum();
		for(int i = 0; i <= bniakey; i++)
		{	
			String tcid = "BRM Sign In No Shipping Address";
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				String Username = sheet.getRow(i).getCell(2).getStringCellValue().toString();
				String passw = sheet.getRow(i).getCell(3).getStringCellValue().toString();
				try 
				{
					if(BNBasicCommonMethods.isElementPresent(SignInEmail))
					{
						SignInEmail.sendKeys(Username);
						log.add("The User Name is successfully entered.");
						if(BNBasicCommonMethods.isElementPresent(SignInPass))
						{
							SignInPass.sendKeys(passw);
						//	Thread.sleep(3000);
							log.add("The password is successfully entered.");
								 if(BNBasicCommonMethods.isElementPresent(secureSignIn))
								 {
									//Thread.sleep(1000);
									secureSignIn.click();
									log.add("Successful enter in Checkout page.");
									BNBasicCommonMethods.waitforElement(wait, obj, "Signoutbtn");
									//Thread.sleep(1000);
									
										WebElement CheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutBtn");
										if(BNBasicCommonMethods.isElementPresent(CheckoutBtn))
										{
	
											//System.out.println("In Checkout Flow");
											//Thread.sleep(1000);
										   //CheckoutBtn.click();
											log.add("Successful Enter in Add a Shipping and Billing Address page.");
											/*try 
											{*/
											pass("Page navigated to checkout edit billing and shipping address",log);
												//driver.navigate().refresh();
												CheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "CheckoutBtn");
												//Thread.sleep(1000);
												((JavascriptExecutor) driver).executeScript("arguments[0].click();", CheckoutBtn);
												//String CurrentUrl =  driver.getCurrentUrl();
												//CheckoutBtn.click();
												//driver.findElement(By.name("s")).sendKeys("\uE035");
												//driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
											//	driver.navigate().refresh();
												//
												//System.out.println("In try for Checkout page");
										/*		
											}
											
											catch(Exception e )
											{
												System.out.println("in catch block");
												String CurrentUrl =  driver.getCurrentUrl();
												driver.navigate().refresh();
												driver.get(driver.getCurrentUrl());
												driver.navigate().to(driver.getCurrentUrl());
												driver.findElement(By.id("Contact-us")).sendKeys(Keys.F5); 
												String CurrentUrl =  driver.getCurrentUrl();
												driver.findElement(By.name("s")).sendKeys("\uE035");
												Thread.sleep(1000);
												//driver.get(CurrentUrl);
												Thread.sleep(2000);
											}*/
										//	BNBasicCommonMethods.WaitForLoading(wait);
								  }

									
																	 }
								 else
								 {
									 fail("NO Successfull login to Checkout page");
								 }
						}
						else
						{
							fail("password field not found.");
							
						}
					}
					else
					{
						fail("Email Id field not found.");
						
					}
				}
				catch(Exception e)
				{
					System.out.println(e.getMessage());
					exception("Sign In to the User Account in Checkout Exception");
					fail(e.getMessage());
				}
			}
		}
	}

	public void meth()
	{
		
	}
/************************************ BNInstore Checkout Main Method to Call Test Cases ***********************************/
	
   @Test(priority = 1)
	public void BNIA_267_Guest_Shipping_and_Billing_Address_Entry_For_New_Customers() throws Exception 
	     {
	   	  sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
	   	  TempSignIn();
	     // addProduct();
	   	 productaddtobag();
	    	bagCount();
	   	  signInorCheckoutoverlay();
	   	  guestCheckoutoverlay();
	   	  guestCheckoutfields();
	   	  guestCheckoutTabKeyUse();
	   	 /* guestCheckoutCustomerServiceNavigation();
	   	  guestCheckoutTermsOfUseNavigation();
	   	  guestCheckoutTermsOfUseNavigation2();
	   	  guestCheckoutCopyrightsPageNavigation();
	   	  guestCheckoutPolicyPageNavigation();*/
	   	  guestCheckoutBlankSaveAlertValidation();
	   	  guestCheckoutDefaultCountry();
	   	  guestCheckoutDefaultText();
	   	  guestCheckoutFirstNameLengthValidation();
	   	  guestCheckoutLastNameLengthValidation();
	   	  guestCheckoutStreetAddressLengthValidation();
	   	  guestCheckoutAptSuiteAddressLengthValidation();
	   	  guestCheckoutCityLengthValidation();
	   	  guestCheckoutStateSelectionValidation();
	   	  guestCheckoutZipCodeLengthValidation();
	   	  guestCheckoutPhoneNumberLengthValidation();
	   	  guestCheckoutCompanyLengthValidation();
	   	  guestCheckoutInvalidFieldDataValidation();
	   	  guestCheckoutEnterdatas();
	   	  guestCheckoutAddressVerification();
	   	  guestCheckoutAddressVerificationEdit();
	   	  guestCheckoutDeliveryPageNavigation();
	   	  guestCheckoutBottomSummarySectionDetails();

	   	  guestCheckoutBreadcrumHighlight();
	   	  /*guestCheckoutOtherwaysToPay();
	   	  guestCheckoutOtherwaysToPayNavigation();*/
	   	  System.out.println("Guest Shipping and Billing Address");
	     }
	     
	 @Test(priority = 2)
	public void BNIA_296_Guest_Delivery_Options() throws Exception 
		{
		 /* TempSignIn();
		  productaddtobag();
	      bagCount();
	   	  signInorCheckoutoverlay();
	   	  guestCheckoutoverlay();
	   	  guestCheckoutEnterdata();
	      guestCheckoutDeliveryPageNavigation();*/
		  tempGuestNavigation();
	      sheet = BNBasicCommonMethods.excelsetUp("Guest Delivery Options");
		  guestCheckoutDeliveryPage();
		  guestCheckoutDeliveryPageSummaryContainer();
		  guestCheckoutDeliveryPreferenceOptions();
		  guestCheckoutDeliveryPreferenceDefaultSelection();
		  guestCheckoutDeliveryPreferenceHighlight();
		  guestCheckoutDeliveryShippingMethods();
		  guestCheckoutDeliveryShippingMethodsDefaultSelection();
		  guestCheckoutShippingMethodsHighlight();
		  guestCheckoutCartItemDetails();
		  guestCheckoutSummaryContainerPriceDetails();
		  guestCheckoutDeliveryEstimatedTax();
		  guestCheckoutDeliverySummaryContainerEstimatedShipping();
		  guestCheckoutDeliveryOrderTotal();
		  guestCheckoutBreadcrumHighlight1();
		  guestCheckoutPaymentsPageNavigation();
		  
		  System.out.println("Guest Delivery Option");
		}

    @Test(priority = 3)
	public void BNIA_298_Payment_Info() throws Exception 
		  {
			/*  TempSignIn();
			  productaddtobag();
		      bagCount();
		   	  signInorCheckoutoverlay();
		   	  guestCheckoutoverlay();
		   	  guestCheckoutEnterdata();
		   	  guestCheckoutPaymentPageNavigation();*/
    		  temppaymentNavigation();
			  sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
			//Guest User
			  payInfoGuestPaymentDefaultSelection();
			  payInfoCCTabHighlight();
			  payInfoGuestPaymentPageElements();
			  payInfoCCBreadCrumHighlight();
			  payInfoGuestCCElements();
			  payInfoGuestCCEmptyValidation();
			  payInfoCCInvalidCardValidation();
			  payInfoGuestCCNumberLengthValidation();
			  payInfoGuestCCNameLengthValidation();
			  payInfoCCInvalidNameValidation();
			  payInfoCCCsvLengthValidation();
			  payInfoGuestCCMonthYearDefaultText();
			  payInfoGuestCCardMonthList();
			  payInfoGuestCCardYearList();
			  payInfoCCiicon();
			  payInfoCCShipsAddressInvalidEmailIdValidation();
			  payInfoCCShipsAddressEmailId();
			  payInfoCCShipsameasdefault();
			  payInfoCCShipsAddressOverlay();
			  payInfoCCShipAddressDetails();
			  payInfoCCCouponConatainer();
			//payInfoCCCouponContainerSectionDetails(); //out of scope
			  payInfoCCGiftCardConatainer();
			  payInfoCCGiftCardLengthValidationandDefaultText();
			  payInfoCCGiftCardInvalidValidation();
			//payInfoCCGiftCardClose();//out of scope
			  payInfoCCGiftCouponCodeFields();
			  payInfoCCGiftCouponCodeInvalidValidation();
			//payInfoCCGiftCouponCodeClose(); //out of scope 
			  payInfoCCBookfairContainer();
			  payInfoCCBookfairValidation();
			  payInfoCCBookfairLengthValidation();
			//payInfoCCGiftBookFairClose();
			  payInfoCCGiftSummaryContainerSubtotal();
			  payInfoCCGiftSummaryContainerEstimatedShip();
			  payInfoCCGiftSummaryContainerEstimatedTax();
			  payInfoCCGiftSummaryContainerOrderTotal();
			  payInfoCCReviewOrderPageNavigation();
			  
			  System.out.println("Guest Payment Info");
			  
		  }
	  
/************************************************* Guest Checkout ***********************************************************************/
	
	// Fields Empty Value
	public void chkfieldvalues() throws Exception
	{
		WebElement fName = BNBasicCommonMethods.findElement(driver, obj, "fName");
		if(BNBasicCommonMethods.isElementPresent(fName))
		{
			if(fName.getAttribute("value").isEmpty())
			{
				log.add("The Value in the First Name field is : " + fName.getAttribute("value").toString());
				fail("The First name field is displayed and the value is empty.",log);
			}
			else
			{
				log.add("The Value in the First Name field is : " + fName.getAttribute("value").toString());
				pass("The First Name field is displayed.",log);
			}
		}
		else
		{
			fail("There is something wrong. First Name field is not displayed." );
		}
		
		WebElement lName = BNBasicCommonMethods.findElement(driver, obj, "lName");
		if(BNBasicCommonMethods.isElementPresent(lName))
		{
			if(lName.getAttribute("value").isEmpty())
			{
				log.add("The Value in the Last Name field is : " + lName.getAttribute("value").toString());
				fail("The Last name field is displayed and the value is empty.",log);
			}
			else
			{
				log.add("The Value in the Last Name field is : " + lName.getAttribute("value").toString());
				pass("The Last Name field is displayed and there is value present.",log);
			}
		}
		else
		{
			fail("There is something wrong. Last Name field is not displayed." );
		}
		
		WebElement stAddress = BNBasicCommonMethods.findElement(driver, obj, "stAddress");
		BNBasicCommonMethods.scrolldown(stAddress, driver);
		if(BNBasicCommonMethods.isElementPresent(stAddress))
		{
			if(stAddress.getAttribute("value").isEmpty())
			{
				log.add("The Value in the Street Address field is : " + stAddress.getAttribute("value").toString());
				fail("The Street Address field is displayed and the value is empty.",log);
			}
			else
			{
				log.add("The Value in the Street Address field is : " + stAddress.getAttribute("value").toString());
				pass("The Street Address field is displayed and there is value present in it.",log);
			}
		}
		else
		{
			fail("There is something wrong. Street Address field is not displayed." );
		}
		
		WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
		if(BNBasicCommonMethods.isElementPresent(city))
		{
			if(city.getAttribute("value").isEmpty())
			{
				log.add("The Value in the City field is : " + city.getAttribute("value").toString());
				fail("The City field is displayed and the value is empty.",log);
			}
			else
			{
				log.add("The Value in the City field is : " + city.getAttribute("value").toString());
				pass("The City field is displayed and there is value in it.",log);
			}
		}
		else
		{
			fail("There is something wrong. City Name field is not displayed." );
		}
		
		WebElement zipCode = BNBasicCommonMethods.findElement(driver, obj, "zipCode");
		if(BNBasicCommonMethods.isElementPresent(zipCode))
		{
			if(zipCode.getAttribute("value").isEmpty())
			{
				log.add("The Value in the Zipcode field is : " + zipCode.getAttribute("value").toString());
				fail("The Zipcode field is displayed and the value is empty.",log);
			}
			else
			{
				log.add("The Value in the Zipcode field is : " + zipCode.getAttribute("value").toString());
				pass("The Zipcode field is displayed and there is value in it.",log);
			}
		}
		else
		{
			fail("There is something wrong. Zipcode field is not displayed." );
		}
		
		WebElement Statecontainer = BNBasicCommonMethods.findElement(driver, obj, "Statecontainer");
		Statecontainer.click();
		List<WebElement> stateLst = driver.findElements(By.xpath("//*[@id='state-option-list']//li"));
		//driver.findElement(By.xpath("//*[@id='state-option-35']")).click();
		//System.out.println(stateLst.get(35).getText());
		stateLst.get(35).click();
		if(BNBasicCommonMethods.isElementPresent(Statecontainer))
		{
			Statecontainer.click();
			/*Select sel = new Select(state);*/
			if(stateLst.get(35).getText()!=null)
			{
				log.add("The Selected Value in the State field is : " + stateLst.get(35).getText());
				pass("The State field is not empty and the State is selected.",log);
				Statecontainer.click();
			}
			else
			{
				log.add("The Selected Value in the State field is : " + stateLst.get(35).getText());
				fail("The State is not selected and the value is empty.",log);
			}
		}
		else
		{
			fail("There is something wrong. State drop down is not displayed." );
		}
		
		WebElement contactNo = BNBasicCommonMethods.findElement(driver, obj, "contactNo");
		if(BNBasicCommonMethods.isElementPresent(contactNo))
		{
			if(contactNo.getAttribute("value").isEmpty())
			{
				log.add("The Value in the Contact Number field is : " + contactNo.getAttribute("value").toString());
				fail("The Contact Number field is displayed and the value is empty.",log);
			}
			else
			{
				log.add("The Value in the Contact Number field is : " + contactNo.getAttribute("value").toString());
				pass("The Contact Number field is displayed and there is value present in it.",log);
			}
		}
		else
		{
			fail("There is something wrong. Contact Number field is not displayed." );
		}
	}
	
	// Color Finder	
	public String colorfinder(WebElement element)
	{
		String ColorName = element.getCssValue("background-color");
		Color colorhxcnvt = Color.fromString(ColorName);
		String hexCode = colorhxcnvt.asHex();
		return hexCode;
	}
	
	
	// Compare the Bottom Container Fields 
	public void valcompare(String[] det, String[] val, String subtotallabelname, String subtotalvalue, String estimatedShiplabel, String estimatedshipvalue, String estimatedTaxlabel, String estimatedTaxvalue, String OrderTotallabel, String OrderTotalvalue)
	{
		if(det[0].equalsIgnoreCase(subtotallabelname))
		{
			log.add("The Subtotal Value and it is dispalyed.");
			log.add("The Expected content was : " + subtotallabelname);
			log.add("The Actual content was : " + det[0]);
			//System.out.println(subtotallabelname);
			//System.out.println(det[0]);
		
		}
		else
		{
			log.add("The Subtotal Value and it is dispalyed.");
			log.add("The Expected content was : " + subtotallabelname);
			log.add("The Actual content was : " + det[0]);
			//System.out.println(subtotallabelname);
			//System.out.println(det[0]);
		
		}
		
		if(val[0].contains(subtotalvalue))
		{
			log.add("The Subtotal Amount is dispalyed.");
			log.add("The Expected content was : " + subtotalvalue);
			log.add("The Actual content was : " + val[0]);
			//System.out.println(subtotalvalue);
			//System.out.println(val[0]);
		
		}
		else
		{
			log.add("The Subtotal Value and Line is dispalyed.");
			log.add("The Expected content was : " + subtotalvalue);
			log.add("The Actual content was : " + val[0]);
			//System.out.println(subtotalvalue);
			//System.out.println(val[0]);
		
		}
		
		if(det[1].equalsIgnoreCase(estimatedShiplabel))
		{
			log.add("The Estimated Ship Label is dispalyed.");
			log.add("The Expected content was : " + estimatedShiplabel);
			log.add("The Actual content was : " + det[1]);
			//System.out.println(estimatedShiplabel);
			//System.out.println(det[1]);
		
		}
		else
		{
			log.add("The Estimated Ship Label is dispalyed.");
			log.add("The Expected content was : " + estimatedShiplabel);
			log.add("The Actual content was : " + det[1]);
			//System.out.println(estimatedShiplabel);
			//System.out.println(det[1]);
		
		}
		
		if(val[1].equalsIgnoreCase(estimatedshipvalue))
		{
			log.add("The Estimated Ship value is dispalyed.");
			log.add("The Expected content was : " + estimatedshipvalue);
			log.add("The Actual content was : " + val[1]);
		
		}
		else
		{
			log.add("The Estimated Ship value is dispalyed.");
			log.add("The Expected content was : " + estimatedshipvalue);
			log.add("The Actual content was : " + val[1]);
		
		}
		
		if(det[2].equalsIgnoreCase(estimatedTaxlabel))
		{
			log.add("The Estimated Tax is dispalyed.");
			log.add("The Expected content was : " + estimatedTaxlabel);
			log.add("The Actual content was : " + det[2]);
		
		}
		else
		{
			log.add("The Estimated Tax is dispalyed.");
			log.add("The Expected content was : " + estimatedTaxlabel);
			log.add("The Actual content was : " + det[2]);
		}
		
		if(val[2].contains(estimatedTaxvalue))
		{
			log.add("The Estimated Tax value is dispalyed.");
			log.add("The Expected content was : " + estimatedTaxvalue);
			log.add("The Actual content was : " + val[2]);
		}
		else
		{
			log.add("The Estimated Tax value is dispalyed.");
			log.add("The Expected content was : " + estimatedTaxvalue);
			log.add("The Actual content was : " + val[2]);
		}
		
		if(det[3].equalsIgnoreCase(OrderTotallabel))
		{
			log.add("The Order Total is dispalyed.");
			log.add("The Expected content was : " + OrderTotallabel);
			log.add("The Actual content was : " + det[3]);
		}
		else
		{
			log.add("The Order Total is dispalyed.");
			log.add("The Expected content was : " + OrderTotallabel);
			log.add("The Actual content was : " + det[3]);
		}
		
		if(val[3].contains(OrderTotalvalue))
		{
			log.add("The Order Total value is dispalyed.");
			log.add("The Expected content was : " + OrderTotalvalue);
			log.add("The Actual content was : " + val[3]);
		
		}
		else
		{
			log.add("The Order Total value is dispalyed.");
			log.add("The Expected content was : " + OrderTotalvalue);
			log.add("The Actual content was : " + val[3]);
		
		}
	}
	
	public void productaddtobag()
	{
		
		
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String EANs = BNBasicCommonMethods.getExcelNumericVal("BRM623", sheet, 3);
			String EAN[] = EANs.split("\n");
		  for(int ctr=0 ; ctr<EAN.length; ctr++)
  		  {
			BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
			WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
			BNLogo.click();
			BNBasicCommonMethods.waitforElement(wait, obj, "SearchTile");
  		   	WebElement SearchTile = BNBasicCommonMethods.findElement(driver, obj, "SearchTile");
  		   	SearchTile.click();
  		   //	Thread.sleep(3000);
  		   	BNBasicCommonMethods.waitforElement(wait, obj, "SearchTxtBxnew");
  		   	WebElement SearchTxtBxnew = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");
  		   	SearchTxtBxnew.clear();
			SearchTxtBxnew.sendKeys(EAN[ctr]);
			SearchTxtBxnew.sendKeys(Keys.ENTER);
			Thread.sleep(1000);
			String url = driver.getCurrentUrl();
			driver.get(url);
			BNBasicCommonMethods.waitforElement(wait, obj, "PDPAddToBag");
			WebElement PDPAddToBag = BNBasicCommonMethods.findElement(driver, obj, "PDPAddToBag");
			PDPAddToBag.click();
			int counter=1;
				do
				{
  				try
  				{
  					/*BNBasicCommonMethods.waitforElement(wait, obj, "ATBBtn");
		  			WebElement ATBBtn = BNBasicCommonMethods.findElement(driver, obj, "ATBBtn");
		  			Thread.sleep(1000);
  					ATBBtn.click();*/
  					//Thread.sleep(4000);
  					BNBasicCommonMethods.waitforElement(wait, obj, "ATBSuccessAlertOverlay");
	  				//WebElement ATBSuccessAlertOverlay = BNBasicCommonMethods.findElement(driver, obj, "ATBSuccessAlertOverlay");
	  				counter=5;
  				}
  				
  				catch(Exception e)
  				{
  					WebElement ATBErrorAlt = driver.findElement(By.xpath("//*[@id='skMob_ErrorsDiv_id']"));
  					if(ATBErrorAlt.isDisplayed())
  					{
  						WebElement ErroBtn = driver.findElement(By.xpath("//*[@id='skMob_ErrorsOK_id']"));
  						ErroBtn.click();
  					}
  					String str1 = driver.getCurrentUrl();
  					driver.get(str1);
  			    }
  				counter++;
				} while(counter == 5 );
				
				WebElement ATBSuccessAlertOverlay = BNBasicCommonMethods.findElement(driver, obj, "ATBSuccessAlertOverlay");
  				if(BNBasicCommonMethods.isElementPresent(ATBSuccessAlertOverlay))
  				{
  					BNBasicCommonMethods.waitforElement(wait, obj, "ContineShoppingBtn");
  					WebElement ContineShoppingBtn = BNBasicCommonMethods.findElement(driver, obj, "ContineShoppingBtn");
  					ContineShoppingBtn.click();
  					BNBasicCommonMethods.waitforElement(wait, obj, "PDPAddToBag");
  					PDPAddToBag = BNBasicCommonMethods.findElement(driver, obj, "PDPAddToBag");
  					PDPAddToBag.click();
  					BNBasicCommonMethods.waitforElement(wait, obj, "ContineShoppingBtn");
  					 ContineShoppingBtn = BNBasicCommonMethods.findElement(driver, obj, "ContineShoppingBtn");
   					ContineShoppingBtn.click();
  					
  					//pass("Add to Bag Success alert overlay is displayed ");
  				}
  				
  				else
  				{
  					fail("Add to bag Success alert overlay is not displayed",log);
  				}	
			
		   }
		  
		}
		
		catch(Exception e)
		{
			exception("Exception , please check while adding the prodcut in the bag "+ e.getMessage());
		}
	}
	
	/*BNIA-781 Verify that when the "Shopping Bag/Add to Bag" icon is clicked in the header it should naviagte to "Shopping Bag" page.*/
	/*Check for Bag Count*/
	public boolean bagCount() throws Exception
	{
	    ChildCreation("BNIA-781 Verify that when the Shopping Bag/Add to Bag icon is clicked in the header it should naviagte to Shopping Bag page.");
		boolean prdtadded = false;
		int count = 1;
		do
		{
			WebElement ShoppingBagCount = BNBasicCommonMethods.findElement(driver, obj, "ShoppingBagCount");
			int prdtval = Integer.parseInt(ShoppingBagCount.getText());
			if(prdtval>0)
			{
				prdtadded=true;
				WebElement ShoppingBagIcon = BNBasicCommonMethods.findElement(driver, obj, "ShoppingBagIcon");
				ShoppingBagIcon.click();
				//Thread.sleep(4000);
				pass("Shopping bag page is displayed and with Added products");
				BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
				WebElement LoginCheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "LoginCheckoutBtn");
				if(LoginCheckoutBtn.isDisplayed())
				{
					pass("Shopping bag page is displayed with added Item");
				}
				else
				{
					fail("Shopping bag page is not displayed with added items");
				}
				wait.until(ExpectedConditions.visibilityOf(LoginCheckoutBtn));
				LoginCheckoutBtn.click();
				//Thread.sleep(1000);	
				//Thread.sleep(3000);
				BNBasicCommonMethods.waitforElement(wait, obj, "signInIframe");
				WebElement signInIframe = BNBasicCommonMethods.findElement(driver, obj, "signInIframe");
				driver.switchTo().frame(signInIframe);
				//wait.until(ExpectedConditions.attributeToBe(miniCartLoadingGauge, "style", "display: none;"));
				//wait.until(ExpectedConditions.visibilityOf(secureCheckOutBtn));
				break;
			}
			else
			{
				count++;
				prdtadded=false;
				productaddtobag();
				continue;
			}
		}while(prdtadded==true||count<10);
		return prdtadded;
	}
	
	/* BNIA-971 Guest Checkout - Verify that on selecting the "Checkout" button the Sign In overlay should be enabled to Proceed for the Sign in/Check out as Guest checkout page.*/
	public void signInorCheckoutoverlay()
	{
		ChildCreation(" BNIA-971 Guest Checkout - Verify that on selecting the Checkout button the Sign In overlay should be enabled to Proceed for the Sign in/Check out as Guest checkout page.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String pgcaption = BNBasicCommonMethods.getExcelVal("BRM623", sheet, 2);
			String[] title = pgcaption.split("\n");
			//addProduct(sheet, Child, false);
			//Thread.sleep(2000);
			//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(checkoutpageframe));
			BNBasicCommonMethods.waitforElement(wait, obj, "pgetitle");
			WebElement pgetitle = BNBasicCommonMethods.findElement(driver, obj, "pgetitle");
			wait.until(ExpectedConditions.visibilityOf(pgetitle));
			if(pgetitle.getText().equals(title[0]))
			{
				//System.out.println(pgetitle.getText());
				log.add("The Page title is : " + pgetitle.getText());
				pass("The user is navigated to the Checkout page and the caption matches with the expected one.",log);
			}
			else
			{
				log.add("The Page title is : " + pgetitle.getText());
				log.add("The expected page title is " + title[0]);
				fail("The user is navigated to the Checkout page and the caption does not matches with the expected one.",log);
			}
			
			WebElement checkoutAsGuestbtn = BNBasicCommonMethods.findElement(driver, obj, "checkoutAsGuestbtn");
			if(BNBasicCommonMethods.isElementPresent(checkoutAsGuestbtn))
			{
				pass("The Checkout as Guset button is visible.");
				if(checkoutAsGuestbtn.getText().equals(title[1]))
				{
					pass("The Checkout as Guest button caption matches with the expected one.");
				}
				else
				{
				fail("The Checkout as Guest button caption does not matches with the expected one.");
				}
			}
			else
			{
				fail("The Checkout as Guest Button is not visible / present.");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong.Please Check." + e.getMessage());
			System.out.println(e.getMessage());
		}
	}

	 /* BNIA-972 Guest Checkout - Verify that Shipping section should be displayed as per the creative in the guest checkout page*/
	public void guestCheckoutoverlay()
	{
		ChildCreation(" BNIA-972 Guest Checkout - Verify that Shipping section should be displayed as per the creative in the guest checkout page.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String pgcaption = BNBasicCommonMethods.getExcelVal("BRM627", sheet, 2);
			String expcolor = BNBasicCommonMethods.getExcelVal("BRM627", sheet, 3);
			String[] title = pgcaption.split("\n");
			WebElement checkoutAsGuestbtn = BNBasicCommonMethods.findElement(driver, obj, "checkoutAsGuestbtn");
			if(BNBasicCommonMethods.isElementPresent(checkoutAsGuestbtn))
			{
				checkoutAsGuestbtn.click();
			   // Thread.sleep(2000);
			    BNBasicCommonMethods.waitforElement(wait, obj, "guestCheckoutAddPageTitle");
				WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
				wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
				//Thread.sleep(1000);
				
				if(BNBasicCommonMethods.isElementPresent(guestCheckoutAddPageTitle))
				{
					log.add("The User is naviagted to the Guest Checkout Page.");
					if(guestCheckoutAddPageTitle.getText().contains(title[0]))
					{
						log.add("The expected page title was : " + title[0]);
						log.add("The actual page title is : " + guestCheckoutAddPageTitle.getText());
						pass("The Guest Checkout Page title matches the expected content.",log);
					}
					else
					{
						System.out.println(guestCheckoutAddPageTitle.getText());
						log.add("The expected page title was : " + title[0]);
						log.add("The actual page title is : " + guestCheckoutAddPageTitle.getText());
						fail("The Guest Checkout Page title does not matches the expected content.",log);
					}
					
					WebElement checkoutSteps = BNBasicCommonMethods.findElement(driver, obj, "checkoutSteps");
					if(BNBasicCommonMethods.isElementPresent(checkoutSteps))
					{
						pass("The Checkout Steps container is displayed.");
						List <WebElement> checkoutStepsLists = driver.findElements(By.xpath("//*[@id='checkoutSteps']//a"));
						if(BNBasicCommonMethods.isListElementPresent(checkoutStepsLists))
						{
							for(int j = 1; j<=checkoutStepsLists.size();j++)
							{
								WebElement chksteps = driver.findElement(By.xpath("(//*[@id='checkoutSteps']//a)["+j+"]"));
								if(chksteps.getText().contains(title[j]))
								{
									log.add("The exected " + j + " step in the Checkout Steps is : " + title[j]);
									log.add("The actual " + j + " step in the Checkout Steps is : " + chksteps.getText());
									pass("The Checkout Steps " + j + " matches with the expected one.",log);
								}
								else
								{
									log.add("The exected " + j + " step in the Checkout Steps is : " + title[j]);
									log.add("The actual " + j + " step in the Checkout Steps is : " + chksteps.getText());
									fail("The Checkout Steps " + j + " does not matches with the expected one.",log);
								}
							}
						}
						else
						{
							fail("The Checkout Steps List is not displayed.Please Check.");
						}
					}
					else
					{
						fail("The Checkout Steps container is not displayed.");
					}
					
					WebElement countrySelection = BNBasicCommonMethods.findElement(driver, obj, "countrySelection");
					if(BNBasicCommonMethods.isElementPresent(countrySelection))
					{
						pass("The Country drop down list box is present and visible.");
					}
					else
					{
						fail("The Country drop down list box is not present / visible.");
					}
					
					WebElement fName = BNBasicCommonMethods.findElement(driver, obj, "fName");
					if(BNBasicCommonMethods.isElementPresent(fName))
					{
						pass("The First Name text field is present and visible.");
					}
					else
					{
						fail("The First Name text field is not present / visible.");
					}
					
					WebElement lName = BNBasicCommonMethods.findElement(driver, obj, "lName");
					if(BNBasicCommonMethods.isElementPresent(lName))
					{
						pass("The Last Name text field is present and visible.");
					}
					else
					{
						fail("The Last Name text field is not present / visible.");
					}
					
					WebElement stAddress = BNBasicCommonMethods.findElement(driver, obj, "stAddress");
					if(BNBasicCommonMethods.isElementPresent(stAddress))
					{
						pass("The Stree Address text field is present and visible.");
					}
					else
					{
						fail("The Stree Address text field is not present / visible.");
					}
					
					WebElement aptSuiteAddress = BNBasicCommonMethods.findElement(driver, obj, "aptSuiteAddress");
					BNBasicCommonMethods.scrolldown(aptSuiteAddress, driver);
					if(BNBasicCommonMethods.isElementPresent(aptSuiteAddress))
					{
						pass("The Apt/Suite text field is present and visible.");
					}
					else
					{
						fail("The Apt/Suite text field is not present / visible.");
					}
					
					WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
					if(BNBasicCommonMethods.isElementPresent(city))
					{
						pass("The City text field is present and visible.");
					}
					else
					{
						fail("The City text field is not present / visible.");
					}
					
					WebElement state = BNBasicCommonMethods.findElement(driver, obj, "state");
					if(BNBasicCommonMethods.isElementPresent(state))
					{
						pass("The State selection drop down is present and visible.");
					}
					else
					{
						fail("The State selection drop down is not present / visible.");
					}
					
					WebElement zipCode = BNBasicCommonMethods.findElement(driver, obj, "zipCode");
					if(BNBasicCommonMethods.isElementPresent(zipCode))
					{
						pass("The Zipcode text field is present and visible.");
					}
					else
					{
						fail("The Zipcode text field is not present / visible.");
					}
					
					WebElement contactNo = BNBasicCommonMethods.findElement(driver, obj, "contactNo");
					if(BNBasicCommonMethods.isElementPresent(contactNo))
					{
						pass("The Phone Number text field is present and visible.");
					}
					else
					{
						fail("The Phone Number text field is not present / visible.");
					}
					
					WebElement companyName = BNBasicCommonMethods.findElement(driver, obj, "companyName");
					if(BNBasicCommonMethods.isElementPresent(companyName))
					{
						pass("The Company Name text field is present and visible.");
					}
					else
					{
						fail("The Company Name text field is not present / visible.");
					}
					
					WebElement summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
					if(BNBasicCommonMethods.isElementPresent(summaryContainer))
					{
						pass("The Summary Container area is present and visible.");
					
					}
					else
					{
						fail("The Summary Container area is not present / visible.");
					
					}
					
					WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
					if(BNBasicCommonMethods.isElementPresent(orderContinuebtn))
					{
						pass("The Continue button is present and visible.");
						if(BNBasicCommonMethods.colorfindernew(orderContinuebtn).equals(expcolor))
						{
							log.add("The expected color of the Contintue button is : " + BNBasicCommonMethods.colorfindernew(orderContinuebtn).toString());
							log.add("The actual color of the Continue button is : " + expcolor);
							pass("The Continue button color matches with the expected One.",log);
						}
						else
						{
							log.add("The expected color of the Contintue button is : " + BNBasicCommonMethods.colorfindernew(orderContinuebtn).toString());
							log.add("The actual color of the Continue button is : " + expcolor);
							fail("The Continue button color does not matches with the expected One.",log);
						}
					}
					else
					{
						fail("The Summary Container area is not present / visible.");
					}
				}
				else
				{
					fail("The User is not naviagted to the Guest Checkout Page.");
				}
			}
			else
			{
				exception("The Checkout as Guest button is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception(" There is something wrong.Please Check." + e.getMessage());
		
		}
	}

	 /*BNIA-976 Guest Checkout - Verify that to add a new shipping address the required input fields should be displayed*/
	public void guestCheckoutfields()
	{
		ChildCreation("BNIA-976 Guest Checkout - Verify that to add a new shipping address the required input fields should be displayed.");
		//log.add("The browser is launched and it is running.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
			BNBasicCommonMethods.scrollup(guestCheckoutAddPageTitle, driver);
			WebElement countrySelection = BNBasicCommonMethods.findElement(driver, obj, "countrySelection");
			if(BNBasicCommonMethods.isElementPresent(countrySelection))
			{
				pass("The Country drop down list box is present and visible.");
			}
			else
			{
				fail("The Country drop down list box is not present / visible.");
			}
			
			WebElement fName = BNBasicCommonMethods.findElement(driver, obj, "fName");
			if(BNBasicCommonMethods.isElementPresent(fName))
			{
				pass("The First Name text field is present and visible.");
			}
			else
			{
				fail("The First Name text field is not present / visible.");
			}
			
			WebElement lName = BNBasicCommonMethods.findElement(driver, obj, "lName");
			if(BNBasicCommonMethods.isElementPresent(lName))
			{
				pass("The Last Name text field is present and visible.");
			}
			else
			{
				fail("The Last Name text field is not present / visible.");
			}
			
			WebElement stAddress = BNBasicCommonMethods.findElement(driver, obj, "stAddress");
			if(BNBasicCommonMethods.isElementPresent(stAddress))
			{
				pass("The Stree Address text field is present and visible.");
			}
			else
			{
				fail("The Stree Address text field is not present / visible.");
			}
			
			WebElement aptSuiteAddress = BNBasicCommonMethods.findElement(driver, obj, "aptSuiteAddress");
			BNBasicCommonMethods.scrolldown(aptSuiteAddress, driver);
			if(BNBasicCommonMethods.isElementPresent(aptSuiteAddress))
			{
				pass("The Apt/Suite text field is present and visible.");
			}
			else
			{
				fail("The Apt/Suite text field is not present / visible.");
			}
			
			WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
			if(BNBasicCommonMethods.isElementPresent(city))
			{
				pass("The City text field is present and visible.");
			}
			else
			{
				fail("The City text field is not present / visible.");
			}
			
			WebElement state = BNBasicCommonMethods.findElement(driver, obj, "state");
			if(BNBasicCommonMethods.isElementPresent(state))
			{
				pass("The State selection drop down is present and visible.");
			}
			else
			{
				fail("The State selection drop down is not present / visible.");
			}
			
			WebElement zipCode = BNBasicCommonMethods.findElement(driver, obj, "zipCode");
			if(BNBasicCommonMethods.isElementPresent(zipCode))
			{
				pass("The Zipcode text field is present and visible.");
			}
			else
			{
				fail("The Zipcode text field is not present / visible.");
			}
			
			WebElement contactNo = BNBasicCommonMethods.findElement(driver, obj, "contactNo");
			if(BNBasicCommonMethods.isElementPresent(contactNo))
			{
				pass("The Phone Number text field is present and visible.");
			}
			else
			{
				fail("The Phone Number text field is not present / visible.");
			}
			
			WebElement companyName = BNBasicCommonMethods.findElement(driver, obj, "companyName");
			if(BNBasicCommonMethods.isElementPresent(companyName))
			{
				pass("The Company Name text field is present and visible.");
			
			}
			else
			{
				fail("The Company Name text field is not present / visible.");
			
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong.Please Check." + e.getMessage());
			System.out.println(e.getMessage());
		}
	}

	 /*BNIA-975 Guest Checkout - Verify that guest user can able to add the new shipping address details in the shipping section*/
	public void guestCheckoutEnterdatas()
	{
		ChildCreation(" BNIA-975 Guest Checkout - Verify that guest user can able to add the new shipping address details in the shipping section.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String firstname = BNBasicCommonMethods.getExcelVal("BRM628", sheet, 6);
			String lastname = BNBasicCommonMethods.getExcelVal("BRM628", sheet, 7);
			String streetAdd = BNBasicCommonMethods.getExcelVal("BRM628", sheet, 8);
			String cty = BNBasicCommonMethods.getExcelVal("BRM628", sheet, 10);
			String zpcode = BNBasicCommonMethods.getExcelNumericVal("BRM628", sheet, 12);
			String cntnumber = BNBasicCommonMethods.getExcelNumericVal("BRM628", sheet, 13);
			WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
			BNBasicCommonMethods.scrollup(guestCheckoutAddPageTitle, driver);
			//Actions act = new Actions(driver);
			WebElement Countrycontainer = BNBasicCommonMethods.findElement(driver, obj, "countrycontiner");
			Countrycontainer.click();
			List<WebElement> countryLst = driver.findElements(By.xpath("//*[@id='country-option-list']//li"));
			//driver.findElement(By.xpath("//*[@id='state-option-35']")).click();
			//System.out.println(countryLst.get(0).getText());
			countryLst.get(0).click();
			/*act.moveToElement(countrySelection).build().perform();
			Select sel = new Select(countrySelection);*/
			/*sel.selectByValue("US");*/
			clearAll();
			WebElement fName = BNBasicCommonMethods.findElement(driver, obj, "fName");
			fName.sendKeys(firstname);
			WebElement lName = BNBasicCommonMethods.findElement(driver, obj, "lName");
			lName.sendKeys(lastname);
			WebElement stAddress = BNBasicCommonMethods.findElement(driver, obj, "stAddress");
			stAddress.sendKeys(streetAdd);
			WebElement companyName = BNBasicCommonMethods.findElement(driver, obj, "companyName");
			BNBasicCommonMethods.scrollup(companyName, driver);
			WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
			city.sendKeys(cty);
		//	WebElement state = BNBasicCommonMethods.findElement(driver, obj, "state");
			// Create instance of Javascript executor
			/*JavascriptExecutor je = (JavascriptExecutor) driver;
			//Identify the WebElement which will appear after scrolling down
			// now execute query which actually will scroll until that element is not appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);",state);
			*/
			WebElement Statecontainer = BNBasicCommonMethods.findElement(driver, obj, "Statecontainer");
			Statecontainer.click();
			JavascriptExecutor je = (JavascriptExecutor) driver;
			//Identify the WebElement which will appear after scrolling down
			// now execute query which actually will scroll until that element is not appeared on page.
			List<WebElement> stateLst = driver.findElements(By.xpath("//*[@id='state-option-list']//li"));
			je.executeScript("arguments[0].scrollIntoView(true);",stateLst.get(35));
			//driver.findElement(By.xpath("//*[@id='state-option-35']")).click();
			//System.out.println(stateLst.get(35).getText());
			stateLst.get(35).click();
			/*Select stsel = new Select(state);
			stsel.selectByIndex(35);*/
			WebElement zipCode = BNBasicCommonMethods.findElement(driver, obj, "zipCode");
			zipCode.sendKeys(zpcode);
			WebElement contactNo = BNBasicCommonMethods.findElement(driver, obj, "contactNo");
			contactNo.sendKeys(cntnumber);
			
			BNBasicCommonMethods.scrollup(guestCheckoutAddPageTitle, driver);
			
			if(fName.getAttribute("value").equals(firstname))
			{
				log.add("The entered value from the excel file was " + firstname);
				log.add("The current value in the First Name field is : " + fName.getAttribute("value").toString());
				pass("The user was able to enter the value in the First Name field.",log);
			}
			else
			{
				log.add("The entered value from the excel file was " + firstname);
				log.add("The current value in the First Name field is : " + fName.getAttribute("value").toString());
				fail("The user was able to enter the value in the First Name field but there is some mismatch.",log);
			}
			
			if(lName.getAttribute("value").equals(lastname))
			{
				log.add("The entered value from the excel file was " + lastname);
				log.add("The current value in the Last Name field is : " + lName.getAttribute("value").toString());
				pass("The user was able to enter the value in the Last Name field.",log);
			}
			else
			{
				log.add("The entered value from the excel file was " + lastname);
				log.add("The current value in the Last Name field is : " + lName.getAttribute("value").toString());
				fail("The user was able to enter the value in the Zipcode field but there is some mismatch.",log);
			}
			if(stAddress.getAttribute("value").equals(streetAdd))
			{
				log.add("The entered value from the excel file was " + streetAdd);
				log.add("The current value in the Street Address field is : " + stAddress.getAttribute("value").toString());
				pass("The user was able to enter the value in the Stree Address field.",log);
			}
			else
			{
				log.add("The entered value from the excel file was " + streetAdd);
				log.add("The current value in the Street Address field is : " + stAddress.getAttribute("value").toString());
				fail("The user was able to enter the value in the Zipcode field but there is some mismatch.",log);
			}
			
			WebElement aptSuiteAddress = BNBasicCommonMethods.findElement(driver, obj, "aptSuiteAddress");
			BNBasicCommonMethods.scrolldown(aptSuiteAddress, driver);
			if(city.getAttribute("value").equals(cty))
			{
				log.add("The entered value from the excel file was " + cty);
				log.add("The current value in the City field is : " + city.getAttribute("value").toString());
				pass("The user was able to enter the value in the City field.",log);
			}
			else
			{
				log.add("The entered value from the excel file was " + cty);
				log.add("The current value in the City field is : " + city.getAttribute("value").toString());
				fail("The user was able to enter the value in the Zipcode field but there is some mismatch.",log);
			}
			
			Statecontainer = BNBasicCommonMethods.findElement(driver, obj, "Statecontainer");
			Statecontainer.click();
			stateLst = driver.findElements(By.xpath("//*[@id='state-option-list']//li"));
			//driver.findElement(By.xpath("//*[@id='state-option-35']")).click();
			//System.out.println(stateLst.get(35).getText());
			if(stateLst.get(35).getText()!=null)
			{
				log.add("The current value in the State Name field is : " + stateLst.get(35).getText());
				pass("The user was able to Select the State.",log);
			}
			else
			{
				log.add("The current value in the State Name field is : " + stateLst.get(35).getText());
				fail("The State is not selected.",log);
				
			}
			if(zipCode.getAttribute("value").toString().equals(zpcode))
			{
				log.add("The entered value from the excel file was " + zpcode);
				log.add("The current value in the Zipcode field is : " + zipCode.getAttribute("value").toString());
				pass("The user was able to enter the value in the Zipcode field.",log);
			}
			else
			{
				log.add("The entered value from the excel file was " + zpcode);
				log.add("The current value in the Zipcode field is : " + zipCode.getAttribute("value").toString());
				fail("The user was able to enter the value in the Zipcode field but there is some mismatch.",log);
			}
			
			
			if(contactNo.getAttribute("value").equals(cntnumber))
			{
				log.add("The entered value from the excel file was " + cntnumber);
				log.add("The current value in the Contact Number field is : " + contactNo.getAttribute("value").toString());
				pass("The user was able to enter the value in the Contact Number field.",log);
			}
			else
			{
				log.add("The entered value from the excel file was " + cntnumber);
				log.add("The current value in the Contact Number field is : " + contactNo.getAttribute("value").toString());
				fail("The user was able to enter the value in the Zipcode field but there is some mismatch.",log);
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception(" There is something wrong.Please Check." + e.getMessage());
			
		}
	}
		
	/* BNIA-977 Guest Checkout - Verify that default text should be displayed in all the input fields in the shipping address section for the guest checkout user*/ 
	public void guestCheckoutDefaultText()
	{
		ChildCreation(" BNIA-977 Guest Checkout - Verify that default text should be displayed in all the input fields in the shipping address section for the guest checkout user.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String fNametext = BNBasicCommonMethods.getExcelVal("BRM630", sheet, 6);
			String lNametext = BNBasicCommonMethods.getExcelVal("BRM630", sheet, 7);
			String stNametext = BNBasicCommonMethods.getExcelVal("BRM630", sheet, 8);
			String suiteNametext = BNBasicCommonMethods.getExcelVal("BRM630", sheet, 9);
			String cityNametxt = BNBasicCommonMethods.getExcelVal("BRM630", sheet, 10);
			String stateNametext = BNBasicCommonMethods.getExcelVal("BRM630", sheet, 11);
			String zipcodeNametext = BNBasicCommonMethods.getExcelVal("BRM630", sheet, 12);
			String phoneNumbertext = BNBasicCommonMethods.getExcelVal("BRM630", sheet, 13);
			String companyNametext = BNBasicCommonMethods.getExcelVal("BRM630", sheet, 14);
			WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
			BNBasicCommonMethods.scrollup(guestCheckoutAddPageTitle, driver);
			WebElement fNametxt = BNBasicCommonMethods.findElement(driver, obj, "fNametxt");
			if(BNBasicCommonMethods.isElementPresent(fNametxt))
			{
				if(fNametxt.getText().equals(fNametext))
				{
					log.add("The current value in the First Name text field is : " + fNametxt.getText());
					pass("The First Name text is present and the value matches with the expected one.",log);
				}
				else
				{
					fail("The First Name text is missing or the value does not matches with the expected one. The current value in the text field is " + fNametxt.getText());
				}
			}
			else
			{
				fail("The First Name inline text is not found.");
			}
			
			WebElement lNametxt = BNBasicCommonMethods.findElement(driver, obj, "lNametxt");
			if(BNBasicCommonMethods.isElementPresent(lNametxt))
			{
				if(lNametxt.getText().equals(lNametext))
				{
					log.add("The current value in the Last Name text field is : " + lNametxt.getText());
					pass("The Last Name text is present and the value matches with the expected one.",log);
				}
				else
				{
					fail("The Last Name text is missing or the value does not matches with the expected one. The current value in the text field is " + lNametxt.getText());
				}
			}
			else
			{
				fail("The Last Name inline text is not found.");
			}
			
			WebElement stAddresstxt  = BNBasicCommonMethods.findElement(driver, obj, "stAddresstxt");
			if(BNBasicCommonMethods.isElementPresent(stAddresstxt))
			{
				if(stAddresstxt.getText().equals(stNametext))
				{
					log.add("The current value in the Street Name text field is : " + stAddresstxt.getText());
					pass("The Street Address text is present and the value matches with the expected one.",log);
				}
				else
				{
					fail("The Street Address text is missing or the value does not matches with the expected one. The current value in the text field is " + stAddresstxt.getText());
				}
			}
			else
			{
				fail("The Street Address inline text is not found.");
			}
			
			WebElement aptSuiteAddress = BNBasicCommonMethods.findElement(driver, obj, "aptSuiteAddress");
			BNBasicCommonMethods.scrolldown(aptSuiteAddress, driver);
			WebElement aptSuiteAddresstxt = BNBasicCommonMethods.findElement(driver, obj, "aptSuiteAddresstxt");
			if(BNBasicCommonMethods.isElementPresent(aptSuiteAddresstxt))
			{
				if(aptSuiteAddresstxt.getText().equals(suiteNametext))
				{
					log.add("The current value in the Apt Suite text field is : " + aptSuiteAddresstxt.getText());
					pass("The Apt / Suite Name text is present and the value matches with the expected one.",log);
				}
				else
				{
					fail("The Apt / Suite Name text is missing or the value does not matches with the expected one. The current value in the text field is " + aptSuiteAddresstxt.getText());
				}
			}
			else
			{
				fail("The Apt/Suite Name inline text is not found.");
			}
			
			WebElement citytxt = BNBasicCommonMethods.findElement(driver, obj, "citytxt");
			if(BNBasicCommonMethods.isElementPresent(citytxt))
			{
				if(citytxt.getText().equals(cityNametxt))
				{
					log.add("The current value in the City text field is : " + citytxt.getText());
					pass("The City Name text is present and the value matches with the expected one.",log);
				}
				else
				{
					fail("The City Name text is missing or the value does not matches with the expected one. The current value in the text field is " + citytxt.getText());
				}
			}
			else
			{
				fail("The City Name inline text is not found.");
			}
			
			WebElement statetxt = BNBasicCommonMethods.findElement(driver, obj, "statetxt");
			if(BNBasicCommonMethods.isElementPresent(statetxt))
			{
				if(statetxt.getText().equals(stateNametext))
				{
					log.add("The current value in the State text field is : " + statetxt.getText());
					pass("The State Name text is present and the value matches with the expected one.",log);
				}
				else
				{
					fail("The State Name text is missing or the value does not matches with the expected one. The current value in the text field is " + statetxt.getText());
				}
			}
			else
			{
				fail("The State Name inline text is not found.");
			}
			
			WebElement zipcodetxt = BNBasicCommonMethods.findElement(driver, obj, "zipcodetxt");
			if(BNBasicCommonMethods.isElementPresent(zipcodetxt))
			{
				if(zipcodetxt.getText().equals(zipcodeNametext))
				{
					log.add("The current value in the Zip Code text field is : " + zipcodetxt.getText());
					pass("The Zip Code text is present and the value matches with the expected one.",log);
				}
				else
				{
					fail("The Zip Code text is missing or the value does not matches with the expected one. The current value in the text field is " + zipcodetxt.getText());
				}
			}
			else
			{
				fail("The Zip Code inline text is not found.");
			}
			
			WebElement contactNotxt = BNBasicCommonMethods.findElement(driver, obj, "contactNotxt");
			if(BNBasicCommonMethods.isElementPresent(contactNotxt))
			{
				if(contactNotxt.getText().equals(phoneNumbertext))
				{
					log.add("The current value in the Contact Number text field is : " + contactNotxt.getText());
					pass("The Contact Number text is present and the value matches with the expected one.",log);
				}
				else
				{
					fail("The Contact Number text is missing or the value does not matches with the expected one. The current value in the text field is " + contactNotxt.getText());
				}
			}
			else
			{
				fail("The Contact Number inline text is not found.");
			}
			
			WebElement companyNametxt = BNBasicCommonMethods.findElement(driver, obj, "companyNametxt");
			if(BNBasicCommonMethods.isElementPresent(companyNametxt))
			{
				if(companyNametxt.getText().equals(companyNametext))
				{
					log.add("The current value in the Company Name field is : " + companyNametxt.getText());
					pass("The Company Name text is present and the value matches with the expected one.",log);
				}
				else
				{
					fail("The Company Name text is missing or the value does not matches with the expected one. The current value in the text field is " + companyNametxt.getText());
				}
			}
			else
			{
				fail("The Company Name inline text is not found.");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong.Please Check." + e.getMessage());
			
		}
	}
 
	/* BNIA-979 Guest Checkout - Verify that user can be able to select country from the drop down and by default United states should be selected*/
	public void guestCheckoutDefaultCountry()
	{
		ChildCreation(" BNIA-979 Guest Checkout - Verify that user can be able to select country from the drop down and by default United states should be selected.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String country = BNBasicCommonMethods.getExcelVal("BRM634", sheet, 5);
			String[] cntyname = country.split("\n");
			WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
			BNBasicCommonMethods.scrollup(guestCheckoutAddPageTitle, driver);
			WebElement countrySelection = BNBasicCommonMethods.findElement(driver, obj, "countrySelection");
			if(BNBasicCommonMethods.isElementPresent(countrySelection))
			{
				WebElement Countrycontainer = BNBasicCommonMethods.findElement(driver, obj, "countrycontiner");
				Countrycontainer.click();
				List<WebElement> countryLst = driver.findElements(By.xpath("//*[@id='country-option-list']//li"));
				//driver.findElement(By.xpath("//*[@id='state-option-35']")).click();
				//countryLst.get(0).click();
				//System.out.println(countryLst.get(0).getText());
				countryLst.get(0).click();
				Countrycontainer.click();
				/*Select cntysel = new Select(countrySelection);*/
				if(countryLst.get(0).getText().equals(cntyname[0]))
				{
					Countrycontainer.click();
					pass("The default Selected Country is : " + countryLst.get(0).getText() + "  and it matches the expected one.");
					Actions act = new Actions(driver);
					//act.moveToElement(countrySelection).click().build().perform();
					/*cntysel.selectByValue(cntyname[1]);*/
					act.sendKeys(Keys.TAB).build().perform();
					Countrycontainer.click();
					if(countryLst.get(0).getText().equals(cntyname[0]))
					{
						pass("The User was able to select the desired country : " +  countryLst.get(1).getText() + "  and it matches the expected one.");
						Countrycontainer.click();
					}
					else
					{
						fail("The User was not able to select the desired country. The current selected Country is " +  countryLst.get(1).getText() + "  and the expected country was : " + cntyname[1]);
						Countrycontainer.click();
					}
				}
				else
				{
					System.out.println(countryLst.get(1).getText());
					fail("The default Selected Country is : " +  countryLst.get(1).getText() +" and it does not matches the expected one. The expected country was " + cntyname[1]);
					//Countrycontainer.click();
				}
			}
		}
		catch(Exception e)
		{
			exception(" There is something wrong.Please Check." + e.getMessage());
			System.out.println(e.getMessage());
		}
	}

	 /*BNIA-980 Guest Checkout - Verify that user can be able to enter first name in the First name field and it should accept maximum of 40 characters*/
	public void guestCheckoutFirstNameLengthValidation() throws IOException
	{
		sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
		ChildCreation(" BNIA-980 Guest Checkout - Verify that user can be able to enter first name in the First name field and it should accept maximum of 40 characters.");
		log.add("The browser is launched and it is running.");
		int brmkey = sheet.getLastRowNum();
		for(int i = 0; i <= brmkey; i++)
		{
			String tcid = "BRM636";
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				String firstName = sheet.getRow(i).getCell(6).getStringCellValue();
				try
				{
					WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
					BNBasicCommonMethods.scrollup(guestCheckoutAddPageTitle, driver);
					Actions act = new Actions(driver);
					WebElement fName = BNBasicCommonMethods.findElement(driver, obj, "fName");
					fName.clear();
					fName.sendKeys(firstName);
					act.sendKeys(Keys.TAB).build().perform();
					if(fName.getAttribute("value").toString().length()<=40)
					{
						log.add("The entered value in the First Name from the excel file is " + firstName + " and its length is " + firstName.length() + " . The current value in the First Name field is " + fName.getText() + " and its length is " + fName.getAttribute("value").length());
						pass("The First Name field accepts character less than 40 only.",log);
					}
					else
					{
						log.add("The entered value in the First Name from the excel file is " + firstName + " and its length is " + firstName.length() + " . The current value in the First Name field is " + fName.getText() + " and its length is " + fName.getAttribute("value").length());
						fail("The First Name field accepts character more than 40 .",log);
					}
				}
				catch(Exception e)
				{
					exception("There is something wrong.Please Check." + e.getMessage());
					System.out.println(e.getMessage());
				}
			}
		}
	}
	
	 /*BNIA-981 Guest Checkout - Verify that user can be able to enter last name in the Last name field and it should accept maximum of 40 characters*/
	public void guestCheckoutLastNameLengthValidation() throws IOException
	{
		ChildCreation(" BNIA-981 Guest Checkout - Verify that user can be able to enter last name in the Last name field and it should accept maximum of 40 characters.");
		log.add("The browser is launched and it is running.");
		sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
		int brmkey = sheet.getLastRowNum();
		for(int i = 0; i <= brmkey; i++)
		{
			String tcid = "BRM640";
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				String lastName = sheet.getRow(i).getCell(7).getStringCellValue();
				try
				{
					Actions act = new Actions(driver);
					WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
					BNBasicCommonMethods.scrollup(guestCheckoutAddPageTitle, driver);
					WebElement lName = BNBasicCommonMethods.findElement(driver, obj, "lName");
					lName.clear();
					lName.sendKeys(lastName);
					act.sendKeys(Keys.TAB).build().perform();
					if(lName.getAttribute("value").length()<=40)
					{
						log.add("The entered value in the Last Name from the excel file is " + lastName + " and its length is " + lastName.length() + " . The current value in the Last Name field is " + lName.getText() + " and its length is " + lName.getAttribute("value").length());
						pass("The Last Name field accepts character less than 40 only.",log);
					}
					else
					{
						log.add("The entered value in the Last Name from the excel file is " + lastName + " and its length is " + lastName.length() + " . The current value in the Last Name field is " + lName.getText() + " and its length is " + lName.getAttribute("value").length());
						fail("The Last Name field accepts character more than 40 .",log);
					}
				}
				catch(Exception e)
				{
					exception("There is something wrong.Please Check." + e.getMessage());
					System.out.println(e.getMessage());
				}
			}
		}
	}

	/* BNIA-982 Guest Checkout - Verify that user can be able to enter street address in the Street address field and it should accept maximum of 35 characters*/
	public void guestCheckoutStreetAddressLengthValidation() throws IOException
	{
		ChildCreation(" BNIA-982 Guest Checkout - Verify that user can be able to enter street address in the Street address field and it should accept maximum of 35 characters.");
		log.add("The browser is launched and running successfully.");
		sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
		int brmkey = sheet.getLastRowNum();
		for(int i = 0; i <= brmkey; i++)
		{
			String tcid = "BRM641";
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				String addr = sheet.getRow(i).getCell(8).getStringCellValue().toString();
				try
				{
					Actions act = new Actions(driver);
					WebElement stAddress = BNBasicCommonMethods.findElement(driver, obj, "stAddress");
					stAddress.clear();
					stAddress.sendKeys(addr);
					act.sendKeys(Keys.TAB).build().perform();
					Thread.sleep(250);
					if(stAddress.getAttribute("value").length()<=35)
					{
						log.add("The entered characters for the Street Address is : " + addr + " , and its length is : " + addr.length());
						log.add("The Current Value in the Street Address field is : " + stAddress.getAttribute("value").toString() + " , and its length is : " + stAddress.getAttribute("value").length());
						pass("The address field accepts character less than 35.",log);
					}
					else
					{
						log.add("The entered characters for the Street Address is : " + addr + " , and its length is : " + addr.length());
						log.add("The Current Value in the Street Address field is : " + stAddress.getAttribute("value").toString() + " , and its length is : " + stAddress.getAttribute("value").length());
						fail("The address field accepts character more than 35.",log);
					}
				}
				catch(Exception e)
				{
					exception(" There is something wrong. Please Check. " + e.getMessage());
				}
			}
		}
	}

	/* BNIA-983 Guest Checkout - Verify that user can be able to enter Apt/Suite details in the Apt/Suite (Optional) field and it should accept maximum of 35 characters*/
	public void guestCheckoutAptSuiteAddressLengthValidation() throws IOException
	{
		ChildCreation(" BNIA-983 Guest Checkout - Verify that user can be able to enter Apt/Suite details in the Apt/Suite (Optional) field and it should accept maximum of 35 characters.");
		log.add("The browser is launched and running successfully.");
		sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
		int brmkey = sheet.getLastRowNum();
		for(int i = 0; i <= brmkey; i++)
		{
			String tcid = "BRM642";
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				String aptsuiteaddr = sheet.getRow(i).getCell(9).getStringCellValue().toString();
				try
				{
					Actions act = new Actions(driver);
					WebElement aptSuiteAddress = BNBasicCommonMethods.findElement(driver, obj, "aptSuiteAddress");
					aptSuiteAddress.clear();
					aptSuiteAddress.sendKeys(aptsuiteaddr);
					act.sendKeys(Keys.TAB).build().perform();
					Thread.sleep(250);
					BNBasicCommonMethods.waitforElement(wait, obj, "stAddress");
					WebElement stAddress = BNBasicCommonMethods.findElement(driver, obj, "stAddress");
					if(aptSuiteAddress.getAttribute("value").length()<=35)
					{
						log.add("The entered characters for the Apt, Suite address field is : " + aptsuiteaddr + " , and its length is : " + aptsuiteaddr.length());
						log.add("The Current Value in the Street Address field is : " + stAddress.getAttribute("value").toString() + " , and its length is : " + aptSuiteAddress.getAttribute("value").length());
						pass("The Apt, Suite field accepts character less than 35.",log);
					}
					else
					{
						log.add("The entered characters for the Apt, Suite Address field is : " + aptsuiteaddr + " , and its length is : " + aptsuiteaddr.length());
						log.add("The Current Value in the Apt, Suite Address field is : " + stAddress.getAttribute("value").toString() + " , and its length is : " + aptSuiteAddress.getAttribute("value").length());
						fail("The Apt, Suite address field accepts character more than 35.",log);
					}
				}
				catch(Exception e)
				{
					exception(" There is something wrong. Please Check. " + e.getMessage());
				}
			}
		}
	}

	 /* BNIA-984 Guest Checkout - Verify that user can be able to enter city details in the City field and it should accept maximum of 60 characters*/
	public void guestCheckoutCityLengthValidation() throws IOException
	{
		ChildCreation(" BNIA-984 Guest Checkout - Verify that user can be able to enter city details in the City field and it should accept maximum of 60 characters");
		log.add("The browser is launched and running successfully.");
		sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
		int brmkey = sheet.getLastRowNum();
		for(int i = 0; i <= brmkey; i++)
		{
			String tcid = "BRM643";
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				String cityname = sheet.getRow(i).getCell(10).getStringCellValue().toString();
				try
				{
					Actions act = new Actions(driver);
					WebElement stAddress = BNBasicCommonMethods.findElement(driver, obj, "stAddress"); 
					BNBasicCommonMethods.scrolldown(stAddress, driver);
					WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
					city.clear();
					city.sendKeys(cityname);
					act.sendKeys(Keys.TAB).build().perform();
					Thread.sleep(250);
					if(city.getAttribute("value").length()<=60)
					{
						log.add("The entered characters for the City Field is : " + cityname + " , and its length is : " + cityname.length());
						log.add("The Current Value in the City Field is : " + city.getAttribute("value").toString() + " , and its length is : " + city.getAttribute("value").length());
						pass("The City Field accepts character less than 60.",log);
					}
					else
					{
						log.add("The entered characters for the City Field is : " + cityname + " , and its length is : " + cityname.length());
						log.add("The Current Value in the City Field is : " + city.getAttribute("value").toString() + " , and its length is : " + city.getAttribute("value").length());
						fail("The City Field accepts character more than 60.",log);
					}
				}
				catch(Exception e)
				{
					exception(" There is something wrong. Please Check. " + e.getMessage());
				}
			}
		}
	}

	 /* BNIA-985 Guest Checkout - Verify that user can be able to select state from the state drop down*/
	/*BNIA-1087 Verify that state field is auto populated the states based on the selected country*/
	public void guestCheckoutStateSelectionValidation()
	{
	    boolean childflag = false;
		ChildCreation(" BNIA-985 Guest Checkout - Verify that user can be able to select state from the state drop down.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String stname = BNBasicCommonMethods.getExcelVal("BRM644", sheet, 11);
			String[] name = stname.split("\n");
				WebElement countrySelection =  BNBasicCommonMethods.findElement(driver, obj, "countrySelection");
				WebElement countrycontiner  = BNBasicCommonMethods.findElement(driver, obj, "countrycontiner");
				WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
				BNBasicCommonMethods.scrollup(guestCheckoutAddPageTitle, driver);
				//BNBasicCommonMethods.scrollup(countrycontiner, driver);
				if(BNBasicCommonMethods.isElementPresent(countrySelection))
				{
					BNBasicCommonMethods.scrollup(countrySelection,driver);
			     //	Actions act = new Actions(driver);
			    	countrycontiner.click();
					//Thread.sleep(1000);
				//	Select cntrysel = new Select(countrySelection);
					WebElement state = BNBasicCommonMethods.findElement(driver, obj, "state");
					countrycontiner.click();
					BNBasicCommonMethods.scrolldown(state,driver);
					WebElement Statecontainer = BNBasicCommonMethods.findElement(driver, obj, "Statecontainer");
					Statecontainer.click();
					List<WebElement> statelist = driver.findElements(By.xpath("//*[@id='state-option-list']//*[contains(@id,'state')]//a"));
					statelist.get(35).click();
					//	WebElement ste = driver.findElement(By.xpath("//*[@id='state']"));
					/*Select sel = new Select(ste);
					System.out.println(sel.getOptions().size());*/
					boolean stfound = false;
					for(int j = 1; j<=statelist.size(); j++)
					{
						WebElement Statenew = driver.findElement(By.xpath("(//*[@class='selectBox-label'])[2]"));
						if(Statenew.getText().contains(name[0]))
						{
							/*stsel.selectByValue(name[0]);*/
							stfound = true;
							break;
						}
						else
						{
							continue;
						}
					}
					
					if(stfound == true)
					{
						childflag = true;
						log.add("The user expected state was : ONTARIO, value " + stname);
						log.add("The state selected for the Country : " + countrySelection.getText() + " is : " +  stname);
						pass("The Desired state for the Country is found and it is selected.",log);
					}
					else
					{
						for(int j = 1; j<=statelist.size(); j++)
						{
							log.add("The State populated for the Selected Country : " + countrySelection.getText() + " is : " + stname);
						}
						log.add("The user expected state was : ONTARIO, value " + stname);
						fail("The desired State is not found for the selected country.",log);
					}
			     }
			  
			}
		catch(Exception e)
		{
			exception(" There is something wrong. Please Check. " + e.getMessage());
			System.out.println(e.getMessage());
		}
		
		ChildCreation("BNIA-1087 Verify that state field is auto populated the states based on the selected country");
        if(childflag)
        	pass("State dropdown is based on selected country in country dropdown");
        else
        	fail("State dropdown is not based on selected country in country dropdown");
	}

	/* BNIA-986 Guest Checkout - Verify that user can be able to enter zip code details in the Zip Code field and it should accept maximum of 10 characters*/
	public void guestCheckoutZipCodeLengthValidation() throws IOException
	{
		ChildCreation(" BNIA-986 Guest Checkout - Verify that user can be able to enter zip code details in the Zip Code field and it should accept maximum of 10 characters.");
		log.add("The browser is launched and running successfully.");
		sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
		int brmkey = sheet.getLastRowNum();
		for(int i = 0; i <= brmkey; i++)
		{
			String tcid = "BRM645";
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				String zpcode = sheet.getRow(i).getCell(12).getStringCellValue().toString();
				try
				{
					Actions act = new Actions(driver);
					WebElement stAddress = BNBasicCommonMethods.findElement(driver, obj, "stAddress");
					BNBasicCommonMethods.scrolldown(stAddress, driver);
					WebElement zipCode = BNBasicCommonMethods.findElement(driver, obj, "zipCode");
					zipCode.clear();
					zipCode.sendKeys(zpcode);
					act.sendKeys(Keys.TAB).build().perform();
					Thread.sleep(250);
					if(zipCode.getAttribute("value").length()<=10)
					{
						log.add("The entered characters for the Zipcode Field is : " + zpcode + " , and its length is : " + zpcode.length());
						log.add("The Current Value in the Zipcode Field is : " + zipCode.getAttribute("value").toString() + " , and its length is : " + zipCode.getAttribute("value").length());
						pass("The Zip Code Field accepts character less than 10.",log);
					}
					else
					{
						log.add("The entered characters for the Zipcode Field is : " + zpcode + " , and its length is : " + zpcode.length());
						log.add("The Current Value in the Zipcode Field is : " + zipCode.getAttribute("value").toString() + " , and its length is : " + zipCode.getAttribute("value").length());
						fail("The Zip Code field accepts character more than 10.",log);
					}
				}
				catch(Exception e)
				{
					exception(" There is something wrong. Please Check. " + e.getMessage());
				}
			}
		}
	}

	/* BNIA-987 Guest Checkout - Verify that user can be able to enter phone number in the phone number field and it should accept maximum of 60 characters*/ 
	public void guestCheckoutPhoneNumberLengthValidation() throws IOException
	{
		ChildCreation(" BNIA-987 Guest Checkout - Verify that user can be able to enter phone number in the phone number field and it should accept maximum of 60 characters.");
		log.add("The browser is launched and running successfully.");
		sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
		int brmkey = sheet.getLastRowNum();
		for(int i = 0; i <= brmkey; i++)
		{
			String tcid = "BRM646";
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				 DataFormatter formatter = new DataFormatter(); //creating formatter using the default locale
				 HSSFCell cell = sheet.getRow(i).getCell(13);
				 cell.setCellType(Cell.CELL_TYPE_STRING);
				 String phoneNolengthValidation = formatter.formatCellValue(cell).toString();
				 try
				 {
					Actions act = new Actions(driver);
					WebElement contactNo = BNBasicCommonMethods.findElement(driver, obj, "contactNo");
					contactNo.clear();
					contactNo.sendKeys(phoneNolengthValidation);
					act.sendKeys(Keys.TAB).build().perform();
					if(contactNo.getAttribute("value").length()<=60)
					{
						log.add("The entered Phone number was " + phoneNolengthValidation + " . The Current value in the Phone Number field is "+ contactNo.getAttribute("value").toString() +  " and its length was " + phoneNolengthValidation.length());
						pass("The length of the Phone No field is less than or equal to 14.",log);
					}
					else
					{
						log.add("The entered Phone number was " + phoneNolengthValidation + " . The Current value in the Phone Number field is "+ contactNo.getAttribute("value").toString() +  " and its length was " + phoneNolengthValidation.length());
						fail("The length of the Phone No field is greater than 14.",log);
					}
				 }
				 catch(Exception e)
				 {
					exception(" There is something wrong. Please Check. " + e.getMessage());
				 } 
			}
		}
	}
	
	 /*BNIA-988 Guest Checkout - Verify that user can be able to enter company name(Optional) in the Company name(Optional) field and it should accept maximum of 35 characters*/ 
	/*BNIA-1085 Verify that company name field accepts only maximum of 35 characters in "Add a shipping address page"*/
	public void guestCheckoutCompanyLengthValidation() throws IOException
	{
		boolean childflag = false;
		ChildCreation(" BNIA-988 Guest Checkout - Verify that user can be able to enter company name(Optional) in the Company name(Optional) field and it should accept maximum of 35 characters. ");
		log.add("The browser is launched and running successfully.");
		sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
		int brmkey = sheet.getLastRowNum();
		for(int i = 0; i <= brmkey; i++)
		{
			String tcid = "BRM647";
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				String companyname = sheet.getRow(i).getCell(14).getStringCellValue().toString();
				try
				{
					Actions act = new Actions(driver);
					WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
					BNBasicCommonMethods.scrolldown(city, driver);
					WebElement companyName = BNBasicCommonMethods.findElement(driver, obj, "companyName");
					companyName.clear();
					companyName.sendKeys(companyname);
					act.sendKeys(Keys.TAB).build().perform();
					Thread.sleep(250);
					if(companyName.getAttribute("value").length()<=35)
					{
						childflag = true;
						log.add("The entered characters for the Company Name field is : " + companyname + " , and its length is : " + companyname.length());
						log.add("The Current Value in the Company Name Field is : " + companyName.getAttribute("value").toString() + " , and its length is : " + companyName.getAttribute("value").length());
						pass("The Company Name Field accepts character less than 35.",log);
					}
					else
					{
						log.add("The entered characters for the Company Name Field is : " + companyname + " , and its length is : " + companyname.length());
						log.add("The Current Value in the Company Name Field is : " + companyName.getAttribute("value").toString() + " , and its length is : " + companyName.getAttribute("value").length());
						fail("The City Field accepts character more than 35.",log);
					}
				}
				catch(Exception e)
				{
					exception(" There is something wrong. Please Check. " + e.getMessage());
				}
			}
		}
		
		ChildCreation("BNIA-1085Verify that company name field accepts only maximum of 35 characters in Add a shipping address page");
        if(childflag)	
        	pass("The Company Name Field accepts character less than 35");
        else
        	fail("The Company Name Field is not accepts character less than 35");
	}

	/* BNIA-989 Guest Checkout - Verify that without entering any values in the input field, clicking on "Continue" button should display a alert message*/ 
	public void guestCheckoutBlankSaveAlertValidation()
	{
		ChildCreation(" BNIA-989 Guest Checkout - Verify that without entering any values in the input field, clicking on Continue button should display a alert message.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String alert = BNBasicCommonMethods.getExcelVal("BRM648", sheet, 4);
			WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
			BNBasicCommonMethods.scrolldown(city, driver);
			WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
			/*BNBasicCommonMethods.scrolldown(orderContinuebtn, driver);*/
			orderContinuebtn.click();
			
			WebElement erromsg = BNBasicCommonMethods.findElement(driver, obj, "erromsg");
			BNBasicCommonMethods.scrollup(erromsg, driver);
			wait.until(ExpectedConditions.visibilityOf(erromsg));
			
			if(erromsg.getText().equals(alert))
			{
				log.add("The raised error " + erromsg.getText());
				pass("The appropriate error is raised when user tries to save the Submit the Order without entering the values.",log);
			}
			else
			{
				log.add("The raised error " + erromsg.getText());
				log.add("The Expected alert was " + alert);
				fail("The appropriate error is raised when user tries to save the Shipping Address with only Spaces.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			exception(" There is something wrong. Please Check. " + e.getMessage());
		}
	}
		
	/* BNIA-990 Guest Checkout - Verify that special characters should not be allowed in First name, last name, phone number and zip code fields and alerts should be displayed*/
	/*BNIA-1086 Verify that phone no,zipcode fields are not accepted any HTML tags*/
	public void guestCheckoutInvalidFieldDataValidation()
	{
		boolean childflag = false;
		ChildCreation(" BNIA-990 Guest Checkout - Verify that special characters should not be allowed in First name, last name, phone number and zip code fields and alerts should be displayed.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String alert = BNBasicCommonMethods.getExcelVal("BRM649", sheet, 4); 
			String firstName = BNBasicCommonMethods.getExcelVal("BRM649", sheet, 6);
			String lastName = BNBasicCommonMethods.getExcelVal("BRM649", sheet, 7);
			String zpcode = BNBasicCommonMethods.getExcelVal("BRM649", sheet, 12);
			WebElement fName = BNBasicCommonMethods.findElement(driver, obj, "fName");
			clearAll();
			fName.clear();
			fName.sendKeys(firstName);
			WebElement lName = BNBasicCommonMethods.findElement(driver, obj, "lName");
			lName.clear();
			lName.sendKeys(lastName);
			WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
			BNBasicCommonMethods.scrolldown(city, driver);
			/*  Actions action = new Actions(driver);
			   action.sendKeys(Keys.PAGE_DOWN);*/
			/*JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,250)", city);*/
			WebElement zipCode = BNBasicCommonMethods.findElement(driver, obj, "zipCode");
			BNBasicCommonMethods.scrollup(zipCode, driver);
			zipCode.clear();
			zipCode.sendKeys(zpcode);
			/*fName.clear();
			lName.clear();*/
			/*zipCode.clear();*/
			WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView(true);",orderContinuebtn);
			orderContinuebtn.click();
			WebElement erromsg = BNBasicCommonMethods.findElement(driver, obj, "erromsg");
			wait.until(ExpectedConditions.visibilityOf(erromsg));
			//BNBasicCommonMethods.scrollup(erromsg, driver);
			if(erromsg.getText().contains(alert))
			{
				childflag = true;
				log.add("The raised error " + erromsg.getText());
				pass("The appropriate error is raised when user tries to save the Submit the Order without entering the values.",log);
			}
			else
			{
				System.out.println(erromsg.getText());
				log.add("The raised error " + erromsg.getText());
				log.add("The Expected alert was " + alert);
				fail("The appropriate error is raised when user tries to save the Shipping Address with only Spaces.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			exception(" There is something wrong. Please Check. " + e.getMessage());
		}
		
		ChildCreation("BNIA-1086 Verify that phone no,zipcode fields are not accepted any HTML tags");
		if(childflag)
			pass("The appropriate error is raised when user tries to save the Submit the Order while entering the invalid values.");
		else
			fail("The appropriate error was not raised when user tries to save the Submit the Order while entering the invalid values.");
	}
		
	/* BRM - 651 Guest Checkout – Verify that clicking on Customer service should redirect to the customer service page 
	public void guestCheckoutCustomerServiceNavigation()
	{
		ChildCreation(" BRM - 651 Verify that clicking on Customer service should redirect to the customer service page.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String pgeurl = BNBasicCommonMethods.getExcelVal("BRM651", sheet, 3);
			WebElement zipCode = BNBasicCommonMethods.findElement(driver, obj, "zipCode");
			BNBasicCommonMethods.scrolldown(zipCode, driver);
			if(BNBasicCommonMethods.isElementPresent(customerService))
			{
				log.add("The Customer Service Link is found.");
				customerService.click();
				Thread.sleep(3000);
				if(driver.getCurrentUrl().equals(pgeurl))
				{
					log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
					log.add("The Expected URL for the Customer Service Link was : " + pgeurl);
					Pass("The user is  navigated to the Customer Service Page.",log);
					driver.navigate().back();
				}
				else
				{
					log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
					log.add("The Expected URL for the Customer Service Link was : " + pgeurl);
					fail("The user is not navigated to the Customer Service Page. Please Check.",log);
					driver.navigate().back();
				}
			}
			else
			{
				fail("The Customer Service link is not dispalyed.");
			}
		}
		catch(Exception e)
		{
			exception(" There is something wrong. Please Check. " + e.getMessage());
		}
	}

	 BRM - 652 Guest Checkout – Verify that clicking on terms of use should redirect to the terms of use page
	public void guestCheckoutTermsOfUseNavigation()
	{
		ChildCreation(" BRM - 652 Verify that clicking on terms of use should redirect to the terms of use page.");
		try
		{
			String excelvalue = BNBasicCommonMethods.getExcelVal("BRM652", sheet, 3);
			String[] pgeurl = excelvalue.split("\n");
			BNBasicCommonMethods.scrolldown(zipCode, driver);
			boolean linkfound = false;
			if(BNBasicCommonMethods.isListElementPresent(copyrightContainer))
			{
				log.add("The Copyright Container is found and it is visible.");
				for(int j = 1; j <= copyrightContainer.size(); j++)
				{
					WebElement pgenamelink = driver.findElement(By.xpath("(//*[@id='copyright-container']//a)["+j+"]"));
					if(pgenamelink.getText().equals(pgeurl[0]))
					{
						log.add("The  Terms of Use Link is found.");
						pgenamelink.click();
						Thread.sleep(3000);
						if(driver.getCurrentUrl().contains(pgeurl[1]))
						{
							linkfound=true;
							log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
							log.add("The Expected URL for the Terms of Use Page was : " + pgeurl[1]);
							Pass("The user is navigated to the Terms of Use Page.",log);
							driver.navigate().back();
							break;
						}
						else
						{
							linkfound=true;
							log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
							log.add("The Expected URL for the Terms of Use page was : " + pgeurl[1]);
							fail("The user is not navigated to the Terms of Use Page. Please Check.",log);
							driver.navigate().back();
							break;
						}
					}
					else
					{
						continue;
					}
				}
				
				if(linkfound == true)
				{
					Pass("The Terms of Use link was found and it was clicked and verified");
				}
				else
				{
					fail("The Terms of Use link is not found. Please Check.");
				}
			}
			else
			{
				fail("The Copyrights Container List is not found. Please Check.");
			}
		}
		catch(Exception e)
		{
			exception(" There is something wrong. Please Check. " + e.getMessage());
		}
	}
	
	 BRM - 653 Guest Checkout – Guest Checkout – Verify that clicking on Copyright should redirect to the Copyright page
	public void guestCheckoutCopyrightsPageNavigation()
	{
		ChildCreation(" BRM - 653 Guest Checkout – Verify that clicking on Copyright should redirect to the Copyright page.");
		try
		{
			String excelvalue = BNBasicCommonMethods.getExcelVal("BRM653", sheet, 3);
			String[] pgeurl = excelvalue.split("\n");
			BNBasicCommonMethods.scrolldown(zipCode, driver);
			boolean linkfound = false;
			if(BNBasicCommonMethods.isListElementPresent(copyrightContainer))
			{
				log.add("The Copyright Container is found and it is visible.");
				for(int j = 1; j <= copyrightContainer.size(); j++)
				{
					WebElement pgenamelink = driver.findElement(By.xpath("(//*[@id='copyright-container']//a)["+j+"]"));
					if(pgenamelink.getText().equals(pgeurl[0]))
					{
						linkfound = true;
						log.add("The  Copyrights link is found.");
						pgenamelink.click();
						Thread.sleep(3000);
						if(driver.getCurrentUrl().equals(pgeurl[1]))
						{
							log.add("The user is navigated to the Copyrights Page.");
							WebElement copyrightpgetitle = driver.findElement(By.xpath("//*[@id='categoryHeaderContainer']//*[@class='text']"));
							if(copyrightpgetitle.getText().contains(pgeurl[0]))
							{
								log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
								log.add("The Expected URL for the Copyrights page was : " + pgeurl[1]);
								Pass("The Copyrights Page title matches. The Copyrights Page title : " + copyrightpgetitle.getText(),log);
								driver.navigate().back();
								break;
							}
							else
							{
								log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
								log.add("The Expected URL for the Copyrights page was : " + pgeurl[1]);
								fail("The Copyrights Page title does not matches. Please Check.",log);
								driver.navigate().back();
								break;
							}
						}
						else
						{
							log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
							log.add("The Expected URL for the Copyrights page was : " + pgeurl[1]);
							fail("The user is not navigated to the Copyrights Page. Please Check.",log);
							driver.navigate().back();
						
						}
					}
					else
					{
						continue;
					}
				}
				if(linkfound == true)
				{
					Pass("The Copyright link was found and it was clicked and navigated.");
				}
				else
				{
					fail("The Copyright link is not found. Please Check.");
				}
			}
			else
			{
				fail("The Copyrights Container List is not found. Please Check.");
			}
		}
		catch(Exception e)
		{
			exception(" There is something wrong. Please Check. " + e.getMessage());
		}
	}

	 BRM - 654 Guest Checkout – Verify that clicking on Privacy policy should redirect to the Privacy policy page
	public void guestCheckoutPolicyPageNavigation()
	{
		ChildCreation(" BRM - 654 Guest Checkout – Verify that clicking on Privacy policy should redirect to the Privacy policy page.");
		try
		{
			String excelvalue = BNBasicCommonMethods.getExcelVal("BRM654", sheet, 3);
			String[] pgeurl = excelvalue.split("\n");
			BNBasicCommonMethods.scrolldown(zipCode, driver);
			boolean linkfound = false;
			if(BNBasicCommonMethods.isListElementPresent(copyrightContainer))
			{
				log.add("The Copyright Container is found and it is visible.");
				for(int j = 1; j <= copyrightContainer.size(); j++)
				{
					WebElement pgenamelink = driver.findElement(By.xpath("(//*[@id='copyright-container']//a)["+j+"]"));
					if(pgenamelink.getText().equals(pgeurl[0]))
					{
						linkfound = true;
						log.add("The  Privacy Policy link is found.");
						pgenamelink.click();
						Thread.sleep(3000);
						if(driver.getCurrentUrl().equals(pgeurl[1]))
						{
							log.add("The user is navigated to the Privacy Policy Page.");
							WebElement privacyPolicypgetitle = driver.findElement(By.xpath("//*[@id='categoryHeaderContainer']//*[@class='text']"));
							if(privacyPolicypgetitle.getText().contains(pgeurl[0]))
							{
								log.add("The Expected URL for the Privacy Policy page was : " + pgeurl[1]);
								log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
								Pass("The Copyrights Page title matches. The Privacy Policy page title : " + privacyPolicypgetitle.getText(),log);
								driver.navigate().back();
								break;
							}
							else
							{
								log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
								log.add("The Expected URL for the Privacy Policy page was : " + pgeurl[1]);
								fail("The Copyrights Page title does not matches. Please Check.",log);
								driver.navigate().back();
								break;
							}
						}
						else
						{
							log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
							log.add("The Expected URL for the Privacy Policy page was : " + pgeurl[1]);
							fail("The user is not navigated to the Privacy Policy Page. Please Check.",log);
							driver.navigate().back();
						}
					}
					else
					{
						continue;
					}
				}
				if(linkfound == true)
				{
					Pass("The Privacy Policy link was found and it was clicked. Please Check.");
				}
				else
				{
					fail("The Privacy Policy link is not found. Please Check.");
				}
			}
			else
			{
				fail("The Copyrights Container List is not found. Please Check.");
			}
		}
		catch(Exception e)
		{
			exception(" There is something wrong. Please Check. " + e.getMessage());
		}
	}

	 BRM - 655 Guest Checkout – Verify that clicking on 1997-2016 Barnesandnoble.com should redirect to the terms of use page
	public void guestCheckoutTermsOfUseNavigation2()
	{
		ChildCreation(" BRM - 655 Verify that clicking on terms of use should redirect to the terms of use page.");
		try
		{
			String excelvalue = BNBasicCommonMethods.getExcelVal("BRM655", sheet, 3);
			String[] pgeurl = excelvalue.split("\n");
			BNBasicCommonMethods.scrolldown(zipCode, driver);
			if(BNBasicCommonMethods.isListElementPresent(copyrightContainer))
			{
				log.add("The Copyright Container is found and it is visible.");
				for(int j = 1; j <= copyrightContainer.size(); j++)
				{
					WebElement pgenamelink = driver.findElement(By.xpath("(//*[@id='copyright-container']//ul//li)["+j+"]"));
					System.out.println(pgenamelink.getText());
					if((pgenamelink.getText()).contains(pgeurl[0]))
					{
						log.add("The bottom Copyright Container with the link is found.");
						pgenamelink.click();
						if(driver.getCurrentUrl().equals(pgeurl))
						{
							log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
							log.add("The Expected URL for the Terms of Use Page was : " + pgeurl[1]);
							Pass("The user is navigated to the Terms of Use Page.",log);
							driver.navigate().back();
							break;
						}
						else
						{
							log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
							log.add("The Expected URL for the Terms of Use page was : " + pgeurl[1]);
							fail("The user is not navigated to the Terms of Use Page. Please Check.",log);
							driver.navigate().back();
							break;
						}
					}
					else
					{
						//Skip("The Terms of Use link is not found. Please Check.");
						continue;
					}
				}
			}
			else
			{
				fail("The Copyrights Container List is not found. Please Check.");
			}
		}
		catch(Exception e)
		{
			exception(" There is something wrong. Please Check. " + e.getMessage());
		}
	}
		*/
	 /*BNIA-996 & BNIA-998 Verify that Address Verification overlay should be shown after adding new shipping address or updating the existing address.*/
	public void guestCheckoutAddressVerification()
	{
		boolean childflag = false;
		ChildCreation("BNIA-996 Verify that Address Verification overlay should be shown after adding new shipping address or updating the existing address.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String pgetitle = BNBasicCommonMethods.getExcelVal("BRM571", sheet, 3);
			BNBasicCommonMethods.waitforElement(wait, obj, "orderContinuebtn");
			WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
			BNBasicCommonMethods.scrolldown(orderContinuebtn, driver);
			orderContinuebtn.click();
			//Thread.sleep(4000);
			BNBasicCommonMethods.waitforElement(wait, obj, "gadrverficationtitle");
			WebElement addressVerificationTitle = BNBasicCommonMethods.findElement(driver, obj, "gadrverficationtitle");
			wait.until(ExpectedConditions.visibilityOf(addressVerificationTitle));
			try
			{
				WebElement addressVerificationPartialMatchesTitle = BNBasicCommonMethods.findElement(driver, obj, "addressVerificationPartialMatchesTitle");
				if(BNBasicCommonMethods.isElementPresent(addressVerificationPartialMatchesTitle))
				{
					pass("The user is redirected to the Address Verification page.");
					if(addressVerificationPartialMatchesTitle.getText().equals(pgetitle))
					{
						childflag = true;
						log.add("The expected page title was : " + pgetitle);
						log.add("The Current page title is : " + addressVerificationPartialMatchesTitle.getText());
						pass("The Page title matches with the expected One.",log);
					}
					else
					{
						log.add("The expected page title was : " + pgetitle);
						log.add("The Current page title is : " + addressVerificationPartialMatchesTitle.getText());
						fail("The Page title does not matches with the expected One.",log);
					}
				}
			}
			catch(Exception e)
			{
				try
				/*else*/
				{
					WebElement addressVerificationMatchesTitle = BNBasicCommonMethods.findElement(driver, obj, "addressVerificationMatchesTitle");
					if(BNBasicCommonMethods.isElementPresent(addressVerificationMatchesTitle))
					{
						pass("The user is redirected to the Address Verification page.");
						if(addressVerificationMatchesTitle.getText().equals(pgetitle))
						{
							childflag = true;
							log.add("The expected page title was : " + pgetitle);
							log.add("The Current page title is : " + addressVerificationMatchesTitle.getText());
							pass("The Page title matches with the expected One.",log);
						}
						else
						{
							log.add("The expected page title was : " + pgetitle);
							log.add("The Current page title is : " + addressVerificationMatchesTitle.getText());
							fail("The Page title does not matches with the expected One.",log);
						}
					}
				   }
					catch(Exception e1)
					{
						try
						/*else*/
						{
							WebElement addressVerificationNoMatchesTitle = BNBasicCommonMethods.findElement(driver, obj, "addressVerificationNoMatchesTitle");
							if(BNBasicCommonMethods.isElementPresent(addressVerificationNoMatchesTitle))
							{
								pass("The user is redirected to the Address Verification page.");
								if(addressVerificationNoMatchesTitle.getText().equals(pgetitle))
								{
									childflag = true;
									log.add("The expected page title was : " + pgetitle);
									log.add("The Current page title is : " + addressVerificationNoMatchesTitle.getText());
									pass("The Page title matches with the expected One.",log);
								}
								else
								{
									log.add("The expected page title was : " + pgetitle);
									log.add("The Current page title is : " + addressVerificationNoMatchesTitle.getText());
									fail("The Page title does not matches with the expected One.",log);
								}
							}
							else
							{
								Skip("The user is not redirected to the Address Verification page. Please Check.");
							}
						 }
						catch(Exception e2)
						{
							exception("Something Wnet Wrong"+e.getMessage());
						}
				   }
			 }
		}
		catch(Exception e)
		{
			System.out.println("Exception"+e.getMessage());
			exception("There is something wrong. Please Check. " + e.getMessage());
		}
		
		ChildCreation("BNIA-998 Verify that Address Verification overlay should be shown after adding new shipping address or updating the existing address.");
        if(childflag)
        	pass("Address verification overlay is displayed");
        else
        	fail("Address verification overlay is not displayed");
	}
		
	/* BNIA-997 Verify that on clicking the "edit" option in address verification page it should open the edit address page with prefilled address*/
	public void guestCheckoutAddressVerificationEdit()
	{
		ChildCreation(" BNIA-997 Verify that on clicking the edit option in address verification page it should open the edit address page with prefilled address.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String editbutton = BNBasicCommonMethods.getExcelVal("BRM582", sheet, 3);
			WebElement addressVerificationTitle = BNBasicCommonMethods.findElement(driver, obj, "gadrverficationtitle");
			wait.until(ExpectedConditions.visibilityOf(addressVerificationTitle));
			try
			{
				WebElement addressVerificationPartialMatchesTitle = BNBasicCommonMethods.findElement(driver, obj, "addressVerificationPartialMatchesTitle");
				if(BNBasicCommonMethods.isElementPresent(addressVerificationPartialMatchesTitle))
				{
					log.add("The user is in the Address Verification page.");
					WebElement addressEnteredEditButton = BNBasicCommonMethods.findElement(driver, obj, "AddressEditBtn");
					BNBasicCommonMethods.scrolldown(addressEnteredEditButton, driver);
					if(BNBasicCommonMethods.isElementPresent(addressEnteredEditButton))
					{
						pass("The Edit button is present in the Address Verification page.");
						//System.out.println(addressEnteredEditButton.getText());
						if(addressEnteredEditButton.getText().equals(editbutton))
						{
							log.add("The expected button caption was : " + editbutton);
							log.add("The Current button caption is : " + addressEnteredEditButton.getText());
							pass("The Edit button caption matches with the Expected One.",log);
							addressEnteredEditButton.click();
							WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
							wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
							chkfieldvalues();
						}
						else
						{
							System.out.println(addressEnteredEditButton.getText());
							log.add("The expected button caption was : " + editbutton);
							log.add("The Current button caption is : " + addressEnteredEditButton.getText());
							fail("The Edit button caption does not matches with the expected One.",log);
							addressEnteredEditButton.click();
							WebElement guestCheckoutAddPageTitle  = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
							wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
							chkfieldvalues();
						}
					}
					else
					{
						fail("The Edit button is not found on the Address Verification Page.");
					}
				}
				}
				catch(Exception e)
				/*else*/
				{
					try
					{
						BNBasicCommonMethods.waitforElement(wait, obj, "addressVerificationMatchesTitle");
						WebElement addressVerificationMatchesTitle = BNBasicCommonMethods.findElement(driver, obj, "addressVerificationMatchesTitle");
						if(BNBasicCommonMethods.isElementPresent(addressVerificationMatchesTitle))
						{
							log.add("The user is redirected to the Address Verification page.");
							WebElement addressEnteredEditButton = BNBasicCommonMethods.findElement(driver, obj, "addressEnteredEditButton1");
							if(BNBasicCommonMethods.isElementPresent(addressEnteredEditButton))
							{
								pass("The Edit button is present in the Address Verification page.");
								if(addressEnteredEditButton.getText().equals(editbutton))
								{
									log.add("The expected button caption was : " + editbutton);
									log.add("The Current button caption is : " + addressEnteredEditButton.getText());
									pass("The Edit button caption matches with the Expected One.",log);
									addressEnteredEditButton.click();
									BNBasicCommonMethods.waitforElement(wait, obj, "guestCheckoutAddPageTitle");
									WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
									wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
									chkfieldvalues();
								}
								else
								{
									log.add("The expected button caption was : " + editbutton);
									log.add("The Current button caption is : " + addressEnteredEditButton.getText());
									fail("The Edit button caption does not matches with the expected One.",log);
									addressEnteredEditButton.click();
									WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
									wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
									chkfieldvalues();
								}
							}
							else
							{
								fail("The Edit button is not found on the Address Verification Page.");
							}
						}
						}
						catch(Exception e1)
						/*else*/
						{
						    try
						    {
								WebElement addressVerificationNoMatchesTitle = BNBasicCommonMethods.findElement(driver, obj, "gadrverficationtitle");
								if(BNBasicCommonMethods.isElementPresent(addressVerificationNoMatchesTitle))
								{
									log.add("The user is redirected to the Address Verification page.");
									WebElement addressEnteredEditButton = BNBasicCommonMethods.findElement(driver, obj, "addressEnteredEditButton");
									if(BNBasicCommonMethods.isElementPresent(addressEnteredEditButton))
									{
										pass("The Edit button is present in the Address Verification page.");
										if(addressEnteredEditButton.getText().equals(editbutton))
										{
											log.add("The expected button caption was : " + editbutton);
											log.add("The Current button caption is : " + addressEnteredEditButton.getText());
											pass("The Edit button caption matches with the Expected One.",log);
											addressEnteredEditButton.click();
											WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
											wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
											chkfieldvalues();
										}
										else
										{
											log.add("The expected button caption was : " + editbutton);
											log.add("The Current button caption is : " + addressEnteredEditButton.getText());
											fail("The Edit button caption does not matches with the expected One.",log);
											addressEnteredEditButton.click();
											WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
											wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
											chkfieldvalues();
										}
									}
									else
									{
										fail("The Edit button is not found on the Address Verification Page.");
									}
								 } 
								
								else
								{
									Skip("The user is not redirected to the Address Verification page. Please Check.");
								}
						    }
						    catch(Exception e2)
						    {
						    	exception("There is something wrong"+e.getMessage());
						    }
					}
				}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong. Please Check. " + e.getMessage());
		}
	}
		
	 /*BNIA-994 Guest Checkout - Verify that on entering all the input fields and clicking on "Continue" button should proceed to the Delivery page*/ 
	/*BNIA-1009 Verify that Address Verification page should be redirect user to delivery page if user selects "use this address" button*/
	public void guestCheckoutDeliveryPageNavigation()
	{
		boolean childflag = false;
		ChildCreation(" BNIA-994 Guest Checkout - Verify that on entering all the input fields and clicking on Continue button should proceed to the Delivery page.");
		try
		{
			
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String pagetitle = BNBasicCommonMethods.getExcelVal("BRM657", sheet, 3);
			WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
			BNBasicCommonMethods.scrolldown(city, driver);
			WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
			orderContinuebtn.click();
		//	Thread.sleep(6000);
			//BNBasicCommonMethods.waitforElement(wait, obj, "addressVerificationTitle");
			BNBasicCommonMethods.waitforElement(wait, obj, "gadrverficationtitle");
			WebElement addressVerificationTitle = BNBasicCommonMethods.findElement(driver, obj, "gadrverficationtitle");
			wait.until(ExpectedConditions.visibilityOf(addressVerificationTitle));
			try
			{
				
				WebElement addressVerificationPartialMatchesTitle = BNBasicCommonMethods.findElement(driver, obj, "addressVerificationPartialMatchesTitle");
				if(BNBasicCommonMethods.isElementPresent(addressVerificationPartialMatchesTitle))
				{
					WebElement addressSuggestionFirstAddress = BNBasicCommonMethods.findElement(driver, obj, "addressSuggestionFirstAddress");
					BNBasicCommonMethods.scrolldown(addressSuggestionFirstAddress, driver);
					Random r = new Random();
					List <WebElement> checkoutAddressVerficationPossibleSuggestion = driver.findElements(By.xpath(" //*[@id='addrMatches']//li"));
					int sel = r.nextInt(checkoutAddressVerficationPossibleSuggestion.size());
					BNBasicCommonMethods.scrolldown(driver.findElement(By.xpath("(//*[@id='addrMatches']//li)["+(sel+1)+"]")), driver);
					WebElement usethibtn = driver.findElement(By.xpath("(//*[@id='addrMatches']//*[@class='btn-submit continue'])["+(sel+1)+"]"));
					usethibtn.click();
					//Thread.sleep(4000);
					BNBasicCommonMethods.waitforElement(wait, obj, "DeliveryOptionTitle");
					WebElement DeliveryOptionTitle = BNBasicCommonMethods.findElement(driver, obj, "DeliveryOptionTitle");
					wait.until(ExpectedConditions.visibilityOf(DeliveryOptionTitle));
					WebElement deliverycheckoutstep = driver.findElement(By.xpath("//*[@id='checkoutStepTwo']"));
					boolean steptwo = false;
					
					if(steptwo == deliverycheckoutstep.getAttribute("class").contains("Active"))
					{
						log.add("The User is navigated to the Delivery Page.");
						if(DeliveryOptionTitle.getText().equals(pagetitle))
						{
							childflag = true;
							log.add("The expected page title was : " + pagetitle);
							log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
							pass("The user is in the Delivery Page and the Page title matches the expected One.",log);
						
							
						}
						else
						{
							log.add("The expected page title was : " + pagetitle);
							log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
							fail("The user is in the Delivery Page.But there is mismatch in the page title.",log);
						}
					}
					else
					{
						fail("The user is not in the Delivery Page.Please Check.");
					}
				}
			}
			catch(Exception e)
			/*{
				else*/
				{
				   try
				   {
						WebElement addressVerificationMatchesTitle = BNBasicCommonMethods.findElement(driver, obj, "addressVerificationMatchesTitle");
						if(BNBasicCommonMethods.isElementPresent(addressVerificationMatchesTitle))
						{
							WebElement addressSuggestionFirstAddress = BNBasicCommonMethods.findElement(driver, obj, "addressSuggestionFirstAddress");
							BNBasicCommonMethods.scrolldown(addressSuggestionFirstAddress, driver);
							Random r = new Random();
							List <WebElement> checkoutAddressVerficationPossibleSuggestion = driver.findElements(By.xpath(" //*[@id='addrMatches']//li"));
							int sel = r.nextInt(checkoutAddressVerficationPossibleSuggestion.size());
							BNBasicCommonMethods.scrolldown(driver.findElement(By.xpath("(//*[@id='addrMatches']//li)["+(sel+1)+"]")), driver);
							WebElement usethibtn = driver.findElement(By.xpath("(//*[@id='addrMatches']//*[@class='btn-submit continue'])["+(sel+1)+"]"));
							usethibtn.click();
							//Thread.sleep(4000);
							BNBasicCommonMethods.waitforElement(wait, obj, "DeliveryOptionTitle");
							WebElement DeliveryOptionTitle = BNBasicCommonMethods.findElement(driver, obj, "DeliveryOptionTitle");
							wait.until(ExpectedConditions.visibilityOf(DeliveryOptionTitle));
							WebElement deliverycheckoutstep = driver.findElement(By.xpath("//*[@id='checkoutStepTwo']"));
							boolean steptwo = false;
							
							if(steptwo == deliverycheckoutstep.getAttribute("class").contains("Active"))
							{
								log.add("The User is navigated to the Delivery Page.");
								if(DeliveryOptionTitle.getText().equals(pagetitle))
								{
									childflag = true;
									log.add("The expected page title was : " + pagetitle);
									log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
									pass("The user is in the Delivery Page and the Page title matches the expected One.",log);
								}
								else
								{
									log.add("The expected page title was : " + pagetitle);
									log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
									fail("The user is in the Delivery Page.But there is mismatch in the page title.",log);
								}
							}
							else
							{
								fail("The user is not in the Delivery Page.Please Check.");
							}
						}
				    }	
					catch(Exception e1)
					/*{
					else*/
					  { 
						try
						{
						 WebElement addressVerificationNoMatchesTitle = BNBasicCommonMethods.findElement(driver, obj, "addressVerificationNoMatchesTitle");
						 if(BNBasicCommonMethods.isElementPresent(addressVerificationNoMatchesTitle))
						 {
							WebElement nomatchesbtn = driver.findElement(By.xpath("//*[@id='asEntered']"));
							nomatchesbtn.click();
							Thread.sleep(500);
							BNBasicCommonMethods.waitforElement(wait, obj, "deliverycheckoutstep");
							WebElement deliverycheckoutstep = driver.findElement(By.xpath("//*[@id='checkoutStepTwo']"));
							boolean steptwo = false;
							
							if(steptwo == deliverycheckoutstep.getAttribute("class").contains("Active"))
							{
								log.add("The User is navigated to the Delivery Page.");
								WebElement DeliveryOptionTitle = BNBasicCommonMethods.findElement(driver, obj, "DeliveryOptionTitle");
								if(DeliveryOptionTitle.getText().equals(pagetitle))
								{
									childflag = true;
									log.add("The expected page title was : " + pagetitle);
									log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
									pass("The user is in the Delivery Page and the Page title matches the expected One.",log);
								}
								else
								{
									log.add("The expected page title was : " + pagetitle);
									log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
									fail("The user is in the Delivery Page.But there is mismatch in the page title.",log);
								}
							}
							else
							{
								fail("The user is not in the Delivery Page.Please Check.");
							}
						}
						else
						{
							fail("The user is not redirected to the Address Verification Page.");
						}
					 }
						catch(Exception e2)
						{
							exception("Something went Wrong"+e.getMessage());
						}
					}
				}
			
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong. Please Check. " + e.getMessage());
		}
		
		ChildCreation("BNIA-1009 Verify that Address Verification page should be redirect user to delivery page if user selects use this address button");
        if(childflag)
        	pass("Address verification page is displayed and selected address form address verification page and navigate to delivey option page");
        else
        	fail("Address verification page is not displayed and selected address form address verification page and is not navigate to delivey option page");
	}
		
	/* BNIA-992, 973, 974 Guest Checkout – Verify that subtotal (number of items) and the total amount should be displayed at the bottom of the details in every section ie (Shipping,Delivery,payment and Review)*/
	public void guestCheckoutBottomSummarySectionDetails() throws IOException
	{
		ChildCreation(" BNIA-992, 973, 974 Verify that subtotal (number of items) and the total amount should be displayed at the bottom of the details in every section ie (Shipping,Delivery,payment and Review).");
		log.add("The browser is launched and it is running.");
		sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
		int brmkey = sheet.getLastRowNum();
		for(int i = 0; i <= brmkey; i++)
		{
			String tcid = "BRM631";
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				String pagetitle = sheet.getRow(i).getCell(2).getStringCellValue().toString();
				String[] chksteps = pagetitle.split("\n");
				String subtotallabelname = sheet.getRow(1).getCell(15).getStringCellValue();
				String subtotalvalue = sheet.getRow(1).getCell(16).getStringCellValue();
				String estimatedShiplabel = sheet.getRow(1).getCell(17).getStringCellValue();
				String estimatedshipvalue = sheet.getRow(1).getCell(18).getStringCellValue();
				String estimatedTaxlabel = sheet.getRow(1).getCell(19).getStringCellValue();
				String estimatedTaxvalue = sheet.getRow(1).getCell(20).getStringCellValue();
				String OrderTotallabel = sheet.getRow(1).getCell(21).getStringCellValue();
				String OrderTotalvalue = sheet.getRow(1).getCell(22).getStringCellValue();
				String creditcardholname = sheet.getRow(i).getCell(24).getStringCellValue();
				DataFormatter formatter = new DataFormatter(); //creating formatter using the default locale
				HSSFCell cell = sheet.getRow(i).getCell(23);
				cell.setCellType(Cell.CELL_TYPE_STRING);
				HSSFCell csvcell = sheet.getRow(i).getCell(25);
				String csvnum = formatter.formatCellValue(csvcell).toString();
				String creditcardholemail= sheet.getRow(i).getCell(26).getStringCellValue();
				try
				{
					//driver.get(BNConstants.guestcheckoutURL);
					List <WebElement> checkoutStepsLists = driver.findElements(By.xpath("//*[@id='checkoutSteps']//a"));
					if(BNBasicCommonMethods.isListElementPresent(checkoutStepsLists))
					{
						for(int j=1; j<=checkoutStepsLists.size(); j++)
						{
							ArrayList<String> labeldet = new ArrayList<>();
							ArrayList<String> valdet = new ArrayList<>();
							//WebElement checkoutSteps = BNBasicCommonMethods.findElement(driver, obj, "checkoutSteps");
						//	BNBasicCommonMethods.scrolldown(checkoutSteps, driver);
							WebElement chkoutstp = driver.findElement(By.xpath("(//*[@id='checkoutSteps']//a)["+j+"]"));
							String step = chkoutstp.getText();
							chkoutstp.click();
							//Thread.sleep(4000);
							//WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
							//wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
							//System.out.println(step);
							if(step.contains(chksteps[0]))
							{
								BNBasicCommonMethods.waitforElement(wait, obj, "contactNo");
								WebElement contactNo = BNBasicCommonMethods.findElement(driver, obj, "contactNo");
								BNBasicCommonMethods.scrolldown(contactNo, driver);
								WebElement summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
								if(BNBasicCommonMethods.isElementPresent(summaryContainer))
								{
									log.add("The Summary Container is found and it is displayed.");
									List <WebElement> orderSummaryLists = driver.findElements(By.xpath("//*[@id='summaryContainer']//li"));
									if(BNBasicCommonMethods.isListElementPresent(orderSummaryLists))
									{
										log.add("The Summary Container List is found and it is displayed.");
										for(int k = 1; k<=orderSummaryLists.size();k++)
										{
											WebElement label = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)["+k+"]//span)[1]"));
											WebElement details = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)["+k+"]//span)[2]"));
									        pass("The " + label.getText() + " line is found and it is displayed.");
											pass("The " + details.getText() + " line is found and it is displayed.",log);
											//System.out.println(label.getText());
											//System.out.println(details.getText());
											labeldet.add(label.getText());
											valdet.add(details.getText());
										}
										
										String[] det = labeldet.toArray(new String[labeldet.size()]);
										String[] val = valdet.toArray(new String[valdet.size()]);
										valcompare(det, val, subtotallabelname, subtotalvalue, estimatedShiplabel, estimatedshipvalue, estimatedTaxlabel, estimatedTaxvalue, OrderTotallabel, OrderTotalvalue);
										WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
										orderContinuebtn.click();
										Thread.sleep(2500);
									}
									else
									{
										fail("The Summary Conatainer List is not found.Please Check.");
									
									}
								}
								else
								{
									fail("The Summary Conatainer is not found.Please Check.");
								
								}
							}
							else if(step.contains(chksteps[1]))
							{
								try
								{
									WebElement DeliveryOptionTitle = BNBasicCommonMethods.findElement(driver, obj, "DeliveryOptionTitle");
									wait.until(ExpectedConditions.visibilityOf(DeliveryOptionTitle));
									if(BNBasicCommonMethods.isElementPresent(DeliveryOptionTitle))
									{
			
									//	BNBasicCommonMethods.scrolldown(deliveryCartsItemContainer, driver);
										WebElement summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
										if(BNBasicCommonMethods.isElementPresent(summaryContainer))
										{
											List <WebElement> orderSummaryLists = driver.findElements(By.xpath("//*[@id='summaryContainer']//li"));
											log.add("The Summary Container is found and it is displayed.");
											if(BNBasicCommonMethods.isListElementPresent(orderSummaryLists))
											{
												log.add("The Summary Container List is found and it is displayed.");
												for(int k = 1; k<=orderSummaryLists.size();k++)
												{
													WebElement label = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)["+k+"]//span)[1]"));
													WebElement details = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)["+k+"]//span)[2]"));
													//System.out.println(label.getText());
													//System.out.println(details.getText());
													labeldet.add(label.getText());
													valdet.add(details.getText());
												}
												
												String[] det = labeldet.toArray(new String[labeldet.size()]);
												String[] val = valdet.toArray(new String[valdet.size()]);
												valcompare(det, val, subtotallabelname, subtotalvalue, estimatedShiplabel, estimatedshipvalue, estimatedTaxlabel, estimatedTaxvalue, OrderTotallabel, OrderTotalvalue);
												WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
												orderContinuebtn.click();
												Thread.sleep(2500);
											}
											else
											{
												fail("The Summary Conatainer List is not found.Please Check.");
											}
										}
										else
										{
											fail("The Summary Conatainer is not found.Please Check.");
										}
									}
									else
									{
										fail("The User is not navigated to the Delivery option Page.");
									}
								}
								catch(Exception e)
								{
									fail("The User is not navigated to the Delivery option Page.");
									System.out.println(e.getMessage());
								}
								
							}
							else if(step.contains(chksteps[2]))
							{
								try
								{
								//	wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
									WebElement paymentsOption = BNBasicCommonMethods.findElement(driver, obj, "paymentsOption");
									if(BNBasicCommonMethods.isElementPresent(paymentsOption))
									{
										WebElement ccNumber = BNBasicCommonMethods.findElement(driver, obj, "ccNumber");
										BNBasicCommonMethods.scrolldown(ccNumber, driver);
										ccNumber.sendKeys("5589550010042341");
										WebElement ccName = BNBasicCommonMethods.findElement(driver, obj, "ccName");
										ccName.sendKeys(creditcardholname);
										WebElement MonthDropdown  = driver.findElement(By.xpath("//*[@id='ccMonth-replacement']"));
										MonthDropdown.click();
										List <WebElement> Monthlistsel = driver.findElements(By.xpath("//*[@id='ccMonth-option-list']//li"));
										
										Monthlistsel.get(Monthlistsel.size()-1).click();
										/*act.moveToElement(ccMonth).build().perform();
										Select mntsel = new Select(ccMonth);
										mntsel.selectByIndex(1);*/
										WebElement YearDropdown  = driver.findElement(By.xpath("//*[@id='ccYear-replacement']"));
										YearDropdown.click();
										List <WebElement> Yearlistsel = driver.findElements(By.xpath("//*[@id='ccYear-option-list']//li"));
										Yearlistsel.get(Yearlistsel.size()-1).click();
										/*Select mntsel = new Select(ccMonth);
										mntsel.selectByIndex(6);
										Select yrsel = new Select(ccYear);
										yrsel.selectByIndex(6);*/
										WebElement ccCsv = BNBasicCommonMethods.findElement(driver, obj, "ccCsv");
										ccCsv.sendKeys(csvnum);
										WebElement ccEmail = BNBasicCommonMethods.findElement(driver, obj, "ccEmail");
										//BNBasicCommonMethods.scrolldown(ccEmail, driver);
										ccEmail.sendKeys(creditcardholemail);
										WebElement summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
										if(BNBasicCommonMethods.isElementPresent(summaryContainer))
										{
											BNBasicCommonMethods.scrolldown(summaryContainer, driver);
											log.add("The Summary Container is found and it is displayed.");
											List <WebElement> orderSummaryLists = driver.findElements(By.xpath("//*[@id='summaryContainer']//li"));
											if(BNBasicCommonMethods.isListElementPresent(orderSummaryLists))
											{
												log.add("The Summary Container List is found and it is displayed.");
												for(int k = 1; k<=orderSummaryLists.size();k++)
												{
													WebElement label = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)["+k+"]//span)[1]"));
													WebElement details = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)["+k+"]//span)[2]"));
													//System.out.println(label.getText());
													//System.out.println(details.getText());
													labeldet.add(label.getText());
													valdet.add(details.getText());
												}
												
												String[] det = labeldet.toArray(new String[labeldet.size()]);
												String[] val = valdet.toArray(new String[valdet.size()]);
												valcompare(det, val, subtotallabelname, subtotalvalue, estimatedShiplabel, estimatedshipvalue, estimatedTaxlabel, estimatedTaxvalue, OrderTotallabel, OrderTotalvalue);
												WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
												orderContinuebtn.click();
												Thread.sleep(2500);
											}
											else
											{
												fail("The Summary Conatainer List is not found.Please Check.");
											
											}
										}
										else
										{
											fail("The Summary Conatainer is not found.Please Check.");
										
										}
									}
									else
									{
										fail("The User is not navigated to the Payments Page and the Payments Option Tab is not found.");
									
									}
								}
								catch(Exception e)
								{
									fail("The User is not navigated to the Delivery option Page.");
									System.out.println(e.getMessage());
								
								}
							}
							else if(step.contains(chksteps[3]))
							{
								WebElement reviewPagetitle = BNBasicCommonMethods.findElement(driver, obj, "reviewPagetitle");
								wait.until(ExpectedConditions.visibilityOf(reviewPagetitle));
								if(BNBasicCommonMethods.isElementPresent(reviewPagetitle))
								{
									pass("The user is navigated to the Review Order Page.");
									WebElement summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
									if(BNBasicCommonMethods.isElementPresent(summaryContainer))
									{
										BNBasicCommonMethods.scrolldown(summaryContainer, driver);
										Thread.sleep(1500);
										List <WebElement> orderSummaryLists = driver.findElements(By.xpath("//*[@id='summaryContainer']//li"));
										log.add("The Summary Container is found and it is displayed.");
										if(BNBasicCommonMethods.isListElementPresent(orderSummaryLists))
										{
											log.add("The Summary Container List is found and it is displayed.");
											for(int k = 1; k<=orderSummaryLists.size();k++)
											{
												WebElement label = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)["+k+"]//span)[1]"));
												WebElement details = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)["+k+"]//span)[2]"));
												//System.out.println(label.getText());
												//System.out.println(details.getText());
												labeldet.add(label.getText());
												valdet.add(details.getText());
											}
											
											String[] det = labeldet.toArray(new String[labeldet.size()]);
											String[] val = valdet.toArray(new String[valdet.size()]);
											valcompare(det, val, subtotallabelname, subtotalvalue, estimatedShiplabel, estimatedshipvalue, estimatedTaxlabel, estimatedTaxvalue, OrderTotallabel, OrderTotalvalue);
											Thread.sleep(2500);
										}
										else
										{
											fail("The Summary Conatainer List is not found.Please Check.");
										
										}
									}
									else
									{
										fail("The Summary Conatainer is not found.Please Check.");
									
									}
								}
								else
								{
									fail("The user is not navigated to the review Order page.");
								
								}
								}
							}
						}
					else
					{
						fail("The Checkout Steps is not found.Please Check.");
					
					}
				}
				catch(Exception e)
				{
					System.out.println(e.getMessage());
					exception(" There is something wrong.Please Check." + e.getMessage());
					System.out.println(e.getMessage());
				}
			}
		}
	}

	/* BNIA-991 Guest Checkout - Verify that Shipping,delivery,payment and review tabs(breadcrumb) are highlighted till shipping*/
	public void guestCheckoutBreadcrumHighlight()
	{
		ChildCreation(" BNIA-991 Guest Checkout - Verify that Shipping,delivery,payment and review tabs(breadcrumb) are highlighted till shipping.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String color = BNBasicCommonMethods.getExcelVal("BRM656", sheet, 3);
			WebElement reviewPagetitle = BNBasicCommonMethods.findElement(driver, obj, "reviewPagetitle");
			wait.until(ExpectedConditions.visibilityOf(reviewPagetitle));
			List <WebElement> checkoutStepsLists = driver.findElements(By.xpath("//*[@id='checkoutSteps']//a"));
			if(BNBasicCommonMethods.isListElementPresent(checkoutStepsLists))
			{
				//WebElement checkoutSteps = BNBasicCommonMethods.findElement(driver, obj, "checkoutSteps");
				//BNCheckoutEditShipandBillAddress.BNBasicCommonMethods.scrollup(checkoutSteps, driver);
				String[] script = new String[4];
				String[] hexCode = new String[script.length];
				String[] breadcrumtitle = new String[script.length];
				log.add("The Checkout Steps are displayed.");
				script[0] = "return window.getComputedStyle(document.querySelector('#checkoutStepOne'),':before').getPropertyValue('border-bottom-color')";
				script[1] = "return window.getComputedStyle(document.querySelector('#checkoutStepTwo'),':before').getPropertyValue('border-bottom-color')";
				script[2] = "return window.getComputedStyle(document.querySelector('#checkoutStepThree'),':before').getPropertyValue('border-bottom-color')";
				script[3] = "return window.getComputedStyle(document.querySelector('#checkoutStepFour'),':before').getPropertyValue('border-bottom-color')";
				
				
				for(int j=0; j<script.length; j++)
				{
					WebElement ele = driver.findElement(By.xpath("(//*[@id='checkoutSteps']//li//a)["+(j+1)+"]"));
					String elecolor = ele.getCssValue("color");
					Color colorhxcnvt = Color.fromString(elecolor);
					breadcrumtitle[j] = colorhxcnvt.asHex();
					JavascriptExecutor js = (JavascriptExecutor)driver;
					String content = (String) js.executeScript(script[j]);
					//System.out.println(content);
					//String split = content.substring(0,16);
					Color colorhxcnvt1 = Color.fromString(content);
					hexCode[j] = colorhxcnvt1.asHex();
					if(color.equals(hexCode[j]))
					{
						log.add("The " + ele.getText() + " breadcrum expected color was " + color);
						log.add("The " + ele.getText() + " breadcrum actual color is " + hexCode[j]);
						pass("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
					}
					else
					{
						log.add("The " + ele.getText() + " breadcrum expected color was " + color);
						log.add("The " + ele.getText() + " breadcrum actual color is " + hexCode[j]);
						fail("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
					}
					
					if(color.equals(breadcrumtitle[j]))
					{
						log.add("The " + ele.getText() + " breadcrum expected color was " + color);
						log.add("The " + ele.getText() + " breadcrum actual color is " + breadcrumtitle[j]);
						pass("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
					}
					else
					{
						log.add("The " + ele.getText() + " breadcrum expected color was " + color);
						log.add("The " + ele.getText() + " breadcrum actual color is " + breadcrumtitle[j]);
						fail("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
					}
				}
			}
		}
		catch (Exception e)
		{
			exception(" There is something wrong.Please Check." + e.getMessage());
			System.out.println(e.getMessage());
		}
	}

/*	 BRM - 550 Verify that user should be able to click the "Other Ways to Pay" option in the checkout page 


	public void guestCheckoutOtherwaysToPay()
	{
		ChildCreation(" BRM - 550 Verify that user should be able to click the Other Ways to Pay option in the checkout page.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String name = BNBasicCommonMethods.getExcelVal("BRM550", sheet, 2);
			String[] caption = name.split("\n");
			WebElement stpthree = driver.findElement(By.xpath("//*[@id='checkoutStepThree']"));
			stpthree.click();
			Thread.sleep(3000);
			BNBasicCommonMethods.waitforElement(wait, obj, "guestCheckoutAddPageTitle");
			WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
			//wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
			if(guestCheckoutAddPageTitle.getText().equals(caption[0]))
			{
				WebElement otherPayOptionNotActive = BNBasicCommonMethods.findElement(driver, obj, "otherPayOptionNotActive");
				log.add("The user is navigated to the Payment Page in the Guest Checkout Page.");
				wait.until(ExpectedConditions.visibilityOf(otherPayOptionNotActive));
				if(BNBasicCommonMethods.isElementPresent(otherPayOptionNotActive))
				{
					pass("The Other Pay Option tab is found.");
					if(otherPayOptionNotActive.getText().equals(caption[1]))
					{
						pass("The Other Pay Option caption matches the expected content.");
					}
					else
					{
						fail("The Other Pay Option caption does not matches the expected content.");
					}
				}
				else
				{
					fail("The Other Pay Option tab is not found.");
				}
			}
			else
			{
				fail("The user is not in the Payments Page of the Guest Checkout.");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong.Please Check." + e.getMessage());
			System.out.println(e.getMessage());
		}
	}

	 BRM - 551 Verify that user should be able to click the "Other Ways to Pay" option in the checkout page 
	public void guestCheckoutOtherwaysToPayNavigation()
	{
		ChildCreation(" BRM - 551 Verify that user should be able to click the Other Ways to Pay option in the checkout page.");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String name = BNBasicCommonMethods.getExcelVal("BRM551", sheet, 2);
			String[] caption = name.split("\n");
			WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
			if(guestCheckoutAddPageTitle.getText().equals(caption[0]))
			{
				log.add("The user is navigated to the Payment Page in the Guest Checkout Page.");
				WebElement otherPayOptionNotActive = BNBasicCommonMethods.findElement(driver, obj, "otherPayOptionNotActive");
				if(BNBasicCommonMethods.isElementPresent(otherPayOptionNotActive))
				{
					log.add("The Other Pay Option tab is found.");
					otherPayOptionNotActive.click();
					Thread.sleep(2000);
					WebElement ele = driver.findElement(By.xpath("(//*[@id='paymentOptions']//*[contains(@style,'block')])[4]"));
					if(BNBasicCommonMethods.isElementPresent(ele))
					{
						pass("The Other Pay Option content is displayed",log);
					}
					else
					{
						fail("The Other Pay Option content is not displayed.",log);
					}
				}
				else
				{
					fail("The Other Pay Option tab is not found.",log);
				}
			}
			else
			{
				fail("The user is not in the Payments Page of the Guest Checkout.",log);
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong.Please Check." + e.getMessage());
			System.out.println(e.getMessage());
		}
	}*/
		
	/* BNIA-978 verify that clicking on tab in virtual keypad , the cursor moves displaying the next field while filling the shipping address*/ 
	public void guestCheckoutTabKeyUse()
	{
		ChildCreation(" BNIA-978 verify that clicking on tab in virtual keypad , the cursor moves displaying the next field while filling the shipping address .");
		try
		{
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			WebElement checkoutSteps = BNBasicCommonMethods.findElement(driver, obj, "checkoutSteps");
			BNBasicCommonMethods.scrollup(checkoutSteps, driver);
			Actions act = new Actions(driver);
			act.sendKeys(Keys.TAB).build().perform();
			pass("The User is navigated to the First Name field.");
			act.sendKeys(Keys.TAB).build().perform();
			pass("The User is navigated to the Last Name field.");
			act.sendKeys(Keys.TAB).build().perform();
			pass("The User is navigated to the Street Address field.");
			act.sendKeys(Keys.TAB).build().perform();
			pass("The User is navigated to the Apt Suite field.");
			act.sendKeys(Keys.TAB).build().perform();
			pass("The User is navigated to the City field.");
			act.sendKeys(Keys.TAB).build().perform();
			pass("The User is navigated to the State field.");
			act.sendKeys(Keys.TAB).build().perform();
			pass("The User is navigated to the Zipcode field.");
			act.sendKeys(Keys.TAB).build().perform();
			pass("The User is navigated to the Contact Number field.");
			act.sendKeys(Keys.TAB).build().perform();
			pass("The User is navigated to the Company field field.");
		}
		catch (Exception e)
		{
			exception("There is something wrong.Please Check." + e.getMessage());
			System.out.println(e.getMessage());
		}
	}	
 
/************************************************************************Guest Delivery Option New************************************************************/ 

	public void tempGuestNavigation()
	{
		try
		{
			//driver.get(BNConstants.guestcheckoutURL);
			List <WebElement> checkoutStepsLists = driver.findElements(By.xpath("//*[@id='checkoutSteps']//a"));
			if(checkoutStepsLists.get(1).isDisplayed())
			{
				Actions act = new Actions(driver);
				for(int ctr=0 ; ctr<50 ;ctr++)
                act.sendKeys(Keys.CONTROL).sendKeys(Keys.ARROW_UP).build().perform();
			}
				//BNBasicCommonMethods.scrollup(checkoutStepsLists.get(1), driver);
			checkoutStepsLists.get(1).click();
			
		}
		catch(Exception e)
		{
			System.out.println("There is something wrong"+e.getMessage());
		}
	}
			
	/*BNIA-781 Verify that when the "Shopping Bag/Add to Bag" icon is clicked in the header it should naviagte to "Shopping Bag" page.*/
	public boolean bagCount1() throws Exception
		{
		    ChildCreation("BNIA-781 Verify that when the Shopping Bag/Add to Bag icon is clicked in the header it should naviagte to Shopping Bag page.");
			boolean prdtadded = false;
			int count = 1;
			do
			{
				WebElement ShoppingBagCount = BNBasicCommonMethods.findElement(driver, obj, "ShoppingBagCount");
				int prdtval = Integer.parseInt(ShoppingBagCount.getText());
				if(prdtval>0)
				{
					prdtadded=true;
					WebElement ShoppingBagIcon = BNBasicCommonMethods.findElement(driver, obj, "ShoppingBagIcon");
					ShoppingBagIcon.click();
					//Thread.sleep(4000);
					pass("Shopping bag page is displayed and with Added products");
					BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
					WebElement LoginCheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "LoginCheckoutBtn");
					if(LoginCheckoutBtn.isDisplayed())
					{
						pass("Shopping bag page is displayed with added Item");
					}
					else
					{
						fail("Shopping bag page is not displayed with added items");
					}
					wait.until(ExpectedConditions.visibilityOf(LoginCheckoutBtn));
					LoginCheckoutBtn.click();
					//Thread.sleep(1000);	
					Thread.sleep(250);
					BNBasicCommonMethods.waitforElement(wait, obj, "signInIframe");
					WebElement signInIframe = BNBasicCommonMethods.findElement(driver, obj, "signInIframe");
					driver.switchTo().frame(signInIframe);
					//wait.until(ExpectedConditions.attributeToBe(miniCartLoadingGauge, "style", "display: none;"));
					//wait.until(ExpectedConditions.visibilityOf(secureCheckOutBtn));
					break;
				}
				else
				{
					count++;
					prdtadded=false;
					productaddtobag();
					continue;
				}
			}while(prdtadded==true||count<10);
			return prdtadded;
		}
		
	/* BNIA-971 Guest Checkout - Verify that on selecting the "Checkout" button the Sign In overlay should be enabled to Proceed for the Sign in/Check out as Guest checkout page.*/
	public void signInorCheckoutoverlay1()
		{
			ChildCreation(" BNIA-971 Guest Checkout - Verify that on selecting the Checkout button the Sign In overlay should be enabled to Proceed for the Sign in/Check out as Guest checkout page.");
			try
			{
				sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
				String pgcaption = BNBasicCommonMethods.getExcelVal("BRM623", sheet, 2);
				String[] title = pgcaption.split("\n");
				//addProduct(sheet, Child, false);
				//Thread.sleep(2000);
				//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(checkoutpageframe));
				BNBasicCommonMethods.waitforElement(wait, obj, "pgetitle");
				WebElement pgetitle = BNBasicCommonMethods.findElement(driver, obj, "pgetitle");
				wait.until(ExpectedConditions.visibilityOf(pgetitle));
				if(pgetitle.getText().equals(title[0]))
				{
					//System.out.println(pgetitle.getText());
					log.add("The Page title is : " + pgetitle.getText());
					pass("The user is navigated to the Checkout page and the caption matches with the expected one.",log);
				}
				else
				{
					log.add("The Page title is : " + pgetitle.getText());
					log.add("The expected page title is " + title[0]);
					fail("The user is navigated to the Checkout page and the caption does not matches with the expected one.",log);
				}
				
				WebElement checkoutAsGuestbtn = BNBasicCommonMethods.findElement(driver, obj, "checkoutAsGuestbtn");
				if(BNBasicCommonMethods.isElementPresent(checkoutAsGuestbtn))
				{
					pass("The Checkout as Guset button is visible.");
					if(checkoutAsGuestbtn.getText().equals(title[1]))
					{
						pass("The Checkout as Guest button caption matches with the expected one.");
					}
					else
					{
					fail("The Checkout as Guest button caption does not matches with the expected one.");
					}
				}
				else
				{
					fail("The Checkout as Guest Button is not visible / present.");
				}
			}
			catch (Exception e)
			{
				exception("There is something wrong.Please Check." + e.getMessage());
				System.out.println(e.getMessage());
			}
		}

	/* BNIA-972 Guest Checkout - Verify that Shipping section should be displayed as per the creative in the guest checkout page*/
	public void guestCheckoutoverlay1()
		{
			ChildCreation(" BNIA-972 Guest Checkout - Verify that Shipping section should be displayed as per the creative in the guest checkout page.");
			try
			{
				sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
				String pgcaption = BNBasicCommonMethods.getExcelVal("BRM627", sheet, 2);
				String expcolor = BNBasicCommonMethods.getExcelVal("BRM627", sheet, 3);
				String[] title = pgcaption.split("\n");
				WebElement checkoutAsGuestbtn = BNBasicCommonMethods.findElement(driver, obj, "checkoutAsGuestbtn");
				if(BNBasicCommonMethods.isElementPresent(checkoutAsGuestbtn))
				{
					checkoutAsGuestbtn.click();
				    Thread.sleep(1500);
				    BNBasicCommonMethods.waitforElement(wait, obj, "guestCheckoutAddPageTitle");
					WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
					//wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
					//Thread.sleep(1000);
					
					if(BNBasicCommonMethods.isElementPresent(guestCheckoutAddPageTitle))
					{
						log.add("The User is naviagted to the Guest Checkout Page.");
						if(guestCheckoutAddPageTitle.getText().contains(title[0]))
						{
							log.add("The expected page title was : " + title[0]);
							log.add("The actual page title is : " + guestCheckoutAddPageTitle.getText());
							pass("The Guest Checkout Page title matches the expected content.",log);
						}
						else
						{
							System.out.println(guestCheckoutAddPageTitle.getText());
							log.add("The expected page title was : " + title[0]);
							log.add("The actual page title is : " + guestCheckoutAddPageTitle.getText());
							fail("The Guest Checkout Page title does not matches the expected content.",log);
						}
						
						WebElement checkoutSteps = BNBasicCommonMethods.findElement(driver, obj, "checkoutSteps");
						if(BNBasicCommonMethods.isElementPresent(checkoutSteps))
						{
							pass("The Checkout Steps container is displayed.");
							List <WebElement> checkoutStepsLists = driver.findElements(By.xpath("//*[@id='checkoutSteps']//a"));
							if(BNBasicCommonMethods.isListElementPresent(checkoutStepsLists))
							{
								for(int j = 1; j<=checkoutStepsLists.size();j++)
								{
									WebElement chksteps = driver.findElement(By.xpath("(//*[@id='checkoutSteps']//a)["+j+"]"));
									if(chksteps.getText().contains(title[j]))
									{
										log.add("The exected " + j + " step in the Checkout Steps is : " + title[j]);
										log.add("The actual " + j + " step in the Checkout Steps is : " + chksteps.getText());
										pass("The Checkout Steps " + j + " matches with the expected one.",log);
									}
									else
									{
										log.add("The exected " + j + " step in the Checkout Steps is : " + title[j]);
										log.add("The actual " + j + " step in the Checkout Steps is : " + chksteps.getText());
										fail("The Checkout Steps " + j + " does not matches with the expected one.",log);
									}
								}
							}
							else
							{
								fail("The Checkout Steps List is not displayed.Please Check.");
							}
						}
						else
						{
							fail("The Checkout Steps container is not displayed.");
						}
						
						WebElement countrySelection = BNBasicCommonMethods.findElement(driver, obj, "countrySelection");
						if(BNBasicCommonMethods.isElementPresent(countrySelection))
						{
							pass("The Country drop down list box is present and visible.");
						}
						else
						{
							fail("The Country drop down list box is not present / visible.");
						}
						
						WebElement fName = BNBasicCommonMethods.findElement(driver, obj, "fName");
						if(BNBasicCommonMethods.isElementPresent(fName))
						{
							pass("The First Name text field is present and visible.");
						}
						else
						{
							fail("The First Name text field is not present / visible.");
						}
						
						WebElement lName = BNBasicCommonMethods.findElement(driver, obj, "lName");
						if(BNBasicCommonMethods.isElementPresent(lName))
						{
							pass("The Last Name text field is present and visible.");
						}
						else
						{
							fail("The Last Name text field is not present / visible.");
						}
						
						WebElement stAddress = BNBasicCommonMethods.findElement(driver, obj, "stAddress");
						if(BNBasicCommonMethods.isElementPresent(stAddress))
						{
							pass("The Stree Address text field is present and visible.");
						}
						else
						{
							fail("The Stree Address text field is not present / visible.");
						}
						
						WebElement aptSuiteAddress = BNBasicCommonMethods.findElement(driver, obj, "aptSuiteAddress");
						BNBasicCommonMethods.scrolldown(aptSuiteAddress, driver);
						if(BNBasicCommonMethods.isElementPresent(aptSuiteAddress))
						{
							pass("The Apt/Suite text field is present and visible.");
						}
						else
						{
							fail("The Apt/Suite text field is not present / visible.");
						}
						
						WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
						if(BNBasicCommonMethods.isElementPresent(city))
						{
							pass("The City text field is present and visible.");
						}
						else
						{
							fail("The City text field is not present / visible.");
						}
						
						WebElement state = BNBasicCommonMethods.findElement(driver, obj, "state");
						if(BNBasicCommonMethods.isElementPresent(state))
						{
							pass("The State selection drop down is present and visible.");
						}
						else
						{
							fail("The State selection drop down is not present / visible.");
						}
						
						WebElement zipCode = BNBasicCommonMethods.findElement(driver, obj, "zipCode");
						if(BNBasicCommonMethods.isElementPresent(zipCode))
						{
							pass("The Zipcode text field is present and visible.");
						}
						else
						{
							fail("The Zipcode text field is not present / visible.");
						}
						
						WebElement contactNo = BNBasicCommonMethods.findElement(driver, obj, "contactNo");
						if(BNBasicCommonMethods.isElementPresent(contactNo))
						{
							pass("The Phone Number text field is present and visible.");
						}
						else
						{
							fail("The Phone Number text field is not present / visible.");
						}
						
						WebElement companyName = BNBasicCommonMethods.findElement(driver, obj, "companyName");
						if(BNBasicCommonMethods.isElementPresent(companyName))
						{
							pass("The Company Name text field is present and visible.");
						}
						else
						{
							fail("The Company Name text field is not present / visible.");
						}
						
						WebElement summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
						if(BNBasicCommonMethods.isElementPresent(summaryContainer))
						{
							pass("The Summary Container area is present and visible.");
						
						}
						else
						{
							fail("The Summary Container area is not present / visible.");
						
						}
						
						WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
						if(BNBasicCommonMethods.isElementPresent(orderContinuebtn))
						{
							pass("The Continue button is present and visible.");
							if(BNBasicCommonMethods.colorfindernew(orderContinuebtn).equals(expcolor))
							{
								log.add("The expected color of the Contintue button is : " + BNBasicCommonMethods.colorfindernew(orderContinuebtn).toString());
								log.add("The actual color of the Continue button is : " + expcolor);
								pass("The Continue button color matches with the expected One.",log);
							}
							else
							{
								log.add("The expected color of the Contintue button is : " + BNBasicCommonMethods.colorfindernew(orderContinuebtn).toString());
								log.add("The actual color of the Continue button is : " + expcolor);
								fail("The Continue button color does not matches with the expected One.",log);
							}
						}
						else
						{
							fail("The Summary Container area is not present / visible.");
						}
					}
					else
					{
						fail("The User is not naviagted to the Guest Checkout Page.");
					}
				}
				else
				{
					exception("The Checkout as Guest button is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				exception(" There is something wrong.Please Check." + e.getMessage());
			
			}
		}

   /*BNIA-975 Guest Checkout - Verify that guest user can able to add the new shipping address details in the shipping section*/
	public void guestCheckoutEnterdata()
		{
			ChildCreation(" BNIA-975 Guest Checkout - Verify that guest user can able to add the new shipping address details in the shipping section.");
			try
			{
				sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
				String firstname = BNBasicCommonMethods.getExcelVal("BRM628", sheet, 6);
				String lastname = BNBasicCommonMethods.getExcelVal("BRM628", sheet, 7);
				String streetAdd = BNBasicCommonMethods.getExcelVal("BRM628", sheet, 8);
				String cty = BNBasicCommonMethods.getExcelVal("BRM628", sheet, 10);
				String zpcode = BNBasicCommonMethods.getExcelNumericVal("BRM628", sheet, 12);
				String cntnumber = BNBasicCommonMethods.getExcelNumericVal("BRM628", sheet, 13);
				WebElement guestCheckoutonetitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutonetitle");
				BNBasicCommonMethods.scrollup(guestCheckoutonetitle, driver);
				//Actions act = new Actions(driver);
				WebElement Countrycontainer = BNBasicCommonMethods.findElement(driver, obj, "countrycontiner");
				Countrycontainer.click();
				List<WebElement> countryLst = driver.findElements(By.xpath("//*[@id='country-option-list']//li"));
				//driver.findElement(By.xpath("//*[@id='state-option-35']")).click();
				//System.out.println(countryLst.get(0).getText());
				countryLst.get(0).click();
				/*act.moveToElement(countrySelection).build().perform();
				Select sel = new Select(countrySelection);*/
				/*sel.selectByValue("US");*/
				clearAll();
				WebElement fName = BNBasicCommonMethods.findElement(driver, obj, "fName");
				fName.sendKeys(firstname);
				WebElement lName = BNBasicCommonMethods.findElement(driver, obj, "lName");
				lName.sendKeys(lastname);
				WebElement stAddress = BNBasicCommonMethods.findElement(driver, obj, "stAddress");
				stAddress.sendKeys(streetAdd);
				WebElement companyName = BNBasicCommonMethods.findElement(driver, obj, "companyName");
				BNBasicCommonMethods.scrollup(companyName, driver);
				WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
				city.sendKeys(cty);
			//	WebElement state = BNBasicCommonMethods.findElement(driver, obj, "state");
				// Create instance of Javascript executor
				/*JavascriptExecutor je = (JavascriptExecutor) driver;
				//Identify the WebElement which will appear after scrolling down
				// now execute query which actually will scroll until that element is not appeared on page.
				je.executeScript("arguments[0].scrollIntoView(true);",state);
				*/
				WebElement Statecontainer = BNBasicCommonMethods.findElement(driver, obj, "Statecontainer");
				Statecontainer.click();
				JavascriptExecutor je = (JavascriptExecutor) driver;
				//Identify the WebElement which will appear after scrolling down
				// now execute query which actually will scroll until that element is not appeared on page.
				List<WebElement> stateLst = driver.findElements(By.xpath("//*[@id='state-option-list']//li"));
				je.executeScript("arguments[0].scrollIntoView(true);",stateLst.get(35));
				//driver.findElement(By.xpath("//*[@id='state-option-35']")).click();
				//System.out.println(stateLst.get(35).getText());
				stateLst.get(35).click();
				/*Select stsel = new Select(state);
				stsel.selectByIndex(35);*/
				WebElement zipCode = BNBasicCommonMethods.findElement(driver, obj, "zipCode");
				zipCode.sendKeys(zpcode);
				WebElement contactNo = BNBasicCommonMethods.findElement(driver, obj, "contactNo");
				contactNo.sendKeys(cntnumber);
				pass("Have Entered new shipping address details in the shipping section");
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				exception(" There is something wrong.Please Check." + e.getMessage());
				
			}
		}
			 
	/*BNIA-994 Guest Checkout - Verify that on entering all the input fields and clicking on "Continue" button should proceed to the Delivery page*/ 
	/*BNIA-1009 Verify that Address Verification page should be redirect user to delivery page if user selects "use this address" button*/
	public void guestCheckoutDeliveryPageNavigation1()
		{
			ChildCreation(" BNIA-994 Guest Checkout - Verify that on entering all the input fields and clicking on Continue button should proceed to the Delivery page.");
			 boolean childflag = false;
			try
			{
			   	
				sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
				String pagetitle = BNBasicCommonMethods.getExcelVal("BRM657", sheet, 3);
				WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
				BNBasicCommonMethods.scrolldown(city, driver);
				WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
				orderContinuebtn.click();
				//Thread.sleep(6000);
				BNBasicCommonMethods.waitforElement(wait, obj, "gadrverficationtitle");
				WebElement addressVerificationTitle = BNBasicCommonMethods.findElement(driver, obj, "gadrverficationtitle");
				wait.until(ExpectedConditions.visibilityOf(addressVerificationTitle));
				BNBasicCommonMethods.waitforElement(wait, obj, "addressVerificationPartialMatchesTitle");
				WebElement addressVerificationPartialMatchesTitle = BNBasicCommonMethods.findElement(driver, obj, "addressVerificationPartialMatchesTitle");
				if(BNBasicCommonMethods.isElementPresent(addressVerificationPartialMatchesTitle))
				{
					WebElement addressSuggestionFirstAddress = BNBasicCommonMethods.findElement(driver, obj, "addressSuggestionFirstAddress");
					BNBasicCommonMethods.scrolldown(addressSuggestionFirstAddress, driver);
					Random r = new Random();
					List <WebElement> checkoutAddressVerficationPossibleSuggestion = driver.findElements(By.xpath(" //*[@id='addrMatches']//li"));
					int sel = r.nextInt(checkoutAddressVerficationPossibleSuggestion.size());
					BNBasicCommonMethods.scrolldown(driver.findElement(By.xpath("(//*[@id='addrMatches']//li)["+(sel+1)+"]")), driver);
					WebElement usethibtn = driver.findElement(By.xpath("(//*[@id='addrMatches']//*[@class='btn-submit continue'])["+(sel+1)+"]"));
					usethibtn.click();
					//Thread.sleep(1500);
					BNBasicCommonMethods.waitforElement(wait, obj, "DeliveryOptionTitle");
					WebElement DeliveryOptionTitle = BNBasicCommonMethods.findElement(driver, obj, "DeliveryOptionTitle");
					wait.until(ExpectedConditions.visibilityOf(DeliveryOptionTitle));
					WebElement deliverycheckoutstep = driver.findElement(By.xpath("//*[@id='checkoutStepTwo']"));
					boolean steptwo = false;
					
					if(steptwo == deliverycheckoutstep.getAttribute("class").contains("Active"))
					{
						log.add("The User is navigated to the Delivery Page.");
						if(DeliveryOptionTitle.getText().equals(pagetitle))
						{
							childflag = true;
							log.add("The expected page title was : " + pagetitle);
							log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
							pass("The user is in the Delivery Page and the Page title matches the expected One.",log);
						
							
						}
						else
						{
							log.add("The expected page title was : " + pagetitle);
							log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
							fail("The user is in the Delivery Page.But there is mismatch in the page title.",log);
						}
					}
					else
					{
						fail("The user is not in the Delivery Page.Please Check.");
					}
				}
				else
				{
					WebElement addressVerificationMatchesTitle = BNBasicCommonMethods.findElement(driver, obj, "addressVerificationMatchesTitle");
					if(BNBasicCommonMethods.isElementPresent(addressVerificationMatchesTitle))
					{
						WebElement addressSuggestionFirstAddress = BNBasicCommonMethods.findElement(driver, obj, "addressSuggestionFirstAddress");
						BNBasicCommonMethods.scrolldown(addressSuggestionFirstAddress, driver);
						Random r = new Random();
						List <WebElement> checkoutAddressVerficationPossibleSuggestion = driver.findElements(By.xpath(" //*[@id='addrMatches']//li"));
						int sel = r.nextInt(checkoutAddressVerficationPossibleSuggestion.size());
						BNBasicCommonMethods.scrolldown(driver.findElement(By.xpath("(//*[@id='addrMatches']//li)["+(sel+1)+"]")), driver);
						WebElement usethibtn = driver.findElement(By.xpath("(//*[@id='addrMatches']//*[@class='btn-submit continue'])["+(sel+1)+"]"));
						usethibtn.click();
						Thread.sleep(250);
						BNBasicCommonMethods.waitforElement(wait, obj, "DeliveryOptionTitle");
						WebElement DeliveryOptionTitle = BNBasicCommonMethods.findElement(driver, obj, "DeliveryOptionTitle");
						wait.until(ExpectedConditions.visibilityOf(DeliveryOptionTitle));
						WebElement deliverycheckoutstep = driver.findElement(By.xpath("//*[@id='checkoutStepTwo']"));
						boolean steptwo = false;
						
						if(steptwo == deliverycheckoutstep.getAttribute("class").contains("Active"))
						{
							log.add("The User is navigated to the Delivery Page.");
							if(DeliveryOptionTitle.getText().equals(pagetitle))
							{
								log.add("The expected page title was : " + pagetitle);
								log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
								pass("The user is in the Delivery Page and the Page title matches the expected One.",log);
							}
							else
							{
								log.add("The expected page title was : " + pagetitle);
								log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
								fail("The user is in the Delivery Page.But there is mismatch in the page title.",log);
							}
						}
						else
						{
							fail("The user is not in the Delivery Page.Please Check.");
						}
					}
					else
					{
						WebElement addressVerificationNoMatchesTitle = BNBasicCommonMethods.findElement(driver, obj, "addressVerificationNoMatchesTitle");
						if(BNBasicCommonMethods.isElementPresent(addressVerificationNoMatchesTitle))
						{
							WebElement nomatchesbtn = driver.findElement(By.xpath("//*[@id='asEntered']"));
							nomatchesbtn.click();
							Thread.sleep(250);
							BNBasicCommonMethods.waitforElement(wait, obj, "deliverycheckoutstep");
							WebElement deliverycheckoutstep = driver.findElement(By.xpath("//*[@id='checkoutStepTwo']"));
							boolean steptwo = false;
							
							if(steptwo == deliverycheckoutstep.getAttribute("class").contains("Active"))
							{
								log.add("The User is navigated to the Delivery Page.");
								WebElement DeliveryOptionTitle = BNBasicCommonMethods.findElement(driver, obj, "DeliveryOptionTitle");
								if(DeliveryOptionTitle.getText().equals(pagetitle))
								{
									log.add("The expected page title was : " + pagetitle);
									log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
									pass("The user is in the Delivery Page and the Page title matches the expected One.",log);
								}
								else
								{
									log.add("The expected page title was : " + pagetitle);
									log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
									fail("The user is in the Delivery Page.But there is mismatch in the page title.",log);
								}
							}
							else
							{
								fail("The user is not in the Delivery Page.Please Check.");
							}
						}
						else
						{
							fail("The user is not redirected to the Address Verification Page.");
						}
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(e.getMessage());
				exception("There is something wrong. Please Check. " + e.getMessage());
			}
			
			ChildCreation("BNIA-1009 Verify that Address Verification page should be redirect user to delivery page if user selects use this address button");
			if(childflag)
				pass("Address verification page is displayed and selected address form address verification page and navigate to delivey option page");
			else
				fail("The user is in the Delivery Page.But there is mismatch in the page title.");
				
		}
			
    /* BNIA-1146 Guest Checkout - Verify that delivery section should allow the users to enter the required delivery details in the guest checkout page.*/
	public void guestCheckoutDeliveryPage()
	{
		ChildCreation("BNIA-1146 Guest Checkout - Verify that delivery section should allow the users to enter the required delivery details in the guest checkout page.");
		try
		{
			 sheet = BNBasicCommonMethods.excelsetUp("Guest Delivery Options");
			String cellVal = BNBasicCommonMethods.getExcelVal("BRM659", sheet, 2);
			String[] Val = cellVal.split("\n");
			BNBasicCommonMethods.waitforElement(wait, obj, "deliveryOptionContainer");
			WebElement deliveryOptionContainer = BNBasicCommonMethods.findElement(driver, obj, "deliveryOptionContainer");
			if(BNBasicCommonMethods.isElementPresent(deliveryOptionContainer))
			{
				pass("The Delivery Option container is found.");
			}
			else
			{
				fail("The Delivery Option container is not found.");
			}
			
			WebElement deliveryPreferenceTitle = BNBasicCommonMethods.findElement(driver, obj, "deliveryPreferenceTitle");
			if(BNBasicCommonMethods.isElementPresent(deliveryPreferenceTitle))
			{
				if((deliveryPreferenceTitle.getText().isEmpty())&&(!deliveryPreferenceTitle.getText().contains(Val[0])))
				{
					fail("The Delivery Preference title is not found / empty.");
				}
				else
				{
					pass("The Delivery Preference title is found.");
				}
			}
			else
			{
				fail("The Delivery Preference title is not found.");
			}
			
			WebElement deliveryOptionShipMethods = BNBasicCommonMethods.findElement(driver, obj, "deliveryOptionShipMethods");
			if(BNBasicCommonMethods.isElementPresent(deliveryOptionShipMethods))
			{
				pass("The Delivery Option Ship Methods is found.");
			}
			else
			{
				fail("The Delivery Option Ship Methods is not found.");
			}
			
			WebElement deliverySpeedTitle = BNBasicCommonMethods.findElement(driver, obj, "deliverySpeedTitle");
			if(BNBasicCommonMethods.isElementPresent(deliverySpeedTitle))
			{
				if((deliverySpeedTitle.getText().isEmpty())&&(!deliverySpeedTitle.getText().contains(Val[1])))
				{
					fail("The Delivery Speed title is not found / empty.");
				}
				else
				{
					pass("The Delivery Speed title is found.The displayed text is : " + deliverySpeedTitle.getText());
				}
			}
			else
			{
				fail("The Delivery Speed title is not found.");
			}
			
			List <WebElement> deliveryOptionMakeitasGift = driver.findElements(By.xpath("//*[@class='make-it-a-gift-select']"));
			if(BNBasicCommonMethods.isListElementPresent(deliveryOptionMakeitasGift))
			{
				pass("The Make it as Gift Option is displayed.");
			}
			else
			{
				fail("The Make it as Gift Option is not displayed.");
			}
			
			WebElement makeItAGiftTitle = BNBasicCommonMethods.findElement(driver, obj, "makeItAGiftTitle");
			if(BNBasicCommonMethods.isElementPresent(makeItAGiftTitle))
			{
				if((makeItAGiftTitle.getText().isEmpty())&&(!makeItAGiftTitle.getText().contains(Val[2])))
				{
					fail("The Make It A Gift title is not found / empty.");
				}
				else
				{
					pass("The Make It A Gift is found.The displayed text is : " + makeItAGiftTitle.getText());
				}
			}
			else
			{
				fail("The Delivery Speed title is not found.");
			}
				
			/*WebElement deliveryOptionProductEdit = BNBasicCommonMethods.findElement(driver, obj, "deliveryOptionProductEdit");
			if(BNBasicCommonMethods.isElementPresent(deliveryOptionProductEdit))
			{
				if((deliveryOptionProductEdit.getText().isEmpty())&&(!deliveryOptionProductEdit.getText().contains(Val[3])))
				{
					fail("The Edit title is not found / empty.");
				}
				else
				{
					pass("The Edit title is found.The displayed text is : " + deliveryOptionProductEdit.getText());
				}
			}
			else
			{
				fail("The Edit link is not displayed.");
			}*/
				
			WebElement summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
			BNBasicCommonMethods.scrolldown(summaryContainer, driver);
			WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
			if(BNBasicCommonMethods.isElementPresent(orderContinuebtn))
			{
				pass("The Order Continue button is displayed.");
			}
			else
			{
				fail("The Order Continue button is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong.Please Check." + e.getMessage());
		}
	}
	 
	 /* BNIA-1147 Verify that subtotal (number of items) and the total amount should be displayed in the Order Summary section.*/
	public void guestCheckoutDeliveryPageSummaryContainer()
	{
		ChildCreation("BNIA-1147 Verify that subtotal (number of items) and the total amount should be displayed in the Order Summary section.");
		try
		{
			String cellVal = BNBasicCommonMethods.getExcelVal("BRM660", sheet, 2);
			String[] Val = cellVal.split("\n");
			WebElement summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
			if(BNBasicCommonMethods.isElementPresent(summaryContainer))
			{
				List <WebElement> orderSummaryLists = driver.findElements(By.xpath("//*[@id='summaryContainer']//li"));
				pass("The Summary Container is displayed.");
				if(BNBasicCommonMethods.isListElementPresent(orderSummaryLists))
				{
					pass("The Summary Container list is displayed.");
					for(int i=1;i<=orderSummaryLists.size();i++)
					{
						WebElement label = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)["+i+"]//span)[1]"));
						//System.out.println(label.getText());
						WebElement details = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)["+i+"]//span)[2]"));
						//System.out.println(details.getText());
						if((label.getText().isEmpty())&&(!label.getText().contains(Val[i])))
						{
							fail("The " + label.getText() + " line is found and it is empty.");
						}
						else
						{
							pass("The " + label.getText() + " line is found and it is displayed.");
						}
						
						if(details.getText().isEmpty())
						{
							fail("The " + details.getText() + " line is found and it is empty.");
						}
						else
						{
							pass("The " + details.getText() + " line is found and it is displayed.");
						}
					}
				}
				else
				{
					fail("The Summary Container list is not displayed.");
				}
			}
			else
			{
				fail("The Summary Container is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong.Please Check." + e.getMessage());
		}
	}
	
	/* BNIA-1148 Verify that Estimated shipping and the amount is displayed below the subtotal in the Order Summary section.*/
	public void guestCheckoutDeliverySummaryContainerEstimatedShipping()
	{
		ChildCreation("BNIA-1148 Verify that Estimated shipping and the amount is displayed below the subtotal in the Order Summary section.");
		try
		{
			String shiplabel = BNBasicCommonMethods.getExcelVal("BRM661", sheet, 2);
			List <WebElement> orderSummaryLists = driver.findElements(By.xpath("//*[@id='summaryContainer']//li"));
			if(BNBasicCommonMethods.isListElementPresent(orderSummaryLists))
			{
				pass("The Summary List is displayed.");
				boolean labelFound = false;
				WebElement label; 
				for(int i = 1;i<=orderSummaryLists.size();i++)
				{
					label = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)["+i+"]//span)[1]"));
					String labelName = label.getText();
					if(labelName.contains(shiplabel))
					{
						labelFound=true;
						break;
					}
					else
					{
						labelFound=false;
						continue;
					}
				}
				
				if(labelFound==true)
				{
					pass("The expected label is found.");
				}
				else
				{
					fail("The expected label is not found.");
				}
			}
			else
			{
				fail("The Summary List is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong.Please Check." + e.getMessage());
		}
	}

	/* BNIA-1149 Verify that delivery preference section has two options: Send everything in as few packages as possible(required for free shipping), Send each item as it is available (at additional cost).*/
	/*BNIA-1138 Verify that on clicking details link, the "Edit delivery preferences" should be displayed as per the creative*/
	/*BNIA-1298 Checkout page : On clicking Freeshipping details, free shipping details popup must be opened*/
	public void guestCheckoutDeliveryPreferenceOptions()
	{
		boolean childflag = false;
		ChildCreation("BNIA-1149 Verify that delivery preference section has two options: Send everything in as few packages as possible(required for free shipping), Send each item as it is available (at additional cost).");
		try
		{
			String cellVal = BNBasicCommonMethods.getExcelVal("BRM661", sheet, 23);
			String[] delOptions = cellVal.split("\n");
			WebElement deliveryOptionContainer = BNBasicCommonMethods.findElement(driver, obj, "deliveryOptionContainer");
			if(BNBasicCommonMethods.isElementPresent(deliveryOptionContainer))
			{
				pass("The Delivery Option Container is displayed.");
				WebElement checkoutSteps = BNBasicCommonMethods.findElement(driver, obj, "checkoutSteps");
				BNBasicCommonMethods.scrollup(checkoutSteps, driver);
				List <WebElement> deliveryOptionDeliveryPreference = driver.findElements(By.xpath("//*[@id='deliveryOptionsContainer']//*[@class='delivery-preferece']//*[@class='radio-label-text']")); 
				List <WebElement> deliverOPtionDetailsLink = driver.findElements(By.xpath("//*[@data-modal-class = 'BN.Modal.Account.HelpShippingPreferences']"));
				if(BNBasicCommonMethods.isListElementPresent(deliveryOptionDeliveryPreference))
				{
					pass("The Delivery Preference is displayed.");
					for(int i = 0;i<deliveryOptionDeliveryPreference.size();i++)
					{
						String delPref = deliveryOptionDeliveryPreference.get(i).getText();
						String expectedText = delOptions[i];
						log.add("The Expected text was : " + expectedText);
						log.add("The Current text was : " + delPref);
						if(delPref.contains(expectedText))
						{
							
							pass("The expected delivery preference text is displayed.",log);
							
							
						}
						else
						{
							fail("The expected delivery preference text is not displayed.",log);
						}
						deliverOPtionDetailsLink.get(i).click();
						 Thread.sleep(500);
						BNBasicCommonMethods.waitforElement(wait, obj, "DetailsContainer");
						WebElement DetailsContainer = BNBasicCommonMethods.findElement(driver, obj, "DetailsContainer");
						   if(DetailsContainer.isDisplayed())
						   {
							   childflag = true;
							   WebElement CheckoutCloseIcon = BNBasicCommonMethods.findElement(driver, obj, "CheckoutCloseIcon");
							   CheckoutCloseIcon.click();
							   Thread.sleep(1500);
							   pass("Details Overlay is Displayed");
						   }
						   else
						   {
							   fail("Details Overlay is not displayed");
						   }
					}
				}
				else
				{
					fail("The Delivery Preference is not displayed.");
				}
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong.Please Check." + e.getMessage());
		}
		
		ChildCreation("BNIA-1138 Verify that on clicking details link, the Edit delivery preferences should be displayed as per the creative");
		if(childflag)
			 pass("Details Overlay is Displayed");
		else
			fail("Details Overlay is not displayed");
	
		ChildCreation("BNIA-1298 Checkout page : On clicking Freeshipping details, free shipping details popup must be opened");
		if(childflag)
			 pass("Details Overlay is Displayed");
		else
			fail("Details Overlay is not displayed");
		
		
	}
	
	/* BNIA-1150 Verify that "Send everything in as few packages as possible(required for free shipping)" option is selected by default inDelivery options section.*/
	public void guestCheckoutDeliveryPreferenceDefaultSelection()
	{
		ChildCreation("BNIA-1150 Verify that Send everything in as few packages as possible(required for free shipping) option is selected by default inDelivery options section.");
		try
		{
			List <WebElement> deliveryOptionDeliveryPreference = driver.findElements(By.xpath("//*[@id='deliveryOptionsContainer']//*[@class='delivery-preferece']//*[@class='radio-label-text']"));
			if(BNBasicCommonMethods.isListElementPresent(deliveryOptionDeliveryPreference))
			{
				pass("The Delivery Preference is displayed.");
				WebElement deliveryOptionDeliverySlow = BNBasicCommonMethods.findElement(driver, obj, "deliveryOptionDeliverySlow");
				if(BNBasicCommonMethods.isElementPresent(deliveryOptionDeliverySlow))
				{
					pass("The Delivery Option slow is displayed.");
					if(deliveryOptionDeliverySlow.isSelected())
					{
						pass("The default slow preference is selected by default.The Selected default option is : " + deliveryOptionDeliveryPreference.get(0).getText());
					}
					else
					{
						fail("The default slow preference is not selected.");
					}
				}
				else
				{
					fail("The Delivery Option slow is not displayed.");
				}
			}
			else
			{
				fail("The Delivery Preference is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong.Please Check." + e.getMessage());
		}
	}
	
	/* BNIA-1151 Verify that Choose delivery speed section has three options: Standard delivery Shipping:XX,XX, Express Delivery Shipping:XXXX, Expedited Delivery Shipping:XX..*/
	/*BNIA-1135 Guest Checkout- Verify that shipping options with delivery preferences should be displayed if the user completed the shipping information details in the checkout page.*/
	public void guestCheckoutDeliveryShippingMethods()
	{
		boolean childflag=false;
		ChildCreation("BNIA-1151 Verify that Choose delivery speed section has three options: Standard delivery Shipping:XXX,XX, Express Delivery Shipping:XXXX, Expedited Delivery Shipping:XXX..");
		try
		{
			String cellVal = BNBasicCommonMethods.getExcelVal("BRM664", sheet, 24);
			String[] shipMeth = cellVal.split("\n");
			List <WebElement> deliveryOptionShipMethodsLabel  = driver.findElements(By.xpath("//*[@id='shipMethods']//*[@class='radio-label-text']"));
			if(BNBasicCommonMethods.isListElementPresent(deliveryOptionShipMethodsLabel))
			{
				pass("The Shipping Methods label is displayed.");
				for(int i = 0; i<deliveryOptionShipMethodsLabel.size();i++)
				{
					String actlabel = deliveryOptionShipMethodsLabel.get(i).getText();
					String explabel = shipMeth[i];
					log.add("The Expected label was : " + shipMeth[i]);
					log.add("The Actual label was : " + actlabel);
					if(actlabel.contains(explabel))
					{
						childflag=true;
						pass("The Expected label is found.",log);
					}
					else
					{
						fail("The Expected label is not found.",log);
					}
				}
				List <WebElement> deliveryOptionShipMethodsArrivesBy = driver.findElements(By.xpath("//*[@id='shipMethods']//*[@class='radio-label-text']/b"));
				for(int i = 0; i<deliveryOptionShipMethodsArrivesBy.size();i++)
				{
					String actArriveBy = deliveryOptionShipMethodsLabel.get(i).getText();
					String expArriveBy = shipMeth[3];
					log.add("The Expected label was : " + shipMeth[3]);
					log.add("The Actual label was : " + actArriveBy);
					if(actArriveBy.contains(expArriveBy))
					{
						
						pass("The Expected Arrive By text is found.",log);
					}
					else
					{
						fail("The Expected Arrive By is not found.",log);
					}
				}
			}
			else
			{
				fail("The Shipping Methods label is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong.Please Check." + e.getMessage());
		}
		
		ChildCreation("BNIA-1135 Guest Checkout- Verify that shipping options with delivery preferences should be displayed if the user completed the shipping information details in the checkout page.");
		if(childflag)
			pass("The Expected label is found.");
		else
			fail("The Expected label is not found.");
	}
	
	/* BNIA-1152 Verify that "Standard delivery shipping:Arrives by (ValXX,XX)" is selected by default in choose delivery speed option in the guest checkout page.*/
	public void guestCheckoutDeliveryShippingMethodsDefaultSelection()
	{
		ChildCreation("BNIA-1152 Verify that Standard delivery shipping:Arrives by XXX,XX is selected by default in choose delivery speed option in the guest checkout page.");
		try
		{
			boolean chkd = false;
			int i;
			String cellVal = BNBasicCommonMethods.getExcelVal("BRM665", sheet, 24);
			List <WebElement> deliveryOptionDeliveryMethod = driver.findElements(By.xpath("//*[@id='shipMethods']//*[@class='delivery-method']"));
			if(BNBasicCommonMethods.isListElementPresent(deliveryOptionDeliveryMethod))
			{
				pass("The Delivery Methods is displayed.");
				//Thread.sleep(500);
				for(i = 0;i<deliveryOptionDeliveryMethod.size();i++)
				{
					boolean selected = deliveryOptionDeliveryMethod.get(i).isSelected();
					if(selected==true)
					{
						chkd = true;
						break;
					}
					else
					{
						chkd = false;
						continue;
					}
				}
				
				if(chkd==true)
				{
					List <WebElement> deliveryOptionShipMethodsLabel = driver.findElements(By.xpath("//*[@id='shipMethods']//*[@class='radio-label-text']"));
					String deloptionText = deliveryOptionShipMethodsLabel.get(i).getText();
					log.add("The Default selection expected label content was : " + cellVal);
					log.add("The Default selection actual label content is : " + deloptionText);
					if(deloptionText.contains(cellVal))
					{
						pass("The Default Selection of the delivery method is as expected.",log);
					}
					else
					{
						fail("The Default Selection of the delivery method is not as expected.",log);
					}
				}
				else
				{
					fail("No Delivery Method is selected.");
				}
			}
			else
			{
				fail("The Delivery Methods is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong.Please Check." + e.getMessage());
		}
	}

	/* BNIA-1153 Verify that product image, product name, author name, price, qty and make it as a gift are displayed in Items from Barnes & Noble section in the delivery tab.*/
	/*BNIA-1145 Verify that delivery section should be displayed as per the creative in the guest checkout page*/
	public void guestCheckoutCartItemDetails()
	{
		ChildCreation("BNIA-1153 Verify that product image, product name, author name, price, qty and make it as a gift are displayed in Items from Barnes & Noble section in the delivery tab.");
		boolean childflag = false;
		try
		{
			WebElement deliveryOptionCartItemContainer = BNBasicCommonMethods.findElement(driver, obj, "deliveryOptionCartItemContainer");
			if(BNBasicCommonMethods.isElementPresent(deliveryOptionCartItemContainer))
			{
				pass("The Cart Item Container is displayed.");
				BNBasicCommonMethods.scrolldown(deliveryOptionCartItemContainer, driver);
				List <WebElement> deliveryOptionCartItemsImage = driver.findElements(By.xpath("//*[@id='checkoutContainer']//*[@id='cartItems']//*[@class='product-img']//img"));
				for(int i = 0;i<deliveryOptionCartItemsImage.size();i++)
				{
					WebElement img = deliveryOptionCartItemsImage.get(i);
					int imgResp = BNBasicCommonMethods.imageBroken(img, log);
					if(imgResp==200)
					{
						childflag = true;
						pass("The Image is not broken.",log);
					}
					else
					{
						fail("The Image is broken.",log);
					}
					
					List <WebElement> deliveryOptionCartItemsProductTitle = driver.findElements(By.xpath("//*[@id='checkoutContainer']//*[@id='cartItems']//*[@class='product-desc']//*[@class='product-title']"));
					WebElement title = deliveryOptionCartItemsProductTitle.get(i);
					String prdtTitle = title.getText();
					if(prdtTitle.isEmpty())
					{
						fail("The Product title is displayed. But the displayed title is empty.");
					}
					else
					{
						pass("The Product title is displayed. The displayed title is : " + prdtTitle);
					}
					
					List <WebElement> deliveryOptionCartItemsProductAuthor = driver.findElements(By.xpath("//*[@id='checkoutContainer']//*[@id='cartItems']//*[@class='product-desc']//*[@class='contributors']"));
					WebElement author = deliveryOptionCartItemsProductAuthor.get(i);
					String prdtAuthor = author.getText();
					if(prdtAuthor.isEmpty())
					{
						fail("The Product author is displayed. But the displayed author is empty.");
					}
					else
					{
						pass("The Product Author is displayed. The displayed author name is : " + prdtAuthor);
					}
					List <WebElement> deliveryOptionCartItemsProductPrice = driver.findElements(By.xpath("//*[@id='checkoutContainer']//*[@id='cartItems']//*[@class='price']//*[@class='item-price']"));
					WebElement price = deliveryOptionCartItemsProductPrice.get(i);
					String prdtPrice = price.getText();
					if(prdtPrice.isEmpty())
					{
						fail("The Product price is displayed. But the displayed price is empty.");
					}
					else
					{
						pass("The Product price is displayed. The displayed price is : " + prdtPrice);
					}
					List <WebElement> deliveryOptionCartItemsProductQty = driver.findElements(By.xpath("//*[@id='checkoutContainer']//*[@id='cartItems']//*[@class='quantity']//*[@class='edit-cart-items']//*[@id='frmUpdateQty__']"));
					WebElement qty = deliveryOptionCartItemsProductQty.get(i);
					String prdtQuantity = qty.getText();
					if(prdtQuantity.isEmpty())
					{
						childflag = true;
						fail("The Product quantity is displayed. But the displayed quantity is empty.");
					}
					else
					{
						pass("The Product quantity is displayed. The displayed quantity is : " + prdtQuantity);
					}
					List<WebElement> deliveryOptionCartItemsGiftSection = driver.findElements(By.xpath("//*[@id='checkoutContainer']//*[@class='product-desc']//*[@class='make-it-a-gift-select']"));
					WebElement giftElement = deliveryOptionCartItemsGiftSection.get(i);
					if(BNBasicCommonMethods.isElementPresent(giftElement))
					{
						pass("The gift section is displayed.");
					}
					else
					{
						fail("The gift section is not displayed.");
					}
				}
			}
			else
			{
				fail("The Cart Item Container is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong.Please Check." + e.getMessage());
		}
		
		ChildCreation("BNIA-1145 Verify that delivery section should be displayed as per the creative in the guest checkout page");
		if(childflag)
			pass("Delivery Option page is Displayed");
		else
			fail("Delivery Option page is not Displayed");
		
		
		
	}
	
	/* BNIA-1154 Verify that Order total value should be updated properly as per the delivery options selected.*/
	public void guestCheckoutSummaryContainerPriceDetails()
	{
		ChildCreation("BNIA-1154 Verify that Order total value should be updated properly as per the delivery options selected.");
		try
		{
			WebElement summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
			
			if(BNBasicCommonMethods.isElementPresent(summaryContainer))
			{
				Float originalVal = null,currVal = null;
				pass("The Summary Container is displayed.");
				List<WebElement> deliveryOptionDeliverySelect = driver.findElements(By.xpath("//*[@class='delivery-preferece']//*[@class='styled-radio']"));
				if(BNBasicCommonMethods.isListElementPresent(deliveryOptionDeliverySelect))
				{
					pass("The Delivery preference list is displayed.");
					List<WebElement> deliveryOptionShippingSelect = driver.findElements(By.xpath("//*[@id='shipMethods']//*[@class='styled-radio']"));
					for(int i = 0; i<deliveryOptionShippingSelect.size();i++)
					{
						deliveryOptionShippingSelect = driver.findElements(By.xpath("//*[@id='shipMethods']//*[@class='styled-radio']"));
						WebElement delOption = deliveryOptionShippingSelect.get(i);
						WebElement scrollupmet = driver.findElement(By.xpath("//*[@id='deliveryOptionsContainer']"));
						BNBasicCommonMethods.scrollup(scrollupmet, driver);
					//	BNBasicCommonMethods.scrolldown(delOption, driver);
						delOption.click();
						Thread.sleep(500);
						BNBasicCommonMethods.waitforElement(wait, obj, "summaryContainer");
						summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
						BNBasicCommonMethods.scrolldown(summaryContainer, driver);
						WebElement deliveryOptionCartTotalPrice = BNBasicCommonMethods.findElement(driver, obj, "deliveryOptionCartTotalPrice");
						if(BNBasicCommonMethods.isElementPresent(deliveryOptionCartTotalPrice))
						{
							pass("The Total Price container is displayed.");
							String Price = deliveryOptionCartTotalPrice.getText().replace("$", "");
							currVal = Float.parseFloat(Price);
						}
						else
						{
							fail("The Total Price container is not displayed.");
						}
						
						if(currVal.equals(originalVal))
						{
							fail("The Value are same.");
						}
						else
						{
							pass("The Value are changed based on the delivery preference.The current Value in the Total Price is : " + currVal);
							originalVal = currVal;
						}
					}
				}
				else
				{
					fail("The Delivery preference list is not displayed.");
				}
			}
			else
			{
				fail("The Summary Container is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong.Please Check." + e.getMessage());
		}
	}
	
	/* BNIA-1155 Verify that tabs are highlighted till delivery and Shipping should be highlighted.*/
	public void guestCheckoutBreadcrumHighlight1()
	{
		ChildCreation("BNIA-1155 Verify that tabs are highlighted till delivery and Shipping should be highlighted.");
		try
		{
			String color = BNBasicCommonMethods.getExcelVal("BRM679", sheet, 3);
			List<WebElement> checkoutStepsLists = driver.findElements(By.xpath("//*[@id='checkoutSteps']//a"));
			if(BNBasicCommonMethods.isListElementPresent(checkoutStepsLists))
			{
				WebElement checkoutSteps = BNBasicCommonMethods.findElement(driver, obj, "checkoutSteps");
				BNBasicCommonMethods.scrollup(checkoutSteps, driver);
				String[] script = new String[2];
				String[] hexCode = new String[script.length];
				String[] breadcrumtitle = new String[script.length];
				log.add("The Checkout Steps are displayed.");
				script[0] = "return window.getComputedStyle(document.querySelector('#checkoutStepOne'),':before').getPropertyValue('border-bottom-color')";
				script[1] = "return window.getComputedStyle(document.querySelector('#checkoutStepTwo'),':before').getPropertyValue('border-bottom-color')";
				
				for(int j=0; j<script.length; j++)
				{
					WebElement ele = driver.findElement(By.xpath("(//*[@id='checkoutSteps']//li//a)["+(j+1)+"]"));
					String elecolor = ele.getCssValue("color");
					Color colorhxcnvt = Color.fromString(elecolor);
					breadcrumtitle[j] = colorhxcnvt.asHex();
					JavascriptExecutor js = (JavascriptExecutor)driver;
					String content = (String) js.executeScript(script[j]);
					//System.out.println(content);
					//String split = content.substring(0,16);
					Color colorhxcnvt1 = Color.fromString(content);
					hexCode[j] = colorhxcnvt1.asHex();
					if(color.equals(hexCode[j]))
					{
						log.add("The " + ele.getText() + " breadcrum expected color was " + color);
						log.add("The " + ele.getText() + " breadcrum actual color is " + hexCode[j]);
						pass("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
					}
					else
					{
						log.add("The " + ele.getText() + " breadcrum expected color was " + color);
						log.add("The " + ele.getText() + " breadcrum actual color is " + hexCode[j]);
						fail("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
					}
				}
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong.Please Check." + e.getMessage());
			System.out.println(e.getMessage());
		}
	}

	/* BNIA-1156 Verify that on selecting all the options and clicking on "Continue" button should proceed to the Payment page.*/
	public void guestCheckoutPaymentsPageNavigation()
	{
		ChildCreation("BNIA-1156 Verify that on selecting all the options and clicking on Continue button should proceed to the Payment page.");
		try 
		{
			WebElement summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
			BNBasicCommonMethods.scrolldown(summaryContainer, driver);
			WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn"); 
			orderContinuebtn.click();
			Thread.sleep(500);
			BNBasicCommonMethods.waitforElement(wait, obj, "reviewPagetitle");
			WebElement paymentsOption = BNBasicCommonMethods.findElement(driver, obj, "reviewPagetitle");
			wait.until(ExpectedConditions.visibilityOf(paymentsOption));
			
			if(BNBasicCommonMethods.isElementPresent(paymentsOption))
			{
				pass("The user is navigated to the payments page.");
			}
			else
			{
				fail("The user is not navigated to the payments page.");
			}
		} 
		catch (Exception e) 
		{
			exception(" There is something wrong. Please Check. " + e.getMessage());
		}
	}
	
	/* BNIA-1160 Verify that estimated tax and the amount is displayed below the Order Shipping.*/
	public void guestCheckoutDeliveryEstimatedTax()
	{
		ChildCreation("BNIA-1160 Verify that estimated tax and the amount is displayed below the Order Shipping.");
		try
		{
			String estimatedTaxlabel = sheet.getRow(0).getCell(19).getStringCellValue();
			WebElement summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
			if(BNBasicCommonMethods.isElementPresent(summaryContainer))
			{
				pass("The Summary Container is displayed.");
				List<WebElement> orderSummaryLists = driver.findElements(By.xpath("//*[@id='summaryContainer']//li"));
				if(BNBasicCommonMethods.isListElementPresent(orderSummaryLists))
				{
					log.add("The Summary Container List is found and it is displayed.");
					boolean fieldFound = false;
					WebElement label = null,details = null; 
					for(int k = 1; k<=orderSummaryLists.size();k++)
					{
						label = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)["+k+"]//span)[1]"));
						details = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)["+k+"]//span)[2]"));
						if(label.getText().contains(estimatedTaxlabel))
						{
							fieldFound=true;
							break;
						}
						else
						{
							fieldFound=false;
							continue;
						}
					}
					
					if(fieldFound==true)
					{
						pass("The " + label.getText() + " line is found and it is displayed.");
						if(details.getText().isEmpty())
						{
							fail("The Estimated tax is not displayed.It is empty.");
						}
						else
						{
							pass("The " + details.getText() + " line is found and it is displayed.");
						}
					}
					else
					{
						fail("The estimated tax is not displayed.");
					}
				}
				else
				{
					fail("The Summary Conatainer List is not found.Please Check.");
				
				}
			}
			else
			{
				fail("The Summary Container is not displayed.");
			}
		}
		catch (Exception e) 
		{
			exception(" There is something wrong. Please Check. " + e.getMessage());
		}
	}
	
	/* BNIA-1161 Verify that ORDER TOTAL and its amount is displayed above the "Continue" button.*/
	public void guestCheckoutDeliveryOrderTotal()
	{
		ChildCreation("BNIA-1161 Verify that ORDER TOTAL and its amount is displayed above the Continue button.");
		try
		{
			String OrderTotallabel = sheet.getRow(0).getCell(21).getStringCellValue();
			List<WebElement> orderSummaryLists = driver.findElements(By.xpath("//*[@id='summaryContainer']//li"));
			if(BNBasicCommonMethods.isListElementPresent(orderSummaryLists))
			{
				log.add("The Summary Container List is found and it is displayed.");
				boolean fieldFound = false;
				WebElement label = null,details = null; 
				for(int k = 1; k<=orderSummaryLists.size();k++)
				{
					label = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)["+k+"]//span)[1]"));
					//System.out.println(label.getText());
					details = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)["+k+"]//span)[2]"));
					//System.out.println(details.getText());
					if(label.getText().contains(OrderTotallabel))
					{
						fieldFound=true;
						break;
					}
					else
					{
						fieldFound=false;
						continue;
					}
				}
				
				if(fieldFound==true)
				{
					pass("The " + label.getText() + " line is found and it is displayed.");
					if(details.getText().isEmpty())
					{
						fail("The Order Total is not displayed.It is empty.");
					}
					else
					{
						pass("The " + details.getText() + " line is found and it is displayed.");
					}
				}
				else
				{
					fail("The Order Total is not displayed.");
				}
			}
			else
			{
				fail("The Summary Conatainer List is not found.Please Check.");
			
			}
		}
		catch (Exception e) 
		{
			exception(" There is something wrong. Please Check. " + e.getMessage());
		}
	}
	
	/* BNIA-1162 Verify whether user can able to select any one of the options in Items from Barnes & Noble section.*/
	public void guestCheckoutDeliveryPreferenceHighlight()
	{
		ChildCreation(" BNIA-1162 Verify whether user can able to select any one of the options in Items from Barnes & Noble section.");
		try
		{
			String cellVal = BNBasicCommonMethods.getExcelVal("BRM940", sheet, 3);
			BNBasicCommonMethods.waitforElement(wait, obj, "deliveryOptionDeliveryPreference");
			List<WebElement> deliveryOptionDeliveryPreference = driver.findElements(By.xpath("//*[@id='deliveryOptionsContainer']//*[@class='delivery-preferece']//*[@class='radio-label-text']"));
			if(BNBasicCommonMethods.isListElementPresent(deliveryOptionDeliveryPreference))
			{
				pass("The Delivery Preference is displayed.");
				List<WebElement> deliveryOptionDeliverySelect = driver.findElements(By.xpath("//*[@class='delivery-preferece']//*[@class='styled-radio']"));
				if(BNBasicCommonMethods.isListElementPresent(deliveryOptionDeliverySelect))
				{
					for(int i = 0; i<deliveryOptionDeliverySelect.size();i++)
					{
						WebElement radio = deliveryOptionDeliverySelect.get(i);
						radio.click();
						Thread.sleep(500);
						BNBasicCommonMethods.waitforElement(wait, obj, "deliveryOptionContainer");
						WebElement deliveryOptionContainer = BNBasicCommonMethods.findElement(driver, obj, "deliveryOptionContainer");
						wait.until(ExpectedConditions.visibilityOf(deliveryOptionContainer));
						deliveryOptionDeliveryPreference = driver.findElements(By.xpath("//*[@id='deliveryOptionsContainer']//*[@class='delivery-preferece']//*[@class='radio-label-text']"));
						WebElement label = deliveryOptionDeliveryPreference.get(i);
						//System.out.println(label.getText());
						String cssVal = label.getCssValue("color");
						Color colorhxcnvt = Color.fromString(cssVal);
						String hexCode = colorhxcnvt.asHex();
						log.add("The expected color was : " + cellVal);
						log.add("The Actual Color is : " + hexCode);
						if(hexCode.contains(cellVal))
						{
							pass("The delivery preference " + label.getText() + " is highlighted with the expected color.",log);
						}
						else
						{
							fail("The delivery preference " + label.getText() + " is not highlighted with the expected color.",log);
						}
					}
					deliveryOptionDeliverySelect = driver.findElements(By.xpath("//*[@class='delivery-preferece']//*[@class='styled-radio']"));
					deliveryOptionDeliverySelect.get(0).click();
				}
				else
				{
					fail("The Delivery Option slow is not displayed.");
				}
			}
			else
			{
				fail("The Delivery Preference is not displayed.");
			}
		}
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
			exception(" There is something wrong. Please Check. " + e.getMessage());
		}
	}
	
	/* BNIA-1163 Verify whether we can able to select any one of the options in Choose delivery speed section.*/
	public void guestCheckoutShippingMethodsHighlight()
	{
		ChildCreation("BNIA-1163 Verify whether we can able to select any one of the options in Choose delivery speed section.");
		try
		{
			String cellVal = BNBasicCommonMethods.getExcelVal("BRM941", sheet, 3);
			BNBasicCommonMethods.waitforElement(wait, obj, "deliveryOptionShipMethodsLabel");
			List<WebElement> deliveryOptionShipMethodsLabel = driver.findElements(By.xpath("//*[@id='shipMethods']//*[@class='radio-label-text']"));
			if(BNBasicCommonMethods.isListElementPresent(deliveryOptionShipMethodsLabel))
			{
				pass("The Shipping Method is displayed.");
				List<WebElement> deliveryOptionShippingSelect = driver.findElements(By.xpath("//*[@id='shipMethods']//*[@class='styled-radio']"));
				if(BNBasicCommonMethods.isListElementPresent(deliveryOptionShippingSelect))
				{
					List <WebElement> deliveryOptionDeliverySelect = driver.findElements(By.xpath("//*[@class='delivery-preferece']//*[@class='styled-radio']"));
					int sel = BNBasicCommonMethods.RandomNumGen(deliveryOptionDeliverySelect, log);
					WebElement radio = deliveryOptionShippingSelect.get(sel);
					radio.click();
					Thread.sleep(500);
					BNBasicCommonMethods.waitforElement(wait, obj, "deliveryOptionContainer");
					WebElement deliveryOptionContainer = BNBasicCommonMethods.findElement(driver, obj, "deliveryOptionContainer");
					wait.until(ExpectedConditions.visibilityOf(deliveryOptionContainer));
					List<WebElement> deliveryOptionContainerText = driver.findElements(By.xpath("//*[@id='shipMethods']//*[@class='radio-label-text']"));
					WebElement label = deliveryOptionContainerText.get(sel);
					String cssVal = label.getCssValue("color");
					Color colorhxcnvt = Color.fromString(cssVal);
					String hexCode = colorhxcnvt.asHex();
					log.add("The expected color was : " + cellVal);
					log.add("The Actual Color is : " + hexCode);
					if(hexCode.contains(cellVal))
					{
						pass("The Shipping Method is highlighted with the expected color.",log);
					}
					else
					{
						fail("The Shipping Method is not highlighted with the expected color.",log);
					}
				}
				else
				{
					fail("The Shipping Method selection option is not displayed.");
				}
			}
			else
			{
				fail("The delivery option shipping methods is not displayed.");
			}
		}
		catch (Exception e) 
		{
			exception(" There is something wrong. Please Check. " + e.getMessage());
		}
	}

	/******************************************************** Payment Information ********************************************************************/

	public void temppaymentNavigation()
	{
		try
		{
			//driver.get(BNConstants.guestcheckoutURL);
			BNBasicCommonMethods.waitforElement(wait, obj, "checkoutStepsLists");
			List <WebElement> checkoutStepsLists = driver.findElements(By.xpath("//*[@id='checkoutSteps']//a"));
			if(checkoutStepsLists.get(2).isDisplayed())
			{
				Actions act = new Actions(driver);
				for(int ctr=0 ; ctr<50 ;ctr++)
                act.sendKeys(Keys.CONTROL).sendKeys(Keys.ARROW_UP).build().perform();
			}
				//BNBasicCommonMethods.scrollup(checkoutStepsLists.get(1), driver);
			checkoutStepsLists.get(2).click();
			
		}
		catch(Exception e)
		{
			System.out.println("There is something wrong"+e.getMessage());
		}
	}	
	
	public void productaddtobag2()
		{
			
			
			try
			{
				sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
				String EANs = BNBasicCommonMethods.getExcelNumericVal("BRM623", sheet, 3);
				String EAN[] = EANs.split("\n");
			  for(int ctr=0 ; ctr<EAN.length ; ctr++)
	  		  {
				BNBasicCommonMethods.waitforElement(wait, obj, "BNLogo");
				WebElement BNLogo = BNBasicCommonMethods.findElement(driver, obj, "BNLogo");
				BNLogo.click();
				BNBasicCommonMethods.waitforElement(wait, obj, "SearchTile");
	  		   	WebElement SearchTile = BNBasicCommonMethods.findElement(driver, obj, "SearchTile");
	  		   	SearchTile.click();
	  		   	Thread.sleep(500);
	  		   	BNBasicCommonMethods.waitforElement(wait, obj, "SearchTxtBxnew");
	  		   	WebElement SearchTxtBxnew = BNBasicCommonMethods.findElement(driver, obj, "SearchTxtBxnew");
	  		   	SearchTxtBxnew.clear();
				SearchTxtBxnew.sendKeys(EAN[ctr]);
				SearchTxtBxnew.sendKeys(Keys.ENTER);
				Thread.sleep(500);
				String url = driver.getCurrentUrl();
				driver.get(url);
				BNBasicCommonMethods.waitforElement(wait, obj, "PDPAddToBag");
				WebElement PDPAddToBag = BNBasicCommonMethods.findElement(driver, obj, "PDPAddToBag");
				PDPAddToBag.click();
				int counter=1;
					do
					{
	  				try
	  				{
	  					/*BNBasicCommonMethods.waitforElement(wait, obj, "ATBBtn");
			  			WebElement ATBBtn = BNBasicCommonMethods.findElement(driver, obj, "ATBBtn");
			  			Thread.sleep(1000);
	  					ATBBtn.click();*/
	  					Thread.sleep(2000);
	  					BNBasicCommonMethods.waitforElement(wait, obj, "ATBSuccessAlertOverlay");
		  				//WebElement ATBSuccessAlertOverlay = BNBasicCommonMethods.findElement(driver, obj, "ATBSuccessAlertOverlay");
		  				counter=5;
	  				}
	  				
	  				catch(Exception e)
	  				{
	  					WebElement ATBErrorAlt = driver.findElement(By.xpath("//*[@id='skMob_ErrorsDiv_id']"));
	  					if(ATBErrorAlt.isDisplayed())
	  					{
	  						WebElement ErroBtn = driver.findElement(By.xpath("//*[@id='skMob_ErrorsOK_id']"));
	  						ErroBtn.click();
	  					}
	  					String str1 = driver.getCurrentUrl();
	  					driver.get(str1);
	  			    }
	  				counter++;
					} while(counter == 5 );
					
					WebElement ATBSuccessAlertOverlay = BNBasicCommonMethods.findElement(driver, obj, "ATBSuccessAlertOverlay");
	  				if(BNBasicCommonMethods.isElementPresent(ATBSuccessAlertOverlay))
	  				{
	  					BNBasicCommonMethods.waitforElement(wait, obj, "ContineShoppingBtn");
	  					WebElement ContineShoppingBtn = BNBasicCommonMethods.findElement(driver, obj, "ContineShoppingBtn");
	  					ContineShoppingBtn.click();
	  					
	  					//pass("Add to Bag Success alert overlay is displayed ");
	  				}
	  				
	  				else
	  				{
	  					fail("Add to bag Success alert overlay is not displayed",log);
	  				}	
				
			   }
			  
			}
			
			catch(Exception e)
			{
				exception("Exception , please check while adding the prodcut in the bag "+ e.getMessage());
			}
		}
		
	/*BNIA-781 Verify that when the "Shopping Bag/Add to Bag" icon is clicked in the header it should naviagte to "Shopping Bag" page.*/
	public boolean bagCount2() throws Exception
		{
		    ChildCreation("BNIA-781 Verify that when the Shopping Bag/Add to Bag icon is clicked in the header it should naviagte to Shopping Bag page.");
			boolean prdtadded = false;
			int count = 1;
			do
			{
				WebElement ShoppingBagCount = BNBasicCommonMethods.findElement(driver, obj, "ShoppingBagCount");
				int prdtval = Integer.parseInt(ShoppingBagCount.getText());
				if(prdtval>0)
				{
					prdtadded=true;
					WebElement ShoppingBagIcon = BNBasicCommonMethods.findElement(driver, obj, "ShoppingBagIcon");
					ShoppingBagIcon.click();
					Thread.sleep(500);
					pass("Shopping bag page is displayed and with Added products");
					BNBasicCommonMethods.waitforElement(wait, obj, "CheckoutSignIn");
					WebElement LoginCheckoutBtn = BNBasicCommonMethods.findElement(driver, obj, "LoginCheckoutBtn");
					if(LoginCheckoutBtn.isDisplayed())
					{
						pass("Shopping bag page is displayed with added Item");
					}
					else
					{
						fail("Shopping bag page is not displayed with added items");
					}
					wait.until(ExpectedConditions.visibilityOf(LoginCheckoutBtn));
					LoginCheckoutBtn.click();
					Thread.sleep(500);	
				//	Thread.sleep(3000);
					BNBasicCommonMethods.waitforElement(wait, obj, "signInIframe");
					WebElement signInIframe = BNBasicCommonMethods.findElement(driver, obj, "signInIframe");
					driver.switchTo().frame(signInIframe);
					//wait.until(ExpectedConditions.attributeToBe(miniCartLoadingGauge, "style", "display: none;"));
					//wait.until(ExpectedConditions.visibilityOf(secureCheckOutBtn));
					break;
				}
				else
				{
					count++;
					prdtadded=false;
					productaddtobag();
					continue;
				}
			}while(prdtadded==true||count<10);
			return prdtadded;
		}
		
	/* BNIA-971 Guest Checkout - Verify that on selecting the "Checkout" button the Sign In overlay should be enabled to Proceed for the Sign in/Check out as Guest checkout page.*/
	public void signInorCheckoutoverlay2()
		{
			ChildCreation(" BNIA-971 Guest Checkout - Verify that on selecting the Checkout button the Sign In overlay should be enabled to Proceed for the Sign in/Check out as Guest checkout page.");
			try
			{
				sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
				String pgcaption = BNBasicCommonMethods.getExcelVal("BRM623", sheet, 2);
				String[] title = pgcaption.split("\n");
				//addProduct(sheet, Child, false);
				//Thread.sleep(2000);
				//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(checkoutpageframe));
				BNBasicCommonMethods.waitforElement(wait, obj, "pgetitle");
				WebElement pgetitle = BNBasicCommonMethods.findElement(driver, obj, "pgetitle");
				wait.until(ExpectedConditions.visibilityOf(pgetitle));
				if(pgetitle.getText().equals(title[0]))
				{
					//System.out.println(pgetitle.getText());
					log.add("The Page title is : " + pgetitle.getText());
					pass("The user is navigated to the Checkout page and the caption matches with the expected one.",log);
				}
				else
				{
					log.add("The Page title is : " + pgetitle.getText());
					log.add("The expected page title is " + title[0]);
					fail("The user is navigated to the Checkout page and the caption does not matches with the expected one.",log);
				}
				
				WebElement checkoutAsGuestbtn = BNBasicCommonMethods.findElement(driver, obj, "checkoutAsGuestbtn");
				if(BNBasicCommonMethods.isElementPresent(checkoutAsGuestbtn))
				{
					pass("The Checkout as Guset button is visible.");
					if(checkoutAsGuestbtn.getText().equals(title[1]))
					{
						pass("The Checkout as Guest button caption matches with the expected one.");
					}
					else
					{
					fail("The Checkout as Guest button caption does not matches with the expected one.");
					}
				}
				else
				{
					fail("The Checkout as Guest Button is not visible / present.");
				}
			}
			catch (Exception e)
			{
				exception("There is something wrong.Please Check." + e.getMessage());
				System.out.println(e.getMessage());
			}
		}

	/* BNIA-972 Guest Checkout - Verify that Shipping section should be displayed as per the creative in the guest checkout page*/
	public void guestCheckoutoverlay2()
		{
			ChildCreation(" BNIA-972 Guest Checkout - Verify that Shipping section should be displayed as per the creative in the guest checkout page.");
			try
			{
				sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
				String pgcaption = BNBasicCommonMethods.getExcelVal("BRM627", sheet, 2);
				String expcolor = BNBasicCommonMethods.getExcelVal("BRM627", sheet, 3);
				String[] title = pgcaption.split("\n");
				WebElement checkoutAsGuestbtn = BNBasicCommonMethods.findElement(driver, obj, "checkoutAsGuestbtn");
				if(BNBasicCommonMethods.isElementPresent(checkoutAsGuestbtn))
				{
					checkoutAsGuestbtn.click();
				    Thread.sleep(500);
				    BNBasicCommonMethods.waitforElement(wait, obj, "guestCheckoutAddPageTitle");
					WebElement guestCheckoutAddPageTitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutAddPageTitle");
					wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
					//Thread.sleep(1000);
					
					if(BNBasicCommonMethods.isElementPresent(guestCheckoutAddPageTitle))
					{
						log.add("The User is naviagted to the Guest Checkout Page.");
						if(guestCheckoutAddPageTitle.getText().contains(title[0]))
						{
							log.add("The expected page title was : " + title[0]);
							log.add("The actual page title is : " + guestCheckoutAddPageTitle.getText());
							pass("The Guest Checkout Page title matches the expected content.",log);
						}
						else
						{
							System.out.println(guestCheckoutAddPageTitle.getText());
							log.add("The expected page title was : " + title[0]);
							log.add("The actual page title is : " + guestCheckoutAddPageTitle.getText());
							fail("The Guest Checkout Page title does not matches the expected content.",log);
						}
						
						WebElement checkoutSteps = BNBasicCommonMethods.findElement(driver, obj, "checkoutSteps");
						if(BNBasicCommonMethods.isElementPresent(checkoutSteps))
						{
							pass("The Checkout Steps container is displayed.");
							List <WebElement> checkoutStepsLists = driver.findElements(By.xpath("//*[@id='checkoutSteps']//a"));
							if(BNBasicCommonMethods.isListElementPresent(checkoutStepsLists))
							{
								for(int j = 1; j<=checkoutStepsLists.size();j++)
								{
									WebElement chksteps = driver.findElement(By.xpath("(//*[@id='checkoutSteps']//a)["+j+"]"));
									if(chksteps.getText().contains(title[j]))
									{
										log.add("The exected " + j + " step in the Checkout Steps is : " + title[j]);
										log.add("The actual " + j + " step in the Checkout Steps is : " + chksteps.getText());
										pass("The Checkout Steps " + j + " matches with the expected one.",log);
									}
									else
									{
										log.add("The exected " + j + " step in the Checkout Steps is : " + title[j]);
										log.add("The actual " + j + " step in the Checkout Steps is : " + chksteps.getText());
										fail("The Checkout Steps " + j + " does not matches with the expected one.",log);
									}
								}
							}
							else
							{
								fail("The Checkout Steps List is not displayed.Please Check.");
							}
						}
						else
						{
							fail("The Checkout Steps container is not displayed.");
						}
						
						WebElement countrySelection = BNBasicCommonMethods.findElement(driver, obj, "countrySelection");
						if(BNBasicCommonMethods.isElementPresent(countrySelection))
						{
							pass("The Country drop down list box is present and visible.");
						}
						else
						{
							fail("The Country drop down list box is not present / visible.");
						}
						
						WebElement fName = BNBasicCommonMethods.findElement(driver, obj, "fName");
						if(BNBasicCommonMethods.isElementPresent(fName))
						{
							pass("The First Name text field is present and visible.");
						}
						else
						{
							fail("The First Name text field is not present / visible.");
						}
						
						WebElement lName = BNBasicCommonMethods.findElement(driver, obj, "lName");
						if(BNBasicCommonMethods.isElementPresent(lName))
						{
							pass("The Last Name text field is present and visible.");
						}
						else
						{
							fail("The Last Name text field is not present / visible.");
						}
						
						WebElement stAddress = BNBasicCommonMethods.findElement(driver, obj, "stAddress");
						if(BNBasicCommonMethods.isElementPresent(stAddress))
						{
							pass("The Stree Address text field is present and visible.");
						}
						else
						{
							fail("The Stree Address text field is not present / visible.");
						}
						
						WebElement aptSuiteAddress = BNBasicCommonMethods.findElement(driver, obj, "aptSuiteAddress");
						BNBasicCommonMethods.scrolldown(aptSuiteAddress, driver);
						if(BNBasicCommonMethods.isElementPresent(aptSuiteAddress))
						{
							pass("The Apt/Suite text field is present and visible.");
						}
						else
						{
							fail("The Apt/Suite text field is not present / visible.");
						}
						
						WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
						if(BNBasicCommonMethods.isElementPresent(city))
						{
							pass("The City text field is present and visible.");
						}
						else
						{
							fail("The City text field is not present / visible.");
						}
						
						WebElement state = BNBasicCommonMethods.findElement(driver, obj, "state");
						if(BNBasicCommonMethods.isElementPresent(state))
						{
							pass("The State selection drop down is present and visible.");
						}
						else
						{
							fail("The State selection drop down is not present / visible.");
						}
						
						WebElement zipCode = BNBasicCommonMethods.findElement(driver, obj, "zipCode");
						if(BNBasicCommonMethods.isElementPresent(zipCode))
						{
							pass("The Zipcode text field is present and visible.");
						}
						else
						{
							fail("The Zipcode text field is not present / visible.");
						}
						
						WebElement contactNo = BNBasicCommonMethods.findElement(driver, obj, "contactNo");
						if(BNBasicCommonMethods.isElementPresent(contactNo))
						{
							pass("The Phone Number text field is present and visible.");
						}
						else
						{
							fail("The Phone Number text field is not present / visible.");
						}
						
						WebElement companyName = BNBasicCommonMethods.findElement(driver, obj, "companyName");
						if(BNBasicCommonMethods.isElementPresent(companyName))
						{
							pass("The Company Name text field is present and visible.");
						}
						else
						{
							fail("The Company Name text field is not present / visible.");
						}
						
						WebElement summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
						if(BNBasicCommonMethods.isElementPresent(summaryContainer))
						{
							pass("The Summary Container area is present and visible.");
						
						}
						else
						{
							fail("The Summary Container area is not present / visible.");
						
						}
						
						WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
						if(BNBasicCommonMethods.isElementPresent(orderContinuebtn))
						{
							pass("The Continue button is present and visible.");
							if(BNBasicCommonMethods.colorfindernew(orderContinuebtn).equals(expcolor))
							{
								log.add("The expected color of the Contintue button is : " + BNBasicCommonMethods.colorfindernew(orderContinuebtn).toString());
								log.add("The actual color of the Continue button is : " + expcolor);
								pass("The Continue button color matches with the expected One.",log);
							}
							else
							{
								log.add("The expected color of the Contintue button is : " + BNBasicCommonMethods.colorfindernew(orderContinuebtn).toString());
								log.add("The actual color of the Continue button is : " + expcolor);
								fail("The Continue button color does not matches with the expected One.",log);
							}
						}
						else
						{
							fail("The Summary Container area is not present / visible.");
						}
					}
					else
					{
						fail("The User is not naviagted to the Guest Checkout Page.");
					}
				}
				else
				{
					exception("The Checkout as Guest button is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				exception(" There is something wrong.Please Check." + e.getMessage());
			
			}
		}

	// Payments Page Field Check
	public void paymentPageFieldCheck()
	{
		try
		{
			pass("The Guest Credit Card Payment Tab is displayed.");
			WebElement guestCCContainerActiveTab = BNBasicCommonMethods.findElement(driver, obj, "guestCCContainerActiveTab");
			if(BNBasicCommonMethods.isElementPresent(guestCCContainerActiveTab))
			 {
				//String active = guestCCContainerActiveTab.getAttribute("style");
				if(guestCCContainerActiveTab.isDisplayed())
				{
					pass("The Credit Card Container is displayed by default.");
							
						WebElement ccLogoContainer = BNBasicCommonMethods.findElement(driver, obj, "ccLogoContainer");
						if(BNBasicCommonMethods.isElementPresent(ccLogoContainer))
						    {
								pass("The Credit Card Container logo is displayed.");
								List <WebElement> ccLogosList = driver.findElements(By.xpath("//*[contains(@class,'sk_cardTypes')]//*[@class='cc-radio-label']"));
								if(BNBasicCommonMethods.isListElementPresent(ccLogosList))
								{
									String ccname = "";
									for(WebElement logo : ccLogosList)
									{
										ccname = logo.getText();
										if(ccname.isEmpty())
										{
											fail("The Credit Card name is not dispalyed.");
										}
										else
										{
											log.add("The displayed credit card logo name is :" + ccname);
										}
									}
									pass("The Credit Card Logo is displayed.",log);
								}
								else
								{
									fail("The Credit Card Logo List is not displayed.");
								}
							}
							else
							{
								fail("The Credit Card Container logo is not displayed.");
							}
							
							WebElement ccNumber = BNBasicCommonMethods.findElement(driver, obj, "ccNumber");
							if(BNBasicCommonMethods.isElementPresent(ccNumber))
							{
								pass("The Credit Card Number is displayed.");
							}
							else
							{
								fail("The Credit Card Number is not displayed.");
							}
							
							WebElement ccName = BNBasicCommonMethods.findElement(driver, obj, "ccName");
							if(BNBasicCommonMethods.isElementPresent(ccName))
							{
								pass("The Credit Card Name is displayed.");
							}
							else
							{
								fail("The Credit Card Name is not displayed.");
							}
							
							WebElement ccMonth = BNBasicCommonMethods.findElement(driver, obj, "ccMonth");
							if(BNBasicCommonMethods.isElementPresent(ccMonth))
							{
								pass("The Credit Card Month drop down is displayed.");
							}
							else
							{
								fail("The Credit Card Month drop down is not displayed.");
							}
							
							WebElement ccYear = BNBasicCommonMethods.findElement(driver, obj, "ccYear");
							if(BNBasicCommonMethods.isElementPresent(ccYear))
							{
								pass("The Credit Card Year drop down field is displayed.");
							}
							else
							{
								fail("The Credit Card Year drop down field is not displayed.");
							}
							
							WebElement ccCsv = BNBasicCommonMethods.findElement(driver, obj, "ccCsv");
							if(BNBasicCommonMethods.isElementPresent(ccCsv))
							{
								pass("The Credit Card Security Field CSV is displayed.");
							}
							else
							{
								fail("The Credit Card Security Field CSV field is not displayed.");
							}
							
							 WebElement cciiconnew = BNBasicCommonMethods.findElement(driver, obj, "cciiconnew");
							if(BNBasicCommonMethods.isElementPresent(cciiconnew))
							{
								pass("The Tooltip icon is dispalyed.");
							}
							else
							{
								fail("The Tooltip icon is not dispalyed.");
							}
							
							WebElement guestSameAsShippingChkboxnew = BNBasicCommonMethods.findElement(driver, obj, "guestSameAsShippingChkboxnew");
							if(BNBasicCommonMethods.isElementPresent(guestSameAsShippingChkboxnew))
							{
								pass("The Same As Shipping Address checkbox is dispalyed.");
							}
							else
							{
								fail("The Same As Shipping Address checkbox is not dispalyed.");
							}
							WebElement guestEmailAddress = BNBasicCommonMethods.findElement(driver, obj, "guestEmailAddress");
							if(BNBasicCommonMethods.isElementPresent(guestEmailAddress))
							{
								pass("The Email Address Field is dispalyed.");
							}
							else
							{
								fail("The Email Address Field is not dispalyed.");
							}
						}
						else 
						{
							fail("The Credit Card Container is not displayed by default.");
						}
					}
					else
					{
						fail("The Credit Card Container is displayed.");
					}
		}
		catch(Exception e)
		{
			exception("There is something wrong . Please Check." + e.getMessage());
			System.out.println(e.getMessage());
		}
	}
	
	public void guestCheckoutPaymentPageNavigation()
	{
		ChildCreation(" BNIA-994 Guest Checkout - Verify that on entering all the input fields and clicking on Continue button should proceed to the Delivery page.");
		try
		{
			
			sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
			String pagetitle = BNBasicCommonMethods.getExcelVal("BRM657", sheet, 3);
			WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
			BNBasicCommonMethods.scrolldown(city, driver);
			WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
			orderContinuebtn.click();
			Thread.sleep(250);
			BNBasicCommonMethods.waitforElement(wait, obj, "gadrverficationtitle");
		//	WebElement addressVerificationTitle = BNBasicCommonMethods.findElement(driver, obj, "gadrverficationtitle");
			//wait.until(ExpectedConditions.visibilityOf(addressVerificationTitle));
			BNBasicCommonMethods.waitforElement(wait, obj, "addressVerificationPartialMatchesTitle");
			WebElement addressVerificationPartialMatchesTitle = BNBasicCommonMethods.findElement(driver, obj, "addressVerificationPartialMatchesTitle");
			if(BNBasicCommonMethods.isElementPresent(addressVerificationPartialMatchesTitle))
			{
				WebElement addressSuggestionFirstAddress = BNBasicCommonMethods.findElement(driver, obj, "addressSuggestionFirstAddress");
				BNBasicCommonMethods.scrolldown(addressSuggestionFirstAddress, driver);
				Random r = new Random();
				List <WebElement> checkoutAddressVerficationPossibleSuggestion = driver.findElements(By.xpath(" //*[@id='addrMatches']//li"));
				int sel = r.nextInt(checkoutAddressVerficationPossibleSuggestion.size());
				BNBasicCommonMethods.scrolldown(driver.findElement(By.xpath("(//*[@id='addrMatches']//li)["+(sel+1)+"]")), driver);
				WebElement usethibtn = driver.findElement(By.xpath("(//*[@id='addrMatches']//*[@class='btn-submit continue'])["+(sel+1)+"]"));
				usethibtn.click();
				//Thread.sleep(4000);
				BNBasicCommonMethods.waitforElement(wait, obj, "DeliveryOptionTitle");
				WebElement DeliveryOptionTitle = BNBasicCommonMethods.findElement(driver, obj, "DeliveryOptionTitle");
				wait.until(ExpectedConditions.visibilityOf(DeliveryOptionTitle));
				WebElement deliverycheckoutstep = driver.findElement(By.xpath("//*[@id='checkoutStepTwo']"));
				boolean steptwo = false;
				
				if(steptwo == deliverycheckoutstep.getAttribute("class").contains("Active"))
				{
					log.add("The User is navigated to the Delivery Page.");
					if(DeliveryOptionTitle.getText().equals(pagetitle))
					{
						log.add("The expected page title was : " + pagetitle);
						log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
						pass("The user is in the Delivery Page and the Page title matches the expected One.",log);
						orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
						orderContinuebtn.click();
					
						
					}
					else
					{
						log.add("The expected page title was : " + pagetitle);
						log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
						fail("The user is in the Delivery Page.But there is mismatch in the page title.",log);
						orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
						orderContinuebtn.click();
					}
				}
				else
				{
					fail("The user is not in the Delivery Page.Please Check.");
				}
			}
			else
			{
				WebElement addressVerificationMatchesTitle = BNBasicCommonMethods.findElement(driver, obj, "addressVerificationMatchesTitle");
				if(BNBasicCommonMethods.isElementPresent(addressVerificationMatchesTitle))
				{
					WebElement addressSuggestionFirstAddress = BNBasicCommonMethods.findElement(driver, obj, "addressSuggestionFirstAddress");
					BNBasicCommonMethods.scrolldown(addressSuggestionFirstAddress, driver);
					Random r = new Random();
					List <WebElement> checkoutAddressVerficationPossibleSuggestion = driver.findElements(By.xpath(" //*[@id='addrMatches']//li"));
					int sel = r.nextInt(checkoutAddressVerficationPossibleSuggestion.size());
					BNBasicCommonMethods.scrolldown(driver.findElement(By.xpath("(//*[@id='addrMatches']//li)["+(sel+1)+"]")), driver);
					WebElement usethibtn = driver.findElement(By.xpath("(//*[@id='addrMatches']//*[@class='btn-submit continue'])["+(sel+1)+"]"));
					usethibtn.click();
					//Thread.sleep(4000);
					WebElement DeliveryOptionTitle = BNBasicCommonMethods.findElement(driver, obj, "DeliveryOptionTitle");
					wait.until(ExpectedConditions.visibilityOf(DeliveryOptionTitle));
					WebElement deliverycheckoutstep = driver.findElement(By.xpath("//*[@id='checkoutStepTwo']"));
					boolean steptwo = false;
					
					if(steptwo == deliverycheckoutstep.getAttribute("class").contains("Active"))
					{
						log.add("The User is navigated to the Delivery Page.");
						if(DeliveryOptionTitle.getText().equals(pagetitle))
						{
							log.add("The expected page title was : " + pagetitle);
							log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
							pass("The user is in the Delivery Page and the Page title matches the expected One.",log);
						}
						else
						{
							log.add("The expected page title was : " + pagetitle);
							log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
							fail("The user is in the Delivery Page.But there is mismatch in the page title.",log);
						}
					}
					else
					{
						fail("The user is not in the Delivery Page.Please Check.");
					}
				}
				else
				{
					WebElement addressVerificationNoMatchesTitle = BNBasicCommonMethods.findElement(driver, obj, "addressVerificationNoMatchesTitle");
					if(BNBasicCommonMethods.isElementPresent(addressVerificationNoMatchesTitle))
					{
						WebElement nomatchesbtn = driver.findElement(By.xpath("//*[@id='asEntered']"));
						nomatchesbtn.click();
						Thread.sleep(4000);
						WebElement deliverycheckoutstep = driver.findElement(By.xpath("//*[@id='checkoutStepTwo']"));
						boolean steptwo = false;
						
						if(steptwo == deliverycheckoutstep.getAttribute("class").contains("Active"))
						{
							log.add("The User is navigated to the Delivery Page.");
							WebElement DeliveryOptionTitle = BNBasicCommonMethods.findElement(driver, obj, "DeliveryOptionTitle");
							if(DeliveryOptionTitle.getText().equals(pagetitle))
							{
								log.add("The expected page title was : " + pagetitle);
								log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
								pass("The user is in the Delivery Page and the Page title matches the expected One.",log);
							}
							else
							{
								log.add("The expected page title was : " + pagetitle);
								log.add("The Current Page title is : " + DeliveryOptionTitle.getText());
								fail("The user is in the Delivery Page.But there is mismatch in the page title.",log);
							}
						}
						else
						{
							fail("The user is not in the Delivery Page.Please Check.");
						}
					}
					else
					{
						fail("The user is not redirected to the Address Verification Page.");
					}
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			exception("There is something wrong. Please Check. " + e.getMessage());
		}
	}
	
	 /*BNIA-975 Guest Checkout - Verify that guest user can able to add the new shipping address details in the shipping section*/
	public void guestCheckoutEnterdata2()
		{
			ChildCreation(" BNIA-975 Guest Checkout - Verify that guest user can able to add the new shipping address details in the shipping section.");
			try
			{
				sheet = BNBasicCommonMethods.excelsetUp("Guest New Entry");
				String firstname = BNBasicCommonMethods.getExcelVal("BRM628", sheet, 6);
				String lastname = BNBasicCommonMethods.getExcelVal("BRM628", sheet, 7);
				String streetAdd = BNBasicCommonMethods.getExcelVal("BRM628", sheet, 8);
				String cty = BNBasicCommonMethods.getExcelVal("BRM628", sheet, 10);
				String zpcode = BNBasicCommonMethods.getExcelNumericVal("BRM628", sheet, 12);
				String cntnumber = BNBasicCommonMethods.getExcelNumericVal("BRM628", sheet, 13);
				WebElement guestCheckoutonetitle = BNBasicCommonMethods.findElement(driver, obj, "guestCheckoutonetitle");
				BNBasicCommonMethods.scrollup(guestCheckoutonetitle, driver);
				//Actions act = new Actions(driver);
				WebElement Countrycontainer = BNBasicCommonMethods.findElement(driver, obj, "countrycontiner");
				Countrycontainer.click();
				List<WebElement> countryLst = driver.findElements(By.xpath("//*[@id='country-option-list']//li"));
				//driver.findElement(By.xpath("//*[@id='state-option-35']")).click();
				//System.out.println(countryLst.get(0).getText());
				countryLst.get(0).click();
				/*act.moveToElement(countrySelection).build().perform();
				Select sel = new Select(countrySelection);*/
				/*sel.selectByValue("US");*/
				clearAll();
				WebElement fName = BNBasicCommonMethods.findElement(driver, obj, "fName");
				fName.sendKeys(firstname);
				WebElement lName = BNBasicCommonMethods.findElement(driver, obj, "lName");
				lName.sendKeys(lastname);
				WebElement stAddress = BNBasicCommonMethods.findElement(driver, obj, "stAddress");
				stAddress.sendKeys(streetAdd);
				WebElement companyName = BNBasicCommonMethods.findElement(driver, obj, "companyName");
				BNBasicCommonMethods.scrollup(companyName, driver);
				WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
				city.sendKeys(cty);
			//	WebElement state = BNBasicCommonMethods.findElement(driver, obj, "state");
				// Create instance of Javascript executor
				/*JavascriptExecutor je = (JavascriptExecutor) driver;
				//Identify the WebElement which will appear after scrolling down
				// now execute query which actually will scroll until that element is not appeared on page.
				je.executeScript("arguments[0].scrollIntoView(true);",state);
				*/
				WebElement Statecontainer = BNBasicCommonMethods.findElement(driver, obj, "Statecontainer");
				Statecontainer.click();
				JavascriptExecutor je = (JavascriptExecutor) driver;
				//Identify the WebElement which will appear after scrolling down
				// now execute query which actually will scroll until that element is not appeared on page.
				List<WebElement> stateLst = driver.findElements(By.xpath("//*[@id='state-option-list']//li"));
				je.executeScript("arguments[0].scrollIntoView(true);",stateLst.get(35));
				//driver.findElement(By.xpath("//*[@id='state-option-35']")).click();
				//System.out.println(stateLst.get(35).getText());
				stateLst.get(35).click();
				/*Select stsel = new Select(state);
				stsel.selectByIndex(35);*/
				WebElement zipCode = BNBasicCommonMethods.findElement(driver, obj, "zipCode");
				zipCode.sendKeys(zpcode);
				WebElement contactNo = BNBasicCommonMethods.findElement(driver, obj, "contactNo");
				contactNo.sendKeys(cntnumber);
				pass("Have Entered new shipping address details in the shipping section");
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				exception(" There is something wrong.Please Check." + e.getMessage());
				
			}
		}
	
	/* BNIA-1180 Guest Checkout - Verify that no tab should be selected by default in the payment page*/
	public void payInfoGuestPaymentDefaultSelection()
	{
		ChildCreation("  BNIA-1180 Guest Checkout - Verify that no tab should be selected by default in the payment page");
		try
		{
			  sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
			  BNBasicCommonMethods.waitforElement(wait, obj, "guestCCContainerActiveTab");
		    WebElement guestCCContainerActiveTab = BNBasicCommonMethods.findElement(driver, obj, "guestCCContainerActiveTab");
		   	if(BNBasicCommonMethods.isElementPresent(guestCCContainerActiveTab))
			{
		   		pass("The Credit Card Container is displayed by default.");
			}
			else
			{
				fail("The Credit Card Container is not displayed .");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong.Please Check." + e.getMessage());
			System.out.println(e.getMessage());
		}
	}
	
	/*BNIA-1178 Verify that payment page should be displayed as per the creative*/
	/* BNIA-1289 Guest Checkout - Verify that payment page should be displayed as per the creative in the guest checkout page*/
	/*BNIA-1290 Verify that payment page should be displayed as per the creative in the guest checkout page*/
	public void payInfoGuestPaymentPageElements() throws Exception
	{
		boolean childflag=false;
		ChildCreation("BNIA-1178 Verify that payment page should be displayed as per the creative");
		WebElement PaymentOption = BNBasicCommonMethods.findElement(driver, obj, "PaymentOption");
		try
		{
			  sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
			if(PaymentOption.isDisplayed())
			{
				childflag=true;
				pass("Payment page is displayed");
			}
			else
			{
				fail("Payment page is not displayed ");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong.Please Check." + e.getMessage());
			System.out.println(e.getMessage());
		}
		
		ChildCreation("BNIA-1289 Guest Checkout - Verify that payment page should be displayed as per the creative in the guest checkout page.");
		if(childflag)
			pass("Payment page is displayed");
		else
			fail("Payment page is not displayed ");
		
		ChildCreation("BNIA-1290 Verify that payment page should be displayed as per the creative in the guest checkout page");
		if(childflag)
			pass("Payment page is displayed");
		else
			fail("Payment page is not displayed ");
			
	}
	
	/* BNIA-1212 Verify that tabs are highlighted till Payment and Shipping & Delivery should be highlighted.*/
	public void payInfoCCBreadCrumHighlight()
	{
		ChildCreation("BNIA-1212 Verify that tabs are highlighted till Payment and Shipping & Delivery should be highlighted.");
		try
			{
				String color = BNBasicCommonMethods.getExcelVal("BRM830", sheet, 11);
				WebElement reviewPagetitle = BNBasicCommonMethods.findElement(driver, obj, "reviewPagetitle");
				wait.until(ExpectedConditions.visibilityOf(reviewPagetitle));
				List <WebElement> checkoutStepsLists = driver.findElements(By.xpath("//*[@id='checkoutSteps']//a"));
				if(BNBasicCommonMethods.isListElementPresent(checkoutStepsLists))
				{
					//WebElement checkoutSteps = BNBasicCommonMethods.findElement(driver, obj, "checkoutSteps");
					//BNCheckoutEditShipandBillAddress.BNBasicCommonMethods.scrollup(checkoutSteps, driver);
					String[] script = new String[3];
					String[] hexCode = new String[script.length];
					String[] breadcrumtitle = new String[script.length];
					log.add("The Checkout Steps are displayed.");
					script[0] = "return window.getComputedStyle(document.querySelector('#checkoutStepOne'),':before').getPropertyValue('border-bottom-color')";
					script[1] = "return window.getComputedStyle(document.querySelector('#checkoutStepTwo'),':before').getPropertyValue('border-bottom-color')";
					script[2] = "return window.getComputedStyle(document.querySelector('#checkoutStepThree'),':before').getPropertyValue('border-bottom-color')";
					for(int j=0; j<script.length; j++)
					{
						WebElement ele = driver.findElement(By.xpath("(//*[@id='checkoutSteps']//li//a)["+(j+1)+"]"));
						String elecolor = ele.getCssValue("color");
						Color colorhxcnvt = Color.fromString(elecolor);
						breadcrumtitle[j] = colorhxcnvt.asHex();
						JavascriptExecutor js = (JavascriptExecutor)driver;
						String content = (String) js.executeScript(script[j]);
						//System.out.println(content);
						//String split = content.substring(0,16);
						Color colorhxcnvt1 = Color.fromString(content);
						hexCode[j] = colorhxcnvt1.asHex();
						if(color.equals(hexCode[j]))
						{
							log.add("The " + ele.getText() + " breadcrum expected color was " + color);
							log.add("The " + ele.getText() + " breadcrum actual color is " + hexCode[j]);
							pass("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
						}
						else
						{
							log.add("The " + ele.getText() + " breadcrum expected color was " + color);
							log.add("The " + ele.getText() + " breadcrum actual color is " + hexCode[j]);
							fail("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
						}
						
						if(color.equals(breadcrumtitle[j]))
						{
							log.add("The " + ele.getText() + " breadcrum expected color was " + color);
							log.add("The " + ele.getText() + " breadcrum actual color is " + breadcrumtitle[j]);
							pass("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
						}
						else
						{
							log.add("The " + ele.getText() + " breadcrum expected color was " + color);
							log.add("The " + ele.getText() + " breadcrum actual color is " + breadcrumtitle[j]);
							fail("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
						}
					}
				}
			}
			catch (Exception e)
			{
				exception(" There is something wrong.Please Check." + e.getMessage());
				System.out.println(e.getMessage());
			}		
	}
		
	/* BNIA-1181 Guest Checkout - Verify that all the mandatory input fields related to credit card should be dispalyed.*/
	/*BNIA-1177 Verify that Card type, card no, expiration date, security code fields should be displayed in the add a card page as per the creative*/
	public void payInfoGuestCCElements() throws Exception
	{
		boolean childflag=false;
		ChildCreation(" BNIA-1181 Guest Checkout - Verify that all the mandatory input fields related to credit card should be dispalyed. ");
		  sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
		WebElement PaymentOption = BNBasicCommonMethods.findElement(driver, obj, "PaymentOption");
		if(PaymentOption.isDisplayed())
		{
			childflag = true;
			paymentPageFieldCheck();
			pass("All the fields are displyed correctly ");
		}
		else
		{
			childflag=false;
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
		
		ChildCreation("BNIA-1177 Verify that Card type, card no, expiration date, security code fields should be displayed in the add a card page as per the creative");
		if(childflag)         
			pass("All the fields are displyed correctly ");
		else
			Skip("The user is not navigated to the Guest User Payments Page.");
	}
	
	/* BNIA-1182 Guest Checkout - Verify that while selecting the "Continue" button without entering any values in the credit card input fields,the alert message should be displayed.*/
	public void payInfoGuestCCEmptyValidation()
	{
		ChildCreation("BNIA-1182 Guest Checkout - Verify that while selecting the Continue button without entering any values in the credit card input fields,the alert message should be displayed.");
		boolean shipPage = true;
		if(shipPage==true)
		{
			try
			{
				  sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
				WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
				String alert = BNBasicCommonMethods.getExcelNumericVal("BRM721", sheet, 10);
				if(BNBasicCommonMethods.isElementPresent(orderContinuebtn))
				{
					BNBasicCommonMethods.scrolldown(orderContinuebtn, driver);
					orderContinuebtn.click();
					BNBasicCommonMethods.waitforElement(wait, obj, "guestError");
					WebElement guestError = BNBasicCommonMethods.findElement(driver, obj, "guestError");
					wait.until(ExpectedConditions.visibilityOf(guestError));
					Thread.sleep(500);
					log.add("The Expected alert was : " + alert);
					if(BNBasicCommonMethods.isElementPresent(guestError))
					{	
						pass("The alert is raised to indicate user.");
						String actAlert = guestError.getText();
						log.add("The Actual Alert was : " + actAlert);
						if(alert.contains(actAlert))
						{
							pass("The alert match the expected content.",log);
						}
						else
						{
							fail("The alert does not match the expected content.",log);
						}
					}
					else
					{
						fail("No Validation is not raised to indicate user.");
					}
				}
			}
			catch (Exception e)
			{
				exception("There is something wrong.Please Check." + e.getMessage());
				System.out.println(e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BNIA-1183 Guest Checkout - Verify that user is able to enter only number in the Card Number field and it should accept maximum of 16 digits.*/
	public void payInfoGuestCCNumberLengthValidation()
	{
		ChildCreation("BNIA-1183 Guest Checkout - Verify that user is able to enter only number in the Card Number field and it should accept maximum of 16 digits.");
			try
			{
				 sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
				String cellVal = BNBasicCommonMethods.getExcelNumericVal("BRM727", sheet, 38);
				String[] Val = cellVal.split("\n");
				WebElement ccNumber = BNBasicCommonMethods.findElement(driver, obj, "ccNumber");
				if(BNBasicCommonMethods.isElementPresent(ccNumber))
				{
					pass("The Credit Card Number field is displayed.");
					for(int i = 0; i<Val.length;i++)
					{
						ccNumber.click();
						ccNumber.clear();
						ccNumber.sendKeys(Val[i]);
						//int size = ccNumber.getAttribute("value").length();
						if((ccNumber.getAttribute("value").length()>0) && (ccNumber.getAttribute("value").length()<=16))
						{
							log.add("The value from the excel sheet for the credit card number is " + Val[i] + " and its length is " + Val[i].length());
							log.add("The Current value in the credit card number feild is " + ccNumber.getAttribute("value") + " and its length is " + ccNumber.getAttribute("value").length());
							pass("The Credit number field accepts characters less than 16.",log);
						}
						else
						{
							log.add("The value from the excel sheet for the credit card number is " + Val[i] + " and its length is " + Val[i].length());
							log.add("The Current value in the credit card number feild is " + ccNumber.getAttribute("value") + " and its length is " + ccNumber.getAttribute("value").length());
							fail("The Credit number field accepts characters more than 16.",log);
						}
					}
				}
				else
				{
					fail("The Credit Card Number field is not displayed.");
				}
			}
			catch (Exception e)
			{
				exception("There is something wrong.Please Check." + e.getMessage());
			}
	}
	
	/* BNIA-1184 Guest Checkout - Verify that name on the card field should accept alphabets with a limit of 60 characters.*/
	public void payInfoGuestCCNameLengthValidation()
	{
		ChildCreation("BNIA-1184 Guest Checkout - Verify that name on the card field should accept alphabets with a limit of 60 characters.");
		try
			{
			   sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
				String cellVal = BNBasicCommonMethods.getExcelNumericVal("BRM730", sheet, 41);
				String[] Val = cellVal.split("\n");
				WebElement guestError = BNBasicCommonMethods.findElement(driver, obj, "guestError");
				BNBasicCommonMethods.scrollup(guestError, driver);
				WebElement ccName = BNBasicCommonMethods.findElement(driver, obj, "ccName");
				if(BNBasicCommonMethods.isElementPresent(ccName))
				{
					pass("The Credit Card Number field is displayed.");
					for(int i = 0; i<Val.length;i++)
					{
						ccName.click();
						ccName.clear();
						ccName.sendKeys(Val[i]);
						//int size = ccName.getAttribute("value").length();
						if(ccName.getAttribute("value").length()<=60)
						{
							log.add("The entered value in the Credit Card Name from the excel file is " + Val[i] + " and its length is " + Val[i].length() + " . The current value in the First Name field is " + ccName.getAttribute("value").toString()+ " and its length is " + ccName.getAttribute("value").length());
							pass("The Credit Card Name field accepts character less than 60 only.",log);
						}
						else
						{
							log.add("The entered value in the Credit Card Name from the excel file is " + Val[i] + " and its length is " + Val[i].length() + " . The current value in the First Name field is " + ccName.getAttribute("value").toString() + " and its length is " + ccName.getAttribute("value").length());
							fail("The First Name field accepts character more than 60 .",log);
						}
					}
				}
				else
				{
					fail("The Credit Card Number field is not displayed.");
				}
			}
			catch (Exception e)
			{
				exception("There is something wrong.Please Check." + e.getMessage());
			}
	}
	
	/* BNIA-1185 Guest Checkout - Verify that Default text "month" and "year" should be displayed in expiration date and user should be able to select month and year from the drop down.*/
	public void payInfoGuestCCMonthYearDefaultText()
	{
		ChildCreation(" BNIA-1185 Guest Checkout - Verify that Default text month and year should be displayed in expiration date and user should be able to select month and year from the drop down.");
		try
		{
			WebElement ccAddDefaultMonthTxt = BNBasicCommonMethods.findElement(driver, obj, "ccAddDefaultMonthTxt");
			if(BNBasicCommonMethods.isElementPresent(ccAddDefaultMonthTxt))
			{
				log.add("The default text displayed in the Month drop down field is " + ccAddDefaultMonthTxt.getText());
				pass("The default text is present for the Month Field.",log);
			}
			else
			{
				fail("The default text value is not present in the Month drop down.");
			}
			WebElement ccAddDefaultYearTxt = BNBasicCommonMethods.findElement(driver, obj, "ccAddDefaultYearTxt");
			if(BNBasicCommonMethods.isElementPresent(ccAddDefaultYearTxt))
			{
				log.add("The default text displayed in the Year drop down field is " + ccAddDefaultYearTxt.getText());
				pass("The default text is present for the Year Field.",log);
			}
			else
			{
				fail("The default text value is not present in the Year drop down.");
			}
			
			WebElement MonthDropdown  = driver.findElement(By.xpath("//*[@id='ccMonth-replacement']"));
			MonthDropdown.click();
			List <WebElement> Monthlis  = driver.findElements(By.xpath("//*[@id='ccMonth-option-list']//*[contains(@id,'ccMonth')]//a"));
			//YearDropdown.click();
			
			/*Select mnt = new Select(ccMonth);
			Select yr = new Select(ccYear);*/
			Random r = new Random();
			//int msel = r.nextInt(mnt.getOptions().size());
			int mlow = 1;
			int mhigh = Monthlis.size();
			int msel = 1;
			if(mhigh>1)
			{
				msel = r.nextInt(mhigh - mlow) + mlow;
			}
			
			//mnt.selectByIndex(msel);
			if(Monthlis.get(msel).getText().isEmpty())
			{
				System.out.println(Monthlis.get(msel).getText().isEmpty());
				fail("The Month is not selected. Please Check");
			}
			else
			{
				//System.out.println("The selected month is : " +Monthlis.get(msel).getText());
				log.add("The selected month is : " + Monthlis.get(msel).getText());
				//log.add("The selected month is : " + mnt.getFirstSelectedOption().getText());
				pass("The Month is selected.",log);
			}
			MonthDropdown.click();
			WebElement YearDropdown  = driver.findElement(By.xpath("//*[@id='ccYear-replacement']"));
			YearDropdown.click();
			List <WebElement> Yearlis  = driver.findElements(By.xpath("//*[@id='ccYear-option-list']//*[contains(@id,'ccYear')]//a"));
			int ylow = 1;
			int yhigh = Yearlis.size();
			int ysel = 1;
			if(yhigh>1)
			{
				ysel = r.nextInt(yhigh - ylow) + ylow;
			}
			//int ysel = r.nextInt(yr.getOptions().size());
		//	yr.selectByIndex(ysel);
			if(Yearlis.get(ysel).getText().isEmpty())
			{
				fail("The Year is not selected. Please Check");
			}
			else
			{
				log.add("The selected year is : " + Yearlis.get(ysel).getText());
				//log.add("The selected year is : " + yr.getFirstSelectedOption().getText());
				pass("The Year is selected.",log);
				YearDropdown.click();
			}
			
		}
		catch(Exception e)
		{
			exception(" There is something wrong. Please Check. " + e.getMessage());
			
		}
		
	}
	
	/* BNIA-1186 Guest Checkout - Verify that on selecting the month, the list of months must be displayed in the drop down..*/
	public void payInfoGuestCCardMonthList()
	{
		ChildCreation(" BNIA-1186 Guest Checkout - Verify that on selecting the month, the list of months must be displayed in the drop down..");
		try
		{
			WebElement ccMonth = BNBasicCommonMethods.findElement(driver, obj, "ccMonth");
			if(BNBasicCommonMethods.isElementPresent(ccMonth))
			{
				log.add("The Month field is displayed.");
				/*Actions act = new Actions(driver);*/
				WebElement MonthDropdown  = driver.findElement(By.xpath("//*[@id='ccMonth-replacement']"));
				MonthDropdown.click();
				List <WebElement> Monthlis  = driver.findElements(By.xpath("//*[@id='ccMonth-option-list']//*[contains(@id,'ccMonth')]//a"));
				/*act.moveToElement(ccMonth).click();
				act.build().perform();
				Select mnthsel = new Select(ccMonth);
				int size = mnthsel.getOptions().size();*/
				if(Monthlis.size() >= 13)
				{
					for(int i = 1; i < Monthlis.size(); i++)
					{
						//System.out.println(Monthlis.get(i).getText());
						log.add("The Month list includes " + Monthlis.get(i).getText());
					}
					pass("The Month Size is : " + Monthlis.size(),log);
					MonthDropdown.click();
				}
				else
				{
					fail("The Month size is less than 12. Please Check");
				}
			}
			else
			{
				fail("The Month field is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			exception(" There is something wrong. Please Check. " + e.getMessage());
		}
	}
	
	/* BNIA-1187 Guest Checkout - Verify that on selecting the year option then the list of years from the current year should be displayed.*/
	public void payInfoGuestCCardYearList()
	{
		ChildCreation(" BNIA-1187 Guest Checkout - Verify that on selecting the year option then the list of years from the current year should be displayed.");
		try
		{
			WebElement ccYear = BNBasicCommonMethods.findElement(driver, obj, "ccYear");
			if(BNBasicCommonMethods.isElementPresent(ccYear))
			{
				log.add("The Year field is displayed.");
				WebElement YearDropdown  = driver.findElement(By.xpath("//*[@id='ccYear-replacement']"));
				YearDropdown.click();
				/*Select yrsel = new Select(ccYear);
				Actions act = new Actions(driver);
				act.moveToElement(ccYear).click();
				act.build().perform();*/
				List <WebElement> Yearlis  = driver.findElements(By.xpath("//*[@id='ccYear-option-list']//*[contains(@id,'ccYear')]//a"));
				if(Yearlis.size()>0)
				{
					pass("The Year drop down is clicked and it is not empty");
					int year = Calendar.getInstance().get(Calendar.YEAR);
					String yr = Integer.toString(year);
					if(yr.equals(Yearlis.get(1).getText()))
					{
						log.add("The current year is " + yr);
						log.add("The first value in the year drop down is " + Yearlis.get(1).getText());
						/*//act.sendKeys(Keys.TAB).build().perform();
						act.click().build().perform();*/
						pass("The Current year and the drop down first value matches",log);
						YearDropdown.click();
					}
					else
					{
						log.add("The current year is " + yr);
						log.add("The first value in the year drop down is " + Yearlis.get(1).getText());
					/*	act.click().build().perform();*/
						fail("The Current year and the drop down first value down not matches",log);
					}
				}
				else
				{
					fail("The Month size is less than 12. Please Check");
				}
			}
			else
			{
				fail("The Month field is not displayed.");
			}
		}
		catch(Exception e)
		{
			exception(" There is something wrong. Please Check. " + e.getMessage());
		}
	}
	
	/* BNIA-1188 Guest Checkout - Verify that security code field accepts numbers with a limit of 4 characters and alert message should be displayed.*/
	public void payInfoCCCsvLengthValidation()
	{
		ChildCreation(" BNIA-1188 Guest Checkout - Verify that security code field accepts numbers with a limit of 4 characters and alert message should be displayed.");
		try
			{
			  sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
				int brmkey = sheet.getLastRowNum();
				for(int i = 0; i <= brmkey; i++)
				{
					String tcid = "BRM740";
					String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
					if(cellCont.equals(tcid))
					{
						DataFormatter formatter = new DataFormatter(); //creating formatter using the default locale
						HSSFCell cell = sheet.getRow(i).getCell(42);
						String csvNumber = formatter.formatCellValue(cell).toString();
						try
						{
							WebElement ccNumber = BNBasicCommonMethods.findElement(driver, obj, "ccNumber");
							BNBasicCommonMethods.scrolldown(ccNumber,driver);
							WebElement ccCsv = BNBasicCommonMethods.findElement(driver, obj, "ccCsv");
							ccCsv.clear();
							ccCsv.sendKeys(csvNumber);
							if((ccCsv.getAttribute("value").length()>0) && (ccCsv.getAttribute("value").length()<=4))
							{
								log.add("The entered value in the CSV field from the excel file is " + csvNumber + " and its length is " + csvNumber.length() + " . The current value in the CSV field is " + ccCsv.getAttribute("value").toString()+ " and its length is " + ccCsv.getAttribute("value").length());
								pass("The Credit Card CSV field accepts character less than 4 only.",log);
							}
							else
							{
								log.add("The entered value in the CSV field from the excel file is " + csvNumber + " and its length is " + csvNumber.length() + " . The current value in the CSV field is " + ccCsv.getAttribute("value").toString() + " and its length is " + ccCsv.getAttribute("value").length());
								fail("The First Name field accepts character more than 4 .",log);
							}
						}
						catch(Exception e)
						{
							exception(" There is something wrong. Please Check. " + e.getMessage());
						}
					}
				}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BNIA-1189 Guest Checkout - Verify that clicking on "i" icon should display a information overlay and again clicking on "I" icon should close the information overlay.*/
	public void payInfoCCiicon()
	{
		ChildCreation(" BNIA-1189 Guest Checkout - Verify that clicking on i icon should display a information overlay and again clicking on I icon should close the information overlay.");
		try
			{
			WebElement cciiconnew = BNBasicCommonMethods.findElement(driver, obj, "cciiconnew");
				if(BNBasicCommonMethods.isElementPresent(cciiconnew))
				{
					pass("The i icon is present in the Add Credit Card page and it is visible.");
					cciiconnew.click();
					Thread.sleep(500);
					//BNBasicCommonMethods.waitforElement(wait, obj, "cciicontooltip");
					WebElement cciicontooltip = BNBasicCommonMethods.findElement(driver, obj, "cciicontooltipnew");
					if(BNBasicCommonMethods.isElementPresent(cciicontooltip))
					{
						if(cciicontooltip.getAttribute("style").contains("block"))
						{
							pass("The tooltip i icon is clicked and it is visible for the user.");
							//Thread.sleep(500);
							/*WebElement popmask = driver.findElement(By.xpath("//*[@class='cvvpopup_mask']"));
							((JavascriptExecutor) driver).executeScript("arguments[0].click();", popmask);
							Thread.sleep(2000);*/
							BNBasicCommonMethods.waitforElement(wait, obj, "cciicontooltipclose");
							WebElement cciicontooltipclose = BNBasicCommonMethods.findElement(driver, obj, "cciicontooltipclose");
							cciicontooltipclose.click();
							if(cciicontooltip.getAttribute("style").contains("block"))
							{
								fail("The Container is not Closed.");
							}
							else
							{
								pass("The tooltip Container is Closed.");
							}
						}
						else
						{
							fail("The tooltip i icon is clicked and it is not visible for the user.");
							WebElement ccAddCancelBtn = BNBasicCommonMethods.findElement(driver, obj, "ccAddCancelBtn");
							BNBasicCommonMethods.scrolldown(ccAddCancelBtn, driver);
							//Thread.sleep(1000);
						}
					}
					else
					{
						fail("The tooltip is not visible for the user.");
					}
				}
				else
				{
					fail("The i icon is not present in the Add Credit Card page.");
				}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BNIA-1190 Guest Checkout - Verify that "Same as Shipping address" should be selected by defauly.*/
	/*BNIA-1210 Verify that on selecting "Same as Shipping address" option, Same as Shipping address option should be highlighted.*/
	public void payInfoCCShipsameasdefault()
	{
		boolean childflag = false;
		ChildCreation(" BNIA-1190 Guest Checkout - Verify that Same as Shipping address should be selected by default.");
		try
			{
			   sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
				String cellVal = BNBasicCommonMethods.getExcelVal("BRM742", sheet, 10);
				WebElement ccNumber = BNBasicCommonMethods.findElement(driver, obj, "ccNumber");
				BNBasicCommonMethods.scrollup(ccNumber, driver);
				WebElement guestSameAsShippingChkboxContainer = BNBasicCommonMethods.findElement(driver, obj, "guestSameAsShippingChkboxContainer");
				if(BNBasicCommonMethods.isElementPresent(guestSameAsShippingChkboxContainer))
				{
					pass("The Same as Shipping Address Container is displayed.");
					WebElement guestSameAsShippingChkboxLabel = BNBasicCommonMethods.findElement(driver, obj, "guestSameAsShippingChkboxLabel");
					if(BNBasicCommonMethods.isElementPresent(guestSameAsShippingChkboxLabel))
					{
						pass("The Shipping Address label is displayed.");
						String label = guestSameAsShippingChkboxLabel.getText();
						log.add("The Expected label was : " + cellVal);
						log.add("The Actual label is : " + label);
						if(cellVal.contains(label))
						{
							childflag = true;
							pass("The Check box label match the expected content.",log);
						}
						else
						{
							fail("The Check box label does not match the expected content.",log);
						}
					}
					else
					{
						fail("The Checkbox label is not displayed.");
					}
				}
				else
					{
					WebElement guestSameAsShippingChkboxContainer1 = BNBasicCommonMethods.findElement(driver, obj, "guestSameAsShippingChkboxContainer1");
					if(BNBasicCommonMethods.isElementPresent(guestSameAsShippingChkboxContainer1))
					
				    {
					pass("The Same as Shipping Address Container is displayed.");
					WebElement guestSameAsShippingChkboxLabel1 = BNBasicCommonMethods.findElement(driver, obj, "guestSameAsShippingChkboxLabel1");
					if(BNBasicCommonMethods.isElementPresent(guestSameAsShippingChkboxLabel1))
					{
						pass("The Shipping Address label is displayed.");
						String label = guestSameAsShippingChkboxLabel1.getText();
						log.add("The Expected label was : " + cellVal);
						log.add("The Actual label is : " + label);
						if(cellVal.contains(label))
						{
							pass("The Check box label match the expected content.",log);
						}
						else
						{
							fail("The Check box label does not match the expected content.",log);
						}
					}
					else
					{
						fail("The Checkbox label is not displayed.");
					}
				}
				else
				{
					fail("The Checkbox container is not dispalyed.");
				}
				WebElement guestSameAsShippingChkboxnew = BNBasicCommonMethods.findElement(driver, obj, "guestSameAsShippingChkboxnew");
				boolean chkBoxchkd = guestSameAsShippingChkboxnew.isSelected();
				if(chkBoxchkd==true)
				{
					pass("The Checkbox is selected by default.");
				}
				else
				{
					fail("The Checkbox is not selected by default.");
				}
			}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
		ChildCreation("BNIA-1210 Verify that on selecting Same as Shipping address option, Same as Shipping address option should be highlighted");
      if(childflag)
      	pass("The option is higlighted by selecting  the Tick box");
      else
      	fail("The option is not higlighted by selecting  the Tick box");
	}
	
	/* BNIA-1191 Guest Checkout - Verify that if a user disables "Same as shipping address" then shipping address overlay should be displayed with all the input fields.*/
	public void payInfoCCShipsAddressOverlay()
	{
		ChildCreation(" BNIA-1191 Guest Checkout - Verify that if a user disables Same as shipping address then shipping address overlay should be displayed with all the input fields.");
		try
			{
			  sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
				WebElement guestSameAsShippingChkboxContainer = BNBasicCommonMethods.findElement(driver, obj, "guestSameAsShippingChkboxContainer");
				if(BNBasicCommonMethods.isElementPresent(guestSameAsShippingChkboxContainer))
				{
					pass("The Same as Shipping Address Container is displayed.");
					WebElement AreaChecked = BNBasicCommonMethods.findElement(driver, obj, "AreaChecked");
					boolean chkBoxchkd   = false;
					chkBoxchkd  = AreaChecked.getAttribute("value").contains("true");
					//boolean chkBoxchkd = Checked.isSelected();
					if(chkBoxchkd==true)
					{
						pass("The Checkbox is selected by default.");
						WebElement guestSameAsShippingChkbox1 = BNBasicCommonMethods.findElement(driver, obj, "guestSameAsShippingChkbox1");
						guestSameAsShippingChkbox1.click();
						BNBasicCommonMethods.waitforElement(wait, obj, "guestSameAsShippingContainer");
						WebElement guestSameAsShippingContainer = BNBasicCommonMethods.findElement(driver, obj, "guestSameAsShippingContainer");
						wait.until(ExpectedConditions.attributeContains(guestSameAsShippingContainer, "style", "block;"));
						//Thread.sleep(1000);
						if(BNBasicCommonMethods.isElementPresent(guestSameAsShippingContainer))
						{
							pass("The Shipping Container is displayed.");
						}
						else
						{
							fail("The Shipping Container is not displayed.");
						}
					}
					else
					{
						fail("The Checkbox is not selected by default.");
					}
				}

				
				else 
					{
					WebElement guestSameAsShippingChkboxContainer1 = BNBasicCommonMethods.findElement(driver, obj, "guestSameAsShippingChkboxContainer1");
					if(BNBasicCommonMethods.isElementPresent(guestSameAsShippingChkboxContainer1))
					
				    {
					pass("The Same as Shipping Address Container is displayed.");
					WebElement guestSameAsShippingChkboxnew = BNBasicCommonMethods.findElement(driver, obj, "AreaChecked");
					boolean chkBoxchkd = guestSameAsShippingChkboxnew.isSelected();
					if(chkBoxchkd==true)
					{
						pass("The Checkbox is selected by default.");
						guestSameAsShippingChkboxnew.click();
						BNBasicCommonMethods.waitforElement(wait, obj, "guestSameAsShippingContainer");
						WebElement guestSameAsShippingContainer = BNBasicCommonMethods.findElement(driver, obj, "AreaChecked");
						wait.until(ExpectedConditions.attributeContains(guestSameAsShippingContainer, "style", "block;"));
						//Thread.sleep(1000);
						if(BNBasicCommonMethods.isElementPresent(guestSameAsShippingContainer))
						{
							pass("The Shipping Container is displayed.");
						}
						else
						{
							fail("The Shipping Container is not displayed.");
						}
					}
					else
					{
						fail("The Checkbox is not selected by default.");
					}
				}
				else
				{
					fail("The Checkbox container is not dispalyed.");
				}
				}
			}
			catch(Exception e)
			{
				System.out.println(e.getMessage());
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BNIA-1192 Guest Checkout - Verify that user is able to enter a email address in email address field*/
	public void payInfoCCShipsAddressEmailId() throws Exception	
	{
		ChildCreation(" BNIA-1192 Guest Checkout - Verify that user is able to enter a email address in email address field");
		try
			{
			  sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
				String cellVal = BNBasicCommonMethods.getExcelVal("BRM745", sheet, 45);
				WebElement guestEmailAddress = BNBasicCommonMethods.findElement(driver, obj, "guestEmailAddress");
				if(BNBasicCommonMethods.isElementPresent(guestEmailAddress))
				{
					pass("The Email Address field is displayed.");
					guestEmailAddress.click();
					guestEmailAddress.clear();
					guestEmailAddress.sendKeys(cellVal);
					log.add("The Entered Value was : " + cellVal);
					String currEmail = guestEmailAddress.getAttribute("value");
					//log.add("The Current Value was : " + currEmail);
					if(cellVal.contains(currEmail))
					{
						pass("The entered email address and the current email address matches.",log);
					}
					else
					{
						fail("The entered email address and the current email address does not match.",log);
					}
				}
				else
				{
					fail("The Email Address field is not displayed.");
				}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
			WebElement guestEmailAddress = BNBasicCommonMethods.findElement(driver, obj, "guestEmailAddress");
			guestEmailAddress.clear();
	}
	
	/* BNIA-1193 Guest Checkout - Verify that without entering the email address, then the alert must be displayed.*/
	/*BNIA-1195 Guest Checkout - Verify that on entering invalid email address in email address field, then the alert must be displayed*/
	public void payInfoCCShipsAddressInvalidEmailIdValidation()
	{
		ChildCreation("BNIA-1193 Guest Checkout - Verify that without entering the email address, then the alert must be displayed.");
	    boolean childflag = false;
		try
			{
			  sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
				String cellVal = BNBasicCommonMethods.getExcelVal("BRM747", sheet, 45);
				String alert = BNBasicCommonMethods.getExcelVal("BRM747", sheet, 10);
				WebElement guestEmailAddress = BNBasicCommonMethods.findElement(driver, obj, "guestEmailAddress");
				if(BNBasicCommonMethods.isElementPresent(guestEmailAddress))
				{
					pass("The Email Address field is displayed.");
					guestEmailAddress.click();
					guestEmailAddress.clear();
					guestEmailAddress.sendKeys(cellVal);
					log.add("The Entered Value was : " + cellVal);
					String currEmail = guestEmailAddress.getAttribute("value");
					if(currEmail.equals(cellVal))
					{
						pass("The entered email address and the current email address matches.",log);
					}
					else
					{
						fail("The entered email address and the current email address does not match.",log);
					}
					
					WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
					BNBasicCommonMethods.scrolldown(orderContinuebtn, driver);
					orderContinuebtn.click();
					BNBasicCommonMethods.waitforElement(wait, obj, "guestError");
					WebElement guestError = BNBasicCommonMethods.findElement(driver, obj, "CCErrorALert");
					wait.until(ExpectedConditions.visibilityOf(guestError));
					BNBasicCommonMethods.scrollup(guestError, driver);
					//Thread.sleep(1000);
					log.add("The Expected alert was : " + alert);
					String actAlert = guestError.getText();
					log.add("The Actual Alert was : " + actAlert);
					if(actAlert.contains(alert))
					{
						childflag = true;
						pass("The alert matches the expected content.",log);
					}
					else
					{
						fail("The alert does not matches the expected content.",log);
					}
					
					BNBasicCommonMethods.scrolldown(guestEmailAddress, driver);
					//Thread.sleep(1000);
					guestEmailAddress.clear();
				}
				else
				{
					fail("The Email Address field is not displayed.");
				}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
		
		   ChildCreation("BNIA-1195 Guest Checkout - Verify that on entering invalid email address in email address field, then the alert must be displayed.");
		   if(childflag)
			   pass("Without entering Email address alert message is displayed");
		   else
			   fail("Without entering Email address alert message is not displayed");
	}
	
	/* BNIA-1196 Verify that payment section should display: B&N Gift cards, Coupon code and Book Fair ID text boxes with "Apply" button */
	public void payInfoCCCouponConatainer()
	{
		ChildCreation("BNIA-1196 Verify that payment section should display: B&N Gift cards, Coupon code and Book Fair ID text boxes with Apply button .");
		try
			{
			  sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
				String cellVal = BNBasicCommonMethods.getExcelNumericVal("BRM754", sheet, 12);
				String[] Val = cellVal.split("\n");
				WebElement couponContainer = BNBasicCommonMethods.findElement(driver, obj, "couponContainer");
				if(BNBasicCommonMethods.isElementPresent(couponContainer))
				{
					BNBasicCommonMethods.scrolldown(couponContainer, driver);
					pass("The Coupon Container is found.");
					WebElement couponContainerTitle = BNBasicCommonMethods.findElement(driver, obj, "couponContainerTitle");
					String title = couponContainerTitle.getText();
					log.add("The Expected title was : " + Val[0]);
					log.add("The Actual title is : " + title);
					if(title.equals(Val[0]))
					{
						pass("The title matches the expected content .",log);
					}
					else
					{
						fail("There is mismatch in the Title.",log);
					}
				}
				else
				{
					fail("The Coupon Container is not found.");
				}
				
				List <WebElement> loyaltyContainerList = driver.findElements(By.xpath("//*[@id='couponContainer']//*[@class='loyalty-options-container']"));
				if(BNBasicCommonMethods.isListElementPresent(loyaltyContainerList))
				{
					pass("The Loyalty Container list is displayed.");
					
					WebElement gftCardTit = driver.findElement(By.xpath("(//*[@id='couponContainer']//*[@class='loyalty-options-container'])[1]//h3"));
					if(gftCardTit.getText().isEmpty())
					{
						fail("The gift catd title is not displayd/empty.");
					}
					else
					{
						pass("The Gift Card Title is displayed.");
						log.add("The expected title was : " + Val[1]);
						String title = gftCardTit.getText();
						log.add("The actual title is : " + gftCardTit.getText());
						/*System.out.println(title);
						System.out.println(Val[1]);*/
						if(title.contains(Val[1]))
						{
							pass("The expected title and the actual title matches.");
						}
						else
						{
							fail("The expected title and the actual title does not matches.");
						}
					}
					
					WebElement couponContainerCouponCodeTitle = BNBasicCommonMethods.findElement(driver, obj, "couponContainerCouponCodeTitle");
					if(BNBasicCommonMethods.isElementPresent(couponContainerCouponCodeTitle))
					{
						log.add("The expected title was : " + Val[2]);
						String title = couponContainerCouponCodeTitle.getText();
						log.add("The actual title is : " + title);
						/*System.out.println(title);
						System.out.println(Val[1]);*/
						if(title.contains(Val[2]))
						{
							pass("The expected title and the actual title matches.");
						}
						else
						{
							fail("The expected title and the actual title does not matches.");
						}
					}
					else
					{
						fail("The Coupon Code text is not dispalyed.");
					}
					
					WebElement couponContainerBookFairTitle = BNBasicCommonMethods.findElement(driver, obj, "couponContainerBookFairTitle");
					if(BNBasicCommonMethods.isElementPresent(couponContainerBookFairTitle))
					{
						log.add("The expected title was : " + Val[3]);
						String title = couponContainerBookFairTitle.getText();
						log.add("The actual title is : " + title);
						/*System.out.println(title);
						System.out.println(Val[1]);*/
						if(title.contains(Val[3]))
						{
							pass("The expected title and the actual title matches.");
						}
						else
						{
							fail("The expected title and the actual title does not matches.");
						}
					}
					else
					{
						fail("The Coupon Code text is not dispalyed.");
					}
				}
				else
				{
					fail("The Loyalty Container list is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BNIA-1197 Verify that "Apply" button should be highlighted in Green color.*/
	public void payInfoCCGiftCardConatainer()
	{
		ChildCreation(" BNIA-1197 Verify that Apply button should be highlighted in Green color. ");
		try
		{
			  sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
			String cellVal= BNBasicCommonMethods.getExcelNumericVal("BRM755", sheet, 12);
			String expColor = BNBasicCommonMethods.getExcelNumericVal("BRM755", sheet, 11);
			String[] Val = cellVal.split("\n");
			List <WebElement> loyaltyContainerList = driver.findElements(By.xpath("//*[@id='couponContainer']//*[@class='loyalty-options-container']"));
			if(BNBasicCommonMethods.isListElementPresent(loyaltyContainerList))
			{
				pass("The Loyalty Container list is displayed.");
				WebElement gftCardImg = driver.findElement(By.xpath("(//*[@id='couponContainer']//*[@class='loyalty-options-container'])[1]//img"));
				int resp = BNBasicCommonMethods.imageBroken(gftCardImg, log);
				if(resp == 200)
				{
					pass("The Gift Card Image is displayed and not broken.",log);
				}
				else
				{
					fail("The Gift Card Image is not displayed and it is broken.",log);
				}
				
				WebElement gftCardTit = driver.findElement(By.xpath("(//*[@id='couponContainer']//*[@class='loyalty-options-container'])[1]//h3"));
				if(gftCardTit.getText().isEmpty())
				{
					fail("The gift catd title is not displayd/empty.");
				}
				else
				{
					pass("The Gift Card Title is displayed.");
					log.add("The expected title was : " + Val[1]);
					String title = gftCardTit.getText();
					log.add("The actual title is : " + gftCardTit.getText());
					/*System.out.println(title);
					System.out.println(Val[1]);*/
					if(title.contains(Val[1]))
					{
						pass("The expected title and the actual title matches.");
					}
					else
					{
						fail("The expected title and the actual title does not matches.");
					}
				}
				
					WebElement couponCreditCard = BNBasicCommonMethods.findElement(driver, obj, "couponCreditCard");
					if(BNBasicCommonMethods.isElementPresent(couponCreditCard))
					{
						pass("The Credit Card Number field in the Coupon Container is displayed.");
					}
					else
					{
						fail("The Credit Card Number field in the Coupon Container is not displayed.");
					}
					
					WebElement couponCreditCardPin = BNBasicCommonMethods.findElement(driver, obj, "couponCreditCardPin");
					if(BNBasicCommonMethods.isElementPresent(couponCreditCardPin))
					{
						pass("The Credit Card Number Pin field in the Coupon Container is displayed.");
					}
					else
					{
						fail("The Credit Card Number Pin field in the Coupon Container is not displayed.");
					}
					
					WebElement couponCreditCardSubmit = BNBasicCommonMethods.findElement(driver, obj, "couponCreditCardSubmit");
					if(BNBasicCommonMethods.isElementPresent(couponCreditCardSubmit))
					{
						pass("The Apply button in Credit Card Section of the Coupon Container is displayed.");
						log.add("The Expected content was : " + Val[2]);
						String caption = couponCreditCardSubmit.getAttribute("value");
						log.add("The actual val is : " + caption);
						if(caption.equals(Val[2]))
						{
							pass("The Submit button caption matches.",log);
						}
						else
						{
							fail("The Submit button caption does not matches.");
						}
						
						String csVal = couponCreditCardSubmit.getCssValue("background").substring(0,16);;
						Color colorhxcnvt = Color.fromString(csVal);
						String hexCode = colorhxcnvt.asHex();
						log.add("The expected color of the button is : " + expColor);
						log.add("The actual color of the button is : " + hexCode);
						if(hexCode.equals(expColor))
						{
							pass("The Apply button color matches the expected one.",log);
						}
						else
						{
							fail("The Apply button color does not matches the expected one.",log);
						}
					}
					else
					{
						fail("The Apply button in Credit Card Section of the Coupon Container is not displayed.");
					}
			}
			else
			{
				fail("The Loyalty Container list is not displayed.");
			}
		}
		catch (Exception e)
		{
			exception("There is something wrong.Please Check." + e.getMessage());
			System.out.println(e.getMessage());
		}
	}
	
	/* BNIA-1199 Verify that if a user enter a invalid card number and pin and clicking on apply, coupon should not applied and alert must me displayed.*/
	public void payInfoCCGiftCardInvalidValidation()
	{
		ChildCreation("BNIA-1199 Verify that if a user enter a invalid card number and pin and clicking on apply, coupon should not applied and alert must me displayed.");
		try
			{
				String expError = BNBasicCommonMethods.getExcelVal("BRM757", sheet, 10);
				String creditCardNumber = BNBasicCommonMethods.getExcelNumericVal("BRM757", sheet, 38);
				String creditCardPin = BNBasicCommonMethods.getExcelNumericVal("BRM757", sheet, 42);
				WebElement couponCreditCard = BNBasicCommonMethods.findElement(driver, obj, "couponCreditCard");
				if(BNBasicCommonMethods.isElementPresent(couponCreditCard))
				{
					log.add("The Credit Card field is displayed.");
					couponCreditCard.click();
					WebElement couponCreditCardTextBox = BNBasicCommonMethods.findElement(driver, obj, "couponCreditCardTextBox");
					couponCreditCardTextBox.clear();
					couponCreditCardTextBox.sendKeys(creditCardNumber);
					WebElement couponCreditCardPin = BNBasicCommonMethods.findElement(driver, obj, "couponCreditCardPin");
					couponCreditCardPin.click();
					WebElement couponCreditCardPinTextBox = BNBasicCommonMethods.findElement(driver, obj, "couponCreditCardPinTextBox");
					couponCreditCardPinTextBox.clear();
					couponCreditCardPinTextBox.sendKeys(creditCardPin);
					WebElement couponCreditCardSubmit = BNBasicCommonMethods.findElement(driver, obj, "couponCreditCardSubmit");
					couponCreditCardSubmit.click();
					BNBasicCommonMethods.waitforElement(wait, obj, "couponCreditCardError");
					WebElement couponCreditCardError = BNBasicCommonMethods.findElement(driver, obj, "couponCreditCardError");
					wait.until(ExpectedConditions.attributeContains(couponCreditCardError, "style", "block;"));
					//wait.until(ExpectedConditions.visibilityOf(couponCreditCardError)).getAttribute("style").contains("display: block;");
					//Thread.sleep(1000);
					log.add("The Expected Error is : " + expError);
					String err1 = couponCreditCardError.getText();
					log.add("The actual error is :  " + err1);
					if(expError.contains(err1))
					{
						pass("The actual alert match the expected alert.",log);
					}
					else
					{
						fail("The actual alert does not match the expected alert.",log);
					}
				}
				else
				{
					fail("The Credit Card field is not displayed.");
				}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BNIA-1200 Verify that Card number should accept maximum of 19 characters and pin number should accept 4 characters and it should be masked by round dots. Default text should be displayed for both the fields.*/
	public void payInfoCCGiftCardLengthValidationandDefaultText()
	{
		ChildCreation("BNIA-1200 Verify that Card number should accept maximum of 19 characters and pin number should accept 4 characters and it should be masked by round dots. Default text should be displayed for both the fields.");
		try
			{
				String cellVal = BNBasicCommonMethods.getExcelNumericVal("BRM758", sheet, 38);
				String[] Val = cellVal.split("\n");
				String cellVal1 = BNBasicCommonMethods.getExcelNumericVal("BRM758", sheet, 42);
				String[] Val1 = cellVal1.split("\n");
				String label = BNBasicCommonMethods.getExcelVal("BRM758", sheet, 12);
				String[] labelVal = label.split("\n");
				for(int i = 0; i<Val.length;i++)
				{
					WebElement couponCreditCard = BNBasicCommonMethods.findElement(driver, obj, "couponCreditCard");
					if(BNBasicCommonMethods.isElementPresent(couponCreditCard))
					{
						log.add("The Credit Card field is displayed.");
						couponCreditCard.click();
						WebElement couponCreditCardTextBox = BNBasicCommonMethods.findElement(driver, obj, "couponCreditCardTextBox");
						couponCreditCardTextBox.clear();
						couponCreditCardTextBox.sendKeys(Val[i]);
						WebElement couponCreditCardPin = BNBasicCommonMethods.findElement(driver, obj, "couponCreditCardPin");
						couponCreditCardPin.click();
						WebElement couponCreditCardPinTextBox = BNBasicCommonMethods.findElement(driver, obj, "couponCreditCardPinTextBox");
						couponCreditCardPinTextBox.clear();
						couponCreditCardPinTextBox.sendKeys(Val1[i]);
						//Thread.sleep(1000);
						log.add("The Expected length in the credit card field is  : " + Val[i]);
						int curcclen =  couponCreditCardTextBox.getAttribute("value").length();
						log.add("The actual length is :  " + curcclen);
						if(curcclen<=19)
						{
							pass("The Card Number length is as expected.",log);
						}
						else
						{
							fail("The Card Number length is not as expected.",log);
						}
						
						log.add("The Expected length in the pin  field is  : " + Val[i]);
						int curpinlen =  couponCreditCardPinTextBox.getAttribute("value").length();
						log.add("The actual length is :  " + curpinlen);
						if(curpinlen<=4)
						{
							pass("The Card PIN length is as expected.",log);
						}
						else
						{
							fail("The Card PIN length is not as expected.",log);
						}
					}
					else
					{
						fail("The Credit Card field is not displayed.");
					}
				}
				
				WebElement couponCreditCardLabel = BNBasicCommonMethods.findElement(driver, obj, "couponCreditCardLabel");
				if(BNBasicCommonMethods.isElementPresent(couponCreditCardLabel))
				{
					pass("The Gift Card Default Text is displayed.");
					log.add("The expected title was : " + labelVal[0]);
					String title = couponCreditCardLabel.getText();
					log.add("The actual title is : " + title);
					/*System.out.println(title);
					System.out.println(Val[1]);*/
					if(title.contains(labelVal[0]))
					{
						pass("The expected title and the actual title matches.");
					}
					else
					{
						fail("The expected title and the actual title does not matches.");
					}
				}
				else
				{
					fail("The Credit Card Default text is not displayed.");
				}
				
				WebElement couponCreditCardPINLabel = BNBasicCommonMethods.findElement(driver, obj, "couponCreditCardPINLabel");
				if(BNBasicCommonMethods.isElementPresent(couponCreditCardPINLabel))
				{
					pass("The Gift Card PIN Default Text is displayed.");
					log.add("The expected title was : " + labelVal[1]);
					String title = couponCreditCardPINLabel.getText();
					log.add("The actual title is : " + title);
					/*System.out.println(title);
					System.out.println(Val[1]);*/
					if(title.contains(labelVal[1]))
					{
						pass("The expected title and the actual title matches.");
					}
					else
					{
						fail("The expected title and the actual title does not matches.");
					}
				}
				else
				{
					fail("The Credit Card PIN Default text is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(e.getMessage());
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BRM - 759 Guest Checkout – Verify that clicking on cross mark should close the expanded B&N Gift cards.*/
	public void payInfoCCGiftCardClose()
	{
		ChildCreation("BRM - 759 Guest Checkout – Verify that clicking on cross mark should close the expanded B&N Gift cards.");
		try
			{
				WebElement couponGiftCardsActive = BNBasicCommonMethods.findElement(driver, obj, "couponGiftCardsActive");
				if(BNBasicCommonMethods.isElementPresent(couponGiftCardsActive))
				{
					pass("The Gift Card Container is Active and the fields are displayed.");
					WebElement couponGiftCardsOpenTrigger = BNBasicCommonMethods.findElement(driver, obj, "couponGiftCardsOpenTrigger");
					if(BNBasicCommonMethods.isElementPresent(couponGiftCardsOpenTrigger))
					{
						couponGiftCardsOpenTrigger.click();
						//Thread.sleep(2000);
						if(BNBasicCommonMethods.isElementPresent(couponGiftCardsActive))
						{
							fail("The Gift Cards Container is still active.");
						}
						else
						{
							pass("The Gift Cards Container is not active.");
						}
					}
					else
					{
						fail("The Gift Cards Close trigger is not displayed.");
					}
				}
				else
				{
					fail("The Gift Cards Container is not active.");
				}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BRM - 760 Guest Checkout – Verify that clicking on + in Coupon code should display a input field and apply button.*/
	public void payInfoCCGiftCouponCodeFields()
	{
		ChildCreation("BNIA-760 Guest Checkout Verify that clicking on + in Coupon code should display a input field and apply button.");
		try
			{
					//Thread.sleep(1000);
					String expColor = BNBasicCommonMethods.getExcelNumericVal("BRM760", sheet, 11);
					String cellVal = BNBasicCommonMethods.getExcelNumericVal("BRM760", sheet, 12);
					String[] Val = cellVal.split("\n");
					List <WebElement> loyaltyContainerList = driver.findElements(By.xpath("//*[@id='couponContainer']//*[@class='loyalty-options-container']"));	 
					if(BNBasicCommonMethods.isListElementPresent(loyaltyContainerList))
					{
						WebElement couponCodeApplyContainer = BNBasicCommonMethods.findElement(driver, obj, "couponCodeApplyContainer");
						if(BNBasicCommonMethods.isElementPresent(couponCodeApplyContainer))
						{
							pass("The Coupon Code text field is displayed.");
						}
						else
						{
							fail("The Coupon Code text field is not displayed.");
						}
						
						WebElement couponCodeDefaultText = BNBasicCommonMethods.findElement(driver, obj, "couponCodeDefaultText");
						if(BNBasicCommonMethods.isElementPresent(couponCodeDefaultText))
						{
							pass("The Default text is displayed for the Coupon field");
							log.add("The Expected Default Text was : " + Val[1]);
							String dfltText = couponCodeDefaultText.getText();
							log.add("The actual default text is : " + dfltText);
							if((dfltText.contains(Val[1])))
							{
								pass("The Default text matches the expected content.",log);
							}
							else
							{
								fail("The Default text does not match the expected conten.",log);
							}
						}
						else
						{
							fail("The Default text is not displayed for the Coupon field");
						}
						
						WebElement couponCodeApplyButton = BNBasicCommonMethods.findElement(driver, obj, "couponCodeApplyButton");
						if(BNBasicCommonMethods.isElementPresent(couponCodeApplyButton))
						{
							pass("The Coupon Code Apply button is displayed.");
							String caption = couponCodeApplyButton.getAttribute("value");
							log.add("The expected value was : " + Val[0]);
							log.add("The actual val is : " + caption);
							if(caption.equals(Val[0]))
							{
								pass("The Submit button caption matches.",log);
							}
							else
							{
								fail("The Submit button caption does not matches.");
							}
							
							String csVal = couponCodeApplyButton.getCssValue("background").substring(0,16);
							Color colorhxcnvt = Color.fromString(csVal);
							String hexCode = colorhxcnvt.asHex();
							log.add("The expected color of the button is : " + expColor);
							log.add("The actual color of the button is : " + hexCode);
							if(hexCode.equals(expColor))
							{
								pass("The Apply button color matches the expected one.",log);
							}
							else
							{
								fail("The Apply button color does not matches the expected one.",log);
							}
						}
						else
						{
							fail("The Coupon Code Apply button is not displayed.");
						}
					}
					else
					{
						fail("The Loyalty Container list is not displayed.");
					}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BNIA-1202 Verify that if a user enter a invalid Coupon code and clicking on apply, coupon should not applied and alert must me displayed. Default text should be displayed for input field.*/
	public void payInfoCCGiftCouponCodeInvalidValidation()
	{
		ChildCreation(" BNIA-1202 Verify that if a user enter a invalid Coupon code and clicking on apply, coupon should not applied and alert must me displayed. Default text should be displayed for input field.");
		try
			{
				String cpnCode = BNBasicCommonMethods.getExcelNumericVal("BRM762", sheet, 42);
				String cellVal = BNBasicCommonMethods.getExcelNumericVal("BRM760", sheet, 12);
				String[] defText = cellVal.split("\n");
				String cpnAlert = BNBasicCommonMethods.getExcelVal("BRM762", sheet, 10);
				String expColor = BNBasicCommonMethods.getExcelVal("BRM762", sheet, 11);
				WebElement couponCodeTextField = BNBasicCommonMethods.findElement(driver, obj, "couponCodeTextField");
				if(BNBasicCommonMethods.isElementPresent(couponCodeTextField))
				{
					pass("The Coupon Code Container is displayed.");
					WebElement couponCodeTextFieldActive = BNBasicCommonMethods.findElement(driver, obj, "couponCodeTextFieldActive");
					couponCodeTextFieldActive.click();
					couponCodeTextFieldActive.clear();
					/*Actions act = new Actions(driver);
					act.sendKeys(cpnCode).build().perform();*/
					couponCodeTextFieldActive.sendKeys(cpnCode);
					WebElement couponCodeApplyButton = BNBasicCommonMethods.findElement(driver, obj, "couponCodeApplyButton");
					couponCodeApplyButton.click();
					BNBasicCommonMethods.waitforElement(wait, obj, "couponCouponError");
					WebElement couponCouponError = BNBasicCommonMethods.findElement(driver, obj, "couponCouponError");
					wait.until(ExpectedConditions.visibilityOf(couponCouponError)).getAttribute("style").contains("display: block;");
					//Thread.sleep(1000);
					log.add("The Expected alert was : " + cpnAlert);
					//String actAlert = couponCouponError.findElement(By.xpath("//*[@class='err']")).getText();
					String actAlert = driver.findElement(By.xpath("//*[@id='frmApplyCoupon']//*[@class='coupon-error coupon-error-cpn']//*[@class='err']")).getText();
					log.add("The actual alert is : " + actAlert);
					if(actAlert.contains(cpnAlert))
					{
						pass("The expected alert and actual alert match.",log);
					}
					else
					{
						Skip("The expected alert and actual alert does not match."+log);
					}
					
					String csVal = couponCodeTextFieldActive.getCssValue("color").substring(0, 16);
					Color colorhxcnvt = Color.fromString(csVal);
					String hexCode = colorhxcnvt.asHex();
					log.add("The expected color was : " + expColor);
					log.add("The actual color is : " + hexCode);
					if(hexCode.contains(expColor))
					{
						pass("The Coupon Code field is highlighted.",log);
					}
					else
					{
						fail("The Coupon Code field is not highlighted.",log);
					}
					WebElement couponCodeDefaultText = BNBasicCommonMethods.findElement(driver, obj, "couponCodeDefaultText");
					if(BNBasicCommonMethods.isElementPresent(couponCodeDefaultText))
					{
						pass("The Default text is displayed for the Coupon field");
						log.add("The Expected Default Text was : " + defText[1]);
						String dfltText = couponCodeDefaultText.getText();
						log.add("The actual default text is : " + dfltText);
						if((dfltText.contains(defText[1])))
						{
							pass("The Default text matches the expected content.",log);
						}
						else
						{
							fail("The Default text does not match the expected conten.",log);
						}
					}
					else
					{
						fail("The Default text is not displayed for the Coupon field");
					}
				}
				else
				{
					fail("The Coupon Code Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BNIA-1215 Guest Checkout - Verify that on selecting Credit Card tab it should be highlighted.*/
	public void payInfoCCTabHighlight()
	{
		ChildCreation(" BNIA-1215 Guest Checkout - Verify that on selecting Credit Card tab it should be highlighted.");
		try
			{
				String expColor = BNBasicCommonMethods.getExcelVal("BRM943", sheet, 11);
				WebElement ccNumber = BNBasicCommonMethods.findElement(driver, obj, "ccNumber");
						if(BNBasicCommonMethods.isElementPresent(ccNumber))
						{
							pass("The credit card payment tab is displayed.");
							ccNumber.click();
							String ColorName = ccNumber.getCssValue("color");
							Color colorhxcnvt = Color.fromString(ColorName);
							String hexCode = colorhxcnvt.asHex();
							log.add("The Expected Color was : " + expColor);
							log.add("The Actual Color is : " + hexCode);
							if(expColor.equals(hexCode))
							{
								pass("The Tab is highlighted with the expected color.",log);
							}
							else
							{
								fail("The Tab is not highlighted with the expected color.",log);
							}
						}
						else
						{
							fail("The credit card payment tab is not displayed.");
						}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BRM - 763 Guest Checkout – Verify that clicking on cross mark closes the expanded Coupon code. out of scope*/
	public void payInfoCCGiftCouponCodeClose()
	{
		ChildCreation(" BRM - 763 Guest Checkout – Verify that clicking on cross mark closes the expanded Coupon code.");
		try
			{
				WebElement couponCodeApplyContainer = BNBasicCommonMethods.findElement(driver, obj, "couponCodeApplyContainer");
				if(BNBasicCommonMethods.isElementPresent(couponCodeApplyContainer))
				{
					boolean couponCodeActive = couponCodeApplyContainer.getAttribute("class").contains("loyalty-field");
					if(couponCodeActive==true)
					{
						pass("The Gift Card Container is Active and the fields are displayed.");
						WebElement couponCouponOpenTrigger = BNBasicCommonMethods.findElement(driver, obj, "couponCouponOpenTrigger");
						if(BNBasicCommonMethods.isElementPresent(couponCouponOpenTrigger))
						{
							couponCouponOpenTrigger.click();
							//Thread.sleep(2000);
							couponCodeActive = couponCodeApplyContainer.getAttribute("class").contains("loyalty-field fieldActive");
							//Thread.sleep(1000);
							if(couponCodeActive==true)
							{
								fail("The Coupon Code Container is not closed.");
							}
							else
							{
								pass("The Coupon Code Container is closed.");
							}
						}
						else
						{
							fail("The Coupon Code Close trigger is not displayed.");
						}
					}
					else
					{
						fail("The Coupon Code Container is not active.");
					}
				}
				else
				{
					fail("The Coupon Code Conatainer is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BRM - 764 Guest Checkout – Verify that clicking on + in Book Fair ID should display a input field and apply button.*/
	public void payInfoCCBookfairContainer()
	{
		ChildCreation(" BNIA-764 Verify that clicking on + in Book Fair ID should display a input field and apply button.");
		try
			{
			 		sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
					String cellVal = BNBasicCommonMethods.getExcelNumericVal("BRM764", sheet, 12);
					String[] Val = cellVal.split("\n");
					List <WebElement> loyaltyContainerList = driver.findElements(By.xpath("//*[@id='couponContainer']//*[@class='loyalty-options-container']"));
					if(BNBasicCommonMethods.isListElementPresent(loyaltyContainerList))
					{
						WebElement couponCodeApplyContainer = BNBasicCommonMethods.findElement(driver, obj, "couponCodeApplyContainer");
						if(BNBasicCommonMethods.isElementPresent(couponCodeApplyContainer))
						{
							pass("The Book Fair ID Code text field is displayed.");
						}
						else
						{
							fail("The Book Fair ID text field is not displayed.");
						}
						
						WebElement bookFairDefaultText = BNBasicCommonMethods.findElement(driver, obj, "bookFairDefaultText");
						if(BNBasicCommonMethods.isElementPresent(bookFairDefaultText))
						{
							pass("The Default text is displayed for the Coupon field");
							log.add("The Expected Default text was : " + Val[1]);
							String title = bookFairDefaultText.getText();
							log.add("The actual Default text is : " + bookFairDefaultText.getText());
							if(title.contains(Val[1]))
							{
								pass("The Default text matches the expected content.",log);
							}
							else
							{
								System.out.println(Val[1]+" : "+title);
								fail("The Default text does not match the expected conten.",log);
							}
						}
						else
						{
							fail("The Default text is not displayed for the Coupon field");
						}
						
						WebElement bookfairApplyButton = BNBasicCommonMethods.findElement(driver, obj, "bookfairApplyButton");
						if(BNBasicCommonMethods.isElementPresent(bookfairApplyButton))
						{
							pass("The Book Fair ID Apply button is displayed.");
							String caption = bookfairApplyButton.getAttribute("value");
							log.add("The actual val is : " + caption);
							if(caption.equals(Val[0]))
							{
								pass("The Submit button caption matches.",log);
							}
							else
							{
								fail("The Submit button caption does not matches.");
							}
						}
						else
						{
							fail("The Book Fair ID Apply button is not displayed.");
						}
					}
					else
					{
						fail("The Loyalty Container list is not displayed.");
					}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BNIA-1204 Verify that if a user enter a invalid Book Fair ID and clicking on apply, coupon should not applied and alert must me displayed.*/
	public void payInfoCCBookfairValidation()
	{
		ChildCreation(" BNIA-766 Verify that if a user enter a invalid Book Fair ID and clicking on apply, coupon should not applied and alert must me displayed.");
		try
			{
				List <WebElement> loyaltyContainerList = driver.findElements(By.xpath("//*[@id='couponContainer']//*[@class='loyalty-options-container']"));
				if(BNBasicCommonMethods.isListElementPresent(loyaltyContainerList))
				{
					String expAlert = BNBasicCommonMethods.getExcelVal("BRM766", sheet, 10);
					String expColor = BNBasicCommonMethods.getExcelVal("BRM766", sheet, 11);
					String bookFairID = BNBasicCommonMethods.getExcelNumericVal("BRM766", sheet, 42);
					WebElement bookFairTextField = BNBasicCommonMethods.findElement(driver, obj, "bookFairTextField");
					if(BNBasicCommonMethods.isElementPresent(bookFairTextField))
					{
						pass("The Apply button is displayed in the Book Fair ID section.");
						bookFairTextField.click();
						bookFairTextField.clear();
						bookFairTextField.sendKeys(bookFairID);
						WebElement bookfairApplyButton = BNBasicCommonMethods.findElement(driver, obj, "bookfairApplyButton");
						bookfairApplyButton.click();
						BNBasicCommonMethods.waitforElement(wait, obj, "couponBookFairErrornew");
						WebElement couponBookFairError = BNBasicCommonMethods.findElement(driver, obj, "couponBookFairErrornew");
						wait.until(ExpectedConditions.visibilityOf(couponBookFairError)).getAttribute("style").contains("display: block;");
						//Thread.sleep(1000);
						log.add("The Expected alert was : " + expAlert);
						//String actAlert = driver.findElement(By.xpath("//*[@id='bookfairApply']//*[@class='coupon-error coupon-error-bf']//*[@class='err']")).getText();
						String actAlert = couponBookFairError.getText();
						log.add("The actual alert was : " + actAlert);
						if(expAlert.contains(actAlert))
						{
							pass("The raised alert match the expected content.",log);
						}
						else
					{
							fail("The raised alert does not match the expected content.",log);
						}
						
							//String csVal = bookFairTextField.getCssValue("border").substring(0, 25);
						bookFairTextField = BNBasicCommonMethods.findElement(driver, obj, "bookFairTextField");
						String csVal = bookFairTextField.getCssValue("color");
						String hexCode = BNBasicCommonMethods.colorfinder(csVal);
						log.add("The expected color was : " + expColor);
						log.add("The actual color is : " + hexCode);
						if(hexCode.contains(expColor))
						{
							pass("The Book Fair ID number field is highlighted.",log);
						}
						else
						{
							fail("The Book Fair ID field is not highlighted.",log);
						}
					}
					else
					{
						fail("The Apply button is not displayed in the Book Fair ID section.");
					}
				}
				else
				{
					fail("The bookfair text field is not displayed.");
				}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BRM - 767  Guest Checkout – Verify that Book Fair ID input field should accept maximum of 12 characters and default text should be displayed for input field.*/
	public void payInfoCCBookfairLengthValidation()
	{
		ChildCreation(" BNIA-1023 Verify that Book Fair ID input field should accept maximum of 12 characters and default text should be displayed for input field.");
		try
			{
				List <WebElement> loyaltyContainerList = driver.findElements(By.xpath("//*[@id='couponContainer']//*[@class='loyalty-options-container']"));	
				if(BNBasicCommonMethods.isListElementPresent(loyaltyContainerList))
				{
					
					String cellVal = BNBasicCommonMethods.getExcelNumericVal("BRM767", sheet, 42);
					String[] bookFairID = cellVal.split("\n");
					String cellVal1 = BNBasicCommonMethods.getExcelNumericVal("BRM764", sheet, 12);
					String[] Val = cellVal1.split("\n");
					WebElement bookFairTextField = BNBasicCommonMethods.findElement(driver, obj, "bookFairTextField");
					if(BNBasicCommonMethods.isElementPresent(bookFairTextField))
					{
						pass("The Apply button is displayed in the Book Fair ID section.");
						for(int i = 0; i<bookFairID.length;i++)
						{
							bookFairTextField.click();
							bookFairTextField.clear();
							bookFairTextField.sendKeys(bookFairID[i]);
							int curlen = bookFairTextField.getAttribute("value").length();
							log.add("The Value from the File was : " + bookFairID[i] + " and its length is  : " + bookFairID[i].length());
							log.add("The Current Value is : " + curlen);
							if(curlen<=12)
							{
								pass("The Length does not exceeds the specified limit.",log);
							}
							else
							{
								fail("The Length exceeds the specified limit.",log);
							}
						}
						
						WebElement bookFairDefaultText = BNBasicCommonMethods.findElement(driver, obj, "bookFairDefaultText");
						if(BNBasicCommonMethods.isElementPresent(bookFairDefaultText))
						{
							pass("The Default text is displayed for the Coupon field");
							log.add("The Expected Default text was : " + Val[1]);
							String title = bookFairDefaultText.getText();
							log.add("The actual Default text is : " + bookFairDefaultText.getText());
							if(title.contains(Val[1]))
							{
								pass("The Default text matches the expected content.",log);
							}
							else
							{
								fail("The Default text does not match the expected conten.",log);
							}
						}
						else
						{
							fail("The Default text is not displayed for the Coupon field");
						}
					}
					else
					{
						fail("The Apply button is not displayed in the Book Fair ID section.");
					}
				}
				else
				{
					fail("The bookfair text field is not displayed.");
				}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}

	/* BRM - 768 Guest Checkout – Verify that clicking on cross mark should close the expanded Book Fair ID.*/
	public void payInfoCCGiftBookFairClose()
	{
		ChildCreation("BRM - 768 Guest Checkout – Verify that clicking on cross mark should close the expanded Book Fair ID.");
		try
			{
			WebElement bookfairApplyContainer = BNBasicCommonMethods.findElement(driver, obj, "bookfairApplyContainer");
				if(BNBasicCommonMethods.isElementPresent(bookfairApplyContainer))
				{
					boolean bookFairActive = bookfairApplyContainer.getAttribute("class").contains("loyalty-field fieldActive");
					if(bookFairActive==true)
					{
						pass("The Gift Card Container is Active and the fields are displayed.");
						WebElement bookFairOpenTrigger = BNBasicCommonMethods.findElement(driver, obj, "bookFairOpenTrigger");
						if(BNBasicCommonMethods.isElementPresent(bookFairOpenTrigger))
						{
							bookFairOpenTrigger.click();
							//Thread.sleep(2000);
							BNBasicCommonMethods.waitforElement(wait, obj, "couponCodeApplyContainer");
							WebElement couponCodeApplyContainer = BNBasicCommonMethods.findElement(driver, obj, "couponCodeApplyContainer");
							bookFairActive = couponCodeApplyContainer.getAttribute("class").contains("loyalty-field fieldActive");
							//Thread.sleep(1000);
							if(bookFairActive==true)
							{
								fail("The Book Fair Container is not closed.");
							}
							else
							{
								pass("The Book Fair Container is closed.");
							}
						}
						else
						{
							fail("The Book Fair Close trigger is not displayed.");
						}
					}
					else
					{
						fail("The Book Fair Container is not active.");
					}
				}
				else
				{
					fail("The Book Fair Conatainer is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}

	/* BNIA-1205 Verify that subtotal (number of items) and the total amount is displayed in the Order Summary Section.*/
	public void payInfoCCGiftSummaryContainerSubtotal()
	{
		ChildCreation("BNIA-1205 Verify that subtotal (number of items) and the total amount is displayed in the Order Summary Section.");
		try
			{
			   sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
				String title = BNBasicCommonMethods.getExcelNumericVal("BRM769", sheet, 12);
				WebElement summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
				if(BNBasicCommonMethods.isElementPresent(summaryContainer))
				{
					pass("The Summary Container is displayed.");
					BNBasicCommonMethods.scrolldown(summaryContainer, driver);
					String label = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)[1]//span)[1]")).getText();
					WebElement details = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)[1]//span)[2]"));
					log.add("The Expected title was : " + title);
					log.add("The Actual title is : " + label);
					if(label.contains(title))
					{
						pass("The " + label+ " line is found and it is displayed.",log);
					}
					else
					{
						fail("The Expected label was not displayed.",log);
					}
					
					Float val = Float.parseFloat(details.getText().replace("$", ""));
					log.add("The displayed price was : " + details.getText());
					if(val>=0)
					{
						pass(" The Price is displayed.",log);
					}
					else
					{
						fail("There is something wrong. Please Check.",log);
					}
				}
				else
				{
					fail("The Summary Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BNIA-1206 Verify that estimated tax and the amount is displayed displayed in the Order Summary Section*/
	public void payInfoCCGiftSummaryContainerEstimatedShip()
	{
		ChildCreation("BNIA-1206 Verify that estimated tax and the amount is displayed displayed in the Order Summary Section.");
		try
			{
				String title = BNBasicCommonMethods.getExcelNumericVal("BRM770", sheet, 12);
				WebElement summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
				if(BNBasicCommonMethods.isElementPresent(summaryContainer))
				{
					pass("The Summary Container is displayed.");
					String label = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)[2]//span)[1]")).getText();
					WebElement details = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)[2]//span)[2]"));
					log.add("The Expected title was : " + title);
					log.add("The Actual title is : " + label);
					if(title.contains(label))
					{
						pass("The " + label+ " line is found and it is displayed.",log);
					}
					else
					{
						fail("The Expected label was not displayed.",log);
					}
					
					try
					{
						Float val = Float.parseFloat(details.getText().replace("$", ""));
						log.add("The displayed price was : " + details.getText());
						if(val>=0)
						{
							pass(" The Price is displayed.",log);
						}
						else
						{
							fail("There is something wrong. Please Check.",log);
						}
					}
					catch(Exception e)
					{
						String text = details.getText();
						log.add("The displayed value was : " + details.getText());
						if(text.equalsIgnoreCase("FREE"))
						{
							pass(" The Value is displayed.",log);
						}
						else
						{
							fail("There is something wrong. Please Check.",log);
						}
					}
					
				}
				else
				{
					fail("The Summary Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BNIA-1283 Guest Checkout - Verify that estimated tax and the amount should be displayed in the Order Summary section*/
	public void payInfoCCGiftSummaryContainerEstimatedTax()
	{
		ChildCreation(" BNIA-1283 Guest Checkout - Verify that estimated tax and the amount should be displayed in the Order Summary section.");
		try
			{
				String title = BNBasicCommonMethods.getExcelNumericVal("BRM771", sheet, 12);
				WebElement summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
				if(BNBasicCommonMethods.isElementPresent(summaryContainer))
				{
					pass("The Summary Container is displayed.");
					String label = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)[3]//span)[1]")).getText();
					WebElement details = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)[3]//span)[2]"));
					log.add("The Expected title was : " + title);
					log.add("The Actual title is : " + label);
					if(label.contains(title))
					{
						pass("The " + label+ " line is found and it is displayed.",log);
					}
					else
					{
						fail("The Expected label was not displayed.",log);
					}
					
					Float val = Float.parseFloat(details.getText().replace("$", ""));
					log.add("The displayed price was : " + details.getText());
					if(val>=0)
					{
						pass(" The Price is displayed.",log);
					}
					else
					{
						fail("There is something wrong. Please Check.",log);
					}
				}
				else
				{
					fail("The Summary Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BNIA-1207 Verify that ORDER TOTAL and its amount is displayed above the "Continue" button.*/
	public void payInfoCCGiftSummaryContainerOrderTotal()
	{
		ChildCreation(" BNIA-1207 Verify that ORDER TOTAL and its amount is displayed above the Continue button.");
		try
			{
				String title = BNBasicCommonMethods.getExcelNumericVal("BRM772", sheet, 12);
				WebElement summaryContainer = BNBasicCommonMethods.findElement(driver, obj, "summaryContainer");
				if(BNBasicCommonMethods.isElementPresent(summaryContainer))
				{
					pass("The Summary Container is displayed.");
					String label = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)[4]//span)[1]")).getText();
					WebElement details = driver.findElement(By.xpath("((//*[@id='summaryContainer']//li)[4]//span)[2]"));
					log.add("The Expected title was : " + title);
					log.add("The Actual title is : " + label);
					if(label.contains(title))
					{
						pass("The " + label+ " line is found and it is displayed.",log);
					}
					else
					{
						fail("The Expected label was not displayed.",log);
					}
					
					Float val = Float.parseFloat(details.getText().replace("$", ""));
					log.add("The displayed price was : " + details.getText());
					if(val>=0)
					{
						pass(" The Price is displayed.",log);
					}
					else
					{
						fail("There is something wrong. Please Check.",log);
					}
				}
				else
				{
					fail("The Summary Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
		
	/* BNIA-1209 Verify and validate all the input fields in shipping address overlay.*/
	public void payInfoCCShipAddressDetails()
	{
		ChildCreation(" BNIA-1209 Verify and validate all the input fields in shipping address overlay.");
		try
			{
				WebElement countrySelection = BNBasicCommonMethods.findElement(driver, obj, "countrySelection");
				if(BNBasicCommonMethods.isElementPresent(countrySelection))
				{
					pass("The Country drop down list box is present and visible.");
				}
				else
				{
					fail("The Country drop down list box is not present / visible.");
				}
				
				WebElement fName = BNBasicCommonMethods.findElement(driver, obj, "fName");
				if(BNBasicCommonMethods.isElementPresent(fName))
				{
					pass("The First Name text field is present and visible.");
				}
				else
				{
					fail("The First Name text field is not present / visible.");
				}
				
				WebElement lName = BNBasicCommonMethods.findElement(driver, obj, "lName");
				if(BNBasicCommonMethods.isElementPresent(lName))
				{
					pass("The Last Name text field is present and visible.");
				}
				else
				{
					fail("The Last Name text field is not present / visible.");
				}
				
				WebElement stAddress = BNBasicCommonMethods.findElement(driver, obj, "stAddress");
				if(BNBasicCommonMethods.isElementPresent(stAddress))
				{
					pass("The Stree Address text field is present and visible.");
				}
				else
				{
					fail("The Stree Address text field is not present / visible.");
				}
				
				WebElement aptSuiteAddress = BNBasicCommonMethods.findElement(driver, obj, "aptSuiteAddress");
				BNBasicCommonMethods.scrolldown(aptSuiteAddress, driver);
				if(BNBasicCommonMethods.isElementPresent(aptSuiteAddress))
				{
					pass("The Apt/Suite text field is present and visible.");
				}
				else
				{
					fail("The Apt/Suite text field is not present / visible.");
				}
				
				WebElement city = BNBasicCommonMethods.findElement(driver, obj, "city");
				if(BNBasicCommonMethods.isElementPresent(city))
				{
					pass("The City text field is present and visible.");
				}
				else
				{
					fail("The City text field is not present / visible.");
				}
				
				WebElement state = BNBasicCommonMethods.findElement(driver, obj, "state");
				if(BNBasicCommonMethods.isElementPresent(state))
				{
					pass("The State selection drop down is present and visible.");
				}
				else
				{
					fail("The State selection drop down is not present / visible.");
				}
				
				WebElement zipCode = BNBasicCommonMethods.findElement(driver, obj, "zipCode");
				if(BNBasicCommonMethods.isElementPresent(zipCode))
				{
					pass("The Zipcode text field is present and visible.");
				}
				else
				{
					fail("The Zipcode text field is not present / visible.");
				}
				
				WebElement contactNo = BNBasicCommonMethods.findElement(driver, obj, "contactNo");
				if(BNBasicCommonMethods.isElementPresent(contactNo))
				{
					pass("The Phone Number text field is present and visible.");
				}
				else
				{
					fail("The Phone Number text field is not present / visible.");
				}
				
				WebElement companyName = BNBasicCommonMethods.findElement(driver, obj, "companyName");
				if(BNBasicCommonMethods.isElementPresent(companyName))
				{
					pass("The Company Name text field is present and visible.");
				}
				else
				{
					fail("The Company Name text field is not present / visible.");
				}
				
				WebElement ccName = BNBasicCommonMethods.findElement(driver, obj, "ccName");
				BNBasicCommonMethods.scrolldown(ccName,driver);
				//Thread.sleep(1000);
				WebElement guestSameAsShippingChkbox1 = BNBasicCommonMethods.findElement(driver, obj, "guestSameAsShippingChkbox1");
				guestSameAsShippingChkbox1.click();
				//Thread.sleep(1000);
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
	}
	
	/* BNIA-1214 Guest Checkout - Verify that alert messages are displayed for wrong card numbers entered.*/
	public void payInfoCCInvalidCardValidation()
	{
		ChildCreation(" BNIA-1214 Guest Checkout - Verify that alert messages are displayed for wrong card numbers entered..");
		try
			{
			  sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
				String cellVal = BNBasicCommonMethods.getExcelNumericVal("BRM902", sheet, 38);
				String alert = BNBasicCommonMethods.getExcelVal("BRM902", sheet, 10);
				WebElement ccNumber = BNBasicCommonMethods.findElement(driver, obj, "ccNumber");
				if(BNBasicCommonMethods.isElementPresent(ccNumber))
				{
					//BNBasicCommonMethods.scrolldown(ccNumber,driver);
					ccNumber.click();
					ccNumber.clear();
					ccNumber.sendKeys(cellVal);
					WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
					BNBasicCommonMethods.scrolldown(orderContinuebtn, driver);
					orderContinuebtn.click();
					BNBasicCommonMethods.waitforElement(wait, obj, "guestError");
					//wait.until(ExpectedConditions.visibilityOf(guestError));
					//Thread.sleep(1000);
					log.add("The Expected alert was : " + alert);
					WebElement guestError = BNBasicCommonMethods.findElement(driver, obj, "guestError");
					if(BNBasicCommonMethods.isElementPresent(guestError))
					{
						pass("The alert is raised to indicate user.");
						String actAlert = guestError.getText();
						log.add("The Actual alert was : " + actAlert);
						if(actAlert.contains(alert))
						{
							pass("The alert match the expected content.",log);
						}
						else
						{
							fail("The alert does not match the expected content.",log);
						}
					}
					else
					{
						fail("No Validation is not raised to indicate user.");
					}
				}
				else
				{
					fail("The Credit Card Number field is not displayed.");
				}
			}
			catch (Exception e)
			{
				exception("There is something wrong.Please Check." + e.getMessage());
			}
	}
	
	/* BNIA-1236 Verify that name on the card field does not accept numbers and special characters.*/
	public void payInfoCCInvalidNameValidation()
	{
		ChildCreation(" BNIA-1236 Verify that name on the card field does not accept numbers and special characters.");
		try
			{
			  sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
				String cellVal = BNBasicCommonMethods.getExcelNumericVal("BRM907", sheet, 38);
				String cellName = BNBasicCommonMethods.getExcelNumericVal("BRM907", sheet, 41);
				String alert = BNBasicCommonMethods.getExcelVal("BRM907", sheet, 10);
				WebElement ccNumber = BNBasicCommonMethods.findElement(driver, obj, "ccNumber");
				if(BNBasicCommonMethods.isElementPresent(ccNumber))
				{
					//BNBasicCommonMethods.scrolldown(ccNumber,driver);
					ccNumber.click();
					ccNumber.clear();
					ccNumber.sendKeys(cellVal);
					WebElement ccName = BNBasicCommonMethods.findElement(driver, obj, "ccName");
					ccName.clear();
					ccName.sendKeys(cellName);
					WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
					BNBasicCommonMethods.scrolldown(orderContinuebtn, driver);
					orderContinuebtn.click();
					BNBasicCommonMethods.waitforElement(wait, obj, "guestError");
					WebElement guestError = BNBasicCommonMethods.findElement(driver, obj, "guestError");
					wait.until(ExpectedConditions.visibilityOf(guestError));
					//Thread.sleep(1000);
					log.add("The Expected alert was : " + alert);
					if(BNBasicCommonMethods.isElementPresent(guestError))
					{
						pass("The alert is raised to indicate user.");
						String actAlert = guestError.getText();
						log.add("The Actual alert was : " + actAlert);
						if(actAlert.contains(alert))
						{
							pass("The alert match the expected content.",log);
						}
						else
						{
							fail("The alert does not match the expected content.",log);
						}
					}
					else
					{
						fail("No Validation is not raised to indicate user.");
					}
				}
				else
				{
					fail("The Credit Card Number field is not displayed.");
				}
			}
			catch (Exception e)
			{
				exception("There is something wrong.Please Check." + e.getMessage());
			}
	}

	/*BNIA-1211 Verify that on updating all the payment details and clicking on "Continue" button should proceed to the Review order page.*/
	/*BNIA-1179 Guest Checkout - Verify that payment page should allow the guest user to fill the required payment details*/
	public void payInfoCCReviewOrderPageNavigation()
	{
		boolean childflag=false;
		ChildCreation("BNIA-1211 Verify that on updating all the payment details and clicking on Continue button should proceed to the Review order page.");
		try
			{
		    	 sheet = BNBasicCommonMethods.excelsetUp("Payment Info");
				String ccNum = BNBasicCommonMethods.getExcelNumericVal("BRM793", sheet, 38);
				String ccNm = BNBasicCommonMethods.getExcelVal("BRM793", sheet, 41);
				String cvv = BNBasicCommonMethods.getExcelNumericVal("BRM793", sheet, 42);
				String email = BNBasicCommonMethods.getExcelVal("BRM793", sheet, 45);
				WebElement ccNumber = BNBasicCommonMethods.findElement(driver, obj, "ccNumber");
				BNBasicCommonMethods.scrolldown(ccNumber, driver);
				WebElement ccMonth = BNBasicCommonMethods.findElement(driver, obj, "ccMonth");
				//Select mntsel = new Select(ccMonth);
				WebElement ccYear = BNBasicCommonMethods.findElement(driver, obj, "ccYear");
				//Select yrsel = new Select(ccYear);
					if(BNBasicCommonMethods.isElementPresent(ccNumber))
					{
						pass("The Credit Card Number is displayed.");
						ccNumber.clear();
						ccNumber.sendKeys(ccNum);
					}
					else
					{
						fail("The Credit Card Number is not displayed.");
					}
						WebElement ccName = BNBasicCommonMethods.findElement(driver, obj, "ccName");
						if(BNBasicCommonMethods.isElementPresent(ccName))
						{
							pass("The Credit Card Name is displayed.");
							ccName.clear();
							ccName.sendKeys(ccNm);
						}
						else
						{
							fail("The Credit Card Name is not displayed.");
						}

						if(BNBasicCommonMethods.isElementPresent(ccMonth))
						{
							WebElement MonthDropdown  = driver.findElement(By.xpath("//*[@id='ccMonth-replacement']"));
							MonthDropdown.click();
							List <WebElement> Monthlistsel = driver.findElements(By.xpath("//*[@id='ccMonth-option-list']//li"));
							Monthlistsel.get(6).click();
							pass("The Credit Card Month drop down is displayed.");
							//MonthDropdown.click();
							//Thread.sleep(1500);
						}
						else
						{
							fail("The Credit Card Month drop down is not displayed.");
						}
						
						
						if(BNBasicCommonMethods.isElementPresent(ccYear))
						{
							WebElement YearDropdown  = driver.findElement(By.xpath("//*[@id='ccYear-replacement']"));
							YearDropdown.click();
							List <WebElement> Yearlistsel = driver.findElements(By.xpath("//*[@id='ccYear-option-list']//li"));
							Yearlistsel.get(6).click();
							//YearDropdown.click();
							pass("The Credit Card Year drop down field is displayed.");
						}
						else
						{
							fail("The Credit Card Year drop down field is not displayed.");
						}
						
						WebElement ccCsv = BNBasicCommonMethods.findElement(driver, obj, "ccCsv");
						if(BNBasicCommonMethods.isElementPresent(ccCsv))
						{
							pass("The Credit Card Security Field CSV is displayed.");
							ccCsv.clear();
							ccCsv.sendKeys(cvv);
						}
						else
						{
							fail("The Credit Card Security Field CSV field is not displayed.");
						}
						
						WebElement guestEmailAddress = BNBasicCommonMethods.findElement(driver, obj, "guestEmailAddress");
						if(BNBasicCommonMethods.isElementPresent(guestEmailAddress))
						{
							BNBasicCommonMethods.scrolldown(guestEmailAddress, driver);
							pass("The Email Address Field is dispalyed.");
							guestEmailAddress.clear();
							guestEmailAddress.sendKeys(email);
						}
						else
						{
							fail("The Email Address Field is not dispalyed.");
						}
						
						WebElement orderContinuebtn = BNBasicCommonMethods.findElement(driver, obj, "orderContinuebtn");
						if(BNBasicCommonMethods.isElementPresent(orderContinuebtn))
						{
							pass("The Order Continue button is displayed.");
							orderContinuebtn.click();
							Thread.sleep(500);
							/*boolean noError = false;
							do
							{
								BNBasicfeature.scrolldown(summaryContainer, driver);
								orderContinuebtn.click();
								Thread.sleep(3000);
								if(BNBasicfeature.isElementPresent(guestError))
								{
									noError = true;
								}
								else
								{
									noError = false;
								}
							}while(!noError==true);*/
							BNBasicCommonMethods.waitforElement(wait, obj, "guestError1");
							WebElement guestError1 = BNBasicCommonMethods.findElement(driver, obj, "guestError1");
							if(BNBasicCommonMethods.isElementPresent(guestError1))
							{
								Skip("The Order is not processed due to Error. The duisplayed error was : " + guestError1.getText());
							}
							else
							{
								BNBasicCommonMethods.waitforElement(wait, obj, "guestreviewHeader");
								WebElement guestreviewHeader = BNBasicCommonMethods.findElement(driver, obj, "guestreviewHeader");
								wait.until(ExpectedConditions.visibilityOf(guestreviewHeader));
								if(BNBasicCommonMethods.isElementPresent(guestreviewHeader))
								{
									pass("The User is navigated to the Guest Review Order Page.");
								}
								else
								{
									fail("The User is not navigated to the Guest Review Order Page.");
								}
							}
						}
						else
						{
							fail("The Order Continue button is not displayed.");
						}
				childflag=true;
			}
			catch(Exception e)
			{
				exception(" There is something wrong. Please Check. " + e.getMessage());
			}
		
		ChildCreation("BNIA-1179 Guest Checkout - Verify that payment page should allow the guest user to fill the required payment details");
		if(childflag)
			pass("User able to fill all required fields and navigates to next page ");
		else
			fail("User not able to fill all required fields and navigates to next page ");
	}

	 @BeforeClass
     public void beforeTest() throws Exception
     {
    	 Thread.sleep(10000);
    	 driver = bnEmulationSetup.bnASTEmulationSetupCheckout();
    	 wait = new WebDriverWait(driver, 30);
     }
     
     @AfterClass
     public void afterTest()
     {
         driver.close();
     }
     
     @BeforeMethod
    	public static void beforeTest(Method name)
    	{
    		parent = reporter.startTest(name.getName());
    		
    	}
    	
    	@org.testng.annotations.AfterMethod
    	public static void AfterMethod(Method name)
    	{
    		reporter.endTest(parent);
    		reporter.flush();
    	}
    	
    	/*@AfterSuite
    	public static void closeReporter() 
    	{
    		reporter.flush();
    	}
    	*/
    	
    	public static void ChildCreation(String name)
    	{
    		child = reporter.startTest(name);
    		parent.appendChild(child);
    	}
    	
    	public static void endTest()
    	{
    		reporter.endTest(child);
    	}
    	
    	public static void createlog(String val)
    	{
    		child.log(LogStatus.INFO, val);
    	}
    	
    	public static void pass(String val)
    	{
    		child.log(LogStatus.PASS, val);
    		endTest();
    	}
    	
    	public static void pass(String val,ArrayList<String> logval)
    	{
    		child.log(LogStatus.PASS, val);
    		for(String lval:logval)
    		{
    			createlog(lval);
    		}
    		logval.removeAll(logval);
    		endTest();
    	}
    	
    	public static void fail(String val)
    	{
    		child.log(LogStatus.FAIL, val);
    		endTest();
    	}
    	
    	public static void fail(String val,ArrayList<String> logval)
    	{
    		child.log(LogStatus.FAIL, val);
    		for(String lval:logval)
    		{
    			createlog(lval);
    		}
    		logval.removeAll(logval);
    		endTest();
    	}
    	
    	public static  void exception(String val)
    	{
    		child.log(LogStatus.ERROR,val);
    		endTest();
    	}
    	
    	public static void Skip(String val)
    	{
    		child.log(LogStatus.SKIP, val);
    		endTest();
    	}
	
	}
